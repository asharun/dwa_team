window.Vue = require("vue");

import LegendList from '../components/LegendList.vue';

var legendList = Vue.component('legend-list', LegendList);

const app = new Vue({
  el: '#app',
  components: {
    legendList
  }
});