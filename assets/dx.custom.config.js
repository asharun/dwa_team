/* Core (dx.module-core.js) */
var DevExpress = require("devextreme/bundles/modules/core");

require("devextreme/integration/jquery");

var ui = DevExpress.ui = require("devextreme/bundles/modules/ui");

var data = DevExpress.data = require("devextreme/bundles/modules/data");

data.odata = require("devextreme/bundles/modules/data.odata");

ui.dxTreeList = require("devextreme/ui/tree_list");
ui.dxTreeView = require("devextreme/ui/tree_view");
ui.dxContextMenu = require("devextreme/ui/context_menu");
ui.dxToast = require("devextreme/ui/toast");
ui.dxDropDownBox = require("devextreme/ui/drop_down_box");
ui.dxDataGrid = require("devextreme/ui/data_grid");