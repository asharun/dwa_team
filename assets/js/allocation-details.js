var BYJU = BYJU || {};
var BASE_URL = base_url + 'index.php/Allocation/';
var HIERARCHY_ID = '';
var PARENT_LEVEL_DATA = {};
PARENT_ACTIVITY_DATA = {};
var ACTIVITY_ID = '';

var NEXT_LEVEL_MAPPING_LIST = {
	SY: 'Year',
	YE:	'Grade',
	GR: 'Subject',
	SU: 'Chapter',
	CH: 'Sub Topic',
	ST: 'Video Clip',
	VC: 'Sec Part'
};

BYJU.allocationDetails = (function() {

	$(document).ready(function() {
		if(window.location.href.indexOf("getDetailsPage") > -1) {
		   $(".allocation-top-bar").find("li.active")
			 .removeClass("active");
		   $('#details-tab').parent().addClass("active");
	  }

		$("#save-level-btn").on("click", function(e) {
				e.preventDefault();
				//console.log("HIERARCHY_ID.." + HIERARCHY_ID);
				//HIERARCHY_ID = data.hierarchy_id;
				if (HIERARCHY_ID != '') {
						updateFormLevelData();
				} else {
						storeFormLevelData();
				}
				$("#tasks").dxTreeList("refresh");
		});

		$("#save-activity-btn").on("click", function(e) {
				e.preventDefault();
				//ACTIVITY_ID = data.hierarchy_id.replace('A-', '');
				//console.log("ACTIVITY_ID.." + ACTIVITY_ID);
				if (ACTIVITY_ID != '') {
					updateFormActivityData();
				} else {
					storeFormActivityData();
				}
				$("#tasks").dxTreeList("refresh");
		});


		$("body").on("changed.bs.select", "#sel_1",function(){
	        if ($(this).val() ) {
	            loadActivityEntities({
	                param: "sel_2",
	                sel_0_ID: window.deptID,
	                sel_1_ID: $(this).val(),
	                sel_2_ID: '',
	                sel_3_ID: '',
	                sel_4_ID: '',
	                dateID: window.currentDate
	            },3);
	        }
	    });

	    $("body").on("changed.bs.select","#sel_2",function(){
			if ($(this).val()) {
	            loadActivityEntities({
	                param: "sel_3",
	                sel_0_ID: window.deptID,
	                sel_1_ID: $('#sel_1').val(),
	                sel_2_ID: $(this).val(),
	                sel_3_ID: '',
	                sel_4_ID: '',
	                dateID: window.currentDate
	            },4);
			}
		});

	    $("body").on("changed.bs.select","#sel_3",function(){
			if ($(this).val()) {
	            loadActivityEntities({
	                param: "sel_4",
	                sel_0_ID: window.deptID,
	                sel_1_ID: $('#sel_1').val(),
	                sel_2_ID: $('#sel_2').val(),
	                sel_3_ID: $(this).val(),
	                sel_4_ID: '',
	                dateID: window.currentDate
	            },0);
	        }
		});
	});

	function loadActivityEntities(postData, countToRefreshData) {
	    $.ajax({
	        url: base_url+"index.php/Load_dt/load_data",
	        type: 'post',
	        data : {
	            param : postData.param,
	            sel_0_ID: postData.sel_0_ID,
	            sel_1_ID: postData.sel_1_ID,
	            sel_2_ID: postData.sel_2_ID,
	            sel_3_ID: postData.sel_3_ID,
	            sel_4_ID: postData.sel_4_ID,
	            dateID: window.currentDate
			},
	        success: function(response){
				//console.log(response);
	            $('#' + postData.param).html(response).selectpicker('refresh');
				if (countToRefreshData > 0) {
					for(;countToRefreshData <= 4; countToRefreshData++) {
						$('#sel_' + countToRefreshData).html('').selectpicker('refresh');
					}
				}
	        }
	    });
	}

	var storeFormLevelData = function() {
		levelFormData = buildLevelFormData();
		if (validateForm(levelFormData, {
			code: 'Code',
			// end_dt: 'End Date',
			// start_dt: 'Start Date',
			// mid_name: 'MID Name',
			//mid_code: 'MID Code',
			status_nm: 'Status'
			//,owner_mid: 'Allocated to'
		})) {
			levelFormData['ins_user'] = window.userId;
			levelFormData['ins_dt'] = new Date();
			storeLevel(PARENT_LEVEL_DATA, levelFormData);
			$("#allocationModal").modal("hide");
			PARENT_LEVEL_DATA = {};
		}
	}

	var storeFormActivityData = function() {
		var activityFormData = buildActivityFormData();
		if (validateForm(activityFormData, {
			end_dt: 'End Date',
			start_dt: 'Start Date',
			project_id_fk: 'Project',
			task_id_fk: 'Category',
			sub_task_id_fk: 'Element',
			job_id_fk: 'Action',
			progress: 'Status',
			alloc_to: 'Allocated to'
		})) {
			activityFormData['ins_user'] = window.userId;
			activityFormData['ins_dt'] = new Date();
			storeActivity(PARENT_ACTIVITY_DATA, activityFormData);
			$("#activityModal").modal("hide");
			PARENT_ACTIVITY_DATA = {};
		}
	}

	var updateFormLevelData = function() {
		levelFormData = buildLevelFormData();
		if (validateForm(levelFormData, {
			code: 'Code',
			end_dt: 'End Date',
			start_dt: 'Start Date',
			mid_name: 'MID Name',
			mid_code: 'MID Code',
			status_nm: 'Status',
			owner_mid: 'Allocated to'
		})) {
			levelFormData['upd_user'] = window.userId;
			levelFormData['upd_dt'] = new Date();
			updateLevel(HIERARCHY_ID, levelFormData);
			$("#allocationModal").modal("hide");
			HIERARCHY_ID = '';
		}
	}

	var updateFormActivityData = function() {
		var activityFormData = buildActivityFormData();
		if (validateForm(activityFormData, {
			end_dt: 'End Date',
			start_dt: 'Start Date',
			project_id_fk: 'Project',
			task_id_fk: 'Category',
			sub_task_id_fk: 'Element',
			job_id_fk: 'Action',
			progress: 'Status',
			alloc_to: 'Allocated to'
		})) {
			activityFormData['upd_user'] = window.userId;
			activityFormData['upd_dt'] = new Date();
			updateActivity(ACTIVITY_ID, activityFormData);
			$("#activityModal").modal("hide");
			ACTIVITY_ID = '';
		}
	}

	var buildLevelFormData = function() {
		// Here bulid up the form data;
		var levelData = {};
		var ownerMids = '';
		$('#levelForm').serializeArray().forEach(function(input) {
			if (input.name == 'allocated_to[]') {
					ownerMids += input.value + ','
			} else {
				levelData[input.name] = input.value;
			}
		});

		ownerMids = ownerMids.replace(/,\s*$/, "");
		levelData['owner_mid'] = ownerMids;
		//console.log(levelData);
		return levelData;
	}

	var buildActivityFormData = function() {
		var levelData = {}, ownerMids = '';
		$('#activityForm').serializeArray().forEach(function(input) {
			if (input.name == 'allocated_to[]') {
					ownerMids += input.value + ','
			} else {
				levelData[input.name] = input.value;
			}
		});
		ownerMids = ownerMids.replace(/,\s*$/, "");
		levelData['alloc_to'] = ownerMids;
		return levelData;
	}

	var addAllocationEntity = function(data) {
		$("#allocationModal").modal();
		if (data.level_id_fk != '') {
			//console.log("Adding Level");
			// SET HIERARCHY_ID TO EDIT
			// HIERARCHY_ID = data.hierarchy_id;
			loadLevelModal(data, 'add');
		}
	}

	var addAllocationActivityEntity = function(data) {
		$("#activityModal").modal();
		//console.log("Adding Activity");
		loadAcitvityModal(data, 'add');
	}

	var editAllocationEntity = function(data) {
		// level_id_fk is null means it must be ACTIVITY
		if (data.level_id_fk == null) {
			$("#activityModal").modal();
			ACTIVITY_ID = data.hierarchy_id.replace('A-', '');
			loadAcitvityModal(data);
		} else if (data.level_id_fk != null) {
			$("#allocationModal").modal();
			if (data.level_id_fk != '') {
				// SET HIERARCHY_ID TO EDIT
				HIERARCHY_ID = data.hierarchy_id;
				loadLevelModal(data);
			}
		} else {
			alert("Not sure what you are editing ?");
		}

	}

	var loadAcitvityModal = function(data, operation) {
		operation = operation || 'edit';
		$('.modal-spinner').removeClass('hide');
		$('.entity-form').addClass('hide');
		setTimeout(function() {
			if (operation == 'edit') {
					loadActivityModalDataToForm(data);
			}
			if (operation == 'add') {
					resetActivityModalDataToForm(data);
					PARENT_ACTIVITY_DATA = Object.assign(data);
			}
			$('.entity-form').removeClass('hide');
			$('.modal-spinner').addClass('hide');
		}, 700);

		if (operation != 'add') {
				$("#activityModal").find('.modal-title')
				.html('Edit- <label class="label label-info">Acitvity</label> ' + data.mid_code);
		} else {
				$("#activityModal").find('.modal-title')
				.html('Add <label class="label label-info">Activity</label> to ' + data.mid_code);
		}
	}

	var loadLevelModal = function(data, operation) {
			operation = operation || 'edit';
			$('.modal-spinner').removeClass('hide');
			$('.entity-form').addClass('hide');
			setTimeout(function() {
					if (operation == 'edit') {
							loadLevelModalDataToForm(data);
					}
					if (operation == 'add') {
							resetLevelModalDataToForm(data);
							PARENT_LEVEL_DATA = Object.assign(data);
					}
					$('.entity-form').removeClass('hide');
					$('.modal-spinner').addClass('hide');
			}, 700);

			if (operation != 'add') {
					$("#allocationModal").find('.modal-title')
					.html('Edit- ' + data.mid_code);
			} else {
					$("#allocationModal").find('.modal-title')
					.html('Add <label class="label label-info">' + NEXT_LEVEL_MAPPING_LIST[data.level_code] + '</label> to ' + data.mid_code);
			}
	}

	var loadActivityModalDataToForm = function(data) {
		//console.log("Here i am loading activity modal");

		$.ajax({
		   url: base_url+"index.php/Load_dt/getActivityFormData",
		   type: 'post',
		   data : {activityData: data},
		   success: function(response){
			   //console.log("Archito Testing data...");
			   //console.log(data);
			   $('#sel_2').html(response.data.categories).selectpicker('refresh');
			   $('#sel_3').html(response.data.elements).selectpicker('refresh');
			   $('#sel_4').html(response.data.actions).selectpicker('refresh');

			   setTimeout(function() {
				$("#activityModal").find("#sel_1").val(data.project_id_fk);
				$("#activityModal").find("#sel_1").selectpicker('refresh');
				$("#activityModal").find("#sel_2").val(data.task_id_fk);
				$("#activityModal").find("#sel_2").selectpicker('refresh');
				$("#activityModal").find("#sel_3").val(data.sub_task_id_fk);
				$("#activityModal").find("#sel_3").selectpicker('refresh');
				$("#activityModal").find("#sel_4").val(data.job_id_fk);
				$("#activityModal").find("#sel_4").selectpicker('refresh');
				$("#activityModal").find('input[name="start_dt"]').val(data.start_dt);
				$("#activityModal").find('input[name="end_dt"]').val(data.end_dt);
				if (data.owner_mid !== '' && data.owner_mid !== null) {
					$("#activityModal").find('select[name="allocated_to[]"]').val(data.owner_mid.split(','));
				} else {
					$("#activityModal").find('select[name="allocated_to[]"]').val('');
				}
				$('.selectpicker').selectpicker('refresh');
				$("#activityModal").find('select[name="progress"]').val(data.status_nm);
				$('.selectpicker').selectpicker('refresh');
				$('.entity-form').removeClass('hide');
				$('.modal-spinner').addClass('hide');
			}, 100);
		   }
	   });

	}

	var loadLevelModalDataToForm = function(data) {
		$("#allocationModal").find('input[name="mid_code"]').val(data.mid_code);
		$("#allocationModal").find('input[name="mid_name"]').val(data.mid_name);
		if (data.owner_mid !== ''&& data.owner_mid !== null) {
			$("#allocationModal").find('select[name="allocated_to[]"]').val(data.owner_mid.split(','));
		} else {
			$("#allocationModal").find('select[name="allocated_to[]"]').val('');
		}
		$('.selectpicker').selectpicker('refresh');

		$("#allocationModal").find('input[name="s_dt"]').val(data.start_dt);
		$("#allocationModal").find('input[name="e_dt"]').val(data.end_dt);
		$("#allocationModal").find('select[name="status_nm"]').val(data.status_nm);
		$('.selectpicker').selectpicker('refresh');

	}

	var resetLevelModalDataToForm = function() {
		$("#levelForm").trigger("reset");
			// $("#allocationModal").find('input[name="mid_code"]').val('');
			// $("#allocationModal").find('input[name="mid_name"]').val('');
			// $("#allocationModal").find('select[name="allocated_to[]"]').val('');
			// $('.selectpicker').selectpicker('refresh');
			// $("#allocationModal").find('input[name="s_dt"]').val('');
			// $("#allocationModal").find('input[name="e_dt"]').val('');
			// $("#allocationModal").find('select[name="status_nm"]').val('');
		$('.selectpicker').selectpicker('refresh');

	}

	var resetActivityModalDataToForm = function(data) {
		$("#activityForm").trigger("reset");
		/*$("#activityModal").find('#sel_1').val('');
		$('.selectpicker').selectpicker('refresh');
		$("#activityModal").find('select[name="allocated_to[]"]').val('');
		$('.selectpicker').selectpicker('refresh');

		$("#activityModal").find('input[name="start_dt"]').val('');
		$("#activityModal").find('input[name="end_dt"]').val('');
		$("#activityModal").find('select[name="progress"]').val('');*/
		$('.selectpicker').selectpicker('refresh');
	}


	var updateLevel = function(key, values) {
		$.ajax({
			url: BASE_URL + 'updateLevel/' + key,
			type: 'POST',
			dataType: 'json',
			data: {data: values},
			success: function(response) {
				//console.log('Success');
				//console.log(response);
			}
		})
	}

	var updateActivity = function(key, values) {
		//console.log("Updating Activity")
		//console.log(key);
		//console.log(values);
		$.ajax({
			url: BASE_URL + 'updateActivity/' + key,
			type: 'POST',
			dataType: 'json',
			data: {data: values},
			success: function(response) {
				//console.log('Activity update Success');
				//console.log(response);
			}
		})
	}

	var storeActivity = function(parentData, data) {
		//console.log("Storing Activity")
		//console.log(parentData);
		data.dept_id_fk = window.deptID;
		//console.log(data);
		$.ajax({
			url: BASE_URL + 'storeActivity/',
			type: 'POST',
			dataType: 'json',
			data: {data: data, parentData: parentData},
			success: function(response) {
				//console.log('Store Activity Success');
				//console.log(response);
			}
		})
	}

	var storeLevel = function(parentData, data) {
		$.ajax({
			url: BASE_URL + 'storeLevel/',
			type: 'POST',
			dataType: 'json',
			data: {data: data, parentData: parentData,dept_id_fk : window.deptID},
			success: function(response) {
				//console.log('Store Success');
				//console.log(response);
			}
		})
	}

	return {
		addAllocationEntity,
		editAllocationEntity,
		addAllocationActivityEntity,
		updateLevel,
		updateActivity
	};
}());