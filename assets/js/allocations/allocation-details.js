var BYJU = BYJU || {},
BASE_URL = base_url + 'index.php/Allocation/',
HIERARCHY_ID = '',
PARENT_LEVEL_DATA = {},
PARENT_ACTIVITY_DATA = {},
ACTIVITY_ID = '',
CONTENT = '1',
MEDIA = '2',
YEAR_LEVEL = '2',
SYLLABUS_LEVEL = '1',
CURRENT_OPERATING_LEVEL = '',
CHAPTER = '5',
VIDEOCLIP = '7';

var NEXT_LEVEL_MAPPING_LIST = {
	SY: 'Year',
	YE:	'Grade',
	GR: 'Subject',
	SU: 'Chapter',
	CH: 'SubTopic',
	ST: 'Video Clip',
	VC: 'Sec Part'
};

window.CURRENT_DATA_SET = '';
window.IS_ALLOCATION_MODAL = false;
BYJU.allocationDetails = (function() {

	$(document).ready(function() {
		if(window.location.href.indexOf("getDetailsPage") > -1) {
		   $(".allocation-top-bar").find("li.active")
			 .removeClass("active");
		   $('#details-tab').parent().addClass("active");
		}

		$('.store-bulk-allocation').on('click', function() {
			$(this).html('<small><strong>Allocating..<strong></small>');
			var treeList = $('#tasks').dxTreeList("instance");
			var userList = $('select[name="bulk_allocated_to[]"]').val();
			var allocationData = treeList.getSelectedRowsData();
			var btn = $(this);
			$.ajax({
				url: BASE_URL + 'bulkAllocate/',
				type: 'POST',
				dataType: 'json',
				data: {userList: userList, allocationData: allocationData},
				success: function(response) {
					setTimeout(function() {
						response.data.forEach(function(input) {
							var userList = '';
							window.CURRENT_KEY = treeList.getRowIndexByKey(input.id);
							var dataSet = input.dataSet;
							dataSet.owner_mid = toArray(input.userList).toString();
							window.CURRENT_DATA_SET = toArray(dataSet);
							var userList = window.CURRENT_DATA_SET['owner_mid'];
							// window.CURRENT_DATA_SET['owner_mid'] = userList.toString();
							treeListRefreshWorkRound();
						});
						btn.html('<small><strong>Allocate<strong></small>');
						$('#bulkAllocationModal').modal('toggle');

					}, 3000)
				},
				error: function() {
					btn.html('<small><strong>Allocate<strong></small>');
					alert("something went wrong, try again later");
				}
			})
		});

		$('.get-bulk-import').on('click', function() {
		    $(this).html('<small><strong>Exporting..<strong></small>');
		    var btn = $(this);
		    var treeList = $('#tasks').dxTreeList("instance");
		    var userList = $('select[name="bulk_allocated_to[]"]').val();
		    var allocationData = treeList.getSelectedRowsData();

		    var importTypes = [];

			if ($('input[name="level-chk"]:checked').val() != undefined)
				importTypes.push($('input[name="level-chk"]:checked').val());

			if ($('input[name="activity-chk"]:checked').val() != undefined)
				importTypes.push($('input[name="activity-chk"]:checked').val());

			if (allocationData.length <= 0) {
				btn.html('<small><strong>Export<strong></small>');
			    alert("Please select level from table");
			    return;
			}

			if (importTypes.length <= 0) {
				btn.html('<small><strong>Export<strong></small>');
			    alert("Please check if export Level or Activity");
			    return;
			}

	        // Here handle for level first
	        $.ajax({
	            url: BASE_URL + 'bulkAllocationImport/',
	            type: 'POST',
	            data: {
	                importTypes: importTypes, allocationData: allocationData
	            },
	            success: function(response) {
					handleBulkImportAjaxResp(response, importTypes);
					btn.html('<small><strong>Export<strong></small>');
					$('#bulkImportModal').modal('toggle');
	            },
	            error: function(error) {
					alert("Oops! Something went wrong, try again after some time");
					btn.html('<small><strong>Export<strong></small>');
					$('#bulkImportModal').modal('toggle');
	            }
	        });

		});

		$('.bulk-upload-btn').on('click', function() {
			$('#bulkAllocationModal').modal('toggle');
		});

		$('.bulk-alloc-import-btn').on('click', function() {
		    $('#bulkImportModal').modal('toggle');
		});

		$("#save-level-btn").on("click", function(e) {
				e.preventDefault();
				// console.log("HIERARCHY_ID.." + HIERARCHY_ID);
				if (HIERARCHY_ID != '') {
					updateFormLevelData();
				} else {
					storeFormLevelData();
				}
				// $("#tasks").dxTreeList("refresh
				treeListRefreshWorkRound();
		});

		$("#save-activity-btn").on("click", function(e) {
				e.preventDefault();
				// console.log("ACTIVITY_ID.." + ACTIVITY_ID);
				if (ACTIVITY_ID != '') {
					updateFormActivityData();
				} else {
					storeFormActivityData();
				}
				// $("#tasks").dxTreeList("refresh");
				treeListRefreshWorkRound();
		});


		$("body").on("changed.bs.select", "#sel_1",function(){
	        if ($(this).val() ) {
	            loadActivityEntities({
	                param: "sel_2",
	                sel_0_ID: window.deptID,
	                sel_1_ID: $(this).val(),
	                sel_2_ID: '',
	                sel_3_ID: '',
	                sel_4_ID: '',
	                dateID: window.currentDate
	            },3);
	        }
	    });

	    $("body").on("changed.bs.select","#sel_2",function(){
			// console.log("Actvity Testing");
			// console.log();
			var projectId =  $('#sel_1').val();

			// HANDLE ELEMENT LOADING WHEN NO PROJECT
			// SELECTED WHEN DEPARTMENT IS MEDIA
			if (window.deptID == MEDIA && projectId == '') {
				projectId = 'no-project-id';
			}

			if ($(this).val()) {
	            loadActivityEntities({
	                param: "sel_3",
	                sel_0_ID: window.deptID,
	                sel_1_ID: projectId,
	                sel_2_ID: $(this).val(),
	                sel_3_ID: '',
	                sel_4_ID: '',
	                dateID: window.currentDate
	            },4);
			}
		});

	    $("body").on("changed.bs.select","#sel_3",function(){
			if ($(this).val()) {
				var projectId = $('#sel_1').val();

				// HANDLE ACTION/JOB LOADING WHEN NO PROJECT
				// SELECTED WHEN DEPARTMENT IS MEDIA
				if (window.deptID == MEDIA && projectId == '') {
					projectId = 'no-project-id';
				}
	            loadActivityEntities({
	                param: "sel_4",
	                sel_0_ID: window.deptID,
	                sel_1_ID: projectId,
	                sel_2_ID: $('#sel_2').val(),
	                sel_3_ID: $(this).val(),
	                sel_4_ID: '',
	                dateID: window.currentDate
	            },0);
	        }
		});

		// Update chapter video duration log
		$(document).on('click', '.update-video-clip-duration-btn', function() {
			var id = $(this).data('id');
			$(this).html('<small><strong>Updating..<strong></small>');
			var formId = 'duration-form-' + id;
			data = {
				recorded: $('#'+formId).find('input[name="recorded"]').val(),
				logged: $('#'+formId).find('input[name="logged"]').val()
			};
			var btn = $(this);
			updateChapterVideoClipDuration(id, data);
			setTimeout(function() {
				btn.html('<small><strong>Update<strong></small>');
			}, 1000);
		});
	});

	function handleBulkImportAjaxResp(response, importTypes) {
		if (response.data.level != undefined && response.data.level.length > 0) {
			exportToCSV(response.data.level, 'LevelData', [
				'MID', 'NAME', 'OWNER', 'START DATE', 'END DATE',
				'ESTIMATED END DATE', 'PROGRESS', 'STATUS', 'DESCRIPTION',
				'PRESENTER', 'STORYBOARD INCHARGE', 'SIGN OFF', 'MANDAYS',
				'VIDEO CLIP TYPE', 'STORYBOARD BY', 'DURATION'
			]);
		}

		if (response.data.activity != undefined && response.data.activity.length > 0) {
			exportToCSV(response.data.activity, 'ActivityData', [
				'PROJECT', 'TASK', 'SUB TASK', 'JOB', 'ALLOCATED TO', 'MID',
				'START DATE', 'END DATE', 'ESTIMATED END DATE', 'PROGRESS', 'STATUS', 'TASK TYPE'
			]);
		}

		if (response.data.activity != undefined && response.data.activity.length <= 0 && importTypes.includes('activity')) {
			alert('Sorry, No Activities available in selected level');
		}
	}

	function loadActivityEntities(postData, countToRefreshData) {
	    $.ajax({
	        url: base_url+"index.php/Load_dt/load_data",
	        type: 'post',
	        data : {
	            param : postData.param,
	            sel_0_ID: postData.sel_0_ID,
	            sel_1_ID: postData.sel_1_ID,
	            sel_2_ID: postData.sel_2_ID,
	            sel_3_ID: postData.sel_3_ID,
	            sel_4_ID: postData.sel_4_ID,
	            dateID: window.currentDate
			},
	        success: function(response){
				// console.log(response);
	            $('#' + postData.param).html(response).selectpicker('refresh');
				if (countToRefreshData > 0) {
					for(;countToRefreshData <= 4; countToRefreshData++) {
						$('#sel_' + countToRefreshData).html('').selectpicker('refresh');
					}
				}
	        }
	    });
	}

	var storeFormLevelData = function() {
		levelFormData = buildLevelFormData();
		if (levelFormData && validateForm(levelFormData, ['mid_code'], {
			mid_code: 'MID Code'
		})) {
			levelFormData['ins_user'] = window.userId;
			levelFormData['ins_dt'] = new Date();
			// var resLevel = storeLevel(PARENT_LEVEL_DATA, levelFormData);
			var parentData = PARENT_LEVEL_DATA;
			var btn = $("#save-level-btn");
			btn.text("Loading..");
			btn.attr('disabled',true);
			$.ajax({
				url: BASE_URL + 'storeLevel/',
				type: 'POST',
				dataType: 'json',
				data: {data: levelFormData, parentData: parentData, dept_id_fk: window.deptID},
			})
			.done(function(resLevel) {
				if(typeof resLevel !== "undefined"){
				var message = typeof resLevel.message != "undefined" ? resLevel.message : "Something went wrong.";
				if(resLevel.status){
					PARENT_LEVEL_DATA = {};
					CURRENT_OPERATING_LEVEL = '';
					swal("Success!",message,"success")
					.then((value) => {
					  window.location.reload();
					});
				}else{
					swal("Error!",message,"error");
				}
			}else{
				swal("Error!","Something went wrong.","error");
			}
			})
			.fail(function() {
			})
			.always(function() {
				btn.text("Save");
				btn.attr('disabled',false);
			});
		}
	}

	var storeFormActivityData = function() {
		var activityFormData = buildActivityFormData();
		if (validateForm(activityFormData, [
			// 'end_dt',
			// 'start_dt',
			// 'project_id_fk',
			'task_id_fk',
			'sub_task_id_fk',
			// 'job_id_fk',
			'progress',
			// 'alloc_to'
		], {
			// end_dt: 'End Date',
			// start_dt: 'Start Date',
			// project_id_fk: 'Project',
			task_id_fk: 'Category',
			sub_task_id_fk: 'Element',
			// job_id_fk: 'Action',
			progress: 'Status',
			// alloc_to: 'Allocated to'
		})) {
			activityFormData['ins_user'] = window.userId;
			activityFormData['ins_dt'] = new Date();
			activityFormData['dept_id_fk'] = window.deptID;
			var parentData = PARENT_ACTIVITY_DATA;
			// var resActivity = storeActivity(PARENT_ACTIVITY_DATA, activityFormData);
			var btn = $("#save-activity-btn");
			btn.text("Loading..");
			btn.attr('disabled',true);
			$.ajax({
				url: BASE_URL + 'storeActivity/',
				type: 'POST',
				dataType: 'json',
				data: {data: activityFormData, parentData: parentData},
			})
			.done(function(resActivity) {
				if(typeof resActivity !== "undefined"){
					var message = typeof resActivity.message != "undefined" ? resActivity.message : "Something went wrong.";
					if(resActivity.status){
						$("#activityModal").modal("hide");
						PARENT_ACTIVITY_DATA = {};
						swal("Success!",message,"success")
						.then((value) => {
						  window.location.reload();
						});
					}else{
						swal("Error!",message,"error");
					}
				}else{
					swal("Error!","Something went wrong.","error");
				}
			})
			.fail(function() {
				swal("Error!","Something went wrong.","error");
			})
			.always(function() {
				btn.text("Save");
				btn.attr('disabled',false);
			});

		}
	}

	var updateFormLevelData = function() {
		levelFormData = buildLevelFormData();

		if (levelFormData == false) {
			return false;
		}
		if (validateForm(levelFormData, ['mid_code'], {
			mid_code: 'MID Code',
		})) {
			var levelCodeName = $(".levelcodename").text().split(':')[0];

 			levelFormData['upd_user'] = window.userId;
			window.CURRENT_DATA_SET = levelFormData;
			var resLevel = updateLevel(HIERARCHY_ID, levelFormData);
			if(typeof resLevel !== "undefined"){
				var message = typeof resLevel.message != "undefined" ? resLevel.message : "Something went wrong.";
				if(resLevel.status){
					HIERARCHY_ID = '';
					PARENT_ACTIVITY_DATA = {};
					CURRENT_OPERATING_LEVEL = '';
					$("#allocationModal").modal("hide");
				}else{
					swal("Error!",message,"error");
				}
			}else{
				swal("Error!","Something went wrong.","error");
			}
			// window.location.reload();
		}
	}

	var updateFormActivityData = function() {
		var activityFormData = buildActivityFormData();
		if (validateForm(activityFormData, [
			// 'end_dt',
			// 'start_dt',
			// 'project_id_fk',
			'task_id_fk',
			'sub_task_id_fk',
			// 'job_id_fk',
			'progress',
			// 'alloc_to'
		], {
			// end_dt: 'End Date',
			// start_dt: 'Start Date',
			// project_id_fk: 'Project',
			task_id_fk: 'Category',
			sub_task_id_fk: 'Element',
			// job_id_fk: 'Action',
			progress: 'Status',
			// alloc_to: 'Allocated to'
		})) {
			activityFormData['upd_user'] = window.userId;
			activityFormData['upd_dt'] = new Date();
			window.CURRENT_DATA_SET = activityFormData;

			var actRes = updateActivity(ACTIVITY_ID, activityFormData);
			if(typeof actRes !== "undefined"){
				var message = typeof actRes.message != "undefined" ? actRes.message : "Something went wrong.";
				if(actRes.status){
					ACTIVITY_ID = '';
					$("#activityModal").modal("hide");
				}else{
					swal("Error!",message,"error");
				}
			}else{
				swal("Error!","Something went wrong.","error");
			}
		}
	}

	var buildLevelFormData = function() {
		// Here bulid up the form data;
		var levelData = {};
		var ownerMids = presenter = storyboardIncharge  = signOff = storyboardedBy = '';
		// console.log($('#levelForm').serializeArray());
		$('#levelForm').serializeArray().forEach(function(input) {
			if (input.name == 'allocated_to[]') {
					ownerMids += input.value + ','
			} else if (input.name == 'presenter[]') {
					presenter += input.value + ','
			} else if (input.name == 'storyboard_incharge[]') {
					storyboardIncharge += input.value + ','
			} else if (input.name == 'sign_off[]') {
					signOff += input.value + ','
			} else if (input.name == 'storyboarded_by[]') {
					storyboardedBy += input.value + ','
			} else {
				levelData[input.name] = input.value;
			}
		});

		var midCodeForValidation = ''
		if (CURRENT_OPERATING_LEVEL == "Year") {
			midCodeForValidation = levelData['mid_code_1'];
		} else {
			midCodeForValidation = levelData['mid_code_2'];
		}

		if (CURRENT_OPERATING_LEVEL != "Video Clip") {
			levelData['video_clip_type'] = '';
		}

		if (!ValidateLevelcode(CURRENT_OPERATING_LEVEL,midCodeForValidation)) {
			alert("MID Code is not valid");
			return false;
		}

		// HANDLE MID CODE AS BECAUSE TWO PARTS OF MID
		// console.log(PARENT_LEVEL_DATA);
		levelData['mid_code'] = (levelData['mid_code_2'] != '' && levelData['mid_code_1'] != '') ? levelData['mid_code_1'] + levelData['mid_code_2'] : '';
		levelData['mid_code'] = (PARENT_LEVEL_DATA.level_id_fk == SYLLABUS_LEVEL) ? levelData['mid_code_1'] + levelData['mid_code_2'] : levelData['mid_code'];
		// console.log("Right now testing here");
		// console.log(levelData);
		delete levelData['mid_code_1'];
		delete levelData['mid_code_2'];
		ownerMids = ownerMids.replace(/,\s*$/, "");
		presenter = presenter.replace(/,\s*$/, "");
		storyboardIncharge = storyboardIncharge.replace(/,\s*$/, "");
		signOff = signOff.replace(/,\s*$/, "");
		storyboardedBy = storyboardedBy.replace(/,\s*$/, "");


		levelData['owner_mid'] = ownerMids;
		levelData['presenter'] = presenter;
		levelData['storyboard_incharge'] = storyboardIncharge;
		levelData['sign_off'] = signOff;
		levelData['storyboarded_by'] = storyboardedBy;

		delete levelData['recorded'];
		delete levelData['logged'];
		return levelData;
	}

	var buildActivityFormData = function() {
		var levelData = {}, ownerMids = '';
		$('#activityForm').serializeArray().forEach(function(input) {
			if (input.name == 'allocated_to[]') {
					ownerMids += input.value + ','
			} else {
				levelData[input.name] = input.value;
			}
		});
		ownerMids = ownerMids.replace(/,\s*$/, "");
		levelData['alloc_to'] = ownerMids;
		return levelData;
	}

	var addAllocationEntity = function(data) {

		hideExtraInfoColumns(Number(data.level_id_fk) + Number(1))

		$("#allocationModal").modal();
		if (data.level_id_fk != '') {
			// console.log("Adding Level");
			// SET HIERARCHY_ID TO EDIT
			// HIERARCHY_ID = data.hierarchy_id;
			loadLevelModal(data, 'add');
		}
		handlerForAllocationModal(Number(data.level_id_fk) + Number(1));
	}

	var addAllocationActivityEntity = function(data) {
		$("#activityModal").modal();
		// console.log("Adding Activity");
		loadAcitvityModal(data, 'add');
		handlerForAllocationModal(data.level_id_fk);
	}

	var hideExtraInfoColumns = function(levelID) {
		$('.video-clip-extra-info').addClass('hide');
		$('.chapter-extra-info').addClass('hide');

		if (levelID == CHAPTER) {
			$('.chapter-extra-info').removeClass('hide');
			$('.video-clip-extra-info').addClass('hide');
		}
		if (levelID == VIDEOCLIP) {
			$('.video-clip-extra-info').removeClass('hide');
			$('.chapter-extra-info').addClass('hide');
		}
	}
	var editAllocationEntity = function(data) {
		var hierarchyId = data.hierarchy_id;
		$.ajax({
			url: BASE_URL + 'getLevel/' + hierarchyId,
			type: 'GET',
			dataType: 'json',
			success: function(response) {
				console.log('Get Success');
				console.log(response.end_dt);
				data = response;
				hideExtraInfoColumns(data.level_id_fk);

				// level_id_fk is null means it must be ACTIVITY
				if (data.level_id_fk == null) {
					$("#activityModal").modal();
					ACTIVITY_ID = data.hier_act_log_id;
					loadAcitvityModal(data);
					handlerForAllocationModal(data.level_id_fk);

				} else if (data.level_id_fk != null) {
					$("#allocationModal").modal();
					if (data.level_id_fk != '') {
						// SET HIERARCHY_ID TO EDIT
						HIERARCHY_ID = data.hierarchy_id;
						loadLevelModal(data);
					}
					handlerForAllocationModal(data.level_id_fk);
				} else {
					alert("Not sure what you are editing ?");
				}
			}
		})


	}

	var loadAcitvityModal = function(data, operation) {
		operation = operation || 'edit';
		$('.modal-spinner').removeClass('hide');
		$('.entity-form').addClass('hide');
		setTimeout(function() {
			if (operation == 'edit') {
					loadActivityModalDataToForm(data);
			}
			if (operation == 'add') {
					resetActivityModalDataToForm(data);
					PARENT_ACTIVITY_DATA = Object.assign(data);
			}
			$('.entity-form').removeClass('hide');
			$('.modal-spinner').addClass('hide');
		}, 700);

		if (operation != 'add') {
				$("#activityModal").find('.modal-title')
				.html('Edit- <label class="label label-info">Acitvity</label> ' + data.mid_code);
		} else {
				$("#activityModal").find('.modal-title')
				.html('Add <label class="label label-info">Activity</label> to ' + data.mid_name);
		}
	}

	var loadLevelModal = function(data, operation) {
		// console.log(data);
			operation = operation || 'edit';
			$('.modal-spinner').removeClass('hide');
			$('.entity-form').addClass('hide');
			$(".chapter-duration-grid-box").html("");
			setTimeout(function() {
					if (operation == 'edit') {
							PARENT_LEVEL_DATA = Object.assign(data);
							loadLevelModalDataToForm(data);

							// RENDER CHAPTER DURATION GRID
							if (data.level_id_fk == CHAPTER) {
								BYJU.allocationDetails.fetchChapterDurationGrid(
									data.hierarchy_id
								);
							}
					}
					if (operation == 'add') {
							HIERARCHY_ID = '';
							PARENT_LEVEL_DATA = Object.assign(data);
							resetLevelModalDataToForm(data);
					}
					$('.entity-form').removeClass('hide');
					$('.modal-spinner').addClass('hide');
			}, 700);

			if (operation != 'add') {
					$("#allocationModal").find('.modal-title')
					.html('Edit- ' + data.mid_name);
			} else {
					$("#allocationModal").find('.modal-title')
					.html('Add <label class="label label-info" id="level-label">' + NEXT_LEVEL_MAPPING_LIST[data.level_code] + '</label> to ' + data.mid_name);
			}

			addMidCodeHelpBlock(data, operation);
	}

	var addMidCodeHelpBlock = function(data, operation) {
		// console.log(operation);
		if (operation == 'add') {
			var levelData = findInArrayByKeyValue(window.allLevelData, 'level_name', $('#level-label').text());
		} else {
			var levelData = findInArrayByKeyValue(window.allLevelData, 'level_code', data.level_code);
		}
		CURRENT_OPERATING_LEVEL = levelData.level_name;
		var midCodeExample = levelData.level_code_example;
		helpBlockDataContent = 'Must have ' + midCodeExample.length + ' character Example ' + midCodeExample;
		if (levelData.level_code == 'YE') {
			$('#mid_code_1_label').html('Year <a class="popuppattern"  title="Field Pattern" onmouseover="" style="cursor: pointer;" data-content="'+helpBlockDataContent+'"><i class="glyphicon glyphicon-info-sign"></i></a>');
			$('#mid_code_2_label').html('CODE');
		} else {
			$('#mid_code_1_label').html('CODE');
			$('#mid_code_2_label').html(levelData.level_name + ' <a class="popuppattern"  title="Field Pattern" onmouseover="" style="cursor: pointer;" data-content="'+helpBlockDataContent+'"><i class="glyphicon glyphicon-info-sign"></i></a>')
		}
	}

	var loadActivityModalDataToForm = function(data) {
		console.log("Here i am loading activity modal");

		$.ajax({
		   url: base_url+"index.php/Load_dt/getActivityFormData",
		   // type: 'post',
		   data : {activityData: data, deptId: window.deptID},
		   success: function(response){
		   	console.log('response');
		   	console.log(response);
			   $('#sel_2').html(response.data.categories).selectpicker('refresh');
			   $('#sel_3').html(response.data.elements).selectpicker('refresh');
			   $('#sel_4').html(response.data.actions).selectpicker('refresh');

			   setTimeout(function() {
				$("#activityModal").find("#sel_1").val(data.project_id_fk);
				// $("#activityModal").find("#sel_1").selectpicker('refresh');
				$("#activityModal").find("#sel_2").val(data.task_id_fk);
				// $("#activityModal").find("#sel_2").selectpicker('refresh');
				$("#activityModal").find("#sel_3").val(data.sub_task_id_fk);
				// $("#activityModal").find("#sel_3").selectpicker('refresh');
				$("#activityModal").find("#sel_4").val(data.job_id_fk);
				// $("#activityModal").find("#sel_4").selectpicker('refresh');
				$("#activityModal").find('input[name="start_dt"]').val( (data.start_dt !== '' && data.start_dt !== null) ? moment( data.start_dt).format("DD-MM-YYYY") : '');
				$("#activityModal").find('input[name="end_dt"]').val((data.end_dt !== '' && data.end_dt !== null) ? moment( data.end_dt).format("DD-MM-YYYY") : '');
				$("#activityModal").find('input[name="est_end_dt"]').val((data.est_end_dt !== '' && data.est_end_dt !== null) ? moment( data.est_end_dt).format("DD-MM-YYYY") : '');

				if (data.alloc_to !== '' && data.alloc_to !== null) {
					$("#activityModal").find('select[name="allocated_to[]"]').val(data.alloc_to.split(','));
				} else {
					$("#activityModal").find('select[name="allocated_to[]"]').val('');
				}
				// $('.selectpicker').selectpicker('refresh');
				$("#activityModal").find('select[name="progress"]').val(data.progress);
				// $('.selectpicker').selectpicker('refresh');
				$("#activityModal").find('select[name="status_nm"]').val(data.status_nm);
				$("#task_type").val(data.task_type);

				$('.selectpicker').selectpicker('refresh');
				$('.entity-form').removeClass('hide');
				$('.modal-spinner').addClass('hide');
			}, 100);
		   }
	   });

	}

	var loadLevelModalDataToForm = function(data) {
		// console.log("DRe", data);
		$("#allocationModal").find('input[name="mid_code"]').val(data.mid_code);
		$("#allocationModal").find('input[name="mid_code"]').val(data.mid_code);
		$("#allocationModal").find('input[name="mid_name"]').val(data.mid_name);
		if (data.owner_mid !== ''&& data.owner_mid !== null) {
			$("#allocationModal").find('select[name="allocated_to[]"]').val(data.owner_mid.split(','));
		} else {
			$("#allocationModal").find('select[name="allocated_to[]"]').val('');
		}
		// $('.selectpicker').selectpicker('refresh');


		if (data.presenter !== '' && data.presenter !== null) {
			$("#allocationModal").find('select[name="presenter[]"]').val(data.presenter.split(','));
		} else {
			$("#allocationModal").find('select[name="presenter[]"]').val('');
		}

		if (data.hier_desc !== '' && data.hier_desc !== null) {
			$("#allocationModal").find('textarea[id="hier_desc"]').val(data.hier_desc);
		}

		// $('.selectpicker').selectpicker('refresh');

		if (data.storyboard_incharge !== '' && data.storyboard_incharge !== null) {
			$("#allocationModal").find('select[name="storyboard_incharge[]"]').val(data.storyboard_incharge.split(','));
		} else {
			$("#allocationModal").find('select[name="storyboard_incharge[]"]').val('');
		}
		// $('.selectpicker').selectpicker('refresh');

		if (data.sign_off !== '' && data.sign_off !== null) {
			$("#allocationModal").find('select[name="sign_off[]"]').val(data.sign_off.split(','));
		} else {
			$("#allocationModal").find('select[name="sign_off[]"]').val('');
		}
		// $('.selectpicker').selectpicker('refresh');

		if (data.storyboarded_by !== '' && data.storyboarded_by !== null) {
			$("#allocationModal").find('select[name="storyboarded_by[]"]').val(data.storyboarded_by.split(','));
		} else {
			$("#allocationModal").find('select[name="storyboarded_by[]"]').val('');
		}
		// $('.selectpicker').selectpicker('refresh');


		$("#allocationModal").find('input[name="start_dt"]').val((data.start_dt !== '' && data.start_dt !== null) ? moment( data.start_dt).format("DD-MM-YYYY") : '');
		$("#allocationModal").find('input[name="end_dt"]').val((data.end_dt !== '' && data.end_dt !== null) ? moment( data.end_dt).format("DD-MM-YYYY") : '');
		$("#allocationModal").find('input[name="est_end_dt"]').val((data.est_end_dt !== '' && data.est_end_dt !== null) ? moment( data.est_end_dt).format("DD-MM-YYYY") : '');

		if (data.mandays !== ''&& data.mandays !== null) {
			$("#allocationModal").find('input[name="mandays"]').val(data.mandays);
		}

		if (data.video_clip_type !== ''&& data.video_clip_type !== null) {
			$('#video_clip_type').val(data.video_clip_type);
		}

		if (data.duration !== ''&& data.duration !== null) {
			$("#allocationModal").find('input[name="duration"]').val(data.duration);
		}

		$("#allocationModal").find('select[name="progress"]').val(data.progress);
		// $('.selectpicker').selectpicker('refresh');
		$("#allocationModal").find('select[name="status_nm"]').val(data.status_nm);
		$('.selectpicker').selectpicker('refresh');
		handleLevelMidCodeUi(data);
	}

	var resetLevelModalDataToForm = function() {
		$("#allocationModal").find('input[name="mid_code_1"]').val('').prop('readonly', false);
		$("#allocationModal").find('input[name="mid_code_2"]').val('').prop('readonly', false);
		$("#levelForm").trigger("reset");
		/*$("#allocationModal").find('input[name="mid_code"]').val('');
		$("#allocationModal").find('input[name="mid_name"]').val('');
		$("#allocationModal").find('select[name="allocated_to[]"]').val('');
		$('.selectpicker').selectpicker('refresh');
		$("#allocationModal").find('select[name="presenter[]"]').val('');
		$('.selectpicker').selectpicker('refresh');
		$("#allocationModal").find('select[name="storyboard_incharge[]"]').val('');
		$('.selectpicker').selectpicker('refresh');
		$("#allocationModal").find('select[name="sign_off[]"]').val('');
		$('.selectpicker').selectpicker('refresh');
		$("#allocationModal").find('select[name="storyboarded_by[]"]').val('');
		$('.selectpicker').selectpicker('refresh');
		// $("#allocationModal").find('input[name="est_end_dt"]').val('');
		$("#allocationModal").find('input[name="mandays"]').val('');
		$("#allocationModal").find('input[name="video_clip_type"]').val('');
		$("#allocationModal").find('input[name="duration"]').val('');
		// $("#allocationModal").find('input[name="start_dt"]').val('');
		// $("#allocationModal").find('input[name="end_dt"]').val('');
		$("#allocationModal").find('select[name="progress"]').val('');
		$('.selectpicker').selectpicker('refresh');
		$("#allocationModal").find('select[name="status_nm"]').val('');*/
		$('.selectpicker').selectpicker('refresh');
		handleLevelMidCodeUi(PARENT_LEVEL_DATA, true);
	}

	/**
	 * @desc Handling disabling and enabling of parent child MID code based on level_id_fk
	 * @param (Object) data row data
	 * @param (Boolean) isAddNextLevel flag to check if is adding nect level or just editing existing level
	 */
	var handleLevelMidCodeUi = function(data, isAddNextLevel) {
		isAddNextLevel = isAddNextLevel || false;
		data = data || null;
		var isAddingYearLevel = (isAddNextLevel && PARENT_LEVEL_DATA.level_id_fk == SYLLABUS_LEVEL) ? true : false;

		// If we are editing year level or just adding new our under syllabus
		if ( (data != null && data.level_id_fk == YEAR_LEVEL) || isAddingYearLevel ) {
			var childMid = data.mid_code.split(data.parent_mid);
			if (isAddingYearLevel) {
				parentMid = childMid[0];
				childMid = '';
			} else {
				parentMid = data.parent_mid != null ? data.parent_mid : childMid[1];
				childMid = childMid[0];
			}
			// console.log("Archito Testing..");
			// console.log(data);
			// console.log(childMid, parentMid);
			$("#allocationModal").find('input[name="mid_code_1"]').val(childMid).prop('readonly', false);
			$("#allocationModal").find('input[name="mid_code_2"]').val(parentMid).prop('readonly', true);
		}

		// If we are adding non year level or just existing non year level
		if ( (data != null && data.level_id_fk != YEAR_LEVEL && !isAddingYearLevel) || (!isAddingYearLevel && isAddNextLevel) ) {
			var childMid = data.mid_code.split(data.parent_mid);
			childMid = (childMid[0] != "") ? childMid[0] : childMid[1];
			if (isAddNextLevel) {
				var parentMid = PARENT_LEVEL_DATA.mid_code;
				childMid = '';
			} else {
				var parentMid = data.parent_mid;
			}
			// console.log("Archito Testing...");
			// console.log(childMid, parentMid);
			$("#allocationModal").find('input[name="mid_code_1"]').val(parentMid).prop('readonly', true);
			$("#allocationModal").find('input[name="mid_code_2"]').val(childMid).prop('readonly', false);
		}

		// Disable MID edition according to department
		if (window.deptID == CONTENT) {
			$("#allocationModal").find('input[name="mid_code_1"]').prop('readonly', true);
			$("#allocationModal").find('input[name="mid_code_2"]').prop('readonly', true);
		}
	}

	var resetActivityModalDataToForm = function(data) {
		$("#activityForm").trigger("reset");
		/*$("#activityModal").find('#sel_1').val('');
		$('.selectpicker').selectpicker('refresh');
		$("#activityModal").find('select[name="allocated_to[]"]').val('');
		$('.selectpicker').selectpicker('refresh');

		$("#activityModal").find('input[name="start_dt"]').val('');
		$("#activityModal").find('input[name="end_dt"]').val('');
		$("#activityModal").find('select[name="progress"]').val('');
		$('.selectpicker').selectpicker('refresh');
		$("#activityModal").find('select[name="status_nm"]').val('');*/
		$('.selectpicker').selectpicker('refresh');

	}


	var updateLevel = function(key, values) {
		var response = {};
		$.ajax({
			url: BASE_URL + 'updateLevel/' + key,
			type: 'POST',
			dataType: 'json',
			async : false,
			data: {data: values, deptId: window.deptID},
			success: function(res) {
				response = res;
			}
		})
		return response;
	}

	var updateActivity = function(key, values) {
		// console.log("Updating Activity")
		// console.log(key);
		// console.log(values);
		var response = {};
		$.ajax({
			url: BASE_URL + 'updateActivity/' + key,
			type: 'POST',
			async : false,
			dataType: 'json',
			data: {data: values},
			success: function(res) {
				response = res;
				// console.log('Activity update Success');
				// console.log(response);
			}
		});
		return response;
	}

	var storeActivity = function(parentData, data) {
		// console.log("Storing Activity")
		// console.log(parentData);
		var response = {};
		data.dept_id_fk = window.deptID;
		// console.log(data);
		var btn = $("#save-activity-btn");
		btn.text("Loading..");
		btn.attr('disabled',true);

		$.ajax({
			url: BASE_URL + 'storeActivity/',
			type: 'POST',
			async : false,
			dataType: 'json',
			data: {data: data, parentData: parentData},
		})
		.done(function(res) {
			response = res;
		})
		.fail(function() {
			swal("Error!","Something went wrong.","error");
		})
		.always(function() {
			btn.text("Save");
			btn.attr('disabled',false);
		});

		return response;
	}

	var storeLevel = function(parentData, data) {
		var response = {};
		var btn = $("#save-activity-btn");
		btn.text("Loading..");
		btn.attr('disabled',true);
		$.ajax({
			url: BASE_URL + 'storeLevel/',
			type: 'POST',
			dataType: 'json',
			async: false,
			data: {data: data, parentData: parentData, dept_id_fk: window.deptID},
		})
		.done(function(res) {
			response = res;
		})
		.fail(function() {
		})
		.always(function() {
			btn.text("Save");
			btn.attr('disabled',false);
		});
		return response;
	}

	var allocationMidData = function() {
		window.isExport = '';
		// console.log("May be, ajax call is working...");
		if (window.exportResource != '') {
			$.ajax({
			   url: window.exportResource,
			   type: 'GET',
			   success: function(response) {
				   // console.log(response.data);
				   csvData = convertToCSV(response.data);
				   saveAs(csvData,"export.csv");
				   $(document).find('.waiting-notif-alloc-export').remove();
				   $('.allocation-export-btn').attr('disabled', false);
			   },
			   error: function(error) {
				   // console.log("Error occured");
				   // console.log(error);
			   }
		   });
	   } else {
		   alert("Please select level to export data");
	   }

	}

	var saveAs = function(csvData) {
	    var blob = new Blob([csvData], { type: "text/csv;charset=utf-8;" });

	    if (navigator.msSaveBlob) {
	        navigator.msSaveBlob(blob, 'Job_Progress.csv')
	    }
	    else {
	        var link = document.createElement("a");
	        var url = URL.createObjectURL(blob);
	        link.setAttribute("href", url);
	        link.setAttribute("download", "Job_Progress.csv");
	        document.body.appendChild(link);
	        link.click();
	        document.body.removeChild(link);
	    };
	}


	var convertToCSV = function(objArray) {
			$(document).find('.waiting-notif-alloc-export').html("Wait, System is creating CSV file..");
	        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
	        var str = '';

	        var hide=["hierarchy_id","level_id_fk","p_hierarchy_id", "show_p", "status_nm"];

	        var headin=[ "MID Code", "MID Name", "Parent MID", "Allocated To", "Progress", "Start Date", "End Date", "Level Code"];

	         str += headin + '\r\n';
	        for (var i = 0; i < array.length; i++) {
	            var line = '';
	        for (var index in array[i]) {
	            if(hide.indexOf(index)<0)
	            {
	                // console.log(array[i], index);
	                var ch_rec=(array[i][index])?array[i][index].replace(/"/g, '\''):array[i][index];
	                //console.log(('jn lsd').replace(/"/g, '\''));
	                if (line != '')
	                {
	                    line += ',';
	                }
	                if(index=="end_dt" || index == "start_dt")
	                {
	                    line += ("'"+ch_rec+"'"=="'null'")?"":'"'+moment(new Date(ch_rec)).format("DD/MM/YYYY")+'"';
	                }
	                else if(index=="owner_mid")
	                {		var userList = ch_rec.split(',');
							var allocatedUsersNames = '';
							userList.forEach(function(key) {
								userStore.byKey(key).done(function(data) {
	                                allocatedUsersNames += data.full_name + ', ';
	                            });
							});
							allocatedUsersNames = allocatedUsersNames.replace(/,\s*$/, "");
							// console.log(allocatedUsersNames);
							ch_rec = allocatedUsersNames;
							line += ("'"+ch_rec+"'"=="'null'")?"":'"'+ch_rec+'"';
	                }
	                else{
	                    // console.log(array[i]["level_code"]);
	                    if (index == "mid_code" && array[i]['level_code'] == "GR") {
	                        line += ("'"+ch_rec+"'"=="'null'")?"":'"  '+ch_rec+'"';

	                    } else if (index == "mid_code" && array[i]['level_code'] == "SU") {
	                        line += ("'"+ch_rec+"'"=="'null'")?"":'"   '+ch_rec+'"';
	                    } else if (index == "mid_code" && array[i]['level_code'] == "CH") {
	                        line += ("'"+ch_rec+"'"=="'null'")?"":'"    '+ch_rec+'"';
	                    } else if (index == "mid_code" && array[i]['level_code'] == "ST") {
	                        line += ("'"+ch_rec+"'"=="'null'")?"":'"     '+ch_rec+'"';
	                    } else if (index == "mid_code" && array[i]['level_code'] == "VC") {
	                        line += ("'"+ch_rec+"'"=="'null'")?"":'"      '+ch_rec+'"';
	                    } else if (index == "mid_code" && array[i]['level_code'] == "SP") {
	                        line += ("'"+ch_rec+"'"=="'null'")?"":'"       '+ch_rec+'"';
	                    } else {
	                        line += ("'"+ch_rec+"'"=="'null'")?"":'"'+ch_rec+'"';
	                    }
	                }
	            }
	            // console.log(line);
	        }

	        str += line + '\r\n';
	    }
	    return str;
	}


	var fetchChapterDurationGrid = function(chapterId) {
		$('.chapter-duration-grid-box').html('<h4><strong>Fetching video clip..</strong><h4>');
		$.ajax({
			url: BASE_URL + 'getChapterDurationGrid/' + chapterId,
			type: 'GET',
			success: function(response) {
				// console.log('Grid Success');
				setTimeout(function() {
					$('.chapter-duration-grid-box').html(response);
				}, 1000);
			}
		})
	}

	var updateChapterVideoClipDuration = function(id, data) {
		$.ajax({
			url: BASE_URL + 'updateChapterDurationInfo/' + id,
			type: 'POST',
			dataType: 'json',
			data: {data: data},
			success: function(response) {
				// console.log('Grid Update');
				// console.log(response);
			}
		})
	}

	var handlerForAllocationModal = function(levelId, isAllocationModal) {
		isAllocationModal = isAllocationModal || false;
		if (isAllocationModal == true) {
			window.IS_ALLOCATION_MODAL = true;
			if (levelId != null) {
				$('#allocationModal').find('div.col-sm-12:not(".allocation-box")').addClass('hide');
				$('#allocationModal').find('div.col-md-3:not(".allocation-box")').addClass('hide');
				if (levelId == CHAPTER) {
					$('#allocationModal').find('div.chapter-extra-info').addClass('hide');
				}
				if (levelId == VIDEOCLIP) {
					$('#allocationModal').find('div.video-clip-extra-info').addClass('hide');
				}
				// $('#allocationModal').find('div.allocation-box').removeClass('col-md-3').addClass('col-sm-12 allocation-box');
			} else {
				$('#activityModal').find('div.col-md-6:not(".allocation-box")').addClass('hide');
				$('#activityModal').find('div.col-sm-12:not(".allocation-box")').addClass('hide');
				$('#activityModal').find('div.col-sm-4:not(".allocation-box")').addClass('hide');
				$('#activityModal').find('div.col-md-3:not(".allocation-box")').addClass('hide');
				// $('#activityModal').find('div.allocation-box').removeClass('col-md-3').addClass('col-sm-12 allocation-box');
			}
		} else {
			window.IS_ALLOCATION_MODAL = false;
			if (levelId != null) {
				$('#allocationModal').find('div.col-sm-12:not(".allocation-box")').removeClass('hide');
				$('#allocationModal').find('div.col-md-3:not(".allocation-box")').removeClass('hide');
				if (levelId == CHAPTER) {
					$('#allocationModal').find('div.chapter-extra-info').removeClass('hide');
				}
				if (levelId == VIDEOCLIP) {
					$('#allocationModal').find('div.video-clip-extra-info').removeClass('hide');
				}
				// $('#allocationModal').find('div.allocation-box').removeClass('col-sm-12').addClass('col-md-3 allocation-box');
			} else {
				$('#activityModal').find('div.col-md-6:not(".allocation-box")').removeClass('hide');
				$('#activityModal').find('div.col-sm-12:not(".allocation-box")').removeClass('hide');
				$('#activityModal').find('div.col-sm-4:not(".allocation-box")').removeClass('hide');
				$('#activityModal').find('div.col-md-3:not(".allocation-box")').removeClass('hide');
				// $('#activityModal').find('div.allocation-box').removeClass('col-sm-12').addClass('col-md-3 allocation-box');
			}
		}
	}

	var treeListRefreshWorkRound = function() {
		if (window.CURRENT_DATA_SET != null) {
			var treeList = $("#tasks").dxTreeList("instance");
			treeList.cellValue(window.CURRENT_KEY, "mid_code", window.CURRENT_DATA_SET['mid_code']);
			treeList.cellValue(window.CURRENT_KEY, "mid_name", window.CURRENT_DATA_SET['mid_name']);
			treeList.cellValue(window.CURRENT_KEY, "owner_mid", window.CURRENT_DATA_SET['owner_mid']);
			treeList.cellValue(window.CURRENT_KEY, "owner_mid", window.CURRENT_DATA_SET['alloc_to']);
			treeList.cellValue(window.CURRENT_KEY, "progress", window.CURRENT_DATA_SET['progress']);
			treeList.cellValue(window.CURRENT_KEY, "start_dt", window.CURRENT_DATA_SET['start_dt']);
			treeList.cellValue(window.CURRENT_KEY, "end_dt", window.CURRENT_DATA_SET['end_dt']);
			// treeList.saveEditData();
			window.CURRENT_DATA_SET = null;
			window.CURRENT_KEY = null;
		}
	}

	return {
		addAllocationEntity,
		editAllocationEntity,
		addAllocationActivityEntity,
		updateLevel,
		updateActivity,
		allocationMidData,
		fetchChapterDurationGrid,
		handlerForAllocationModal,
		treeListRefreshWorkRound
	};
}());