var page_count = 1;

function disableMIDUpdate() {
	if (window.CURRENT_DEPT_ID == '1') {
		$(document).find('#mid_main_code').prop('readonly', true);
		$(document).find('#mid_fix_code').prop('readonly', true);
	}
}

function htmlbodyHeightUpdate( ){
	var height3 = $( window ).height()
	var height1 = $('.nav').height()+50
	height2 = $('.main').height()
	if(height2 > height3){
		$('html').height(Math.max(height1,height3,height2)+10);
		$('body').height(Math.max(height1,height3,height2)+10);
	}
	else
	{
		$('html').height(Math.max(height1,height3,height2));
		$('body').height(Math.max(height1,height3,height2));
	}

}

function ValidateLevelcode(maincodename,main_code_name) {
	if(main_code_name.length!=0){
		if(maincodename=='Syllabus'){
			if(main_code_name.length!=4){
				return false;
			}else{
				return true;
			}
		}
		if(maincodename=='Year'){
			if(main_code_name.length!=2 || $.isNumeric(main_code_name)!=true){
				return false;
			}else{
				return true;
			}
		}
		if(maincodename=='Grade'){
			if(main_code_name.length>2 || $.isNumeric(main_code_name)!=true){
				return false;
			}else{
				return true;
			}
		}
		if(maincodename=='Subject'){
			if(main_code_name.length!=3){
				return false;
			}else{
				return true;
			}
		}
		if(maincodename=='Chapter'){
			if(main_code_name.length>3 || $.isNumeric(main_code_name)!=true){
				return false;
			}else{
				return true;
			}
		}
		if(maincodename=='SubTopic'){
			if(main_code_name.length!=5){
				return false;
			}else{
				return true;
			}
		}
		if(maincodename=='SubTopic' || maincodename=='Video Clip' || maincodename=='Sec Part'){
			if(main_code_name.length!=5){
				return false;
			}else{
				return true;
			}
		}
	}else{
		return false;
	}

}

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
    var CSV = '';    
    

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";
        
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {
            
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);
        
        //append Label row with line break
        CSV += row + '\r\n';
    }
    
    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";
        
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);
        
        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
    //Generate a file name
    //this will remove the blank-spaces from the title and replace it with an underscore
    var fileName = ReportTitle.replace(/ /g,"_");   
    
    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    
    
    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");    
    link.href = uri;
    
    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

// function getAllocationOverview(qstring = {},is_paginate=false){
// 	if(is_paginate){
// 		$(".over_data_loader").LoadingOverlay("show", {
// 		    background  : "rgba(255, 255, 255, 255)",
// 		    maxSize : 60
// 		});
// 	}
// 	$.ajax({
// 	  	url: window.location.href,
// 	  	type: "get",
// 	  	data:qstring,
// 	}).done(function (data, status, jqXHR) {
// 		if(typeof data != "undefined"){
// 			if(is_paginate){
// 				$(".card_overview").last().after(data);
// 			}else{
// 				//Bind data on first ajax call
// 				$(".overview_data").html(data);
// 			}
// 		}else{
// 			swal("Error", message, "error");
// 		}
// 	}).fail(function (jqXHR,status,err) {
// 	  	swal("Error", "Oops! Something went wrong", "error");
// 	}).always(function () {
// 		$(".over_data_loader").LoadingOverlay("hide");
// 	});
// }

	$(document).ready(function () {
		// if (window.ALLOCATION_CURRENT_TAB == 'LoadAllocationOverview') {
		// 	$(window).scroll(function() {
		// 		if($(window).scrollTop() + $(window).height() >= $(document).height()) {
		//             page_count ++;
		//             // $(window).unbind('scroll');
		//             getAllocationOverview({page:page_count},true);
		//         }
		//     });
		// }
		disableMIDUpdate();
		htmlbodyHeightUpdate()
		$( window ).resize(function() {
			htmlbodyHeightUpdate()
		});
		$( window ).scroll(function() {
			height2 = $('.main').height()
  			htmlbodyHeightUpdate()
		});

	});

	$('.initalizedt').datepicker().datepicker("setDate", new Date());
	$(document).ready(function () {
		 //$("#togglenavclose").hide();
		$("#togglenavopen").click(function(){
				//console.log( $(".nav").find('.sidebar'))
			  $(".sidebar").css("margin-left", "-145px");
			  $(".main").css("margin-left", "50px");
			  $(this).addClass("hide");
			  $("#togglenavclose").removeClass("hide");
		})
		$("#togglenavclose").click(function(){
				//console.log( $(".nav").find('.sidebar'))
			  $(".sidebar").css("margin-left", "0px");
			  $(".main").css("margin-left", "200px");
			  $("#togglenavclose").addClass("hide");
			  $("#togglenavopen").removeClass("hide");
		})

		$(document).on("click", ".addallocationtab", function(e){
		const CHAPTER = '5';
		const VIDEOCLIP = '7';


		$("#AllocationModal").modal('show');

		if($(this).data('btn')=='edit'){
			var edit_id=$(this).data('edit_id');
			$('.alloctionsave').hide();
			$('.alloctionupdate').show();
			$.ajax({
       		    url:base_url+'index.php/allocation/EditAllocationOverview',
       		    type:"post",
                data:{'editid':edit_id},
                success: function(data){
                	var midcod = '';
                	var codeslpite=$(".mid_fix_code").val();
                	var result=JSON.parse(data);

           			if(result[0].level_id_fk==2){
           				midcod=result[0].mid_code.split(codeslpite)[0];
           			}else if(result[0].level_id_fk==1){
           				midcod=result[0].mid_code;

           			}
           			else{
           				midcod=result[0].mid_code.split(codeslpite)[1];

           			}
                	if(result){
                		$("#mid_main_code").val(midcod);
                		/*if($('#sel_dept_1').val()==1){
                			$("#mid_main_code").attr('readonly','readonly');
                		}*/
                		$("#hier_desc").val(result[0].hier_desc);
                		$(".mid_name").val(result[0].mid_name);
                		$('#start_dt').val((result[0].start_dt !== ""  && result[0].start_dt !== null) ? moment(result[0].start_dt).format("DD-MM-YYYY") : '');
                		$('#end_dt').val((result[0].end_dt !== ""  && result[0].end_dt !== null) ? moment(result[0].end_dt).format("DD-MM-YYYY") : '');
						$('#est_end_dt').val((result[0].est_end_dt !== ""  && result[0].est_end_dt !== null) ? moment(result[0].est_end_dt).format("DD-MM-YYYY") : '');

						//console.log(result[0]);
						if(result[0].owner_mid)
						{
						if (result[0].owner_mid.indexOf(',') > -1)
						{
							$('#owner_mid').selectpicker('val',result[0].owner_mid.split(","));
						}else{
						$('#owner_mid').selectpicker('val',result[0].owner_mid);
						}
						}
						$('.selectpicker').selectpicker('refresh');
                		$("#status_nm").val(result[0].status_nm);
                		$('#status_nm option[value='+result[0].status_nm+']').attr('selected','selected');

                		$("#hierarchyid").val(result[0].hierarchy_id);

						// Extra info when chapters
						if (result[0].level_id_fk == CHAPTER) {
							$('#presenter').selectpicker('val',result[0].presenter.split(","));
							$('#storyboard_incharge').selectpicker('val',result[0].storyboard_incharge.split(","));
							$('#sign_off').selectpicker('val',result[0].sign_off.split(","));
							$("#mandays").val(result[0].mandays);

							// RENDER CHAPTER DURATION GRID
							BYJU.allocationDetails.fetchChapterDurationGrid(
								result[0].hierarchy_id
							);
						}

						// Extra info when video clip
						if (result[0].level_id_fk == VIDEOCLIP) {
							$('#storyboarded_by').selectpicker('val',result[0].storyboarded_by.split(","));
							$("#video_clip_type").val(result[0].video_clip_type);
							$("#duration").val(result[0].duration);
						}
                	}
        		}
		    })
		}else{
			$('#alloctionmodal')[0].reset();
			$(".chapter-duration-grid-box").html("");
			$(".selectpicker").selectpicker("refresh");
			// $('.initalizedt').datepicker({format: 'dd-mm-yyyy'}).datepicker("setDate", new Date());
			$('.alloctionsave').show();
			$('.alloctionupdate').hide();
		}

	})




		$(document).on("click", ".loadboxurl", function(e){
			e.preventDefault();
			var level_name=$(this).data("level_name");
			var dept_id=$("#sel_dept_1").val();
			var level_id=$(this).data("level_id");
		 	var hierarchy_id=$(this).data("hierarchy_id");


			if(typeof level_name != "undefined" && typeof dept_id != "undefined" && typeof level_id != "undefined" && hierarchy_id != "undefined"){
		 		window.location.href=base_url+'index.php/allocation/LoadAllocationOverview/'+level_name+'/'+dept_id+'/'
				+level_id+'/'+hierarchy_id;
		 	}
		})

		$('body').on('change','#sel_dept_1', function() {
				var dept_id=$(this).val();
  				window.location.href=base_url+'index.php/allocation/'+ $(this).data('tab-name') +'/syllabus/'+dept_id+'/1'
		});


		$(".alloctionsave").click(function(e){
		   	e.preventDefault();
		   	 $err='';

		   	 var dept_id=$("#sel_dept_1").val();
		   	 var levelcodename=$(".levelcodename").text().split(':')[0];
		   	 var main_code_name	=$("#mid_main_code").val();
		   		if(dept_id==''){
		   			$err+="Select dept id\n";
		   		}
		   		 if(ValidateLevelcode(levelcodename,main_code_name)==false){
		   		 	$err+="Enter Valid main code\n";
		   		 }
				 console.log("Archito Testing here now");
				 console.log($("#alloctionmodal").serialize());
				 // return false;

		   		if($err==''){
		   			var btn = $(this);
					btn.attr("disabled", true);
						var dat =$("#alloctionmodal").serialize();

						$.ajax({
							url: base_url+'index.php/allocation/SaveAllocationOverview',
							type: 'post',
							dataType: 'json',
							data: {'data':dat,dept_id:dept_id},
						})
						.done(function(response) {
							if(typeof response != "undefined"){
								var message = typeof response.message != "undefined" ? response.message : "Something went wrong.";
								if(response.status){
									swal("Success!",message,"success")
									.then((value) => {
									  window.location.reload();
									});
								}else{
									swal("Error!",message,"error");
								}
							}
						})
						.fail(function() {
							swal("Error!","Something went wrong.",'error');
						})
						.always(function() {
							btn.attr("disabled", false);
						});
						
				}else{
			    	$err1="Please correct the following errors:-\n";
			    	$err1+=$err;
			    	swal("Error!",$err1,'error');
			    	return false;
			    }
		   });

			$(".alloctionupdate").click(function(e){
		   	e.preventDefault();
		   	 $err='';
		   	 var dept_id=$("#sel_dept_1").val();
		 	 var levelcodename=$(".levelcodename").text().split(':')[0];
		   	 var main_code_name	=$("#mid_main_code").val();
		   		if(dept_id==''){
		   			$err+="Select dept id\n";
		   		}
		   		if(ValidateLevelcode(levelcodename,main_code_name)==false){
		   		 	$err+="Enter Valid main code\n";
		   		 }

		   		if($err==''){
		   			var btn = $(this);
		   			var dat =$("#alloctionmodal").serialize();

		   			btn.attr("disabled", true);
		   			$.ajax({
		   				url:base_url+'index.php/allocation/UpdateAllocationOverview',
		   				type: 'post',
		   				dataType: 'json',
		   				data: {'data':dat,dept_id:dept_id},
		   			})
		   			.done(function(response) {
		   				if(typeof response != "undefined"){
							var message = typeof response.message != "undefined" ? response.message : "Something went wrong.";
							if(response.status){
								swal("Success!",message,"success")
								.then((value) => {
								  window.location.reload();
								});
							}else{
								swal("Error!",message,"error");
							}
						}
		   			})
		   			.fail(function() {
		   				swal("Error!","Something went wrong.",'error');
		   			})
		   			.always(function() {
		   				btn.attr("disabled", false);
		   			});
		   			
			    }else{
			    	$err1="Please correct the following errors:-\n";
			    	$err1+=$err;
			    	swal("Error!",$err1,'error');
			    	return false;
			    }
		   });

// for navigation
		var currentTabName = (window.ALLOCATION_CURRENT_TAB !== '') ? window.ALLOCATION_CURRENT_TAB : 'LoadAllocationOverview';
		$(".selectednav").click(function(){
			var dept_id=$("#sel_dept_1").val();
			var levelname=$(this).data('levelname');
			var levelid=$(this).data('levelid');
			var parentid=$(this).data('parentid');

			if(typeof levelname != "undefined" && typeof dept_id != "undefined" && typeof levelid != "undefined" && parentid != "undefined"){
			 	if(levelid==1){
					window.location.href=base_url+'index.php/allocation/'+currentTabName+'/'+levelname+'/'+dept_id+'/'+levelid;
		               }else{
						window.location.href=base_url+'index.php/allocation/'+currentTabName+'/'+levelname+'/'+dept_id+'/'+levelid+'/'+parentid;
						/*$.ajax({
				       		 url:base_url+'index.php/allocation/getparentid',
				       		  type:"post",
				             data:{dept_id:dept_id,levelid:levelid,parentid:parentid},
				                success: function(data){
				                	//console.log(data)
									if(data)
									{
				                	var result=JSON.parse(data);
				                	var pid=result[0]['hierarchy_id'];
									if(levelid==1){
				                				pid='';
							window.location.href=base_url+'index.php/allocation/LoadAllocationOverview/'+levelname+'/'+dept_id+'/'+levelid+'/'+pid
				                			}else{
					                			if(pid==null){
						                			pid='';
						                		}else{
						                			pid=result[0]['hierarchy_id'];
						                		}
				                			}
									}else{
									pid='';
									}
				               window.location.href=base_url+'index.php/allocation/LoadByNavigation/'+levelname+'/'+dept_id+'/'+levelid+'/'+pid

				        	}
						        });*/
				}
			}else{
				window.location.reload();
			}

		});


		$(".drillup").click(function(e){
			e.preventDefault();
			var dept_id=$("#sel_dept_1").val();
			var levelname=$(this).data('levelname');
			var levelid=$(this).data('levelid');
			var parentid=$(this).data('parentid');

				if(typeof levelname != "undefined" && typeof dept_id != "undefined" && typeof levelid != "undefined" && parentid != "undefined"){
					if(levelid==1){
						window.location.href=base_url+'index.php/allocation/'+currentTabName+'/'+levelname+'/'+dept_id+'/'+levelid;
					}else{
						$.ajax({
							url:base_url+'index.php/allocation/getparentid',
							type:"post",
							data:{dept_id:dept_id,levelid:levelid,parentid:parentid},
							success: function(data){
								console.log(data);
								if(typeof data != "undefined" && data){
									var result=JSON.parse(data);
									var pid=result[0]['p_hierarchy_id'];
									window.location.href=base_url+'index.php/allocation/'+currentTabName+'/'+levelname+'/'+dept_id+'/'+levelid+'/'+pid;
								}else{
									window.location.href=base_url+'index.php/allocation/'+currentTabName+'/'+levelname+'/'+dept_id+'/'+levelid;
								}
							}
						});
					}
				}else{
					window.location.reload();
				}
		});
    $('.select2').select2();


$(document).on("submit","#frm_mid_overview",function(e){
	e.preventDefault()
	var s_value= $("#mid_code_search").val();
	var dept = $("#sel_dept_1").val();
	if(s_value !== ""){
		$.ajax({
		  	url: base_url+'index.php/allocation/getMidOverview',
		  	type: "post",
		  	dataType : "json",
		  	data:{mid:s_value,dept_id:dept,s_value:s_value},
		}).done(function (res, status, jqXHR) {
			if(typeof res != "undefined" && res.status){
				var url = $(location).attr('href'),
				 parts = url.split("/"),
				 allocationFunc = parts[6];
				var redirect_url=`${base_url}index.php/allocation/${allocationFunc}/${res.data.level}/${res.data.dept}/${res.data.level_id}/${res.data.hier_id}?mid=${s_value}`
				if(typeof redirect_url !== "undefined"){
					window.location.href = redirect_url;
				}
			}else{
				swal("Error","No record found.", "error");
			}
		}).fail(function (jqXHR,status,err) {
		  	swal("Error", "Oops! Something went wrong", "error");
		}).always(function () {
		});
	}else{
		$(".selectednav").trigger("click");
	}
});
/*$('#mid_code_search').focus(function() {
	        $(this).autocomplete({
	      source: function( request, response ){
	     	var level_id=$('#level_id_fk').val();
	     	var p_hierarchy_id=$('#p_hierarchy_id').val();
	     	var dept_id=$("#sel_dept_1").val();
		     	$(".midsearchcode").each(function() {
			   		if($(this).data('midcode').toLowerCase().includes(request.term.toLowerCase())){
			   			$("."+$(this).data('midcode')).css('display','block')
			   		}else{
			   			$("."+$(this).data('midcode')).css('display','none')
			   		}
			   		if(request.term==''){
			   			$("."+$(this).data('midcode')).css('display','block')
			   		}
				});
	      },
	       minLength: 0,

	  });
    });*/

	/*Update level attributes start*/

	$(document).on("click",".btn_up_allocation",function(e){
		e.preventDefault();
		$("#frm_allocation").trigger("reset");
		$(".file_errors").html("");
		$("#department").selectpicker("refresh");
		$("#csv_upload_allocation").modal("show");
	});

	//Upload csv
	$(document).on("submit",'#frm_allocation',function(e){
		e.preventDefault();
		$(".file_errors").html("");
		var btn = $("#btn_lvl_csv") ,file_data = $('#level_file').prop('files')[0] ,fileExtension;
		if(typeof file_data === "undefined"){
			swal("Oops!", "Please select file.", "error");
			return false;
		}
		fileExtension = file_data.name.split('.').pop();
		if(fileExtension !== "csv"){
			swal("Oops!", "Only csv file allowed.", "error");
			return false;
		}
		var frmData = new FormData(this);

		btn.attr('disabled',true);
		btn.val('Please wait..');
		$.ajax({
		  	url: base_url+'index.php/allocation/updateLevelCSV',
		  	type: "post",
		  	data:frmData,
		  	dataType : 'json',
		  	contentType: false,
         	cache: false,
   			processData:false,
		}).done(function (data, status, jqXHR) {
			var message = (typeof data.message !== "undefined") ? data.message : "Something went wrong.";
			if(typeof data != "undefined" && data.status){
				var csvErrors = (typeof data.errors != "undefined") ?  data.errors : [];
				var alertType = (csvErrors.length > 0) ? "info" : "success";
				if(csvErrors.length > 0){
					JSONToCSVConvertor(csvErrors,'Errors_Attributes_Update',true);
				}
				swal("", message, alertType)
				.then((value) => {
				  window.location.reload();
				});
			}else{
				swal("Error", message, "error");
			}

		}).fail(function (jqXHR,status,err) {
		  	swal("Error", "Oops! Something went wrong", "error");

		}).always(function () {
		 	btn.attr('disabled',false);
			btn.val('Upload');
		});

	});
	/*Update level attributes end*/

	/*Allocate activity start*/
	$(document).on("click",'.btn_alct_act',function(e){
		e.preventDefault();
		$("#frm_act_allocate").trigger("reset");
		$(".file_errors").html("");
		$("#dept_allocation").selectpicker("refresh");
		$("#mdl_activity_allocate").modal("show");
	});

	$(document).on("submit","#frm_act_allocate",function(e){
		e.preventDefault();
		$(".file_errors").html("");
		var btn = $("#btn_act_upload") ,file_data = $('#activity_file').prop('files')[0] ,fileExtension;
		if(typeof file_data === "undefined"){
			swal("Oops!", "Please select file.", "error");
			return false;
		}
		fileExtension = file_data.name.split('.').pop();
		if(fileExtension !== "csv"){
			swal("Oops!", "Only csv file allowed.", "error");
			return false;
		}
		var frmData = new FormData(this);

		btn.attr('disabled',true);
		btn.val('Please wait..');
		$.ajax({
		  	url: base_url+'index.php/allocation/activitiesAllocate',
		  	type: "post",
		  	data:frmData,
		  	dataType: "json",
		  	contentType: false,
         	cache: false,
   			processData:false,
		}).done(function (data, status, jqXHR) {
			var message = (typeof data.message !== "undefined") ? data.message : "Something wen't wrong.";

			if(typeof data != "undefined" && data.status){
				var csvErrors = (typeof data.errors != "undefined") ?  data.errors : [];
				var alertType = (csvErrors.length > 0) ? "info" : "success";

				if(csvErrors.length > 0){
					JSONToCSVConvertor(csvErrors,'Errors_Activity_Update',true);
				}
				swal("", message, alertType)
				.then((value) => {
				  window.location.reload();
				});
			}else{
				swal("Error", message, "error");
			}
		}).fail(function (jqXHR,status,err) {
		  	swal("Error", "Oops! Something went wrong", "error");

		}).always(function () {
		 	btn.attr('disabled',false);
			btn.val('Upload');
		});
	});

	/*Allocate activity end*/

    $("body").on('mouseover', '.popuppattern', function(e) {
             $(this).popover('show');
    });
   $("body").on('mouseleave', '.popuppattern', function(e) {
             $(this).popover('hide');
    });
});