var BYJU = BYJU || {};
window.feedbackGivenUserList = [];

BYJU.userFeedback = (function (){
    
    
    $(document).ready(function() {
		
		var isPreviousWeek = getWeekNumber(new Date(getQueryStringValue('date_view')))[1] == getWeekNumber(new Date())[1] - 1 ? true : false;
        // Hiding checkbox if not previous week of current week
        // if (!isPreviousWeek) {
            // setTimeout(function (){
                // $('.bs-checkbox').hide();
				////$('#table').find('th:nth-child(1), tr td:nth-child(1)').hide();
            // }, 500);
        // }
		
        $('#gradesModal').on('shown.bs.modal', function() {
			getFeedbackGradesData();
		});
    });

    var deleteGrade = function(id) {
        if (confirm("Do you want to delete this")){
            $.ajax({
                url: base_url + 'index.php/User/deleteGrade',
                type: 'POST',
                data: {id : id},
                success: function(response) {
                    getFeedbackGradesData();
                }
            });
        }
    }

    var updateGrade = function(id) {
        $('input[name="grade"]').val($('.grade-list-table').find('tr[data-id="'+id+'"]').find('td:nth-child(1)').text());
        $('input[name="value"]').val($('.grade-list-table').find('tr[data-id="'+id+'"]').find('td:nth-child(2)').text());
        //console.log($('.grade-list-table').find('tr[data-id="'+id+'"]').find('td:nth-child(3)').text());
        // if ($('.grade-list-table').find('tr[data-id="'+id+'"]').find('td:nth-child(3)').text() == '1') {
            // $('input[name="active"]').prop('checked',true);        
        // } else {
            // $('input[name="active"]').prop('checked',false);        
        // }
        $('.store-grade-form').attr('update-grade', id);
        $('.grade-btn').text('Update');
    }

    var renderGradesTable = function (gradesData) {
        var table = '<table class="table grade-list-table"><thead><tr><th>Grade</th><th>Value</th><th>Status</th><th></th><th></th><tr></thead><tbody>'
        gradesData.forEach(function(value, index, object) {
            table += '<tr data-id="'+value.id+'"><td>'+value.grade+'</td><td>'+value.value+'</td><td>'+value.status_nm+'</td><td><i class="glyphicon glyphicon-remove hide" onclick="BYJU.userFeedback.deleteGrade('+value.id+')"></i></td><td><i class="fa fa-pencil" onclick="BYJU.userFeedback.updateGrade('+value.id+')"></i></td><tr>';
        });
        table += '</tbody></table>';
        $('.weekly-feedback-panel').empty();
        $('.weekly-feedback-panel').append(table);
    }

    

    var getFeedbackGradesData = function () {
        $.ajax({
            url: base_url +'index.php/User/getFeedbackGradesData',
            type: 'GET',
            success: function(response) {
                renderGradesTable(response.gradesData);
            }
        });
    }

    var showFeedbackModal = function(permission) {
        var isProjectLevel = getQueryStringValue('lvl') == '1' ? true : false;
       // var isCurrentWeek = getWeekNumber(new Date(getQueryStringValue('date_view')))[1] == getWeekNumber(new Date())[1] ? true : false;

		 var isPreviousWeek = getWeekNumber(new Date(getQueryStringValue('date_view')))[1] == getWeekNumber(new Date())[1] - 1 ? true : false;

        // if (permission && isProjectLevel && isPreviousWeek) {
            // //$('.feedback-button').removeClass('hide');
        // }
    }

    var hideFeedbackModal = function() {
       //  $('.feedback-button').addClass('hide');
    }

    var storeFeedback = function(formId) {
		//console.log($(this));
		var t_dis_but=$("body").find(".f_disb");
		//console.log(t_dis_but);
		t_dis_but.attr("disabled", true);
        var allFormData = [];
        var blankInput = true;
        $('.' + formId).each(function(i, el) {
            if ($(el).serializeArray()[0].value == "") {
                alert ("Please choose Grades for all selected members");
				//$(this).attr("disabled", false);
				t_dis_but.attr("disabled", false);
                blankInput = true;
                return false
            }else{
				 blankInput = false;
			}
			
            allFormData.push({
                'user_id_fk': $(el).data('id'), 
                'project_id_fk': getQueryStringValue('pro'), 
                'grade_id_fk': $(el).serializeArray()[0].value, 
				'fb_given_date': getQueryStringValue('date_view'),
                'remarks': $(el).serializeArray()[1].value,
                'feedback_already_given': $(el).data('feedback-given')
            });
        })
       if (!blankInput) {
            $.ajax({
                url: base_url + 'index.php/User/upsertFeedback',
                type: 'POST',
                data: {formdata : allFormData},
                success: function(response) {
                    //console.log("Archito Testing");
                    location.reload();
                }
            });
       }else{
		  t_dis_but.attr("disabled", false);
		   //$(this).attr("disabled", false);
	   }
    }

    var storeGrade = function(formId) {
        var formData = $('.' + formId).serializeArray();
        var pDate = new Date();
        var activeGrade = 1;        
        var pDateSeconds = pDate.valueOf()/1000;
        
        if (formData[2] == undefined) {
            activeGrade = 0;            
        }
        if (formData[0].value == '') {
            alert('Please input Grade');
            return false;      
        }
        if (formData[1].value == '') {
            alert('Please input Value');
            return false;      
        }


        var allFormData = {
            'user': $('.' + formId).data('user-id'),
            'grade': formData[0].value,
            'value': formData[1].value,
            'status': activeGrade,
            'update_id': $('.' + formId).attr('update-grade')
        };

        $.ajax({
            url: base_url + 'index.php/User/upsertGrade',
            type: 'POST',
            data: {formdata : allFormData},
            success: function(response) {
               // console.log(response);
                getFeedbackGradesData();
                $('.store-grade-form').removeAttr('update-grade');
                $('.grade-btn').text('Add New');
                $('input').val('');
               // $('input[name="active"]').prop('checked', false);
			   getActiveFeedbackGradesData();
            }
        });
    }

    var getWeeklyProjectFeedback = function () {
        var projectId = getQueryStringValue('pro');
        var users = getKeyValueFromArrayOfObject(window.usersToGiveFeedback, 'user_id_fk');
        $.ajax({
            url: base_url + 'index.php/User/getUsersWeeklyProjectFeedback',
            type: 'GET',
            data: {projectId : projectId, users : users,dateView: getQueryStringValue('date_view')},
            success: function(response) {
                window.weeklyProjectFeedbackData = response.weeklyProjectFeedback;
                window.feedbackGivenUserList = getKeyValueFromArrayOfObject (response.weeklyProjectFeedback, 'user_id_fk');
            }
        });
    }

    var getFeedbackByUserId = function (userId) {
        var feedbackData = [];
        $.each( window.weeklyProjectFeedbackData, function (index, value) {
            if (value.user_id_fk == userId) {
                feedbackData = value;
                return false;
            }
        });
        return feedbackData;
    }
    
    /**
     * @description Get the all specific key values from the array of object
     * @param {*} array 
     * @param {*} key 
     */
    var getKeyValueFromArrayOfObject = function (array, key) {
        var valueList = array.map(function(value) {
            return value[key];
        });
        return valueList;
    }

    var createFeedbackForm = function () {
        $('.feedback-panel').empty();
        // Here make an ajax call to get the feeback data of selected users
        getWeeklyProjectFeedback();
        //$('.feedback-panel').append('<i class="fa fa-refresh fa-spin fa-3x fa-fw weekly-spinner"></i>');
        setTimeout(function () {
            $.each(window.usersToGiveFeedback, function (index, value) {
                // if weekly feedback already given set the feebbackGiven flage to true
                if (window.feedbackGivenUserList.indexOf(value.user_id_fk) > -1) {
                    $('.feedback-panel').append(getSingleFeedbackForm(value, true));
                    // Set the already inserted value
                    var feedbackFormData = getFeedbackByUserId(value.user_id_fk); 
                   // console.log(feedbackFormData);
                    $('#store-feedback-form-' + value.user_id_fk).find('select').val(feedbackFormData.grade_id_fk);
                    $('#store-feedback-form-' + value.user_id_fk).find('textarea').val(feedbackFormData.remarks);
                } else {
                    $('.feedback-panel').append(getSingleFeedbackForm(value));
                }
            });
            $('.weekly-spinner').remove();
        }, 1000);    
    }

    var getSingleFeedbackForm = function (user, feedbackGiven) {
        feedbackGiven = feedbackGiven || false;
        var panelClass = (feedbackGiven == true) ? 'panel-success' : 'panel-default';
        var feedbackGivenLabel = (feedbackGiven == true) ? '<small class="pull-right"><b>Feedback Given</b></small>' : '';
        return '<form class="store-feedback-form" id="store-feedback-form-'+user.user_id_fk+'" data-id="'+user.user_id_fk+'" data-feedback-given="'+feedbackGiven+'">'+
        '<div class="panel '+panelClass+'">'+
        '<div class="panel-heading">'+
          user.full_name+ feedbackGivenLabel +
        '</div>'+
        '<div class="panel-body">'+
        '<div class="col-sm-4 form-group">'+
            '<label>Grade</label><br>'+ window.gradeOptions +
        '</div>'+
        '<div class="col-sm-8 form-group">'+
            '<label>Remarks</label>'+
            '<textarea cols="60" class="form-control" style="width:100%;" name="remarks"></textarea>'+
        '</div>'+
        '</div>'+
        '</div>'+        
      '</form>';
    }
	var getActiveFeedbackGradesData = function () {
        $.ajax({
            url: base_url + 'index.php/User/getActiveFeedbackGradesData',
            type: 'GET',
            success: function(response) {
                //console.log(response);
                window.feedbackGradeOptions = response.gradesData;
                window.gradeOptions = '<select name="grade" class="form-control" style="width:100%;"><option value="">Select Grade</option>';
                $.each(window.feedbackGradeOptions, function(index, value) {
                    window.gradeOptions += '<option value="'+value.id+'">' +value.value+ '</option>';
                });
                window.gradeOptions += '</select>';
            }
        });
    }

    return {
        showFeedbackModal,
        hideFeedbackModal,
        storeFeedback,
        createFeedbackForm,
        storeGrade,
        deleteGrade,
        updateGrade,
		getActiveFeedbackGradesData
    };

}());