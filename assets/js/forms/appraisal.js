$(document).ready(function() {

    $("body").on("click", ".md_t_h",function(){
    window.location = base_url+"index.php/User/home_page";
    });

   
    $("body").on("click", ".form_select",function(e){
        e.preventDefault()
        var t=$(this).attr('hrf');
        if (t == "Appraisal_response" || t == "self_response") {
            window.location = base_url+"index.php/Form/"+t;
        } else {
            window.location = base_url+"index.php/Form/load_view_f?a="+t;
        }
    });
	
	$("body").on("change", "#sel_slot_1",function(e){
      
	  if($(this).val())
	  {
        var t=$(this).val();
		var ur=window.location.href;
		ur = removeParam('us', ur);
		ur = removeParam('slt_id', ur);
		window.location =ur+"&slt_id="+t;
	  }
    });
	
	function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}
	
});