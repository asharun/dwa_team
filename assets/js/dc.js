/*!
 *  dc 3.0.9
 *  http://dc-js.github.io/dc.js/
 *  Copyright 2012-2016 Nick Zhu & the dc.js Developers
 *  https://github.com/dc-js/dc.js/blob/master/AUTHORS
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

! function () {
	function t(t, e) {
		"use strict";
		var n = {
			version: "3.0.9",
			constants: {
				CHART_CLASS: "dc-chart",
				DEBUG_GROUP_CLASS: "debug",
				STACK_CLASS: "stack",
				DESELECTED_CLASS: "deselected",
				SELECTED_CLASS: "selected",
				NODE_INDEX_NAME: "__index__",
				GROUP_INDEX_NAME: "__group_index__",
				DEFAULT_CHART_GROUP: "__default_chart_group__",
				EVENT_DELAY: 40,
				NEGLIGIBLE_NUMBER: 1e-10
			},
			_renderlet: null
		};
		n.chartRegistry = function () {
			function t(t) {
				return t || (t = n.constants.DEFAULT_CHART_GROUP), e[t] || (e[t] = []), t
			}
			var e = {};
			return {
				has: function (t) {
					for (var n in e)
						if (e[n].indexOf(t) >= 0) return !0;
					return !1
				},
				register: function (n, r) {
					r = t(r), e[r].push(n)
				},
				deregister: function (n, r) {
					r = t(r);
					for (var i = 0; i < e[r].length; i++)
						if (e[r][i].anchorName() === n.anchorName()) {
							e[r].splice(i, 1);
							break
						}
				},
				clear: function (t) {
					t ? delete e[t] : e = {}
				},
				list: function (n) {
					return n = t(n), e[n]
				}
			}
		}(), n.registerChart = function (t, e) {
			n.chartRegistry.register(t, e)
		}, n.deregisterChart = function (t, e) {
			n.chartRegistry.deregister(t, e)
		}, n.hasChart = function (t) {
			return n.chartRegistry.has(t)
		}, n.deregisterAllCharts = function (t) {
			n.chartRegistry.clear(t)
		}, n.filterAll = function (t) {
			for (var e = n.chartRegistry.list(t), r = 0; r < e.length; ++r) e[r].filterAll()
		}, n.refocusAll = function (t) {
			for (var e = n.chartRegistry.list(t), r = 0; r < e.length; ++r) e[r].focus && e[r].focus()
		}, n.renderAll = function (t) {
			for (var e = n.chartRegistry.list(t), r = 0; r < e.length; ++r) e[r].render();
			null !== n._renderlet && n._renderlet(t)
		}, n.redrawAll = function (t) {
			for (var e = n.chartRegistry.list(t), r = 0; r < e.length; ++r) e[r].redraw();
			null !== n._renderlet && n._renderlet(t)
		}, n.disableTransitions = !1, n.transition = function (t, e, r, i) {
			if (n.disableTransitions || e <= 0) return t;
			var a = t.transition(i);
			return (e >= 0 || void 0 !== e) && (a = a.duration(e)), (r >= 0 || void 0 !== r) && (a = a.delay(r)), a
		}, n.optionalTransition = function (t, e, r, i) {
			return t ? function (t) {
				return n.transition(t, e, r, i)
			} : function (t) {
				return t
			}
		}, n.afterTransition = function (t, e) {
			if (t.empty() || !t.duration) e.call(t);
			else {
				var n = 0;
				t.each(function () {
					++n
				}).on("end", function () {
					--n || e.call(t)
				})
			}
		}, n.units = {}, n.units.integers = function (t, e) {
			return Math.abs(e - t)
		}, n.units.ordinal = function () {
			throw new Error("dc.units.ordinal should not be called - it is a placeholder")
		}, n.units.fp = {}, n.units.fp.precision = function (t) {
			var e = function (t, r) {
				var i = Math.abs((r - t) / e.resolution);
				return n.utils.isNegligible(i - Math.floor(i)) ? Math.floor(i) : Math.ceil(i)
			};
			return e.resolution = t, e
		}, n.round = {}, n.round.floor = function (t) {
			return Math.floor(t)
		}, n.round.ceil = function (t) {
			return Math.ceil(t)
		}, n.round.round = function (t) {
			return Math.round(t)
		}, n.override = function (t, e, n) {
			var r = t[e];
			t["_" + e] = r, t[e] = n
		}, n.renderlet = function (t) {
			return arguments.length ? (n._renderlet = t, n) : n._renderlet
		}, n.instanceOfChart = function (t) {
			return t instanceof Object && t.__dcFlag__ && !0
		}, n.errors = {}, n.errors.Exception = function (t) {
			var e = t || "Unexpected internal error";
			this.message = e, this.toString = function () {
				return e
			}, this.stack = (new Error).stack
		}, n.errors.Exception.prototype = Object.create(Error.prototype), n.errors.Exception.prototype.constructor = n.errors.Exception, n.errors.InvalidStateException = function () {
			n.errors.Exception.apply(this, arguments)
		}, n.errors.InvalidStateException.prototype = Object.create(n.errors.Exception.prototype), n.errors.InvalidStateException.prototype.constructor = n.errors.InvalidStateException, n.errors.BadArgumentException = function () {
			n.errors.Exception.apply(this, arguments)
		}, n.errors.BadArgumentException.prototype = Object.create(n.errors.Exception.prototype), n.errors.BadArgumentException.prototype.constructor = n.errors.BadArgumentException, n.dateFormat = t.timeFormat("%m/%d/%Y"), n.printers = {}, n.printers.filters = function (t) {
			for (var e = "", r = 0; r < t.length; ++r) r > 0 && (e += ", "), e += n.printers.filter(t[r]);
			return e
		}, n.printers.filter = function (t) {
			var e = "";
			return void 0 !== t && null !== t && (t instanceof Array ? t.length >= 2 ? e = "[" + t.map(function (t) {
				return n.utils.printSingleValue(t)
			}).join(" -> ") + "]" : t.length >= 1 && (e = n.utils.printSingleValue(t[0])) : e = n.utils.printSingleValue(t)), e
		}, n.pluck = function (t, e) {
			return e ? function (n, r) {
				return e.call(n, n[t], r)
			} : function (e) {
				return e[t]
			}
		}, n.utils = {}, n.utils.printSingleValue = function (t) {
			var e = "" + t;
			return t instanceof Date ? e = n.dateFormat(t) : "string" == typeof t ? e = t : n.utils.isFloat(t) ? e = n.utils.printSingleValue.fformat(t) : n.utils.isInteger(t) && (e = Math.round(t)), e
		}, n.utils.printSingleValue.fformat = t.format(".2f"), n.utils.toTimeFunc = function (t) {
			return "time" + t.charAt(0).toUpperCase() + t.slice(1)
		}, n.utils.add = function (e, r, i) {
			if ("string" == typeof r && (r = r.replace("%", "")), e instanceof Date) return "string" == typeof r && (r = +r), "millis" === i ? new Date(e.getTime() + r) : ("function" != typeof (i = i || t.timeDay) && (i = t[n.utils.toTimeFunc(i)]), i.offset(e, r));
			if ("string" == typeof r) {
				var a = +r / 100;
				return e > 0 ? e * (1 + a) : e * (1 - a)
			}
			return e + r
		}, n.utils.subtract = function (e, r, i) {
			if ("string" == typeof r && (r = r.replace("%", "")), e instanceof Date) return "string" == typeof r && (r = +r), "millis" === i ? new Date(e.getTime() - r) : ("function" != typeof (i = i || t.timeDay) && (i = t[n.utils.toTimeFunc(i)]), i.offset(e, -r));
			if ("string" == typeof r) {
				var a = +r / 100;
				return e < 0 ? e * (1 + a) : e * (1 - a)
			}
			return e - r
		}, n.utils.isNumber = function (t) {
			return t === +t
		}, n.utils.isFloat = function (t) {
			return t === +t && t !== (0 | t)
		}, n.utils.isInteger = function (t) {
			return t === +t && t === (0 | t)
		}, n.utils.isNegligible = function (t) {
			return !n.utils.isNumber(t) || t < n.constants.NEGLIGIBLE_NUMBER && t > -n.constants.NEGLIGIBLE_NUMBER
		}, n.utils.clamp = function (t, e, n) {
			return t < e ? e : t > n ? n : t
		}, n.utils.constant = function (t) {
			return function () {
				return t
			}
		};
		var r = 0;
		return n.utils.uniqueId = function () {
				return ++r
			}, n.utils.nameToId = function (t) {
				return t.toLowerCase().replace(/[\s]/g, "_").replace(/[\.']/g, "")
			}, n.utils.appendOrSelect = function (t, e, n) {
				n = n || e;
				var r = t.select(e);
				return r.empty() && (r = t.append(n)), r
			}, n.utils.safeNumber = function (t) {
				return n.utils.isNumber(+t) ? +t : 0
			}, n.utils.arraysEqual = function (t, e) {
				return !t && !e || !(!t || !e) && (t.length === e.length && t.every(function (t, n) {
					return t.valueOf() === e[n].valueOf()
				}))
			}, n.utils.allChildren = function (t) {
				var e = [];
				if (e.push(t.path), console.log("currentNode", t), t.children)
					for (var r = 0; r < t.children.length; r++) e = e.concat(n.utils.allChildren(t.children[r]));
				return e
			}, n.utils.toHierarchy = function (t, e) {
				for (var n = {
						key: "root",
						children: []
					}, r = 0; r < t.length; r++)
					for (var i = t[r], a = i.key, o = e(i), l = n, u = 0; u < a.length; u++) {
						var c, s = a.slice(0, u + 1),
							d = l.children,
							f = a[u];
						u + 1 < a.length ? (void 0 === (c = function (t, e) {
							for (var n = 0; n < t.length; n++)
								if (t[n].key === e) return t[n]
						}(d, f)) && (c = {
							key: f,
							children: [],
							path: s
						}, d.push(c)), l = c) : (c = {
							key: f,
							value: o,
							data: i,
							path: s
						}, d.push(c))
					}
				return n
			}, n.utils.getAncestors = function (t) {
				for (var e = [], n = t; n.parent;) e.unshift(n.name), n = n.parent;
				return e
			}, n.utils.arraysIdentical = function (t, e) {
				var n = t.length;
				if (n !== e.length) return !1;
				for (; n--;)
					if (t[n] !== e[n]) return !1;
				return !0
			}, n.logger = function () {
				var t = {};
				t.enableDebugLog = !1, t.warn = function (e) {
					return console && (console.warn ? console.warn(e) : console.log && console.log(e)), t
				};
				var e = {};
				return t.warnOnce = function (r) {
					return e[r] || (e[r] = !0, n.logger.warn(r)), t
				}, t.debug = function (e) {
					return t.enableDebugLog && console && (console.debug ? console.debug(e) : console.log && console.log(e)), t
				}, t.deprecate = function (e, n) {
					var r = !1;
					return function () {
						return r || (t.warn(n), r = !0), e.apply(this, arguments)
					}
				}, t
			}(), n.config = function () {
				var t = {},
					e = ["#3182bd", "#6baed6", "#9ecae1", "#c6dbef", "#e6550d", "#fd8d3c", "#fdae6b", "#fdd0a2", "#31a354", "#74c476", "#a1d99b", "#c7e9c0", "#756bb1", "#9e9ac8", "#bcbddc", "#dadaeb", "#636363", "#969696", "#bdbdbd", "#d9d9d9"],
					r = e;
				return t.defaultColors = function (i) {
					return arguments.length ? (r = i, t) : (r === e && n.logger.warnOnce("You are using d3.schemeCategory20c, which has been removed in D3v5. See the explanation at https://github.com/d3/d3/blob/master/CHANGES.md#changes-in-d3-50. DC is using it for backward compatibility, however it will be changed in DCv3.1. You can change it by calling dc.config.defaultColors(newScheme). See https://github.com/d3/d3-scale-chromatic for some alternatives."), r)
				}, t
			}(), n.events = {
				current: null
			}, n.events.trigger = function (t, e) {
				e ? (n.events.current = t, setTimeout(function () {
					t === n.events.current && t()
				}, e)) : t()
			}, n.filters = {}, n.filters.RangedFilter = function (t, e) {
				var n = new Array(t, e);
				return n.isFiltered = function (t) {
					return t >= this[0] && t < this[1]
				}, n.filterType = "RangedFilter", n
			}, n.filters.TwoDimensionalFilter = function (t) {
				if (null === t) return null;
				var e = t;
				return e.isFiltered = function (t) {
					return t.length && t.length === e.length && t[0] === e[0] && t[1] === e[1]
				}, e.filterType = "TwoDimensionalFilter", e
			}, n.filters.RangedTwoDimensionalFilter = function (t) {
				if (null === t) return null;
				var e, n = t;
				return e = n[0] instanceof Array ? [
					[Math.min(t[0][0], t[1][0]), Math.min(t[0][1], t[1][1])],
					[Math.max(t[0][0], t[1][0]), Math.max(t[0][1], t[1][1])]
				] : [
					[t[0], -1 / 0],
					[t[1], 1 / 0]
				], n.isFiltered = function (t) {
					var n, r;
					return t instanceof Array ? (n = t[0], r = t[1]) : (n = t, r = e[0][1]), n >= e[0][0] && n < e[1][0] && r >= e[0][1] && r < e[1][1]
				}, n.filterType = "RangedTwoDimensionalFilter", n
			}, n.filters.HierarchyFilter = function (t) {
				if (null === t) return null;
				var e = t.slice(0);
				return e.isFiltered = function (t) {
					if (!(e.length && t && t.length && t.length >= e.length)) return !1;
					for (var n = 0; n < e.length; n++)
						if (t[n] !== e[n]) return !1;
					return !0
				}, e
			}, n.baseMixin = function (r) {
				function i() {
					s && (k ? s.attr("viewBox") || s.attr("viewBox", "0 0 " + r.width() + " " + r.height()) : s.attr("width", r.width()).attr("height", r.height()))
				}

				function a(t) {
					if (!r[t] || !r[t]()) throw new n.errors.InvalidStateException("Mandatory attribute chart." + t + " is missing on chart[#" + r.anchorName() + "]")
				}
				r.__dcFlag__ = n.utils.uniqueId();
				var o, l, u, c, s, d, f, h, g, p, y, v = 200,
					x = function (t) {
						var e = t && t.getBoundingClientRect && t.getBoundingClientRect().width;
						return e && e > v ? e : v
					},
					m = x,
					A = 200,
					b = function (t) {
						var e = t && t.getBoundingClientRect && t.getBoundingClientRect().height;
						return e && e > A ? e : A
					},
					_ = b,
					k = !1,
					D = n.pluck("key"),
					C = n.pluck("value"),
					w = n.pluck("key"),
					L = n.pluck("key"),
					R = !1,
					E = function (t) {
						return r.keyAccessor()(t) + ": " + r.valueAccessor()(t)
					},
					M = !0,
					S = !1,
					O = 750,
					B = 0,
					F = n.printers.filters,
					T = ["dimension", "group"],
					N = n.constants.DEFAULT_CHART_GROUP,
					H = t.dispatch("preRender", "postRender", "preRedraw", "postRedraw", "filtered", "zoomed", "renderlet", "pretransition"),
					G = [],
					P = function (t, e) {
						return 0 === e.length ? t.filter(null) : 1 !== e.length || e[0].isFiltered ? 1 === e.length && "RangedFilter" === e[0].filterType ? t.filterRange(e[0]) : t.filterFunction(function (t) {
							for (var n = 0; n < e.length; n++) {
								var r = e[n];
								if (r.isFiltered && r.isFiltered(t)) return !0;
								if (r <= t && r >= t) return !0
							}
							return !1
						}) : t.filterExact(e[0]), e
					},
					U = function (t) {
						return t.all()
					};
				r.height = function (t) {
					return arguments.length ? (_ = t ? "function" == typeof t ? t : n.utils.constant(t) : b, h = void 0, r) : (n.utils.isNumber(h) || (h = _(c.node())), h)
				}, r.width = function (t) {
					return arguments.length ? (m = t ? "function" == typeof t ? t : n.utils.constant(t) : x, f = void 0, r) : (n.utils.isNumber(f) || (f = m(c.node())), f)
				}, r.minWidth = function (t) {
					return arguments.length ? (v = t, r) : v
				}, r.minHeight = function (t) {
					return arguments.length ? (A = t, r) : A
				}, r.useViewBoxResizing = function (t) {
					return arguments.length ? (k = t, r) : k
				}, r.dimension = function (t) {
					return arguments.length ? (o = t, r.expireCache(), r) : o
				}, r.data = function (t) {
					return arguments.length ? (U = "function" == typeof t ? t : n.utils.constant(t), r.expireCache(), r) : U.call(r, l)
				}, r.group = function (t, e) {
					return arguments.length ? (l = t, r._groupName = e, r.expireCache(), r) : l
				}, r.ordering = function (t) {
					return arguments.length ? (L = t, g = e.quicksort.by(L), r.expireCache(), r) : L
				}, r._computeOrderedGroups = function (t) {
					var n = t.slice(0);
					return n.length <= 1 ? n : (g || (g = e.quicksort.by(L)), g(n, 0, n.length))
				}, r.filterAll = function () {
					return r.filter(null)
				}, r.select = function (t) {
					return c.select(t)
				}, r.selectAll = function (t) {
					return c ? c.selectAll(t) : null
				}, r.anchor = function (e, i) {
					if (!arguments.length) return u;
					if (n.instanceOfChart(e))(u = e.anchor()).children && (u = "#" + e.anchorName()), c = e.root(), d = !0;
					else {
						if (!e) throw new n.errors.BadArgumentException("parent must be defined");
						u = e.select && e.classed ? e.node() : e, (c = t.select(u)).classed(n.constants.CHART_CLASS, !0), n.registerChart(r, i), d = !1
					}
					return N = i, r
				}, r.anchorName = function () {
					var t = r.anchor();
					return t && t.id ? t.id : t && t.replace ? t.replace("#", "") : "dc-chart" + r.chartID()
				}, r.root = function (t) {
					return arguments.length ? (c = t, r) : c
				}, r.svg = function (t) {
					return arguments.length ? (s = t, r) : s
				}, r.resetSvg = function () {
					return r.select("svg").remove(), s = r.root().append("svg"), i(), s
				}, r.filterPrinter = function (t) {
					return arguments.length ? (F = t, r) : F
				}, r.controlsUseVisibility = function (t) {
					return arguments.length ? (S = t, r) : S
				}, r.turnOnControls = function () {
					if (c) {
						var t = r.controlsUseVisibility() ? "visibility" : "display";
						r.selectAll(".reset").style(t, null), r.selectAll(".filter").text(F(r.filters())).style(t, null)
					}
					return r
				}, r.turnOffControls = function () {
					if (c) {
						var t = r.controlsUseVisibility() ? "visibility" : "display",
							e = r.controlsUseVisibility() ? "hidden" : "none";
						r.selectAll(".reset").style(t, e), r.selectAll(".filter").style(t, e).text(r.filter())
					}
					return r
				}, r.transitionDuration = function (t) {
					return arguments.length ? (O = t, r) : O
				}, r.transitionDelay = function (t) {
					return arguments.length ? (B = t, r) : B
				}, r._mandatoryAttributes = function (t) {
					return arguments.length ? (T = t, r) : T
				}, r.render = function () {
					h = f = void 0, H.call("preRender", r, r), T && T.forEach(a);
					var t = r._doRender();
					return p && p.render(), r._activateRenderlets("postRender"), t
				}, r._activateRenderlets = function (t) {
					H.call("pretransition", r, r), r.transitionDuration() > 0 && s ? s.transition().duration(r.transitionDuration()).delay(r.transitionDelay()).on("end", function () {
						H.call("renderlet", r, r), t && H.call(t, r, r)
					}) : (H.call("renderlet", r, r), t && H.call(t, r, r))
				}, r.redraw = function () {
					i(), H.call("preRedraw", r, r);
					var t = r._doRedraw();
					return p && p.render(), r._activateRenderlets("postRedraw"), t
				}, r.commitHandler = function (t) {
					return arguments.length ? (y = t, r) : y
				}, r.redrawGroup = function () {
					return y ? y(!1, function (t, e) {
						t ? console.log(t) : n.redrawAll(r.chartGroup())
					}) : n.redrawAll(r.chartGroup()), r
				}, r.renderGroup = function () {
					return y ? y(!1, function (t, e) {
						t ? console.log(t) : n.renderAll(r.chartGroup())
					}) : n.renderAll(r.chartGroup()), r
				}, r._invokeFilteredListener = function (t) {
					void 0 !== t && H.call("filtered", r, r, t)
				}, r._invokeZoomedListener = function () {
					H.call("zoomed", r, r)
				};
				var Y = function (t, e) {
					return null === e || void 0 === e ? t.length > 0 : t.some(function (t) {
						return e <= t && e >= t
					})
				};
				r.hasFilterHandler = function (t) {
					return arguments.length ? (Y = t, r) : Y
				}, r.hasFilter = function (t) {
					return Y(G, t)
				};
				var z = function (t, e) {
					for (var n = 0; n < t.length; n++)
						if (t[n] <= e && t[n] >= e) {
							t.splice(n, 1);
							break
						}
					return t
				};
				r.removeFilterHandler = function (t) {
					return arguments.length ? (z = t, r) : z
				};
				var V = function (t, e) {
					return t.push(e), t
				};
				r.addFilterHandler = function (t) {
					return arguments.length ? (V = t, r) : V
				};
				var I = function (t) {
					return []
				};
				return r.resetFilterHandler = function (t) {
					return arguments.length ? (I = t, r) : I
				}, r.replaceFilter = function (t) {
					return G = I(G), r.filter(t), r
				}, r.filter = function (t) {
					if (!arguments.length) return G.length > 0 ? G[0] : null;
					var e = G;
					return t instanceof Array && t[0] instanceof Array && !t.isFiltered ? t[0].forEach(function (t) {
						e = Y(e, t) ? z(e, t) : V(e, t)
					}) : e = null === t ? I(e) : Y(e, t) ? z(e, t) : V(e, t), G = function (t) {
						if (r.dimension() && r.dimension().filter) {
							var e = P(r.dimension(), t);
							e && (t = e)
						}
						return t
					}(e), r._invokeFilteredListener(t), null !== c && r.hasFilter() ? r.turnOnControls() : r.turnOffControls(), r
				}, r.filters = function () {
					return G
				}, r.highlightSelected = function (e) {
					t.select(e).classed(n.constants.SELECTED_CLASS, !0), t.select(e).classed(n.constants.DESELECTED_CLASS, !1)
				}, r.fadeDeselected = function (e) {
					t.select(e).classed(n.constants.SELECTED_CLASS, !1), t.select(e).classed(n.constants.DESELECTED_CLASS, !0)
				}, r.resetHighlight = function (e) {
					t.select(e).classed(n.constants.SELECTED_CLASS, !1), t.select(e).classed(n.constants.DESELECTED_CLASS, !1)
				}, r.onClick = function (t) {
					var e = r.keyAccessor()(t);
					n.events.trigger(function () {
						r.filter(e), r.redrawGroup()
					})
				}, r.filterHandler = function (t) {
					return arguments.length ? (P = t, r) : P
				}, r._doRender = function () {
					return r
				}, r._doRedraw = function () {
					return r
				}, r.legendables = function () {
					return []
				}, r.legendHighlight = function () {}, r.legendReset = function () {}, r.legendToggle = function () {}, r.isLegendableHidden = function () {
					return !1
				}, r.keyAccessor = function (t) {
					return arguments.length ? (D = t, r) : D
				}, r.valueAccessor = function (t) {
					return arguments.length ? (C = t, r) : C
				}, r.label = function (t, e) {
					return arguments.length ? (w = t, (void 0 === e || e) && (R = !0), r) : w
				}, r.renderLabel = function (t) {
					return arguments.length ? (R = t, r) : R
				}, r.title = function (t) {
					return arguments.length ? (E = t, r) : E
				}, r.renderTitle = function (t) {
					return arguments.length ? (M = t, r) : M
				}, r.renderlet = n.logger.deprecate(function (t) {
					return r.on("renderlet." + n.utils.uniqueId(), t), r
				}, 'chart.renderlet has been deprecated.  Please use chart.on("renderlet.<renderletKey>", renderletFunction)'), r.chartGroup = function (t) {
					return arguments.length ? (d || n.deregisterChart(r, N), N = t, d || n.registerChart(r, N), r) : N
				}, r.expireCache = function () {
					return r
				}, r.legend = function (t) {
					return arguments.length ? ((p = t).parent(r), r) : p
				}, r.chartID = function () {
					return r.__dcFlag__
				}, r.options = function (t) {
					var e = ["anchor", "group", "xAxisLabel", "yAxisLabel", "stack", "title", "point", "getColor", "overlayGeoJson"];
					for (var i in t) "function" == typeof r[i] ? t[i] instanceof Array && -1 !== e.indexOf(i) ? r[i].apply(r, t[i]) : r[i].call(r, t[i]) : n.logger.debug("Not a valid option setter name: " + i);
					return r
				}, r.on = function (t, e) {
					return H.on(t, e), r
				}, r
			}, n.marginMixin = function (t) {
				var e = {
					top: 10,
					right: 50,
					bottom: 30,
					left: 30
				};
				return t.margins = function (n) {
					return arguments.length ? (e = n, t) : e
				}, t.effectiveWidth = function () {
					return t.width() - t.margins().left - t.margins().right
				}, t.effectiveHeight = function () {
					return t.height() - t.margins().top - t.margins().bottom
				}, t
			}, n.colorMixin = function (e) {
				var r = t.scaleOrdinal(n.config.defaultColors()),
					i = !0,
					a = function (t) {
						return e.keyAccessor()(t)
					};
				return e.colors = function (i) {
					return arguments.length ? (r = i instanceof Array ? t.scaleQuantize().range(i) : "function" == typeof i ? i : n.utils.constant(i), e) : r
				}, e.ordinalColors = function (n) {
					return e.colors(t.scaleOrdinal().range(n))
				}, e.linearColors = function (n) {
					return e.colors(t.scaleLinear().range(n).interpolate(t.interpolateHcl))
				}, e.colorAccessor = function (t) {
					return arguments.length ? (a = t, i = !1, e) : a
				}, e.defaultColorAccessor = function () {
					return i
				}, e.colorDomain = function (t) {
					return arguments.length ? (r.domain(t), e) : r.domain()
				}, e.calculateColorDomain = function () {
					var n = [t.min(e.data(), e.colorAccessor()), t.max(e.data(), e.colorAccessor())];
					return r.domain(n), e
				}, e.getColor = function (t, e) {
					return r(a.call(this, t, e))
				}, e.colorCalculator = n.logger.deprecate(function (t) {
					return arguments.length ? (e.getColor = t, e) : e.getColor
				}, "colorMixin.colorCalculator has been deprecated. Please colorMixin.colors and colorMixin.colorAccessor instead"), e
			}, n.coordinateGridMixin = function (e) {
				function r(r, i) {
					e.isOrdinal() ? (v.bandwidth || (n.logger.warn("For compatibility with d3v4+, dc.js d3.0 ordinal bar/line/bubble charts need d3.scaleBand() for the x scale, instead of d3.scaleOrdinal(). Replacing .x() with a d3.scaleBand with the same domain - make the same change in your code to avoid this warning!"), v = t.scaleBand().domain(v.domain())), (e.elasticX() || 0 === v.domain().length) && v.domain(e._ordinalXDomain())) : e.elasticX() && v.domain([e.xAxisMin(), e.xAxisMax()]);
					var a = v.domain();
					!i && n.utils.arraysEqual(b, a) || e.rescale(), b = a, e.isOrdinal() ? v.range([0, e.xAxisLength()]).paddingInner($).paddingOuter(e._useOuterPadding() ? Q : 0) : v.range([0, e.xAxisLength()]), E = E.scale(e.x()),
						function (t) {
							var r = t.select("g." + f);
							if (V) {
								r.empty() && (r = t.insert("g", ":first-child").attr("class", d + " " + f).attr("transform", "translate(" + e.margins().left + "," + e.margins().top + ")"));
								var i = E.tickValues() ? E.tickValues() : "function" == typeof v.ticks ? v.ticks.apply(v, E.tickArguments()) : v.domain(),
									a = r.selectAll("line").data(i),
									o = a.enter().append("line").attr("x1", function (t) {
										return v(t)
									}).attr("y1", e._xAxisY() - e.margins().top).attr("x2", function (t) {
										return v(t)
									}).attr("y2", 0).attr("opacity", 0);
								n.transition(o, e.transitionDuration(), e.transitionDelay()).attr("opacity", .5), n.transition(a, e.transitionDuration(), e.transitionDelay()).attr("x1", function (t) {
									return v(t)
								}).attr("y1", e._xAxisY() - e.margins().top).attr("x2", function (t) {
									return v(t)
								}).attr("y2", 0), a.exit().remove()
							} else r.selectAll("line").remove()
						}(r)
				}

				function i() {
					return tt ? t.axisRight() : t.axisLeft()
				}

				function a() {
					return e.anchorName().replace(/[ .#=\[\]"]/g, "-") + "-clip"
				}

				function o() {
					var t = n.utils.appendOrSelect(g, "defs"),
						r = a(),
						i = n.utils.appendOrSelect(t, "#" + r, "clipPath").attr("id", r),
						o = 2 * J;
					n.utils.appendOrSelect(i, "rect").attr("width", e.xAxisLength() + o).attr("height", e.yAxisHeight() + o).attr("transform", "translate(-" + J + ", -" + J + ")")
				}

				function l(t) {
					e.isOrdinal() && (U = !1), r(e.g(), t), e._prepareYAxis(e.g()), e.plotData(), (e.elasticX() || I || t) && e.renderXAxis(e.g()), (e.elasticY() || I || t) && e.renderYAxis(e.g()), t ? e.renderBrush(e.g(), !1) : e.redrawBrush(e.filter(), I), e.fadeDeselectedArea(e.filter()), I = !1
				}

				function u(t, r) {
					var i;
					! function (t) {
						return t instanceof Array && t.length > 1
					}(t) ? (e.x().domain(m), i = null) : (e.x().domain(t), i = n.filters.RangedFilter(t[0], t[1])), e.replaceFilter(i), e.rescale(), e.redraw(), r || (L && !n.utils.arraysEqual(e.filter(), L.filter()) && n.events.trigger(function () {
						L.replaceFilter(i), L.redraw()
					}), e._invokeZoomedListener(), n.events.trigger(function () {
						e.redrawGroup()
					}, n.constants.EVENT_DELAY))
				}

				function c() {
					W && W.transform(e.root(), function (e, n, r) {
						var i = (n[1] - n[0]) / (e[1] - e[0]),
							a = -1 * r(e[0]);
						return t.zoomIdentity.scale(i).translate(a, 0)
					}(e.x().domain(), m, x))
				}

				function s(t, e) {
					return t && 2 === t.length && e && 2 === e.length ? ((t[0] > e[1] || t[1] < e[0]) && console.warn("Could not intersect extents, will reset"), [t[0] > e[0] ? t[0] : e[0], t[1] < e[1] ? t[1] : e[1]]) : t
				}
				var d = "grid-line",
					f = "vertical",
					h = "custom-brush-handle";
				(e = n.colorMixin(n.marginMixin(n.baseMixin(e)))).colors(t.scaleOrdinal(t.schemeCategory10)), e._mandatoryAttributes().push("x");
				var g, p, y, v, x, m, A, b, _, k, D, C, w, L, R, E = t.axisBottom(),
					M = n.units.integers,
					S = 0,
					O = t.timeDay,
					B = !1,
					F = 0,
					T = null,
					N = 0,
					H = !1,
					G = 0,
					P = t.brushX(),
					U = !0,
					Y = !1,
					z = !1,
					V = !1,
					I = !1,
					X = [1, 1 / 0],
					q = !0,
					W = t.zoom().on("zoom", function () {
						if (t.event.sourceEvent && (!t.event.sourceEvent.type || -1 === ["start", "zoom", "end"].indexOf(t.event.sourceEvent.type))) {
							var n = t.event.transform.rescaleX(x).domain();
							e.focus(n, !1)
						}
					}),
					j = t.zoom().on("zoom", null),
					K = !1,
					Z = !1,
					J = 0,
					Q = .5,
					$ = 0,
					tt = !1;
				return e.rescale = function () {
					return w = void 0, I = !0, e
				}, e.resizing = function () {
					return I
				}, e.rangeChart = function (t) {
					return arguments.length ? ((L = t).focusChart(e), e) : L
				}, e.zoomScale = function (t) {
					return arguments.length ? (X = t, e) : X
				}, e.zoomOutRestrict = function (t) {
					return arguments.length ? (q = t, e) : q
				}, e._generateG = function (t) {
					g = void 0 === t ? e.svg() : t;
					var n = window.location.href.split("#")[0];
					return p = g.append("g"), y = p.append("g").attr("class", "chart-body").attr("transform", "translate(" + e.margins().left + ", " + e.margins().top + ")").attr("clip-path", "url(" + n + "#" + a() + ")"), p
				}, e.g = function (t) {
					return arguments.length ? (p = t, e) : p
				}, e.mouseZoomable = function (t) {
					return arguments.length ? (Z = t, e) : Z
				}, e.chartBodyG = function (t) {
					return arguments.length ? (y = t, e) : y
				}, e.x = function (t) {
					return arguments.length ? (v = t, m = v.domain(), e.rescale(), e) : v
				}, e.xOriginalDomain = function () {
					return m
				}, e.xUnits = function (t) {
					return arguments.length ? (M = t, e) : M
				}, e.xAxis = function (t) {
					return arguments.length ? (E = t, e) : E
				}, e.elasticX = function (t) {
					return arguments.length ? (B = t, e) : B
				}, e.xAxisPadding = function (t) {
					return arguments.length ? (S = t, e) : S
				}, e.xAxisPaddingUnit = function (t) {
					return arguments.length ? (O = t, e) : O
				}, e.xUnitCount = function () {
					return void 0 === w && (e.isOrdinal() ? w = e.x().domain().length : (w = e.xUnits()(e.x().domain()[0], e.x().domain()[1])) instanceof Array && (w = w.length)), w
				}, e.useRightYAxis = function (t) {
					return arguments.length ? (tt !== t && T && n.logger.warn("Value of useRightYAxis has been altered, after yAxis was created. You might get unexpected yAxis behavior. Make calls to useRightYAxis sooner in your chart creation process."), tt = t, e) : tt
				}, e.isOrdinal = function () {
					return e.xUnits() === n.units.ordinal
				}, e._useOuterPadding = function () {
					return !0
				}, e._ordinalXDomain = function () {
					return e._computeOrderedGroups(e.data()).map(e.keyAccessor())
				}, e.renderXAxis = function (t) {
					var r = t.select("g.x");
					r.empty() && (r = t.append("g").attr("class", "axis x").attr("transform", "translate(" + e.margins().left + "," + e._xAxisY() + ")"));
					var i = t.select("text.x-axis-label");
					i.empty() && e.xAxisLabel() && (i = t.append("text").attr("class", "x-axis-label").attr("transform", "translate(" + (e.margins().left + e.xAxisLength() / 2) + "," + (e.height() - F) + ")").attr("text-anchor", "middle")), e.xAxisLabel() && i.text() !== e.xAxisLabel() && i.text(e.xAxisLabel()), n.transition(r, e.transitionDuration(), e.transitionDelay()).attr("transform", "translate(" + e.margins().left + "," + e._xAxisY() + ")").call(E), n.transition(i, e.transitionDuration(), e.transitionDelay()).attr("transform", "translate(" + (e.margins().left + e.xAxisLength() / 2) + "," + (e.height() - F) + ")")
				}, e._xAxisY = function () {
					return e.height() - e.margins().bottom
				}, e.xAxisLength = function () {
					return e.effectiveWidth()
				}, e.xAxisLabel = function (t, n) {
					return arguments.length ? (A = t, e.margins().bottom -= F, F = void 0 === n ? 12 : n, e.margins().bottom += F, e) : A
				}, e._prepareYAxis = function (n) {
					if (void 0 === _ || e.elasticY()) {
						void 0 === _ && (_ = t.scaleLinear());
						var r = e.yAxisMin() || 0,
							a = e.yAxisMax() || 0;
						_.domain([r, a]).rangeRound([e.yAxisHeight(), 0])
					}
					_.range([e.yAxisHeight(), 0]), T || (T = i()), T.scale(_), e._renderHorizontalGridLinesForAxis(n, _, T)
				}, e.renderYAxisLabel = function (t, r, i, a) {
					a = a || G;
					var o = e.g().select("text.y-axis-label." + t + "-label"),
						l = e.margins().top + e.yAxisHeight() / 2;
					o.empty() && r && (o = e.g().append("text").attr("transform", "translate(" + a + "," + l + "),rotate(" + i + ")").attr("class", "y-axis-label " + t + "-label").attr("text-anchor", "middle").text(r)), r && o.text() !== r && o.text(r), n.transition(o, e.transitionDuration(), e.transitionDelay()).attr("transform", "translate(" + a + "," + l + "),rotate(" + i + ")")
				}, e.renderYAxisAt = function (t, r, i) {
					var a = e.g().select("g." + t);
					a.empty() && (a = e.g().append("g").attr("class", "axis " + t).attr("transform", "translate(" + i + "," + e.margins().top + ")")), n.transition(a, e.transitionDuration(), e.transitionDelay()).attr("transform", "translate(" + i + "," + e.margins().top + ")").call(r)
				}, e.renderYAxis = function () {
					var t = tt ? e.width() - e.margins().right : e._yAxisX();
					e.renderYAxisAt("y", T, t);
					var n = tt ? e.width() - G : G,
						r = tt ? 90 : -90;
					e.renderYAxisLabel("y", e.yAxisLabel(), r, n)
				}, e._renderHorizontalGridLinesForAxis = function (t, r, i) {
					var a = t.select("g.horizontal");
					if (z) {
						var o = i.tickValues() ? i.tickValues() : r.ticks ? r.ticks.apply(r, i.tickArguments()) : r.domain();
						a.empty() && (a = t.insert("g", ":first-child").attr("class", d + " horizontal").attr("transform", "translate(" + e.margins().left + "," + e.margins().top + ")"));
						var l = a.selectAll("line").data(o),
							u = l.enter().append("line").attr("x1", 1).attr("y1", function (t) {
								return r(t)
							}).attr("x2", e.xAxisLength()).attr("y2", function (t) {
								return r(t)
							}).attr("opacity", 0);
						n.transition(u, e.transitionDuration(), e.transitionDelay()).attr("opacity", .5), n.transition(l, e.transitionDuration(), e.transitionDelay()).attr("x1", 1).attr("y1", function (t) {
							return r(t)
						}).attr("x2", e.xAxisLength()).attr("y2", function (t) {
							return r(t)
						}), l.exit().remove()
					} else a.selectAll("line").remove()
				}, e._yAxisX = function () {
					return e.useRightYAxis() ? e.width() - e.margins().right : e.margins().left
				}, e.yAxisLabel = function (t, n) {
					return arguments.length ? (k = t, e.margins().left -= G, G = void 0 === n ? 12 : n, e.margins().left += G, e) : k
				}, e.y = function (t) {
					return arguments.length ? (_ = t, e.rescale(), e) : _
				}, e.yAxis = function (t) {
					return arguments.length ? (T = t, e) : (T || (T = i()), T)
				}, e.elasticY = function (t) {
					return arguments.length ? (H = t, e) : H
				}, e.renderHorizontalGridLines = function (t) {
					return arguments.length ? (z = t, e) : z
				}, e.renderVerticalGridLines = function (t) {
					return arguments.length ? (V = t, e) : V
				}, e.xAxisMin = function () {
					var r = t.min(e.data(), function (t) {
						return e.keyAccessor()(t)
					});
					return n.utils.subtract(r, S, O)
				}, e.xAxisMax = function () {
					var r = t.max(e.data(), function (t) {
						return e.keyAccessor()(t)
					});
					return n.utils.add(r, S, O)
				}, e.yAxisMin = function () {
					var r = t.min(e.data(), function (t) {
						return e.valueAccessor()(t)
					});
					return n.utils.subtract(r, N)
				}, e.yAxisMax = function () {
					var r = t.max(e.data(), function (t) {
						return e.valueAccessor()(t)
					});
					return n.utils.add(r, N)
				}, e.yAxisPadding = function (t) {
					return arguments.length ? (N = t, e) : N
				}, e.yAxisHeight = function () {
					return e.effectiveHeight()
				}, e.round = function (t) {
					return arguments.length ? (C = t, e) : C
				}, e._rangeBandPadding = function (t) {
					return arguments.length ? ($ = t, e) : $
				}, e._outerRangeBandPadding = function (t) {
					return arguments.length ? (Q = t, e) : Q
				}, n.override(e, "filter", function (t) {
					return arguments.length ? (e._filter(t), e.redrawBrush(t, !1), e) : e._filter()
				}), e.brush = function (t) {
					return arguments.length ? (P = t, e) : P
				}, e.renderBrush = function (t, n) {
					U && (P.on("start brush end", e._brushing), D = t.append("g").attr("class", "brush").attr("transform", "translate(" + e.margins().left + "," + e.margins().top + ")"), e.setBrushExtents(), e.createBrushHandlePaths(D, n), e.redrawBrush(e.filter(), n))
				}, e.createBrushHandlePaths = function (t) {
					var n = t.selectAll("path." + h).data([{
						type: "w"
					}, {
						type: "e"
					}]);
					(n = n.enter().append("path").attr("class", h).merge(n)).attr("d", e.resizeHandlePath)
				}, e.extendBrush = function (t) {
					return t && e.round() && (t[0] = e.round()(t[0]), t[1] = e.round()(t[1])), t
				}, e.brushIsEmpty = function (t) {
					return !t || t[1] <= t[0]
				}, e._brushing = function () {
					if (t.event.sourceEvent && (!t.event.sourceEvent.type || -1 === ["start", "brush", "end"].indexOf(t.event.sourceEvent.type))) {
						var r = t.event.selection;
						r && (r = r.map(e.x().invert)), r = e.extendBrush(r), e.redrawBrush(r, !1);
						var i = e.brushIsEmpty(r) ? null : n.filters.RangedFilter(r[0], r[1]);
						n.events.trigger(function () {
							e.applyBrushSelection(i)
						}, n.constants.EVENT_DELAY)
					}
				}, e.applyBrushSelection = function (t) {
					e.replaceFilter(t), e.redrawGroup()
				}, e.setBrushExtents = function (t) {
					P.extent([
						[0, 0],
						[e.effectiveWidth(), e.effectiveHeight()]
					]), D.call(P)
				}, e.redrawBrush = function (t, r) {
					if (U && D)
						if (I && e.setBrushExtents(r), t) {
							var i = [v(t[0]), v(t[1])],
								a = n.optionalTransition(r, e.transitionDuration(), e.transitionDelay())(D);
							a.call(P.move, i), a.selectAll("path." + h).attr("display", null).attr("transform", function (e, n) {
								return "translate(" + v(t[n]) + ", 0)"
							}).attr("d", e.resizeHandlePath)
						} else D.call(P.move, null), D.selectAll("path." + h).attr("display", "none");
					e.fadeDeselectedArea(t)
				}, e.fadeDeselectedArea = function (t) {}, e.resizeHandlePath = function (t) {
					var n = +("e" === (t = t.type)),
						r = n ? 1 : -1,
						i = e.effectiveHeight() / 3;
					return "M" + .5 * r + "," + i + "A6,6 0 0 " + n + " " + 6.5 * r + "," + (i + 6) + "V" + (2 * i - 6) + "A6,6 0 0 " + n + " " + .5 * r + "," + 2 * i + "ZM" + 2.5 * r + "," + (i + 8) + "V" + (2 * i - 8) + "M" + 4.5 * r + "," + (i + 8) + "V" + (2 * i - 8)
				}, e.clipPadding = function (t) {
					return arguments.length ? (J = t, e) : J
				}, e._preprocessData = function () {}, e._doRender = function () {
					return e.resetSvg(), e._preprocessData(), e._generateG(), o(), l(!0), x = v.copy(), Z ? e._enableMouseZoom() : K && e._disableMouseZoom(), e
				}, e._doRedraw = function () {
					return e._preprocessData(), l(!1), o(), e
				}, e._enableMouseZoom = function () {
					K = !0;
					var t = [
						[0, 0],
						[e.effectiveWidth(), e.effectiveHeight()]
					];
					if (W.scaleExtent(X).extent(t).duration(e.transitionDuration()), q) {
						var n = Math.max(X[0], 1);
						W.translateExtent(t).scaleExtent([n, X[1]])
					}
					e.root().call(W), c()
				}, e._disableMouseZoom = function () {
					e.root().call(j)
				}, e.focus = function (t, e) {
					q && (t = s(t, m), L && (t = s(t, L.x().domain()))), u(t, e), c()
				}, e.refocused = function () {
					return !n.utils.arraysEqual(e.x().domain(), m)
				}, e.focusChart = function (t) {
					return arguments.length ? (R = t, e.on("filtered.dcjs-range-chart", function (t) {
						t.filter() ? n.utils.arraysEqual(t.filter(), R.filter()) || n.events.trigger(function () {
							R.focus(t.filter(), !0)
						}) : n.events.trigger(function () {
							R.x().domain(R.xOriginalDomain(), !0)
						})
					}), e) : R
				}, e.brushOn = function (t) {
					return arguments.length ? (U = t, e) : U
				}, e.parentBrushOn = function (t) {
					return arguments.length ? (Y = t, e) : Y
				}, e.gBrush = function () {
					return D
				}, e
			}, n.stackMixin = function (e) {
				function r(t, r) {
					var i = t.accessor || e.valueAccessor();
					t.name = String(t.name || r);
					var a = t.group.all().map(function (n, r) {
						return {
							x: e.keyAccessor()(n, r),
							y: t.hidden ? null : i(n, r),
							data: n,
							layer: t.name,
							hidden: t.hidden
						}
					});
					t.domainValues = a.filter(function () {
						if (!e.x()) return n.utils.constant(!0);
						var t = e.x().domain();
						if (e.isOrdinal()) return function () {
							return !0
						};
						if (e.elasticX()) return function () {
							return !0
						};
						return function (e) {
							return e.x >= t[0] && e.x <= t[t.length - 1]
						}
					}()), t.values = e.evadeDomainFilter() ? a : t.domainValues
				}

				function i(t) {
					var e = u.map(n.pluck("name")).indexOf(t);
					return u[e]
				}

				function a() {
					var t = e.data().map(function (t) {
						return t.domainValues
					});
					return Array.prototype.concat.apply([], t)
				}

				function o(t) {
					return !t.hidden
				}
				var l = t.stack(),
					u = [],
					c = {},
					s = !1,
					d = !1;
				return e.stack = function (t, n, r) {
					if (!arguments.length) return u;
					arguments.length <= 2 && (r = n);
					var i = {
						group: t
					};
					return "string" == typeof n && (i.name = n), "function" == typeof r && (i.accessor = r), u.push(i), e
				}, n.override(e, "group", function (t, n, r) {
					return arguments.length ? (u = [], c = {}, e.stack(t, n), r && e.valueAccessor(r), e._group(t, n)) : e._group()
				}), e.hidableStacks = function (t) {
					return arguments.length ? (s = t, e) : s
				}, e.hideStack = function (t) {
					var n = i(t);
					return n && (n.hidden = !0), e
				}, e.showStack = function (t) {
					var n = i(t);
					return n && (n.hidden = !1), e
				}, e.getValueAccessorByIndex = function (t) {
					return u[t].accessor || e.valueAccessor()
				}, e.yAxisMin = function () {
					var r = t.min(a(), function (t) {
						return t.y < 0 ? t.y + t.y0 : t.y0
					});
					return n.utils.subtract(r, e.yAxisPadding())
				}, e.yAxisMax = function () {
					var r = t.max(a(), function (t) {
						return t.y > 0 ? t.y + t.y0 : t.y0
					});
					return n.utils.add(r, e.yAxisPadding())
				}, e.xAxisMin = function () {
					var r = t.min(a(), n.pluck("x"));
					return n.utils.subtract(r, e.xAxisPadding(), e.xAxisPaddingUnit())
				}, e.xAxisMax = function () {
					var r = t.max(a(), n.pluck("x"));
					return n.utils.add(r, e.xAxisPadding(), e.xAxisPaddingUnit())
				}, n.override(e, "title", function (t, n) {
					return t ? "function" == typeof t ? e._title(t) : t === e._groupName && "function" == typeof n ? e._title(n) : "function" != typeof n ? c[t] || e._title() : (c[t] = n, e) : e._title()
				}), e.stackLayout = function (t) {
					return arguments.length ? (l = t, e) : l
				}, e.evadeDomainFilter = function (t) {
					return arguments.length ? (d = t, e) : d
				}, e.data(function () {
					var t = u.filter(o);
					if (!t.length) return [];
					t.forEach(r);
					var n = t[0].values.map(function (e, n) {
							var r = {
								x: e.x
							};
							return t.forEach(function (t) {
								r[t.name] = t.values[n].y
							}), r
						}),
						i = t.map(function (t) {
							return t.name
						});
					return e.stackLayout().keys(i)(n).forEach(function (e, n) {
						e.forEach(function (e, r) {
							t[n].values[r].y0 = e[0], t[n].values[r].y1 = e[1]
						})
					}), t
				}), e._ordinalXDomain = function () {
					var t = a().map(n.pluck("data"));
					return e._computeOrderedGroups(t).map(e.keyAccessor())
				}, e.colorAccessor(function (t) {
					return this.layer || this.name || t.name || t.layer
				}), e.legendables = function () {
					return u.map(function (t, n) {
						return {
							chart: e,
							name: t.name,
							hidden: t.hidden || !1,
							color: e.getColor.call(t, t.values, n)
						}
					})
				}, e.isLegendableHidden = function (t) {
					var e = i(t.name);
					return !!e && e.hidden
				}, e.legendToggle = function (t) {
					s && (e.isLegendableHidden(t) ? e.showStack(t.name) : e.hideStack(t.name), e.renderGroup())
				}, e
			}, n.capMixin = function (e) {
				var r = 1 / 0,
					i = !0,
					a = "Others";
				e.ordering(function (t) {
					return -t.value
				});
				var o = function (n, r) {
					var i = t.sum(r, e.valueAccessor()),
						a = r.map(e.keyAccessor());
					return i > 0 ? n.concat([{
						others: a,
						key: e.othersLabel(),
						value: i
					}]) : n
				};
				return e.cappedKeyAccessor = function (t, n) {
					return t.others ? t.key : e.keyAccessor()(t, n)
				}, e.cappedValueAccessor = function (t, n) {
					return t.others ? t.value : e.valueAccessor()(t, n)
				}, e.data(function (t) {
					if (r === 1 / 0) return e._computeOrderedGroups(t.all());
					var n, a = t.all();
					if (a = e._computeOrderedGroups(a), r)
						if (i) n = a.slice(r), a = a.slice(0, r);
						else {
							var l = Math.max(0, a.length - r);
							n = a.slice(0, l), a = a.slice(l)
						}
					return o ? o(a, n) : a
				}), e.cap = function (t) {
					return arguments.length ? (r = t, e) : r
				}, e.takeFront = function (t) {
					return arguments.length ? (i = t, e) : i
				}, e.othersLabel = function (t) {
					return arguments.length ? (a = t, e) : a
				}, e.othersGrouper = function (t) {
					return arguments.length ? (o = t, e) : o
				}, n.override(e, "onClick", function (t) {
					t.others && e.filter([t.others]), e._onClick(t)
				}), e
			}, n.bubbleMixin = function (e) {
				var r = .3,
					i = 10,
					a = !1,
					o = !1;
				e.BUBBLE_NODE_CLASS = "node", e.BUBBLE_CLASS = "bubble", e.MIN_RADIUS = 10, (e = n.colorMixin(e)).renderLabel(!0), e.data(function (n) {
					var r = n.all();
					if (a) {
						var i = e.radiusValueAccessor();
						r.sort(function (e, n) {
							return t.descending(i(e), i(n))
						})
					}
					return r
				});
				var l = t.scaleLinear().domain([0, 100]),
					u = function (t) {
						return t.r
					};
				e.r = function (t) {
					return arguments.length ? (l = t, e) : l
				}, e.elasticRadius = function (t) {
					return arguments.length ? (o = t, e) : o
				}, e.calculateRadiusDomain = function () {
					o && e.r().domain([e.rMin(), e.rMax()])
				}, e.radiusValueAccessor = function (t) {
					return arguments.length ? (u = t, e) : u
				}, e.rMin = function () {
					return t.min(e.data(), function (t) {
						return e.radiusValueAccessor()(t)
					})
				}, e.rMax = function () {
					return t.max(e.data(), function (t) {
						return e.radiusValueAccessor()(t)
					})
				}, e.bubbleR = function (t) {
					var n = e.radiusValueAccessor()(t),
						r = e.r()(n);
					return (isNaN(r) || n <= 0) && (r = 0), r
				};
				var c = function (t) {
						return e.label()(t)
					},
					s = function (t) {
						return e.bubbleR(t) > i
					},
					d = function (t) {
						return s(t) ? 1 : 0
					},
					f = function (t) {
						return s(t) ? "all" : "none"
					};
				e._doRenderLabel = function (t) {
					if (e.renderLabel()) {
						var r = t.select("text");
						r.empty() && (r = t.append("text").attr("text-anchor", "middle").attr("dy", ".3em").on("click", e.onClick)), r.attr("opacity", 0).attr("pointer-events", f).text(c), n.transition(r, e.transitionDuration(), e.transitionDelay()).attr("opacity", d)
					}
				}, e.doUpdateLabels = function (t) {
					if (e.renderLabel()) {
						var r = t.select("text").attr("pointer-events", f).text(c);
						n.transition(r, e.transitionDuration(), e.transitionDelay()).attr("opacity", d)
					}
				};
				var h = function (t) {
					return e.title()(t)
				};
				return e._doRenderTitles = function (t) {
					if (e.renderTitle()) {
						t.select("title").empty() && t.append("title").text(h)
					}
				}, e.doUpdateTitles = function (t) {
					e.renderTitle() && t.select("title").text(h)
				}, e.sortBubbleSize = function (t) {
					return arguments.length ? (a = t, e) : a
				}, e.minRadius = function (t) {
					return arguments.length ? (e.MIN_RADIUS = t, e) : e.MIN_RADIUS
				}, e.minRadiusWithLabel = function (t) {
					return arguments.length ? (i = t, e) : i
				}, e.maxBubbleRelativeSize = function (t) {
					return arguments.length ? (r = t, e) : r
				}, e.fadeDeselectedArea = function (t) {
					e.hasFilter() ? e.selectAll("g." + e.BUBBLE_NODE_CLASS).each(function (t) {
						e.isSelectedNode(t) ? e.highlightSelected(this) : e.fadeDeselected(this)
					}) : e.selectAll("g." + e.BUBBLE_NODE_CLASS).each(function () {
						e.resetHighlight(this)
					})
				}, e.isSelectedNode = function (t) {
					return e.hasFilter(t.key)
				}, e.onClick = function (t) {
					var r = t.key;
					n.events.trigger(function () {
						e.filter(r), e.redrawGroup()
					})
				}, e
			}, n.pieChart = function (e, r) {
				function i() {
					var e = t.min([S.width(), S.height()]) / 2;
					p = y && y < e ? y : e;
					var r, i = u(),
						g = t.pie().sort(null).value(S.cappedValueAccessor);
					if (t.sum(S.data(), S.cappedValueAccessor) ? (r = g(S.data()), v.classed(C, !1)) : (r = g([{
							key: w,
							value: 1,
							others: [w]
						}]), v.classed(C, !0)), v) {
						var x = v.select("g." + k).selectAll("g." + b).data(r),
							m = v.select("g." + D).selectAll("text." + _).data(r);
						! function (t, e) {
							t.exit().remove(), e.exit().remove()
						}(x, m),
						function (t, e, r, i) {
							var u = function (t) {
								return t.enter().append("g").attr("class", function (t, e) {
									return b + " _" + e
								})
							}(t);
							(function (t, e) {
								var r = t.append("path").attr("fill", d).on("click", f).attr("d", function (t, n) {
										return h(t, n, e)
									}),
									i = n.transition(r, S.transitionDuration(), S.transitionDelay());
								i.attrTween && i.attrTween("d", s)
							})(u, r),
							function (t) {
								S.renderTitle() && t.append("title").text(function (t) {
									return S.title()(t.data)
								})
							}(u),
							function (t, e, n) {
								if (S.renderLabel()) {
									var r = t.enter().append("text").attr("class", function (t, e) {
										var n = b + " " + _ + " _" + e;
										return A && (n += " external"), n
									}).on("click", f).on("mouseover", function (t, e) {
										o(e, !0)
									}).on("mouseout", function (t, e) {
										o(e, !1)
									});
									a(r, n), A && M && l(e, n)
								}
							}(e, i, r)
						}(x, m, i, r),
						function (t, e) {
							(function (t, e) {
								var r = v.selectAll("g." + b).data(t).select("path").attr("d", function (t, n) {
										return h(t, n, e)
									}),
									i = n.transition(r, S.transitionDuration(), S.transitionDelay());
								i.attrTween && i.attrTween("d", s);
								i.attr("fill", d)
							})(t, e),
							function (t, e) {
								if (S.renderLabel()) {
									var n = v.selectAll("text." + _).data(t);
									a(n, e), A && M && l(t, e)
								}
							}(t, e),
							function (t) {
								S.renderTitle() && v.selectAll("g." + b).data(t).select("title").text(function (t) {
									return S.title()(t.data)
								})
							}(t)
						}(r, i), S.hasFilter() ? S.selectAll("g." + b).each(function (t) {
							c(t) ? S.highlightSelected(this) : S.fadeDeselected(this)
						}) : S.selectAll("g." + b).each(function () {
							S.resetHighlight(this)
						}), n.transition(v, S.transitionDuration(), S.transitionDelay()).attr("transform", "translate(" + S.cx() + "," + S.cy() + ")")
					}
				}

				function a(e, r) {
					S._applyLabelText(e), n.transition(e, S.transitionDuration(), S.transitionDelay()).attr("transform", function (e) {
						return function (e, n) {
							var r;
							r = A ? t.arc().outerRadius(p - R + A).innerRadius(p - R + A).centroid(e) : n.centroid(e);
							return isNaN(r[0]) || isNaN(r[1]) ? "translate(0,0)" : "translate(" + r + ")"
						}(e, r)
					}).attr("text-anchor", "middle")
				}

				function o(t, e) {
					S.select("g.pie-slice._" + t).classed("highlight", e)
				}

				function l(e, r) {
					var i = v.selectAll("polyline." + b).data(e);
					i.exit().remove(), i = i.enter().append("polyline").attr("class", function (t, e) {
						return "pie-path _" + e + " " + b
					}).on("click", f).on("mouseover", function (t, e) {
						o(e, !0)
					}).on("mouseout", function (t, e) {
						o(e, !1)
					}).merge(i);
					var a = t.arc().outerRadius(p - R + A).innerRadius(p - R),
						l = n.transition(i, S.transitionDuration(), S.transitionDelay());
					l.attrTween ? l.attrTween("points", function (e) {
						var n = this._current || e;
						n = {
							startAngle: n.startAngle,
							endAngle: n.endAngle
						};
						var i = t.interpolate(n, e);
						return this._current = i(0),
							function (t) {
								var e = i(t);
								return [r.centroid(e), a.centroid(e)]
							}
					}) : l.attr("points", function (t) {
						return [r.centroid(t), a.centroid(t)]
					}), l.style("visibility", function (t) {
						return t.endAngle - t.startAngle < 1e-4 ? "hidden" : "visible"
					})
				}

				function u() {
					return t.arc().outerRadius(p - R).innerRadius(L)
				}

				function c(t) {
					return S.hasFilter(S.cappedKeyAccessor(t.data))
				}

				function s(e) {
					e.innerRadius = L;
					var n = this._current;
					n = function (t) {
						return !t || isNaN(t.startAngle) || isNaN(t.endAngle)
					}(n) ? {
						startAngle: 0,
						endAngle: 0
					} : {
						startAngle: n.startAngle,
						endAngle: n.endAngle
					};
					var r = t.interpolate(n, e);
					return this._current = r(0),
						function (t) {
							return h(r(t), 0, u())
						}
				}

				function d(t, e) {
					return S.getColor(t.data, e)
				}

				function f(t, e) {
					v.attr("class") !== C && S.onClick(t.data, e)
				}

				function h(t, e, n) {
					var r = n(t, e);
					return r.indexOf("NaN") >= 0 && (r = "M0,0"), r
				}

				function g(e, n) {
					S.selectAll("g.pie-slice").each(function (r) {
						e.name === r.data.key && t.select(this).classed("highlight", n)
					})
				}
				var p, y, v, x, m, A, b = "pie-slice",
					_ = "pie-label",
					k = "pie-slice-group",
					D = "pie-label-group",
					C = "empty-chart",
					w = "empty",
					L = 0,
					R = 0,
					E = .5,
					M = !1,
					S = n.capMixin(n.colorMixin(n.baseMixin({})));
				return S.colorAccessor(S.cappedKeyAccessor), S.title(function (t) {
					return S.cappedKeyAccessor(t) + ": " + S.cappedValueAccessor(t)
				}), S.slicesCap = S.cap, S.label(S.cappedKeyAccessor), S.renderLabel(!0), S.transitionDuration(350), S.transitionDelay(0), S._doRender = function () {
					return S.resetSvg(), (v = S.svg().append("g").attr("transform", "translate(" + S.cx() + "," + S.cy() + ")")).append("g").attr("class", k), v.append("g").attr("class", D), i(), S
				}, S._applyLabelText = function (t) {
					t.text(function (t) {
						return ! function (t) {
							return 0 === S.cappedValueAccessor(t)
						}(t.data) && ! function (t) {
							var e = t.endAngle - t.startAngle;
							return isNaN(e) || e < E
						}(t) || c(t) ? S.label()(t.data) : ""
					})
				}, S.externalRadiusPadding = function (t) {
					return arguments.length ? (R = t, S) : R
				}, S.innerRadius = function (t) {
					return arguments.length ? (L = t, S) : L
				}, S.radius = function (t) {
					return arguments.length ? (y = t, S) : y
				}, S.cx = function (t) {
					return arguments.length ? (x = t, S) : x || S.width() / 2
				}, S.cy = function (t) {
					return arguments.length ? (m = t, S) : m || S.height() / 2
				}, S._doRedraw = function () {
					return i(), S
				}, S.minAngleForLabel = function (t) {
					return arguments.length ? (E = t, S) : E
				}, S.emptyTitle = function (t) {
					return 0 === arguments.length ? w : (w = t, S)
				}, S.externalLabels = function (t) {
					return 0 === arguments.length ? A : (A = t || void 0, S)
				}, S.drawPaths = function (t) {
					return 0 === arguments.length ? M : (M = t, S)
				}, S.legendables = function () {
					return S.data().map(function (t, e) {
						var n = {
							name: t.key,
							data: t.value,
							others: t.others,
							chart: S
						};
						return n.color = S.getColor(t, e), n
					})
				}, S.legendHighlight = function (t) {
					g(t, !0)
				}, S.legendReset = function (t) {
					g(t, !1)
				}, S.legendToggle = function (t) {
					S.onClick({
						key: t.name,
						others: t.others
					})
				}, S.anchor(e, r)
			}, n.sunburstChart = function (e, r) {
				function i(t) {
					return t.path ? t.value : w.cappedValueAccessor(t)
				}

				function a() {
					var e = t.min([w.width(), w.height()]) / 2;
					p = y && y < e ? y : e;
					var r, i = l();
					if (t.sum(w.data(), w.valueAccessor()) ? ((r = u(n.utils.toHierarchy(w.data(), w.valueAccessor()))).shift(), v.classed(_, !1)) : (r = u(n.utils.toHierarchy([], function (t) {
							return t.value
						})), v.classed(_, !0)), v) {
						var a = v.selectAll("g." + b).data(r);
						! function (t, e, r) {
							var i = function (t) {
								return t.enter().append("g").attr("class", function (t, e) {
									return b + " _" + e + " " + b + "-level-" + t.depth
								})
							}(t);
							(function (t, e) {
								var r = t.append("path").attr("fill", s).on("click", f).attr("d", function (t) {
										return h(e, t)
									}),
									i = n.transition(r, w.transitionDuration());
								i.attrTween && i.attrTween("d", c)
							})(i, e),
							function (t) {
								w.renderTitle() && t.append("title").text(function (t) {
									return w.title()(t)
								})
							}(i),
							function (t, e) {
								if (w.renderLabel()) {
									var n = v.selectAll("text." + b).data(t);
									n.exit().remove();
									var r = n.enter().append("text").attr("class", function (t, e) {
										var n = b + " _" + e;
										return A && (n += " external"), n
									}).on("click", f);
									o(r, e)
								}
							}(r, e)
						}(a, i, r),
						function (t, e) {
							(function (t, e) {
								var r = v.selectAll("g." + b).data(t).select("path").attr("d", function (t, n) {
										return h(e, t)
									}),
									i = n.transition(r, w.transitionDuration());
								i.attrTween && i.attrTween("d", c);
								i.attr("fill", s)
							})(t, e),
							function (t, e) {
								if (w.renderLabel()) {
									var n = v.selectAll("text." + b).data(t);
									o(n, e)
								}
							}(t, e),
							function (t) {
								w.renderTitle() && v.selectAll("g." + b).data(t).select("title").text(function (t) {
									return w.title()(t)
								})
							}(t)
						}(r, i),
						function (t) {
							t.exit().remove()
						}(a), w.hasFilter() ? w.selectAll("g." + b).each(function (t) {
							! function (t) {
								return function (t) {
									for (var e = 0; e < w.filters().length; e++) {
										var n = w.filters()[e];
										if (n.isFiltered(t)) return !0
									}
									return !1
								}(t.path)
							}(t) ? w.fadeDeselected(this): w.highlightSelected(this)
						}) : w.selectAll("g." + b).each(function (t) {
							w.resetHighlight(this)
						}), n.transition(v, w.transitionDuration(), w.transitionDelay()).attr("transform", "translate(" + w.cx() + "," + w.cy() + ")")
					}
				}

				function o(e, r) {
					n.transition(e, w.transitionDuration()).attr("transform", function (e) {
						return function (e, n) {
							var r;
							r = A ? t.svg.arc().outerRadius(p + A).innerRadius(p + A).centroid(e) : n.centroid(e);
							return isNaN(r[0]) || isNaN(r[1]) ? "translate(0,0)" : "translate(" + r + ")"
						}(e, r)
					}).attr("text-anchor", "middle").text(function (t) {
						return function (t) {
							return 0 === i(t)
						}(t) || function (t) {
							var e = t.x1 - t.x0;
							return isNaN(e) || e < C
						}(t) ? "" : w.label()(t)
					})
				}

				function l() {
					return t.arc().startAngle(function (t) {
						return t.x0
					}).endAngle(function (t) {
						return t.x1
					}).innerRadius(function (t) {
						return t.data.path && 1 === t.data.path.length ? D : Math.sqrt(t.y0)
					}).outerRadius(function (t) {
						return Math.sqrt(t.y1)
					})
				}

				function u(e) {
					var n = t.hierarchy(e).sum(function (t) {
						return t.children ? 0 : i(t)
					}).sort(function (e, n) {
						return t.ascending(e.data.path, n.data.path)
					});
					t.partition().size([2 * Math.PI, p * p])(n);
					return n.descendants().map(function (t) {
						return t.key = t.data.key, t.path = t.data.path, t
					})
				}

				function c(e) {
					var n = this._current;
					(function (t) {
						return !t || isNaN(t.x0) || isNaN(t.y0)
					})(n) && (n = {
						x0: 0,
						x1: 0,
						y0: 0,
						y1: 0
					});
					var r = {
							x0: e.x0,
							x1: e.x1,
							y0: e.y0,
							y1: e.y1
						},
						i = t.interpolate(n, r);
					return this._current = i(0),
						function (t) {
							return h(l(), Object.assign({}, e, i(t)))
						}
				}

				function s(t, e) {
					return w.getColor(t.data, e)
				}

				function d(t) {
					for (var e = t.path || t.key, r = n.filters.HierarchyFilter(e), i = function (t) {
							for (var e = n.filters.HierarchyFilter(t), r = [], i = 0; i < w.filters().length; i++) {
								var a = w.filters()[i];
								(a.isFiltered(t) || e.isFiltered(a)) && r.push(a)
							}
							return r
						}(e), a = !1, o = i.length - 1; o >= 0; o--) {
						var l = i[o];
						n.utils.arraysIdentical(l, e) && (a = !0), w.filter(i[o])
					}
					n.events.trigger(function () {
						a || w.filter(r), w.redrawGroup()
					})
				}

				function f(t, e) {
					v.attr("class") !== _ && d(t)
				}

				function h(t, e) {
					var n = t(e);
					return n.indexOf("NaN") >= 0 && (n = "M0,0"), n
				}

				function g(e, n) {
					w.selectAll("g.pie-slice").each(function (r) {
						e.name === r.key && t.select(this).classed("highlight", n)
					})
				}
				var p, y, v, x, m, A, b = "pie-slice",
					_ = "empty-chart",
					k = "empty",
					D = 0,
					C = .5,
					w = n.capMixin(n.colorMixin(n.baseMixin({})));
				return w.colorAccessor(w.cappedKeyAccessor), w.title(function (t) {
					return w.cappedKeyAccessor(t) + ": " + i(t)
				}), w.label(w.cappedKeyAccessor), w.renderLabel(!0), w.transitionDuration(350), w.filterHandler(function (t, e) {
					return 0 === e.length ? t.filter(null) : t.filterFunction(function (t) {
						for (var n = 0; n < e.length; n++) {
							var r = e[n];
							if (r.isFiltered && r.isFiltered(t)) return !0
						}
						return !1
					}), e
				}), w._doRender = function () {
					return w.resetSvg(), v = w.svg().append("g").attr("transform", "translate(" + w.cx() + "," + w.cy() + ")"), a(), w
				}, w.innerRadius = function (t) {
					return arguments.length ? (D = t, w) : D
				}, w.radius = function (t) {
					return arguments.length ? (y = t, w) : y
				}, w.cx = function (t) {
					return arguments.length ? (x = t, w) : x || w.width() / 2
				}, w.cy = function (t) {
					return arguments.length ? (m = t, w) : m || w.height() / 2
				}, w.minAngleForLabel = function (t) {
					return arguments.length ? (C = t, w) : C
				}, w.emptyTitle = function (t) {
					return 0 === arguments.length ? k : (k = t, w)
				}, w.externalLabels = function (t) {
					return 0 === arguments.length ? A : (A = t || void 0, w)
				}, w._doRedraw = function () {
					return a(), w
				}, w.onClick = f, w.legendables = function () {
					return w.data().map(function (t, e) {
						var n = {
							name: t.key,
							data: t.value,
							others: t.others,
							chart: w
						};
						return n.color = w.getColor(t, e), n
					})
				}, w.legendHighlight = function (t) {
					g(t, !0)
				}, w.legendReset = function (t) {
					g(t, !1)
				}, w.legendToggle = function (t) {
					w.onClick({
						key: t.name,
						others: t.others
					})
				}, w.anchor(e, r)
			}, n.barChart = function (e, r) {
				function i(t) {
					return n.utils.safeNumber(Math.abs(f.y()(t.y + t.y0) - f.y()(t.y0)))
				}

				function a(t) {
					var e = f.x()(t.x);
					return g || (e += c / 2), f.isOrdinal() && void 0 !== h && (e += h / 2), n.utils.safeNumber(e)
				}

				function o(t) {
					var e = f.y()(t.y + t.y0);
					return t.y < 0 && (e -= i(t)), n.utils.safeNumber(e - d)
				}

				function l(t) {
					var e = f.x()(t.x);
					return g && (e -= c / 2), f.isOrdinal() && void 0 !== h && (e += h / 2), n.utils.safeNumber(e)
				}

				function u(e, n) {
					return function () {
						var r = t.select(this).attr("fill") === e;
						return n ? !r : r
					}
				}
				var c, s = 1,
					d = 3,
					f = n.stackMixin(n.coordinateGridMixin({})),
					h = 2,
					g = !1,
					p = !1;
				return n.override(f, "rescale", function () {
					return f._rescale(), c = void 0, f
				}), n.override(f, "render", function () {
					return f.round() && g && !p && n.logger.warn("By default, brush rounding is disabled if bars are centered. See dc.js bar chart API documentation for details."), f._render()
				}), f.label(function (t) {
					return n.utils.printSingleValue(t.y0 + t.y)
				}, !1), f.plotData = function () {
					var e = f.chartBodyG().selectAll("g.stack").data(f.data());
					! function () {
						if (void 0 === c) {
							var t = f.xUnitCount();
							((c = f.isOrdinal() && void 0 === h ? Math.floor(f.x().bandwidth()) : h ? Math.floor((f.xAxisLength() - (t - 1) * h) / t) : Math.floor(f.xAxisLength() / (1 + f.barPadding()) / t)) === 1 / 0 || isNaN(c) || c < s) && (c = s)
						}
					}();
					var r = (e = e.enter().append("g").attr("class", function (t, e) {
						return "stack _" + e
					}).merge(e)).size() - 1;
					e.each(function (e, u) {
						var s = t.select(this);
						! function (t, e, r) {
							var a = t.selectAll("rect.bar").data(r.values, n.pluck("x")),
								o = a.enter().append("rect").attr("class", "bar").attr("fill", n.pluck("data", f.getColor)).attr("x", l).attr("y", f.yAxisHeight()).attr("height", 0),
								u = o.merge(a);
							f.renderTitle() && o.append("title").text(n.pluck("data", f.title(r.name))), f.isOrdinal() && u.on("click", f.onClick), n.transition(u, f.transitionDuration(), f.transitionDelay()).attr("x", l).attr("y", function (t) {
								var e = f.y()(t.y + t.y0);
								return t.y < 0 && (e -= i(t)), n.utils.safeNumber(e)
							}).attr("width", c).attr("height", function (t) {
								return i(t)
							}).attr("fill", n.pluck("data", f.getColor)).select("title").text(n.pluck("data", f.title(r.name))), n.transition(a.exit(), f.transitionDuration(), f.transitionDelay()).attr("x", function (t) {
								return f.x()(t.x)
							}).attr("width", .9 * c).remove()
						}(s, 0, e), f.renderLabel() && r === u && function (t, e, r) {
							var i = t.selectAll("text.barLabel").data(r.values, n.pluck("x")),
								l = i.enter().append("text").attr("class", "barLabel").attr("text-anchor", "middle").attr("x", a).attr("y", o).merge(i);
							f.isOrdinal() && (l.on("click", f.onClick), l.attr("cursor", "pointer")), n.transition(l, f.transitionDuration(), f.transitionDelay()).attr("x", a).attr("y", o).text(function (t) {
								return f.label()(t)
							}), n.transition(i.exit(), f.transitionDuration(), f.transitionDelay()).attr("height", 0).remove()
						}(s, 0, e)
					})
				}, f.fadeDeselectedArea = function (t) {
					var e = f.chartBodyG().selectAll("rect.bar");
					if (f.isOrdinal()) f.hasFilter() ? (e.classed(n.constants.SELECTED_CLASS, function (t) {
						return f.hasFilter(t.x)
					}), e.classed(n.constants.DESELECTED_CLASS, function (t) {
						return !f.hasFilter(t.x)
					})) : (e.classed(n.constants.SELECTED_CLASS, !1), e.classed(n.constants.DESELECTED_CLASS, !1));
					else if (f.brushOn() || f.parentBrushOn())
						if (f.brushIsEmpty(t)) e.classed(n.constants.DESELECTED_CLASS, !1);
						else {
							var r = t[0],
								i = t[1];
							e.classed(n.constants.DESELECTED_CLASS, function (t) {
								return t.x < r || t.x >= i
							})
						}
				}, f.centerBar = function (t) {
					return arguments.length ? (g = t, f) : g
				}, n.override(f, "onClick", function (t) {
					f._onClick(t.data)
				}), f.barPadding = function (t) {
					return arguments.length ? (f._rangeBandPadding(t), h = void 0, f) : f._rangeBandPadding()
				}, f._useOuterPadding = function () {
					return void 0 === h
				}, f.outerPadding = f._outerRangeBandPadding, f.gap = function (t) {
					return arguments.length ? (h = t, f) : h
				}, f.extendBrush = function (t) {
					return t && f.round() && (!g || p) && (t[0] = f.round()(t[0]), t[1] = f.round()(t[1])), t
				}, f.alwaysUseRounding = function (t) {
					return arguments.length ? (p = t, f) : p
				}, f.legendHighlight = function (t) {
					f.isLegendableHidden(t) || f.g().selectAll("rect.bar").classed("highlight", u(t.color)).classed("fadeout", u(t.color, !0))
				}, f.legendReset = function () {
					f.g().selectAll("rect.bar").classed("highlight", !1).classed("fadeout", !1)
				}, n.override(f, "xAxisMax", function () {
					var t = this._xAxisMax();
					if ("resolution" in f.xUnits()) {
						t += f.xUnits().resolution
					}
					return t
				}), f.anchor(e, r)
			}, n.lineChart = function (e, r) {
				function i(t, e) {
					return x.getColor.call(t, t.values, e)
				}

				function a() {
					var e = null;
					if (D) return D;
					if ("function" == typeof C) e = C;
					else {
						e = {
							linear: t.curveLinear,
							"linear-closed": t.curveLinearClosed,
							step: t.curveStep,
							"step-before": t.curveStepBefore,
							"step-after": t.curveStepAfter,
							basis: t.curveBasis,
							"basis-open": t.curveBasisOpen,
							"basis-closed": t.curveBasisClosed,
							bundle: t.curveBundle,
							cardinal: t.curveCardinal,
							"cardinal-open": t.curveCardinalOpen,
							"cardinal-closed": t.curveCardinalClosed,
							monotone: t.curveMonotoneX
						}[C]
					}
					return e || (e = t.curveLinear), null !== w && ("function" != typeof e.tension ? n.logger.warn("tension was specified but the curve/interpolate does not support it.") : e = e.tension(w)), e
				}

				function o(t) {
					return !t || t.indexOf("NaN") >= 0 ? "M0,0" : t
				}

				function l(e, r) {
					if ("always" === x.xyTipsOn() || !x.brushOn() && !x.parentBrushOn() && x.xyTipsOn()) {
						var i = h + "-list",
							a = e.select("g." + i);
						a.empty() && (a = e.append("g").attr("class", i)), r.each(function (e, r) {
							var i = e.values;
							d && (i = i.filter(d));
							var o = a.select("g." + h + "._" + r);
							o.empty() && (o = a.append("g").attr("class", h + " _" + r)),
								function (t) {
									(t.select("path." + p).empty() ? t.append("path").attr("class", p) : t.select("path." + p)).style("display", "none").attr("stroke-dasharray", "5,5");
									(t.select("path." + y).empty() ? t.append("path").attr("class", y) : t.select("path." + y)).style("display", "none").attr("stroke-dasharray", "5,5")
								}(o);
							var l = o.selectAll("circle." + g).data(i, n.pluck("x")),
								s = l.enter().append("circle").attr("class", g).attr("cx", function (t) {
									return n.utils.safeNumber(x.x()(t.x))
								}).attr("cy", function (t) {
									return n.utils.safeNumber(x.y()(t.y + t.y0))
								}).attr("r", u()).style("fill-opacity", _).style("stroke-opacity", k).attr("fill", x.getColor).attr("stroke", x.getColor).on("mousemove", function () {
									var e = t.select(this);
									! function (t) {
										t.style("fill-opacity", .8), t.style("stroke-opacity", .8), t.attr("r", A)
									}(e),
									function (t, e) {
										var n = t.attr("cx"),
											r = t.attr("cy"),
											i = "M" + (x._yAxisX() - x.margins().left) + " " + r + "L" + n + " " + r,
											a = "M" + n + " " + x.yAxisHeight() + "L" + n + " " + r;
										e.select("path." + p).style("display", "").attr("d", i), e.select("path." + y).style("display", "").attr("d", a)
									}(e, o)
								}).on("mouseout", function () {
									! function (t) {
										t.style("fill-opacity", _).style("stroke-opacity", k).attr("r", u())
									}(t.select(this)),
									function (t) {
										t.select("path." + p).style("display", "none"), t.select("path." + y).style("display", "none")
									}(o)
								}).merge(l);
							s.call(c, e), n.transition(s, x.transitionDuration()).attr("cx", function (t) {
								return n.utils.safeNumber(x.x()(t.x))
							}).attr("cy", function (t) {
								return n.utils.safeNumber(x.y()(t.y + t.y0))
							}).attr("fill", x.getColor), l.exit().remove()
						})
					}
				}

				function u() {
					return b || A
				}

				function c(t, e) {
					x.renderTitle() && (t.select("title").remove(), t.append("title").text(n.pluck("data", x.title(e.name))))
				}

				function s(e, n, r) {
					return function () {
						var i = t.select(this),
							a = i.attr("stroke") === e && i.attr("stroke-dasharray") === (n instanceof Array ? n.join(",") : null) || i.attr("fill") === e;
						return r ? !a : a
					}
				}
				var d, f, h = "dc-tooltip",
					g = "dot",
					p = "yRef",
					y = "xRef",
					v = 3,
					x = n.stackMixin(n.coordinateGridMixin({})),
					m = !1,
					A = 5,
					b = null,
					_ = 1e-6,
					k = 1e-6,
					D = null,
					C = null,
					w = null,
					L = !0;
				return x.transitionDuration(500), x.transitionDelay(0), x._rangeBandPadding(1), x.plotData = function () {
					var e = x.chartBodyG(),
						r = e.select("g.stack-list");
					r.empty() && (r = e.append("g").attr("class", "stack-list"));
					var u = r.selectAll("g.stack").data(x.data()),
						c = u.enter().append("g").attr("class", function (t, e) {
							return "stack _" + e
						});
					(function (e, r) {
						var l = t.line().x(function (t) {
							return x.x()(t.x)
						}).y(function (t) {
							return x.y()(t.y + t.y0)
						}).curve(a());
						d && l.defined(d);
						var u = e.append("path").attr("class", "line").attr("stroke", i);
						f && u.attr("stroke-dasharray", f), n.transition(r.select("path.line"), x.transitionDuration(), x.transitionDelay()).attr("stroke", i).attr("d", function (t) {
							return o(l(t.values))
						})
					})(c, u = c.merge(u)),
					function (e, r) {
						if (m) {
							var l = t.area().x(function (t) {
								return x.x()(t.x)
							}).y1(function (t) {
								return x.y()(t.y + t.y0)
							}).y0(function (t) {
								return x.y()(t.y0)
							}).curve(a());
							d && l.defined(d), e.append("path").attr("class", "area").attr("fill", i).attr("d", function (t) {
								return o(l(t.values))
							}), n.transition(r.select("path.area"), x.transitionDuration(), x.transitionDelay()).attr("fill", i).attr("d", function (t) {
								return o(l(t.values))
							})
						}
					}(c, u), l(e, u), x.renderLabel() && function (e) {
						e.each(function (e, r) {
							var i = t.select(this).selectAll("text.lineLabel").data(e.values, n.pluck("x")),
								a = i.enter().append("text").attr("class", "lineLabel").attr("text-anchor", "middle").merge(i);
							n.transition(a, x.transitionDuration()).attr("x", function (t) {
								return n.utils.safeNumber(x.x()(t.x))
							}).attr("y", function (t) {
								var e = x.y()(t.y + t.y0) - v;
								return n.utils.safeNumber(e)
							}).text(function (t) {
								return x.label()(t)
							}), n.transition(i.exit(), x.transitionDuration()).attr("height", 0).remove()
						})
					}(u)
				}, x.curve = function (t) {
					return arguments.length ? (D = t, x) : D
				}, x.interpolate = n.logger.deprecate(function (t) {
					return arguments.length ? (C = t, x) : C
				}, "dc.lineChart.interpolate has been deprecated since version 3.0 use dc.lineChart.curve instead"), x.tension = n.logger.deprecate(function (t) {
					return arguments.length ? (w = t, x) : w
				}, "dc.lineChart.tension has been deprecated since version 3.0 use dc.lineChart.curve instead"), x.defined = function (t) {
					return arguments.length ? (d = t, x) : d
				}, x.dashStyle = function (t) {
					return arguments.length ? (f = t, x) : f
				}, x.renderArea = function (t) {
					return arguments.length ? (m = t, x) : m
				}, x.label(function (t) {
					return n.utils.printSingleValue(t.y0 + t.y)
				}, !1), x.xyTipsOn = function (t) {
					return arguments.length ? (L = t, x) : L
				}, x.dotRadius = function (t) {
					return arguments.length ? (A = t, x) : A
				}, x.renderDataPoints = function (t) {
					return arguments.length ? (t ? (_ = t.fillOpacity || .8, k = t.strokeOpacity || 0, b = t.radius || 2) : (_ = 1e-6, k = 1e-6, b = null), x) : {
						fillOpacity: _,
						strokeOpacity: k,
						radius: b
					}
				}, x.legendHighlight = function (t) {
					x.isLegendableHidden(t) || x.g().selectAll("path.line, path.area").classed("highlight", s(t.color, t.dashstyle)).classed("fadeout", s(t.color, t.dashstyle, !0))
				}, x.legendReset = function () {
					x.g().selectAll("path.line, path.area").classed("highlight", !1).classed("fadeout", !1)
				}, n.override(x, "legendables", function () {
					var t = x._legendables();
					return f ? t.map(function (t) {
						return t.dashstyle = f, t
					}) : t
				}), x.anchor(e, r)
			}, n.dataCount = function (e, r) {
				var i = t.format(",d"),
					a = n.baseMixin({}),
					o = {
						some: "",
						all: ""
					};
				return a.html = function (t) {
					return arguments.length ? (t.all && (o.all = t.all), t.some && (o.some = t.some), a) : o
				}, a.formatNumber = function (t) {
					return arguments.length ? (i = t, a) : i
				}, a._doRender = function () {
					var t = a.dimension().size(),
						e = a.group().value(),
						n = i(t),
						r = i(e);
					return t === e && "" !== o.all ? a.root().html(o.all.replace("%total-count", n).replace("%filter-count", r)) : "" !== o.some ? a.root().html(o.some.replace("%total-count", n).replace("%filter-count", r)) : (a.selectAll(".total-count").text(n), a.selectAll(".filter-count").text(r)), a
				}, a._doRedraw = function () {
					return a._doRender()
				}, a.anchor(e, r)
			}, n.dataTable = function (e, r) {
				function i() {
					var e = !0;
					if (h.forEach(function (t) {
							e &= "function" == typeof t
						}), !e) {
						var n = d.selectAll("thead").data([0]);
						n.exit().remove();
						var r = (n = n.enter().append("thead").merge(n)).selectAll("tr").data([0]);
						r.exit().remove();
						var i = (r = r.enter().append("tr").merge(r)).selectAll("th").data(h);
						i.exit().remove(), i.enter().append("th").merge(i).attr("class", s).html(function (t) {
							return d._doColumnHeaderFormat(t)
						})
					}
					var l = d.root().selectAll("tbody").data(function () {
							var e;
							e = p === t.ascending ? d.dimension().bottom(f) : d.dimension().top(f);
							return t.nest().key(d.group()).sortKeys(p).entries(e.sort(function (t, e) {
								return p(g(t), g(e))
							}).slice(y, a))
						}(), function (t) {
							return d.keyAccessor()(t)
						}),
						u = l.enter().append("tbody");
					return !0 === v && u.append("tr").attr("class", c).append("td").attr("class", o).attr("colspan", h.length).html(function (t) {
						return d.keyAccessor()(t)
					}), l.exit().remove(), u
				}
				var a, o = "dc-table-label",
					l = "dc-table-row",
					u = "dc-table-column",
					c = "dc-table-group",
					s = "dc-table-head",
					d = n.baseMixin({}),
					f = 25,
					h = [],
					g = function (t) {
						return t
					},
					p = t.ascending,
					y = 0,
					v = !0;
				return d._doRender = function () {
					return d.selectAll("tbody").remove(),
						function (t) {
							var e = t.order().selectAll("tr." + l).data(function (t) {
									return t.values
								}),
								n = e.enter().append("tr").attr("class", l);
							h.forEach(function (t, e) {
								n.append("td").attr("class", u + " _" + e).html(function (e) {
									return d._doColumnValueFormat(t, e)
								})
							}), e.exit().remove()
						}(i()), d
				}, d._doColumnValueFormat = function (t, e) {
					return "function" == typeof t ? t(e) : "string" == typeof t ? e[t] : t.format(e)
				}, d._doColumnHeaderFormat = function (t) {
					return "function" == typeof t ? d._doColumnHeaderFnToString(t) : "string" == typeof t ? d._doColumnHeaderCapitalize(t) : String(t.label)
				}, d._doColumnHeaderCapitalize = function (t) {
					return t.charAt(0).toUpperCase() + t.slice(1)
				}, d._doColumnHeaderFnToString = function (t) {
					var e = String(t),
						n = e.indexOf("return ");
					if (n >= 0) {
						var r = e.lastIndexOf(";");
						if (r >= 0) {
							(e = e.substring(n + 7, r)).indexOf("numberFormat") >= 0 && (e = e.replace("numberFormat", ""))
						}
					}
					return e
				}, d._doRedraw = function () {
					return d._doRender()
				}, d.size = function (t) {
					return arguments.length ? (f = t, d) : f
				}, d.beginSlice = function (t) {
					return arguments.length ? (y = t, d) : y
				}, d.endSlice = function (t) {
					return arguments.length ? (a = t, d) : a
				}, d.columns = function (t) {
					return arguments.length ? (h = t, d) : h
				}, d.sortBy = function (t) {
					return arguments.length ? (g = t, d) : g
				}, d.order = function (t) {
					return arguments.length ? (p = t, d) : p
				}, d.showGroups = function (t) {
					return arguments.length ? (v = t, d) : v
				}, d.anchor(e, r)
			}, n.dataGrid = function (e, r) {
				function i() {
					var e = u.root().selectAll("div." + l).data(function () {
							var e = u.dimension().top(c);
							return t.nest().key(u.group()).sortKeys(f).entries(e.sort(function (t, e) {
								return f(d(t), d(e))
							}).slice(h, a))
						}(), function (t) {
							return u.keyAccessor()(t)
						}),
						n = e.enter().append("div").attr("class", l);
					return g && n.html(function (t) {
						return g(t)
					}), e.exit().remove(), n
				}
				var a, o = "dc-grid-item",
					l = "dc-grid-top",
					u = n.baseMixin({}),
					c = 999,
					s = function (t) {
						return "you need to provide an html() handling param:  " + JSON.stringify(t)
					},
					d = function (t) {
						return t
					},
					f = t.ascending,
					h = 0,
					g = function (t) {
						return "<div class='dc-grid-group'><h1 class='dc-grid-label'>" + u.keyAccessor()(t) + "</h1></div>"
					};
				return u._doRender = function () {
					return u.selectAll("div." + l).remove(),
						function (t) {
							var e = t.order().selectAll("div." + o).data(function (t) {
								return t.values
							});
							e.exit().remove(), e = e.enter().append("div").attr("class", o).html(function (t) {
								return s(t)
							}).merge(e)
						}(i()), u
				}, u._doRedraw = function () {
					return u._doRender()
				}, u.beginSlice = function (t) {
					return arguments.length ? (h = t, u) : h
				}, u.endSlice = function (t) {
					return arguments.length ? (a = t, u) : a
				}, u.size = function (t) {
					return arguments.length ? (c = t, u) : c
				}, u.html = function (t) {
					return arguments.length ? (s = t, u) : s
				}, u.htmlGroup = function (t) {
					return arguments.length ? (g = t, u) : g
				}, u.sortBy = function (t) {
					return arguments.length ? (d = t, u) : d
				}, u.order = function (t) {
					return arguments.length ? (f = t, u) : f
				}, u.anchor(e, r)
			}, n.bubbleChart = function (t, e) {
				var r = n.bubbleMixin(n.coordinateGridMixin({}));
				r.transitionDuration(750), r.transitionDelay(0);
				var i = function (t) {
					return "translate(" + function (t) {
						var e = r.x()(r.keyAccessor()(t));
						return !isNaN(e) && isFinite(e) || (e = 0), e
					}(t) + "," + function (t) {
						var e = r.y()(r.valueAccessor()(t));
						return !isNaN(e) && isFinite(e) || (e = 0), e
					}(t) + ")"
				};
				return r.plotData = function () {
					r.calculateRadiusDomain(), r.r().range([r.MIN_RADIUS, r.xAxisLength() * r.maxBubbleRelativeSize()]);
					var t = r.data(),
						e = r.chartBodyG().selectAll("g." + r.BUBBLE_NODE_CLASS).data(t, function (t) {
							return t.key
						});
					r.sortBubbleSize() && e.order(),
						function (t) {
							t.exit().remove()
						}(e),
						function (t) {
							n.transition(t, r.transitionDuration(), r.transitionDelay()).attr("transform", i).select("circle." + r.BUBBLE_CLASS).attr("fill", r.getColor).attr("r", function (t) {
								return r.bubbleR(t)
							}).attr("opacity", function (t) {
								return r.bubbleR(t) > 0 ? 1 : 0
							}), r.doUpdateLabels(t), r.doUpdateTitles(t)
						}(e = function (t) {
							var e = t.enter().append("g");
							return e.attr("class", r.BUBBLE_NODE_CLASS).attr("transform", i).append("circle").attr("class", function (t, e) {
								return r.BUBBLE_CLASS + " _" + e
							}).on("click", r.onClick).attr("fill", r.getColor).attr("r", 0), t = e.merge(t), n.transition(t, r.transitionDuration(), r.transitionDelay()).select("circle." + r.BUBBLE_CLASS).attr("r", function (t) {
								return r.bubbleR(t)
							}).attr("opacity", function (t) {
								return r.bubbleR(t) > 0 ? 1 : 0
							}), r._doRenderLabel(e), r._doRenderTitles(e), t
						}(e)), r.fadeDeselectedArea(r.filter())
				}, r.renderBrush = function () {}, r.redrawBrush = function (t, e) {
					r.fadeDeselectedArea(t)
				}, r.anchor(t, e)
			}, n.compositeChart = function (e, r) {
				function i(e, r) {
					var i, a, s, d, h;
					return e && (i = t.min(u(o())), a = n.utils.add(t.max(c(o())), f.yAxisPadding())), r && (s = t.min(u(l())), d = n.utils.add(t.max(c(l())), f.yAxisPadding())), f.alignYAxes() && e && r && (h = function (t, e, n, r) {
						var i = (r - n) / (e - t);
						return {
							lyAxisMin: Math.min(t, n / i),
							lyAxisMax: Math.max(e, r / i),
							ryAxisMin: Math.min(n, t * i),
							ryAxisMax: Math.max(r, e * i)
						}
					}(i, a, s, d)), h || {
						lyAxisMin: i,
						lyAxisMax: a,
						ryAxisMin: s,
						ryAxisMax: d
					}
				}

				function a(t, e) {
					t._generateG(f.g()), t.g().attr("class", d + " _" + e)
				}

				function o() {
					return h.filter(function (t) {
						return !t.useRightYAxis()
					})
				}

				function l() {
					return h.filter(function (t) {
						return t.useRightYAxis()
					})
				}

				function u(t) {
					return t.map(function (t) {
						return t.yAxisMin()
					})
				}

				function c(t) {
					return t.map(function (t) {
						return t.yAxisMax()
					})
				}
				var s, d = "sub",
					f = n.coordinateGridMixin({}),
					h = [],
					g = {},
					p = !1,
					y = !0,
					v = !1,
					x = t.axisRight(),
					m = 0,
					A = 12,
					b = !1;
				return f._mandatoryAttributes([]), f.transitionDuration(500), f.transitionDelay(0), n.override(f, "_generateG", function () {
					for (var t = this.__generateG(), e = 0; e < h.length; ++e) {
						var n = h[e];
						a(n, e), n.dimension() || n.dimension(f.dimension()), n.group() || n.group(f.group()), n.chartGroup(f.chartGroup()), n.svg(f.svg()), n.xUnits(f.xUnits()), n.transitionDuration(f.transitionDuration(), f.transitionDelay()), n.parentBrushOn(f.brushOn()), n.brushOn(!1), n.renderTitle(f.renderTitle()), n.elasticX(f.elasticX())
					}
					return t
				}), f.on("filtered.dcjs-composite-chart", function (t) {
					for (var e = 0; e < h.length; ++e) h[e].replaceFilter(f.filter())
				}), f._prepareYAxis = function () {
					var e = 0 !== o().length,
						n = 0 !== l().length,
						r = i(e, n);
					e && function (e) {
						var n = void 0 === f.y() || f.elasticY(),
							r = n || f.resizing();
						void 0 === f.y() && f.y(t.scaleLinear()), n && f.y().domain([e.lyAxisMin, e.lyAxisMax]), r && f.y().rangeRound([f.yAxisHeight(), 0]), f.y().range([f.yAxisHeight(), 0]), f.yAxis(f.yAxis().scale(f.y()))
					}(r), n && function (e) {
						var n = void 0 === f.rightY() || f.elasticY(),
							r = n || f.resizing();
						void 0 === f.rightY() && f.rightY(t.scaleLinear()), n && f.rightY().domain([e.ryAxisMin, e.ryAxisMax]), r && f.rightY().rangeRound([f.yAxisHeight(), 0]), f.rightY().range([f.yAxisHeight(), 0]), f.rightYAxis(f.rightYAxis().scale(f.rightY()))
					}(r), o().length > 0 && !b ? f._renderHorizontalGridLinesForAxis(f.g(), f.y(), f.yAxis()) : l().length > 0 && f._renderHorizontalGridLinesForAxis(f.g(), s, x)
				}, f.renderYAxis = function () {
					0 !== o().length && (f.renderYAxisAt("y", f.yAxis(), f.margins().left), f.renderYAxisLabel("y", f.yAxisLabel(), -90)), 0 !== l().length && (f.renderYAxisAt("yr", f.rightYAxis(), f.width() - f.margins().right), f.renderYAxisLabel("yr", f.rightYAxisLabel(), 90, f.width() - A))
				}, f.plotData = function () {
					for (var t = 0; t < h.length; ++t) {
						var e = h[t];
						e.g() || a(e, t), p && e.colors(f.colors()), e.x(f.x()), e.xAxis(f.xAxis()), e.useRightYAxis() ? (e.y(f.rightY()), e.yAxis(f.rightYAxis())) : (e.y(f.y()), e.yAxis(f.yAxis())), e.plotData(), e._activateRenderlets()
					}
				}, f.useRightAxisGridLines = function (t) {
					return arguments ? (b = t, f) : b
				}, f.childOptions = function (t) {
					return arguments.length ? (g = t, h.forEach(function (t) {
						t.options(g)
					}), f) : g
				}, f.fadeDeselectedArea = function (t) {
					if (f.brushOn())
						for (var e = 0; e < h.length; ++e) {
							h[e].fadeDeselectedArea(t)
						}
				}, f.rightYAxisLabel = function (t, e) {
					return arguments.length ? (m = t, f.margins().right -= A, A = void 0 === e ? 12 : e, f.margins().right += A, f) : m
				}, f.compose = function (t) {
					return (h = t).forEach(function (t) {
						t.height(f.height()), t.width(f.width()), t.margins(f.margins()), y && t.title(f.title()), t.options(g)
					}), f
				}, f.children = function () {
					return h
				}, f.shareColors = function (t) {
					return arguments.length ? (p = t, f) : p
				}, f.shareTitle = function (t) {
					return arguments.length ? (y = t, f) : y
				}, f.rightY = function (t) {
					return arguments.length ? (s = t, f.rescale(), f) : s
				}, f.alignYAxes = function (t) {
					return arguments.length ? (v = t, f.rescale(), f) : v
				}, delete f.yAxisMin, delete f.yAxisMax, n.override(f, "xAxisMin", function () {
					return n.utils.subtract(t.min(h.map(function (t) {
						return t.xAxisMin()
					})), f.xAxisPadding(), f.xAxisPaddingUnit())
				}), n.override(f, "xAxisMax", function () {
					return n.utils.add(t.max(h.map(function (t) {
						return t.xAxisMax()
					})), f.xAxisPadding(), f.xAxisPaddingUnit())
				}), f.legendables = function () {
					return h.reduce(function (t, e) {
						return p && e.colors(f.colors()), t.push.apply(t, e.legendables()), t
					}, [])
				}, f.legendHighlight = function (t) {
					for (var e = 0; e < h.length; ++e) {
						h[e].legendHighlight(t)
					}
				}, f.legendReset = function (t) {
					for (var e = 0; e < h.length; ++e) {
						h[e].legendReset(t)
					}
				}, f.legendToggle = function () {
					console.log("composite should not be getting legendToggle itself")
				}, f.rightYAxis = function (t) {
					return arguments.length ? (x = t, f) : x
				}, f.anchor(e, r)
			}, n.seriesChart = function (e, r) {
				function i(t) {
					u[t].g() && u[t].g().remove(), delete u[t]
				}

				function a() {
					Object.keys(u).map(i), u = {}
				}
				var o, l = n.compositeChart(e, r),
					u = {},
					c = n.lineChart,
					s = t.ascending,
					d = function (e, n) {
						return t.ascending(l.keyAccessor()(e), l.keyAccessor()(n))
					};
				return l._mandatoryAttributes().push("seriesAccessor", "chart"), l.shareColors(!0), l._preprocessData = function () {
					var e, a = [],
						f = t.nest().key(o);
					s && f.sortKeys(s), d && f.sortValues(d);
					var h = f.entries(l.data()).map(function (t, i) {
						var o = u[t.key] || c.call(l, l, r, t.key, i);
						return u[t.key] || (e = !0), u[t.key] = o, a.push(t.key), o.dimension(l.dimension()).group({
							all: "function" == typeof t.values ? t.values : n.utils.constant(t.values)
						}, t.key).keyAccessor(l.keyAccessor()).valueAccessor(l.valueAccessor()).brushOn(!1)
					});
					Object.keys(u).filter(function (t) {
						return -1 === a.indexOf(t)
					}).forEach(function (t) {
						i(t), e = !0
					}), l._compose(h), e && l.legend() && l.legend().render()
				}, l.chart = function (t) {
					return arguments.length ? (c = t, a(), l) : c
				}, l.seriesAccessor = function (t) {
					return arguments.length ? (o = t, a(), l) : o
				}, l.seriesSort = function (t) {
					return arguments.length ? (s = t, a(), l) : s
				}, l.valueSort = function (t) {
					return arguments.length ? (d = t, a(), l) : d
				}, l._compose = l.compose, delete l.compose, l
			}, n.geoChoroplethChart = function (e, r) {
				function i(e) {
					var r = function () {
						for (var t = {}, e = c.data(), n = 0; n < e.length; ++n) t[c.keyAccessor()(e[n])] = c.valueAccessor()(e[n]);
						return t
					}();
					if (function (t) {
							return u(t).keyAccessor
						}(e)) {
						var i = function (t) {
							return c.svg().selectAll(function (t) {
								return "g.layer" + t + " g." + u(t).name
							}(t)).classed("selected", function (e) {
								return a(t, e)
							}).classed("deselected", function (e) {
								return o(t, e)
							}).attr("class", function (e) {
								var r = u(t).name,
									i = n.utils.nameToId(u(t).keyAccessor(e)),
									l = r + " " + i;
								return a(t, e) && (l += " selected"), o(t, e) && (l += " deselected"), l
							})
						}(e);
						! function (e, r, i) {
							var a = e.select("path").attr("fill", function () {
								var e = t.select(this).attr("fill");
								return e || "none"
							}).on("click", function (t) {
								return c.onClick(t, r)
							});
							n.transition(a, c.transitionDuration(), c.transitionDelay()).attr("fill", function (t, e) {
								return c.getColor(i[u(r).keyAccessor(t)], e)
							})
						}(i, e, r),
						function (t, e, n) {
							c.renderTitle() && t.selectAll("title").text(function (t) {
								var r = l(e, t),
									i = n[r];
								return c.title()({
									key: r,
									value: i
								})
							})
						}(i, e, r)
					}
				}

				function a(t, e) {
					return c.hasFilter() && c.hasFilter(l(t, e))
				}

				function o(t, e) {
					return c.hasFilter() && !c.hasFilter(l(t, e))
				}

				function l(t, e) {
					return u(t).keyAccessor(e)
				}

				function u(t) {
					return h[t]
				}
				var c = n.colorMixin(n.baseMixin({}));
				c.colorAccessor(function (t) {
					return t || 0
				});
				var s, d, f = t.geoPath(),
					h = [];
				c._doRender = function () {
					c.resetSvg();
					for (var t = 0; t < h.length; ++t) {
						var e = c.svg().append("g").attr("class", "layer" + t).selectAll("g." + u(t).name).data(u(t).data);
						(e = e.enter().append("g").attr("class", u(t).name).merge(e)).append("path").attr("fill", "white").attr("d", g()), e.append("title"), i(t)
					}
					s = !1
				}, c.onClick = function (t, e) {
					var r = u(e).keyAccessor(t);
					n.events.trigger(function () {
						c.filter(r), c.redrawGroup()
					})
				}, c._doRedraw = function () {
					for (var t = 0; t < h.length; ++t) i(t), s && c.svg().selectAll("g." + u(t).name + " path").attr("d", g());
					s = !1
				}, c.overlayGeoJson = function (t, e, n) {
					for (var r = 0; r < h.length; ++r)
						if (h[r].name === e) return h[r].data = t, h[r].keyAccessor = n, c;
					return h.push({
						name: e,
						data: t,
						keyAccessor: n
					}), c
				}, c.projection = function (t) {
					return arguments.length ? (d = t, s = !0, c) : d
				};
				var g = function () {
					return void 0 === d ? (n.logger.warn("choropleth projection default of geoAlbers is deprecated, in next version projection will need to be set explicitly"), f.projection(t.geoAlbersUsa())) : f.projection(d)
				};
				return c.geoJsons = function () {
					return h
				}, c.geoPath = function () {
					return f
				}, c.removeGeoJson = function (t) {
					for (var e = [], n = 0; n < h.length; ++n) {
						var r = h[n];
						r.name !== t && e.push(r)
					}
					return h = e, c
				}, c.anchor(e, r)
			}, n.bubbleOverlay = function (e, r) {
				function i() {
					var t = {};
					return s.data().forEach(function (e) {
						t[s.keyAccessor()(e)] = e
					}), t
				}

				function a(t, e) {
					var r = u + " " + n.utils.nameToId(t.name),
						i = o.select("g." + n.utils.nameToId(t.name));
					return i.empty() && (i = o.append("g").attr("class", r).attr("transform", "translate(" + t.x + "," + t.y + ")")), i.datum(e[t.name]), i
				}
				var o, l = "bubble-overlay",
					u = "node",
					c = "bubble",
					s = n.bubbleMixin(n.baseMixin({})),
					d = [];
				return s.transitionDuration(750), s.transitionDelay(0), s.radiusValueAccessor(function (t) {
					return t.value
				}), s.point = function (t, e, n) {
					return d.push({
						name: t,
						x: e,
						y: n
					}), s
				}, s._doRender = function () {
					return (o = s.select("g." + l)).empty() && (o = s.svg().append("g").attr("class", l)), o = o, s.r().range([s.MIN_RADIUS, s.width() * s.maxBubbleRelativeSize()]),
						function () {
							var t = i();
							s.calculateRadiusDomain(), d.forEach(function (e) {
								var r = a(e, t),
									i = r.select("circle." + c);
								i.empty() && (i = r.append("circle").attr("class", c).attr("r", 0).attr("fill", s.getColor).on("click", s.onClick)), n.transition(i, s.transitionDuration(), s.transitionDelay()).attr("r", function (t) {
									return s.bubbleR(t)
								}), s._doRenderLabel(r), s._doRenderTitles(r)
							})
						}(), s.fadeDeselectedArea(s.filter()), s
				}, s._doRedraw = function () {
					return function () {
						var t = i();
						s.calculateRadiusDomain(), d.forEach(function (e) {
							var r = a(e, t),
								i = r.select("circle." + c);
							n.transition(i, s.transitionDuration(), s.transitionDelay()).attr("r", function (t) {
								return s.bubbleR(t)
							}).attr("fill", s.getColor), s.doUpdateLabels(r), s.doUpdateTitles(r)
						})
					}(), s.fadeDeselectedArea(s.filter()), s
				}, s.debug = function (e) {
					if (e) {
						var r = s.select("g." + n.constants.DEBUG_GROUP_CLASS);
						r.empty() && (r = s.svg().append("g").attr("class", n.constants.DEBUG_GROUP_CLASS));
						var i = r.append("text").attr("x", 10).attr("y", 20);
						r.append("rect").attr("width", s.width()).attr("height", s.height()).on("mousemove", function () {
							var e = t.mouse(r.node()),
								n = e[0] + ", " + e[1];
							i.text(n)
						})
					} else s.selectAll(".debug").remove();
					return s
				}, s.anchor(e, r), s
			}, n.rowChart = function (e, r) {
				function i() {
					var e = s.select("g.axis");
					! function () {
						if (!d || f) {
							var e = t.extent(h, D.cappedValueAccessor);
							e[0] > 0 && (e[0] = 0), e[1] < 0 && (e[1] = 0), d = t.scaleLinear().domain(e).range([0, D.effectiveWidth()])
						}
						C.scale(d)
					}(), e.empty() && (e = s.append("g").attr("class", "axis")), e.attr("transform", "translate(0, " + D.effectiveHeight() + ")"), n.transition(e, D.transitionDuration(), D.transitionDelay()).call(C)
				}

				function a() {
					h = D.data(), i(), s.selectAll("g.tick").select("line.grid-line").remove(), s.selectAll("g.tick").append("line").attr("class", "grid-line").attr("x1", 0).attr("y1", 0).attr("x2", 0).attr("y2", function () {
						return -D.effectiveHeight()
					});
					var t = s.selectAll("g." + b).data(h);
					! function (t) {
						t.exit().remove()
					}(t),
					function (t) {
						var e, r = h.length;
						e = A || (D.effectiveHeight() - (r + 1) * m) / r;
						y || (p = e / 2);
						var i = t.attr("transform", function (t, n) {
							return "translate(0," + ((n + 1) * m + n * e) + ")"
						}).select("rect").attr("height", e).attr("fill", D.getColor).on("click", l).classed("deselected", function (t) {
							return !!D.hasFilter() && !c(t)
						}).classed("selected", function (t) {
							return !!D.hasFilter() && c(t)
						});
						n.transition(i, D.transitionDuration(), D.transitionDelay()).attr("width", function (t) {
								return Math.abs(o() - d(D.cappedValueAccessor(t)))
							}).attr("transform", u),
							function (t) {
								D.renderTitle() && (t.select("title").remove(), t.append("title").text(D.title()))
							}(t),
							function (t) {
								if (D.renderLabel()) {
									var e = t.select("text").attr("x", g).attr("y", p).attr("dy", v).on("click", l).attr("class", function (t, e) {
										return b + " _" + e
									}).text(function (t) {
										return D.label()(t)
									});
									n.transition(e, D.transitionDuration(), D.transitionDelay()).attr("transform", u)
								}
								if (D.renderTitleLabel()) {
									var r = t.select("." + _).attr("x", D.effectiveWidth() - x).attr("y", p).attr("dy", v).attr("text-anchor", "end").on("click", l).attr("class", function (t, e) {
										return _ + " _" + e
									}).text(function (t) {
										return D.title()(t)
									});
									n.transition(r, D.transitionDuration(), D.transitionDelay()).attr("transform", u)
								}
							}(t)
					}(t = function (t) {
						var e = t.enter().append("g").attr("class", function (t, e) {
							return b + " _" + e
						});
						return e.append("rect").attr("width", 0),
							function (t) {
								D.renderLabel() && t.append("text").on("click", l), D.renderTitleLabel() && t.append("text").attr("class", _).on("click", l)
							}(e), e
					}(t).merge(t))
				}

				function o() {
					var t = d(0);
					return t === -1 / 0 || t != t ? d(1) : t
				}

				function l(t) {
					D.onClick(t)
				}

				function u(t) {
					var e = d(D.cappedValueAccessor(t)),
						n = o();
					return "translate(" + (e > n ? n : e) + ",0)"
				}

				function c(t) {
					return D.hasFilter(D.cappedKeyAccessor(t))
				}
				var s, d, f, h, g = 10,
					p = 15,
					y = !1,
					v = "0.35em",
					x = 2,
					m = 5,
					A = !1,
					b = "row",
					_ = "titlerow",
					k = !1,
					D = n.capMixin(n.marginMixin(n.colorMixin(n.baseMixin({})))),
					C = t.axisBottom();
				return D.rowsCap = D.cap, D._doRender = function () {
					return D.resetSvg(), s = D.svg().append("g").attr("transform", "translate(" + D.margins().left + "," + D.margins().top + ")"), a(), D
				}, D.title(function (t) {
					return D.cappedKeyAccessor(t) + ": " + D.cappedValueAccessor(t)
				}), D.label(D.cappedKeyAccessor), D.x = function (t) {
					return arguments.length ? (d = t, D) : d
				}, D.renderTitleLabel = function (t) {
					return arguments.length ? (k = t, D) : k
				}, D._doRedraw = function () {
					return a(), D
				}, D.xAxis = function (t) {
					return arguments.length ? (C = t, this) : C
				}, D.fixedBarHeight = function (t) {
					return arguments.length ? (A = t, D) : A
				}, D.gap = function (t) {
					return arguments.length ? (m = t, D) : m
				}, D.elasticX = function (t) {
					return arguments.length ? (f = t, D) : f
				}, D.labelOffsetX = function (t) {
					return arguments.length ? (g = t, D) : g
				}, D.labelOffsetY = function (t) {
					return arguments.length ? (p = t, y = !0, D) : p
				}, D.titleLabelOffsetX = function (t) {
					return arguments.length ? (x = t, D) : x
				}, D.anchor(e, r)
			}, n.legend = function () {
				function t() {
					return c + u
				}
				var e, r, i, a = {},
					o = 0,
					l = 0,
					u = 12,
					c = 5,
					s = !1,
					d = 560,
					f = 70,
					h = !1,
					g = n.pluck("name");
				return a.parent = function (t) {
					return arguments.length ? (e = t, a) : e
				}, a.render = function () {
					e.svg().select("g.dc-legend").remove(), i = e.svg().append("g").attr("class", "dc-legend").attr("transform", "translate(" + o + "," + l + ")");
					var a = e.legendables();
					void 0 !== r && (a = a.slice(0, r));
					var p = i.selectAll("g.dc-legend-item").data(a).enter().append("g").attr("class", "dc-legend-item").on("mouseover", function (t) {
						e.legendHighlight(t)
					}).on("mouseout", function (t) {
						e.legendReset(t)
					}).on("click", function (t) {
						t.chart.legendToggle(t)
					});
					i.selectAll("g.dc-legend-item").classed("fadeout", function (t) {
						return t.chart.isLegendableHidden(t)
					}), a.some(n.pluck("dashstyle")) ? p.append("line").attr("x1", 0).attr("y1", u / 2).attr("x2", u).attr("y2", u / 2).attr("stroke-width", 2).attr("stroke-dasharray", n.pluck("dashstyle")).attr("stroke", n.pluck("color")) : p.append("rect").attr("width", u).attr("height", u).attr("fill", function (t) {
						return t ? t.color : "blue"
					}), p.append("text").text(g).attr("x", u + 2).attr("y", function () {
						return u / 2 + (this.clientHeight ? this.clientHeight : 13) / 2 - 2
					});
					var y = 0,
						v = 0;
					p.attr("transform", function (e, n) {
						if (s) {
							var r = !0 === h ? this.getBBox().width + c : f;
							y + r > d && y > 0 && (++v, y = 0);
							var i = "translate(" + y + "," + v * t() + ")";
							return y += r, i
						}
						return "translate(0," + n * t() + ")"
					})
				}, a.x = function (t) {
					return arguments.length ? (o = t, a) : o
				}, a.y = function (t) {
					return arguments.length ? (l = t, a) : l
				}, a.gap = function (t) {
					return arguments.length ? (c = t, a) : c
				}, a.itemHeight = function (t) {
					return arguments.length ? (u = t, a) : u
				}, a.horizontal = function (t) {
					return arguments.length ? (s = t, a) : s
				}, a.legendWidth = function (t) {
					return arguments.length ? (d = t, a) : d
				}, a.itemWidth = function (t) {
					return arguments.length ? (f = t, a) : f
				}, a.autoItemWidth = function (t) {
					return arguments.length ? (h = t, a) : h
				}, a.legendText = function (t) {
					return arguments.length ? (g = t, a) : g
				}, a.maxItems = function (t) {
					return arguments.length ? (r = n.utils.isNumber(t) ? t : void 0, a) : r
				}, a
			}, n.htmlLegend = function () {
				var e, r, i, a, o = {},
					l = n.pluck("name"),
					u = !1,
					c = !1;
				return o.parent = function (t) {
					return arguments.length ? (e = t, o) : e
				}, o.render = function () {
					var t = u ? "dc-legend-item-horizontal" : "dc-legend-item-vertical";
					r.select("div.dc-html-legend").remove();
					var o = r.append("div").attr("class", "dc-html-legend");
					o.attr("style", "max-width:" + r.nodes()[0].style.width);
					var s = e.legendables(),
						d = e.filters();
					void 0 !== i && (s = s.slice(0, i));
					var f = a || t,
						h = o.selectAll("div." + f).data(s).enter().append("div").classed(f, !0).on("mouseover", e.legendHighlight).on("mouseout", e.legendReset).on("click", e.legendToggle);
					c && h.classed(n.constants.SELECTED_CLASS, function (t) {
						return -1 !== d.indexOf(t.name)
					}), h.append("span").attr("class", "dc-legend-item-color").style("background-color", n.pluck("color")), h.append("span").attr("class", "dc-legend-item-label").attr("title", l).text(l)
				}, o.container = function (e) {
					return arguments.length ? (r = t.select(e), o) : r
				}, o.legendItemClass = function (t) {
					return arguments.length ? (a = t, o) : a
				}, o.highlightSelected = function (t) {
					return arguments.length ? (c = t, o) : c
				}, o.horizontal = function (t) {
					return arguments.length ? (u = t, o) : u
				}, o.legendText = function (t) {
					return arguments.length ? (l = t, o) : l
				}, o.maxItems = function (t) {
					return arguments.length ? (i = n.utils.isNumber(t) ? t : void 0, o) : i
				}, o
			}, n.scatterPlot = function (e, r) {
				function i(t, e) {
					return c(t) ? b[e] ? Math.pow(h, 2) : Math.pow(g, 2) : Math.pow(v, 2)
				}

				function a(t, e) {
					l.renderTitle() && (t.selectAll("title").remove(), t.append("title").text(function (t) {
						return l.title()(t)
					}))
				}

				function o(e, r) {
					var i = l.chartBodyG().selectAll(".chart-body path.symbol").filter(function () {
							return e(t.select(this))
						}),
						a = u.size();
					u.size(Math.pow(r, 2)), n.transition(i, l.transitionDuration(), l.transitionDelay()).attr("d", u), u.size(a)
				}
				var l = n.coordinateGridMixin({}),
					u = t.symbol(),
					c = function (t) {
						return t.value
					},
					s = l.keyAccessor();
				l.keyAccessor(function (t) {
					return s(t)[0]
				}), l.valueAccessor(function (t) {
					return s(t)[1]
				}), l.colorAccessor(function () {
					return l._groupName
				}), l.title(function (t) {
					return l.keyAccessor()(t) + "," + l.valueAccessor()(t) + ": " + l.existenceAccessor()(t)
				});
				var d = function (t) {
						return "translate(" + l.x()(l.keyAccessor()(t)) + "," + l.y()(l.valueAccessor()(t)) + ")"
					},
					f = 7,
					h = 5,
					g = 3,
					p = null,
					y = 1,
					v = 0,
					x = 0,
					m = 1,
					A = null,
					b = [];
				return l.brush(t.brush()), u.size(i), n.override(l, "_filter", function (t) {
					return arguments.length ? l.__filter(n.filters.RangedTwoDimensionalFilter(t)) : l.__filter()
				}), l.plotData = function () {
					var t = l.chartBodyG().selectAll("path.symbol").data(l.data());
					n.transition(t.exit(), l.transitionDuration(), l.transitionDelay()).attr("opacity", 0).remove(), (t = t.enter().append("path").attr("class", "symbol").attr("opacity", 0).attr("fill", l.getColor).attr("transform", d).merge(t)).call(a, l.data()), t.each(function (t, e) {
						b[e] = !l.filter() || l.filter().isFiltered([l.keyAccessor()(t), l.valueAccessor()(t)])
					}), n.transition(t, l.transitionDuration(), l.transitionDelay()).attr("opacity", function (t, e) {
						return c(t) ? b[e] ? m : l.excludedOpacity() : x
					}).attr("fill", function (t, e) {
						return A && !c(t) ? A : l.excludedColor() && !b[e] ? l.excludedColor() : l.getColor(t)
					}).attr("transform", d).attr("d", u)
				}, l.existenceAccessor = function (t) {
					return arguments.length ? (c = t, this) : c
				}, l.symbol = function (t) {
					return arguments.length ? (u.type(t), l) : u.type()
				}, l.customSymbol = function (t) {
					return arguments.length ? ((u = t).size(i), l) : u
				}, l.symbolSize = function (t) {
					return arguments.length ? (h = t, l) : h
				}, l.highlightedSize = function (t) {
					return arguments.length ? (f = t, l) : f
				}, l.excludedSize = function (t) {
					return arguments.length ? (g = t, l) : g
				}, l.excludedColor = function (t) {
					return arguments.length ? (p = t, l) : p
				}, l.excludedOpacity = function (t) {
					return arguments.length ? (y = t, l) : y
				}, l.hiddenSize = l.emptySize = function (t) {
					return arguments.length ? (v = t, l) : v
				}, l.emptyColor = function (t) {
					return arguments.length ? (A = t, l) : A
				}, l.emptyOpacity = function (t) {
					return arguments.length ? (x = t, l) : x
				}, l.nonemptyOpacity = function (t) {
					return arguments.length ? (m = t, l) : x
				}, l.legendables = function () {
					return [{
						chart: l,
						name: l._groupName,
						color: l.getColor()
					}]
				}, l.legendHighlight = function (e) {
					o(function (t) {
						return t.attr("fill") === e.color
					}, f), l.chartBodyG().selectAll(".chart-body path.symbol").filter(function () {
						return t.select(this).attr("fill") !== e.color
					}).classed("fadeout", !0)
				}, l.legendReset = function (e) {
					o(function (t) {
						return t.attr("fill") === e.color
					}, h), l.chartBodyG().selectAll(".chart-body path.symbol").filter(function () {
						return t.select(this).attr("fill") !== e.color
					}).classed("fadeout", !1)
				}, l.createBrushHandlePaths = function () {}, l.extendBrush = function (t) {
					return l.round() && (t[0] = t[0].map(l.round()), t[1] = t[1].map(l.round())), t
				}, l.brushIsEmpty = function (t) {
					return !t || t[0][0] >= t[1][0] || t[0][1] >= t[1][1]
				}, l._brushing = function () {
					if (t.event.sourceEvent && (!t.event.sourceEvent.type || -1 === ["start", "brush", "end"].indexOf(t.event.sourceEvent.type))) {
						var e = t.event.selection,
							r = l.brushIsEmpty(e);
						e && (e = e.map(function (t) {
							return t.map(function (t, e) {
								return (0 === e ? l.x() : l.y()).invert(t)
							})
						}), e = l.extendBrush(e), r = r && l.brushIsEmpty(e)), l.redrawBrush(e, !1);
						var i = r ? null : n.filters.RangedTwoDimensionalFilter(e);
						n.events.trigger(function () {
							l.replaceFilter(i), l.redrawGroup()
						}, n.constants.EVENT_DELAY)
					}
				}, l.redrawBrush = function (t, e) {
					var r = l.brush(),
						i = l.gBrush();
					if (l.brushOn() && i)
						if (l.resizing() && l.setBrushExtents(e), t) {
							t = t.map(function (t) {
								return t.map(function (t, e) {
									return (0 === e ? l.x() : l.y())(t)
								})
							});
							n.optionalTransition(e, l.transitionDuration(), l.transitionDelay())(i).call(r.move, t)
						} else i.call(r.move, t);
					l.fadeDeselectedArea(t)
				}, l.setBrushY = function (t) {
					t.call(l.brush().y(l.y()))
				}, l.anchor(e, r)
			}, n.numberDisplay = function (e, r) {
				var i, a = t.format(".2s"),
					o = n.baseMixin({}),
					l = {
						one: "",
						some: "",
						none: ""
					};
				return o._mandatoryAttributes(["group"]), o.ordering(function (t) {
					return t.value
				}), o.html = function (t) {
					return arguments.length ? (t.none ? l.none = t.none : t.one ? l.none = t.one : t.some && (l.none = t.some), t.one ? l.one = t.one : t.some && (l.one = t.some), t.some ? l.some = t.some : t.one && (l.some = t.one), o) : l
				}, o.value = function () {
					return o.data()
				}, o.data(function (t) {
					var e = t.value ? t.value() : function (t) {
						if (!t.length) return null;
						var e = o._computeOrderedGroups(t);
						return e[e.length - 1]
					}(t.all());
					return o.valueAccessor()(e)
				}), o.transitionDuration(250), o.transitionDelay(0), o._doRender = function () {
					var e = o.value(),
						n = o.selectAll(".number-display");
					n.empty() && (n = n.data([0]).enter().append("span").attr("class", "number-display").merge(n)), n.transition().duration(o.transitionDuration()).delay(o.transitionDelay()).ease(t.easeQuad).tween("text", function () {
						var n = isFinite(i) ? i : 0,
							r = t.interpolateNumber(n || 0, e);
						i = e;
						var a = this;
						return function (t) {
							var n = null,
								i = o.formatNumber()(r(t));
							0 === e && "" !== l.none ? n = l.none : 1 === e && "" !== l.one ? n = l.one : "" !== l.some && (n = l.some), a.innerHTML = n ? n.replace("%number", i) : i
						}
					})
				}, o._doRedraw = function () {
					return o._doRender()
				}, o.formatNumber = function (t) {
					return arguments.length ? (a = t, o) : a
				}, o.anchor(e, r)
			}, n.heatMap = function (e, r) {
				function i(t, e) {
					var r = g.selectAll(".box-group").filter(function (n) {
							return n.key[t] === e
						}),
						i = r.filter(function (t) {
							return !g.hasFilter(t.key)
						});
					n.events.trigger(function () {
						var t = (i.empty() ? r : i).data().map(function (t) {
							return n.filters.TwoDimensionalFilter(t.key)
						});
						g._filter([t]), g.redrawGroup()
					})
				}
				var a, o, l, u = t.ascending,
					c = t.ascending,
					s = t.scaleBand(),
					d = t.scaleBand(),
					f = 6.75,
					h = 6.75,
					g = n.colorMixin(n.marginMixin(n.baseMixin({})));
				g._mandatoryAttributes(["group"]), g.title(g.colorAccessor());
				var p = function (t) {
						return t
					},
					y = function (t) {
						return t
					};
				g.colsLabel = function (t) {
					return arguments.length ? (p = t, g) : p
				}, g.rowsLabel = function (t) {
					return arguments.length ? (y = t, g) : y
				};
				var v = function (t) {
						i(0, t)
					},
					x = function (t) {
						i(1, t)
					},
					m = function (t) {
						var e = t.key;
						n.events.trigger(function () {
							g.filter(e), g.redrawGroup()
						})
					};
				return n.override(g, "filter", function (t) {
					return arguments.length ? g._filter(n.filters.TwoDimensionalFilter(t)) : g._filter()
				}), g.rows = function (t) {
					return arguments.length ? (l = t, g) : l
				}, g.rowOrdering = function (t) {
					return arguments.length ? (c = t, g) : c
				}, g.cols = function (t) {
					return arguments.length ? (o = t, g) : o
				}, g.colOrdering = function (t) {
					return arguments.length ? (u = t, g) : u
				}, g._doRender = function () {
					return g.resetSvg(), a = g.svg().append("g").attr("class", "heatmap").attr("transform", "translate(" + g.margins().left + "," + g.margins().top + ")"), g._doRedraw()
				}, g._doRedraw = function () {
					var t = g.data(),
						e = g.rows() || t.map(g.valueAccessor()),
						r = g.cols() || t.map(g.keyAccessor());
					c && (e = e.sort(c)), u && (r = r.sort(u)), e = d.domain(e), r = s.domain(r);
					var i = e.domain().length,
						o = r.domain().length,
						l = Math.floor(g.effectiveWidth() / o),
						p = Math.floor(g.effectiveHeight() / i);
					r.rangeRound([0, g.effectiveWidth()]), e.rangeRound([g.effectiveHeight(), 0]);
					var y = a.selectAll("g.box-group").data(g.data(), function (t, e) {
						return g.keyAccessor()(t, e) + "\0" + g.valueAccessor()(t, e)
					});
					y.exit().remove();
					var v = y.enter().append("g").attr("class", "box-group");
					v.append("rect").attr("class", "heat-box").attr("fill", "white").attr("x", function (t, e) {
						return r(g.keyAccessor()(t, e))
					}).attr("y", function (t, n) {
						return e(g.valueAccessor()(t, n))
					}).on("click", g.boxOnClick()), y = v.merge(y), g.renderTitle() && (v.append("title"), y.select("title").text(g.title())), n.transition(y.select("rect"), g.transitionDuration(), g.transitionDelay()).attr("x", function (t, e) {
						return r(g.keyAccessor()(t, e))
					}).attr("y", function (t, n) {
						return e(g.valueAccessor()(t, n))
					}).attr("rx", f).attr("ry", h).attr("fill", g.getColor).attr("width", l).attr("height", p);
					var x = a.select("g.cols");
					x.empty() && (x = a.append("g").attr("class", "cols axis"));
					var m = x.selectAll("text").data(r.domain());
					m.exit().remove(), m = m.enter().append("text").attr("x", function (t) {
						return r(t) + l / 2
					}).style("text-anchor", "middle").attr("y", g.effectiveHeight()).attr("dy", 12).on("click", g.xAxisOnClick()).text(g.colsLabel()).merge(m), n.transition(m, g.transitionDuration(), g.transitionDelay()).text(g.colsLabel()).attr("x", function (t) {
						return r(t) + l / 2
					}).attr("y", g.effectiveHeight());
					var A = a.select("g.rows");
					A.empty() && (A = a.append("g").attr("class", "rows axis"));
					var b = A.selectAll("text").data(e.domain());
					return b.exit().remove(), b = b.enter().append("text").style("text-anchor", "end").attr("x", 0).attr("dx", -2).attr("y", function (t) {
						return e(t) + p / 2
					}).attr("dy", 6).on("click", g.yAxisOnClick()).text(g.rowsLabel()).merge(b), n.transition(b, g.transitionDuration(), g.transitionDelay()).text(g.rowsLabel()).attr("y", function (t) {
						return e(t) + p / 2
					}), g.hasFilter() ? g.selectAll("g.box-group").each(function (t) {
						g.isSelectedNode(t) ? g.highlightSelected(this) : g.fadeDeselected(this)
					}) : g.selectAll("g.box-group").each(function () {
						g.resetHighlight(this)
					}), g
				}, g.boxOnClick = function (t) {
					return arguments.length ? (m = t, g) : m
				}, g.xAxisOnClick = function (t) {
					return arguments.length ? (v = t, g) : v
				}, g.yAxisOnClick = function (t) {
					return arguments.length ? (x = t, g) : x
				}, g.xBorderRadius = function (t) {
					return arguments.length ? (f = t, g) : f
				}, g.yBorderRadius = function (t) {
					return arguments.length ? (h = t, g) : h
				}, g.isSelectedNode = function (t) {
					return g.hasFilter(t.key)
				}, g.anchor(e, r)
			},
			function () {
				function e(t) {
					return [0, t.length - 1]
				}

				function r(e) {
					return [t.quantile(e, .25), t.quantile(e, .5), t.quantile(e, .75)]
				}
				t.box = function () {
					function i(e) {
						e.each(function (e, n) {
							e = e.map(s).sort(t.ascending);
							var r, i, b = t.select(this),
								_ = e.length;
							if (0 !== _) {
								var k = e.quartiles = f(e),
									D = d && d.call(this, e, n),
									C = D && D.map(function (t) {
										return e[t]
									}),
									w = D ? t.range(0, D[0]).concat(t.range(D[1] + 1, _)) : t.range(_);
								m ? (r = e[0], i = e[_ - 1]) : (r = e[D[0]], i = e[D[1]]);
								var L = t.range(D[0], D[1] + 1),
									R = t.scaleLinear().domain(c && c.call(this, e, n) || [r, i]).range([o, 0]),
									E = this.__chart__ || t.scaleLinear().domain([0, 1 / 0]).range(R.range());
								this.__chart__ = R;
								var M = b.selectAll("line.center").data(C ? [C] : []);
								M.enter().insert("line", "rect").attr("class", "center").attr("x1", a / 2).attr("y1", function (t) {
									return E(t[0])
								}).attr("x2", a / 2).attr("y2", function (t) {
									return E(t[1])
								}).style("opacity", 1e-6).transition().duration(l).delay(u).style("opacity", 1).attr("y1", function (t) {
									return R(t[0])
								}).attr("y2", function (t) {
									return R(t[1])
								}), M.transition().duration(l).delay(u).style("opacity", 1).attr("x1", a / 2).attr("x2", a / 2).attr("y1", function (t) {
									return R(t[0])
								}).attr("y2", function (t) {
									return R(t[1])
								}), M.exit().transition().duration(l).delay(u).style("opacity", 1e-6).attr("y1", function (t) {
									return R(t[0])
								}).attr("y2", function (t) {
									return R(t[1])
								}).remove();
								var S = b.selectAll("rect.box").data([k]);
								S.enter().append("rect").attr("class", "box").attr("x", 0).attr("y", function (t) {
									return E(t[2])
								}).attr("width", a).attr("height", function (t) {
									return E(t[0]) - E(t[2])
								}).style("fill-opacity", g ? .1 : 1).transition().duration(l).delay(u).attr("y", function (t) {
									return R(t[2])
								}).attr("height", function (t) {
									return R(t[0]) - R(t[2])
								}), S.transition().duration(l).delay(u).attr("width", a).attr("y", function (t) {
									return R(t[2])
								}).attr("height", function (t) {
									return R(t[0]) - R(t[2])
								});
								var O = b.selectAll("line.median").data([k[1]]);
								O.enter().append("line").attr("class", "median").attr("x1", 0).attr("y1", E).attr("x2", a).attr("y2", E).transition().duration(l).delay(u).attr("y1", R).attr("y2", R), O.transition().duration(l).delay(u).attr("x1", 0).attr("x2", a).attr("y1", R).attr("y2", R);
								var B = b.selectAll("line.whisker").data(C || []);
								if (B.enter().insert("line", "circle, text").attr("class", "whisker").attr("x1", 0).attr("y1", E).attr("x2", a).attr("y2", E).style("opacity", 1e-6).transition().duration(l).delay(u).attr("y1", R).attr("y2", R).style("opacity", 1), B.transition().duration(l).delay(u).attr("x1", 0).attr("x2", a).attr("y1", R).attr("y2", R).style("opacity", 1), B.exit().transition().duration(l).delay(u).attr("y1", R).attr("y2", R).style("opacity", 1e-6).remove(), m) {
									var F = A ? "outlierBold" : "outlier",
										T = A ? 3 : 5,
										N = A ? function () {
											return Math.floor(Math.random() * (a * v) + 1 + (a - a * v) / 2)
										} : function () {
											return a / 2
										},
										H = b.selectAll("circle." + F).data(w, Number);
									H.enter().insert("circle", "text").attr("class", F).attr("r", T).attr("cx", N).attr("cy", function (t) {
										return E(e[t])
									}).style("opacity", 1e-6).transition().duration(l).delay(u).attr("cy", function (t) {
										return R(e[t])
									}).style("opacity", .6), x && (H.selectAll("title").remove(), H.append("title").text(function (t) {
										return e[t]
									})), H.transition().duration(l).delay(u).attr("cx", N).attr("cy", function (t) {
										return R(e[t])
									}).style("opacity", .6), H.exit().transition().duration(l).delay(u).attr("cy", 0).style("opacity", 1e-6).remove()
								}
								if (g) {
									var G = b.selectAll("circle.data").data(L);
									G.enter().insert("circle", "text").attr("class", "data").attr("r", p).attr("cx", function () {
										return Math.floor(Math.random() * (a * v) + 1 + (a - a * v) / 2)
									}).attr("cy", function (t) {
										return E(e[t])
									}).style("opacity", 1e-6).transition().duration(l).delay(u).attr("cy", function (t) {
										return R(e[t])
									}).style("opacity", y), x && (G.selectAll("title").remove(), G.append("title").text(function (t) {
										return e[t]
									})), G.transition().duration(l).delay(u).attr("cx", function () {
										return Math.floor(Math.random() * (a * v) + 1 + (a - a * v) / 2)
									}).attr("cy", function (t) {
										return R(e[t])
									}).style("opacity", y), G.exit().transition().duration(l).delay(u).attr("cy", 0).style("opacity", 1e-6).remove()
								}
								var P = h || R.tickFormat(8),
									U = b.selectAll("text.box").data(k);
								U.enter().append("text").attr("class", "box").attr("dy", ".3em").attr("dx", function (t, e) {
									return 1 & e ? 6 : -6
								}).attr("x", function (t, e) {
									return 1 & e ? a : 0
								}).attr("y", E).attr("text-anchor", function (t, e) {
									return 1 & e ? "start" : "end"
								}).text(P).transition().duration(l).delay(u).attr("y", R), U.transition().duration(l).delay(u).text(P).attr("x", function (t, e) {
									return 1 & e ? a : 0
								}).attr("y", R);
								var Y = b.selectAll("text.whisker").data(C || []);
								Y.enter().append("text").attr("class", "whisker").attr("dy", ".3em").attr("dx", 6).attr("x", a).attr("y", E).text(P).style("opacity", 1e-6).transition().duration(l).delay(u).attr("y", R).style("opacity", 1), Y.transition().duration(l).delay(u).text(P).attr("x", a).attr("y", R).style("opacity", 1), Y.exit().transition().duration(l).delay(u).attr("y", R).style("opacity", 1e-6).remove(), delete e.quartiles
							}
						}), t.timerFlush()
					}
					var a = 1,
						o = 1,
						l = 0,
						u = 0,
						c = null,
						s = Number,
						d = e,
						f = r,
						h = null,
						g = !1,
						p = 3,
						y = .3,
						v = .8,
						x = !1,
						m = !0,
						A = !1;
					return i.width = function (t) {
						return arguments.length ? (a = t, i) : a
					}, i.height = function (t) {
						return arguments.length ? (o = t, i) : o
					}, i.tickFormat = function (t) {
						return arguments.length ? (h = t, i) : h
					}, i.showOutliers = function (t) {
						return arguments.length ? (m = t, i) : m
					}, i.boldOutlier = function (t) {
						return arguments.length ? (A = t, i) : A
					}, i.renderDataPoints = function (t) {
						return arguments.length ? (g = t, i) : g
					}, i.renderTitle = function (t) {
						return arguments.length ? (x = t, i) : x
					}, i.dataOpacity = function (t) {
						return arguments.length ? (y = t, i) : y
					}, i.dataWidthPortion = function (t) {
						return arguments.length ? (v = t, i) : v
					}, i.duration = function (t) {
						return arguments.length ? (l = t, i) : l
					}, i.domain = function (t) {
						return arguments.length ? (c = null === t ? t : "function" == typeof t ? t : n.utils.constant(t), i) : c
					}, i.value = function (t) {
						return arguments.length ? (s = t, i) : s
					}, i.whiskers = function (t) {
						return arguments.length ? (d = t, i) : d
					}, i.quartiles = function (t) {
						return arguments.length ? (f = t, i) : f
					}, i
				}
			}(), n.boxPlot = function (e, r) {
				function i() {
					return t.min(l.data(), function (e) {
						return t.min(l.valueAccessor()(e))
					})
				}

				function a() {
					return t.max(l.data(), function (e) {
						return t.max(l.valueAccessor()(e))
					})
				}

				function o() {
					return (a() - i()) / l.effectiveHeight()
				}
				var l = n.coordinateGridMixin({}),
					u = function (t) {
						return function (e) {
							var n = e.quartiles[0],
								r = e.quartiles[2],
								i = (r - n) * t,
								a = -1,
								o = e.length;
							do {
								++a
							} while (e[a] < n - i);
							do {
								--o
							} while (e[o] > r + i);
							return [a, o]
						}
					}(1.5),
					c = t.box(),
					s = null,
					d = !1,
					f = .3,
					h = .8,
					g = !0,
					p = !1,
					y = 8,
					v = function (t, e) {
						return l.isOrdinal() ? l.x().bandwidth() : t / (1 + l.boxPadding()) / e
					};
				l.x(t.scaleBand()), l.xUnits(n.units.ordinal), l.data(function (t) {
					return t.all().map(function (t) {
						return t.map = function (e) {
							return e.call(t, t)
						}, t
					}).filter(function (t) {
						return 0 !== l.valueAccessor()(t).length
					})
				}), l.boxPadding = l._rangeBandPadding, l.boxPadding(.8), l.outerPadding = l._outerRangeBandPadding, l.outerPadding(.5), l.boxWidth = function (t) {
					return arguments.length ? (v = "function" == typeof t ? t : n.utils.constant(t), l) : v
				};
				var x = function (t, e) {
					return "translate(" + l.x()(l.keyAccessor()(t, e)) + ", 0)"
				};
				return l._preprocessData = function () {
					l.elasticX() && l.x().domain([])
				}, l.plotData = function () {
					var e = v(l.effectiveWidth(), l.xUnitCount());
					c.whiskers(u).width(e).height(l.effectiveHeight()).value(l.valueAccessor()).domain(l.y().domain()).duration(l.transitionDuration()).tickFormat(s).renderDataPoints(d).dataOpacity(f).dataWidthPortion(h).renderTitle(l.renderTitle()).showOutliers(g).boldOutlier(p);
					var r = l.chartBodyG().selectAll("g.box").data(l.data(), l.keyAccessor());
					! function (e) {
						n.transition(e, l.transitionDuration(), l.transitionDelay()).attr("transform", x).call(c).each(function (e) {
							var n = l.getColor(e, 0);
							t.select(this).select("rect.box").attr("fill", n), t.select(this).selectAll("circle.data").attr("fill", n)
						})
					}(function (t) {
						var e = t.enter().append("g");
						return e.attr("class", "box").attr("transform", x).call(c).on("click", function (t) {
							l.filter(l.keyAccessor()(t)), l.redrawGroup()
						}), e.merge(t)
					}(r)),
					function (t) {
						t.exit().remove().call(c)
					}(r), l.fadeDeselectedArea(l.filter())
				}, l.fadeDeselectedArea = function (t) {
					if (l.hasFilter())
						if (l.isOrdinal()) l.g().selectAll("g.box").each(function (t) {
							l.isSelectedNode(t) ? l.highlightSelected(this) : l.fadeDeselected(this)
						});
						else {
							if (!l.brushOn() && !l.parentBrushOn()) return;
							var e = t[0],
								n = t[1],
								r = l.keyAccessor();
							l.g().selectAll("g.box").each(function (t) {
								var i = r(t);
								i < e || i >= n ? l.fadeDeselected(this) : l.highlightSelected(this)
							})
						}
					else l.g().selectAll("g.box").each(function () {
						l.resetHighlight(this)
					})
				}, l.isSelectedNode = function (t) {
					return l.hasFilter(l.keyAccessor()(t))
				}, l.yAxisMin = function () {
					var t = y * o();
					return n.utils.subtract(i() - t, l.yAxisPadding())
				}, l.yAxisMax = function () {
					var t = y * o();
					return n.utils.add(a() + t, l.yAxisPadding())
				}, l.tickFormat = function (t) {
					return arguments.length ? (s = t, l) : s
				}, l.yRangePadding = function (t) {
					return arguments.length ? (y = t, l) : y
				}, l.renderDataPoints = function (t) {
					return arguments.length ? (d = t, l) : d
				}, l.dataOpacity = function (t) {
					return arguments.length ? (f = t, l) : f
				}, l.dataWidthPortion = function (t) {
					return arguments.length ? (h = t, l) : h
				}, l.showOutliers = function (t) {
					return arguments.length ? (g = t, l) : g
				}, l.boldOutlier = function (t) {
					return arguments.length ? (p = t, l) : p
				}, l.anchor(e, r)
			}, n.selectMenu = function (e, r) {
				function i(e, n) {
					var r, i = t.event.target;
					if (i.selectedOptions) {
						r = Array.prototype.slice.call(i.selectedOptions).map(function (t) {
							return t.value
						})
					} else {
						r = [].slice.call(t.event.target.options).filter(function (t) {
							return t.selected
						}).map(function (t) {
							return t.value
						})
					}
					1 === r.length && "" === r[0] ? r = s || null : c || 1 !== r.length || (r = r[0]), l.onChange(r)
				}
				var a, o = "dc-select-option",
					l = n.baseMixin({}),
					u = "Select all",
					c = !1,
					s = null,
					d = null,
					f = function (t, e) {
						return l.keyAccessor()(t) > l.keyAccessor()(e) ? 1 : l.keyAccessor()(e) > l.keyAccessor()(t) ? -1 : 0
					},
					h = function (t) {
						return l.valueAccessor()(t) > 0
					};
				l.data(function (t) {
					return t.all().filter(h)
				}), l._doRender = function () {
					return l.select("select").remove(), (a = l.root().append("select").classed("dc-select-menu", !0)).append("option").text(u).attr("value", ""), l._doRedraw(), l
				};
				var g = window.navigator.userAgent;
				return g.indexOf("Trident/") > 0 && -1 === g.indexOf("MSIE") && (l.redraw = l.render), l._doRedraw = function () {
					return c ? a.attr("multiple", !0) : a.attr("multiple", null), null !== d ? a.attr("size", d) : a.attr("size", null),
						function () {
							var t = a.selectAll("option." + o).data(l.data(), function (t) {
								return l.keyAccessor()(t)
							});
							t.exit().remove(), t.enter().append("option").classed(o, !0).attr("value", function (t) {
								return l.keyAccessor()(t)
							}).merge(t).text(l.title()), a.selectAll("option." + o).sort(f), a.on("change", i)
						}(), l.hasFilter() && c ? a.selectAll("option").property("selected", function (t) {
							return void 0 !== t && l.filters().indexOf(String(l.keyAccessor()(t))) >= 0
						}) : l.hasFilter() ? a.property("value", l.filter()) : a.property("value", ""), l
				}, l.onChange = function (t) {
					t && c ? l.replaceFilter([t]) : t ? l.replaceFilter(t) : l.filterAll(), n.events.trigger(function () {
						l.redrawGroup()
					})
				}, l.order = function (t) {
					return arguments.length ? (f = t, l) : f
				}, l.promptText = function (t) {
					return arguments.length ? (u = t, l) : u
				}, l.filterDisplayed = function (t) {
					return arguments.length ? (h = t, l) : h
				}, l.multiple = function (t) {
					return arguments.length ? (c = t, l) : c
				}, l.promptValue = function (t) {
					return arguments.length ? (s = t, l) : s
				}, l.numberVisible = function (t) {
					return arguments.length ? (d = t, l) : d
				}, l.size = n.logger.deprecate(l.numberVisible, "selectMenu.size is ambiguous - use numberVisible instead"), l.anchor(e, r)
			}, n.textFilterWidget = function (t, e) {
				var r = n.baseMixin({}),
					i = function (t) {
						return t.toLowerCase()
					},
					a = function (t) {
						return t = i(t),
							function (e) {
								return -1 !== i(e).indexOf(t)
							}
					},
					o = "search";
				return r.group(function () {
					throw "the group function on textFilterWidget should never be called, please report the issue"
				}), r._doRender = function () {
					r.select("input").remove();
					return r.root().append("input").classed("dc-text-filter-input", !0).on("input", function () {
						r.dimension().filterFunction(a(this.value)), n.events.trigger(function () {
							n.redrawAll()
						}, n.constants.EVENT_DELAY)
					}), r._doRedraw(), r
				}, r._doRedraw = function () {
					return r.root().selectAll("input").attr("placeholder", o), r
				}, r.normalize = function (t) {
					return arguments.length ? (i = t, r) : i
				}, r.placeHolder = function (t) {
					return arguments.length ? (o = t, r) : o
				}, r.filterFunctionFactory = function (t) {
					return arguments.length ? (a = t, r) : a
				}, r.anchor(t, e)
			}, n.cboxMenu = function (e, r) {
				function i(e, n) {
					var r;
					t.select(t.event.target).datum() ? (r = t.select(this).selectAll("input").filter(function (t) {
						if (t) return this.checked
					}).nodes().map(function (t) {
						return t.value
					}), c || 1 !== r.length || (r = r[0])) : r = d || null, l.onChange(r)
				}
				var a, o = "dc-cbox-item",
					l = n.baseMixin({}),
					u = "Select all",
					c = !1,
					s = "radio",
					d = null,
					f = Math.floor(1e5 * Math.random()) + 1,
					h = function (t, e) {
						return l.keyAccessor()(t) > l.keyAccessor()(e) ? 1 : l.keyAccessor()(e) > l.keyAccessor()(t) ? -1 : 0
					},
					g = function (t) {
						return l.valueAccessor()(t) > 0
					};
				return l.data(function (t) {
					return t.all().filter(g)
				}), l._doRender = function () {
					return l._doRedraw()
				}, l._doRedraw = function () {
					return l.select("ul").remove(), a = l.root().append("ul").classed("dc-cbox-group", !0),
						function () {
							var t = a.selectAll("li." + o).data(l.data(), function (t) {
								return l.keyAccessor()(t)
							});
							if (t.exit().remove(), (t = t.enter().append("li").classed(o, !0).merge(t)).append("input").attr("type", s).attr("value", function (t) {
									return l.keyAccessor()(t)
								}).attr("name", "domain_" + f).attr("id", function (t, e) {
									return "input_" + f + "_" + e
								}), t.append("label").attr("for", function (t, e) {
									return "input_" + f + "_" + e
								}).text(l.title()), c) a.append("li").append("input").attr("type", "reset").text(u).on("click", i);
							else {
								var e = a.append("li");
								e.append("input").attr("type", s).attr("value", d).attr("name", "domain_" + f).attr("id", function (t, e) {
									return "input_" + f + "_all"
								}).property("checked", !0), e.append("label").attr("for", function (t, e) {
									return "input_" + f + "_all"
								}).text(u)
							}
							a.selectAll("li." + o).sort(h), a.on("change", i)
						}(), l.hasFilter() && c ? a.selectAll("input").property("checked", function (t) {
							return t && l.filters().indexOf(String(l.keyAccessor()(t))) >= 0 || !1
						}) : l.hasFilter() && a.selectAll("input").property("checked", function (t) {
							return !!t && l.keyAccessor()(t) === l.filter()
						}), l
				}, l.onChange = function (t) {
					t && c ? l.replaceFilter([t]) : t ? l.replaceFilter(t) : l.filterAll(), n.events.trigger(function () {
						l.redrawGroup()
					})
				}, l.order = function (t) {
					return arguments.length ? (h = t, l) : h
				}, l.promptText = function (t) {
					return arguments.length ? (u = t, l) : u
				}, l.filterDisplayed = function (t) {
					return arguments.length ? (g = t, l) : g
				}, l.multiple = function (t) {
					return arguments.length ? (c = t, s = c ? "checkbox" : "radio", l) : c
				}, l.promptValue = function (t) {
					return arguments.length ? (d = t, l) : d
				}, l.anchor(e, r)
			}, n.abstractBubbleChart = n.bubbleMixin, n.baseChart = n.baseMixin, n.capped = n.capMixin, n.colorChart = n.colorMixin, n.coordinateGridChart = n.coordinateGridMixin, n.marginable = n.marginMixin, n.stackableChart = n.stackMixin, n.d3 = t, n.crossfilter = e, n
	}
	if ("function" == typeof define && define.amd) define(["d3", "crossfilter2"], t);
	else if ("object" == typeof module && module.exports) {
		var e = require("d3"),
			n = require("crossfilter2");
		"function" != typeof n && (n = n.crossfilter), module.exports = t(e, n)
	} else this.dc = t(d3, crossfilter)
}();
//# sourceMappingURL=dc.min.js.map