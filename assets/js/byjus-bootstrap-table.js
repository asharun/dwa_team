var BYJU = BYJU || {};

var stickyHeaderContainer = '#table3-sticky-header-sticky-header-container';
var bootstrapTableId = '#table3';
window.freezedColumn = [];
window.headerWidths = {};
window.headerLefts = {};

BYJU.bootstrapTable = (function () {

	$(window).scroll(function() {
		freezAll();
	});

	$(document).ready(function() {
		// Handle fixed column selection here
		$(document).on('select2:select', '#freez-column-selector', function (e) {
			var data = e.params.data;
			freezColumn(Number(data.id) + Number(1));
		});

		// Handle fixed column unselection here
		$(document).on('select2:unselect', '#freez-column-selector', function (e) {
			var data = e.params.data;
			//console.log(Number(data.id) + Number(1));
			unfreezColumn(Number(data.id) + Number(1));
		});
	});

	var freezAll = function () {
		if ($('.fixed-table-container').length > 0) {
			// Getting bootstrap table left and right 
			if ($('.bootstrap-table').length) {
				var bootstrapTableLeft = $('.bootstrap-table').offset().left;
				var bootstrapTableRight = bootstrapTableLeft + $('.bootstrap-table').width();
			} 

			var tableContainerTop = $('.fixed-table-container').offset().top - $(window).scrollTop();
			$.each(window.freezedColumn, function(index, value) {
				var freezedColumn = $('#table3 > thead > tr').find('th:nth-child('+value+')');
				var freezedColumnLeft = (freezedColumn.offset().left);
				var freezedColumnRight = freezedColumn.offset().left + freezedColumn.width();
				
				// Freez the Column only if its visible within the boostrap table 
				if (freezedColumnLeft > bootstrapTableLeft && freezedColumnRight < bootstrapTableRight) 
				{
					if (tableContainerTop <= 0 ) {
						var cssSettings = freezStickyHeaderHandler(value);
						applyHeaderCss(cssSettings);
						freezOriginalHeader(value);
						freezColumnValueHandler(value);
	
					}
					else {
						(value !== null) ? freezStickyHeaderHandler(value, true) : "";
						freezOriginalHeader(value);
						freezColumnValueHandler(value);
					}
				} 
				else {
				//	freezCleanUp();
					if (tableContainerTop <= 0 ) {
						var cssSettings = freezStickyHeaderHandler(value);
						applyHeaderCss(cssSettings);
						freezOriginalHeader(value);
						freezColumnValueHandler(value);
					}
					else {
						(value !== null) ? freezStickyHeaderHandler(value, true) : "";
						freezOriginalHeader(value);
						freezColumnValueHandler(value);
					}
				}
				
			})

		}
	}

    var initFreez = function () {
		//console.log("dafds");
		var options = '<select class="form-control" id="freez-column-selector" multiple="multiple">';
		$('#table3').find('thead > tr').find('th').each(function(index, el) {
			var columnNumber = Number(index) + 1;
			var freezer = ' <a href="#" onclick="BYJU.bootstrapTable.freezColumn('+columnNumber+')"><i class="far glyphicon glyphicon-screenshot" title="Freez this column"></i></a>';
			if (index != 0) {
				$(el).find('.th-inner').append(freezer);
			}
			 //console.log(freezer);
			// options += '<option value="'+index+'">' + $(el).find('.th-inner').text() + '</option>';
		});
		// options += '</select>';
		// $('#fixed-col-option-box').append(options);
		// $('#freez-column-selector').select2({
		// 	width: '220px',
		// 	placeholder: 'Select Fixed Columns'
		// });
		// console.log(options);
	}
	
    var freezColumn = function (colNum) {
		// console.log(colNum);
		var selectedHeader = $('#table3 > thead > tr').find('th:nth-child('+colNum+')');

		// Freez only when visible on viewport
		if (selectedHeader.is(":visible")) {
			window.freezedColumn.push(colNum);		
			var originalHeader = $('#table3 > thead > tr').find('th:nth-child('+colNum+')');
			var headerLeft = (originalHeader.offset().left )+ 'px';
			window.headerLefts[colNum] = headerLeft;
			// Here handle original bootstrap header to freez
			freezCleanUp();
			freezAll();
			// freezOriginalHeader(colNum);
			// freezColumnValueHandler(colNum);
		} else {
			alert('Please scroll down the table to column you want to freeze');
		}
		
	}

	var unfreezColumn = function (colNum) {

		var index = window.freezedColumn.indexOf(colNum);
		if (index > -1) {
			window.freezedColumn.splice(index, 1);
		}
		
		removeFreezMess(colNum);
	}

	var freezCleanUp = function () {
		$.each(window.freezedColumn, function(index, value) {
			removeFreezMess(value);
		})
	}

	var removeFreezMess = function(colNum) {
		var stickyHeaderId = 'freez-' + colNum;
		var origHeaderId = 'freez-orig-' + colNum;
		var columnValuesId = 'freez-td-' + colNum;

		//console.log(stickyHeaderId, origHeaderId, columnValuesId);
		$("[id^="+stickyHeaderId+"]").remove();
		$("[id^="+origHeaderId+"]").remove();
		$("[id^="+columnValuesId+"]").remove();
	}

	var freezOriginalHeader = function (colNum, unfreez) {
		
		unfreez = unfreez || false;
		var freezer = ' <a href="#" onclick="BYJU.bootstrapTable.unfreezColumn('+colNum+')"><i class="far glyphicon glyphicon-screenshot" title="Unfreez this column"></i></a>';
		var originalHeader = $('#table3 > thead > tr').find('th:nth-child('+colNum+')');
		var headerLeft = (originalHeader.offset().left )+ 'px';
		var headerTop = originalHeader.offset().top - $(window).scrollTop() + 'px';
		var headerWidth = originalHeader.outerWidth() + 'px';
		var headerHeight = originalHeader.outerHeight() + 'px';
		var origHeaderTextWithFreezer = originalHeader.find('.th-inner').text() + freezer;
		var newHeaderId = 'freez-orig-' + colNum;

		window.headerWidths[colNum] = headerWidth; 

	
		$('#' + newHeaderId).remove();
		// console.log(headerLeft, headerTop, headerWidth, headerHeight);
		if (unfreez) {
			originalHeader.attr('style', '');
		} else {
			//headerLeft=headerLeft;
			$('body').append('<th class="l_font_fix_3" id="'+newHeaderId+'">'+origHeaderTextWithFreezer+'</th>');
			$('#' + newHeaderId).css({
				'position':'fixed',
				'top':headerTop,
				'display':'inline-block',
				'background-color': 'white',
				'left':window.headerLefts[colNum],
				'height':headerHeight,
				'width': headerWidth,
				'padding-top':'9px',
				'padding-left':'5px',
				'border':'1px solid gray',
			});
		}
	}
	
	var freezStickyHeaderHandler = function (colNum, unfreez) {
		unfreez = unfreez || false;
		var headerId = "freez-"+colNum;
		var thisHeader = $(stickyHeaderContainer).
		find('th:nth-child('+ colNum + ')');
		var freezer = ' <a href="#" onclick="BYJU.bootstrapTable.unfreezColumn('+colNum+')"><i class="far glyphicon glyphicon-screenshot" title="Unfreez this column"></i></a>';
		var headerText = thisHeader.find('.th-inner').text();
		if (thisHeader.length) {
			var left =thisHeader.offset().left + 'px';
			var width = thisHeader.width() + 'px';
			var height = thisHeader.height() + 'px';
		}
		if (unfreez) {
			$('#' + headerId).remove();
		} else {
			if ($("#freez-" + colNum).length <= 0 && headerText != '') {
				headerText = headerText + freezer;
				var html = '<th class="l_font_fix_3" id="'+headerId+'">'+headerText+'</th>';
				$('body').append(html)
			}
			return {'headerId': headerId, 'left':window.headerLefts[colNum], 'width':width, 'height':height};	
		}

		// console.log(header.html());
	} 

	var freezColumnValueHandler = function (colNum, unfreez) {
		unfreez = unfreez || false;
		$(bootstrapTableId).find('tbody > tr').each(function(i, el) {
			var currentTD = $(el).find('td:nth-child('+colNum+')');
			if (currentTD.html() != undefined) {
				var tdWIdth = currentTD.outerWidth() + 'px';
				var tdHeight = currentTD.parent().height() + 'px';
				//var tdTop = currentTD.offset().top + 'px';
				var tdTop = currentTD.offset().top - $(window).scrollTop() + 'px';
				var tdLeft = (currentTD.offset().left) + 'px';
				// console.log(tdWIdth, tdHeight, tdTop, tdLeft);
				$('#freez-td-' + i).remove();
				if (unfreez) {
					currentTD.attr('style', '');
				} else {
					var valueCellId = 'freez-td-' + colNum + '-' + i;
					if ( $('#' + valueCellId).length <= 0 && currentTD.text() != '') {
						var tableCell = '<td id="'+valueCellId+'" class="l_font_fix_3">'+currentTD.text()+'</td>';
						$('body').append(tableCell);
					}
					
					//tdLeft=(tdLeft);
					//console.log(tdLeft);
					$('#' + valueCellId).css({
						'position':'fixed',
						'top':tdTop,
						'left':window.headerLefts[colNum],
						'width':  window.headerWidths[colNum],
						'height': tdHeight,
						'background-color': 'white',
						'border': '1px solid gray',
						'padding-top': '9px',
						'padding-left': '5px',
					});
				}	
			}
		})
	}

	var applyHeaderCss = function (css) {
		//console.log(css);
		$('#' + css.headerId).css({
			'position':'fixed',
			'padding-top':'9px',
			'padding-left':'5px',
			'display':'inline-block',
			'top':'0px',
			'left':css.left,
			'width':css.width,
			'height':css.height,
			'background-color':'white',
			'border':'1px solid gray',
			'z-index':'999999'
		})
	}

    // var renderFooter = function (data, event) 
	// {
		// event = event || null;
		// var lastRowHtml = '<tr style="border-top: 4px solid gainsboro"><td class="l_font_fix_3" style="">-</td> <td class="l_font_fix_3 hidden resp_id" style="">-</td> <td class="l_font_fix_3 hidden act_id" style="">-</td> <td class="l_font_fix_3 act_nm" style="">-</td> <td class="l_font_fix_3" style="">-</td> <td class="l_font_fix_3 act_nm" style="">-</td> <td class="l_font_fix_3" style="">-</td> <td class="l_font_fix_3" style=""></td> <td class="l_font_fix_3" style="">-</td> <td class="l_font_fix_3" style="">-</td><td class="l_font_fix_3" id="filled-xp-total" style=""></td> <td class="l_font_fix_3" style="">-</td> <td class="l_font_fix_3" style=""></td> <td class="l_font_fix_3" style="" id="comp-xp-total">-</td> <td class="l_font_fix_3" style="">-</td> <td class="l_font_fix_3" style="text-align: center; ">-</td> <td class="l_font_fix_3" style="">-</td> <td class="l_font_fix_3" style="">-</td> <td class="l_font_fix_3" style="">-</td> <td class="l_font_fix_3" style="">-</td> <td class="l_font_fix_3 aud_c hidden" style="">-</td> <td class="l_font_fix_3 aud_c hidden" style="" id="audit-xp-total">-</td> <td class="l_font_fix_3 aud_c hidden" style="">-</td> <td class="l_font_fix_3 aud_c hidden" style="">-</td> <td class="l_font_fix_3 aud_c hidden" style="">-</td> <td class="l_font_fix_3 aud_c hidden" style="">-</td> <td class="l_font_fix_3 aud_c hidden" style="">-</td> <td class="l_font_fix_3 rev_c hidden" style="">-</td> <td class="l_font_fix_3 rev_c hidden" style="">-</td> <td class="l_font_fix_3 rev_c hidden" style="">-</td> <td class="l_font_fix_3 rev_c hidden" style="">-</td> <td class="l_font_fix_3  con_c hidden" style="" id="confirmed-xp-total">-</td> <td class="l_font_fix_3  con_c hidden" style="">-</td> <td class="l_font_fix_3  con_c hidden" style="">-</td></tr>'; 
		// if (event == 'search') {
			// $('#table3 > tbody').find('tr:last-child').empty();
		// }
		// // console.log(data);
		// $('#table3 > tbody').
		// find('tr:last-child').
		// after(lastRowHtml);
		
		// var totalFilledXp = data.reduce(function(a, b){
			// return add(a, b.equiv_xp);
		// }, 0);

		// var totalCompXp = data.reduce(function(a, b){
			// return add(a, b.comp_xp);
		// }, 0);

		// var totalAuditXp = data.reduce(function(a, b){
			// return add(a, b.audit_xp);
		// }, 0);

		// var totalConfirmedXp = data.reduce(function(a, b){
			// return add(a, b.total_xp);
		// }, 0);

		// $(document).find('#filled-xp-total').
		// html('<strong>' + roudUpToTwoDecimals(totalFilledXp) + '<strong>');
		
		// $(document).find('#comp-xp-total').
		// html('<strong>' + roudUpToTwoDecimals(totalCompXp) + '<strong>');

		// $(document).find('#audit-xp-total').
		// html('<strong>' + roudUpToTwoDecimals(totalAuditXp) + '<strong>');

		// $(document).find('#confirmed-xp-total').
		// html('<strong>' + roudUpToTwoDecimals(totalConfirmedXp) + '<strong>');
	// }

	var add = function (a, b) {
		if (a == null || isNaN(a)) {
			a = 0;
		}
		if (b == null || isNaN(a)) {
			b = 0;
		}
		return a + parseFloat(b);
	}

    return {
        freezColumn,
        initFreez,
		freezColumn,
		unfreezColumn,
		freezCleanUp
    };
}());