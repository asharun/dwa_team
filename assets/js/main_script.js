Array.prototype.indexOfAny = function (array) {
    return this.findIndex(function(v) { return array.indexOf(v) != -1; });
}

Array.prototype.containsAny = function (array) {
    return this.indexOfAny(array) != -1;
}

var getCommonElementsInArray = function (array1, array2) {
	var common = $.grep(array1, function(element) {
		return $.inArray(element, array2 ) !== -1;
	});
	return common;
}

var roudUpToTwoDecimals = function (number) {
	return Math.round(number * 100) / 100 ;
}

function toArray(_Object){
       var _Array = new Array();
       for(var name in _Object){
               _Array[name] = _Object[name];
       }
       return _Array;
}

/**
 *  @desc Find row key in associative array using value and column name
 *  @param <array> assocArray
 *  @param <string> column
 *  @param <string> value
 */
function findInArrayByKeyValue(assocArray, column, value) {
    for (var key in assocArray) {
        if (assocArray[key][column] == value) return assocArray[key];
    }
    return false;
}

// Generic Method to validate form data
function validateForm(data, keysToValidate, keyAnnotationList) {
	if (window.IS_ALLOCATION_MODAL == true) {
       return true;
    }
  for (var key in data) {
    if (data.hasOwnProperty(key)) {
        if (keysToValidate.indexOf(key) > -1 && data[key] == '') {
          alert(keyAnnotationList[key] + " must be filled out.");
          return false;
        }
    }
  }
  return true;
}


// Get PARAM value from string
var getQueryStringValue = function (key) {
	return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));

}

var getBootstrapTableCheckedBoxCount = function (tableId) {
	var arr = $('#' + tableId).find('[type="checkbox"]:checked').map(function(){
		return $(this).closest('tr').find('td:nth-child(2)').text();
	}).get();
	return arr.length;
}

function statusFilter(value) {
	if (value == 'confirmed') {
		window.location = window.location.href + "&confirmed=true";
	} else {
		window.location = removeParam('confirmed', window.location.href);
	}
}

function activeTimeSpan() {
	var timeSpan = getQueryStringValue('time_span');
	if (timeSpan != '') {
		$('select[name="time-span-filter"]')
		.find('option[value="'+timeSpan+'"]')
		.prop('selected', true);
	}
}
activeTimeSpan();

var restoreWeeklyFilterList = [
	'Trend_Report',
	'NonQ_Share',
	'Top_Bottom'
];

function timeSpanFilter(option)
{
	var dateView = getQueryStringValue('date_view');
	if (dateView != '') {
		var date = new Date(dateView);
		var dateRange = getTimeSpanDates(date, option);
	} else {
		var date = new Date();
		// Hadling where s_dt exists in url
		var s_dtFilter = getQueryStringValue('s_dt');
		if (s_dtFilter != '')
			date = new Date(s_dtFilter);

		var dateRange = getTimeSpanDates(date, option);
	}
	var url = window.location.href;
	url = removeParam('start_date', url);
	url = removeParam('end_date', url);
	url = removeParam('time_span', url);
	if (option != 'weekly')
		url = url + '&start_date=' + dateRange.firstDay + '&end_date=' + dateRange.lastDay + '&time_span=' + option;

	if (option == 'weekly' && restoreWeeklyFilterList.includes(getQueryStringValue('a'))) {
		url = removeParam('s_dt', url);
		url = removeParam('e_dt', url);
		var date = new Date();
		url = url + '&s_dt=' + moment(date).subtract(6 , 'day').format("YYYY-MM-DD") + '&e_dt=' + moment(date).format("YYYY-MM-DD");
	}
	window.location = url;
}

function formatDate(date, formatter) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join(formatter);
}

function getTimeSpanDates (date, type) {
	console.log(date, type);
	var firstDay =lastDay = '';
	firstDay = new Date(date.getFullYear(), date.getMonth(), 1);

	switch(type) {
		case 'monthly':
			lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
		  break;
		case 'quarterly':
			lastDay = new Date(date.getFullYear(), date.getMonth() + 3, 0);
		  break;
		  case 'half-yearly':
			lastDay = new Date(date.getFullYear(), date.getMonth() + 6, 0);
		  break;
		case 'yearly':
			lastDay = new Date(date.getFullYear(), date.getMonth() + 12, 0);
		  break;
		  case 'all-time':
			firstDay = new Date('2018-08-06');
			var date = new Date();
		  	lastDay =  new Date(date.getFullYear(), date.getMonth() + 1, 0);
		  break;
		default:
		  // code block

		  console.log(firstDay, lastDay);
	}

	return { 'firstDay': formatDate(firstDay, '-'), 'lastDay': formatDate(lastDay, '-') };
}

function getTimeSpanRedirectUrl (dateView, redirectUrl) {
	// Get the new date range
	var dateRange = getTimeSpanDates(new Date(dateView), getQueryStringValue('time_span'));
	// add params to existing url
	return redirectUrl + '&start_date=' + dateRange.firstDay + '&end_date=' + dateRange.lastDay + '&time_span=' + getQueryStringValue('time_span');
}

function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 *1000));
        var expires = "; expires=" + date.toGMTString();
    } else {
        var expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length,c.length);
        }
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

function getWeekNumber(d) {
    // Copy date so don't modify original
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
    // Get first day of year
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
    // Return array of year and week number
    return [d.getUTCFullYear(), weekNo];
}

function getRowsNumberForTable() {
	if (readCookie("global-table-rows-number") != null) {
		return readCookie("global-table-rows-number");
	} else {
		return 10;
	}
}

Array.prototype.popArray = function (element) {
	var index = this.indexOf(element);
	if (index > -1) {
		this.splice(index, 1);
	}
}

// function loadjscssfile(filename, filetype) {
	// if(filetype == "js") {
		// var cssNode = document.createElement('script');
		// cssNode.setAttribute("type", "text/javascript");
		// cssNode.setAttribute("src", filename);
	// } else if(filetype == "css") {
		// var cssNode = document.createElement("link");
		// cssNode.setAttribute("rel", "stylesheet");
		// cssNode.setAttribute("type", "text/css");
		// cssNode.setAttribute("href", filename);
	// }
	// if(typeof cssNode != "undefined")
		// document.getElementsByTagName("head")[0].appendChild(cssNode);
// }


/**
 * @param {*} string
 * @desc Get the decimal Number from given string
 */
function getDecimalNumberFromString(string) {
	//console.log(string);
	return Number(string.replace(/[^0-9\.]+/g,""));
}

jQuery(document).ready(function(){
	if (getQueryStringValue('confirmed') == 'true') {
		$('select[name="status-filter"]').val('confirmed');
	}

	$("table").on('page-change.bs.table', function (e, size, number) {
		if (readCookie("global-table-rows-number") == null) {
			eraseCookie("global-table-rows-number");
		}
		createCookie("global-table-rows-number", number, 1);
	});


	$("body").on("click", ".m_col",function(){
		var page_view = $(this).attr("data_id");
		if(page_view == "Operations"){
			page_view = "user_mst&loc=crud";
		}
		if(page_view == "Utility"){
			page_view = "utility_user&loc=crud";
		}
		if(page_view == "Work_Request"){
			page_view = "view_work_request&loc=crud";
		}
	window.location = base_url+"index.php/User/load_view_f?a="+page_view;
	});

		$("body").on("click", ".md_t_h",function(){
		window.location = base_url+"index.php/User/home_page";
	});

			$("body").on("click", ".mp_col",function(){
		var page_view = $(this).attr("data_id");
		if(page_view == "Operations"){
			page_view = "user_mst&loc=crud";
		}
		if(page_view == "Utility"){
			page_view = "utility_user&loc=crud";
		}
		if(page_view == "Work_Request"){
			page_view = "view_work_request&loc=crud";
		}
	window.location = base_url+"index.php/User/load_view_f?a="+page_view;

	});

	$("body").on("click", ".curr_dt_view",function(){
			window.location = base_url+"index.php/User/load_view_f?a=Daily_Tasks";
	});

	$("body").on("click", ".cal_1view",function(){
		window.location = base_url+"index.php/User/load_view_f?a=Daily_Tasks";
	});

	$("body").on("click", ".col_men",function(){
		 $('.m_col').toggle();
	});

	$("body").on("click", ".tab_stages",function(){
		var ele=$(this).attr('ch');
		$('.tab_stages').removeClass('tab_dis_selec');
		$(this).addClass('tab_dis_selec');
		 $('.hid').siblings('.hid').not('#'+ele).hide();
		 $('#'+ele).show();
	});

		$("body").on("click", ".stab_stages",function(){
		var ele=$(this).attr('ch');

		var deps_v=$("body").find("#sel_dept_1").val();
		var str='';
		var pro_v=$("body").find("#sel_1234").val();

		if(pro_v)
		{
			str=str+"&pro="+pro_v;
		}

		if(deps_v)
		{
			str=str+"&dept="+deps_v;
		}
		window.location = base_url+"index.php/User/load_view_f?a="+ele+str;
	});

	$("body").on("click", ".stab_stages_2",function(){
		var ele=$(this).attr('ch');
		var str='';
			if($(".ch_dt").val())
			{
				var ele2=Date.parse($(".ch_dt").val());
				var date_v=moment(ele2).format("YYYY-MM-DD");
				str=str+"&date_view="+date_v;
			}
			if($(".s_dt").val())
			{
				var ele12=Date.parse($(".s_dt").val());
				var s_dt=moment(ele12).format("YYYY-MM-DD");
				str=str+"&s_dt="+s_dt;
			}
			if($(".e_dt").val())
			{
				var ele22=Date.parse($(".e_dt").val());
				var e_dt=moment(ele22).format("YYYY-MM-DD");
				str=str+"&e_dt="+e_dt;
			}
		var pro_v=$("body").find("#sel_1234").val();
		var lvl=$("body").find("#sel_2345").val();

		if(pro_v)
		{
			str=str+"&pro="+pro_v;
		}
		if(lvl)
		{
			str=str+"&lvl="+lvl;
		}
			if($("body").find("#sel_dept_1").val())
			{
				var deps_v=$("body").find("#sel_dept_1").val();
				str=str+"&dept="+deps_v;
			}
		window.location = base_url+"index.php/User/load_view_f?a="+ele+str;
	});

	$("body").on("click", ".stab_stages_1",function(){
		var ele=$(this).attr('ch');
		window.location = base_url+"index.php/User/load_view_f?a="+ele+'&loc=crud';
	});

	$(document).on("click",".btn_work_request",function(e){
		e.preventDefault();
		var ele=$(this).attr('ch');
		window.location = base_url+"index.php/User/load_view_f?a="+ele+'&loc=crud';
	});

		$('body').on('change','input[name="ch_optionval0"]', function() {
  // this, in the anonymous function, refers to the changed-<input>:
  // select the element(s) you want to show/hide:
  if(this.value == 'Y')
  {
  $('.issue-fields').show();
  }else
  {
	$('.issue-fields').hide();
	$('.iss_desc').val('');
	$('#iss_cat').val('');
	$('#iss_cat').selectpicker('refresh');
  }
// trigger the change event, to show/hide the .business-fields element(s) on
// page-load:

});


	$("body").on("click", ".edit_task",function(){
		var t_date=$(this).attr('t_date');

			window.location = base_url+"index.php/User/task_edit?t_date="+t_date;
	});



		$("body").on("click", ".home",function(){
			window.location = base_url+"index.php/User/home_page";
		});

	$("body").on("click", ".arr2",function(){
		var w_val = $(this).attr("w_val");
		var ch = $(this).attr("ch");
		var pro_v=$("body").find("#sel_1234").val();
		var emp_c=$("body").find("#sel_emp").val();
		var dept=$("body").find("#sel_dept_1").val();
						var lvl=$("body").find("#sel_2345").val();
						//console.log(lvl);
						var str='';
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						if(emp_c)
						{
							str=str+"&emp="+emp_c;
						}
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
					//window.location = base_url+"index.php/User/load_view_f?a="+ch+"&date_view="+w_val+str;

					var redirectUrl = base_url+"index.php/User/load_view_f?a="+ch+"&date_view="+w_val+str;

						// Manipulate url if time span fitlers are there in url
						if (window.useDateFilters == '1') {
							redirectUrl = getTimeSpanRedirectUrl(w_val, redirectUrl);
						}
						window.location = redirectUrl;
	});


	$("body").on("click", ".arr",function(){
		var w_val = $(this).attr("w_val");
		var ch = $(this).attr("ch");
		var redirectUrl = base_url+"index.php/User/load_view_f?a="+ch+"&date_view="+w_val;

		if($("body").find("#sel_1234").val())
		{
			var pro_v=$("body").find("#sel_1234").val();
			//window.location = base_url+"index.php/User/load_view_f?a="+ch+"&date_view="+w_val+"&pro="+pro_v;
			redirectUrl = base_url+"index.php/User/load_view_f?a="+ch+"&date_view="+w_val+"&pro="+pro_v;
		}
		// Manipulate url if time span fitlers are there in url
		if (window.useDateFilters == '1') {
			redirectUrl = getTimeSpanRedirectUrl(w_val, redirectUrl);
		}
		window.location = redirectUrl;
	});


	/*
    ==============================================================
       Counter Script Start
    ============================================================== */
    if($('.counter').length){
        $('.counter').counterUp({
          delay: 10,
          time: 1000
        });
    }

	$("body").on("click", ".add_task",function(){
		$("#show_gl_col").modal({
           //backdrop: 'static',
            keyboard: false
        });
	});


	$("body").on("click", ".std_upd",function(){
		//alert($("#table tr.selected").find(".std_id"));
		var tc='';
		var ac='';
		$(this).attr("disabled", true);
			$("#table tr.selected").each(function(index) {
				if ($(this).find(".std_id").text())
				{
				tc = tc+','+$(this).find(".std_id").text();
				}
				if ($(this).find(".resp_id").text())
				{
				ac = ac+','+$(this).find(".resp_id").text();
				}
			});

			if(tc && $(".new_tgt").val() && $(".start_dt").attr('dt') && ac && $('#change_desc').val())
			{
				var C=[];
				C[0]=$(".new_tgt").val();
				C[1] = tc.substring(1);
				C[2]=$(".u_in").attr('user');
				C[3]=$(".start_dt").attr('dt');
				C[4]=$(".end_dt").attr('dt');
				C[5]=ac.substring(1);
				C[6]=$('#change_desc').val();
				$.post(base_url+"index.php/User/std_update",{C : C}, function(data, textStatus) {
			//console.log(data);
            if (textStatus == 'success') {
               window.location.reload();
            }else{
				$(this).attr("disabled", false);
			}
			});
			}else{
				$(this).attr("disabled", false);
			}


	});

	$("body").on("click", ".is_approve",function(){
		 var tc1= $("#table3 tr.selected").length;
			if(tc1)
			{
			$("#show_gl_col23").modal({
					//backdrop: 'static',
					keyboard: false
				});
			}
	});
		$("body").on("click", ".is_reject",function(){
		var tc='';
		 $(this).attr("disabled", true);
		 	$("#table3 tr.selected").each(function(index) {
				if ($(this).find(".resp_id").text())
				{
				tc = tc+','+$(this).find(".resp_id").text();
				}
			});
			if(tc)
			{
				var C=[];
				C[1] = tc.substring(1);
				C[2]=$(".u_in").attr('user');
				$.post(base_url+"index.php/User/iss_reject",{C : C}, function(data, textStatus) {
            if (textStatus == 'success') {
               window.location.reload();
				}else{
					$(this).attr("disabled", false);
				}
			});
		}else{
			$(this).attr("disabled", false);
		}
	});


	$("body").on("click", ".is_app_sub",function(){
		var tc='';
		$(this).attr("disabled", true);
		 $("#table3 tr.selected").each(function(index) {
				if ($(this).find(".resp_id").text())
				{
				tc = tc+','+$(this).find(".resp_id").text();
				}
			});
			if(tc && $(".iss_comp_xp").val())
			{
				var C=[];
				C[0]=$(".iss_comp_xp").val();
				C[1] = tc.substring(1);
				C[2]=$(".u_in").attr('user');
				$.post(base_url+"index.php/User/iss_approve",{C : C}, function(data, textStatus) {
            if (textStatus == 'success') {
               window.location.reload();
				}else{
					$(this).attr("disabled", false);
				}
			});
			}else{
				$(this).attr("disabled", false);
			}
	});

	//Change Issue Status
	$(document).on('change', '#issue_status', function(event) {
		event.preventDefault();
		/* Act on the event */
		var status = $(this).val();
		var dept = $("#sel_dept_1").val();
		var redirectUrl = base_url+ `index.php/User/load_view_f?a=Issues_comp&dept=${dept}&issue_status=${status}`;
		if(typeof redirectUrl !== "undefined") window.location.href = redirectUrl
	});

	var iStatus = "";
	//Show modal for edit xps
	$(document).on('click', '.btn_i_status', function(event) {
		event.preventDefault();
		var btnType = $(this).data('type');
		iStatus = btnType === "award" ? "1" : "2";
		
		/* Act on the event */
		var totalCheck= $("#table2 tr.selected").length;
		if(totalCheck === 0) {
			alert("Please select record..");
			return;
		}
		$(".frm_response").trigger('reset');
		btnType === "award" ? $("#mdl_awd_res").modal('show') : $("#mdl_reject_res").modal('show');
	});

	

	//Update the award xps
	$(document).on('submit', '#frm_awd_response', function(event) {
		event.preventDefault();
		/* Act on the event */
		var checkboxValue = "";
		var oldXp = "";
		$("#table2 tr.selected").each(function(index) {
			var chbText = $(this).find(".resp_id").text();
			var chbXp = $(this).find(".comp_old_xp").text();
			if (chbText)
			{
				checkboxValue += ','+chbText;
			}
			if(chbXp){
				oldXp += ',' + chbXp;
			}
		});
		var formData = $(this).serialize();
		checkboxValue = checkboxValue.substring(1);
		oldXp = oldXp.substring(1);
		formData+=`&r_id=${checkboxValue}&issue_status=${iStatus}&oldXp=${oldXp}`;

		var btn = $("#btn_upd_xp");
		btn.attr('disabled', true);
		btn.text('Updating...');
		$.ajax({
			url: `${base_url}index.php/User/updateIssueXp`,
			type: 'post',
			dataType: 'json',
			data: formData,
		})
		.done(function(response) {
			if(typeof response != "undefined" && response.status){
				window.location.reload();
			}
		})
		.fail(function() {
			alert("Something wen't wrong");
		})
		.always(function() {
			btn.attr('disabled', false);
			btn.text('Submit');
			$("#mdl_awd_res").modal("hide");
		});
	});

	//Update the reject status
	$(document).on('submit', '#frm_rej_response', function(event) {
		event.preventDefault();
		/* Act on the event */
		var checkboxValue = "";
		$("#table2 tr.selected").each(function(index) {
			var chbText = $(this).find(".resp_id").text();
			if (chbText)
			{
				checkboxValue += ','+chbText;
			}
		});
		var formData = $(this).serialize();
		checkboxValue = checkboxValue.substring(1);
		formData+=`&r_id=${checkboxValue}&issue_status=${iStatus}`;

		var btn = $("#btn_upd_xp");
		btn.attr('disabled', true);
		btn.text('Updating...');
		$.ajax({
			url: `${base_url}index.php/User/updateIssueXp`,
			type: 'post',
			dataType: 'json',
			data: formData,
		})
		.done(function(response) {
			if(typeof response != "undefined" && response.status){
				window.location.reload();
			}
		})
		.fail(function() {
			alert("Something wen't wrong");
		})
		.always(function() {
			btn.attr('disabled', false);
			btn.text('Submit');
			$("#mdl_reject_res").modal("hide");
		});
	});

	$("body").on("click", ".audit_ins",function(){
		var tc='';
		$(this).attr("disabled", true);
		 $("#table3 tr.selected").each(function(index) {
				if ($(this).find(".resp_id").text())
				{
				tc = tc+','+$(this).find(".resp_id").text();
				}
			});
			if(tc && $(".au_work").val())
			{
				var C=[];
				C[0]=$(".au_work").val();
				C[1] = tc.substring(1);
				C[2]=$(".u_in").attr('user');
				C[3]=$(".au_desc").val();
				C[4]='audit_ins';
				C[5]=$("input[name='option_32']:checked").attr('val');
				//console.log(C);
				$.post(base_url+"index.php/User/audit_conf",{C : C}, function(data, textStatus) {
            if (textStatus == 'success') {
               window.location.reload();
				}else{
					$(this).attr("disabled", false);
				}
			});
			}else{
				$(this).attr("disabled", false);
			}
	});

		$("body").on("click", ".rev_ins",function(){
		var tc='';
		$(this).attr("disabled", true);
		 $("#table3 tr.selected").each(function(index) {
				if ($(this).find(".resp_id").text())
				{
				tc = tc+','+$(this).find(".resp_id").text();
				}
			});
			if(tc && $(".rev_xp").val())
			{
				var C=[];
				C[0]=$(".rev_xp").val();
				C[1] = tc.substring(1);
				C[2]=$(".u_in").attr('user');
				C[3]=$(".review_desc").val();
				C[4]='rev_ins';
				//console.log(C);
				$.post(base_url+"index.php/User/audit_conf",{C : C}, function(data, textStatus) {
            if (textStatus == 'success') {
               window.location.reload();
				}else{
					$(this).attr("disabled", false);
				}
			});
			}else{
				$(this).attr("disabled", false);
			}
	});

	$("body").on("click", ".conf_ins",function(){
		var tc='';		var iss_c=0;
		$(this).attr("disabled", true);
		 $("#table3 tr.selected").each(function(index) {
				if ($(this).find(".resp_id").text())
				{
				tc = tc+','+$(this).find(".resp_id").text();
					if($(this).find(".iss_cur_st").text()=='Pending')
					{
					iss_c++;
					}
				}
			});
			if(iss_c==0)
			{
				if(tc && $(".fin_xp").val())
				{
					var C=[];
					C[0]=$(".fin_xp").val();
					C[1] = tc.substring(1);
					C[2]=$(".u_in").attr('user');
					C[3]='';
					C[4]='conf_ins';
					$.post(base_url+"index.php/User/audit_conf",{C : C}, function(data, textStatus) {
				if (textStatus == 'success') {
				   window.location.reload();
					}else{
						$(this).attr("disabled", false);
					}
				});
				}else{
					$(this).attr("disabled", false);
				}
			}else{
				alert("#"+iss_c+" Log(s) selected have pending issues!");
				$(this).attr("disabled", false);
				//window.location.reload();
			}
	});

	$("body").on("click", ".rev_upd",function(){
		var tc='';
		$(this).attr("disabled", true);
		 $("#table3 tr.selected").each(function(index) {
				if ($(this).find(".resp_id").text())
				{
				tc = tc+','+$(this).find(".resp_id").text();
				}
			});
			if(tc)
			{
				var C=[];
				C[0]='';
				C[1] = tc.substring(1);
				C[2]=$(".u_in").attr('user');
				C[3]='';
				C[4]='rev_upd';
				$.post(base_url+"index.php/User/audit_conf",{C : C}, function(data, textStatus) {
            if (textStatus == 'success') {
               window.location.reload();
				}else{
					$(this).attr("disabled", false);
				}
			});
			}else{
				$(this).attr("disabled", false);
			}
	});

	$("body").on("click", ".conf_upd",function(){
		var tc='';	var iss_c=0;
		$(this).attr("disabled", true);
		 $("#table3 tr.selected").each(function(index) {
				if ($(this).find(".resp_id").text())
				{
				tc = tc+','+$(this).find(".resp_id").text();
					if($(this).find(".iss_cur_st").text()=='Pending')
					{
					iss_c++;
					}
				}
			});
			if(iss_c==0)
			{
				if(tc)
				{
					var C=[];
					C[0]='';
					C[1] = tc.substring(1);
					C[2]=$(".u_in").attr('user');
					C[3]='';
					C[4]='conf_upd';
					$.post(base_url+"index.php/User/audit_conf",{C : C}, function(data, textStatus) {
				if (textStatus == 'success') {
				   window.location.reload();
					}else{
						$(this).attr("disabled", false);
					}
				});
				}else{
					$(this).attr("disabled", false);
				}
			}else{
				alert("#"+iss_c+" Log(s) selected have pending issues!");
				window.location.reload();
			}
	});


	$("body").on("click", ".audit_upd",function(){
		var tc='',u_w='';
		$(this).attr("disabled", true);
		var tot_cnt=$("#table3 tr.selected").length;
		var upd_cnt=0;

		 $("#table3 tr.selected").each(function(index) {
				if ($(this).find(".resp_id").text())
				{
				tc = tc+','+$(this).find(".resp_id").text();
				}
				if ($(this).find(".upd_work").text())
				{
				u_w = u_w+','+$(this).find(".upd_work").text();
				upd_cnt++;
				}
			});

			if(tot_cnt!=upd_cnt)
			{
				$(this).attr("disabled", false);
				alert('Check the "Upd Work" column!');
			}
			else{
			if(tc && u_w)
			{
				var C=[];
				C[0]=u_w.substring(1);
				C[1]=tc.substring(1);
				C[2]=$(".u_in").attr('user');
				C[3]='';
				C[4]='audit_upd';
				C[5]=$("input[name='option_32']:checked").attr('val');
				//console.log(C);
				$.post(base_url+"index.php/User/audit_conf",{C : C}, function(data, textStatus) {
            if (textStatus == 'success') {
               window.location.reload();
				}
			});
			}else{
				$(this).attr("disabled", false);
			}
			}
	});

	$("body").on("click", ".audit_app",function(){
		var tc='';

$(this).attr("disabled", true);
		 $("#table3 tr.selected").each(function(index) {
				if ($(this).find(".resp_id").text())
				{
				tc = tc+','+$(this).find(".resp_id").text();
				}
			});
			if(tc)
			{
				var C=[];
				C[0]='';
				C[1] = tc.substring(1);
				C[2]=$(".u_in").attr('user');
				C[3]='';
				C[4]='audit_app';
				C[5]=$("input[name='option_32']:checked").attr('val');
				$.post(base_url+"index.php/User/audit_conf",{C : C}, function(data, textStatus) {
            if (textStatus == 'success') {
              window.location.reload();
				}
			});
			}else{
				$(this).attr("disabled", false);
			}
	});

	$("body").on("click", ".task_modi",function(){
       resp_id=$(this).attr('resp_id');
	   sh=$('.add_task').attr('sh');
	   $.ajax({
				url: base_url+"index.php/User/task_modi?resp_id="+resp_id+"&sh="+sh,
				success: function(data){
					$('#modal_edit').html(data);
			$("#show_gl_col1").modal({
            keyboard: false
			});
		}});
	});


	// $("body").on("click", ".crud_view",function(){
       // tab_id=$(this).attr('tab_id');
	   // sh=$('body').find('.stab_dis_selec').attr('ch');
	   // $.ajax({
				// url: base_url+"index.php/Crud/view_tab_data?a="+sh+"&tab_id="+tab_id,
				// success: function(data){
					// $('#modal_edit').html(data);
			// $("#show_gl_crud").modal({
            // keyboard: false
			// });
		// }});
	// });

	$("body").on("click", ".crud_ins_utility",function(){
	   sh=$('body').find('.stab_dis_ut').attr('ch');
	   $.ajax({
				url: base_url+"index.php/Crud/ins_tab_data?a="+sh,
				success: function(data){
					$('#modal_edit').html(data);
					$(".selectpicker").selectpicker();
			$("#show_gl_crud").modal({
            keyboard: false
			});
		}});

	});

	$("body").on("click", ".crud_edit",function(){
       tab_id=$(this).attr('tab_id');
	   sh=$('body').find('.stab_dis_selec').attr('ch');
	   var type_page=$(this).data('type');
	   $.ajax({
				url: base_url+"index.php/Crud/edit_tab_data?a="+sh+"&tab_id="+tab_id,
				success: function(data){
					$('#modal_edit').html(data);
					$(".selectpicker").selectpicker();
					if(typeof type_page != "undefined" && type_page === "user_mst"){
						$(".join-date").datepicker(
							{format : 'dd-mm-yyyy',endDate: new Date()}
						);
					}
						$("#show_gl_crud").modal({
						keyboard: false
					});
		}});
		});

		$("body").on("click", ".crud_edit_util",function(){
       tab_id=$(this).attr('tab_id');
	   sh=$('body').find('.stab_dis_ut').attr('ch');
	   $.ajax({
				url: base_url+"index.php/Crud/edit_tab_data?a="+sh+"&tab_id="+tab_id,
				success: function(data){
					$('#modal_edit').html(data);
					$(".selectpicker").selectpicker();
						$("#show_gl_crud").modal({
						keyboard: false
					});
		}});
	});

	$("body").on("click", ".crud_ins",function(){
	   sh=$('body').find('.stab_dis_selec').attr('ch');
	   var type_page=$(this).data('type');
	   $.ajax({
				url: base_url+"index.php/Crud/ins_tab_data?a="+sh,
				success: function(data){
					$('#modal_edit').html(data);
					$(".selectpicker").selectpicker();
					if(typeof type_page != "undefined" && type_page === "user_mst"){
						$(".join-date").datepicker({format : 'dd-mm-yyyy',endDate: new Date()});
					}
			$("#show_gl_crud").modal({
            keyboard: false
			});
		}});

	});

	$("body").on("click", ".tgt_modi",function(){
       resp_id=$(this).closest('tr').find('.resp_id').text();
	   act_nm=$(this).closest('tr').find('.act_nm').text();
	   //console.log(act_nm);
	   $.ajax({
				url: base_url+"index.php/User/tgt_modi?resp_id="+resp_id+"&act_nm="+act_nm,
				success: function(data){
					$('#modal_edit').html(data);
			$("#show_gl_col24").modal({
           // backdrop: 'static',
            keyboard: false
			});
		}});
	});

	$("body").on("click", ".task_view",function(){
       resp_id=$(this).attr('resp_id');
	   $.ajax({
				url: base_url+"index.php/User/task_view?resp_id="+resp_id,
				success: function(data){
					//console.log('Success');
					$('#modal_view').html(data);
			$("#show_gl_col2").modal({
           //backdrop: 'static',
            keyboard: false
			});
		}});
	});


	  $("body").on("click", ".a_task_sub", function(e) {
        e.preventDefault();
		  let userDoneWithActivityChk = $(
            'select[name=user-progress-on-activity]'
        ).val();
        //alert(userDoneWithActivityChk);
        if (userDoneWithActivityChk == 'Y') {
            if (!confirm("Are you sure to mark it complete for you ?")) {
                $('select[name=user-progress-on-activity]')
                .val('N');
                userDoneWithActivityChk = 'In Progress';
            }else{
				userDoneWithActivityChk = 'Completed';
			}
        }else{
			 userDoneWithActivityChk = 'In Progress';
		}
		if( $("body").find('#a_sel_dept_1').val() && $("body").find('.a_word_comp').val() && $("body").find('.a_word_comp').val()!=0 && $("body").find('.a_tas_desc').val() && $("body").find('#a_mile_id').val())
		{
			$(this).attr("disabled", true);
        var C = [];
C[0]=$("body").find('.a_wu').attr('act_id');
C[1]=$("body").find('.a_wu').attr('id_v');
C[2]=$("body").find('.dat_val').attr('user');
C[3]=$("body").find('.dat_val').attr('date_val');
C[4]=$("body").find('.a_tas_desc').val();
C[5]=$("body").find('.a_std_tgt').text();
C[6]=$("body").find('.a_word_comp').val();
C[7]=$("body").find('.a_t_status').text();
C[8]=$("body").find("input[name='a_ch_optionval0']:checked").val();
C[9]=$("body").find('.a_iss_desc').val();
C[10]=$("body").find('#a_sel_dept_1').val();
C[11]=$("body").find('#a_mile_id').val();
C[12]=$("body").find('#a_iss_cat').val();
 C[13]=userDoneWithActivityChk;
//console.log(C);
     $.post(base_url+"index.php/User/insert_resp",{C : C}, function(data, textStatus) {
			//console.log(data);
            if (textStatus == 'success') {
              window.location.reload();
            }
        });
		}else{
			if(!$("body").find('.a_tas_desc').val())
			{
				alert("Please enter Task Desc!");
			}
			else if(!$("body").find('#a_mile_id').val())
			{
				alert("Please choose MID!");
			}
			else{
			alert("Please enter Work Done!");
			}
		}

    });

	  $("body").on("click", ".task_sub", function(e) {
        e.preventDefault();
		if($('#sel_dept_1').val() && $('.word_comp').val() && $('.word_comp').val()!=0 && $('.tas_desc').val() && $('#mile_id').val())
		{
			$(this).attr("disabled", true);
        var C = [];
C[0]=$('.wu').attr('act_id');
C[1]=$('.wu').attr('id_v');
C[2]=$('.dat_val').attr('user');
C[3]=$('.dat_val').attr('date_val');
C[4]=$('.tas_desc').val();
C[5]=$('.std_tgt').text();
C[6]=$('.word_comp').val();
C[7]=$('.t_status').text();
C[8]=$("input[name='ch_optionval0']:checked").val();
C[9]=$('.iss_desc').val();
C[10]=$('#sel_dept_1').val();
C[11]=$('#mile_id').val();
C[12]=$('#iss_cat').val();
 C[13]='In Progress';
//console.log(C);
     $.post(base_url+"index.php/User/insert_resp",{C : C}, function(data, textStatus) {
			//console.log(data);
            if (textStatus == 'success') {
               window.location.reload();
            }
        });
		}else{
			if(!$('.tas_desc').val())
			{
				alert("Please enter Task Desc!");
			}
			else if(!$('#mile_id').val())
			{
				alert("Please choose MID!");
			}
			else{
			alert("Please enter Work Done!");
			}
		}

    });


 $("body").on("click", ".upd_ap", function(e) {
        e.preventDefault();
		// let userDoneWithActivityChk = $('select[name="user-progress-on-activity-upd"]').val();
        // //alert(userDoneWithActivityChk);
        // if (userDoneWithActivityChk == 'Y') {
            // if (!confirm("Are you sure to mark it complete for you ?")) {
                // $('select[name=user-progress-on-activity]')
                // .val('N');
                // userDoneWithActivityChk = 'In Progress';
            // }else{
				// userDoneWithActivityChk = 'Completed';
			// }
        // }
        var C = [];
		C[0]=$('.upd_qty').attr('resp_id');
		C[1]=$('.upd_qty').val();
		C[2]=$('.upd_Desc').val();
		C[3]=$('.dat_val').attr('user');
		// C[4]=$('select[name="user-progress-on-activity-upd"]').val();
		if($('.upd_qty').val() && $('.upd_qty').val()!=0)
		{
     $.post(base_url+"index.php/User/upd_resp",{C : C}, function(data, textStatus) {
			//console.log(data);
            if (textStatus == 'success') {
               window.location.reload();
            }
        });
		}else{
			alert("Please enter Work Done!");
		}
    });

});


// Convert single object to CSV row
var objectToCSVRow = function(dataObject) {
    var dataArray = new Array;
    for (var o in dataObject) {
        var innerValue = dataObject[o]===null?'':dataObject[o].toString();
        var result = innerValue.replace(/"/g, '""');
        result = '"' + result + '"';
        dataArray.push(result);
    }
    return dataArray.join(',') + '\r\n';
}

// Export as a CSV from javascript array of objects
var exportToCSV = function(arrayOfObjects, fileName, headerList) {
    fileName = fileName || 'Sample';
    headerList = headerList || null;

    if (!arrayOfObjects.length) {
        return;
    }

    var csvContent = "data:text/csv;charset=utf-8,";

    // headers
    if (headerList != null) {
        csvContent += headerList.join(',') + '\r\n';
    } else {
        csvContent += Object.keys(arrayOfObjects[0]).join(',') + '\r\n';
    }

    arrayOfObjects.forEach(function(item){
        console.log("Item");
        console.log(item);
        csvContent += objectToCSVRow(item);
    });

    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", fileName + ".csv");
    document.body.appendChild(link); // Required for FF
    link.click();
    document.body.removeChild(link);
}

// Get unique array
function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}
