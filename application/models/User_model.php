<?php
class User_model extends CI_model{
	public $tx_cn='';
	public $tx_cn1='';

	public function __construct(){
      $this->tx_cn='"'.$this->config->item('tx_cn').'"';
	  $this->tx_cn1='"'.$this->config->item('tx_cn1').'"';
		define('CONTENT', '1');
	  define('MEDIA', '2');	  
   }
	public function fetchAccess(){                    
                    $this->db->where('status_nm', 'Active');
                    $res = $this->db->get('access_mst');                
          if($res->result())
          {
             return $res->result();
          }
          else{
            return false;
          }            
        }
		
		public function fetchfeedstart()
		{		
                    $sql='SELECT * FROM feed_start';
                    $res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }            
        }
		
		public function load_cate_issues()
		{		
                    $sql='SELECT * FROM issue_cat_mst';
                    $res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }            
        }
		
		public function fetchQRAccess($uids,$type)
		{	
		$currentDate = date('Y-m-d');
  $sql='SELECT GROUP_CONCAT(DISTINCT B.accss) AS accss
							FROM (
							SELECT "self_form_q" AS accss 
									FROM appraisal_user_mst 
								WHERE user_id_fk='.$uids.' AND status_nm="Active" 
								AND slot_id_fk IN (SELECT slot_id from slot_mst S 
													JOIN feed_start FS 
													ON S.slot_id=FS.slot_id_fk   
													AND "'.$currentDate.'" BETWEEN FS.feed_s AND FS.feed_e 
								WHERE S.slot_type="'.$type.'" AND S.status_nm="Active") 
							UNION 
							SELECT "feed_po_q" 
									FROM appraisal_fb_pro_mst 
								WHERE (po_id_fk='.$uids.') AND status_nm="Active"  
							AND slot_id_fk IN (SELECT slot_id from slot_mst S 
													JOIN feed_start FS 
													ON S.slot_id=FS.slot_id_fk   
													AND "'.$currentDate.'" BETWEEN FS.feed_s AND FS.feed_e 
								WHERE S.slot_type="'.$type.'" AND S.status_nm="Active")
							UNION 
							SELECT "feed_mr_q" 
							FROM appraisal_fb_pro_mst 
							WHERE (mr_id_fk='.$uids.') AND status_nm="Active"  
							AND slot_id_fk IN (SELECT slot_id from slot_mst S 
													JOIN feed_start FS 
													ON S.slot_id=FS.slot_id_fk   
													AND "'.$currentDate.'" BETWEEN FS.feed_s AND FS.feed_e 
								WHERE S.slot_type="'.$type.'" AND S.status_nm="Active")
							UNION SELECT "appraisal_response_q" AS accss 
									FROM appraisal_dm_mst 
								WHERE user_id_fk='.$uids.' AND status_nm="Active" 
								AND slot_id_fk IN (SELECT slot_id from slot_mst S 													
											WHERE S.slot_type="'.$type.'" AND S.status_nm="Active")
							)B';
                    $res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }               
        }
		
		public function fetchformAccess($uids,$type)
		{		
		
		$currentDate = date('Y-m-d');
  $sql='SELECT GROUP_CONCAT(DISTINCT B.accss) AS accss
							FROM (
							SELECT "self_form" AS accss 
									FROM appraisal_user_mst 
								WHERE user_id_fk='.$uids.' AND status_nm="Active" 
								AND slot_id_fk IN (SELECT slot_id from slot_mst S 
													JOIN feed_start FS 
													ON S.slot_id=FS.slot_id_fk   
													AND "'.$currentDate.'" BETWEEN FS.feed_s AND FS.feed_e 
								WHERE S.slot_type="'.$type.'" AND S.status_nm="Active") 
							UNION 
							SELECT "feed_po" 
									FROM appraisal_fb_pro_mst 
								WHERE (po_id_fk='.$uids.') AND status_nm="Active"  
							AND slot_id_fk IN (SELECT slot_id from slot_mst S 
													JOIN feed_start FS 
													ON S.slot_id=FS.slot_id_fk  
													AND "'.$currentDate.'" BETWEEN FS.feed_s AND FS.feed_e 
								WHERE S.slot_type="'.$type.'" AND S.status_nm="Active")
							UNION 
							SELECT "feed_mr" 
							FROM appraisal_fb_pro_mst 
							WHERE (mr_id_fk='.$uids.') AND status_nm="Active"  
							AND slot_id_fk IN (SELECT slot_id from slot_mst S 
													JOIN feed_start FS 
													ON S.slot_id=FS.slot_id_fk  
													AND "'.$currentDate.'" BETWEEN FS.feed_s AND FS.feed_e 
								WHERE S.slot_type="'.$type.'" AND S.status_nm="Active")
							UNION SELECT "appraisal_response" AS accss 
									FROM appraisal_dm_mst 
								WHERE user_id_fk='.$uids.' AND status_nm="Active" 
								AND slot_id_fk IN (SELECT slot_id from slot_mst S 													
											WHERE S.slot_type="'.$type.'" AND S.status_nm="Active")
							)B';
                    $res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }              
		  
                    // $sql='SELECT GROUP_CONCAT(DISTINCT B.accss) AS accss
							// FROM (
							// SELECT "self_form" AS accss 
									// FROM appraisal_user_mst 
								// WHERE user_id_fk='.$uids.' AND status_nm="Active" 
							// UNION 
							// SELECT "feed_po" 
									// FROM appraisal_fb_pro_mst 
								// WHERE (po_id_fk='.$uids.') AND status_nm="Active" 
							// UNION 
							// SELECT "feed_mr" 
							// FROM appraisal_fb_pro_mst 
							// WHERE (mr_id_fk='.$uids.') AND status_nm="Active" 
							// UNION SELECT "appraisal_response" AS accss 
									// FROM appraisal_dm_mst 
								// WHERE user_id_fk='.$uids.' AND status_nm="Active" 
							// )B';
                    // $res = $this->db->query($sql);                
          // if($res->result_array())
          // {
             // return $res->result_array();
          // }
          // else{
            // return false;
          // }            
        }
		
		public function get_param_review($r_ids)
		{		
                    $sql='SELECT P.project_name,CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))  AS trade_name,
							RP.r_param_id,IFNULL(RP.r_param_name,"Quality Score") AS r_param_name,GROUP_CONCAT(R.response_id) AS r_ids
							FROM response_mst R 
							INNER JOIN activity_mst A 
							ON A.activity_id=R.activity_id_fk AND A.status_nm ="Active" 
							INNER JOIN project_mst P 
							ON A.project_id_fk=P.project_id 
							INNER JOIN sub_mst ST
							ON A.sub_id_fk=ST.sub_id 
							INNER JOIN job_mst J
							ON A.job_id_fk=J.job_id 
							LEFT JOIN review_param_mst RP 
							ON A.project_id_fk=RP.project_id_fk AND RP.status_nm="Active" 
							AND CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1)) =RP.trade_name
							WHERE R.response_id IN ('.$r_ids.') AND R.status_nm ="Active" 
							GROUP BY P.project_name,CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1)),
							RP.r_param_id,RP.r_param_name;';
                    $res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }            
        }
		public function get_p_val_rev()
		{		
                    $sql='SELECT R.r_val_id,r_val_name 
							FROM review_val_mst R 							
							WHERE R.status_nm="Active"; ';
                    $res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }            
        }
		
		
		public function get_not_quantifiable($file_nm,$date_start,$date_end,$dept,$pro_j){
            $qry="SELECT YEARWEEK(R1.day_val, 1) AS week_val,
						SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS all_total_x,
                        SUM(CASE WHEN w.is_quantify='N' THEN (IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) ELSE 0 END) AS total_x
                    FROM
                        `response_mst` `R1` 
                        INNER JOIN activity_mst A 
                        ON A.activity_id=R1.activity_id_fk AND A.status_nm = 'Active' 
                        INNER JOIN work_unit_mst w ON 
                        A.`wu_id_fk` = w.wu_id AND w.status_nm = 'Active'
                    WHERE
                        R1.dept_id_fk = '".$dept."' AND R1.day_val BETWEEN '".$date_start."' AND '".$date_end."' 
						AND `R1`.`status_nm` = 'Active' AND A.project_id_fk='".$pro_j."' 
                    GROUP BY
                        YEARWEEK(R1.day_val, 1);
                        ";
                        
        $sqlQuery = $this->db->query($qry);
            $resultSet = $sqlQuery->result_array();             
            
            if($resultSet)
              {
                return $resultSet;
              }
              else{
              // print_r("No data");die;
              }
     }
	 
		public function pt_get_not_quantifiable($file_nm,$date_start,$date_end,$pt_emp){
            $qry="SELECT YEARWEEK(R1.day_val, 1) AS week_val,
						SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS all_total_x,
                        SUM(CASE WHEN w.is_quantify='N' THEN (IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) ELSE 0 END) AS total_x
                    FROM
                        `response_mst` `R1` 
                        INNER JOIN activity_mst A 
                        ON A.activity_id=R1.activity_id_fk 
                        INNER JOIN work_unit_mst w ON 
                        A.`wu_id_fk` = w.wu_id AND w.status_nm = 'Active'
                    WHERE
                         R1.day_val BETWEEN '".$date_start."' AND '".$date_end."' 
						AND `R1`.`status_nm` = 'Active' AND R1.user_id_fk IN ('".$pt_emp."')  
                    GROUP BY
                        YEARWEEK(R1.day_val, 1);
                        ";
                        
        $sqlQuery = $this->db->query($qry);
            $resultSet = $sqlQuery->result_array();             
            
            if($resultSet)
              {
                return $resultSet;
              }
              else{
              // print_r("No data");die;
              }
     }
		
		public function get_emp_xps($date_s,$date_e,$dept_opt,$pro_j){
	 	 $qry="SELECT
        DE.week_val,
        AVG(DE.equiv_xp) AS Avg_val,
        MAX(DE.equiv_xp) AS v1,
		MIN(DE.equiv_xp) AS v2 ,
		COUNT(DISTINCT DE.user_id_fk) AS tem_cnt
    FROM (
SELECT 
		YEARWEEK(R.day_val, 1) AS week_val,
            R.user_id_fk,
            SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
        FROM
            `response_mst` `R`  
       INNER JOIN (SELECT week_v,project_id_fk AS project_id,project_name,user_id_fk 
		FROM (
			SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
				@type := tab1.user_id_fk AS dummy
			FROM
				(
				SELECT YEARWEEK(R.day_val, 1) AS week_v,
					A.project_id_fk,
					P1.project_name,
					R.user_id_fk,
					SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
				FROM
					`response_mst` `R` 
				INNER JOIN (SELECT @num:= 0,    @type := '') B
					ON 1=1
				INNER JOIN `activity_mst` `A` ON
					`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 
				INNER JOIN `project_mst` `P1` 
				ON `P1`.`project_id`=`A`.`project_id_fk` 
				WHERE `R`.`dept_id_fk` = '".$dept_opt."' 
						AND	`R`.day_val BETWEEN '".$date_s."' AND '".$date_e."' 
					AND `R`.`status_nm` = 'Active' 
				GROUP BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`,A.project_id_fk
				ORDER BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`,equiv_xp DESC,P1.project_name
			) tab1
			GROUP BY    week_v,user_id_fk,project_id_fk,project_name,equiv_xp 
			HAVING row_number = 1)E 
			WHERE E.project_id_fk='".$pro_j."'
			)CTE ON CTE.user_id_fk=R.user_id_fk AND CTE.week_v=YEARWEEK(R.day_val, 1)
	 WHERE
            R.dept_id_fk ='".$dept_opt."'   
			AND R.day_val BETWEEN '".$date_s."' AND '".$date_e."' 
			AND `R`.`status_nm` = 'Active'  
        GROUP BY
            YEARWEEK(R.day_val, 1),`R`.`user_id_fk` 
			)DE
    GROUP BY DE.week_val;";
						
		$sqlQuery = $this->db->query($qry);
			$resultSet = $sqlQuery->result_array();             
			
			if($resultSet)
	          {
	            return $resultSet;
	          }
	          
	 	
	 }
	 
	 public function pt_get_emp_xps($date_start,$date_end,$pt_emp){
	 	 $qry="SELECT
        DE.week_val,
        AVG(DE.equiv_xp) AS Avg_val,
        MAX(DE.equiv_xp) AS v1,MIN(DE.equiv_xp) AS v2 ,
		COUNT(DISTINCT DE.user_id_fk) AS tem_cnt
    FROM (           
        SELECT
            YEARWEEK(R.day_val, 1) AS week_val,
            P.project_name,
            R.user_id_fk,
            SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
        FROM
            `response_mst` `R`    
        INNER JOIN `activity_mst` `A` ON
            `R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active'
        INNER JOIN `user_mst` `U` ON
            `U`.`user_id` = `R`.`user_id_fk`
        INNER JOIN `project_mst` `P` ON
            `P`.`project_id` = `A`.`project_id_fk`     
        WHERE
            R.day_val BETWEEN '".$date_start."' AND '".$date_end."'  AND `R`.`status_nm` = 'Active'
            AND R.user_id_fk IN ('".$pt_emp."') 
        GROUP BY
            YEARWEEK(R.day_val, 1),P.project_id,P.project_name,`R`.`user_id_fk`
    ) DE
    GROUP BY DE.week_val;";
						
		$sqlQuery = $this->db->query($qry);
			$resultSet = $sqlQuery->result_array();             
			
			if($resultSet)
	          {
	            return $resultSet;
	          }
	          
	 	
	 }
	 
	 public function get_emp_xps2($date_s,$date_e,$dept_opt,$pro_j){
	 	 $qry=" SELECT DE.week_v AS week_val,
		AVG(SM.equiv_xps/DE.day_cnt) AS Avg_per_day 
		FROM 
		( SELECT Main.week_v,Main.user_id,Main.day_cnt
				FROM 
					(
				SELECT YEARWEEK(IOR.date_val, 1) AS week_v,IOR.user_id ,
			SUM(IOR.attendance_cnt) AS day_cnt 
		FROM in_out_upload  IOR 													
			WHERE IOR.date_val BETWEEN '".$date_s."' AND '".$date_e."'     
		 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm='Active' 
		 AND IOR.user_id IS NOT NULL 
		 GROUP BY YEARWEEK(IOR.date_val, 1) ,IOR.user_id )Main  
		 INNER JOIN ( SELECT week_v,project_id_fk AS project_id,project_name,user_id_fk AS user_id 
						FROM (
					SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
						@type := tab1.user_id_fk AS dummy
					FROM
						(
						SELECT YEARWEEK(R.day_val, 1) AS week_v,
							A.project_id_fk,
							P1.project_name,
							R.user_id_fk,
							SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
						FROM
							`response_mst` `R` 
						INNER JOIN (SELECT @num := 0,    @type := '') B
							ON 1=1
						INNER JOIN `activity_mst` `A` ON
							`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 
						INNER JOIN `project_mst` `P1` 
						ON `P1`.`project_id`=`A`.`project_id_fk` 
						WHERE
							R.day_val BETWEEN '".$date_s."' AND '".$date_e."'   
							AND  R.dept_id_fk ='".$dept_opt."' 
							AND `R`.`status_nm` = 'Active'
						GROUP BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`,A.project_id_fk
						ORDER BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`,equiv_xp DESC,P1.project_name
					) tab1
					GROUP BY    week_v,user_id_fk,project_id_fk,project_name,equiv_xp 
					HAVING row_number = 1)E 
					WHERE E.project_id_Fk='".$pro_j."'  
					)U 
					ON U.week_v=Main.week_v AND U.user_id=Main.user_id 
					)DE 
		INNER JOIN (SELECT 
				YEARWEEK(R.day_val, 1) AS week_val,
					R.user_id_fk,
					SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xps
				FROM
					`response_mst` `R` 			
			 WHERE
					R.day_val BETWEEN '".$date_s."' AND '".$date_e."'   
					AND `R`.`status_nm` = 'Active'  
				GROUP BY
					YEARWEEK(R.day_val, 1),`R`.`user_id_fk` 
			)SM 
			ON SM.week_val=DE.week_v AND SM.user_id_fk=DE.user_id  
		GROUP BY DE.week_v; ";
						
		$sqlQuery = $this->db->query($qry);
			$resultSet = $sqlQuery->result_array();             
			
			if($resultSet)
	          {
	            return $resultSet;
	          }
	          
	 	
	 }
	 
	 public function get_emp_xps2_dh($date_s,$date_e,$dept_opt,$pro_j){
		 if($pro_j)
		 {
	 	 $qry=" SELECT DE.week_v AS week_val,
		AVG((SM.contri_xps/(SM.contri_xps/SM.agg_xps))/DE.day_cnt) AS Avg_per_day 
FROM 
(
SELECT 
     Main.week_v ,Main.user_id ,Main.day_cnt
	  FROM (
		SELECT YEARWEEK(IOR.date_val, 1) AS week_v,IOR.user_id ,
	SUM(IOR.attendance_cnt) AS day_cnt 
FROM in_out_upload  IOR 													
	WHERE IOR.date_val BETWEEN '".$date_s."' AND '".$date_e."'  
 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm='Active' 
 AND IOR.user_id IS NOT NULL 
 GROUP BY YEARWEEK(IOR.date_val, 1) ,IOR.user_id )Main
INNER JOIN (
				SELECT YEARWEEK(R.day_val, 1) AS week_v,
					R.user_id_fk  AS user_id 
				FROM
					`response_mst` `R` 
				INNER JOIN `activity_mst` `A` ON
					`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 				 
				WHERE  A.project_id_fk='".$pro_j."' AND 
					R.day_val BETWEEN '".$date_s."' AND '".$date_e."'  
					AND  R.dept_id_fk ='".$dept_opt."' 
					AND `R`.`status_nm` = 'Active' 
				GROUP BY YEARWEEK(R.day_val, 1),R.user_id_fk			
			)U 
ON U.week_v=Main.week_v AND Main.user_id=U.user_id 
)DE 
 INNER JOIN (
	SELECT 
		YEARWEEK(R.day_val, 1) AS week_val, 
            R.user_id_fk,
			SUM(CASE WHEN A.project_id_fk='".$pro_j."' THEN IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0) 
			ELSE 0 END) AS contri_xps,
            SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS agg_xps
        FROM
            `response_mst` `R`  
			INNER JOIN `activity_mst` `A` ON
			`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 			
				WHERE  
					R.day_val BETWEEN '".$date_s."' AND '".$date_e."'    
					AND `R`.`status_nm` = 'Active' 
				GROUP BY YEARWEEK(R.day_val, 1),R.user_id_fk
	)SM ON SM.week_val=DE.week_v AND SM.user_id_fk=DE.user_id
GROUP BY DE.week_v;";
		 }else{
			 	 $qry="
		 SELECT DE.week_v AS week_val,
		AVG(SM.agg_xps/DE.day_cnt) AS Avg_per_day 
FROM 
(
SELECT YEARWEEK(IOR.date_val, 1) AS week_v,IOR.user_id ,
	SUM(IOR.attendance_cnt) AS day_cnt 
FROM in_out_upload  IOR 													
	WHERE IOR.date_val BETWEEN '".$date_s."' AND '".$date_e."' 
 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm='Active' 
 AND IOR.user_id IS NOT NULL 
 GROUP BY YEARWEEK(IOR.date_val, 1) ,IOR.user_id 
)DE 
 INNER JOIN (SELECT 
		YEARWEEK(R.day_val, 1) AS week_val,
            R.user_id_fk,
            SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS agg_xps
        FROM
            `response_mst` `R`  	
			INNER JOIN `activity_mst` `A` ON
			`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 			
				WHERE  
					R.day_val BETWEEN '".$date_s."' AND '".$date_e."'  
					AND `R`.`status_nm` = 'Active' 
				GROUP BY YEARWEEK(R.day_val, 1),R.user_id_fk
	)SM ON SM.week_val=DE.week_v AND SM.user_id_fk=DE.user_id
GROUP BY DE.week_v;
	";
			 
		 }				
		$sqlQuery = $this->db->query($qry);	
			$resultSet = $sqlQuery->result_array();             
			
			if($resultSet)
	          {
	            return $resultSet;
	          }
	          
	 	
	 }
	 
	 public function get_em_descriptives($date_s,$date_e,$dept_opt){
		 
	 	 $qry=" SELECT DE.week_v AS week_val,DE.project_id_fk,DE.project_name,
		AVG((DE.contri_xps/(DE.contri_xps/SM.agg_xps))/DE.day_cnt) AS Avg_per_day ,
		SUM(DE.nq_sum)/SUM(DE.contri_xps) AS nq_sum
FROM 
(
SELECT 
     Main.week_v ,U.project_id_fk,U.project_name,Main.user_id ,Main.day_cnt,U.contri_xps,U.nq_sum
	  FROM (
		SELECT YEARWEEK(IOR.date_val, 1) AS week_v,IOR.user_id ,
	SUM(IOR.attendance_cnt) AS day_cnt 
FROM in_out_upload  IOR 													
	WHERE IOR.date_val BETWEEN '".$date_s."' AND '".$date_e."'  
 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm='Active' 
 AND IOR.user_id IS NOT NULL 
 GROUP BY YEARWEEK(IOR.date_val, 1) ,IOR.user_id )Main
INNER JOIN (
				SELECT YEARWEEK(R.day_val, 1) AS week_v,
				A.project_id_fk,P.project_name,
					R.user_id_fk  AS user_id ,
					SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS contri_xps ,
					SUM(CASE WHEN WUM.is_quantify='N' THEN (IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) ELSE 0 END) AS nq_sum 
				FROM
					`response_mst` `R` 
				INNER JOIN `activity_mst` `A` ON
					`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 
				INNER JOIN `project_mst` `P` ON
					`P`.`project_id` = `A`.`project_id_fk` AND `P`.`status_nm` = 'Active' 
				INNER JOIN `work_unit_mst` `WUM` ON
					`WUM`.`wu_id` = `A`.`wu_id_fk` AND `WUM`.`status_nm` = 'Active' 					
				WHERE  
					R.day_val BETWEEN '".$date_s."' AND '".$date_e."'  
					AND  R.dept_id_fk ='".$dept_opt."' 
					AND `R`.`status_nm` = 'Active' 
				GROUP BY YEARWEEK(R.day_val, 1),A.project_id_fk,P.project_name,R.user_id_fk			
			)U 
ON U.week_v=Main.week_v AND Main.user_id=U.user_id 
)DE 
 INNER JOIN (
	SELECT 
		YEARWEEK(R.day_val, 1) AS week_val, 
            R.user_id_fk,
            SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS agg_xps
        FROM
            `response_mst` `R`  
			INNER JOIN `activity_mst` `A` ON
			`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 			
				WHERE  
					R.day_val BETWEEN '".$date_s."' AND '".$date_e."'    
					AND `R`.`status_nm` = 'Active' 
				GROUP BY YEARWEEK(R.day_val, 1),R.user_id_fk
	)SM ON SM.week_val=DE.week_v AND SM.user_id_fk=DE.user_id
GROUP BY DE.week_v,DE.project_id_fk,DE.project_name;";
						
		$sqlQuery = $this->db->query($qry);	
			$resultSet = $sqlQuery->result_array();             
			
			if($resultSet)
	          {
	            return $resultSet;
	          }
	          
	 	
	 }
	  
	  public function pt_get_emp_xps2($date_s,$date_e,$pt_emp){
	 	 $qry="
		 SELECT 
		 YEARWEEK(BE.day_val, 1) AS week_val,
		 AVG(BE.Avg_val) AS Avg_per_day
		 FROM (SELECT
        DE.day_val,
        (DE.equiv_xp/DE.tem_mem) AS Avg_val 
    FROM (           
        SELECT
            R.day_val,
            P.project_name,
            SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp,
			COUNT(DISTINCT R.user_id_fk) AS tem_mem
        FROM
            `response_mst` `R`    
        INNER JOIN `activity_mst` `A` ON
            `R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active'
        INNER JOIN `user_mst` `U` ON
            `U`.`user_id` = `R`.`user_id_fk`
        INNER JOIN `project_mst` `P` ON
            `P`.`project_id` = `A`.`project_id_fk`     
        WHERE
             R.day_val BETWEEN '".$date_s."' AND '".$date_e."'  AND `R`.`status_nm` = 'Active'
            AND R.user_id_fk  IN ('".$pt_emp."') 
        GROUP BY
            R.day_val,P.project_id,P.project_name
    ) DE
    GROUP BY DE.day_val) BE 
	GROUP BY YEARWEEK(BE.day_val, 1)
	";
						
		$sqlQuery = $this->db->query($qry);
			$resultSet = $sqlQuery->result_array();             
			
			if($resultSet)
	          {
	            return $resultSet;
	          }
	          
	 	
	 }
		// public function get_emp_xps($date_s,$date_e,$dept_opt,$pro_j){
	 	 // $qry="SELECT
        // DE.week_val,
        // AVG(DE.equiv_xp) AS Avg_val,
        // MAX(DE.equiv_xp) AS v1,MIN(DE.equiv_xp) AS v2 ,
        // MAX(DE.def_val) AS day_cnt,
        // AVG(DE.equiv_xp)/MAX(DE.def_val) AS Avg_per_day
    // FROM (           
        // SELECT
            // YEARWEEK(R.day_val, 1) AS week_val,
            // P.project_name,
            // R.user_id_fk,
            // DEF.def_val,
            // SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
        // FROM
            // `response_mst` `R`    
        // INNER JOIN `activity_mst` `A` ON
            // `R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active'
        // INNER JOIN `user_mst` `U` ON
            // `U`.`user_id` = `R`.`user_id_fk`
        // INNER JOIN `project_mst` `P` ON
            // `P`.`project_id` = `A`.`project_id_fk`
        // INNER JOIN def_values DEF
                        // ON DEF.def_variable='Day_Count' AND R.day_val BETWEEN DEF.`start_dt` AND IFNULL(DEF.end_dt,'".$date_e."' )
                        // AND DEF.status_nm='Active'     
        // WHERE
            // R.dept_id_fk ='".$dept_opt."'  AND R.day_val BETWEEN '".$date_s."' AND '".$date_e."'  AND `R`.`status_nm` = 'Active'
            // AND P.project_id='".$pro_j."' 
        // GROUP BY
            // YEARWEEK(R.day_val, 1),P.project_id,P.project_name,`R`.`user_id_fk`,DEF.def_val
    // ) DE
    // GROUP BY DE.week_val";
						
		// $sqlQuery = $this->db->query($qry);
			// $resultSet = $sqlQuery->result_array();             
			
			// if($resultSet)
	          // {
	            // return $resultSet;
	          // }
	          
	 	
	 // }
		
		public function getWeeklyContributors($startDate, $endDate, $type,$dept_id)
		{
			if ($type == '0') {
				$query = 'SELECT E.project_id_fk ,E.project_name,E.user_id_fk,U.full_name
					FROM (
							SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
								@type := tab1.user_id_fk AS dummy
							FROM
								(
								SELECT
									A.project_id_fk,
									P1.project_name,
									R.user_id_fk,
									ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp 
								FROM
									`response_mst` `R` 
									INNER JOIN (SELECT @num := 0,    @type := "") B
									ON 1=1
								INNER JOIN `activity_mst` `A` ON
									`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active" 
								INNER JOIN `project_mst` `P1` 
								ON `P1`.`project_id`=`A`.`project_id_fk` 
								WHERE
									R.day_val BETWEEN "'.$startDate.'" AND "'.$endDate.'" AND `R`.`status_nm` = "Active" 
									AND R.dept_id_fk="'.$dept_id.'"
								GROUP BY `R`.`user_id_fk`,A.project_id_fk
								ORDER BY `R`.`user_id_fk`,equiv_xp DESC,P1.project_name
							) tab1
							GROUP BY    user_id_fk,project_id_fk,project_name,equiv_xp 
							HAVING row_number = 1)E 
				JOIN user_mst U 
				ON U.user_id=E.user_id_fk 
				 WHERE U.status_nm="Active" 
				ORDER BY E.project_name,U.full_name;';
			} else {
				$query = 'SELECT DISTINCT 
					A.project_id_fk, 
					P1.project_name,
					R.user_id_fk,
					U.full_name
				FROM
					`response_mst` `R` 
				INNER JOIN 
					`activity_mst` `A` 
				ON
					`R`.`activity_id_fk` = `A`.`activity_id` 
				AND 
					`A`.`status_nm` = "Active" 
				INNER JOIN 
					`project_mst` `P1` 
				ON 
					`P1`.`project_id`=`A`.`project_id_fk`
				INNER JOIN 
					`user_mst` `U`
				ON  
					`U`.`user_id` = `R`.`user_id_fk` 
				WHERE
					R.day_val BETWEEN "'.$startDate.'" AND "'.$endDate.'" 
				AND `R`.`status_nm` = "Active" AND R.dept_id_fk="'.$dept_id.'" 
				AND U.status_nm="Active" 
				ORDER BY P1.project_name,U.full_name ;';
			}
			
			$sqlQuery = $this->db->query($query);
			$resultSet = $sqlQuery->result_array();             
			
			if($resultSet)
			{
				return $resultSet;
			}
			else{
				return false;
			}
		}
		
		public function trend_status($file_nm, $date_start,$pro_sel_val, $date_end,$dept_opt)
		{
			$query='SELECT F.dates_v,@i:=log_cnt+@i AS log_cnt_rt,
				@j:=log_del_cnt+@j AS log_del_cnt_rt,
				@k:=audit_cnt+@k AS audit_cnt_rt,
				@l:=rev_cnt+@l AS rev_cnt_rt,
				@m:=conf_cnt+@m AS conf_cnt_rt
				FROM (
				SELECT E.dates_v,
						COUNT(CASE WHEN B.day_val=E.dates_v THEN 1 ELSE NULL END) AS log_cnt, 
				COUNT(CASE WHEN  B.day_val=E.dates_v AND B.ins_dt=B.day_val THEN 1 ELSE NULL END)  AS log_del_cnt , 
				COUNT(CASE WHEN B.audit_upd_dt=E.dates_v THEN 1 ELSE NULL END) AS audit_cnt , 
				COUNT(CASE WHEN B.review_upd_dt=E.dates_v THEN 1 ELSE NULL END) AS rev_cnt , 
				COUNT(CASE WHEN B.conf_upd_dt=E.dates_v THEN 1 ELSE NULL END) AS conf_cnt 
						FROM 
							(
						   SELECT
							   DATE_ADD("'.$date_start.'",INTERVAL(num.n -1) DAY) AS dates_v
						   FROM
							   numbers num
						   WHERE num.n <= 15
							) E
						JOIN (SELECT @i:=0,@j:=0,@k:=0,@l:=0,@m:=0) C ON 1=1 
						LEFT JOIN (SELECT R.response_id,R.day_val,DATE(CONVERT_TZ(R.ins_dt,"+00:00",'.$this->tx_cn.')) AS ins_dt,
										DATE(CONVERT_TZ(R.audit_upd_dt,"+00:00",'.$this->tx_cn.')) AS audit_upd_dt,
										(CASE WHEN R.review_upd_dt IS NULL AND R.conf_upd_dt IS NOT NULL 
										THEN DATE(CONVERT_TZ(R.conf_upd_dt,"+00:00",'.$this->tx_cn.')) 
										ELSE DATE(CONVERT_TZ(R.review_upd_dt,"+00:00",'.$this->tx_cn.')) END ) AS review_upd_dt,
										DATE(CONVERT_TZ(R.conf_upd_dt,"+00:00",'.$this->tx_cn.')) AS conf_upd_dt
							FROM  response_mst R 
							JOIN activity_mst A 
							ON R.activity_id_fk=A.activity_id AND A.status_nm="Active" 
							WHERE R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND R.status_nm="Active" 
							AND A.project_id_fk='.$pro_sel_val.' AND R.user_id_fk IS NOT NULL AND R.dept_id_fk IN ("'.$dept_opt.'") 
							)B 
							ON 1=1
						GROUP BY E.dates_v)F;';
			$sqlQuery = $this->db->query($query);
			$resultSet = $sqlQuery->result_array();             
			
			if($resultSet)
			{
				return $resultSet;
			}
			else{
				return false;
			}
		}
		
		public function pt_trend_status($file_nm, $date_start,$pt_emp, $date_end,$dept_opt)
		{
			$query='
			SELECT F.dates_v,@i:=log_cnt+@i AS log_cnt_rt,
				@j:=log_del_cnt+@j AS log_del_cnt_rt,
				@k:=audit_cnt+@k AS audit_cnt_rt,
				@l:=rev_cnt+@l AS rev_cnt_rt,
				@m:=conf_cnt+@m AS conf_cnt_rt
				FROM (
				SELECT E.dates_v,
						COUNT(CASE WHEN B.day_val=E.dates_v THEN 1 ELSE NULL END) AS log_cnt, 
				COUNT(CASE WHEN  B.day_val=E.dates_v AND B.ins_dt=B.day_val THEN 1 ELSE NULL END)  AS log_del_cnt , 
				COUNT(CASE WHEN B.audit_upd_dt=E.dates_v THEN 1 ELSE NULL END) AS audit_cnt , 
				COUNT(CASE WHEN B.review_upd_dt=E.dates_v THEN 1 ELSE NULL END) AS rev_cnt , 
				COUNT(CASE WHEN B.conf_upd_dt=E.dates_v THEN 1 ELSE NULL END) AS conf_cnt 
						FROM 
							(
						   SELECT
							   DATE_ADD("'.$date_start.'",INTERVAL(num.n -1) DAY) AS dates_v
						   FROM
							   numbers num
						   WHERE num.n <= 15
							) E 
						JOIN (SELECT @i:=0,@j:=0,@k:=0,@l:=0,@m:=0) C ON 1=1 
						LEFT JOIN (SELECT R.response_id,R.day_val,DATE(CONVERT_TZ(R.ins_dt,"+00:00",'.$this->tx_cn.')) AS ins_dt,
										DATE(CONVERT_TZ(R.audit_upd_dt,"+00:00",'.$this->tx_cn.')) AS audit_upd_dt,
										(CASE WHEN R.review_upd_dt IS NULL AND R.conf_upd_dt IS NOT NULL 
										THEN DATE(CONVERT_TZ(R.conf_upd_dt,"+00:00",'.$this->tx_cn.')) 
										ELSE DATE(CONVERT_TZ(R.review_upd_dt,"+00:00",'.$this->tx_cn.')) END ) AS review_upd_dt,
										DATE(CONVERT_TZ(R.conf_upd_dt,"+00:00",'.$this->tx_cn.')) AS conf_upd_dt
							FROM  response_mst R 
							JOIN activity_mst A 
							ON R.activity_id_fk=A.activity_id AND A.status_nm="Active" 
							WHERE R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND R.user_id_fk IN ('.$pt_emp.') AND R.status_nm="Active" 
							)B 
							ON 1=1
						GROUP BY E.dates_v
						)F ;';
			$sqlQuery = $this->db->query($query);
			$resultSet = $sqlQuery->result_array();             
			
			if($resultSet)
			{
				return $resultSet;
			}
			else{
				return false;
			}
		}
	
		
		 public function get_emp_leave($week_start,$pro_v,$dept,$start_dt,$end_dt){
						$qry='SELECT DISTINCT
					   dates_v AS day_val,
					   U.full_name,
					(CASE WHEN IFNULL(LT.is_half_day, 0) = 1 AND LT.conf_status="Approved" THEN "Present|Half Day" 
					WHEN IFNULL(LT.is_half_day, 0) = 0 AND LT.conf_status="Approved" THEN CONCAT("Absent","|",TY.leave_type_name)
					WHEN LT_comp.end_date = E.dates_v AND LT_comp.conf_status="Approved" THEN "Present|"
					WHEN LT_comp.start_date = E.dates_v AND LT_comp.conf_status="Approved" THEN "Absent|Comp Off"
					WHEN WO.conf_status = "Approved" THEN "Absent|Week Off"
					ELSE "Present|" END) AS l_stat
					FROM
					   (
					   SELECT
						   DATE_ADD("'.$week_start.'",INTERVAL(num.n -1) DAY) AS dates_v
					   FROM
						   numbers num
					   WHERE num.n <= 7
					) E

					INNER JOIN user_mst U ON
					   1=1
					LEFT JOIN comp_track LT ON
					   LT.user_id_fk = U.user_id
					AND E.dates_v BETWEEN LT.start_date AND LT.end_date
					AND IFNULL(LT.is_comp, 0)= 0
					  AND LT.status_nm = "Active"
					 LEFT JOIN leave_type_mst TY
					 ON TY.l_type_id=LT.leave_type
					LEFT JOIN emp_week_off WO ON
					   WO.user_id_fk = U.user_id AND WO.week_day = WEEKDAY(E.dates_v)
					AND E.dates_v BETWEEN WO.start_dt AND IFNULL(WO.end_dt, E.dates_v) AND WO.status_nm = "Active"
					LEFT JOIN comp_track LT_comp ON
					   LT_comp.user_id_fk = U.user_id
					AND
					(LT_comp.end_date = E.dates_v OR LT_comp.start_date = E.dates_v )
					AND LT_comp.is_comp = 1 AND LT_comp.status_nm = "Active"
					WHERE U.user_id in (
					SELECT R.user_id_fk
					FROM response_mst R
					INNER JOIN activity_mst A
					ON A.activity_id=R.activity_id_fk AND A.project_id_fk="'.$pro_v.'" AND A.status_nm = "Active" 
					WHERE  R.day_val BETWEEN "'.$start_dt.'" AND "'.$end_dt.'" AND  R.dept_id_fk ="'.$dept.'" 
					AND R.status_nm = "Active"
					)
						ORDER BY U.full_name,dates_v';
						
			$sqlQuery = $this->db->query($qry);
			$resultSet = $sqlQuery->result_array();             
			
			if($resultSet)
	          {
	            return $resultSet;
	          }
	          else{
				return false;
	          }
	 }
		
	public function fetchdept_data($u_id){
		$this->db->select(' D.dept_id,D.dept_name ',FALSE);
		$this->db->from('dept_mst D');						
		$this->db->join('user_mst U', 'U.user_id='.$u_id.' AND (IFNULL(U.dept_id_fk,0)=0 OR find_in_set(D.dept_id,U.dept_id_fk)<>0 ) ','inner');
		$this->db->where('D.status_nm', 'Active');
		$sqlQuery = $this->db->get();
		$resultSet = $sqlQuery->result_array();             
		
		if($resultSet)
          {
             return $resultSet;
          }
          else{
            return false;
          }
	}		
	
	public function sel_emp_leaves($u_id,$s_dt,$action){
		if($action=="Week Off")
		{
		$sql='SELECT  
				C.dt AS w_day,LM.l_type_id,LM.leave_type_name AS w_val,
					CASE WHEN E.conf_status="Approved" THEN LM.b_code ELSE "" END AS  w_b_code,
					CASE WHEN E.conf_status="Approved" THEN LM.color_code ELSE "" END AS  w_color_code
			FROM
				(
				SELECT
					ADDDATE("'.$s_dt.'", CD.day_num) dt
				FROM
					calendar CD
				WHERE
					CD.day_num <(DAY(LAST_DAY("'.$s_dt.'")))
			) C
			JOIN emp_week_off E ON
				C.dt BETWEEN E.start_dt AND IFNULL(E.end_dt, C.dt) AND WEEKDAY(C.dt) = E.week_day 
			JOIN `leave_type_mst` LM 
			ON LM.leave_type_name="'.$action.'" AND LM.status_nm="Active" 
			WHERE C.dt NOT IN ( SELECT `end_date` FROM `comp_track` WHERE `user_id_fk`='.$u_id.' AND is_comp=1 AND `conf_status`="Approved" 
			AND status_nm="Active" ) AND 
				E.user_id_fk ='.$u_id.' AND E.status_nm = "Active" ;';
		}
		if($action=="Declared Hols")
		{
		$sql='SELECT  
				C.dt AS w_day,C.hol_name AS w_val,
					LM.l_type_id,LM.b_code AS w_b_code,
					LM.color_code AS w_color_code 
			FROM
				(
				SELECT
					CD.day_val AS dt,CD.hol_name 
				FROM
					`nation_hols` CD 
				WHERE
					DATE_FORMAT(CD.day_val, "%Y-%m-01")=("'.$s_dt.'") AND CD.status_nm="Active" 
			)C 
			JOIN `leave_type_mst` LM 
			ON LM.leave_type_name="'.$action.'" AND LM.status_nm="Active" ; ';
		}
		if($action=="Others")
		{
			$sql='SELECT  
				C.dt AS w_day,LM.l_type_id,
				CASE WHEN IFNULL(E.is_half_day,0)=0 THEN LM.leave_type_name ELSE CONCAT("Half Day-",LM.leave_type_name) END AS w_val,
				CASE WHEN E.conf_status="Approved" THEN LM.b_code ELSE "" END AS w_b_code,
					 CASE WHEN E.conf_status="Approved" THEN LM.color_code ELSE "" END AS w_color_code 
			FROM
				(
				SELECT
					ADDDATE("'.$s_dt.'", CD.day_num) dt
				FROM
					calendar CD
				WHERE
					CD.day_num <(DAY(LAST_DAY("'.$s_dt.'")))
			) C
			JOIN comp_track E ON
				 E.user_id_fk ='.$u_id.' AND  C.dt BETWEEN E.start_date AND E.end_date AND 
				IFNULL(E.is_comp,0)=0   
			JOIN `leave_type_mst` LM 
			ON E.leave_type=LM.l_type_id AND LM.status_nm="Active" 
			WHERE 
				E.status_nm = "Active" ;';
		}
		if($action=="Comp Off")
		{		
		$sql='SELECT  
				C.dt AS w_day,LM.leave_type_name AS w_val,
					LM.l_type_id,
					CASE WHEN C.conf_status="Approved" THEN LM.b_code ELSE "" END AS w_b_code,
					 CASE WHEN C.conf_status="Approved" THEN LM.color_code ELSE "" END AS w_color_code 
			FROM
				(
				SELECT
					CD.start_date AS dt,CD.leave_type ,CD.conf_status
				FROM
					`comp_track` CD 
				WHERE CD.`user_id_fk`='.$u_id.' 
			AND CD.status_nm="Active" AND CD.is_comp=1 AND DATE_FORMAT(CD.start_date, "%Y-%m-01")=("'.$s_dt.'") 
			)C 
			JOIN `leave_type_mst` LM 
			ON C.leave_type=LM.l_type_id AND LM.status_nm="Active" ; ';
		}
		$sqlQuery = $this->db->query($sql);
		$resultSet = $sqlQuery->result_array();             
		
		if($resultSet)
          {
             return $resultSet;
          }
          else{
            return false;
          }
	}	
	
	public function select_allocations($user_id,$t_date1)
	{
		$currentDate =$t_date1;
		$sql='SELECT HA.dept_id_fk,
				HA.project_id_fk,
				HA.task_id_fk,
				HA.sub_task_id_fk,
				HA.job_id_fk ,D.dept_name,
				P.project_name,T.task_name,S.sub_name,J.job_name,
				CONCAT_WS("_",P.project_name,T.task_name,S.sub_name,J.job_name) AS lloc_name,
				GROUP_CONCAT(HM.mid_code) as mid_code
				from 
				(SELECT hier_act_log_id_fk FROM hier_user_alloc  
					WHERE alloc_user='.$user_id.'  AND IFNULL(progress,"") <> "Completed" 
				AND status_nm="Active" 
				)HU  
                INNER JOIN hier_act_log HA 
                ON HA.hier_act_log_id=HU.hier_act_log_id_fk 
				 AND IFNULL(HA.progress,"") <> "Completed" 
				 AND ("'.$currentDate.'"  >= HA.start_dt) 
				LEFT JOIN hierarchy_mst HM 
				ON HA.p_hierarchy_id=HM.hierarchy_id 
				LEFT JOIN project_mst P 
				ON P.project_id=HA.project_id_fk 
				LEFT JOIN task_mst T 
				ON T.task_id=HA.task_id_fk 
				LEFT JOIN sub_mst S 
				ON S.sub_id=HA.sub_task_id_fk 
				LEFT JOIN job_mst J 
				ON J.job_id=HA.job_id_fk
				LEFT JOIN dept_mst D 
				ON D.dept_id=HA.dept_id_fk 
				LEFT JOIN activity_mst A 
				ON A.project_id_fk=HA.project_id_fk 
					AND A.task_id_fk=HA.task_id_fk
					AND A.sub_id_fk=HA.sub_task_id_fk
					AND A.job_id_fk=HA.job_id_fk 
				WHERE  IFNULL(A.is_closed,"N")="N"  AND A.status_nm="Active" 
				GROUP BY HA.dept_id_fk, HA.project_id_fk, HA.task_id_fk, HA.sub_task_id_fk, HA.job_id_fk,
				CONCAT_WS("_",P.project_name,T.task_name,S.sub_name,J.job_name)
				;';
		
		$sqlQuery = $this->db->query($sql);
		$resultSet = $sqlQuery->result_array();             
		
		if($resultSet)
          {
             return $resultSet;
          }
          else{
            return false;
          }
	}
	public function sel_emp_view($u_id,$s_dt,$action){
		if($action=="Declared Hols")
		{
		$sql='SELECT  "Declared Hols" AS action,
		DATE_FORMAT(C.day_val, "%d-%b-%Y") AS start_date,
				DATE_FORMAT(C.day_val, "%d-%b-%Y")  AS end_date,C.hol_name AS w_val,
					LM.l_type_id,LM.b_code AS w_b_code,
					LM.color_code AS w_color_code ,"" AS conf_status,"" AS reason, "" AS wo_day,
					DATE_FORMAT(CONVERT_TZ(C.ins_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS ins_dt ,
					UI.full_name AS ins_user,
					DATE_FORMAT(CONVERT_TZ(C.upd_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS upd_dt ,	
					UP.full_name AS upd_user,0 AS edit 		
			FROM
				(
				SELECT
					*
				FROM
					`nation_hols` CD 
				WHERE
					CD.day_val="'.$s_dt.'" AND CD.status_nm="Active" 
			)C 
			JOIN `leave_type_mst` LM 
			ON LM.leave_type_name="'.$action.'" AND LM.status_nm="Active"  
			LEFT JOIN user_mst UI 
			ON UI.user_id=C.ins_user 
			LEFT JOIN user_mst UP 
			ON UP.user_id=C.upd_user ;';
		}
		if($action=="Others")
		{
			$sql='SELECT  C.comp_track_id,"Others" AS action,
			DATE_FORMAT(C.start_date, "%d-%b-%Y") AS start_date ,LM.l_type_id,LM.leave_type_name AS w_val,
					LM.b_code AS w_b_code,C.reason ,C.conf_status,
					DATE_FORMAT(C.end_date, "%d-%b-%Y") AS end_date ,
					C.is_half_day,
					LM.color_code AS w_color_code,
					DATE_FORMAT(CONVERT_TZ(C.ins_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS ins_dt ,
					UI.full_name AS ins_user,
					DATE_FORMAT(CONVERT_TZ(C.upd_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS upd_dt ,	
					UP.full_name AS upd_user ,CASE WHEN C.conf_status="Approved" THEN 0 ELSE 1 END AS edit 
			FROM
				(
				SELECT
					*
				FROM
					comp_track E
				WHERE E.user_id_fk ='.$u_id.' AND  "'.$s_dt.'" BETWEEN E.start_date AND E.end_date AND 
				IFNULL(E.is_comp,0)=0 AND E.status_nm = "Active"  
			)C			 
			JOIN `leave_type_mst` LM 
			ON C.leave_type=LM.l_type_id AND LM.status_nm="Active"  
			LEFT JOIN user_mst UI 
			ON UI.user_id=C.ins_user 
			LEFT JOIN user_mst UP 
			ON UP.user_id=C.upd_user ;';
		}
		if($action=="Comp Off")
		{		
		$sql='SELECT  C.comp_track_id,"Comp Off" AS action,
				DATE_FORMAT(C.start_date, "%d-%b-%Y") AS start_date ,LM.leave_type_name AS w_val,
					LM.l_type_id,LM.b_code AS w_b_code,C.conf_status,C.reason ,
					DATE_FORMAT(C.end_date, "%d-%b-%Y") AS end_date ,
					LM.color_code AS w_color_code
					,DATE_FORMAT(CONVERT_TZ(C.ins_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS ins_dt ,
					UI.full_name AS ins_user,
					DATE_FORMAT(CONVERT_TZ(C.upd_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS upd_dt ,	
					UP.full_name AS upd_user ,CASE WHEN C.conf_status="Approved" THEN 0 ELSE 1 END AS edit 
			FROM
				(
				SELECT
					*
				FROM
					`comp_track` CD 
				WHERE CD.`user_id_fk`='.$u_id.' 
			AND CD.status_nm="Active" AND CD.is_comp=1 AND "'.$s_dt.'" = CD.start_date  
			)C 
			JOIN `leave_type_mst` LM 
			ON C.leave_type=LM.l_type_id AND LM.status_nm="Active" 
			LEFT JOIN user_mst UI 
			ON UI.user_id=C.ins_user 
			LEFT JOIN user_mst UP 
			ON UP.user_id=C.upd_user
			; ';
		}
		
		// if($action=="Others")
		// {
		// $sql='SELECT  
				// C.day_val AS w_day,LM.leave_type_name AS w_val,
					// LM.l_type_id,LM.b_code AS w_b_code,
					// LM.color_code AS w_color_code,C.conf_status,C.reason ,C.work_day ,
					// DATE_FORMAT(C.ins_dt, "%d/%m/%y %k:%i:%s")  AS ins_dt ,
					// UI.full_name AS ins_user,
					// DATE_FORMAT(C.upd_dt, "%d/%m/%y %k:%i:%s")  AS upd_dt ,	
					// UP.full_name AS upd_user ,1 AS edit 
			// FROM
				// (
				// SELECT
					// *
				// FROM
					// `leave_track` CD 
				// WHERE CD.day_val="'.$s_dt.'"  AND CD.`user_id_fk`='.$u_id.' 
			// AND CD.status_nm="Active" 
			// )C 
			// JOIN `leave_type_mst` LM 
			// ON C.leave_type=LM.l_type_id AND LM.status_nm="Active" 
			// LEFT JOIN user_mst UI 
			// ON UI.user_id=C.ins_user 
			// LEFT JOIN user_mst UP 
			// ON UP.user_id=C.upd_user ;';
		// }
		$sqlQuery = $this->db->query($sql);
		$resultSet = $sqlQuery->result_array();             
		
		if($resultSet)
          {
             return $resultSet;
          }
          else{
            return false;
          }
	}
	
	public function sel_emp_w_off($u_id){
		
		$sql='SELECT  C.emp_week_off_id,C.week_day,
				DATE_FORMAT(C.start_dt, "%d-%b-%Y") AS start_dt,C.conf_status,
					DATE_FORMAT(C.end_dt, "%d-%b-%Y") AS end_dt ,
					DATE_FORMAT(CONVERT_TZ(C.ins_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS ins_dt ,
					UI.full_name AS ins_user,
					DATE_FORMAT(CONVERT_TZ(C.upd_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS upd_dt ,	
					UP.full_name AS upd_user 
			FROM
				(
				SELECT
					*
				FROM
					`emp_week_off` CD 
				WHERE CD.`user_id_fk`='.$u_id.' 
			AND CD.status_nm="Active" 
			)C 
			LEFT JOIN user_mst UI 
			ON UI.user_id=C.ins_user 
			LEFT JOIN user_mst UP 
			ON UP.user_id=C.upd_user
			; ';		
		$sqlQuery = $this->db->query($sql);
		$resultSet = $sqlQuery->result_array();             
		
		if($resultSet)
          {
             return $resultSet;
          }
          else{
            return false;
          }
	}
 	public function fetchMetrics($u_id,$role_id,$dept_opt){

				$day = date('Y-m-d');
				
				$day_prev=date('Y-m-d',strtotime($day . ' - 1 days'));
							$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}
		$date_start = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
        $date_end   = date('Y-m-d', strtotime($day . ' +' . (7 - $day_w) . ' days'));							
							
                    $this->db->select(' ROUND(IFNULL(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),0),2) AS week_xp_val, ROUND(IFNULL(SUM(CASE WHEN (R.day_val="'.$day_prev.'") THEN (IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))) ELSE 0 END ),0),2) AS today_xp ',FALSE);
					if($role_id!='1' && $role_id!='2')
					{
					$this->db->where('R.user_id_fk', $u_id);
					}
					if($role_id=='2' &&  $dept_opt)
					{
						if(is_array($dept_opt))
						{
						$dept_id=implode(",",$dept_opt);
						}else{
						$dept_id=$dept_opt;
						}			
						$this->db->where('R.dept_id_fk IN ('.$dept_id.') ',null,false);
					}
					$this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" ', null, false);
                 	$this->db->where('R.status_nm', 'Active');
					$res = $this->db->get('response_mst R');                
             if($res->result())
				  {
					 return $res->result();
				  }
				  else{
					return false;
				  }            
        }  
		
		public function login_suser_check($email){
					
					$sql="SELECT U.user_email AS em
					FROM `redirect_user` U 
					WHERE U.`user_email`= '".$email."' 
					AND U.`status_nm` = 'Active';";
					
                    $sqlQuery = $this->db->query($sql);
                    $resultSet = $sqlQuery->result_array();     

					
          if($resultSet)
          {
              return $resultSet;
          }
          else{
            return false;
          } 
        }
		
	public function login_suser($email){
					
					$sql="SELECT U.*,IFNULL(U.is_grading,'N') AS is_grad,
							(CASE WHEN HP.user_id_fk IS NULL THEN U.access_name ELSE CONCAT(U.access_name,'|Allocations|allocation-details|allocation_header') END) AS access_name_mod
					FROM `user_mst` U 
					LEFT JOIN (SELECT DISTINCT user_id_fk FROM user_permissions 
							   WHERE status_nm='Active' AND IFNULL(is_alloc,0)=1
							   )HP
					ON U.user_id=HP.user_id_fk 
					WHERE U.`user_login_name`= '".$email."' 
					AND U.`status_nm` = 'Active';";
					
                    $sqlQuery = $this->db->query($sql);
                    $resultSet = $sqlQuery->result_array();     

					
          if($resultSet)
          {
              return $resultSet;
          }
          else{
            return false;
          } 
        }
	
	public function load_milestone($dept_id,$act_id,$project_id){
		$sql='
SELECT M.milestone_id 
FROM `mile_act_mst` `M`
WHERE `M`.`dept_id_fk` = "'.$dept_id.'"
AND `M`.`status_nm` = "Active"
AND ( M.activity_id_fk IN ("'.$act_id.'") OR IFNULL(M.activity_id_fk,0)=0 ) 
AND (M.project_id_fk IN ("'.$project_id.'") OR IFNULL(M.project_id_fk,0)=0 )
UNION 
SELECT
    CONCAT_WS("",A.year_nm,B.syll_name,C.grade_name,D.subj_name,E.chapter_nm,F.sub_div_nm,G.div_no,H.sec_div_nm,I.sec_div_no) AS milestone_id
FROM
    (
    SELECT M.activity_id_fk,
        M.gen_id,
        SUBSTRING_INDEX(SUBSTRING_INDEX(M.year, ",", Num.n),",",-1) AS year_nm
    FROM
        numbers Num
    INNER JOIN mid_gen M 
	ON CHAR_LENGTH(M.year) - CHAR_LENGTH(REPLACE(M.year, ",", "")) >= Num.n -1 
	WHERE `M`.`dept_id_fk` = "'.$dept_id.'"
AND `M`.`status_nm` = "Active"
AND (M.activity_id_fk IN ("'.$act_id.'") OR IFNULL(M.activity_id_fk,0)=0 )
) A
JOIN(
    SELECT M.gen_id,
        SUBSTRING_INDEX(SUBSTRING_INDEX(M.syllabus, ",", Num.n),",",-1) AS syll_name
    FROM
        numbers Num
    INNER JOIN mid_gen M 
	ON CHAR_LENGTH(M.syllabus) - CHAR_LENGTH(REPLACE(M.syllabus, ",", "")) >= Num.n -1 
	WHERE `M`.`dept_id_fk` = "'.$dept_id.'"
AND `M`.`status_nm` = "Active"
AND (M.activity_id_fk IN ("'.$act_id.'") OR IFNULL(M.activity_id_fk,0)=0 ) 
	) B
ON B.gen_id = A.gen_id
LEFT JOIN(
    SELECT M.gen_id,
        SUBSTRING_INDEX(SUBSTRING_INDEX(M.grade, ",", Num.n),",",-1) AS grade_name
    FROM
        numbers Num
    INNER JOIN mid_gen M 
	ON CHAR_LENGTH(M.grade) - CHAR_LENGTH(REPLACE(M.grade, ",", "")) >= Num.n -1 
	WHERE `M`.`dept_id_fk` = "'.$dept_id.'"
AND `M`.`status_nm` = "Active"
AND (M.activity_id_fk IN ("'.$act_id.'") OR IFNULL(M.activity_id_fk,0)=0 ) 
	) C
ON C.gen_id = A.gen_id
LEFT JOIN(
    SELECT M.gen_id,
        SUBSTRING_INDEX(SUBSTRING_INDEX(M.subject, ",", Num.n),",",-1) AS subj_name
    FROM
        numbers Num
    INNER JOIN mid_gen M 
	ON CHAR_LENGTH(M.subject) - CHAR_LENGTH(REPLACE(M.subject, ",", "")) >= Num.n -1 
	WHERE `M`.`dept_id_fk` = "'.$dept_id.'"
AND `M`.`status_nm` = "Active"
AND (M.activity_id_fk IN ("'.$act_id.'") OR IFNULL(M.activity_id_fk,0)=0 )
	) D
ON D.gen_id = A.gen_id
LEFT JOIN(
    SELECT M.gen_id,
        SUBSTRING_INDEX(SUBSTRING_INDEX(M.chapter, ",", Num.n),",",-1) AS chapter_nm
    FROM
        numbers Num
    INNER JOIN mid_gen M 
	ON CHAR_LENGTH(M.chapter) - CHAR_LENGTH(REPLACE(M.chapter, ",", "")) >= Num.n -1 
	WHERE `M`.`dept_id_fk` = "'.$dept_id.'"
AND `M`.`status_nm` = "Active"
AND (M.activity_id_fk IN ("'.$act_id.'") OR IFNULL(M.activity_id_fk,0)=0 ) 
) E 
ON E.gen_id = A.gen_id 
LEFT JOIN(
SELECT M.gen_id,
        SUBSTRING_INDEX(SUBSTRING_INDEX(M.sub_div,  ",", Num.n),",",-1) AS sub_div_nm
    FROM
        numbers Num
    INNER JOIN mid_gen M 
	ON CHAR_LENGTH(M.sub_div) - CHAR_LENGTH(REPLACE(M.sub_div, ",", "")) >= Num.n -1 
	WHERE `M`.`dept_id_fk` = "'.$dept_id.'"
AND `M`.`status_nm` = "Active"
AND (M.activity_id_fk IN ("'.$act_id.'") OR IFNULL(M.activity_id_fk,0)=0 )
) F
ON F.gen_id = A.gen_id
LEFT JOIN(
    SELECT M.gen_id,
        SUBSTRING_INDEX(SUBSTRING_INDEX(M.div_num, ",", Num.n),",",-1) AS div_no
    FROM
        numbers Num
    INNER JOIN mid_gen M 
	ON CHAR_LENGTH(M.div_num) - CHAR_LENGTH(REPLACE(M.div_num, ",", "")) >= Num.n -1 
	WHERE `M`.`dept_id_fk` = "'.$dept_id.'"
AND `M`.`status_nm` = "Active"
AND (M.activity_id_fk IN ("'.$act_id.'") OR IFNULL(M.activity_id_fk,0)=0 ) 
) G
ON G.gen_id = A.gen_id
LEFT JOIN(
SELECT M.gen_id,
        SUBSTRING_INDEX(SUBSTRING_INDEX(M.sec_sub_div,  ",", Num.n),",",-1) AS sec_div_nm
    FROM
        numbers Num
    INNER JOIN mid_gen M 
	ON CHAR_LENGTH(M.sec_sub_div) - CHAR_LENGTH(REPLACE(M.sec_sub_div, ",", "")) >= Num.n -1 
	WHERE `M`.`dept_id_fk` = "'.$dept_id.'"
AND `M`.`status_nm` = "Active"
AND (M.activity_id_fk IN ("'.$act_id.'") OR IFNULL(M.activity_id_fk,0)=0 )
) H
ON H.gen_id = A.gen_id
LEFT JOIN(

    SELECT M.gen_id,
        SUBSTRING_INDEX(SUBSTRING_INDEX(M.sec_div_num, ",", Num.n),",",-1) AS sec_div_no
    FROM
        numbers Num
    INNER JOIN mid_gen M 
	ON CHAR_LENGTH(M.sec_div_num) - CHAR_LENGTH(REPLACE(M.sec_div_num, ",", "")) >= Num.n -1 
	WHERE `M`.`dept_id_fk` = "'.$dept_id.'"
AND `M`.`status_nm` = "Active"
AND (M.activity_id_fk IN ("'.$act_id.'") OR IFNULL(M.activity_id_fk,0)=0 ) 
) I
ON I.gen_id = A.gen_id 
 ORDER BY milestone_id ; ';
		$sqlQuery = $this->db->query($sql);
		
		$resultSet = $sqlQuery->result_array();				
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	}
	
	public function getAllCategories($deptId)
	{
		$currentDate = date('Y-m-d');
		$this->db->select(' distinct task_mst.task_id AS p_id,IFNULL(task_mst.task_name,"")  AS p_nm  ',FALSE);
		$this->db->from('task_mst');
		$this->db->join('activity_mst', 'task_mst.task_id = activity_mst.task_id_fk AND activity_mst.status_nm="Active" AND activity_mst.is_closed IS NULL ','inner');
		$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
		$this->db->join('std_tgt_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND "'.$currentDate.'" >= std_tgt_mst.start_dt AND std_tgt_mst.end_dt IS NULL AND std_tgt_mst.status_nm="Active" ','inner');
		$this->db->join('project_mst', 'project_mst.project_id = activity_mst.project_id_fk AND project_mst.status_nm="Active" ','inner');
		$this->db->where('task_mst.dept_id_fk', $deptId);
		$this->db->where('task_mst.status_nm', 'Active');
		$this->db->order_by('task_mst.task_name', 'ASC');
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}
	
	public function load_task_sel($p,$sel_0_ID,$sel_1_ID, $sel_2_ID, $sel_3_ID, $sel_4_ID,$dateID){
		if($p=='a_sel_1')
		{
			$this->db->select('  P.project_id AS p_id ,P.project_name AS p_nm',FALSE);
		}else if($p=='a_sel_2')
		{
			$this->db->select('T.task_id AS p_id ,T.task_name  AS p_nm',FALSE);
		}else if($p=='a_sel_3')
		{
			$this->db->select('  S.sub_id AS p_id ,S.sub_name  AS p_nm',FALSE);
		}else if($p=='a_sel_4')
		{
			$this->db->select(' J.job_id AS p_id ,J.job_name  AS p_nm',FALSE);
		}	
		
		$this->db->from('activity_mst A');						
		$this->db->join('work_unit_mst WU', 'WU.wu_id = A.wu_id_fk AND WU.status_nm="Active" ','inner');
		$this->db->join('std_tgt_mst STD', 'STD.activity_id_fk=A.activity_id AND "'.$dateID.'" >= STD.start_dt AND STD.end_dt IS NULL AND STD.status_nm="Active" ','inner');
		$this->db->join('project_mst P', 'P.project_id = A.project_id_fk AND P.status_nm="Active" ','inner');						
		$this->db->join('task_mst T', 'T.task_id = A.task_id_fk AND T.status_nm="Active" ','inner');
		$this->db->join('sub_mst S', 'S.sub_id = A.sub_id_fk AND S.status_nm="Active" ','inner');
		$this->db->join('job_mst J', 'J.job_id = A.job_id_fk AND J.status_nm="Active"   ','inner');
		$this->db->where('A.status_nm', 'Active');
		$this->db->where(' A.is_closed IS NULL ', null,FALSE);						
		$this->db->where('A.dept_id_fk',$sel_0_ID);						
		if($sel_1_ID)
		{						
		$this->db->where('P.project_id', $sel_1_ID);
		}
		if($sel_2_ID)
		{
		$this->db->where('T.task_id', $sel_2_ID);
		}						
		if($sel_3_ID)
		{												
		$this->db->where('S.sub_id', $sel_3_ID);
		}						
		 if($sel_4_ID)
		{												
		$this->db->where('J.job_id',$sel_4_ID);
		}
	$sqlQuery = $this->db->get();
	$resultSet = $sqlQuery->result_array();				
  if($resultSet)
  {
	  return $resultSet;
  }
  else{
	return false;
  } 
					
}
					
	public function load_select($p,$sel_0_ID,$sel_1_ID, $sel_2_ID, $sel_3_ID, $sel_4_ID,$dateID){
				if($p=='sel_1')
					{
						$this->db->select('  DISTINCT project_mst.project_id AS p_id,project_mst.project_name  AS p_nm ',FALSE);
						$this->db->from('project_mst');
						$this->db->join('activity_mst', 'project_mst.project_id = activity_mst.project_id_fk AND activity_mst.status_nm="Active" AND IFNULL(activity_mst.is_closed,"N")="N"','inner');
						$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
						$this->db->join('std_tgt_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND "'.$dateID.'" >= std_tgt_mst.start_dt AND std_tgt_mst.end_dt IS NULL AND std_tgt_mst.status_nm="Active" ','inner');
						$this->db->where('project_mst.status_nm', 'Active');
						$this->db->where('project_mst.dept_id_fk',$sel_0_ID);
						$this->db->order_by('project_mst.project_name', 'ASC');				
					}
					if($p=='sel_2')
					{					
						$this->db->select(' distinct task_mst.task_id AS p_id,IFNULL(task_mst.task_name,"")  AS p_nm  ',FALSE);
						$this->db->from('task_mst');
						$this->db->join('activity_mst', 'task_mst.task_id = activity_mst.task_id_fk AND activity_mst.status_nm="Active" AND activity_mst.is_closed IS NULL ','inner');
						$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
						$this->db->join('std_tgt_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND "'.$dateID.'" >= std_tgt_mst.start_dt AND std_tgt_mst.end_dt IS NULL AND std_tgt_mst.status_nm="Active" ','inner');
						$this->db->join('project_mst', 'project_mst.project_id = activity_mst.project_id_fk AND project_mst.status_nm="Active" ','inner');
						 $this->db->where('project_mst.project_id', $sel_1_ID);
						 $this->db->where('task_mst.dept_id_fk',$sel_0_ID);
						 $this->db->where('task_mst.status_nm', 'Active');
						 $this->db->order_by('task_mst.task_name', 'ASC');
					}
					else if($p=='sel_3')
					{
						$this->db->select(' distinct sub_mst.sub_id AS p_id ,IFNULL(sub_mst.sub_name,"")  AS p_nm  ',FALSE);
						$this->db->from('sub_mst');
						$this->db->join('activity_mst', 'sub_mst.sub_id = activity_mst.sub_id_fk AND activity_mst.status_nm="Active" AND activity_mst.is_closed IS NULL','inner');
						$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
						$this->db->join('std_tgt_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND "'.$dateID.'" >= std_tgt_mst.start_dt AND std_tgt_mst.end_dt IS NULL AND std_tgt_mst.status_nm="Active" ','inner');
						$this->db->join('project_mst', 'project_mst.project_id = activity_mst.project_id_fk AND project_mst.status_nm="Active" ','inner');
						$this->db->join('task_mst', 'task_mst.task_id = activity_mst.task_id_fk AND task_mst.status_nm="Active" ','inner');
						$this->db->where('sub_mst.dept_id_fk',$sel_0_ID);
						$this->db->where('task_mst.task_id', $sel_2_ID);
						 $this->db->where('project_mst.project_id', $sel_1_ID);
						 $this->db->where('sub_mst.status_nm', 'Active');
						 $this->db->order_by('sub_mst.sub_name', 'ASC');
					}
					else if($p=='sel_4')
					{						
						$this->db->select(' distinct job_mst.job_id AS p_id ,IFNULL(job_mst.job_name,"")  AS p_nm  ',FALSE);
						$this->db->from('job_mst');
						$this->db->join('activity_mst', 'job_mst.job_id = activity_mst.job_id_fk AND activity_mst.status_nm="Active" AND activity_mst.is_closed IS NULL ','inner');
						$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
						$this->db->join('std_tgt_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND "'.$dateID.'" >= std_tgt_mst.start_dt AND std_tgt_mst.end_dt IS NULL AND std_tgt_mst.status_nm="Active" ','inner');
						$this->db->join('project_mst', 'project_mst.project_id = activity_mst.project_id_fk AND project_mst.status_nm="Active" ','inner');
						$this->db->join('task_mst', 'task_mst.task_id = activity_mst.task_id_fk AND task_mst.status_nm="Active" ','inner');
						$this->db->join('sub_mst', 'sub_mst.sub_id = activity_mst.sub_id_fk AND sub_mst.status_nm="Active" ','inner');
						$this->db->where('job_mst.dept_id_fk',$sel_0_ID);
						$this->db->where('task_mst.task_id', $sel_2_ID);
						 $this->db->where('project_mst.project_id', $sel_1_ID);
						 $this->db->where('sub_mst.sub_id', $sel_3_ID);
						 $this->db->where('job_mst.status_nm', 'Active');
						 $this->db->order_by('job_mst.job_name', 'ASC');
						
					}else if($p=='a_sel_cont')
					{						
						$this->db->select('  P.project_id ,P.project_name,T.task_id,T.task_name ,S.sub_id ,S.sub_name,J.job_id ,J.job_name ',FALSE);						
						$this->db->from('activity_mst A');						
						$this->db->join('work_unit_mst WU', 'WU.wu_id = A.wu_id_fk AND WU.status_nm="Active" ','inner');
						$this->db->join('std_tgt_mst STD', 'STD.activity_id_fk=A.activity_id AND "'.$dateID.'" >= STD.start_dt AND STD.end_dt IS NULL AND STD.status_nm="Active" ','inner');
						$this->db->join('project_mst P', 'P.project_id = A.project_id_fk AND P.status_nm="Active" ','inner');						
						$this->db->join('task_mst T', 'T.task_id = A.task_id_fk AND T.status_nm="Active" ','inner');
						$this->db->join('sub_mst S', 'S.sub_id = A.sub_id_fk AND S.status_nm="Active" ','inner');
						$this->db->join('job_mst J', 'J.job_id = A.job_id_fk AND J.status_nm="Active"   ','inner');
						$this->db->where('A.status_nm', 'Active');
						$this->db->where(' IFNULL(A.is_closed,"N")="N"', null,FALSE);						
						$this->db->where('A.dept_id_fk',$sel_0_ID);						
						if($sel_1_ID)
						{						
						$this->db->where('P.project_id', $sel_1_ID);
						}
						if($sel_2_ID)
						{
						$this->db->where('T.task_id', $sel_2_ID);
						}						
						if($sel_3_ID)
						{												
						$this->db->where('S.sub_id', $sel_3_ID);
						}						
						 if($sel_4_ID)
						{												
						$this->db->where('J.job_id',$sel_4_ID);
						}
					}else if($p=='load_info')
				{
					$this->db->select(' activity_mst.activity_id AS p_id ,work_unit_mst.wu_id,work_unit_mst.wu_name ,std_tgt_mst.tgt_val',FALSE);
						$this->db->from('activity_mst');
						$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
						$this->db->join('std_tgt_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND "'.$dateID.'" >= std_tgt_mst.start_dt AND std_tgt_mst.end_dt IS NULL AND std_tgt_mst.status_nm="Active" ','inner');
							
						$this->db->where('activity_mst.dept_id_fk',$sel_0_ID);
						 $this->db->where('activity_mst.status_nm', 'Active');				
						 $this->db->where('activity_mst.is_closed IS NULL', null, false);				
						if(!empty($sel_1_ID))
						{
							$this->db->join('project_mst', 'project_mst.project_id = activity_mst.project_id_fk AND project_mst.status_nm="Active" ','inner');
							$this->db->where('project_mst.project_id', $sel_1_ID);
						}
						if(!empty($sel_2_ID))
						{
							$this->db->join('task_mst', 'task_mst.task_id = activity_mst.task_id_fk AND task_mst.status_nm="Active" ','inner');
							$this->db->where('task_mst.task_id', $sel_2_ID);
						}
						if(!empty($sel_3_ID))
						{
							$this->db->join('sub_mst', 'sub_mst.sub_id = activity_mst.sub_id_fk AND sub_mst.status_nm="Active" ','inner');
							$this->db->where('sub_mst.sub_id', $sel_3_ID);
						}
						if(!empty($sel_4_ID))
						{
							$this->db->join('job_mst', 'job_mst.job_id = activity_mst.job_id_fk AND job_mst.status_nm="Active" ','inner');							
							$this->db->where('job_mst.job_id', $sel_4_ID);	
						}
					}
					$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();				
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	}
	public function load_select_media($p,$sel_0_ID,$sel_1_ID, $sel_2_ID, $sel_3_ID, $sel_4_ID,$dateID){
				if($p=='sel_1')
					{	
						$this->db->select('  DISTINCT project_mst.project_id AS p_id,project_mst.project_name  AS p_nm ',FALSE);
						$this->db->from('project_mst');
						$this->db->join('activity_mst', 'project_mst.project_id = activity_mst.project_id_fk AND activity_mst.status_nm="Active" ','inner');
						$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
						$this->db->join('std_tgt_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND "'.$dateID.'" >= std_tgt_mst.start_dt AND std_tgt_mst.end_dt IS NULL AND std_tgt_mst.status_nm="Active" ','inner');
						if($sel_2_ID){$this->db->join('task_mst', 'task_mst.task_id = activity_mst.task_id_fk AND task_mst.status_nm="Active" ','inner');}
						$this->db->where('project_mst.status_nm', 'Active');
						$this->db->where('project_mst.dept_id_fk',$sel_0_ID);
						if($sel_2_ID){$this->db->where('task_mst.task_id', $sel_2_ID);}
						$this->db->order_by('project_mst.project_name', 'ASC');				
					}
					if($p=='sel_2')
					{					
						$this->db->select(' distinct task_mst.task_id AS p_id,IFNULL(task_mst.task_name,"")  AS p_nm  ',FALSE);
						$this->db->from('task_mst');
						$this->db->join('activity_mst', 'task_mst.task_id = activity_mst.task_id_fk AND activity_mst.status_nm="Active" AND activity_mst.is_closed IS NULL ','inner');
						$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
						$this->db->join('std_tgt_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND "'.$dateID.'" >= std_tgt_mst.start_dt AND std_tgt_mst.end_dt IS NULL AND std_tgt_mst.status_nm="Active" ','inner');
						$this->db->join('project_mst', 'project_mst.project_id = activity_mst.project_id_fk AND project_mst.status_nm="Active" ','inner');
						if($sel_1_ID){$this->db->where('project_mst.project_id', $sel_1_ID);}
						 $this->db->where('task_mst.dept_id_fk',$sel_0_ID);
						 $this->db->where('task_mst.status_nm', 'Active');
						 $this->db->order_by('task_mst.task_name', 'ASC');
					}
					else if($p=='sel_3')
					{	
						$this->db->select(' distinct sub_mst.sub_id AS p_id ,IFNULL(sub_mst.sub_name,"")  AS p_nm  ',FALSE);
						$this->db->from('sub_mst');
						$this->db->join('activity_mst', 'sub_mst.sub_id = activity_mst.sub_id_fk AND activity_mst.status_nm="Active" AND activity_mst.is_closed IS NULL','inner');
						$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
						$this->db->join('std_tgt_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND "'.$dateID.'" >= std_tgt_mst.start_dt AND std_tgt_mst.end_dt IS NULL AND std_tgt_mst.status_nm="Active" ','inner');
						$this->db->join('project_mst', 'project_mst.project_id = activity_mst.project_id_fk AND project_mst.status_nm="Active" ','inner');
						$this->db->join('task_mst', 'task_mst.task_id = activity_mst.task_id_fk AND task_mst.status_nm="Active" ','inner');
						$this->db->where('sub_mst.dept_id_fk',$sel_0_ID);
						$this->db->where('task_mst.task_id', $sel_2_ID);
						if($sel_1_ID){$this->db->where('project_mst.project_id', $sel_1_ID);}
						 $this->db->where('sub_mst.status_nm', 'Active');
						 $this->db->order_by('sub_mst.sub_name', 'ASC');
					}
					else if($p=='sel_4')
					{						
						$this->db->select(' distinct job_mst.job_id AS p_id ,IFNULL(job_mst.job_name,"")  AS p_nm  ',FALSE);
						$this->db->from('job_mst');
						$this->db->join('activity_mst', 'job_mst.job_id = activity_mst.job_id_fk AND activity_mst.status_nm="Active" AND activity_mst.is_closed IS NULL ','inner');
						$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
						$this->db->join('std_tgt_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND "'.$dateID.'" >= std_tgt_mst.start_dt AND std_tgt_mst.end_dt IS NULL AND std_tgt_mst.status_nm="Active" ','inner');
						$this->db->join('project_mst', 'project_mst.project_id = activity_mst.project_id_fk AND project_mst.status_nm="Active" ','inner');
						$this->db->join('task_mst', 'task_mst.task_id = activity_mst.task_id_fk AND task_mst.status_nm="Active" ','inner');
						$this->db->join('sub_mst', 'sub_mst.sub_id = activity_mst.sub_id_fk AND sub_mst.status_nm="Active" ','inner');
						$this->db->where('job_mst.dept_id_fk',$sel_0_ID);
						$this->db->where('task_mst.task_id', $sel_2_ID);
						if($sel_1_ID){$this->db->where('project_mst.project_id', $sel_1_ID);}
						 $this->db->where('sub_mst.sub_id', $sel_3_ID);
						 $this->db->where('job_mst.status_nm', 'Active');
						 $this->db->order_by('job_mst.job_name', 'ASC');
						
					}else if($p=='load_info')
				{
					$this->db->select(' activity_mst.activity_id AS p_id ,work_unit_mst.wu_id,work_unit_mst.wu_name ,std_tgt_mst.tgt_val',FALSE);
						$this->db->from('activity_mst');
						$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
						$this->db->join('std_tgt_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND "'.$dateID.'" >= std_tgt_mst.start_dt AND std_tgt_mst.end_dt IS NULL AND std_tgt_mst.status_nm="Active" ','inner');
							
						$this->db->where('activity_mst.dept_id_fk',$sel_0_ID);
						 $this->db->where('activity_mst.status_nm', 'Active');				
						 $this->db->where('activity_mst.is_closed IS NULL', null, false);				
						if(!empty($sel_1_ID))
						{
							$this->db->join('project_mst', 'project_mst.project_id = activity_mst.project_id_fk AND project_mst.status_nm="Active" ','inner');
							$this->db->where('project_mst.project_id', $sel_1_ID);
						}
						if(!empty($sel_2_ID))
						{
							$this->db->join('task_mst', 'task_mst.task_id = activity_mst.task_id_fk AND task_mst.status_nm="Active" ','inner');
							$this->db->where('task_mst.task_id', $sel_2_ID);
						}
						if(!empty($sel_3_ID))
						{
							$this->db->join('sub_mst', 'sub_mst.sub_id = activity_mst.sub_id_fk AND sub_mst.status_nm="Active" ','inner');
							$this->db->where('sub_mst.sub_id', $sel_3_ID);
						}
						if(!empty($sel_4_ID))
						{
							$this->db->join('job_mst', 'job_mst.job_id = activity_mst.job_id_fk AND job_mst.status_nm="Active" ','inner');							
							$this->db->where('job_mst.job_id', $sel_4_ID);	
						}
					}
					$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();				
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	}

		 public function sel_pro_data($pids,$dept_opt){
						$this->db->select('  DISTINCT project_mst.project_id AS sel_1_id,project_mst.project_name  AS sel_1_name ',FALSE);
						$this->db->from('project_mst');
						$this->db->join('activity_mst', 'project_mst.project_id = activity_mst.project_id_fk AND activity_mst.status_nm="Active" ','inner');
						//$this->db->join('response_mst', 'response_mst.activity_id_fk = activity_mst.activity_id AND response_mst.status_nm="Active" ','inner');
						$this->db->where('project_mst.dept_id_fk', $dept_opt);
						 $this->db->where('project_mst.status_nm', 'Active');
						 if($pids!='0' && ($pids))
							 {
								 $key=explode(",",$pids);
								 $this->db->where_in('project_mst.project_id', $key);							
							 }
						 $this->db->order_by('project_mst.project_name', 'ASC');
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 public function pt_sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt){
	 //if($file_nm=='Reports' || $file_nm=='PT_Report_Card' || $file_nm=='PT_Report_Card')
				{ 
						$this->db->select('  user_mst.user_id AS sel_1_id, user_mst.full_name AS sel_1_name 
												FROM `user_mst` 
												WHERE user_mst.user_id='.$u_id.'  
												UNION 
												SELECT DISTINCT user_mst.user_id AS sel_1_id,user_mst.full_name  AS sel_1_name ',FALSE);
						$this->db->from('user_mst'); 
						
						if($role_id==4 )
						{
						$this->db->join('response_mst', 'response_mst.user_id_fk = user_mst.user_id 
						AND response_mst.day_val >= "'.$date_start.'" 
						AND response_mst.day_val <= "'.$date_end.'"  AND response_mst.status_nm="Active" ','inner');
						$this->db->join('activity_mst A', 'A.activity_id=response_mst.activity_id_fk AND A.status_nm="Active" ','inner');
							 if($pids!='0' && ($pids))
							 {
								 $key=explode(",",$pids);
								 $this->db->where_in('A.project_id_fk', $key);							
							 }
						}
						if($role_id==2)
						{
						if(is_array($dept_opt))
								{
								foreach($dept_opt AS $dept_id)
								{
									$this->db->where(' find_in_set('.$dept_id.',user_mst.dept_id_fk)<>0 ', null,false);
								}
								}else{
								$this->db->where(' find_in_set('.$dept_opt.',user_mst.dept_id_fk)<>0 ', null,false);
								}								
						}
						
						if($role_id==6|| $role_id==5)
						{
						$this->db->where('user_mst.user_id', $u_id);	
						}
						 $this->db->where('user_mst.status_nm', 'Active');						
						$this->db->order_by('sel_1_name', 'ASC');
						 
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
				}	
				
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 public function sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_id){
		 if($file_nm=='apply_work_request' || $file_nm=='apply_od_request')
						 {
						$this->db->select('  DISTINCT user_mst.user_id AS sel_1_id,user_mst.full_name  AS sel_1_name ',FALSE);
						$this->db->from('user_mst');
						 $this->db->where('user_mst.status_nm', 'Active');
						 // if($role_id!='1' && $role_id!='2')
							// {
							// $this->db->where_in('user_mst.manager_id', $u_id);
							// }						
							
								$where=' (IFNULL(user_mst.dept_id_fk,0)=0 OR find_in_set('.$dept_id.',user_mst.dept_id_fk)<>0 ) ';
							$this->db->where($where);
							$this->db->where('user_mst.role_id_fk NOT IN (1) ',null,false);
							
						$this->db->order_by('user_mst.full_name', 'ASC');
						$sqlQuery = $this->db->get();
						$resultSet = $sqlQuery->result_array();
						 }
		 if($file_nm=='Reportees')
						 {
						$this->db->select('  DISTINCT user_mst.user_id AS sel_1_id,user_mst.full_name  AS sel_1_name ',FALSE);
						$this->db->from('user_mst');
						 $this->db->where('user_mst.status_nm', 'Active');
						 if($role_id!='1' && $role_id!='2')
							{
							$this->db->where_in('user_mst.manager_id', $u_id);
							}						
							if($dept_id)
							{
								$where=' (IFNULL(user_mst.dept_id_fk,0)=0';
								if(is_array($dept_id))
								{
								foreach($dept_id AS $dept_opt)
								{
								$where=$where.' OR find_in_set('.$dept_opt.',user_mst.dept_id_fk)<>0 ';
								}
								}else{
								$where=$where.' OR find_in_set('.$dept_id.',user_mst.dept_id_fk)<>0 ';
								}
								$where=$where.') ';
							$this->db->where($where);
							}
						$this->db->order_by('user_mst.full_name', 'ASC');
						$sqlQuery = $this->db->get();
						$resultSet = $sqlQuery->result_array();
						 }
		if($file_nm=='R_Leave_Confirm')
						 {
						$this->db->select('  DISTINCT user_mst.user_id AS sel_1_id,user_mst.full_name  AS sel_1_name ',FALSE);
						$this->db->from('user_mst');
						 $this->db->where('user_mst.status_nm', 'Active');
						 if($role_id!='1' && $role_id!='2')
							{
							$this->db->where_in('user_mst.manager_id', $u_id);
							}
							
							if($dept_id)
							{
								$where=' (IFNULL(user_mst.dept_id_fk,0)=0';
								if(is_array($dept_id))
								{
								foreach($dept_id AS $dept_opt)
								{
								$where=$where.' OR find_in_set('.$dept_opt.',user_mst.dept_id_fk)<>0 ';
								}
								}else{
								$where=$where.' OR find_in_set('.$dept_id.',user_mst.dept_id_fk)<>0 ';
								}
								$where=$where.') ';
							$this->db->where($where);
							}
						$this->db->order_by('user_mst.full_name', 'ASC');
						$sqlQuery = $this->db->get();
						$resultSet = $sqlQuery->result_array();
						 }	
				if($file_nm=='Po_Leave_Confirm')
						 {
						$this->db->select('  DISTINCT user_mst.user_id AS sel_1_id,user_mst.full_name  AS sel_1_name ',FALSE);
						$this->db->from('user_mst');
						 $this->db->where('user_mst.status_nm', 'Active');
						 if($role_id!='1' && $role_id!='2')
							{
							$this->db->where_in('user_mst.manager_id', $u_id);
							}
							
							if($dept_id)
							{
								$where=' (IFNULL(user_mst.dept_id_fk,0)=0';
								if(is_array($dept_id))
								{
								foreach($dept_id AS $dept_opt)
								{
								$where=$where.' OR find_in_set('.$dept_opt.',user_mst.dept_id_fk)<>0 ';
								}
								}else{
								$where=$where.' OR find_in_set('.$dept_id.',user_mst.dept_id_fk)<>0 ';
								}
								$where=$where.') ';
							$this->db->where($where);
							}
						$this->db->order_by('user_mst.full_name', 'ASC');
						$sqlQuery = $this->db->get();
						$resultSet = $sqlQuery->result_array();
						 }						 
				
		 if($file_nm=='R_Breakdown')
						 {
						$this->db->select('  DISTINCT user_mst.user_id AS sel_1_id,user_mst.full_name  AS sel_1_name ',FALSE);
						$this->db->from('user_mst');
						 $this->db->where('user_mst.status_nm', 'Active');
						 if($role_id!='1' && $role_id!='2')
							{
							$this->db->where_in('user_mst.manager_id', $u_id);
							}
							if($dept_id)
							{
								$where=' (IFNULL(user_mst.dept_id_fk,0)=0';
								if(is_array($dept_id))
								{
								foreach($dept_id AS $dept_opt)
								{
								$where=$where.' OR find_in_set('.$dept_opt.',user_mst.dept_id_fk)<>0 ';
								}
								}else{
								$where=$where.' OR find_in_set('.$dept_id.',user_mst.dept_id_fk)<>0 ';
								}
								$where=$where.') ';
							$this->db->where($where);
							}
						$this->db->order_by('user_mst.full_name', 'ASC');
						$sqlQuery = $this->db->get();
						$resultSet = $sqlQuery->result_array();
						 }
			if($file_nm=='R_Team_Graphs')
						 {
						$this->db->select('  DISTINCT user_mst.user_id AS sel_1_id,user_mst.full_name  AS sel_1_name ',FALSE);
						$this->db->from('user_mst');
						 $this->db->where('user_mst.status_nm', 'Active');
						 if($role_id!='1' && $role_id!='2')
							{
							$this->db->where_in('user_mst.manager_id', $u_id);
							}
							if($dept_id)
							{
								$where=' (IFNULL(user_mst.dept_id_fk,0)=0';
								if(is_array($dept_id))
								{
								foreach($dept_id AS $dept_opt)
								{
								$where=$where.' OR find_in_set('.$dept_opt.',user_mst.dept_id_fk)<>0 ';
								}
								}else{
								$where=$where.' OR find_in_set('.$dept_id.',user_mst.dept_id_fk)<>0 ';
								}
								$where=$where.') ';
							$this->db->where($where);
							}
						$this->db->order_by('user_mst.full_name', 'ASC');
						$sqlQuery = $this->db->get();
						$resultSet = $sqlQuery->result_array();
						 }
				if($file_nm=='Employee_Info')
				{ 
						$this->db->select('  DISTINCT user_mst.user_id AS sel_1_id,user_mst.full_name  AS sel_1_name ',FALSE);
						$this->db->from('user_mst');
						$this->db->join('response_mst', 'response_mst.user_id_fk = user_mst.user_id AND response_mst.day_val >= "'.$date_start.'" AND response_mst.day_val <= "'.$date_end.'"  AND response_mst.status_nm="Active" ','inner');
						$this->db->join('activity_mst A', 'A.activity_id=response_mst.activity_id_fk AND A.status_nm="Active" ','inner');
						if(is_array($dept_id))
						{
						$dept_opt=implode(",",$dept_id);
						}else{
						$dept_opt=$dept_id;
						}
						$this->db->where('A.dept_id_fk IN ('.$dept_opt.') ',null,false);
						 $this->db->where('user_mst.status_nm', 'Active');
							 if($pids!='0' && ($pids))
							 {
								 $key=explode(",",$pids);
								 $this->db->where_in('A.project_id_fk', $key);							
							 }
						$this->db->order_by('user_mst.full_name', 'ASC');
						 
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
				}	
				
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	
	 public function sel_leave_type($tbl,$action=null){
		 
			if($action=="dropdown")
			{
				$this->db->where('dropdown', 'Y');				
			}
				$sqlQuery = $this->db->get($tbl);
				$resultSet = $sqlQuery->result_array();
		  if($resultSet)
		  {
			  return $resultSet;
		  }
		  else{
			return false;
		  } 
	 }	
	 
	 
	  public function sel1_data($t_date1,$dept_opt){
						$this->db->select('  DISTINCT project_mst.project_id AS sel_1_id,project_mst.project_name  AS sel_1_name ',FALSE);
						$this->db->from('project_mst');
						$this->db->join('activity_mst', 'project_mst.project_id = activity_mst.project_id_fk AND activity_mst.status_nm="Active" AND IFNULL(activity_mst.is_closed,"N")="N"','inner');
						$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
						$this->db->join('std_tgt_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND "'.$t_date1.'" >= std_tgt_mst.start_dt AND std_tgt_mst.end_dt IS NULL AND std_tgt_mst.status_nm="Active" ','inner');
						$this->db->where('project_mst.status_nm', 'Active');
						$this->db->where('project_mst.dept_id_fk',$dept_opt);
						$this->db->order_by('project_mst.project_name', 'ASC');
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
		  if($resultSet)
		  {
			  return $resultSet;
		  }
		  else{
			return false;
		  } 
	 }	
	 
	 
	
	 
	 
	 public function load_target_boots($file_nm,$pro,$dept_opt,$stat)
	{
		
	$this->db->select('  DISTINCT CASE WHEN (work_unit_mst.is_quantify="Y" AND activity_mst.is_closed IS NULL ) THEN 1 ELSE 0 END AS state,std_tgt_mst.std_tgt_id AS id ,work_unit_mst.wu_name,activity_mst.activity_name, IFNULL(PT.p_tgt,"")  AS pre_tgt_val,
	std_tgt_mst.std_tgt_id ,std_tgt_mst.change_desc ,std_tgt_mst.tgt_val,activity_mst.activity_id,user_mst.full_name,
		ACH.achieved_tgt,UNIX_TIMESTAMP(std_tgt_mst.start_dt) AS day_val,UNIX_TIMESTAMP(std_tgt_mst.ins_dt)  AS ins_dt ',FALSE);
						$this->db->from('std_tgt_mst');
						$this->db->join('activity_mst', 'std_tgt_mst.activity_id_fk=activity_mst.activity_id AND activity_mst.status_nm="Active"  ','inner');
						$this->db->join('work_unit_mst', 'work_unit_mst.wu_id = activity_mst.wu_id_fk AND work_unit_mst.status_nm="Active" ','inner');
						$this->db->join('user_mst', 'user_mst.user_id = std_tgt_mst.ins_user','inner');
						$this->db->join('project_mst', 'project_mst.project_id=activity_mst.project_id_fk AND project_mst.status_nm="Active" ','inner');
						$this->db->join(' ( SELECT
		Num1.activity_id_fk,
		ROUND(((SUM(w_done) * 8) / SUM(Num1.quo * Num2.DC0)),2) AS achieved_tgt 
	FROM 
		(
		SELECT
			R.day_val,
			R.user_id_fk,	
			R.activity_id_fk,
			IFNULL(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)),(R.final_xp) + IFNULL(R.comp_xp, 0))),0) AS quo,
			IFNULL(SUM(IFNULL(R.audit_work, R.work_comp)),0) AS w_done
		FROM
			response_mst R
		JOIN activity_mst A ON
			R.activity_id_fk = A.activity_id AND A.status_nm = "Active" 
		JOIN work_unit_mst W1 ON
			A.`wu_id_fk` = W1.wu_id AND W1.status_nm = "Active" 
		WHERE 
			R.dept_id_fk IN ("'.$dept_opt.'") AND R.status_nm = "Active"  
		GROUP BY
			R.day_val,R.user_id_fk,R.activity_id_fk  
				) Num1
			LEFT JOIN(
		SELECT R.day_val,
			R.user_id_fk,
			(8 - 0.8 *(IFNULL(SUM(CASE WHEN w.is_quantify = "N" 
					THEN IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)),(R.final_xp) + IFNULL(R.comp_xp, 0)) ELSE 0 END),0)))/(IFNULL(SUM(CASE WHEN w.is_quantify = "Y" THEN IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)),(R.final_xp) + IFNULL(R.comp_xp, 0)) ELSE 0 END),0)) AS DC0
		FROM
			`response_mst` R
		JOIN activity_mst A ON
			R.activity_id_fk = A.activity_id AND A.status_nm = "Active"
		JOIN work_unit_mst w ON
			A.`wu_id_fk` = w.wu_id AND w.status_nm = "Active"
		WHERE
			 R.dept_id_fk IN ("'.$dept_opt.'") AND R.status_nm = "Active" 
		GROUP BY 
			R.day_val,R.user_id_fk) Num2
		ON
			Num2.day_val = Num1.day_val AND 
			Num2.user_id_fk = Num1.user_id_fk 
		GROUP BY
			Num1.activity_id_fk)ACH', 'std_tgt_mst.activity_id_fk=ACH.activity_id_fk ','left');		
			$this->db->join(' (SELECT * FROM (SELECT
					S.activity_id_fk,
					S.tgt_val AS p_tgt,
					IF(@pre_tgt = S.activity_id_fk, 2, 1) AS s_nm,
					@pre_tgt := S.activity_id_fk
				FROM
					std_tgt_mst S
				JOIN( SELECT @pre_tgt := "" ) F
				ON 1 = 1
				WHERE
					S.status_nm = "Active" AND S.start_dt <= S.end_dt AND S.end_dt IS NOT NULL
				ORDER BY S.activity_id_fk,S.start_dt DESC )AB WHERE AB.s_nm=1)PT ','std_tgt_mst.activity_id_fk=PT.activity_id_fk ','left');
						 $this->db->where('std_tgt_mst.status_nm', 'Active');
						 $this->db->where('std_tgt_mst.end_dt IS NULL', null, false);
						 if($stat==2)
							{
								$this->db->where('activity_mst.is_closed', 'Y');
							}
		
						if($stat==1)
							{
								$this->db->where('IFNULL(activity_mst.is_closed,"N")', 'N');
							}
						$this->db->where('activity_mst.dept_id_fk', $dept_opt);
								 $this->db->where_in('project_mst.project_id', $pro);
						 $this->db->order_by('activity_mst.activity_name', 'ASC');							
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return json_encode($resultSet);
	  }
	  else{
		return json_encode (json_decode ("{}"));;
	  }
	 }	 
	
	public function load_issue_boots($file_nm,$pids,$st_dt,$e_dt,$dept_opt,$issueStatus = NULL)
	{		
	

	$this->db->select(' 1  AS id,A.activity_id,A.activity_name,IFNULL(IC.category_name,"") AS  issue_cat,
						R.response_id ,U.full_name,IFNULL(R.issue_desc,"") AS issue_desc,
						UNIX_TIMESTAMP(CONVERT_TZ(R.day_val,"+00:00",'.$this->tx_cn1.')) AS day_val,
						R.work_comp,ROUND(R.equiv_xp,2) AS equiv_xp,
						CASE WHEN R.tast_stat="Audit Pending" THEN CONCAT(R.tast_stat,"|","t_stat_1")
						WHEN R.tast_stat="Review Pending" THEN CONCAT(R.tast_stat,"|","t_stat_2")
						 WHEN R.tast_stat="Confirmation Pending" THEN CONCAT(R.tast_stat,"|","t_stat_3")
	 					WHEN R.tast_stat="Confirmed" THEN CONCAT(R.tast_stat,"|","t_stat_4") END AS tast_stat1,W.wu_name,
						ROUND(R.comp_xp,2) AS comp_xp,UNIX_TIMESTAMP(R.comp_upd_dt)  AS comp_upd_dt,
						R.audit_desc,R.audit_xp,
						UC.full_name AS comp_user ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('user_mst U', 'R.user_id_fk=U.user_id','inner');
						$this->db->join('activity_mst A', 'A.activity_id=R.activity_id_fk AND A.status_nm="Active" ','inner');
						$this->db->join('work_unit_mst W', 'A.wu_id_fk=W.wu_id AND W.status_nm="Active" ','left');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						$this->db->join('user_mst UC', 'R.comp_user=UC.user_id','left');
						$this->db->join('issue_cat_mst IC', 'R.iss_cat_id_fk=IC.iss_cat_id AND  IC.status_nm="Active"','left');
						$this->db->where('R.dept_id_fk', $dept_opt);
						$this->db->where('R.status_nm', 'Active');
						$this->db->where('R.issue_flag','1');
						if($issueStatus){
							$this->db->where('R.tast_stat != ','Confirmed');
						}
						 
						 if($pids!='0' && ($pids))
							 {
								 $key=explode(",",$pids);
								 $this->db->where_in('P.project_id', $key);							
							 }			
							if($file_nm=='Issues')
							 {
								 $this->db->where('R.issue_chk_flag IS NULL', null, false);
								 $this->db->order_by('R.day_val', 'ASC');	
							 }

							 if($file_nm=='Issues_comp')
							 {
							 	$this->db->where('R.issue_chk_flag',$issueStatus);
								$this->db->order_by('R.comp_upd_dt', 'DESC');
							 }
							/* if($file_nm=='Issues_reject')
							 {
								$this->db->where('R.issue_chk_flag','2');
								$this->db->order_by('R.comp_upd_dt', 'DESC');
							 }*/	 
						 						
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return json_encode($resultSet);
	  }
	  else{
		return json_encode (json_decode ("{}"));;
	  }
	}	 
	
	//Update xp logs
	public function updateXP($data,$responseId){
		$this->db->trans_start();
		$this->db->where_in('response_id', $responseId);
		$this->db->update('response_mst', $data);

		$this->db->trans_complete();
		if($this->db->trans_status() === FALSE){
			return false;
		}else{
			return true;
		}
	}


	//Add history of change log
	public function insertIssueHistory($hData){
		$this->db->trans_start();
		$this->db->insert_batch('issue_history', $hData); 

		$this->db->trans_complete();
		if($this->db->trans_status() === FALSE){
			return false;
		}else{
			return true;
		}

	}
	
	public function l_stat_boots($pro,$st_dt3,$e_dt3,$st_dt7,$e_dt7,$dept_opt)
	{
		$st_dt3_f = date('d-M', strtotime($st_dt3));	
		$e_dt3_f = date('d-M', strtotime($e_dt3));	
		$st_dt7_f = date('d-M', strtotime($st_dt7));	
		$e_dt7_f = date('d-M', strtotime($e_dt7));	
		
	$sql="SELECT
		Num1.activity_id_fk,
		Num1.activity_name,
		Num1.wu_name,
		CONCAT('".$st_dt3_f."',' to ','".$e_dt3_f."') AS date_3,
		CONCAT('".$st_dt7_f."',' to ','".$e_dt7_f."') AS date_7,
		SUM(CASE WHEN Num1.day_val BETWEEN '".$st_dt3."' AND '".$e_dt3."' THEN w_done ELSE 0 END) AS quantity_done3,
		SUM(CASE WHEN Num1.day_val BETWEEN '".$st_dt3."' AND '".$e_dt3."' THEN  Num1.quo ELSE 0 END ) AS final_xp3,
		ROUND(((SUM(CASE WHEN Num1.day_val BETWEEN '".$st_dt3."' AND '".$e_dt3."' THEN w_done ELSE 0 END) * 8) / SUM( (CASE WHEN Num1.day_val BETWEEN '".$st_dt3."' AND '".$e_dt3."' THEN  Num1.quo ELSE 0 END) * (CASE WHEN Num1.day_val BETWEEN '".$st_dt3."' AND '".$e_dt3."' THEN  Num2.DC0 ELSE 0 END) )),2) AS achieved_tgt3,
		SUM(w_done) AS quantity_done7,
		SUM(Num1.quo) AS final_xp7,
		SUM(Num2.DC0) AS DC07,
		ROUND(((SUM(w_done) * 8) / SUM(Num1.quo* Num2.DC0)),2) AS achieved_tgt7,	
		Num1.std_tgt_val AS std_tgt
	FROM 
		(
		SELECT
			R.day_val,
			R.user_id_fk,
			R.std_tgt_val,
			A.activity_name,w.wu_name,
			R.activity_id_fk,
			IFNULL(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)),(R.final_xp) + IFNULL(R.comp_xp, 0))),0) AS quo,
			IFNULL(SUM(IFNULL(R.audit_work, R.work_comp)),0) AS w_done
		FROM
			response_mst R
		JOIN activity_mst A ON
			R.activity_id_fk = A.activity_id AND A.status_nm = 'Active' 
		JOIN work_unit_mst w ON
			A.`wu_id_fk` = w.wu_id AND w.status_nm = 'Active' 
		WHERE 
			R.dept_id_fk='".$dept_opt."' AND R.status_nm = 'Active' AND R.day_val BETWEEN '".$st_dt7."' AND '".$e_dt7."' AND A.project_id_fk IN('".$pro."')
		GROUP BY
			R.day_val,R.user_id_fk,A.activity_name,w.wu_name,R.activity_id_fk ,R.std_tgt_val 
				) Num1
			JOIN(
		SELECT R.day_val,
			R.user_id_fk,
			(8 - 0.8 *(IFNULL(SUM(CASE WHEN w.is_quantify = 'N' 
					THEN IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)),(R.final_xp) + IFNULL(R.comp_xp, 0)) ELSE 0 END),0)))/(IFNULL(SUM(CASE WHEN w.is_quantify = 'Y' THEN IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)),(R.final_xp) + IFNULL(R.comp_xp, 0)) ELSE 0 END),0)) AS DC0
		FROM
			`response_mst` R
		JOIN activity_mst A ON
			R.activity_id_fk = A.activity_id AND A.status_nm = 'Active'
		JOIN work_unit_mst w ON
			A.`wu_id_fk` = w.wu_id AND w.status_nm = 'Active'
		WHERE
			 R.dept_id_fk='".$dept_opt."' AND R.status_nm = 'Active' AND R.day_val BETWEEN '".$st_dt7."' AND '".$e_dt7."' AND A.project_id_fk IN('".$pro."')
		GROUP BY 
			R.day_val,R.user_id_fk) Num2
		ON
			Num2.day_val = Num1.day_val AND Num2.user_id_fk = Num1.user_id_fk 
		GROUP BY
			Num1.activity_id_fk,Num1.activity_name,Num1.wu_name,Num1.std_tgt_val;";
 
					
				$sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return json_encode($resultSet);
	  }
	  else{
		return json_encode (json_decode ("{}"));;
	  }
	}	 
	
	public function load_a_data_pull($file_nm,$st_dt,$e_dt)
	{
		if($file_nm=="Week_Targets")
		{
			$sql="SELECT A.activity_id,
					(CASE WHEN A.dept_id_fk=1 then 'Content' else 'Media' END) AS dept_name, 
					P.project_name, A.activity_name,W.wu_name,
					 (CASE WHEN S.end_dt IS NULL THEN NULL ELSE S.change_desc END)AS prev_change_desc, 
					 S.tgt_val, 
					 U.full_name AS ins_user, 
					 DATE_FORMAT(S.ins_dt, '%d/%m/%y %k:%i:%s')  AS ins_dt,
					 DATE_FORMAT(S.start_dt, '%d/%m/%y') AS start_date, 
					 DATE_FORMAT(S.end_dt, '%d/%m/%y') AS end_date, 
					 U1.full_name AS upd_user, 
					 DATE_FORMAT(S.upd_dt,'%d/%m/%y %k:%i:%s')  AS upd_dt 
					FROM `std_tgt_mst` S
					INNER JOIN `activity_mst` A ON S.`activity_id_fk`=A.`activity_id` AND A.`status_nm`='Active'
					INNER JOIN `work_unit_mst` W ON W.`wu_id` = A.`wu_id_fk` AND W.`status_nm`='Active'
					INNER JOIN `user_mst` U ON U.`user_id` = S.`ins_user`
					INNER JOIN `project_mst` P  ON P.`project_id`=A.`project_id_fk` AND P.`status_nm`='Active'
					LEFT JOIN `user_mst` U1 ON U1.`user_id` = S.`upd_user`
					WHERE S.`status_nm` = 'Active' 
					AND A.activity_id IN (SELECT activity_id_fk
					FROM std_tgt_mst 
					WHERE activity_id_fk IN (
					SELECT activity_id_fk FROM 
					std_tgt_mst 
					WHERE DATE(start_dt) BETWEEN '".$st_dt."' AND '".$e_dt."' 
					 OR DATE(end_dt) BETWEEN '".$st_dt."' AND '".$e_dt."' 
					) AND status_nm='Active'  AND start_dt<=IFNULL(end_dt,start_dt) 
					AND start_dt<='".$e_dt."'
					GROUP BY activity_id_fk 
					having count(*)>1) AND S.start_dt<=IFNULL(S.end_dt,S.start_dt)
					ORDER BY A.`activity_name` ASC,S.start_dt,S.end_dt;";
		}
		if($file_nm=="All_Targets")
		{
			$sql="SELECT (CASE WHEN A.dept_id_fk=1 then 'Content' else 'Media' END) AS dept_name,
				P.project_name, A.activity_name,W.wu_name,
				 (CASE WHEN S.end_dt IS NULL THEN NULL ELSE S.change_desc END)AS prev_change_desc
				 , S.tgt_val, 
				 U.full_name AS ins_user, 
				  DATE_FORMAT(S.ins_dt, '%d/%m/%y %k:%i:%s')  AS ins_dt,
									 DATE_FORMAT(S.start_dt, '%d/%m/%y') AS start_date, 
									 DATE_FORMAT(S.end_dt, '%d/%m/%y') AS end_date, 
									 U1.full_name AS upd_user, 
									 DATE_FORMAT(S.upd_dt,'%d/%m/%y %k:%i:%s')  AS upd_dt 
				FROM `std_tgt_mst` S
				INNER JOIN `activity_mst` A ON S.`activity_id_fk`=A.`activity_id` AND A.`status_nm`='Active'
				INNER JOIN `work_unit_mst` W ON W.`wu_id` = A.`wu_id_fk` AND W.`status_nm`='Active'
				INNER JOIN `user_mst` U ON U.`user_id` = S.`ins_user`
				INNER JOIN `project_mst` P  ON P.`project_id`=A.`project_id_fk` AND P.`status_nm`='Active'
				LEFT JOIN `user_mst` U1 ON U1.`user_id` = S.`upd_user`
				WHERE S.`status_nm` = 'Active' 
				ORDER BY A.`activity_name` ASC,S.start_dt,S.end_dt;";
			}
		
			$sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return json_encode($resultSet);
	  }
	  else{
		return json_encode (json_decode ("{}"));;
	  }
		
	}
	
	public function load_stat_boots($pro,$st_dt,$e_dt,$dept_opt)
	{
	$sql="SELECT
		Num1.activity_id_fk,
		Num1.activity_name,
		Num1.wu_name,
		SUM(w_done) AS quantity_done,
		SUM(Num1.quo) AS final_xp,
		ROUND(((SUM(w_done) * 8) / SUM(Num1.quo * Num2.DC0)),2) AS achieved_tgt,
		Num1.std_tgt_val AS std_tgt
	FROM 
		(
		SELECT
			R.day_val,
			R.user_id_fk,
			R.std_tgt_val,
			A.activity_name,
			W1.wu_name,			
			R.activity_id_fk,
			IFNULL(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)),(R.final_xp) + IFNULL(R.comp_xp, 0))),0) AS quo,
			IFNULL(SUM(IFNULL(R.audit_work, R.work_comp)),0) AS w_done
		FROM
			response_mst R
		JOIN activity_mst A ON
			R.activity_id_fk = A.activity_id AND A.status_nm = 'Active' 
		JOIN work_unit_mst W1 ON
			A.`wu_id_fk` = W1.wu_id AND W1.status_nm = 'Active' 
		WHERE 
			R.dept_id_fk='".$dept_opt."' AND R.status_nm = 'Active' AND R.day_val BETWEEN '".$st_dt."' AND '".$e_dt."' AND A.project_id_fk IN('".$pro."')
		GROUP BY
			R.day_val,R.user_id_fk,A.activity_name,W1.wu_name,R.activity_id_fk ,R.std_tgt_val 
				) Num1
			JOIN(
		SELECT R.day_val,
			R.user_id_fk,
			(8 - 0.8 *(IFNULL(SUM(CASE WHEN w.is_quantify = 'N' 
					THEN IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)),(R.final_xp) + IFNULL(R.comp_xp, 0)) ELSE 0 END),0)))/(IFNULL(SUM(CASE WHEN w.is_quantify = 'Y' THEN IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)),(R.final_xp) + IFNULL(R.comp_xp, 0)) ELSE 0 END),0)) AS DC0
		FROM
			`response_mst` R
		JOIN activity_mst A ON
			R.activity_id_fk = A.activity_id AND A.status_nm = 'Active'
		JOIN work_unit_mst w ON
			A.`wu_id_fk` = w.wu_id AND w.status_nm = 'Active'
		WHERE
			 R.dept_id_fk='".$dept_opt."' AND R.status_nm = 'Active' AND R.day_val BETWEEN '".$st_dt."' AND '".$e_dt."' AND A.project_id_fk IN('".$pro."')
		GROUP BY 
			R.day_val,R.user_id_fk) Num2
		ON
			Num2.day_val = Num1.day_val AND Num2.user_id_fk = Num1.user_id_fk 
		GROUP BY
			Num1.activity_id_fk,Num1.activity_name,Num1.wu_name,Num1.std_tgt_val;";
			
				$sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return json_encode($resultSet);
	  }
	  else{
		return json_encode (json_decode ("{}"));;
	  }
	}	 
	
	public function load_issue_cnt($file_nm,$pids,$st_dt,$e_dt,$dept_opt)
	{
	$this->db->select(' count(*) AS i_cnt ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('user_mst U', 'R.user_id_fk=U.user_id','inner');
						$this->db->join('activity_mst A', 'A.activity_id=R.activity_id_fk AND A.status_nm="Active" ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						$this->db->join('user_mst UC', 'R.comp_user=UC.user_id','left');
						$this->db->where('R.dept_id_fk', $dept_opt);
						 $this->db->where('R.status_nm', 'Active');
						 $this->db->where('R.issue_flag','1');
						 if($pids!='0' && ($pids))
							 {
								 $key=explode(",",$pids);
								 $this->db->where_in('P.project_id', $key);							
							 }			
							
								 $this->db->where('R.issue_chk_flag IS NULL', null, false);
								 $this->db->order_by('R.day_val', 'ASC');	
							 						 						
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  }
	}
	
	
	public function load_la_cnt($file_nm,$u_id,$action,$date_start,$date_end,$lvl,$emp,$role_id,$dept_opt)
	{
		$where='';
		$sql1='';
		
		if($action==1)
		{
		$sql1=' SELECT count(*) AS i_cnt
			FROM
				 emp_week_off E 			   
			JOIN `leave_type_mst` LM 
			ON LM.leave_type_name="Week Off" AND LM.status_nm="Active" 
			JOIN user_mst U 
			ON U.user_id=E.user_id_fk 
			LEFT JOIN user_mst UI 
			ON UI.user_id=E.ins_user 
			LEFT JOIN user_mst UP 
			ON UP.user_id=E.upd_user 
			WHERE  E.status_nm="Active" 
			AND E.conf_status="Pending"';
			if($dept_opt)
				{
					$where=' AND (IFNULL(U.dept_id_fk,0)=0';
					if(is_array($dept_opt))
					{
					foreach($dept_opt AS $dept_it)
					{
					$where=$where.' OR find_in_set('.$dept_it.',U.dept_id_fk)<>0 ';
					}
					}else{
						$where=$where.' OR find_in_set('.$dept_opt.',U.dept_id_fk)<>0 ';
					}
					$where=$where.') ';
				}
			
			// if($emp)
			// {
			// $where =$where.' AND E.user_id_fk IN ('.$emp.') ';
			// }
			 if($role_id!='1' && $role_id!='2')
			{
			 $where =$where.' AND U.manager_id='.$u_id.' ';
			}
		}
		else{
			$sql1=' SELECT count(*) AS i_cnt
			FROM
				 comp_track E 			   
			JOIN `leave_type_mst` LM 
			ON E.leave_type=LM.l_type_id AND LM.status_nm="Active" 
			JOIN user_mst U 
			ON U.user_id=E.user_id_fk
			LEFT JOIN user_mst UI 
			ON UI.user_id=E.ins_user 
			LEFT JOIN user_mst UP 
			ON UP.user_id=E.upd_user 
			WHERE  E.status_nm="Active" 
			AND E.conf_status="Pending"';	
				if($dept_opt)
				{
					$where=' AND (IFNULL(U.dept_id_fk,0)=0';
					if(is_array($dept_opt))
					{
					foreach($dept_opt AS $dept_it)
					{
					$where=$where.' OR find_in_set('.$dept_it.',U.dept_id_fk)<>0 ';
					}
					}else{
						$where=$where.' OR find_in_set('.$dept_opt.',U.dept_id_fk)<>0 ';
					}
					$where=$where.') ';
				}			
			if($emp)
			{
			$where =' AND E.user_id_fk IN ('.$emp.') ';
			}			
			 if($role_id!='1' && $role_id!='2')
			{
			 $where =' AND U.manager_id='.$u_id.' ';
			}
		}
			$sql=$sql1.$where;
							 						 						
				$sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  }
	}
	
	public function load_conf_boots($file_nm,$pids,$st_dt,$e_dt,$user_id,$tab,$dept_opt)
	{
		if($file_nm=='Audit')
		{		
		$this->db->select(' 1 AS id,R.response_id,R.milestone_id,
						R.activity_id_fk,A.activity_name,WU.wu_name,
						IFNULL(IC.category_name,"") AS  issue_cat,
				U.full_name,
				UNIX_TIMESTAMP(CONVERT_TZ(R.day_val,"+00:00",'.$this->tx_cn1.')) AS day_val,
				R.task_desc,ROUND(IFNULL(R.comp_xp,0),2) AS comp_xp,
				CASE WHEN CONCAT(IFNULL(R.issue_flag,0),IFNULL(R.issue_chk_flag,0))=10 THEN "Pending" ELSE "NA" END AS iss_cur_stat,
				R.work_comp,R.std_tgt_val,IFNULL(APR.work_comp,"") AS upd_comp,
				UNIX_TIMESTAMP(R.ins_dt) AS ins_dt,
			APR.change_desc,ROUND(R.equiv_xp,2) AS equiv_xp,ROUND(((APR.work_comp/R.std_tgt_val)*10),2) AS upd_xp,
				 CONCAT(R.tast_stat,"|","t_stat_1") AS tast_stat1',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('user_mst U', 'R.user_id_fk=U.user_id','inner');
						$this->db->join('activity_mst A', 'A.activity_id=R.activity_id_fk ','inner');
						$this->db->join('work_unit_mst WU', 'A.wu_id_fk=WU.wu_id  ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk ','inner');
						$this->db->join('audit_pend_resp APR', 'R.response_id=APR.response_id_fk AND APR.update_status="Pending"','left'); 
						$this->db->join('issue_cat_mst IC', 'R.iss_cat_id_fk=IC.iss_cat_id AND  IC.status_nm="Active"','left');
						$this->db->where('R.dept_id_fk', $dept_opt);
						$this->db->where("R.day_val >= '".$st_dt."' ",null,false);
						$this->db->where('R.tast_stat','Audit Pending');
				//$this->db->order_by('R.day_val', 'DESC');	
			}
									
			 if($file_nm=='Allocation')
				 {
					 $this->db->select(' 1 AS id,R.milestone_id,IFNULL(URE.full_name,"") AS rev_assignee,R.response_id,R.activity_id_fk,A.activity_name,WU.wu_name,
				U.full_name,
				UNIX_TIMESTAMP(CONVERT_TZ(R.day_val,"+00:00",'.$this->tx_cn1.')) AS day_val,
				R.task_desc,ROUND(IFNULL(R.comp_xp,0),2) AS comp_xp,
				CASE WHEN CONCAT(IFNULL(R.issue_flag,0),IFNULL(R.issue_chk_flag,0))=10 THEN "Pending" ELSE "NA" END AS iss_cur_stat,
				R.work_comp,R.std_tgt_val,ROUND(R.equiv_xp,2) AS equiv_xp,
				IFNULL(IC.category_name,"") AS  issue_cat,
				UNIX_TIMESTAMP(R.ins_dt) AS ins_dt,
                ROUND(R.audit_xp,2) AS audit_xp,
                R.audit_work AS audit_comp, 
				UA.full_name AS audit_user,
				R.audit_desc,
				DATE_FORMAT(CONVERT_TZ(R.audit_upd_dt,"+00:00",'.$this->tx_cn.'), "%d-%b-%Y %k:%i:%s") AS audit_upd_dt, 
				CONCAT(R.tast_stat,"|","t_stat_2") AS tast_stat1',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('user_mst U', 'R.user_id_fk=U.user_id','inner');
						$this->db->join('activity_mst A', 'A.activity_id=R.activity_id_fk  ','inner');
						$this->db->join('work_unit_mst WU', 'A.wu_id_fk=WU.wu_id  ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk  ','inner');
						$this->db->join('user_mst UA', 'R.audit_user=UA.user_id','left');
						$this->db->join('user_mst URE', 'R.rev_assignee=URE.user_id','left');
						$this->db->join('issue_cat_mst IC', 'R.iss_cat_id_fk=IC.iss_cat_id AND  IC.status_nm="Active"','left');
						$this->db->where("R.day_val >= '".$st_dt."' ",null,false);
						$this->db->where('R.dept_id_fk', $dept_opt);
					 $this->db->where('R.tast_stat','Review Pending');
					 if($tab==2)
					 {
						$this->db->where('R.rev_assignee IS NOT NULL',null,false);	 
					 }else{
					 $this->db->where('R.rev_assignee IS NULL',null,false);
					 }								 
					 //$this->db->order_by('R.day_val', 'DESC');
				 }
				 if($file_nm=='Review')
				 {
					 $this->db->select(' 1 AS id,R.milestone_id,R.response_id,R.activity_id_fk,A.activity_name,WU.wu_name,
				U.full_name,UNIX_TIMESTAMP(CONVERT_TZ(R.day_val,"+00:00",'.$this->tx_cn1.')) AS day_val,R.task_desc,ROUND(IFNULL(R.comp_xp,0),2) AS comp_xp,
				CASE WHEN CONCAT(IFNULL(R.issue_flag,0),IFNULL(R.issue_chk_flag,0))=10 THEN "Pending" ELSE "NA" END AS iss_cur_stat,
				R.work_comp,R.std_tgt_val,ROUND(R.equiv_xp,2) AS equiv_xp,
				IFNULL(IC.category_name,"") AS  issue_cat,
				UNIX_TIMESTAMP(R.ins_dt) AS ins_dt,
                ROUND(R.audit_xp,2) AS audit_xp,
                R.audit_work AS audit_comp, 
				UA.full_name AS audit_user,
				R.audit_desc,R.assignee_desc,
				DATE_FORMAT(CONVERT_TZ(R.audit_upd_dt,"+00:00",'.$this->tx_cn.'), "%d-%b-%Y %k:%i:%s") AS audit_upd_dt, 
				CONCAT(R.tast_stat,"|","t_stat_2") AS tast_stat1',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('user_mst U', 'R.user_id_fk=U.user_id','inner');
						$this->db->join('activity_mst A', 'A.activity_id=R.activity_id_fk  ','inner');
						$this->db->join('work_unit_mst WU', 'A.wu_id_fk=WU.wu_id ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk ','inner');
						$this->db->join('user_mst UA', 'R.audit_user=UA.user_id','left');
						$this->db->join('issue_cat_mst IC', 'R.iss_cat_id_fk=IC.iss_cat_id AND  IC.status_nm="Active"','left');
						$this->db->where('R.dept_id_fk', $dept_opt);
						$this->db->where("R.day_val >= '".$st_dt."' ",null,false);
						$this->db->where('R.tast_stat','Review Pending');
					 $this->db->where('R.rev_assignee',$user_id);
					 //$this->db->order_by('R.day_val', 'DESC');
				 }
				 if($file_nm=='Confirm')
				 {
					 $this->db->select(' 1 AS id,R.milestone_id,R.response_id,R.activity_id_fk,A.activity_name,WU.wu_name,
				U.full_name,UNIX_TIMESTAMP(CONVERT_TZ(R.day_val,"+00:00",'.$this->tx_cn1.')) AS day_val,R.task_desc,ROUND(IFNULL(R.comp_xp,0),2) AS comp_xp,
				IFNULL(IC.category_name,"") AS  issue_cat,
				CASE WHEN CONCAT(IFNULL(R.issue_flag,0),IFNULL(R.issue_chk_flag,0))=10 THEN "Pending" ELSE "NA" END AS iss_cur_stat,
				R.work_comp,R.std_tgt_val,ROUND(R.equiv_xp,2) AS equiv_xp,
				ROUND(R.audit_xp,2) AS audit_xp,
                R.audit_work AS audit_comp, 
				UA.full_name AS audit_user,
				R.audit_desc,
				DATE_FORMAT(CONVERT_TZ(R.audit_upd_dt,"+00:00",'.$this->tx_cn.'), "%d-%b-%Y %k:%i:%s") AS audit_upd_dt, 
				IFNULL(FB.r_feedb,RPVR.r_val_name) AS review_xp,	
				R.review_desc,
				UR.full_name AS review_user,
				DATE_FORMAT(CONVERT_TZ(R.review_upd_dt,"+00:00",'.$this->tx_cn.'), "%d-%b-%Y %k:%i:%s") AS review_upd_dt,
				UNIX_TIMESTAMP(R.ins_dt) AS ins_dt,
				 ROUND(R.total_xp,2) AS total_xp,
				 CONCAT(R.tast_stat,"|","t_stat_3") AS tast_stat1',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('user_mst U', 'R.user_id_fk=U.user_id','inner');
						$this->db->join('activity_mst A', 'A.activity_id=R.activity_id_fk ','inner');
						$this->db->join('work_unit_mst WU', 'A.wu_id_fk=WU.wu_id  ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk   ','inner');
						$this->db->join(' (SELECT RFL.response_id_fk,
											GROUP_CONCAT(RP.r_param_name," - ",IFNULL(RPV.r_val_name,"N/A")) AS r_feedb
												FROM `review_log` RFL 
												JOIN review_param_mst RP 
												ON RFL.r_param_id_fk=RP.r_param_id 
												LEFT JOIN review_val_mst RPV
												ON RPV.r_val_id=RFL.feedback_val  
												WHERE RFL.status_nm="Active" 
												GROUP BY RFL.response_id_fk) FB', 'R.response_id=FB.response_id_fk ','left');
						$this->db->join('review_val_mst RPVR', 'RPVR.r_val_id=R.review_xp','left');
						$this->db->join('user_mst UA', 'R.audit_user=UA.user_id','left');
						$this->db->join('user_mst UR', 'R.review_user=UR.user_id','left');
						$this->db->join('issue_cat_mst IC', 'R.iss_cat_id_fk=IC.iss_cat_id AND  IC.status_nm="Active"','left');
						$this->db->where('R.dept_id_fk', $dept_opt);
						$this->db->where("R.day_val >= '".$st_dt."' ",null,false);
					$this->db->where('R.tast_stat','Confirmation Pending');
				//	$this->db->order_by('R.day_val', 'DESC');
				 }	
			 if($file_nm=='Confirmation_Process')
				 {
					 //$this->db->save_queries = false;
					 $this->db->select(' 1 AS id,IFNULL(R.milestone_id,"None") AS milestone_id,R.response_id,R.activity_id_fk,
					 A.activity_name,WU.wu_name,
				U.full_name,
				UNIX_TIMESTAMP(CONVERT_TZ(R.day_val,"+00:00",'.$this->tx_cn1.')) AS day_val,
				UNIX_TIMESTAMP(R.ins_dt) AS ins_dt 		,
				R.work_comp,
				ROUND(R.equiv_xp,2) AS equiv_xp,
				R.task_desc,
				(CASE WHEN CONCAT(IFNULL(R.issue_flag,0),IFNULL(R.issue_chk_flag,0))=10 THEN "Pending" 
					WHEN CONCAT(IFNULL(R.issue_flag,0),IFNULL(R.issue_chk_flag,0))=12 THEN "Rejected" 
					WHEN CONCAT(IFNULL(R.issue_flag,0),IFNULL(R.issue_chk_flag,0))=11 THEN "Approved" 
				ELSE "None" END) AS iss_stat,
				IFNULL(IC.category_name,"") AS  issue_cat,
				ROUND(IFNULL(R.comp_xp,0),2) AS comp_xp,
				R.issue_desc,				
				(CASE WHEN R.tast_stat="Audit Pending" THEN CONCAT(R.tast_stat,"|","t_stat_1")
					WHEN R.tast_stat="Review Pending" THEN CONCAT(R.tast_stat,"|","t_stat_2")
					WHEN R.tast_stat="Confirmation Pending" THEN CONCAT(R.tast_stat,"|","t_stat_3")
					WHEN R.tast_stat="Confirmed" THEN CONCAT(R.tast_stat,"|","t_stat_4") END) AS tast_stat1,
				(CASE WHEN R.tast_stat="Audit Pending" THEN "N" ELSE "Y" END) AS audit_yn,
				(CASE WHEN (R.tast_stat="Audit Pending" OR (R.tast_stat="Review Pending" AND R.rev_assignee IS NULL)) THEN "N" ELSE "Y" END) AS alloc_yn,
				(CASE WHEN (R.tast_stat="Confirmation Pending" OR R.tast_stat="Confirmed") THEN "Y" ELSE "N" END) AS review_yn,
				(CASE WHEN R.tast_stat="Confirmed" THEN "Y" ELSE "N" END) AS conf_yn,
				R.audit_work AS audit_comp, 
                ROUND(R.audit_xp,2) AS audit_xp,
				UA.full_name AS audit_user,				
				UNIX_TIMESTAMP(R.audit_upd_dt) AS audit_upd_dt, 
				R.audit_desc,
				R.assignee_desc,
				IFNULL(URE.full_name,"") AS rev_assignee,
				IFNULL(FB.r_feedb,RPVR.r_val_name) AS review_xp,				
				UR.full_name AS review_user,				
				UNIX_TIMESTAMP(R.review_upd_dt) AS review_upd_dt,
				R.review_desc,
				 ROUND(R.total_xp,2) AS total_xp,
				UC.full_name AS conf_user,
				UNIX_TIMESTAMP(R.conf_upd_dt) AS conf_upd_dt		',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('user_mst U', 'R.user_id_fk=U.user_id','inner');
						$this->db->join('activity_mst A', 'A.activity_id=R.activity_id_fk  ','inner');
						$this->db->join('work_unit_mst WU', 'A.wu_id_fk=WU.wu_id  ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk   ','inner');
						$this->db->join('user_mst UA', 'R.audit_user=UA.user_id','left');
						$this->db->join('user_mst URE', 'R.rev_assignee=URE.user_id','left');
						$this->db->join(' (SELECT RFL.response_id_fk,
											GROUP_CONCAT(RP.r_param_name," - ",IFNULL(RPV.r_val_name,"N/A")) AS r_feedb
												FROM `review_log` RFL 
												JOIN review_param_mst RP 
												ON RFL.r_param_id_fk=RP.r_param_id 
												LEFT JOIN review_val_mst RPV
												ON RPV.r_val_id=RFL.feedback_val  
												WHERE RFL.status_nm="Active" 
												GROUP BY RFL.response_id_fk) FB', 'R.response_id=FB.response_id_fk ','left');
						$this->db->join('user_mst UR', 'R.review_user=UR.user_id','left');
						$this->db->join('review_val_mst RPVR', 'RPVR.r_val_id=R.review_xp','left');
						$this->db->join('user_mst UC', 'R.conf_user=UC.user_id','left');
						$this->db->join('issue_cat_mst IC', 'R.iss_cat_id_fk=IC.iss_cat_id AND  IC.status_nm="Active"','left');
						$this->db->where('R.dept_id_fk', $dept_opt);
					$this->db->where("R.day_val BETWEEN '".$st_dt."' AND '".$e_dt."'",null,false);
					//$this->db->order_by('R.day_val', 'DESC');
				 }							 
					$this->db->where('R.status_nm', 'Active');

							 if($pids!='0' && ($pids))
							 {
								 $key=explode(",",$pids);
								 $this->db->where_in('P.project_id', $key);							
							 }
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return json_encode($resultSet);
	  }
	  else{
		return json_encode (json_decode ("{}"));;
	  }
	}	 
	
	public function load_rev_data($file_nm,$pids,$dept_opt)
	{			
	$this->db->select(' U.user_id,U.full_name AS reviewer ',FALSE);
						$this->db->from('user_mst U');
						$this->db->where('U.status_nm', 'Active');
						 $this->db->where('U.access_name like "%Review%"', null,false);
						 if($pids!='0' && ($pids))
							 {
								 $where='( IFNULL(U.project_id_fk,0)=0 OR ';
								 $jddd=0;
								 $key=explode(",",$pids);
								 foreach($key AS $pd)
								 {
									 if($jddd!=0)
									 {
										 $where=$where.' OR ';
									 }
								 $where=$where.' find_in_set('.$pd.',U.project_id_fk)<>0 ';							
								 $jddd++;
								 }
								 $where=$where.')';
								 $this->db->where($where);
							 }
								$where1=' ( IFNULL(U.dept_id_fk,0)=0 OR find_in_set('.$dept_opt.',U.dept_id_fk)<>0 )';
								 $this->db->where($where1);
								$this->db->order_by('U.full_name', 'ASC');					 						
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return ($resultSet);
	  }
	  else{
		return false;
	  }
	}
	
	public function load_conf_cnt($file_nm,$pids,$st_dt,$e_dt, $u_id,$dept_opt)
	{			
	$this->db->select(' IFNULL(SUM(CASE WHEN R.day_val  >= "'.$e_dt.'" AND R.tast_stat="Audit Pending" THEN 1 ELSE 0 END),0) AS Audit_cnt,
				IFNULL(SUM(CASE WHEN R.tast_stat="Review Pending" AND R.rev_assignee IS NULL THEN 1 ELSE 0 END),0) AS Allocation_cnt,
				IFNULL(SUM(CASE WHEN R.tast_stat="Review Pending" AND R.rev_assignee='.$u_id.' THEN 1 ELSE 0 END),0) AS Review_cnt,
				IFNULL(SUM(CASE WHEN R.tast_stat="Confirmation Pending" THEN 1 ELSE 0 END),0) AS Confirm_cnt ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'A.activity_id=R.activity_id_fk ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk  ','inner');
						$this->db->where('R.dept_id_fk', $dept_opt);						
						$this->db->where("R.day_val >= '".$st_dt."' ",null,false);
						 $this->db->where('R.status_nm', 'Active');
						 if($pids!='0' && ($pids))
							 {
								 $key=explode(",",$pids);
								 $this->db->where_in('P.project_id', $key);							
							 }			
						
								 $this->db->where_in('R.tast_stat',array('Audit Pending','Review Pending','Confirmation Pending'));						
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return ($resultSet);
	  }
	  else{
		return false;
	  }
	}	
	 public function select_proj_xp($date_start,$date_end,$u_id){	
	    $this->db->select(" U.user_id,U.full_name,Main.selected_date AS day_val,DATE_FORMAT(Main.selected_date,'%d-%b-%Y') AS dt_val,ROUND(IFNULL(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),0),2) AS proj_xp,
		IFNULL(I.esp_wrk_hrs,0) AS esp_wrk_h,		
		IFNULL(CONCAT(LPAD(FLOOR(I.in_time), 2, 0), ':', LPAD(ROUND((I.in_time - FLOOR(I.in_time)) * 60) %60, 2, 0)),'0:00') AS in_time_f, 
		IFNULL(CONCAT(LPAD(FLOOR(I.sapience_on),2,0),':',LPAD(ROUND((I.sapience_on - FLOOR(I.sapience_on)) * 60) %60, 2, 0)),'0:00') AS sapience_on_f, 
		IFNULL(CONCAT(LPAD(FLOOR(I.esp_wrk_hrs),2,0),':', LPAD(ROUND((I.esp_wrk_hrs - FLOOR(I.esp_wrk_hrs)) * 60) %60, 2, 0)),'0:00') AS esp_wrk_hrs_f,
		ROUND(IFNULL(SUM(CASE WHEN R.tast_stat='Confirmed'  THEN (IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))) ELSE 0 END ),0),2) AS act_xp ",FALSE);
		$this->db->from('user_mst U');
		$this->db->join(' (SELECT V.selected_date
								FROM
								(
								SELECT
									ADDDATE("'.$date_start.'",t2.i * 100 + t1.i * 10 + t0.i) AS selected_date
								FROM
									(SELECT  day_num AS i  FROM calendar cal WHERE cal.day_num <= 9) t0,
									(SELECT  day_num AS i  FROM calendar cal WHERE cal.day_num <= 9) t1,
									(SELECT  day_num AS i  FROM calendar cal WHERE cal.day_num <= 9) t2
								) V
						WHERE V.selected_date BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
				AND V.selected_date NOT IN (SELECT hol.day_val from nation_hols hol WHERE hol.status_nm="Active") ) Main','1=1','inner');
		$this->db->join('response_mst R', 'U.user_id = R.user_id_fk AND Main.selected_date=R.day_val AND R.status_nm="Active"','left');
		$this->db->join('emp_week_off WO', ' WO.user_id_fk = U.user_id AND WO.week_day = WEEKDAY(Main.selected_date)	
	   AND Main.selected_date BETWEEN WO.start_dt AND IFNULL(WO.end_dt, Main.selected_date) AND WO.conf_status="Approved"','left');
		$this->db->join("in_out_upload I ", 'U.user_id = I.user_id AND Main.selected_date=I.date_val AND I.status_nm="Active"','left');
					$this->db->where('U.user_id IN ('.$u_id.') ',null,false);
					$this->db->where('( WO.user_id_fk IS NULL OR ( WO.user_id_fk IS NOT NULL AND IFNULL(I.attendance_cnt,0)<>0)
					  OR(WO.user_id_fk IS NOT NULL AND R.response_id IS NOT NULL))',null,false);					
					$this->db->group_by('U.full_name,U.user_id,Main.selected_date');
					$this->db->order_by('U.full_name,U.user_id,Main.selected_date','ASC');
					$res = $this->db->get();                
            $resultSet = $res->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  }
	}	 
	 
	 public function select_in_out_data($date_start,$date_end,$u_id){
	    $this->db->select(" U.user_id,U.full_name,I.date_val,
		(CONCAT(LPAD(FLOOR(I.in_time),2,0),':', LPAD(ROUND((I.in_time - FLOOR(I.in_time)) * 60) %60,2,0))) AS in_time_f,
		(CONCAT(LPAD(FLOOR(I.sapience_on),2,0),':', LPAD(ROUND((I.sapience_on - FLOOR(I.sapience_on)) * 60) %60,2,0))) AS sapience_on_f,
		(CONCAT(LPAD(FLOOR(I.esp_wrk_hrs),2,0),':', LPAD(ROUND((I.esp_wrk_hrs - FLOOR(I.esp_wrk_hrs)) * 60) %60,2,0))) AS esp_wrk_hrs_f ",FALSE);
	
		$this->db->join('user_mst U', 'U.user_id = I.user_id','inner');
					$this->db->where('I.user_id IN ('.$u_id.')',null,false );
					$this->db->where('I.date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" ', null, false);
                 	$this->db->where('I.status_nm', 'Active');
					//$this->db->where('I.in_time <> "0.00"',null,false);
					$this->db->order_by('U.full_name,I.date_val','ASC');
					$res = $this->db->get('in_out_upload I');                
            $resultSet = $res->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  }
	}
	
	 public function emp_select2($file_nm,$date_start,$pro_sel,$date_end,$u_id,$var){
		 if($file_nm=="R_Team_Graphs")
		 {
			 if($var==0)
			 {
				$selec_val='CONCAT(A.activity_name," (",W.wu_name,")")';
			 }
			 if($var==1)
			 {
				$selec_val='P.project_name';
			 }
			 if($var==2)
			 {
				$selec_val='T.task_name';
			 }
			 if($var==3)
			 {
				$selec_val='ST.sub_name';
			 }
			 if($var==4)
			 {
				$selec_val='J.job_name';
			 }
			 if($var==5)
			 {
				$selec_val='CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))';
			 }
			$this->db->select('  U.full_name,'.$selec_val.' AS activity_name,
			ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp  ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('user_mst U', 'U.user_id = R.user_id_fk','inner');
						$this->db->join('work_unit_mst W', 'W.wu_id = A.wu_id_fk AND W.status_nm="Active" ','left');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						if($var==2)
						{
						$this->db->join('task_mst T', 'T.task_id = A.task_id_fk AND T.status_nm="Active" ','inner');
						}
						if($var==3 || $var==5)
						{
						$this->db->join('sub_mst ST', 'ST.sub_id = A.sub_id_fk AND ST.status_nm="Active" ','inner');
						}
						if($var==4  || $var==5)
						{
						$this->db->join('job_mst J', 'J.job_id = A.job_id_fk AND J.status_nm="Active" ','inner');
						}
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');	
							$this->db->where_in('R.user_id_fk', $pro_sel);
						 $this->db->group_by("U.full_name,".$selec_val."",false);
						$this->db->order_by('activity_name', 'ASC');
						
					$this->db->order_by("equiv_xp", "DESC");
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
		 }
		 if($file_nm=="R_Breakdown")
		 {
			 if($var=="job")
				{
			$this->db->select('SUBSTRING_INDEX(T.job_name, "_", 1)  AS tab_name,
				ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp   ',FALSE);
				$this->db->group_by('SUBSTRING_INDEX(T.job_name, "_", 1)');
				}else if($var=="trade")
				{
				$this->db->select('CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))  AS tab_name,
				ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp   ',FALSE);
				$this->db->group_by('CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))',false);
				}else
				{
					$this->db->select(' T.'.$var.'_name AS tab_name,
				ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp   ',FALSE);
				$this->db->group_by("T.".$var."_name");
				}
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
					if($var=="trade")
						{
							$this->db->join('sub_mst ST', 'ST.sub_id=A.sub_id_fk AND ST.status_nm="Active" ','inner');
							$this->db->join('job_mst J', 'J.job_id=A.job_id_fk AND J.status_nm="Active" ','inner');
						}
						else
						{
						$this->db->join($var.'_mst T', 'T.'.$var.'_id=A.'.$var.'_id_fk AND T.status_nm="Active" ','inner');
						}
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');	
							$this->db->where_in('R.user_id_fk', $pro_sel);					 
						 $this->db->order_by('equiv_xp', 'DESC');		
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
		 }	
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
		 
	 }
	 public function team_member_select2($file_nm,$date_start,$pro_sel,$date_end,$u_id,$var,$dept_opt){
		 if($file_nm=="FV_Breakdown" || $file_nm=="FV_Drilldown")
		 {	
				if($var=="all")
				{
					$this->db->select(' P.project_name, R.milestone_id,U.user_id,U.full_name,CONCAT(S.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))  AS task_name,S.sub_name,SUBSTRING_INDEX(J.job_name, "_", 1)  AS job_name ,
				ROUND(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp,0))+IFNULL(R.comp_xp,0)),2) AS equiv_xp  ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
					$this->db->join('user_mst U', 'U.user_id=R.user_id_fk AND U.status_nm="Active" ','inner');

					$this->db->join('task_mst T', 'T.task_id=A.task_id_fk AND T.status_nm="Active" ','inner');
							$this->db->join('sub_mst S', 'S.sub_id=A.sub_id_fk AND S.status_nm="Active" ','inner');
							$this->db->join('job_mst J', 'J.job_id=A.job_id_fk AND J.status_nm="Active" ','inner');
						$this->db->where('R.dept_id_fk', $dept_opt);
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');	
						 $this->db->group_by('P.project_name, CONCAT(S.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1)),S.sub_name,SUBSTRING_INDEX(J.job_name, "_", 1) ',false);
						 // $this->db->order_by('tab_name', 'ASC');	
						 // $this->db->order_by('equiv_xp', 'DESC');		
				
				}else{
			 			if($var=="job")
				{
			$this->db->select('SUBSTRING_INDEX(T.job_name, "_", 1)  AS tab_name,
				ROUND(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp,0))+IFNULL(R.comp_xp,0)),2) AS equiv_xp   ',FALSE);
				$this->db->group_by('SUBSTRING_INDEX(T.job_name, "_", 1)');
				}else if($var=="trade")
				{
				$this->db->select('CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))  AS tab_name,
				ROUND(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp,0))+IFNULL(R.comp_xp,0)),2) AS equiv_xp  ',FALSE);
				$this->db->group_by('CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))',false);
				}else if($var=="mid")
				{
					$this->db->select(' R.milestone_id AS tab_name,
				ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp   ',FALSE);
				$this->db->group_by("R.milestone_id");
				}else if($var=="user"){
					$this->db->select('T.full_name AS tab_name,
					ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp',FALSE);
					$this->db->group_by("T.full_name");
				}else
				{
					$this->db->select(' T.'.$var.'_name AS tab_name,
				ROUND(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp,0))+IFNULL(R.comp_xp,0)),2) AS equiv_xp  ',FALSE);
				$this->db->group_by("T.".$var."_name");
				}
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						//$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
					if($var=="trade")
						{
							$this->db->join('sub_mst ST', 'ST.sub_id=A.sub_id_fk AND ST.status_nm="Active" ','inner');
							$this->db->join('job_mst J', 'J.job_id=A.job_id_fk AND J.status_nm="Active" ','inner');
						}else if($var=="mid")
						{
						}
						else
						{
						if($var=="user"){
								$this->db->join($var.'_mst T', 'T.'.$var.'_id=R.'.$var.'_id_fk AND T.status_nm="Active" ','inner');
							}else{
								$this->db->join($var.'_mst T', 'T.'.$var.'_id=A.'.$var.'_id_fk AND T.status_nm="Active" ','inner');
							}
						}
						
						
						$this->db->where('R.dept_id_fk', $dept_opt);

						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');	
						 $this->db->order_by('tab_name', 'ASC');	
						 $this->db->order_by('equiv_xp', 'DESC');		
			 }
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
		 }
		 if($file_nm=="Graph_Drilldown")
		 {
			 
			 $this->db->select(' R.milestone_id,R.milestone_id,U.user_id,U.full_name,T.task_name AS cat_name, CONCAT(S.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))  AS task_name,S.sub_name,SUBSTRING_INDEX(J.job_name, "_", 1)  AS job_name ,
				ROUND(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp,0))+IFNULL(R.comp_xp,0)),2) AS equiv_xp  ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						$this->db->join('user_mst U', 'U.user_id=R.user_id_fk AND U.status_nm="Active" ','inner');
						$this->db->join('task_mst T', 'T.task_id=A.task_id_fk AND T.status_nm="Active" ','inner');
							$this->db->join('sub_mst S', 'S.sub_id=A.sub_id_fk AND S.status_nm="Active" ','inner');
							$this->db->join('job_mst J', 'J.job_id=A.job_id_fk AND J.status_nm="Active" ','inner');
						$this->db->where('R.dept_id_fk', $dept_opt);
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');	
						 if ($this->input->get('confirmed') == 'true') {
							$this->db->where('R.tast_stat', 'Confirmed');	
						 } 
						 $this->db->where_in('P.project_id', $pro_sel);					 
						 $this->db->group_by('R.milestone_id,T.task_name, CONCAT(S.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1)),S.sub_name,SUBSTRING_INDEX(J.job_name, "_", 1) ',false);
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
		 }
		 if($file_nm=="Graph_Breakdown")
		 {
				if($var=="job")
				{
			$this->db->select('SUBSTRING_INDEX(T.job_name, "_", 1)  AS tab_name,
				ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp   ',FALSE);
				$this->db->group_by('SUBSTRING_INDEX(T.job_name, "_", 1)');
				}else if($var=="trade")
				{
				$this->db->select('CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))  AS tab_name,
				ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp   ',FALSE);
				$this->db->group_by('CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))',false);
				}else if($var=="mid")
				{
					$this->db->select(' R.milestone_id AS tab_name,
				ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp   ',FALSE);
				$this->db->group_by("R.milestone_id");
				}else if($var=="user"){
					$this->db->select('T.full_name AS tab_name,
					IO.atten_cnt,
					ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp',FALSE);
					$this->db->group_by("T.full_name");
				}else
				{
					$this->db->select(' T.'.$var.'_name AS tab_name,
				ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp   ',FALSE);
				$this->db->group_by("T.".$var."_name");
				}
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
					if($var=="trade")
						{
							$this->db->join('sub_mst ST', 'ST.sub_id=A.sub_id_fk AND ST.status_nm="Active" ','inner');
							$this->db->join('job_mst J', 'J.job_id=A.job_id_fk AND J.status_nm="Active" ','inner');
						}else if($var=="mid")
						{
						}else if($var=="user"){
								$this->db->join($var.'_mst T', 'T.'.$var.'_id=R.'.$var.'_id_fk AND T.status_nm="Active" ','inner');
								$this->db->join('in_out_upload IO ', 'T.'.$var.'_id=R.'.$var.'_id_fk AND T.status_nm="Active" ','left');
						}else
						{
						$this->db->join($var.'_mst T', 'T.'.$var.'_id=A.'.$var.'_id_fk AND T.status_nm="Active" ','inner');
						}
						
						$this->db->where('R.dept_id_fk', $dept_opt);
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');	
						 if ($this->input->get('confirmed') == 'true') {
							$this->db->where('R.tast_stat', 'Confirmed');	
						 } 
							$this->db->where_in('P.project_id', $pro_sel);					 
						 $this->db->order_by('equiv_xp', 'DESC');		
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
		 }	
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
		 
	 }

	 
	 public function pt_team_member_select2($file_nm,$date_start,$pt_emp,$date_end,$u_id,$var,$dept_opt){
		 if($file_nm=="PT_Pies")
		 {			 
			 $this->db->select(' P.project_name,IFNULL(R.milestone_id,"None") AS milestone_id,T.task_name AS cat_name, CONCAT(S.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))  AS task_name,S.sub_name,SUBSTRING_INDEX(J.job_name, "_", 1)  AS job_name ,
				ROUND(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp,0))+IFNULL(R.comp_xp,0)),2) AS equiv_xp  ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						$this->db->join('task_mst T', 'T.task_id=A.task_id_fk AND T.status_nm="Active" ','inner');
							$this->db->join('sub_mst S', 'S.sub_id=A.sub_id_fk AND S.status_nm="Active" ','inner');
							$this->db->join('job_mst J', 'J.job_id=A.job_id_fk AND J.status_nm="Active" ','inner');
						//$this->db->where_in('R.dept_id_fk', $dept_opt);
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');	
						 if ($this->input->get('confirmed') == 'true') {
							$this->db->where('R.tast_stat', 'Confirmed');	
						 } 
						 $this->db->where('R.user_id_fk IN ('.$pt_emp.')',null,false);					 
						 $this->db->group_by('P.project_name,R.milestone_id,T.task_name, CONCAT(S.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1)),S.sub_name,SUBSTRING_INDEX(J.job_name, "_", 1) ',false);
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
		 }
		 
		 if($file_nm=="PT_Pie_Grading")
		 {			 
			 $this->db->select(' P.project_name,IFNULL(R.milestone_id,"None") AS milestone_id,T.task_name AS cat_name, CONCAT(S.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))  AS task_name,S.sub_name,SUBSTRING_INDEX(J.job_name, "_", 1)  AS job_name ,
			 IFNULL(JB.value,"None") AS gra_name,
				ROUND(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp,0))+IFNULL(R.comp_xp,0)),2) AS equiv_xp  ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						$this->db->join('task_mst T', 'T.task_id=A.task_id_fk AND T.status_nm="Active" ','inner');
							$this->db->join('sub_mst S', 'S.sub_id=A.sub_id_fk AND S.status_nm="Active" ','inner');
							$this->db->join('job_mst J', 'J.job_id=A.job_id_fk AND J.status_nm="Active" ','inner');
						$this->db->join('(SELECT UF.user_id_fk,UF.project_id_fk,
										CEIL(AVG(FG.grade)) AS gradng 
										FROM 
										user_feedbacks UF 
										JOIN feedback_grades FG 
										ON UF.grade_id_fk=FG.id 
										WHERE UF.status_nm = "Active" AND 
										UF.fb_given_date BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
										)FGRAD ', 'FGRAD.user_id_fk=R.user_id_fk AND FGRAD.project_id_fk= P.project_id ','left');	
						$this->db->join('feedback_grades JB', 'FGRAD.gradng=JB.grade  AND JB.status_nm="Active" ','left');
						//$this->db->where_in('R.dept_id_fk', $dept_opt);
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');	
						 if ($this->input->get('confirmed') == 'true') {
							$this->db->where('R.tast_stat', 'Confirmed');	
						 } 
						 $this->db->where('R.user_id_fk IN ('.$pt_emp.')',null,false);					 
						 $this->db->group_by('P.project_name,JB.value,R.milestone_id,T.task_name, CONCAT(S.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1)),S.sub_name,SUBSTRING_INDEX(J.job_name, "_", 1) ',false);
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
		 }
		 
		 if($file_nm=="PT_Pie_Reviews")
		 {			 
			 $this->db->select(' P.project_name,IFNULL(R.milestone_id,"None") AS milestone_id,T.task_name AS cat_name, CONCAT(S.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))  AS task_name,S.sub_name,SUBSTRING_INDEX(J.job_name, "_", 1)  AS job_name ,
			 IFNULL(FR.r_param_name,"Quality Score") AS r_param,
			 IFNULL(CEIL(AVG(IFNULL(FR.r_feedb,R.review_xp))),"None") AS feedb,
(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp,0))+IFNULL(R.comp_xp,0))/(IFNULL(FRC.f_cnt,1)))  AS equiv_xp 
	  ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						$this->db->join('task_mst T', 'T.task_id=A.task_id_fk AND T.status_nm="Active" ','inner');
							$this->db->join('sub_mst S', 'S.sub_id=A.sub_id_fk AND S.status_nm="Active" ','inner');
							$this->db->join('job_mst J', 'J.job_id=A.job_id_fk AND J.status_nm="Active" ','inner');
							$this->db->join('review_val_mst RPVR', 'RPVR.r_val_id=R.review_xp','left');
							$this->db->join('(SELECT RFL.response_id_fk,RP.r_param_name,
											IFNULL(RFL.feedback_val,0) AS r_feedb
												FROM `review_log` RFL 
												JOIN review_param_mst RP 
												ON RFL.r_param_id_fk=RP.r_param_id 
												WHERE RFL.status_nm="Active")FR','R.response_id=FR.response_id_fk','left');
							$this->db->join('(SELECT RFL.response_id_fk,COUNT(*) AS f_cnt
												FROM `review_log` RFL 												
										WHERE RFL.status_nm="Active" 
										GROUP BY RFL.response_id_fk )FRC','R.response_id=FRC.response_id_fk','left');
						$this->db->join('(SELECT UF.user_id_fk,UF.project_id_fk,
										CEIL(AVG(FG.grade)) AS gradng 
										FROM 
										user_feedbacks UF 
										JOIN feedback_grades FG 
										ON UF.grade_id_fk=FG.id 
										WHERE UF.status_nm = "Active" AND 
										UF.fb_given_date BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
										)FGRAD ', 'FGRAD.user_id_fk=R.user_id_fk AND FGRAD.project_id_fk= P.project_id ','left');	
						$this->db->join('feedback_grades JB', 'FGRAD.gradng=JB.grade  AND JB.status_nm="Active" ','left');
						//$this->db->where_in('R.dept_id_fk', $dept_opt);
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');	
						 if ($this->input->get('confirmed') == 'true') {
							$this->db->where('R.tast_stat', 'Confirmed');	
						 } 
						 $this->db->where('R.user_id_fk IN ('.$pt_emp.')',null,false);					 
						 $this->db->group_by('P.project_name,IFNULL(FR.r_param_name,"Quality Score"),R.milestone_id,T.task_name, CONCAT(S.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1)),S.sub_name,SUBSTRING_INDEX(J.job_name, "_", 1) ',false);
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
		 }
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 public function get_percentile($percentile, $array) {
    sort($array);
	$pos = (count($array) - 1) * ($percentile/100);
 
  $base = floor($pos);
  $rest = $pos - $base;
 
  if( isset($array[$base+1]) ) {
    return $array[$base] + $rest * ($array[$base+1] - $array[$base]);
  } else {
    return $array[$base];
  }
}

	 public function get_data_perc($file_nm,$date_start,$pro_sel,$date_end,$u_id,$dept_opt){		 
		 if($file_nm=="Graph_Summary")
		 {
			 $sql="SELECT U.full_name,
					R.user_id_fk,
					SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp,
					MAX(AGG.total_x) AS total_cp ,
					MAX(Main.attendance) AS att_cnt 
				FROM
					`response_mst` `R`  
				INNER JOIN `user_mst` `U` 
				ON `U`.`user_id` = `R`.`user_id_fk` 
				INNER JOIN `activity_mst` `A` 
				ON `R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 	
				INNER JOIN(
									SELECT R1.user_id_fk,
										SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS total_x
									FROM
										`response_mst` `R1`
									WHERE
										R1.day_val BETWEEN '".$date_start."' AND '".$date_end."'   
										AND `R1`.`status_nm` = 'Active'
									GROUP BY `R1`.`user_id_fk`
								) AGG
						ON AGG.user_id_fk = R.user_id_fk 	
				LEFT JOIN (SELECT IOR.user_id ,
									SUM(IOR.attendance_cnt) AS attendance 							
								FROM in_out_upload  IOR 													
									WHERE IOR.date_val BETWEEN  '".$date_start."' AND '".$date_end."'  
								 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm='Active'   
								 AND IOR.user_id IS NOT NULL 
								 GROUP BY IOR.user_id ) Main 
					ON Main.user_id = R.user_id_fk 
			 WHERE
					R.day_val BETWEEN '".$date_start."' AND '".$date_end."' 
					AND A.project_id_fk='".$pro_sel."' 
					AND `R`.`status_nm` = 'Active'  
				GROUP BY `R`.`user_id_fk`,U.full_name 
				ORDER BY `equiv_xp` ASC ;";	
				
				$sqlQuery =$this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }	
		 if($file_nm=="Graph_Sum_Sc")
		 {
			  $sql="SELECT U.full_name,
            R.user_id_fk,
            SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp,
			MAX(Main.attendance) AS att_cnt 
        FROM
            `response_mst` `R`  
		INNER JOIN `user_mst` `U` 
		ON `U`.`user_id` = `R`.`user_id_fk` 						
       INNER JOIN (
		SELECT project_id_fk AS project_id,project_name,user_id_fk 
		FROM (
			SELECT *,  
			@num := IF(@type = tab1.user_id_fk,@num + 1, 1 ) AS row_number,
				@type := tab1.user_id_fk AS dummy
			FROM
				(
				SELECT 
					A.project_id_fk,
					P1.project_name,
					R.user_id_fk,
					SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
				FROM
					`response_mst` `R` 
				INNER JOIN (SELECT @num := 0,@type := '') B
					ON 1=1
				INNER JOIN `activity_mst` `A` ON
					`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 
				INNER JOIN `project_mst` `P1` 
				ON `P1`.`project_id`=`A`.`project_id_fk` 
				WHERE
					R.day_val BETWEEN '".$date_start."' AND '".$date_end."' 
					AND  R.dept_id_fk ='".$dept_opt."' 
					AND `R`.`status_nm` = 'Active'
				GROUP BY `R`.`user_id_fk`,A.project_id_fk
				ORDER BY `R`.`user_id_fk`,equiv_xp DESC,P1.project_name
			) tab1
			GROUP BY    user_id_fk,project_id_fk,project_name,equiv_xp 
			HAVING row_number = 1
			)E 
			WHERE E.project_id_Fk='".$pro_sel."' 
			)CTE 
		ON CTE.user_id_fk=R.user_id_fk 
		LEFT JOIN (SELECT IOR.user_id ,
									SUM(IOR.attendance_cnt) AS attendance 							
								FROM in_out_upload  IOR 													
									WHERE IOR.date_val BETWEEN  '".$date_start."' AND '".$date_end."'  
								 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm='Active'   
								 AND IOR.user_id IS NOT NULL 
								 GROUP BY IOR.user_id ) Main 
					ON Main.user_id = R.user_id_fk 
	 WHERE
            R.day_val BETWEEN '".$date_start."' AND '".$date_end."' 
			AND `R`.`status_nm` = 'Active'  
        GROUP BY `R`.`user_id_fk`,U.full_name 
		ORDER BY `equiv_xp` ASC;";		
				$sqlQuery =$this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  }  
	 }
	public function team_member_select($file_nm,$date_start,$pro_sel,$date_end,$u_id,$lvl,$dept_opt){		 
		 
		 if($file_nm=="Team_Graphs" || $file_nm=="Team_Split_Graph")
		 {
			 if($lvl==0)
			 {
				$selec_val='CONCAT(A.activity_name," (",W.wu_name,")")';
			 }
			 if($lvl==1)
			 {
				$selec_val='P.project_name';
			 }
			 if($lvl==2)
			 {
				$selec_val='T.task_name';
			 }
			 if($lvl==3)
			 {
				$selec_val='ST.sub_name';
			 }
			 if($lvl==4)
			 {
				$selec_val='J.job_name';
			 }
			 if($lvl==5)
			 {
				$selec_val='CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))';
			 }
			 
			$this->db->select('  U.full_name,'.$selec_val.' AS activity_name,
			ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp  ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('user_mst U', 'U.user_id = R.user_id_fk','inner');
						$this->db->join('work_unit_mst W', 'W.wu_id = A.wu_id_fk AND W.status_nm="Active" ','left');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						if($lvl==2)
						{
						$this->db->join('task_mst T', 'T.task_id = A.task_id_fk AND T.status_nm="Active" ','inner');
						}
						if($lvl==3 || $lvl==5)
						{
						$this->db->join('sub_mst ST', 'ST.sub_id = A.sub_id_fk AND ST.status_nm="Active" ','inner');
						}
						if($lvl==4  || $lvl==5)
						{
						$this->db->join('job_mst J', 'J.job_id = A.job_id_fk AND J.status_nm="Active" ','inner');
						}
						$this->db->where('R.dept_id_fk', $dept_opt);
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');	
							$this->db->where_in('P.project_id', $pro_sel);
						 $this->db->group_by("U.full_name,".$selec_val."",false);
						if($file_nm=="Team_Graphs")
						{
										$this->db->order_by('activity_name', 'ASC');
						}else{
							$this->db->order_by('U.full_name', 'ASC');
						}
					$this->db->order_by("equiv_xp", "DESC");
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
		 }
		 
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 	 
	 }
	 
	public function tm_stats($file_nm,$date_start,$pro_sel,$date_end,$u_id,$lvl,$dept_opt)
	{
		 if($file_nm=="Graph_Sum_Sc")
		 {
			$sql= "SELECT 
					 AVG(DE.equiv_xp) AS Avg_val,
					 SUM(DE.equiv_xp) AS Sum_val 
					FROM 
					(
						SELECT 
								R.user_id_fk,
								SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
							FROM
								`response_mst` `R`  						
						   INNER JOIN (
							SELECT project_id_fk AS project_id,project_name,user_id_fk 
							FROM (
								SELECT *,  
								@num := IF(@type = tab1.user_id_fk,@num + 1, 1 ) AS row_number,
									@type := tab1.user_id_fk AS dummy
								FROM
									(
									SELECT 
										A.project_id_fk,
										P1.project_name,
										R.user_id_fk,
										SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
									FROM
										`response_mst` `R` 
									INNER JOIN (SELECT @num := 0,@type := '') B
										ON 1=1
									INNER JOIN `activity_mst` `A` ON
										`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 
									INNER JOIN `project_mst` `P1` 
									ON `P1`.`project_id`=`A`.`project_id_fk` 
									WHERE
										R.day_val BETWEEN '".$date_start."' AND '".$date_end."' 
										AND  R.dept_id_fk ='".$dept_opt."' 
										AND `R`.`status_nm` = 'Active'
									GROUP BY `R`.`user_id_fk`,A.project_id_fk
									ORDER BY `R`.`user_id_fk`,equiv_xp DESC,P1.project_name
								)tab1
								GROUP BY    user_id_fk,project_id_fk,project_name,equiv_xp 
								HAVING row_number = 1
								)E 
								WHERE E.project_id_Fk='".$pro_sel."' 
								)CTE 
							ON CTE.user_id_fk=R.user_id_fk 
						 WHERE
								R.day_val BETWEEN '".$date_start."' AND '".$date_end."' 
								AND `R`.`status_nm` = 'Active'  
							GROUP BY `R`.`user_id_fk`
						)DE		;";
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }
		 if($file_nm=="Graph_Summary")
		 {
			$sql= "SELECT SUM(DE.equiv_xp)/SUM(DE.prop) AS Avg_val,
						SUM(DE.equiv_xp) AS Sum_val
					FROM 
						(
						SELECT R.user_id_fk, 
						  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp ,
						  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) /AGG.total_x AS prop
						  FROM `response_mst` `R` 
						  INNER JOIN `activity_mst` `A` 
						  ON `R`.`activity_id_fk`=`A`.`activity_id` 
						  AND `A`.`status_nm`='Active'  
						  INNER JOIN `user_mst` `U` 
						  ON `U`.`user_id` = `R`.`user_id_fk`  
						  INNER JOIN `project_mst` `P` ON
							`P`.`project_id` = `A`.`project_id_fk` 
						  INNER JOIN(
									SELECT R1.user_id_fk,
										SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS total_x
									FROM
										`response_mst` `R1`
									WHERE
										R1.day_val BETWEEN '".$date_start."' AND '".$date_end."'   
										AND `R1`.`status_nm` = 'Active'
									GROUP BY `R1`.`user_id_fk`
								) AGG
						ON AGG.user_id_fk = R.user_id_fk 
						   WHERE R.dept_id_fk='".$dept_opt."' 						   
						   AND R.day_val BETWEEN '".$date_start."' AND '".$date_end."'   
						   AND  `R`.`status_nm` = 'Active' 
						   AND  `P`.`project_id` IN (".$pro_sel.") 
						   GROUP BY `R`.`user_id_fk`,AGG.total_x
						   ) DE; ";
				$sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }	
		 
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 	 
	 }
	 
	public function pt_team_member_select($file_nm,$date_start,$pt_emp,$date_end){
		 if($file_nm=="PT_Stats")
		 {
			$sql= ' SELECT AVG(DE.total_xp) AS Avg_val,
							SUM(DE.total_xp) AS Sum_val,
							MAX(DE.total_xp) AS Max_val,
							MIN(DE.total_xp) AS Min_val 
					FROM 
						(
						SELECT R.day_val,R.user_id_fk, 
						  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS total_xp 
						  FROM `response_mst` `R` 
						  INNER JOIN `activity_mst` `A` 
						  ON `R`.`activity_id_fk`=`A`.`activity_id` 
						  AND `A`.`status_nm`="Active" 
						  INNER JOIN `user_mst` `U` 
						  ON `U`.`user_id` = `R`.`user_id_fk`  
						  INNER JOIN `project_mst` `P` ON
							`P`.`project_id` = `A`.`project_id_fk` 						  
						   WHERE R.user_id_fk IN ("'.$pt_emp.'") 
						   AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
						   AND  `R`.`status_nm` = "Active"    
						   GROUP BY R.day_val,`R`.`user_id_fk`
						   ) DE; ';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }
		 	
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
		 
	 }	

public function pt_scatter_plot($file_nm,$date_start,$pt_emp,$date_end,$input_val=NULL,$output_val=NULL){
		if($file_nm=="PT_Feedback")
		{	
	 $this->db->select('  DATE_FORMAT(R.day_val, "%d/%m/%y") AS val_name,R.user_id_fk,
			I.in_time,
		 ROUND(IFNULL(I.esp_wrk_hrs,0),2) AS esp_wrk_hrs ,
		 I.attendance_cnt,ROUND(I.sapience_on,2) AS sapience_on,ROUND(I.sapience_off,2) AS sapience_off,
		 U.full_name, 		 
		 ROUND(IFNULL(SUM(IFNULL(final_xp,IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0)),0),2) AS proj_xp, 
		ROUND(IFNULL(SUM(CASE WHEN R.tast_stat="Confirmed"  THEN (IFNULL(final_xp,IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0))  ELSE 0 END ),0),2) AS act_xp  ',FALSE);
						$this->db->from('response_mst R');		
					$this->db->join('user_mst U ','U.user_id=R.user_id_fk','inner');	
					$this->db->join('in_out_upload I ','I.user_id=R.user_id_fk AND I.date_val=R.day_val AND I.status_nm="Active"','left');
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');		
						 $this->db->where('R.user_id_fk IN ('.$pt_emp.')',null,false);
						 $this->db->group_by("R.user_id_fk,U.full_name,R.day_val ",false);
						 $this->db->order_by("U.full_name,R.day_val", 'ASC',false); 
			$sqlQuery = $this->db->get();
			$resultSet = $sqlQuery->result_array();
		 }
		 if($file_nm=="FV_Feed")
		{	
	
	if($input_val=='esp_wrk_hrs' && $output_val=='act'){
				 $extendqry='ROUND(IFNULL(SUM(CASE WHEN R.tast_stat = "Confirmed" THEN (IFNULL(final_xp, IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0)) ELSE 0
          					END)/I.attendance_cnt,0),2) AS act_xp,
							(CASE WHEN IFNULL(I.attendance_cnt,0)=0 THEN 0.00 ELSE ROUND(I.esp_wrk_hrs/I.attendance_cnt,2) END) AS esp_wrk_hrs';
		}else if($input_val=='esp_wrk_hrs' & $output_val=='proj_xp'){
				$extendqry=' ROUND(IFNULL(SUM(IFNULL(final_xp, IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0))/I.attendance_cnt,0),2) AS proj_xp,
							(CASE WHEN IFNULL(I.attendance_cnt,0)=0 THEN 0.00 ELSE ROUND(I.esp_wrk_hrs/I.attendance_cnt,2) END) AS esp_wrk_hrs';
		}else if($input_val=='sapience_on' & $output_val=='act'){
			$extendqry='ROUND(IFNULL(SUM(CASE WHEN R.tast_stat = "Confirmed" THEN (IFNULL(final_xp, IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0)) ELSE 0
          					END)/I.attendance_cnt,0),2) AS act_xp,
						(CASE WHEN IFNULL(I.sapience_on,0)=0 THEN 0.00 ELSE ROUND(I.sapience_on/I.sapience_on,2) END) AS sapience_on';
			
		}else if($input_val=='sapience_on' & $output_val=='proj_xp'){
			$extendqry='ROUND(IFNULL(SUM(IFNULL(final_xp, IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0))/I.attendance_cnt,0),2) AS proj_xp,
						(CASE WHEN IFNULL(I.sapience_on,0)=0 THEN 0.00 ELSE ROUND(I.sapience_on/I.sapience_on,2) END) AS sapience_on';
		}
			
			$sql='SELECT   R.user_id_fk, U.full_name,'.$extendqry.'
FROM
    `response_mst` `R`
INNER JOIN `user_mst` `U` ON
    `U`.`user_id` = `R`.`user_id_fk`  
INNER JOIN(
    SELECT
        week_v,
        project_id_fk AS project_id,
        project_name,
        user_id_fk AS user_id
    FROM
        (
        SELECT *,@num := IF(@type = tab1.user_id_fk,@num + 1,1) AS row_number,
            @type := tab1.user_id_fk AS dummy
        FROM
            (
            SELECT
                YEARWEEK(R.day_val, 1) AS week_v,
                A.project_id_fk,
                P1.project_name,
                R.user_id_fk,
                SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
            FROM
                `response_mst` `R`
            INNER JOIN(SELECT @num := 0,@type := "") B
        ON 1 = 1
        INNER JOIN `activity_mst` `A` ON
            `R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active"
        INNER JOIN `project_mst` `P1` ON
            `P1`.`project_id` = `A`.`project_id_fk`
        WHERE
            R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND `R`.`status_nm` = "Active"
        GROUP BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`,A.project_id_fk
        ORDER BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`, equiv_xp DESC,P1.project_name
        ) tab1
    GROUP BY
        week_v,user_id_fk,project_id_fk,project_name,equiv_xp
    HAVING row_number = 1
    ) E
WHERE E.project_id_Fk IN ('.$pt_emp.')
)CTE ON CTE.user_id=R.user_id_fk 
INNER JOIN (
				SELECT user_id,SUM(IFNULL(sapience_on,0.00)) as sapience_on,
				SUM(IFNULL(esp_wrk_hrs,0.00)) AS esp_wrk_hrs , 
				SUM(attendance_cnt) AS attendance_cnt 
				FROM in_out_upload 
				WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND IFNULL(attendance_cnt,0.00)<>0.00 
				AND status_nm="Active" 
				group by user_id 
			) I 
	ON I.user_id=R.user_id_fk 
WHERE
    R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND `R`.`status_nm` = "Active" 
GROUP BY R.user_id_fk,U.full_name ';
			$sqlQuery = $this->db->query($sql);
			$resultSet = $sqlQuery->result_array();
		 }
		  if($file_nm=="WPR_Feed")
		{	
	
	if($input_val=='esp_wrk_hrs' && $output_val=='act'){
				 $extendqry='ROUND(IFNULL(SUM(CASE WHEN R.tast_stat = "Confirmed" THEN (IFNULL(final_xp, IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0)) ELSE 0
          					END)/I.attendance_cnt,0),2) AS act_xp,
							(CASE WHEN IFNULL(I.attendance_cnt,0)=0 THEN 0.00 ELSE ROUND(I.esp_wrk_hrs/I.attendance_cnt,2) END) AS esp_wrk_hrs';
		}else if($input_val=='esp_wrk_hrs' & $output_val=='proj_xp'){
				$extendqry=' ROUND(IFNULL(SUM(IFNULL(final_xp, IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0))/I.attendance_cnt,0),2) AS proj_xp,
							(CASE WHEN IFNULL(I.attendance_cnt,0)=0 THEN 0.00 ELSE ROUND(I.esp_wrk_hrs/I.attendance_cnt,2) END) AS esp_wrk_hrs';
		}else if($input_val=='sapience_on' & $output_val=='act'){
			$extendqry='ROUND(IFNULL(SUM(CASE WHEN R.tast_stat = "Confirmed" THEN (IFNULL(final_xp, IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0)) ELSE 0
          					END)/I.attendance_cnt,0),2) AS act_xp,
						(CASE WHEN IFNULL(I.sapience_on,0)=0 THEN 0.00 ELSE ROUND(I.sapience_on/I.sapience_on,2) END) AS sapience_on';
			
		}else if($input_val=='sapience_on' & $output_val=='proj_xp'){
			$extendqry='ROUND(IFNULL(SUM(IFNULL(final_xp, IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0))/I.attendance_cnt,0),2) AS proj_xp,
						(CASE WHEN IFNULL(I.sapience_on,0)=0 THEN 0.00 ELSE ROUND(I.sapience_on/I.sapience_on,2) END) AS sapience_on';
		}
			
			$sql='SELECT   R.user_id_fk, U.full_name,'.$extendqry.'
FROM
    `response_mst` `R`
INNER JOIN `user_mst` `U` ON
    `U`.`user_id` = `R`.`user_id_fk`  
INNER JOIN(
    SELECT
        week_v,
        project_id_fk AS project_id,
        project_name,
        user_id_fk AS user_id
    FROM
        (
        SELECT *,@num := IF(@type = tab1.user_id_fk,@num + 1,1) AS row_number,
            @type := tab1.user_id_fk AS dummy
        FROM
            (
            SELECT
                YEARWEEK(R.day_val, 1) AS week_v,
                A.project_id_fk,
                P1.project_name,
                R.user_id_fk,
                SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
            FROM
                `response_mst` `R`
            INNER JOIN(SELECT @num := 0,@type := "") B
        ON 1 = 1
        INNER JOIN `activity_mst` `A` ON
            `R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active"
        INNER JOIN `project_mst` `P1` ON
            `P1`.`project_id` = `A`.`project_id_fk`
        WHERE
            R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND `R`.`status_nm` = "Active"
        GROUP BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`,A.project_id_fk
        ORDER BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`, equiv_xp DESC,P1.project_name
        ) tab1
    GROUP BY
        week_v,user_id_fk,project_id_fk,project_name,equiv_xp
    HAVING row_number = 1
    ) E
WHERE E.project_id_Fk IN ('.$pt_emp.')
)CTE ON CTE.user_id=R.user_id_fk 
INNER JOIN (
				SELECT user_id,SUM(IFNULL(sapience_on,0.00)) as sapience_on,
				SUM(IFNULL(esp_wrk_hrs,0.00)) AS esp_wrk_hrs , 
				SUM(attendance_cnt) AS attendance_cnt 
				FROM in_out_upload 
				WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND IFNULL(attendance_cnt,0.00)<>0.00 
				AND status_nm="Active" 
				group by user_id 
			) I 
	ON I.user_id=R.user_id_fk 
WHERE
    R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND `R`.`status_nm` = "Active" 
GROUP BY R.user_id_fk,U.full_name ';
			$sqlQuery = $this->db->query($sql);
			$resultSet = $sqlQuery->result_array();
		 }
		 
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
}	


// public function daily_scatter_plot($file_nm,$date_start,$date_end,$pt_emp){
		
		 // if($file_nm=="Reports")
		// {	
			
			// $sql='SELECT 
    // R.user_id_fk,
    // U.full_name,
    // ROUND(IFNULL(SUM(IFNULL(final_xp, IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0))/I.attendance_cnt,0),2) AS proj_xp,
	// ROUND(IFNULL(SUM(CASE WHEN R.tast_stat = "Confirmed" THEN (IFNULL(final_xp, IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0)) ELSE 0
            // END)/I.attendance_cnt,0),2) AS act_xp,
	// (CASE WHEN IFNULL(I.attendance_cnt,0)=0 THEN 0.00 ELSE ROUND(I.esp_wrk_hrs/I.attendance_cnt,2) END) AS esp_wrk_hrs 
// FROM
    // `response_mst` `R`
// INNER JOIN `user_mst` `U` ON
    // `U`.`user_id` = `R`.`user_id_fk`  
// INNER JOIN(
    // SELECT
        // week_v,
        // project_id_fk AS project_id,
        // project_name,
        // user_id_fk AS user_id
    // FROM
        // (
        // SELECT *,@num := IF(@type = tab1.user_id_fk,@num + 1,1) AS row_number,
            // @type := tab1.user_id_fk AS dummy
        // FROM
            // (
            // SELECT
                // YEARWEEK(R.day_val, 1) AS week_v,
                // A.project_id_fk,
                // P1.project_name,
                // R.user_id_fk,
                // SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
            // FROM
                // `response_mst` `R`
            // INNER JOIN(SELECT @num := 0,@type := "") B
        // ON 1 = 1
        // INNER JOIN `activity_mst` `A` ON
            // `R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active"
        // INNER JOIN `project_mst` `P1` ON
            // `P1`.`project_id` = `A`.`project_id_fk`
        // WHERE
            // R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND `R`.`status_nm` = "Active"
        // GROUP BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`,A.project_id_fk
        // ORDER BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`, equiv_xp DESC,P1.project_name
        // ) tab1
    // GROUP BY
        // week_v,user_id_fk,project_id_fk,project_name,equiv_xp
    // HAVING row_number = 1
    // ) E
// WHERE E.project_id_Fk = '.$pt_emp.' 
// )CTE ON CTE.user_id=R.user_id_fk 
// INNER JOIN (
				// SELECT user_id,
				// SUM(IFNULL(esp_wrk_hrs,0.00)) AS esp_wrk_hrs , 
				// SUM(attendance_cnt) AS attendance_cnt 
				// FROM in_out_upload 
				// WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND IFNULL(attendance_cnt,0.00)<>0.00 
				// AND status_nm="Active" 
				// group by user_id 
			// ) I 
	// ON I.user_id=R.user_id_fk 
// WHERE
    // R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND `R`.`status_nm` = "Active" 
// GROUP BY R.user_id_fk,U.full_name ';
			// $sqlQuery = $this->db->query($sql);
			// $resultSet = $sqlQuery->result_array();
		 // }
	  // if($resultSet)
	  // {
		  // return $resultSet;
	  // }
	  // else{
		// return false;
	  // } 
// }	
	 
	 public function pt_team_member_select3($file_nm,$date_start,$pt_emp,$date_end,$action){
		 if($file_nm=="PT_Stats" && $action=="xp")
		 {
			$sql= 'SELECT  BE.user_id_fk,BE.activity_id_fk ,BE.act_nm,BE.totl_xp ,BE.row_mn
						 FROM 
						 (SELECT DE.user_id_fk,DE.activity_id_fk ,DE.act_nm,DE.totl_xp , 
						@i:=(IF(DE.totl_xp=@pre_val,@i,@i+1)) AS row_mn,
						@pre_val:=DE.totl_xp 
					FROM 
						(
						SELECT R.activity_id_fk,CONCAT(A.activity_name,"(",W.wu_name,")") AS act_nm,R.user_id_fk, 
						  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS totl_xp 
						  FROM `response_mst` `R` 
						  INNER JOIN `activity_mst` `A` 
						  ON `R`.`activity_id_fk`=`A`.`activity_id` 
						  AND `A`.`status_nm`="Active" 
						  INNER JOIN `project_mst` `P` ON
							`P`.`project_id` = `A`.`project_id_fk` 	
							INNER JOIN `work_unit_mst` `W` ON
							`W`.`wu_id` = `A`.`wu_id_fk` 								
						   WHERE R.user_id_fk IN ("'.$pt_emp.'") 
						   AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
						   AND  `R`.`status_nm` = "Active"    
						   GROUP BY R.activity_id_fk,CONCAT(A.activity_name,"_",W.wu_name),`R`.`user_id_fk` 
						   ) DE 
						   JOIN (SELECT @i:=0,@pre_val:="")OP 
						   ON 1=1 
						ORDER BY `DE`.`user_id_fk` ,DE.totl_xp DESC 
						)BE 
						HAVING BE.row_mn=1 
						    ';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }		 	
		if($file_nm=="PT_Stats" && $action=="score")
		 {
			$sql= 'SELECT  BE.user_id_fk,BE.activity_id_fk ,BE.act_nm,BE.totl_xp ,BE.row_mn
						 FROM 
						 (SELECT DE.user_id_fk,DE.activity_id_fk ,DE.act_nm,DE.totl_xp , 
						@i:=(IF(DE.totl_xp=@pre_val,@i,@i+1)) AS row_mn,
						@pre_val:=DE.totl_xp 
					FROM 
						(
						SELECT R.activity_id_fk,CONCAT(A.activity_name,"(",W.wu_name,")") AS act_nm,
						R.user_id_fk, 
						  SUM(R.review_xp) AS totl_xp 
						  FROM `response_mst` `R` 
						  INNER JOIN `activity_mst` `A` 
						  ON `R`.`activity_id_fk`=`A`.`activity_id` 
						  AND `A`.`status_nm`="Active" 
						  INNER JOIN `project_mst` `P` ON
							`P`.`project_id` = `A`.`project_id_fk` 	
							INNER JOIN `work_unit_mst` `W` ON
							`W`.`wu_id` = `A`.`wu_id_fk` 								
						   WHERE R.user_id_fk IN ("'.$pt_emp.'") 
						   AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
						   AND  `R`.`status_nm` = "Active"    
						   GROUP BY R.activity_id_fk,CONCAT(A.activity_name,"_",W.wu_name),`R`.`user_id_fk` 
						   ) DE 
						   JOIN (SELECT @i:=0,@pre_val:="")OP 
						   ON 1=1 
						ORDER BY `DE`.`user_id_fk` ,DE.totl_xp DESC 
						)BE 
						HAVING BE.row_mn=1 
						    ';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
		 
	 }
	 
	  public function fv_summ_c($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,$action,$dept_opt)
	 {
		  if($file_nm=="Floor_View" && $action==1)
		 {
			$sql= ' 	SELECT DE.project_id,DE.project_name,
			SUM(DE.equiv_xp)/SUM(DE.prop) AS Avg_val,
			SUM(DE.day_cnt) AS ov_eff_den,
			SUM(DE.equiv_xp)/SUM(DE.prop2) AS efficiency,
			ROUND(SUM(DE.prop),2) AS team_mem,
			SUM(DE.equiv_xp) AS Sum_val,
			SUM(DE.nq_sum) AS nonq_sum,
			COUNT(DISTINCT DE.user_id_fk) AS Mem_c,
			MAX(DE.equiv_xp) AS v1,
			MIN(DE.equiv_xp) AS v2 ,
			SUM(DE.A_stat) AS A_stat,
			SUM(DE.R_stat) AS R_stat,
			SUM(DE.D_stat) AS D_stat,
			SUM(DE.C_stat) AS C_stat,
			MAX(DE.G_done) AS G_done ,
			CEIL(AVG(DE.a_grade)) AS a_grade
			FROM 
			(SELECT P.project_id,P.project_name,R.user_id_fk, 
			Main.day_cnt,
		  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp,
		  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) /AGG.total_x AS prop,
		  (SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) * Main.day_cnt) /AGG.total_x AS prop2,
		  SUM(CASE WHEN WUM.is_quantify="N" THEN (IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) ELSE 0 END) AS nq_sum,
		  SUM(CASE WHEN R.tast_stat="Audit Pending" THEN 1 ELSE 0 END) AS A_stat,
		  SUM(CASE WHEN R.tast_stat="Review Pending" THEN 1 ELSE 0 END) AS R_stat,
		  SUM(CASE WHEN R.tast_stat="Confirmation Pending" THEN 1 ELSE 0 END) AS D_stat,
		  SUM(CASE WHEN R.tast_stat="Confirmed" THEN 1 ELSE 0 END) AS C_stat ,
		  MAX(IFNULL(UF.g_cnt,0)) AS G_done ,
		  AVG(UF.avg_grade) AS a_grade
		  FROM `response_mst` `R`  		  
		  INNER JOIN `activity_mst` `A` 
		   ON `R`.`activity_id_fk`=`A`.`activity_id` 		      
		  AND `A`.`status_nm`="Active" 
		  INNER JOIN `project_mst` `P` 
		 	ON `P`.`project_id`=`A`.`project_id_fk`  AND `P`.`status_nm` = "Active" 
		  LEFT JOIN `user_mst` `U` 
		  ON `U`.`user_id` = `R`.`user_id_fk` 
		  LEFT JOIN `work_unit_mst` `WUM` 
		  ON `A`.`wu_id_fk` = `WUM`.`wu_id` 
		  LEFT JOIN (SELECT
						project_id_fk,
						COUNT(DISTINCT user_id_fk) AS g_cnt	,
						avg(grade_id_fk) AS avg_grade 
					FROM
						user_feedbacks 
					WHERE
						fb_given_date >= "'.$date_start.'" AND fb_given_date <= "'.$date_end.'" 
						AND status_nm = "Active" 
						GROUP BY project_id_fk 
						)UF
				ON UF.project_id_fk=P.project_id
		  LEFT JOIN(
					SELECT R1.user_id_fk,
						SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS total_x
					FROM
						`response_mst` `R1`
					WHERE
						R1.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
						AND `R1`.`status_nm` = "Active"
					GROUP BY `R1`.`user_id_fk`
				) AGG
		ON AGG.user_id_fk = R.user_id_fk 
		LEFT JOIN (SELECT IOR.user_id ,
									SUM(IOR.attendance_cnt) AS day_cnt 
								FROM in_out_upload  IOR 													
									WHERE IOR.date_val BETWEEN  "'.$date_start.'" AND "'.$date_end.'"  
								 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm="Active" 
								 AND IOR.user_id IS NOT NULL 
								 GROUP BY IOR.user_id ) Main 
					ON Main.user_id = R.user_id_fk   
		    WHERE R.dept_id_fk='.$dept_opt.'  AND `R`.`status_nm`="Active" 
				AND  R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
		   GROUP BY P.project_id,P.project_name,`R`.`user_id_fk`,AGG.total_x,Main.day_cnt
		   ) DE 
		   GROUP BY DE.project_id,DE.project_name ORDER BY DE.project_name; ';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }
		 if($file_nm=="Floor_View" && $action==2)
		 {
			$sql= 'SELECT P.project_id,P.project_name,R.user_id_fk, 
			AGG.total_x,
		  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp,
		  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) /AGG.total_x AS prop 
		  FROM `project_mst` `P` 		  
		  LEFT JOIN `activity_mst` `A` 
		   ON `P`.`project_id`=`A`.`project_id_fk`		   
		  AND `A`.`status_nm`="Active" 
		  LEFT JOIN `response_mst` `R` 
		  ON `R`.`activity_id_fk`=`A`.`activity_id` 		   
		  AND `R`.`status_nm`="Active" 
		  LEFT JOIN `user_mst` `U` 
		  ON `U`.`user_id` = `R`.`user_id_fk` 
		  LEFT JOIN(
					SELECT R1.user_id_fk,
						SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS total_x
					FROM
						`response_mst` `R1`
					WHERE
						R1.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
						AND `R1`.`status_nm` = "Active"
					GROUP BY `R1`.`user_id_fk`
				) AGG
		ON AGG.user_id_fk = R.user_id_fk
		    WHERE P.dept_id_fk='.$dept_opt.' AND `P`.`status_nm` = "Active" 
				AND  R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
		   GROUP BY P.project_id,P.project_name,`R`.`user_id_fk`,AGG.total_x;';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 public function fv_wo($file_nm, $date_start,$date_end,$cn_dt,$action,$dept_opt)
	 {		 
			 if($file_nm=="FV_Wow" && $action==1)
			 {
				$sql='SELECT B.week_val,B.project_id,B.project_name,SUM(B.xp) AS equ_x 
							 FROM ( 
								SELECT
								TOT.week_val,
								TOT.project_id,
								TOT.project_name,
								TOT.activity_id_fk,
								(TOT.qty_xp / TGT.tgt_val)*10 AS xp
							FROM
								(
								SELECT
									YEARWEEK(R.day_val, 1) AS week_val,
									P.project_id,
									P.project_name,
									R.activity_id_fk,
									SUM(IFNULL(R.audit_work, R.work_comp)) AS qty_xp
								FROM
									response_mst R
								INNER JOIN activity_mst AC ON
									R.activity_id_fk = AC.activity_id 
								INNER JOIN work_unit_mst W 
								ON W.wu_id=AC.wu_id_fk 
								INNER JOIN project_mst P ON
									P.project_id = AC.project_id_fk
								WHERE
									R.dept_id_fk IN ('.$dept_opt.' ) AND W.is_quantify="Y"  AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND R.status_nm = "Active"
								GROUP BY
									YEARWEEK(R.day_val, 1),
									P.project_id,
									P.project_name,
									R.activity_id_fk
							)TOT
						INNER JOIN(
									SELECT S2.activity_id_fk,S2.tgt_val
										FROM
											std_tgt_mst S2 
										INNER JOIN (    
												   SELECT  S3.activity_id_fk,MIN(S3.start_dt) AS st_dt
													FROM
														std_tgt_mst S3
													   INNER JOIN activity_mst A6
														ON A6.activity_id=S3.activity_id_fk AND A6.dept_id_fk IN ('.$dept_opt.' )
													WHERE
														S3.start_dt >= "'.$cn_dt.'" AND S3.start_dt <= IFNULL(S3.end_dt, S3.start_dt) 
														AND S3.status_nm = "Active" 
														AND S3.activity_id_fk IN (SELECT R2.activity_id_fk FROM response_mst R2
																						WHERE R2.dept_id_fk IN ('.$dept_opt.' ) AND 
																						R2.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
																						AND R2.status_nm = "Active")
														GROUP BY S3.activity_id_fk 
													 UNION
													SELECT S5.activity_id_fk,MAX(S5.start_dt) AS st_dt
													FROM
														std_tgt_mst S5
													INNER JOIN activity_mst A5 ON
														A5.activity_id = S5.activity_id_fk AND A5.dept_id_fk IN ('.$dept_opt.' )
													WHERE
														S5.start_dt <= IFNULL(S5.end_dt, S5.start_dt) AND S5.status_nm = "Active" 
														AND S5.activity_id_fk IN (SELECT R2.activity_id_fk FROM response_mst R2
																						WHERE R2.dept_id_fk IN ('.$dept_opt.' ) AND 
																						R2.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
																						AND R2.status_nm = "Active")
													GROUP BY
														S5.activity_id_fk
													HAVING MAX(S5.start_dt) <= "'.$cn_dt.'"
												)S4
											ON S2.activity_id_fk=S4.activity_id_fk AND S2.start_dt=S4.st_dt
										WHERE S2.start_dt <= IFNULL(S2.end_dt, S2.start_dt) AND S2.status_nm="Active" 
										AND S2.activity_id_fk is not null
						) TGT
						ON TGT.activity_id_fk = TOT.activity_id_fk
						)B 
						 GROUP BY B.week_val,B.project_id,B.project_name ORDER BY B.project_name; ';
					
			 }
			 if($file_nm=="FV_Wow" && $action==0)
			 {
				$sql='SELECT B.week_val,B.project_id,B.project_name,SUM(B.xp) AS equ_x 
							 FROM ( 
								SELECT
								TOT.week_val,
								TOT.project_id,
								TOT.project_name,
								TOT.activity_id_fk,
								(TOT.qty_xp / TGT.tgt_val)*10 AS xp
							FROM
								(
								SELECT
									YEARWEEK(R.day_val, 1) AS week_val,
									P.project_id,
									P.project_name,
									R.activity_id_fk,
									SUM(IFNULL(R.audit_work, R.work_comp)) AS qty_xp
								FROM
									response_mst R
								INNER JOIN activity_mst AC ON
									R.activity_id_fk = AC.activity_id
								INNER JOIN project_mst P ON
									P.project_id = AC.project_id_fk
								WHERE
									R.dept_id_fk IN ('.$dept_opt.' ) AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND R.status_nm = "Active"
								GROUP BY
									YEARWEEK(R.day_val, 1),
									P.project_id,
									P.project_name,
									R.activity_id_fk
							)TOT
						INNER JOIN(
									SELECT S2.activity_id_fk,S2.tgt_val
										FROM
											std_tgt_mst S2 
										INNER JOIN (    
												   SELECT  S3.activity_id_fk,MIN(S3.start_dt) AS st_dt
													FROM
														std_tgt_mst S3
													   INNER JOIN activity_mst A6
														ON A6.activity_id=S3.activity_id_fk AND A6.dept_id_fk IN ('.$dept_opt.' )
													WHERE
														S3.start_dt >= "'.$cn_dt.'" AND S3.start_dt <= IFNULL(S3.end_dt, S3.start_dt) 
														AND S3.status_nm = "Active" AND S3.activity_id_fk IN (SELECT R2.activity_id_fk FROM response_mst R2
																						WHERE R2.dept_id_fk IN ('.$dept_opt.' ) AND 
																						R2.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
																						AND R2.status_nm = "Active")
														GROUP BY S3.activity_id_fk 
													 UNION
													SELECT S5.activity_id_fk,MAX(S5.start_dt) AS st_dt
													FROM
														std_tgt_mst S5
													INNER JOIN activity_mst A5 ON
														A5.activity_id = S5.activity_id_fk AND A5.dept_id_fk IN ('.$dept_opt.' )
													WHERE
														S5.start_dt <= IFNULL(S5.end_dt, S5.start_dt) AND S5.status_nm = "Active" 
														AND S5.activity_id_fk IN (SELECT R2.activity_id_fk FROM response_mst R2
																						WHERE R2.dept_id_fk IN ('.$dept_opt.' ) AND 
																						R2.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
																						AND R2.status_nm = "Active")
													GROUP BY
														S5.activity_id_fk
													HAVING MAX(S5.start_dt) <= "'.$cn_dt.'"
												)S4
											ON S2.activity_id_fk=S4.activity_id_fk AND S2.start_dt=S4.st_dt
										WHERE S2.start_dt <= IFNULL(S2.end_dt, S2.start_dt) AND S2.status_nm="Active" 
										AND S2.activity_id_fk is not null
						) TGT
						ON TGT.activity_id_fk = TOT.activity_id_fk
						)B 
				GROUP BY B.week_val,B.project_id,B.project_name ORDER BY B.project_name; '; 	
			 }
		$sqlQuery = $this->db->query($sql);
		$resultSet = $sqlQuery->result_array();
		
		if($resultSet)
			  {
				  return $resultSet;
			  }
			  else{
				return false;
			  } 
	 }
	 
	  public function fv_cn_dt($file_nm, $date_start,$date_end,$action)
	 {		 
			 if($file_nm=="FV_Wow" && $action=="Tgt_Date")
			 {		
			 $this->db->where('status_nm', 'Active');
			 $this->db->where('def_variable',$action);
			 $this->db->from('def_values');  		 
                 $this->db->select('def_val'); 			 
				 $sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
			 }
			  if($file_nm=="FV_Wow" && $action=="Day_Count")
			 { 
				$sql1='SELECT DISTINCT YearWeek(R.day_val,1) AS w_val,DEF.def_val
						FROM response_mst R 
						INNER JOIN def_values DEF 
						ON DEF.def_variable="Day_Count" 
						AND R.day_val BETWEEN DEF.`start_dt` AND IFNULL(DEF.end_dt,"'.$date_end.'"  )
						AND DEF.status_nm="Active" 
						WHERE  R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
						AND R.status_nm="Active" ; ';
				$sqlQuery = $this->db->query($sql1);				
				$resultSet = $sqlQuery->result_array();				 
			 }
		if($resultSet)
			  {
				  return $resultSet;
			  }
			  else{
				return false;
			  } 
	 }
	 
	 public function fv_wo_mem($file_nm, $date_start,$date_end,$cn_dt,$action,$tot,$dept_opt,$o_n_c)
	 {
			 if($file_nm=="FV_Wow" && $action==1)
			 {
				 if($tot==1)
				 { 
					 if($o_n_c==1)
					 {
						 $sql='SELECT DE.week_val,
					 DE.project_id,
						DE.project_name,
						ROUND(SUM(DE.prop),2) AS mem 
					FROM
					(
					SELECT
						YEARWEEK(R.day_val, 1) AS week_val,
						P.project_id,
						P.project_name,
						R.user_id_fk,
						(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) /AGG.total_x)* Main.day_cnt AS prop 
					FROM
						response_mst R
					INNER JOIN activity_mst ac ON
						R.activity_id_fk = ac.activity_id 
					INNER JOIN work_unit_mst W 
					 ON W.wu_id=ac.wu_id_fk 
					INNER JOIN project_mst P ON
						P.project_id = ac.project_id_fk 
					   INNER JOIN(
									SELECT YEARWEEK(R1.day_val, 1) AS week_val,R1.user_id_fk,
										SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS total_x
									FROM
										`response_mst` `R1` 
										INNER JOIN activity_mst A ON
											R1.activity_id_fk = A.activity_id 
										INNER JOIN work_unit_mst WA 
										 ON WA.wu_id=A.wu_id_fk 
									WHERE
										R1.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
										AND WA.is_quantify="Y"  
										AND `R1`.`status_nm` = "Active"
									GROUP BY YEARWEEK(R1.day_val, 1),`R1`.`user_id_fk`
								) AGG
						ON AGG.user_id_fk = R.user_id_fk AND AGG.week_val = YEARWEEK(R.day_val, 1) 
					LEFT JOIN (SELECT YEARWEEK(IOR.date_val, 1) AS week_v,IOR.user_id ,
									SUM(IOR.attendance_cnt) AS day_cnt 
								FROM in_out_upload  IOR 													
									WHERE IOR.date_val BETWEEN  "'.$date_start.'" AND "'.$date_end.'"  
								 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm="Active" 
								 AND IOR.user_id IS NOT NULL 
								 GROUP BY YEARWEEK(IOR.date_val, 1) ,IOR.user_id ) Main 
					ON Main.user_id = R.user_id_fk AND Main.week_v = YEARWEEK(R.day_val, 1)  
					WHERE
						R.dept_id_fk = '.$dept_opt.' AND W.is_quantify="Y"  
						AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND R.status_nm = "Active" 						
					GROUP BY YEARWEEK(R.day_val, 1),P.project_id,P.project_name,R.user_id_fk,AGG.total_x,Main.day_cnt
					)DE 					
					GROUP BY DE.week_val,DE.project_id,DE.project_name;';
						 
					 }else{
			$sql='SELECT DE.week_val,
					 DE.project_id,
						DE.project_name,
						ROUND(SUM(DE.prop),2) AS mem 
					FROM
					(
					SELECT
						YEARWEEK(R.day_val, 1) AS week_val,
						P.project_id,
						P.project_name,
						R.user_id_fk,
						SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) /AGG.total_x AS prop 
					FROM
						response_mst R
					INNER JOIN activity_mst ac ON
						R.activity_id_fk = ac.activity_id 
					INNER JOIN work_unit_mst W 
					 ON W.wu_id=ac.wu_id_fk 
					INNER JOIN project_mst P ON
						P.project_id = ac.project_id_fk 
					   INNER JOIN(
									SELECT YEARWEEK(R1.day_val, 1) AS week_val,R1.user_id_fk,
										SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS total_x
									FROM
										`response_mst` `R1` 
										INNER JOIN activity_mst A ON
											R1.activity_id_fk = A.activity_id 
										INNER JOIN work_unit_mst WA 
										 ON WA.wu_id=A.wu_id_fk 
									WHERE
										R1.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
										AND WA.is_quantify="Y"  
										AND `R1`.`status_nm` = "Active"
									GROUP BY YEARWEEK(R1.day_val, 1),`R1`.`user_id_fk`
								) AGG
						ON AGG.user_id_fk = R.user_id_fk AND AGG.week_val = YEARWEEK(R.day_val, 1) 
					LEFT JOIN (SELECT YEARWEEK(IOR.date_val, 1) AS week_v,IOR.user_id ,
									SUM(IOR.attendance_cnt) AS day_cnt 
								FROM in_out_upload  IOR 													
									WHERE IOR.date_val BETWEEN  "'.$date_start.'" AND "'.$date_end.'"  
								 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm="Active" 
								 AND IOR.user_id IS NOT NULL 
								 GROUP BY YEARWEEK(IOR.date_val, 1) ,IOR.user_id ) Main 
					ON Main.user_id = R.user_id_fk AND Main.week_v = YEARWEEK(R.day_val, 1)  
					WHERE
						R.dept_id_fk = '.$dept_opt.' AND W.is_quantify="Y"  
						AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND R.status_nm = "Active" 
						
					GROUP BY YEARWEEK(R.day_val, 1),P.project_id,P.project_name,R.user_id_fk,AGG.total_x,Main.day_cnt
					)DE 
					
					GROUP BY DE.week_val,DE.project_id,DE.project_name ORDER BY DE.project_name;';
					 }
				 }
				 if($tot==2)
				 {
					 if($o_n_c==1)
					 {
				$sql=' SELECT CC.week_val,(SUM(CC.day_cnt)) AS mem 
						FROM 
						(
						SELECT YearWeek(R.day_val,1) AS week_val, 
						R.user_id_fk,
							Main.day_cnt 
						FROM response_mst R  
						INNER JOIN activity_mst ac 
						ON R.activity_id_fk=ac.activity_id	
						INNER JOIN work_unit_mst W 
						ON W.wu_id=ac.wu_id_fk 
						LEFT JOIN (SELECT YEARWEEK(IOR.date_val, 1) AS week_v,IOR.user_id ,
									SUM(IOR.attendance_cnt) AS day_cnt 
								FROM in_out_upload  IOR 													
									WHERE IOR.date_val BETWEEN  "'.$date_start.'" AND "'.$date_end.'"  
								 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm="Active" 
								 AND IOR.user_id IS NOT NULL 
								 GROUP BY YEARWEEK(IOR.date_val, 1) ,IOR.user_id )Main 
					ON Main.user_id = R.user_id_fk AND Main.week_v = YEARWEEK(R.day_val, 1)
						WHERE  R.dept_id_fk='.$dept_opt.' AND W.is_quantify="Y" 
						AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
						AND R.status_nm="Active"  
						GROUP BY YearWeek(R.day_val,1),R.user_id_fk,Main.day_cnt
						)CC 
						GROUP BY CC.week_val;';
					 }else{
						 $sql=' SELECT YearWeek(R.day_val,1) AS week_val,COUNT(DISTINCT R.user_id_fk) AS mem 
						FROM response_mst R  
						INNER JOIN activity_mst ac 
						ON R.activity_id_fk=ac.activity_id	
						INNER JOIN work_unit_mst W 
						ON W.wu_id=ac.wu_id_fk 
						WHERE  R.dept_id_fk='.$dept_opt.' AND W.is_quantify="Y" 
						AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
						AND R.status_nm="Active"  
						GROUP BY YearWeek(R.day_val,1);';
						 
					 }
				 }
			 }
			 if($file_nm=="FV_Wow" && $action==0)
			 {
				 if($tot==1)
				 {
					 if($o_n_c==1)
					 {
				$sql='SELECT DE.week_val,
							 DE.project_id,
								DE.project_name,
								ROUND(SUM(DE.prop),2) AS mem 
							FROM
							(
							SELECT
								YEARWEEK(R.day_val, 1) AS week_val,
								P.project_id,
								P.project_name,
								R.user_id_fk,
								(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) /AGG.total_x)* Main.day_cnt AS prop 
							FROM
								response_mst R
							INNER JOIN activity_mst ac ON
								R.activity_id_fk = ac.activity_id
							INNER JOIN project_mst P ON
								P.project_id = ac.project_id_fk 
							   INNER JOIN(
									SELECT YEARWEEK(R1.day_val, 1) AS week_val,R1.user_id_fk,
										SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS total_x
									FROM
										`response_mst` `R1`
									WHERE
										R1.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
										AND `R1`.`status_nm` = "Active"
									GROUP BY YEARWEEK(R1.day_val, 1),`R1`.`user_id_fk`
								) AGG
						ON AGG.user_id_fk = R.user_id_fk 
						AND AGG.week_val = YEARWEEK(R.day_val, 1)  
						LEFT JOIN (SELECT YEARWEEK(IOR.date_val, 1) AS week_v,IOR.user_id ,
									SUM(IOR.attendance_cnt) AS day_cnt 
								FROM in_out_upload  IOR 													
									WHERE IOR.date_val BETWEEN  "'.$date_start.'" AND "'.$date_end.'"  
								 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm="Active" 
								 AND IOR.user_id IS NOT NULL 
								 GROUP BY YEARWEEK(IOR.date_val, 1) ,IOR.user_id ) Main 
					ON Main.user_id = R.user_id_fk AND Main.week_v = YEARWEEK(R.day_val, 1) 
							WHERE
								R.dept_id_fk = '.$dept_opt.' AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
								AND R.status_nm = "Active"
							GROUP BY YEARWEEK(R.day_val, 1),P.project_id,P.project_name,R.user_id_fk,AGG.total_x,Main.day_cnt 
							)DE
							GROUP BY DE.week_val,DE.project_id,DE.project_name ORDER BY DE.project_name;';
				 }else{
					 $sql='SELECT DE.week_val,
							 DE.project_id,
								DE.project_name,
								ROUND(SUM(DE.prop),2) AS mem 
							FROM
							(
							SELECT
								YEARWEEK(R.day_val, 1) AS week_val,
								P.project_id,
								P.project_name,
								R.user_id_fk,
								SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) /AGG.total_x AS prop 
							FROM
								response_mst R
							INNER JOIN activity_mst ac ON
								R.activity_id_fk = ac.activity_id
							INNER JOIN project_mst P ON
								P.project_id = ac.project_id_fk 
							   INNER JOIN(
									SELECT YEARWEEK(R1.day_val, 1) AS week_val,R1.user_id_fk,
										SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS total_x
									FROM
										`response_mst` `R1`
									WHERE
										R1.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
										AND `R1`.`status_nm` = "Active"
									GROUP BY YEARWEEK(R1.day_val, 1),`R1`.`user_id_fk`
								) AGG
						ON AGG.user_id_fk = R.user_id_fk 
						AND AGG.week_val = YEARWEEK(R.day_val, 1)  
							WHERE
								R.dept_id_fk = '.$dept_opt.' AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
								AND R.status_nm = "Active"
							GROUP BY YEARWEEK(R.day_val, 1),P.project_id,P.project_name,R.user_id_fk,AGG.total_x
							)DE
							GROUP BY DE.week_val,DE.project_id,DE.project_name ORDER BY DE.project_name;';
				 }
				 }
				 if($tot==2)
				 {
						if($o_n_c==1)
						{
				$sql=' 
				 SELECT CC.week_val,(SUM(CC.day_cnt)) AS mem 
						FROM 
						(
						SELECT YearWeek(R.day_val,1) AS week_val, 
						R.user_id_fk,
							Main.day_cnt 
						FROM response_mst R  
						INNER JOIN activity_mst ac 
						ON R.activity_id_fk=ac.activity_id	
						LEFT JOIN (SELECT YEARWEEK(IOR.date_val, 1) AS week_v,IOR.user_id ,
									SUM(IOR.attendance_cnt) AS day_cnt 
								FROM in_out_upload  IOR 													
									WHERE IOR.date_val BETWEEN  "'.$date_start.'" AND "'.$date_end.'"  
								 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm="Active" 
								 AND IOR.user_id IS NOT NULL 
								 GROUP BY YEARWEEK(IOR.date_val, 1) ,IOR.user_id )Main 
					ON Main.user_id = R.user_id_fk AND Main.week_v = YEARWEEK(R.day_val, 1) 
						WHERE  R.dept_id_fk='.$dept_opt.' AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
						AND R.status_nm="Active"  
						GROUP BY YearWeek(R.day_val,1),R.user_id_fk,Main.day_cnt
						)CC 
						GROUP BY CC.week_val ;';
						}else{
							$sql=' SELECT YearWeek(R.day_val,1) AS week_val,COUNT(DISTINCT R.user_id_fk) AS mem 
						FROM response_mst R  
						INNER JOIN activity_mst ac 
						ON R.activity_id_fk=ac.activity_id	
						WHERE  R.dept_id_fk='.$dept_opt.' AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
						AND R.status_nm="Active"  
						GROUP BY YearWeek(R.day_val,1);';
						}
				 }
			 }
		$sqlQuery = $this->db->query($sql);
		$resultSet = $sqlQuery->result_array();
		
		if($resultSet)
			  {
				  return $resultSet;
			  }
			  else{
				return false;
			  } 
	 }
	 
	 public function fv_summ($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,$action,$dept_opt)
	 {
		 if($file_nm=="FV_Spread" && $action==1)
		 {
			 if($pro_sel_val)
			 {
				 $sql= ' SELECT 
						DE.week_val,
						SUM(DE.equiv_xp)/SUM(DE.prop) AS Avg_val,
						ROUND(SUM(DE.prop),2) AS team_mem,
						MAX(DE.equiv_xp) AS v1,
						MIN(DE.equiv_xp) AS v2 
				FROM (		   
					SELECT
						YEARWEEK(R.day_val, 1) AS week_val,
						P.project_name,
						R.user_id_fk,
						SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp,
						SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) /AGG.total_x AS prop
					FROM
						`response_mst` `R`
					INNER JOIN(
								SELECT YEARWEEK(R1.day_val, 1) AS week_val,
									R1.user_id_fk,
									SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS total_x
								FROM
									`response_mst` `R1`
								WHERE
									R1.dept_id_fk = '.$dept_opt.' AND R1.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND `R1`.`status_nm` = "Active"
								GROUP BY
									YEARWEEK(R1.day_val, 1),
									`R1`.`user_id_fk`
							) AGG
					ON AGG.week_val = YEARWEEK(R.day_val, 1) AND AGG.user_id_fk = R.user_id_fk
					INNER JOIN `activity_mst` `A` ON
						`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active"
					INNER JOIN `user_mst` `U` ON
						`U`.`user_id` = `R`.`user_id_fk`
					INNER JOIN `project_mst` `P` ON
						`P`.`project_id` = `A`.`project_id_fk` 
					LEFT JOIN (SELECT YEARWEEK(IOR.date_val, 1) AS week_v,IOR.user_id ,
									SUM(IOR.attendance_cnt) AS day_cnt 
								FROM in_out_upload  IOR 													
									WHERE IOR.date_val BETWEEN  "'.$date_start.'" AND "'.$date_end.'"  
								 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm="Active" 
								 AND IOR.user_id IS NOT NULL 
								 GROUP BY YEARWEEK(IOR.date_val, 1) ,IOR.user_id) Main 
					ON Main.user_id = R.user_id_fk AND Main.week_v = YEARWEEK(R.day_val, 1)  
					WHERE
						R.dept_id_fk = '.$dept_opt.' AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND `R`.`status_nm` = "Active" 
						AND P.project_id="'.$pro_sel_val.'"  
					GROUP BY
						YEARWEEK(R.day_val, 1),P.project_id,P.project_name,`R`.`user_id_fk`,AGG.total_x
				) DE GROUP BY DE.week_val;';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
			 }else{
			$sql= 'SELECT 
		DE.week_val,
		SUM(DE.equiv_xp)/SUM(DE.prop) AS Avg_val,
		ROUND(SUM(DE.prop),2) AS team_mem,
		MAX(DE.equiv_xp) AS v1,MIN(DE.equiv_xp) AS v2 
	FROM (		   
		SELECT
			YEARWEEK(R.day_val, 1) AS week_val,
			P.project_name,
			R.user_id_fk,
			SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp,
			SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) /AGG.total_x AS prop
		FROM
			`response_mst` `R`
		INNER JOIN(
					SELECT YEARWEEK(R1.day_val, 1) AS week_val,
						R1.user_id_fk,
						SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS total_x
					FROM
						`response_mst` `R1`
					WHERE
						R1.dept_id_fk = '.$dept_opt.' AND R1.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND `R1`.`status_nm` = "Active"
					GROUP BY
						YEARWEEK(R1.day_val, 1),
						`R1`.`user_id_fk`
				) AGG
		ON AGG.week_val = YEARWEEK(R.day_val, 1) AND AGG.user_id_fk = R.user_id_fk
		INNER JOIN `activity_mst` `A` ON
			`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active"
		INNER JOIN `user_mst` `U` ON
			`U`.`user_id` = `R`.`user_id_fk`
		INNER JOIN `project_mst` `P` ON
			`P`.`project_id` = `A`.`project_id_fk`
		WHERE
			R.dept_id_fk = '.$dept_opt.' AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND `R`.`status_nm` = "Active" 
		GROUP BY
			YEARWEEK(R.day_val, 1),P.project_id,P.project_name,`R`.`user_id_fk`,AGG.total_x
	) DE GROUP BY DE.week_val;';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
			 }
		 }
		 if($file_nm=="Top_Bottom" && $action==1)
		 {
				 $sql= ' SELECT 
		DE.week_val,
		SUM(DE.equiv_xp)/SUM(DE.prop) AS Avg_val,
		ROUND(SUM(DE.prop),2) AS team_mem,
		COUNT(DISTINCT DE.user_id_fk) AS team_cont,
		MAX(DE.equiv_xp) AS v1,MIN(DE.equiv_xp) AS v2 
	FROM (		   
		SELECT
			YEARWEEK(R.day_val, 1) AS week_val,
			P.project_name,
			R.user_id_fk,
			SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp,
			SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) /AGG.total_x AS prop
		FROM
			`response_mst` `R`
		INNER JOIN(
					SELECT YEARWEEK(R1.day_val, 1) AS week_val,
						R1.user_id_fk,
						SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS total_x
					FROM
						`response_mst` `R1`
					WHERE
						R1.dept_id_fk = '.$dept_opt.' AND R1.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND `R1`.`status_nm` = "Active"
					GROUP BY
						YEARWEEK(R1.day_val, 1),
						`R1`.`user_id_fk`
				) AGG
		ON AGG.week_val = YEARWEEK(R.day_val, 1) AND AGG.user_id_fk = R.user_id_fk
		INNER JOIN `activity_mst` `A` ON
			`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active"
		INNER JOIN `user_mst` `U` ON
			`U`.`user_id` = `R`.`user_id_fk`
		INNER JOIN `project_mst` `P` ON
			`P`.`project_id` = `A`.`project_id_fk`
		WHERE
			R.dept_id_fk = '.$dept_opt.' AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND `R`.`status_nm` = "Active" 
			AND P.project_id="'.$pro_sel_val.'"  
		GROUP BY
			YEARWEEK(R.day_val, 1),P.project_id,P.project_name,`R`.`user_id_fk`,AGG.total_x
	) DE GROUP BY DE.week_val;';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
			 }
		 if($file_nm=="Top_Bottom" && $action==2)
		 {			 
				 $sql= ' SELECT YearWeek(R.day_val,1) AS week_val,P.project_id,P.project_name,R.user_id_fk,U.full_name,
		  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp 
		  FROM `response_mst` `R` 
		  INNER JOIN `activity_mst` `A` 
		  ON `R`.`activity_id_fk`=`A`.`activity_id` 
		  AND `A`.`status_nm`="Active" 
		  INNER JOIN `user_mst` `U` 
		  ON `U`.`user_id` = `R`.`user_id_fk` 
		  INNER JOIN `project_mst` `P` ON
			`P`.`project_id` = `A`.`project_id_fk` 
		   WHERE R.dept_id_fk='.$dept_opt.' AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
		   AND  `R`.`status_nm` = "Active"  AND P.project_id="'.$pro_sel_val.'"  
		   GROUP BY YearWeek(R.day_val,1),P.project_id,P.project_name,`R`.`user_id_fk`,U.full_name ;';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
			 
		 }
		 if($file_nm=="FV_Spread" && $action==2)
		 {
			  if($pro_sel_val)
			 {
				 $sql= ' SELECT YearWeek(R.day_val,1) AS week_val,P.project_id,P.project_name,R.user_id_fk,U.full_name,
		  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp 
		  FROM `response_mst` `R` 
		  INNER JOIN `activity_mst` `A` 
		  ON `R`.`activity_id_fk`=`A`.`activity_id` 
		  AND `A`.`status_nm`="Active" 
		  INNER JOIN `user_mst` `U` 
		  ON `U`.`user_id` = `R`.`user_id_fk` 
		  INNER JOIN `project_mst` `P` ON
			`P`.`project_id` = `A`.`project_id_fk` 
		   WHERE R.dept_id_fk='.$dept_opt.' AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
		   AND  `R`.`status_nm` = "Active"  AND P.project_id="'.$pro_sel_val.'"  
		   GROUP BY YearWeek(R.day_val,1),P.project_id,P.project_name,`R`.`user_id_fk`,U.full_name ;';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
			 }else{
			$sql= ' SELECT YearWeek(R.day_val,1) AS week_val,P.project_id,P.project_name,R.user_id_fk,U.full_name,
		  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp 
		  FROM `response_mst` `R` 
		  INNER JOIN `activity_mst` `A` 
		  ON `R`.`activity_id_fk`=`A`.`activity_id` 
		  AND `A`.`status_nm`="Active" 
		  INNER JOIN `user_mst` `U` 
		  ON `U`.`user_id` = `R`.`user_id_fk` 
		  INNER JOIN `project_mst` `P` ON
			`P`.`project_id` = `A`.`project_id_fk` 
		   WHERE R.dept_id_fk='.$dept_opt.' AND  R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
		   AND  `R`.`status_nm` = "Active" 
		   GROUP BY YearWeek(R.day_val,1),P.project_id,P.project_name,`R`.`user_id_fk`,U.full_name; ';
				$sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
			 }
		 }
		 if(($file_nm=="FV_Summary") && $action==1)
		 {
			$sql= ' SELECT DE.project_id,DE.project_name,
			SUM(DE.equiv_xp)/SUM(DE.prop) AS Avg_val,
		ROUND(SUM(DE.prop),2) AS team_mem,
			SUM(DE.equiv_xp) AS Sum_val,
			MAX(DE.equiv_xp) AS v1,
			MIN(DE.equiv_xp) AS v2 
			FROM 
			(
			SELECT P.project_id,P.project_name,R.user_id_fk, 
		  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp ,
		  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) /AGG.total_x AS prop
		  FROM `response_mst` `R` 
		  INNER JOIN `activity_mst` `A` 
		  ON `R`.`activity_id_fk`=`A`.`activity_id` 
		  AND `A`.`status_nm`="Active" 
		  INNER JOIN `user_mst` `U` 
		  ON `U`.`user_id` = `R`.`user_id_fk`  
		  INNER JOIN `project_mst` `P` ON
		   `P`.`project_id` = `A`.`project_id_fk` 
		  INNER JOIN(
					SELECT R1.user_id_fk,
						SUM(IFNULL(R1.final_xp,IFNULL(R1.equiv_xp, 0)) + IFNULL(R1.comp_xp, 0)) AS total_x
					FROM
						`response_mst` `R1`
					WHERE
						R1.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
						AND `R1`.`status_nm` = "Active"
					GROUP BY `R1`.`user_id_fk`
				) AGG 
			ON AGG.user_id_fk = R.user_id_fk 				
		   WHERE R.dept_id_fk='.$dept_opt.' AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
		   AND  `R`.`status_nm` = "Active" 
		   GROUP BY P.project_id,P.project_name,`R`.`user_id_fk`,AGG.total_x
		   ) DE GROUP BY DE.project_id,DE.project_name; ';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }
		 if(($file_nm=="Floor_View") && $action==1)
		 {
			// $sql= 'SELECT
        // DE.project_id,DE.project_name,
        // AVG(DE.equiv_xp) AS Avg_val,
        // MAX(DE.equiv_xp) AS v1,
		// MIN(DE.equiv_xp) AS v2 ,
		// SUM(DE.equiv_xp) AS Sum_val,
		// COUNT(DISTINCT DE.user_id_fk) AS team_mem
    // FROM (
// SELECT   CTE.project_id,CTE.project_name,
		    // R.user_id_fk,
            // SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
        // FROM
            // `response_mst` `R`  
       // INNER JOIN (SELECT project_id_fk AS project_id,project_name,user_id_fk 
		// FROM (
			// SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
				// @type := tab1.user_id_fk AS dummy
			// FROM
				// (
				// SELECT 
					// A.project_id_fk,
					// P1.project_name,
					// R.user_id_fk,
					// SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
				// FROM
					// `response_mst` `R` 
				// INNER JOIN (SELECT @num:= 0,    @type := "") B
					// ON 1=1
				// INNER JOIN `activity_mst` `A` ON
					// `R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active"  
				// INNER JOIN `project_mst` `P1` 
				// ON `P1`.`project_id`=`A`.`project_id_fk` 
				// WHERE `R`.`dept_id_fk` = '.$dept_opt.' AND	`R`.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"    
					// AND `R`.`status_nm` = "Active" 
				// GROUP BY `R`.`user_id_fk`,A.project_id_fk
				// ORDER BY `R`.`user_id_fk`,equiv_xp DESC,P1.project_name
			// ) tab1
			// GROUP BY   user_id_fk,project_id_fk,project_name,equiv_xp 
			// HAVING row_number = 1)E 
			// )CTE ON CTE.user_id_fk=R.user_id_fk 
	 // WHERE
            // R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"
			// AND `R`.`status_nm` = "Active"   
        // GROUP BY
            // CTE.project_id,CTE.project_name,`R`.`user_id_fk` )DE
    // GROUP BY DE.project_id,DE.project_name; ';
	
	$sql='SELECT
   DE.project_id,
   DE.project_name,
   AVG(DE.equiv_xp) AS Avg_val,
   MAX(DE.equiv_xp) AS v1,
   MIN(DE.equiv_xp) AS v2,
   SUM(DE.equiv_xp) AS Sum_val,
    COUNT(*) AS team_mem,
	SUM(Main.esp_wrk_hrs) AS num,
	SUM(Main.attendance_cnt) AS denom,
	IRR.in_time AS mediantime,
   ROUND(SUM(Main.esp_wrk_hrs)/SUM(Main.attendance_cnt),2)  AS workhrs 
FROM
   (
   SELECT
       CTE.project_id,
       CTE.project_name,
       R.user_id_fk,
       SUM(
           IFNULL(
               R.final_xp,
               IFNULL(R.equiv_xp, 0)
           ) + IFNULL(R.comp_xp, 0)
       ) AS equiv_xp
   FROM
       `response_mst` `R`
   INNER JOIN(
       SELECT
           project_id_fk AS project_id,
           project_name,
           user_id_fk
       FROM
           (
           SELECT
               *,
               @num := IF(
                   @type = tab1.user_id_fk,
                   @num + 1,
                   1
               ) AS row_number,
               @type := tab1.user_id_fk AS dummy
           FROM
               (
               SELECT
                   A.project_id_fk,
                   P1.project_name,
                   R.user_id_fk,
                   SUM(
                       IFNULL(
                           R.final_xp,
                           IFNULL(R.equiv_xp, 0)
                       ) + IFNULL(R.comp_xp, 0)
                   ) AS equiv_xp
               FROM
                   `response_mst` `R`
               INNER JOIN(
               SELECT
                   @num := 0,
                   @type := ""
               ) B
           ON
               1 = 1
           INNER JOIN `activity_mst` `A` ON
               `R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active"
           INNER JOIN `project_mst` `P1` ON
               `P1`.`project_id` = `A`.`project_id_fk`
           WHERE
               `R`.`dept_id_fk` = '.$dept_opt.' AND `R`.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
			   AND `R`.`status_nm` = "Active"
           GROUP BY
               `R`.`user_id_fk`,
               A.project_id_fk
           ORDER BY
               `R`.`user_id_fk`,
               equiv_xp
           DESC
               ,
               P1.project_name
           ) tab1
       GROUP BY
           user_id_fk,
           project_id_fk,
           project_name,
           equiv_xp
       HAVING
           row_number = 1
       ) E
   ) CTE
ON
   CTE.user_id_fk = R.user_id_fk
WHERE
   R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND `R`.`status_nm` = "Active"
GROUP BY
   CTE.project_id,
   CTE.project_name,
   `R`.`user_id_fk`
) DE
LEFT JOIN (SELECT
		CS.user_id,ROUND((MAX(in_time) + MIN(in_time)) / 2,2) AS in_time
	FROM
		(
		SELECT user_id,(CASE WHEN attendance_cnt=0.5 THEN 0 ELSE IFNULL(in_time,0) END) AS in_time, @i := IF(user_id = @prev_user, @i +1, 1) AS seq, @prev_user := user_id
		FROM
			in_out_upload O 
			,(SELECT @i := 1, @prev_user := "") ORE  
			
			WHERE O.date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND O.status_nm="Active" 
		 AND IFNULL(O.attendance_cnt,0.00)<>0.00 
		ORDER BY user_id,in_time
	) CS
	INNER JOIN(
		SELECT
			user_id,FLOOR((COUNT(1)+1) * 0.5) AS min_cnt,FLOOR((COUNT(1)+2) * 0.5) AS max_cnt
		FROM
			in_out_upload 
			WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND status_nm="Active" 
		 AND IFNULL(attendance_cnt,0.00)<>0.00  
		GROUP BY user_id
	) CC
	ON CS.user_id = CC.user_id AND CS.seq BETWEEN CC.min_cnt AND CC.max_cnt
	GROUP BY CS.user_id) IRR ON IRR.user_id=DE.user_id_fk
LEFT JOIN (SELECT IOR.user_id ,
									SUM(IOR.attendance_cnt) AS attendance_cnt,
									SUM(IOR.esp_wrk_hrs) AS esp_wrk_hrs 								
								FROM in_out_upload  IOR 													
									WHERE IOR.date_val BETWEEN  "'.$date_start.'" AND "'.$date_end.'"  
								 AND IFNULL(IOR.attendance_cnt,0.00)<>0.00 AND IOR.status_nm="Active" 
								 AND IOR.user_id IS NOT NULL 
								 GROUP BY IOR.user_id ) Main 
					ON Main.user_id = DE.user_id_fk 
GROUP BY
   DE.project_id,
   DE.project_name';
   
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }
		 // if(($file_nm=="FV_Summary") && $action==2)
		 // {
			// $sql= ' SELECT P.project_id,P.project_name,R.user_id_fk,U.full_name, 
		  // SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp 
		  // FROM `response_mst` `R` 
		  // INNER JOIN `activity_mst` `A` 
		  // ON `R`.`activity_id_fk`=`A`.`activity_id` 
		  // AND `A`.`status_nm`="Active" 
		  // INNER JOIN `user_mst` `U` 
		  // ON `U`.`user_id` = `R`.`user_id_fk` 
		   // INNER JOIN `project_mst` `P` ON
		   // `P`.`project_id` = `A`.`project_id_fk` 
		   // WHERE R.dept_id_fk='.$dept_opt.' AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
		   // AND  `R`.`status_nm` = "Active" 
		   // GROUP BY P.project_id,P.project_name,R.user_id_fk,U.full_name; ';
		   // $sqlQuery = $this->db->query($sql);
				// $resultSet = $sqlQuery->result_array();
		 // }
		 
		 if(($file_nm=="Floor_View" || $file_nm=="FV_Summary") && $action==2)
		 {
			$sql= ' SELECT   CTE.project_id,CTE.project_name,
		    R.user_id_fk,U.full_name,
            SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
        FROM
            `response_mst` `R`  
			INNER JOIN user_mst U 
				ON U.user_id=R.user_id_fk 
       INNER JOIN (SELECT project_id_fk AS project_id,project_name,user_id_fk 
		FROM (
			SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
				@type := tab1.user_id_fk AS dummy
			FROM
				(
				SELECT 
					A.project_id_fk,
					P1.project_name,
					R.user_id_fk,
					SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
				FROM
					`response_mst` `R` 				
				INNER JOIN (SELECT @num:= 0,    @type := "") B
					ON 1=1
				INNER JOIN `activity_mst` `A` ON
					`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active"  
				INNER JOIN `project_mst` `P1` 
				ON `P1`.`project_id`=`A`.`project_id_fk` 
				WHERE `R`.`dept_id_fk` = '.$dept_opt.' 
						AND	`R`.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"    
					AND `R`.`status_nm` = "Active" 
				GROUP BY `R`.`user_id_fk`,A.project_id_fk
				ORDER BY `R`.`user_id_fk`,equiv_xp DESC,P1.project_name
			) tab1
			GROUP BY   user_id_fk,project_id_fk,project_name,equiv_xp 
			HAVING row_number = 1)E 
			)CTE ON CTE.user_id_fk=R.user_id_fk 
	 WHERE
            R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"
			AND `R`.`status_nm` = "Active"   
        GROUP BY CTE.project_id,CTE.project_name,`R`.`user_id_fk`,U.full_name ;';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
		 
	 }
	 
	 public function fv_summ_top_b($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,$action,$dept_opt,$lvl)
	 {
		 if($action==1)
		 {
			if($file_nm=="Top_Bottom" && $lvl==0)
			{
				 $sql= ' SELECT 
						DE.week_val,
						MAX(DE.equiv_xp) AS v1,MIN(DE.equiv_xp) AS v2 
				FROM (		   
					SELECT
						YEARWEEK(R.day_val, 1) AS week_val,
						R.user_id_fk,
						SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
				FROM
					`response_mst` `R` 				
				INNER JOIN `activity_mst` `A` ON
					`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active"
				INNER JOIN `user_mst` `U` ON
					`U`.`user_id` = `R`.`user_id_fk`
				INNER JOIN `project_mst` `P` ON
					`P`.`project_id` = `A`.`project_id_fk`
				WHERE
					P.project_id="'.$pro_sel_val.'"  AND 
					R.dept_id_fk = '.$dept_opt.' 
					AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
					AND `R`.`status_nm` = "Active" 
				GROUP BY
					YEARWEEK(R.day_val, 1),P.project_id,P.project_name,`R`.`user_id_fk`
			) DE GROUP BY DE.week_val;';
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
			 }
			 if($file_nm=="Top_Bottom" && $lvl==1)
			{
				 $sql= "SELECT
        DE.week_val,
        MAX(DE.equiv_xp) AS v1,
		MIN(DE.equiv_xp) AS v2 
    FROM (
SELECT 
		YEARWEEK(R.day_val, 1) AS week_val,
            R.user_id_fk,
            SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
        FROM
            `response_mst` `R`  
       INNER JOIN (SELECT week_v,project_id_fk AS project_id,project_name,user_id_fk 
		FROM (
			SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
				@type := tab1.user_id_fk AS dummy
			FROM
				(
				SELECT YEARWEEK(R.day_val, 1) AS week_v,
					A.project_id_fk,
					P1.project_name,
					R.user_id_fk,
					SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
				FROM
					`response_mst` `R` 
				INNER JOIN (SELECT @num := 0,    @type := '') B
					ON 1=1 
				INNER JOIN `user_mst` `U` 
						  ON `U`.`user_id` = `R`.`user_id_fk` 
				INNER JOIN `activity_mst` `A` ON
					`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 
				INNER JOIN `project_mst` `P1` 
				ON `P1`.`project_id`=`A`.`project_id_fk` 
				WHERE
					R.day_val BETWEEN '".$date_start."' AND '".$date_end."'  
					AND  R.dept_id_fk ='".$dept_opt."'  
					AND `R`.`status_nm` = 'Active'
				GROUP BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`,A.project_id_fk
				ORDER BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`,equiv_xp DESC,P1.project_name
			) tab1
			GROUP BY    week_v,user_id_fk,project_id_fk,project_name,equiv_xp 
			HAVING row_number = 1)E 
			WHERE E.project_id_Fk='".$pro_sel_val."' 
			)CTE ON CTE.user_id_fk=R.user_id_fk AND CTE.week_v=YEARWEEK(R.day_val, 1)
	 WHERE
            R.dept_id_fk ='".$dept_opt."'  
			AND R.day_val BETWEEN '".$date_start."' AND '".$date_end."' 
			AND `R`.`status_nm` = 'Active'  
        GROUP BY
            YEARWEEK(R.day_val, 1),`R`.`user_id_fk`
	)DE GROUP BY DE.week_val;";
		   $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
			 }
		 }
		 if($action==2)
		 {
			 if($file_nm=="Top_Bottom" && $lvl==0)
			 {			 
					 $sql= ' SELECT YearWeek(R.day_val,1) AS week_val,
								P.project_id,P.project_name,
									R.user_id_fk,U.full_name,
						  SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp 
						  FROM `response_mst` `R` 
						  INNER JOIN `activity_mst` `A` 
						  ON `R`.`activity_id_fk`=`A`.`activity_id` 
						  AND `A`.`status_nm`="Active" 
						  INNER JOIN `user_mst` `U` 
						  ON `U`.`user_id` = `R`.`user_id_fk` 
						  INNER JOIN `project_mst` `P` ON
							`P`.`project_id` = `A`.`project_id_fk` 
						   WHERE R.dept_id_fk='.$dept_opt.' 
						   AND R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
						   AND  `R`.`status_nm` = "Active"  AND P.project_id="'.$pro_sel_val.'"  
						   GROUP BY YearWeek(R.day_val,1),P.project_id,P.project_name,`R`.`user_id_fk`,U.full_name ;';
			   $sqlQuery = $this->db->query($sql);
					$resultSet = $sqlQuery->result_array();			 
			 }
			if($file_nm=="Top_Bottom" && $lvl==1)
			 {			 
					 $sql= " SELECT 
		YEARWEEK(R.day_val, 1) AS week_val,
            R.user_id_fk,
            SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
        FROM
            `response_mst` `R`  
       INNER JOIN (SELECT week_v,project_id_fk AS project_id,project_name,user_id_fk 
		FROM (
			SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
				@type := tab1.user_id_fk AS dummy
			FROM
				(
				SELECT YEARWEEK(R.day_val, 1) AS week_v,
					A.project_id_fk,
					P1.project_name,
					R.user_id_fk,
					SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
				FROM
					`response_mst` `R` 
				INNER JOIN (SELECT @num := 0,    @type := '') B
					ON 1=1 
				INNER JOIN `user_mst` `U` 
						  ON `U`.`user_id` = `R`.`user_id_fk` 
				INNER JOIN `activity_mst` `A` ON
					`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 
				INNER JOIN `project_mst` `P1` 
				ON `P1`.`project_id`=`A`.`project_id_fk` 
				WHERE
					R.day_val BETWEEN '".$date_start."' AND '".$date_end."'  
					AND  R.dept_id_fk ='".$dept_opt."'  
					AND `R`.`status_nm` = 'Active'
				GROUP BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`,A.project_id_fk
				ORDER BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk`,equiv_xp DESC,P1.project_name
			) tab1
			GROUP BY    week_v,user_id_fk,project_id_fk,project_name,equiv_xp 
			HAVING row_number = 1)E 
			WHERE E.project_id_Fk='".$pro_sel_val."' 
			)CTE ON CTE.user_id_fk=R.user_id_fk AND CTE.week_v=YEARWEEK(R.day_val, 1)
	 WHERE
            R.dept_id_fk ='".$dept_opt."'  
			AND R.day_val BETWEEN '".$date_start."' AND '".$date_end."' 
			AND `R`.`status_nm` = 'Active'  
        GROUP BY YEARWEEK(R.day_val, 1),`R`.`user_id_fk` ;";
			   $sqlQuery = $this->db->query($sql);
					$resultSet = $sqlQuery->result_array();			 
			 }
		 }		 
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  }		 
	 }
	 
	public function select_cal_view($date_start,$date_end,$u_id){		
		$this->db->select('  R.user_id_fk,R.response_id,IFNULL(R.audit_work,R.work_comp) AS work_comp,
				ROUND(IFNULL(R.equiv_xp,0),2) AS equiv_xp,
				ROUND(IFNULL(R.comp_xp,0),2) AS comp_xp,
				ROUND(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0))),2) AS tot_xp,
				A.activity_name,R.audit_desc,
				CASE WHEN R.tast_stat="Audit Pending" THEN "t_stat_1"
					WHEN R.tast_stat="Review Pending" THEN "t_stat_2"
					WHEN R.tast_stat="Confirmation Pending" THEN "t_stat_3"
					WHEN R.tast_stat="Confirmed" THEN "t_stat_4" END AS badge_col,
				R.std_tgt_val,R.day_val ,R.tast_stat AS stat_nm,
				CASE WHEN R.tast_stat="Audit Pending" THEN "t_col_1"
					WHEN R.tast_stat="Review Pending" THEN "t_col_2"
					WHEN R.tast_stat="Confirmation Pending" THEN "t_col_3" 
					WHEN R.tast_stat="Confirmed" THEN "t_col_4" END AS tast_stat,
				CASE WHEN R.issue_flag=1 THEN "Y" ELSE "N" END AS issue_flag ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('user_mst U', 'U.user_id = R.user_id_fk','inner');
						$this->db->where('R.user_id_fk', $u_id);	
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');	
						 
						 $sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
		 
	 }
	 
	 public function pa_cal_view($date_start,$date_end,$u_id,$pro){		
		$sql='  SELECT DISTINCT R.day_val,R.user_id_fk,U.full_name  
 FROM response_mst R 
 INNER JOIN activity_mst A 
 ON R.activity_id_fk=A.activity_id 
 INNER JOIN user_mst U 
 ON U.user_id=R.user_id_fk AND U.status_nm="Active" 
 WHERE R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND `R`.`status_nm` = "Active" 
 AND A.project_id_fk="'.$pro.'" ';	
						 
						 $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
		 
	 }
	 
	 public function select_grade_dt($date_start, $date_end, $u_id)
	 {
		// $sql='  SELECT
				// U.user_id,U.full_name,FG.value,COUNT(UF.id) AS cnt
			// FROM
				// user_mst U  
				// JOIN feedback_grades FG 
				// ON 1=1 
				// LEFT JOIN `user_feedbacks` UF
				// ON U.user_id = UF.user_id_fk 
				// AND UF.grade_id_fk=FG.id 
				// AND UF.status_nm = "Active"  
				// AND UF.fb_given_date BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
				// WHERE U.user_id IN ('.$u_id.') 
				// GROUP BY U.user_id,U.full_name,FG.value 
				// ORDER BY FG.grade;	 ';	
				
		$sql='SELECT
				YEARWEEK(UF.fb_given_date, 1) AS week_val,U.user_id,U.full_name,AVG(FG.grade) AS fg_val
			FROM
				user_mst U  
				JOIN feedback_grades FG 
				ON 1=1 
				JOIN `user_feedbacks` UF
				ON U.user_id = UF.user_id_fk 
				AND UF.grade_id_fk=FG.id 
				AND UF.status_nm = "Active"  
				AND UF.fb_given_date BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
				WHERE U.user_id IN ('.$u_id.') 
				GROUP BY YEARWEEK(UF.fb_given_date, 1),U.user_id,U.full_name;';
						 
						 $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
		 
	 }
	 
	 
	 public function select_io_tbl($file_nm,$pro_sel,$date_start,$date_end,$lvl,$emp,$dept_opt){
		if($file_nm=="Emp_io_upload")
		 {
			 if($lvl==1)
			 {
			 
				$this->db->select(' UNIX_TIMESTAMP(CONVERT_TZ(I.date_val,"+00:00",'.$this->tx_cn1.')) AS date_val,I.out_time,I.esp_status,I.in_time,I.esp_wrk_hrs,I.sapience_on,I.sapience_off,I.total_days,
				I.attendance_cnt,U.full_name ',FALSE);
				$this->db->from('in_out_upload I');
				$this->db->join('(SELECT DISTINCT user_id_fk from response_mst R1 
				JOIN activity_mst A 
				ON R1.activity_id_fk=A.activity_id AND A.status_nm="Active"
				WHERE R1.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
				AND R1.status_nm="Active" AND R1.dept_id_fk='.$dept_opt.' 
				AND A.project_id_fk IN ('. $pro_sel.'))R','I.user_id=R.user_id_fk ','inner');						
				$this->db->join('user_mst U', 'U.user_id = R.user_id_fk','inner');
				$this->db->where('I.date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" ', null, false);
				$this->db->where('I.status_nm', 'Active');
				 $this->db->order_by("U.full_name,I.date_val",false); 
		 
			$sqlQuery = $this->db->get();
			$resultSet = $sqlQuery->result_array();
		 }else{
			 $this->db->select(' UNIX_TIMESTAMP(CONVERT_TZ(I.date_val,"+00:00",'.$this->tx_cn1.'))  AS date_val,I.out_time,I.esp_status,I.in_time,I.esp_wrk_hrs,I.sapience_on,I.sapience_off,I.total_days,I.attendance_cnt,U.full_name ',FALSE);
				$this->db->from('in_out_upload I');									
				$this->db->join('user_mst U', 'U.user_id = I.user_id','inner');
				$this->db->where('I.date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" ', null, false);
				$this->db->where('I.user_id', $pro_sel, false);	
				$this->db->where('I.status_nm', 'Active');				
				 $this->db->order_by("U.full_name,I.date_val",false); 
		 
			$sqlQuery = $this->db->get();
			$resultSet = $sqlQuery->result_array();
			 
		 }
	  if($resultSet)
	  {
		  return json_encode($resultSet);
	  }
	  else{
		return json_encode (json_decode ("{}"));;
	  } 
	 }
	 }
	 
	  public function pt_buddy_reports($file_nm,$date_s,$date_e,$role,$dept_opt,$pro_j){
		if($file_nm=="PT_Report_Card")
		 {
			 $sql="SELECT 1 AS user_id_fk,
			 'ZZZZZZZ' AS dis_nm,
				0 AS in_time,
			 ROUND(AVG(I.esp_wrk_hrs),2) AS esp_wrk_hrs,
			 ROUND(AVG(I.attendance_cnt),2) AS attendance_cnt ,
			 0 AS total_days,
			 ROUND(AVG(I.sapience_on),2) AS sapience_on,
			 0 AS sapience_off,
			 CONCAT(DE.project_name,' Buddy') AS full_name,
			 DE.project_id,
			 DE.project_name AS val_name,
			 0 AS grade,
			 '' AS grade_remarks,
			 0 AS w_done,
			 ROUND(AVG(SM.act_xp),2) AS totl_xp, 
			 ROUND(AVG(SM.proj_xp),2) AS proj_xp,
			 ROUND(AVG(SM.act_xp),2) AS act_xp ,	
	AVG((SM.act_xp*10)/I.attendance_cnt) AS cu ,
	NULL AS quality_score
FROM 
	( SELECT 
            R.user_id_fk,
            SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS proj_xp,			
			SUM(CASE WHEN R.tast_stat='Confirmed'  THEN IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0) ELSE 0 END) AS act_xp
        FROM
            `response_mst` `R`  
	 WHERE
            R.day_val BETWEEN '".$date_s."' AND '".$date_e."'  
			AND `R`.`status_nm` = 'Active'  
        GROUP BY `R`.`user_id_fk` 
	)SM 
	INNER JOIN (
			SELECT project_id_fk AS project_id,project_name,user_id_fk AS user_id
			FROM (
				SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
					@type := tab1.user_id_fk AS dummy
				FROM
					(
					SELECT 
						A.project_id_fk,
						P1.project_name,
						R.user_id_fk,
						SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
					FROM
						`response_mst` `R` 
					INNER JOIN (SELECT @num := 0,    @type := '') B
						ON 1=1
					INNER JOIN `activity_mst` `A` ON
						`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active' 
					INNER JOIN `project_mst` `P1` 
					ON `P1`.`project_id`=`A`.`project_id_fk` 
					WHERE
						R.day_val BETWEEN '".$date_s."' AND '".$date_e."'  
						AND `R`.`status_nm` = 'Active'
					GROUP BY `R`.`user_id_fk`,A.project_id_fk
					ORDER BY `R`.`user_id_fk`,equiv_xp DESC,P1.project_name
				) tab1
				GROUP BY    user_id_fk,project_id_fk,project_name,equiv_xp 
				HAVING row_number = 1)E 
				WHERE E.project_id_Fk IN (".$pro_j.")
	)DE ON DE.user_id=SM.user_id_fk 
	LEFT JOIN (SELECT user_id,
					SUM(IFNULL(esp_wrk_hrs,0.00))/SUM(attendance_cnt) AS esp_wrk_hrs , 
					AVG(IFNULL(sapience_on,0.00)) AS sapience_on, 
					AVG(IFNULL(sapience_off,0.00)) AS sapience_off,
					SUM(total_days) AS total_days,
					SUM(attendance_cnt) AS attendance_cnt 
					FROM in_out_upload 
					WHERE date_val BETWEEN '".$date_s."' AND '".$date_e."'  
						 AND IFNULL(attendance_cnt,0.00)<>0.00 
						 AND status_nm='Active'  
					GROUP BY user_id) I 
	ON I.user_id=DE.user_id 
	GROUP BY DE.project_id,DE.project_name;";
		 }
		  $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
				
	  if($resultSet)
	  {
		  return ($resultSet);
	  }
	  else{
		return false;
	  } 
	 }
	 
	  public function pt_select_mem_reports($file_nm,$pids,$date_start,$date_end,$lvl,$emp,$role,$dept_opt){
		if($file_nm=="PT_Report_Card")
		 {
			 //echo "sfdl";
			 $denom='I.attendance_cnt';
			 $gradeColumns = '';
			 $add_th='IRR.in_time';
			 if($lvl==0)
			 {
				$selec_val='W.wu_name,A.activity_name';
			 }
			 if($lvl==1)
			 {
				$selec_val='P.project_id,P.project_name';
				$gradeColumns = ' FG.value AS grade, UF.rem AS grade_remarks,';
				$gradeColumns = 'null AS grade, null AS grade_remarks,';
				//$add_th='IRR.in_time';
			 }
			 if($lvl==2)
			 {
				$selec_val='T.task_name';
			 }
			 if($lvl==3)
			 {
				$selec_val='ST.sub_name';
			 }
			 if($lvl==4)
			 {
				$selec_val='J.job_name';
			 }
			 if($lvl==5)
			 {
				$selec_val='CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))';
			 }
			 if($lvl==6)
			 {
				$selec_val='DATE_FORMAT(R.day_val, "%d/%m/%y")';
				$add_th='I.in_time';
				$denom='1';
			 }
			  if($lvl==7)
			 {
				$selec_val='W.wu_name,A.activity_name,DATE_FORMAT(R.day_val, "%d/%m/%y")';
			 }
			 
			  if($lvl==8)
			 {
				$selec_val='R.Milestone_id';
			 }
		 $this->db->select('  R.user_id_fk, U.full_name AS dis_nm,'.$add_th.',
		 ROUND(I.esp_wrk_hrs/'.$denom.',2) AS esp_wrk_hrs  ,
		 I.attendance_cnt,I.total_days,ROUND(I.sapience_on,2) AS sapience_on,ROUND(I.sapience_off,2) AS sapience_off,U.full_name,'.$selec_val.' AS val_name,'.$gradeColumns.'
		 IFNULL(SUM(IFNULL(R.audit_work, R.work_comp)),0) AS w_done,
		 ROUND(MAX(RTO.tot_xp),2) AS totl_xp,
		 ROUND(IFNULL(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),0),2) AS proj_xp, 
		ROUND(IFNULL(SUM(CASE WHEN R.tast_stat="Confirmed"  THEN (IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))) ELSE 0 END ),0),2) AS act_xp,
		ROUND((MAX(RTO.tot_xp))*10/I.attendance_cnt,2) AS cu ,CEIL(AVG(NULLIF(R.review_xp,0))) AS quality_score ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('work_unit_mst W', 'W.wu_id = A.wu_id_fk AND W.status_nm="Active" ','inner');
						$this->db->join('user_mst U', 'U.user_id = R.user_id_fk','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						$this->db->join(' (SELECT user_id_fk,
										SUM(CASE WHEN tast_stat="Confirmed" THEN IFNULL(final_xp,IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0) ELSE 0 END ) AS tot_xp
									FROM response_mst WHERE day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
									AND status_nm="Active"   GROUP BY user_id_fk)RTO','RTO.user_id_fk=R.user_id_fk','left');
						if ($lvl == 1) {
							
							// $this->db->join('(SELECT
										// project_id_fk,
										// user_id_fk,
										// CEIL(AVG(`grade_id_fk`)) AS grade,
										// GROUP_CONCAT(DISTINCT IFNULL(`remarks`,"")) AS rem
									// FROM
										// user_feedbacks 
									// WHERE
										// fb_given_date >= "'.$date_start.'" AND fb_given_date <= "'.$date_end.'" AND status_nm = "Active" 
										// GROUP BY project_id_fk,user_id_fk )UF', 'P.project_id = UF.project_id_fk AND U.user_id = UF.user_id_fk ', 'left');
									
							//$this->db->join('feedback_grades FG', 'UF.grade = FG.id', 'left');
						}
						 if($lvl==6)
						 {
							$this->db->join(' (SELECT user_id,date_val,in_time, esp_wrk_hrs , sapience_on,sapience_off,attendance_cnt,total_days  FROM in_out_upload WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND status_nm="Active" AND user_id IN ('.$emp.') ) I ','I.user_id=R.user_id_fk AND I.date_val=R.day_val','left');
						 }else{
							$this->db->join(' (SELECT user_id,
													SUM(IFNULL(esp_wrk_hrs,0.00)) AS esp_wrk_hrs , 
													AVG(IFNULL(sapience_on,0.00)) AS sapience_on, 
													AVG(IFNULL(sapience_off,0.00)) AS sapience_off,
													SUM(total_days) AS total_days,
													SUM(attendance_cnt) AS attendance_cnt 
													FROM in_out_upload 
													WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
														 AND IFNULL(attendance_cnt,0.00)<>0.00 AND user_id IN ('.$emp.') 
														 AND status_nm="Active" 
													group by user_id ) I ','I.user_id=R.user_id_fk','left');
													
							$this->db->join(' (SELECT
														CS.user_id,ROUND((MAX(in_time) + MIN(in_time)) / 2,2) AS in_time
													FROM
														(
														SELECT user_id,(CASE WHEN attendance_cnt=0.5 THEN 0 ELSE IFNULL(in_time,0) END) AS in_time, @i := IF(user_id = @prev_user, @i +1, 1) AS seq, @prev_user := user_id
														FROM
															in_out_upload O 
															,(SELECT @i := 1, @prev_user := "") ORE  
															
															WHERE O.date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND O.status_nm="Active" 
														 AND IFNULL(O.attendance_cnt,0.00)<>0.00 AND O.user_id IN ('.$emp.')
														ORDER BY user_id,in_time
													) CS
													INNER JOIN(
														SELECT
															user_id,FLOOR((COUNT(1)+1) * 0.5) AS min_cnt,FLOOR((COUNT(1)+2) * 0.5) AS max_cnt
														FROM
															in_out_upload 
															WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND status_nm="Active" 
														 AND IFNULL(attendance_cnt,0.00)<>0.00   
														 AND user_id IN ('.$emp.')
														GROUP BY user_id
													) CC
													ON CS.user_id = CC.user_id AND CS.seq BETWEEN CC.min_cnt AND CC.max_cnt
													GROUP BY CS.user_id) IRR ','IRR.user_id=R.user_id_fk','left');
						 }
						$this->db->where_in('R.dept_id_fk ', $dept_opt);
						if($lvl==2)
						{
						$this->db->join('task_mst T', 'T.task_id = A.task_id_fk AND T.status_nm="Active" ','inner');
						}
						if($lvl==3 || $lvl==5)
						{
						$this->db->join('sub_mst ST', 'ST.sub_id = A.sub_id_fk AND ST.status_nm="Active" ','inner');
						}
						if($lvl==4  || $lvl==5)
						{
						$this->db->join('job_mst J', 'J.job_id = A.job_id_fk AND J.status_nm="Active" ','inner');
						}
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');		
						 $this->db->where('R.user_id_fk IN ('.$emp.')',null,false);
						 
						 
						if($role==4 && $pids){
						  $this->db->where('P.project_id IN ('.$pids.')',null,false);
						}
						 $this->db->group_by("R.user_id_fk,U.full_name,".$selec_val." ",false);
						 $this->db->order_by("U.full_name,".$selec_val, 'ASC',false); 
						   $sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
						
		 }
		 if($file_nm=="PT_Feedback")
		 {
			 //echo "sfdl";
			 $pi='';
			 if($role==4 && $pids){
						$pi=' AND P.project_id IN ('.$pids.')';
						}
			$sql='SELECT DATE_FORMAT(UF.fb_given_date, "%d-%b-%Y") AS fb_given_date,
				YEARWEEK(UF.fb_given_date, 1) AS week_val,U.user_id,U.full_name,FG.grade,
				UF.remarks,
				P.project_id,P.project_name,
				UFI.full_name AS f_full_name 
			FROM
				user_mst U  
				JOIN feedback_grades FG 
				ON 1=1 
				JOIN `user_feedbacks` UF 
				ON U.user_id = UF.user_id_fk 
				AND UF.grade_id_fk=FG.id 
				AND UF.status_nm = "Active"  
				AND UF.fb_given_date BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
				JOIN project_mst P 
				ON P.project_id=UF.project_id_fk 
				LEFT JOIN user_mst UFI 
				ON UFI.user_id=IFNULL(UF.upd_user,UF.ins_user) 
				WHERE U.user_id IN ('.$emp.') '.$pi.' '; 
				  $sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
		 }
		
				
	  if($resultSet)
	  {
		  return ($resultSet);
	  }
	  else{
		return false;
	  } 
	 }
	 
	 public function select_mem_reports($file_nm,$pro_sel,$date_start,$date_end,$lvl,$emp,$dept_opt){
		 
		if($file_nm=="Team_Member_Info" || $file_nm=="Employee_Info")
		 {
			 $userId=$this->session->userdata('user_id');
			 
		 $sql='SELECT user_id_fk FROM user_permissions WHERE user_id_fk='.$userId.' AND is_wpr=1 AND status_nm="Active" AND dept_id_fk IN ('.$dept_opt.');';
		 $query = $this->db->query($sql);
		
		 
			 $gradeColumns = '';
			 $add_th='IRR.in_time';
			 $denom='I.attendance_cnt';
			 if($lvl==0)
			 {
				$selec_val='W.wu_name,A.activity_name';
			 }
			 if($lvl==1)
			 {
				$selec_val='P.project_name';
				$gradeColumns = ' UF.f_full_name,FG.value AS grade, UF.rem AS grade_remarks,';
			 }
			 if($lvl==2)
			 {
				$selec_val='T.task_name';
			 }
			 if($lvl==3)
			 {
				$selec_val='ST.sub_name';
			 }
			 if($lvl==4)
			 {
				$selec_val='J.job_name';
			 }
			 if($lvl==5)
			 {
				$selec_val='CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))';
			 }
			 if($lvl==6)
			 {
				$selec_val='DATE_FORMAT(R.day_val, "%d/%m/%y")';
				$add_th='I.in_time';
				$denom='1';
			 }
			  if($lvl==7)
			 {
				$selec_val='W.wu_name,A.activity_name,DATE_FORMAT(R.day_val, "%d/%m/%y")';
			 }
			 
			  if($lvl==8)
			 {
				$selec_val='R.Milestone_id';
			 }
		 $this->db->select('  R.user_id_fk,'.$add_th.',ROUND(I.esp_wrk_hrs/'.$denom.',2) AS esp_wrk_hrs  ,I.attendance_cnt,I.total_days,ROUND(I.sapience_on,2) AS sapience_on,ROUND(I.sapience_off,2) AS sapience_off,U.full_name,'.$selec_val.' AS val_name,'.$gradeColumns.'
		 IFNULL(SUM(IFNULL(R.audit_work, R.work_comp)),0) AS w_done,
		 ROUND(IFNULL(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)),0),2) AS proj_xp, 
		ROUND(IFNULL(SUM(CASE WHEN R.tast_stat="Confirmed"  THEN (IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) ELSE 0 END ),0),2) AS act_xp,
		ROUND(MAX(RTO.pro_tot_xp),2) AS tot_proj_xp,
		ROUND(MAX(RTO.tot_xp),2) AS tot_act_xp,		
		ROUND((MAX(RTO.pro_tot_xp))*10/I.attendance_cnt,2) AS cu	,
		ROUND((MAX(RTO.tot_xp))*10/I.attendance_cnt,2) AS ccu		',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('work_unit_mst W', 'W.wu_id = A.wu_id_fk AND W.status_nm="Active" ','inner');
						$this->db->join('user_mst U', 'U.user_id = R.user_id_fk','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						$this->db->join(' (SELECT user_id_fk,
										SUM(CASE WHEN tast_stat="Confirmed" THEN IFNULL(final_xp,IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0) ELSE 0 END ) AS tot_xp,
										SUM(IFNULL(final_xp,IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0)) AS pro_tot_xp
									FROM response_mst WHERE day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
									AND status_nm="Active"   GROUP BY user_id_fk)RTO','RTO.user_id_fk=R.user_id_fk','left');
									
						if ($lvl == 1) {
							if($emp)
						{
								$this->db->join('(SELECT
												FD.project_id_fk,
												FD.user_id_fk,
												CEIL(AVG(FD.`grade_id_fk`)) AS grade,
												GROUP_CONCAT(DISTINCT IFNULL(FD.`remarks`,"")) AS rem
												,GROUP_CONCAT(DISTINCT IFNULL(UFI.full_name,"")) AS f_full_name 
											FROM
												user_feedbacks FD 
												LEFT JOIN user_mst UFI 
												ON UFI.user_id=IFNULL(FD.upd_user,FD.ins_user) 
											WHERE
												FD.fb_given_date >= "'.$date_start.'" AND FD.fb_given_date <= "'.$date_end.'" AND FD.status_nm = "Active"
												GROUP BY FD.project_id_fk,FD.user_id_fk )UF', 'P.project_id = UF.project_id_fk AND U.user_id = UF.user_id_fk ', 'left');
						}else{
							$fil_p='';
							if($pro_sel)
							{
								$fil_p='AND FD.project_id_fk='.$pro_sel.' ';						
							}
							$this->db->join('(SELECT
										FD.project_id_fk,
										FD.user_id_fk,
										CEIL(AVG(FD.`grade_id_fk`)) AS grade,
										GROUP_CONCAT(DISTINCT IFNULL(FD.`remarks`,"")) AS rem
										,GROUP_CONCAT(DISTINCT IFNULL(UFI.full_name,"")) AS f_full_name 
									FROM
										user_feedbacks FD 
												LEFT JOIN user_mst UFI 
												ON UFI.user_id=IFNULL(FD.upd_user,FD.ins_user) 
									WHERE
										FD.fb_given_date >= "'.$date_start.'" AND FD.fb_given_date <= "'.$date_end.'" AND FD.status_nm = "Active" '.$fil_p.' 
										GROUP BY FD.project_id_fk,FD.user_id_fk )UF', 'P.project_id = UF.project_id_fk AND U.user_id = UF.user_id_fk ', 'left');
						}
						
						
							$this->db->join('feedback_grades FG', 'UF.grade = FG.id', 'left');
						}
						 if($lvl==6)
						 {
							$this->db->join(' (SELECT user_id,date_val,in_time, esp_wrk_hrs , sapience_on,sapience_off,attendance_cnt,total_days  from in_out_upload WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND status_nm="Active"  ) I ','I.user_id=R.user_id_fk AND I.date_val=R.day_val','left');
						 }else{
							$this->db->join(' (SELECT user_id,
													SUM(IFNULL(esp_wrk_hrs,0.00)) AS esp_wrk_hrs , 
													AVG(IFNULL(sapience_on,0.00)) AS sapience_on, 
													AVG(IFNULL(sapience_off,0.00)) AS sapience_off,
													SUM(total_days) AS total_days,
													SUM(attendance_cnt) AS attendance_cnt 
													FROM in_out_upload 
													WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
														 AND IFNULL(attendance_cnt,0.00)<>0.00 AND status_nm="Active" 
													group by user_id ) I ','I.user_id=R.user_id_fk','left');
													
							$this->db->join(' (SELECT
														CS.user_id,ROUND((MAX(in_time) + MIN(in_time)) / 2,2) AS in_time
													FROM
														(
														SELECT user_id,(CASE WHEN attendance_cnt=0.5 THEN 0 ELSE IFNULL(in_time,0) END) AS in_time, @i := IF(user_id = @prev_user, @i +1, 1) AS seq, @prev_user := user_id
														FROM
															in_out_upload O 
															,(SELECT @i := 1, @prev_user := "") ORE  
															
															WHERE O.date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
														 AND IFNULL(O.attendance_cnt,0.00)<>0.00 AND O.status_nm="Active" 
														ORDER BY user_id,in_time
													) CS
													INNER JOIN(
														SELECT
															user_id,FLOOR((COUNT(1)+1) * 0.5) AS min_cnt,FLOOR((COUNT(1)+2) * 0.5) AS max_cnt
														FROM
															in_out_upload 
															WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
														 AND IFNULL(attendance_cnt,0.00)<>0.00 	AND status_nm="Active" 								 
														GROUP BY user_id
													) CC
													ON CS.user_id = CC.user_id AND CS.seq BETWEEN CC.min_cnt AND CC.max_cnt
													GROUP BY CS.user_id) IRR ','IRR.user_id=R.user_id_fk','left');
						 }
						 
						 if($query->num_rows() > 0)
						 {
						 
						 $this->db->join('(
						SELECT R3.response_id FROM response_mst R3 
						INNER JOIN activity_mst A 
						ON R3.activity_id_fk=A.activity_id AND A.status_nm="Active"
						INNER JOIN(
									SELECT HP.level_id_fk,
									  HP.mid_code,
									  (CASE WHEN HP.level_id_fk=1 THEN 3 ELSE 1 END) AS l_start,
										(CASE WHEN HP.level_id_fk=1 THEN 4 ELSE L.length_limit END) AS length_limit,
										L.level_code,
										HP.project_id_fk ,
										HP.dept_id_fk 
									FROM
									  user_permissions HP
									LEFT JOIN level_mst L ON
									  L.level_id = HP.level_id_fk
									WHERE
									  HP.user_id_fk ='.$userId.' AND HP.status_nm = "Active"   AND IFNULL(HP.is_wpr,0)=1 
									) P
									ON R3.dept_id_fk=P.dept_id_fk  AND 
									(
										(IFNULL(P.mid_code, "") = "") 
										OR(FIND_IN_SET(SUBSTRING(R3.milestone_id, P.l_start, P.length_limit),P.mid_code)>0)
									) AND (A.project_id_fk=P.project_id_fk OR IFNULL(P.project_id_fk,0)=0 )
								WHERE R3.dept_id_fk IN ('. $dept_opt.') AND 
							R3.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND R3.status_nm = "Active" 
						)UP','R.response_id=UP.response_id','inner');
						 }

						$this->db->where('R.dept_id_fk', $dept_opt);
						if($lvl==2)
						{
						$this->db->join('task_mst T', 'T.task_id = A.task_id_fk AND T.status_nm="Active" ','inner');
						}
						if($lvl==3 || $lvl==5)
						{
						$this->db->join('sub_mst ST', 'ST.sub_id = A.sub_id_fk AND ST.status_nm="Active" ','inner');
						}
						if($lvl==4  || $lvl==5)
						{
						$this->db->join('job_mst J', 'J.job_id = A.job_id_fk AND J.status_nm="Active" ','inner');
						}
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');
						if($emp)
						{
							$this->db->where_in('R.user_id_fk', $emp);
						}else{
							if($pro_sel)
							{
						  $this->db->where_in('P.project_id', $pro_sel);
							}
						}
						 $this->db->group_by("R.user_id_fk,U.full_name,".$selec_val." ",false);
						 $this->db->order_by("U.full_name,".$selec_val, 'ASC',false); 
						 	$sqlQuery = $this->db->get();
		 }
		 
		 if($file_nm=="FV_Member_Info")
		 {			 
	 $denom='I.attendance_cnt';
			 if($lvl==2)
						{							
							if($pro_sel)
							{
							$sql='SELECT U.full_name, IRR.in_time, ROUND(I.esp_wrk_hrs/'.$denom.', 2) AS esp_wrk_hrs, 
					I.attendance_cnt, I.total_days, ROUND(I.sapience_on, 2) AS sapience_on, 
					CTE.project_name,
					ROUND(I.sapience_off, 2) AS sapience_off, 
					ROUND(RTO.tot_xp, 2) AS proj_xp, 
					ROUND(RTO.tot_act_xp, 2) AS act_xp, 
					 ROUND((MAX(RTO.tot_xp))*10/I.attendance_cnt, 2) AS cu,
					 ROUND((MAX(RTO.tot_act_xp))*10/I.attendance_cnt, 2) AS ccu
					FROM `user_mst` `U` 
					 INNER JOIN (SELECT project_id_fk AS project_id,project_name,user_id_fk 
							FROM (
								SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
									@type := tab1.user_id_fk AS dummy
								FROM
									(
									SELECT 
										A.project_id_fk,
										P1.project_name,
										R.user_id_fk,
										SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
									FROM
										`response_mst` `R` 
									INNER JOIN (SELECT @num:= 0,    @type := "") B
										ON 1=1
									INNER JOIN `activity_mst` `A` ON
										`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active" 
									INNER JOIN `project_mst` `P1` 
									ON `P1`.`project_id`=`A`.`project_id_fk` 
									WHERE `R`.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
										AND `R`.`status_nm` = "Active" 
									GROUP BY `R`.`user_id_fk`,A.project_id_fk
									ORDER BY `R`.`user_id_fk`,equiv_xp DESC,P1.project_name
								) tab1
								GROUP BY    user_id_fk,project_id_fk,project_name,equiv_xp 
								HAVING row_number = 1)E 			
								WHERE E.project_id_fk="'.$pro_sel.'"  
								)CTE 
					ON CTE.user_id_fk=U.user_id  
					LEFT JOIN  (SELECT user_id_fk,SUM(IFNULL(final_xp,IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0)) AS tot_xp,
								SUM(CASE WHEN tast_stat="Confirmed"  THEN IFNULL(final_xp,IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0) ELSE 0 END) AS tot_act_xp
														FROM response_mst WHERE day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
														AND status_nm="Active"  
								GROUP BY user_id_fk  )RTO ON `RTO`.`user_id_fk`=`U`.`user_id`
					LEFT JOIN  (SELECT user_id,
									SUM(IFNULL(esp_wrk_hrs,0.00)) AS esp_wrk_hrs , 
									AVG(IFNULL(sapience_on,0.00)) AS sapience_on, 
									AVG(IFNULL(sapience_off,0.00)) AS sapience_off,
									SUM(total_days) AS total_days,
									SUM(attendance_cnt) AS attendance_cnt 
									FROM in_out_upload 
									WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND status_nm="Active" 
										 AND IFNULL(attendance_cnt,0.00)<>0.00 
									GROUP BY user_id ) I  ON `I`.`user_id`=`U`.`user_id`
					LEFT JOIN  (SELECT
									CS.user_id,ROUND((MAX(in_time) + MIN(in_time)) / 2,2) AS in_time
								FROM
									(
									SELECT user_id,(CASE WHEN attendance_cnt=0.5 THEN 0 ELSE IFNULL(in_time,0) END) AS in_time, 
									@i := IF(user_id = @prev_user, @i +1, 1) AS seq, @prev_user := user_id
										FROM
											in_out_upload O 
											,(SELECT @i := 1, @prev_user := "") ORE  															
											WHERE O.date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
										 AND IFNULL(O.attendance_cnt,0.00)<>0.00 AND O.status_nm="Active" 
										ORDER BY user_id,in_time
									) CS
							INNER JOIN(
								SELECT
									user_id,FLOOR((COUNT(1)+1) * 0.5) AS min_cnt,FLOOR((COUNT(1)+2) * 0.5) AS max_cnt
								FROM
									in_out_upload 
									WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND status_nm="Active" 
								 AND IFNULL(attendance_cnt,0.00)<>0.00 
							GROUP BY user_id
							) CC
							ON CS.user_id = CC.user_id AND CS.seq BETWEEN CC.min_cnt AND CC.max_cnt
							GROUP BY CS.user_id) IRR  
						ON `IRR`.`user_id`=`U`.`user_id`
					WHERE (IFNULL(U.dept_id_fk,0)=0 OR find_in_set('.$dept_opt.',U.dept_id_fk)<>0) 
					AND `U`.`status_nm` = "Active"
					GROUP BY U.full_name
					ORDER BY U.full_name ASC;';	
							}else{
						$sql='SELECT U.full_name, IRR.in_time, ROUND(I.esp_wrk_hrs/'.$denom.', 2) AS esp_wrk_hrs, 
					I.attendance_cnt, I.total_days, ROUND(I.sapience_on, 2) AS sapience_on, 
					CTE.project_name,
					ROUND(I.sapience_off, 2) AS sapience_off, 
					ROUND(RTO.tot_xp, 2) AS proj_xp, 
					ROUND(RTO.tot_act_xp, 2) AS act_xp, 
					 ROUND((MAX(RTO.tot_xp))*10/I.attendance_cnt, 2) AS cu,
					 ROUND((MAX(RTO.tot_act_xp))*10/I.attendance_cnt, 2) AS ccu
					FROM `user_mst` `U` 
					 LEFT JOIN (SELECT project_id_fk AS project_id,project_name,user_id_fk 
							FROM (
								SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
									@type := tab1.user_id_fk AS dummy
								FROM
									(
									SELECT 
										A.project_id_fk,
										P1.project_name,
										R.user_id_fk,
										SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xp
									FROM
										`response_mst` `R` 
									INNER JOIN (SELECT @num:= 0,    @type := "") B
										ON 1=1
									INNER JOIN `activity_mst` `A` ON
										`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active" 
									INNER JOIN `project_mst` `P1` 
									ON `P1`.`project_id`=`A`.`project_id_fk` 
									WHERE `R`.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'" 
										AND `R`.`status_nm` = "Active" 
									GROUP BY `R`.`user_id_fk`,A.project_id_fk
									ORDER BY `R`.`user_id_fk`,equiv_xp DESC,P1.project_name
								) tab1
								GROUP BY    user_id_fk,project_id_fk,project_name,equiv_xp 
								HAVING row_number = 1)E 			
								)CTE 
					ON CTE.user_id_fk=U.user_id  
					LEFT JOIN  (SELECT user_id_fk,SUM(IFNULL(final_xp,IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0)) AS tot_xp,
								SUM(CASE WHEN tast_stat="Confirmed"  THEN IFNULL(final_xp,IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0) ELSE 0 END) AS tot_act_xp
														FROM response_mst WHERE day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
														AND status_nm="Active"  
								GROUP BY user_id_fk  )RTO ON `RTO`.`user_id_fk`=`U`.`user_id`
					LEFT JOIN  (SELECT user_id,
									SUM(IFNULL(esp_wrk_hrs,0.00)) AS esp_wrk_hrs , 
									AVG(IFNULL(sapience_on,0.00)) AS sapience_on, 
									AVG(IFNULL(sapience_off,0.00)) AS sapience_off,
									SUM(total_days) AS total_days,
									SUM(attendance_cnt) AS attendance_cnt 
									FROM in_out_upload 
									WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND status_nm="Active" 
										 AND IFNULL(attendance_cnt,0.00)<>0.00 
									GROUP BY user_id ) I  ON `I`.`user_id`=`U`.`user_id`
					LEFT JOIN  (SELECT
									CS.user_id,ROUND((MAX(in_time) + MIN(in_time)) / 2,2) AS in_time
								FROM
									(
									SELECT user_id,(CASE WHEN attendance_cnt=0.5 THEN 0 ELSE IFNULL(in_time,0) END) AS in_time, 
									@i := IF(user_id = @prev_user, @i +1, 1) AS seq, @prev_user := user_id
										FROM
											in_out_upload O 
											,(SELECT @i := 1, @prev_user := "") ORE  															
											WHERE O.date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
										 AND IFNULL(O.attendance_cnt,0.00)<>0.00 AND O.status_nm="Active" 
										ORDER BY user_id,in_time
									) CS
							INNER JOIN(
								SELECT
									user_id,FLOOR((COUNT(1)+1) * 0.5) AS min_cnt,FLOOR((COUNT(1)+2) * 0.5) AS max_cnt
								FROM
									in_out_upload 
									WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  AND status_nm="Active" 
								 AND IFNULL(attendance_cnt,0.00)<>0.00 
							GROUP BY user_id
							) CC
							ON CS.user_id = CC.user_id AND CS.seq BETWEEN CC.min_cnt AND CC.max_cnt
							GROUP BY CS.user_id) IRR  
						ON `IRR`.`user_id`=`U`.`user_id`
					WHERE (IFNULL(U.dept_id_fk,0)=0 OR find_in_set('.$dept_opt.',U.dept_id_fk)<>0) 
					AND `U`.`status_nm` = "Active"
					GROUP BY U.full_name
					ORDER BY U.full_name ASC;';	
						}
							
						 	$sqlQuery = $this->db->query($sql);
						}
						else{
			 if($lvl==0)
			 {
				$selec_val='W.wu_name,A.activity_name';
				$gradeColumns='';
			 }
			 if($lvl==1)
			 {
				$selec_val='  1 ';
				$gradeColumns = '  UF.f_full_name,FG.value AS grade, UF.rem AS grade_remarks,';
			 }
			 
				 $this->db->select('  P.project_name,R.user_id_fk,IRR.in_time,
				 ROUND(I.esp_wrk_hrs/'.$denom.',2) AS esp_wrk_hrs ,I.attendance_cnt,
				 I.total_days,
				 ROUND(I.sapience_on,2) AS sapience_on,
				 ROUND(I.sapience_off,2) AS sapience_off,
				 U.full_name,'.$selec_val.' AS val_name,
				 IFNULL(SUM(IFNULL(R.audit_work, R.work_comp)),0) AS w_done,'.$gradeColumns.'
				ROUND(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)),2) AS proj_xp,
				ROUND(SUM(IFNULL(R.equiv_xp, 0)),2) AS filled_xp,
				ROUND(SUM(IFNULL(R.audit_xp, 0)),2) AS audit_xp,
				ROUND(SUM(IFNULL(R.comp_xp, 0)),2) AS comp_xp,
				ROUND(SUM(CASE WHEN R.tast_stat="Confirmed"  THEN IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0) ELSE 0 END),2) AS act_xp,
				ROUND(MAX(RTO.tot_xp),2) AS tot_proj_xp,
				ROUND(MAX(RTO.c_tot_xp),2) AS tot_act_xp,
				ROUND((MAX(RTO.tot_xp))*10/I.attendance_cnt,2) AS cu ,
				ROUND((MAX(RTO.c_tot_xp))*10/I.attendance_cnt,2) AS ccu 
				',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('work_unit_mst W', 'W.wu_id = A.wu_id_fk AND W.status_nm="Active" ','inner');
						$this->db->join('user_mst U', 'U.user_id = R.user_id_fk','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						 	$this->db->join(' (SELECT user_id_fk,SUM(IFNULL(final_xp,IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0)) AS tot_xp,
							SUM(CASE WHEN tast_stat="Confirmed" THEN IFNULL(final_xp,IFNULL(equiv_xp, 0)) + IFNULL(comp_xp, 0) ELSE 0 END) AS c_tot_xp
									FROM response_mst WHERE day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
									AND status_nm="Active"  GROUP BY user_id_fk  )RTO','RTO.user_id_fk=R.user_id_fk','inner');
						 $this->db->join(' (SELECT user_id,
													SUM(IFNULL(esp_wrk_hrs,0.00)) AS esp_wrk_hrs , 
													AVG(IFNULL(sapience_on,0.00)) AS sapience_on, 
													AVG(IFNULL(sapience_off,0.00)) AS sapience_off,
													SUM(total_days) AS total_days,
													SUM(attendance_cnt) AS attendance_cnt 
													FROM in_out_upload 
													WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
														 AND IFNULL(attendance_cnt,0.00)<>0.00 
													group by user_id ) I ','I.user_id=R.user_id_fk','left');
							if($lvl==1)
							{								
							$this->db->join('(SELECT
										FD.project_id_fk,
										FD.user_id_fk,
										CEIL(AVG(FD.`grade_id_fk`)) AS grade,
										GROUP_CONCAT(DISTINCT IFNULL(FD.`remarks`,"")) AS rem
										,GROUP_CONCAT(DISTINCT IFNULL(UFI.full_name,"")) AS f_full_name 
											FROM
												user_feedbacks FD 
												LEFT JOIN user_mst UFI 
												ON UFI.user_id=IFNULL(FD.upd_user,FD.ins_user) 
									WHERE
										FD.fb_given_date >= "'.$date_start.'" AND FD.fb_given_date <= "'.$date_end.'" AND FD.status_nm = "Active" 
										GROUP BY FD.project_id_fk,FD.user_id_fk )UF', 'P.project_id = UF.project_id_fk AND U.user_id = UF.user_id_fk ', 'left');
									
							$this->db->join('feedback_grades FG', 'UF.grade = FG.id', 'left');
							}
							$this->db->join(' (SELECT
														CS.user_id,ROUND((MAX(in_time) + MIN(in_time)) / 2,2) AS in_time
													FROM
														(
														SELECT user_id,(CASE WHEN attendance_cnt=0.5 THEN 0 ELSE IFNULL(in_time,0) END) AS in_time, @i := IF(user_id = @prev_user, @i +1, 1) AS seq, @prev_user := user_id
														FROM
															in_out_upload O 
															,(SELECT @i := 1, @prev_user := "") ORE  															
															WHERE O.date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
														 AND IFNULL(O.attendance_cnt,0.00)<>0.00
													ORDER BY user_id,in_time
													) CS
													INNER JOIN(
														SELECT
															user_id,FLOOR((COUNT(1)+1) * 0.5) AS min_cnt,FLOOR((COUNT(1)+2) * 0.5) AS max_cnt
														FROM
															in_out_upload 
															WHERE date_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"  
														 AND IFNULL(attendance_cnt,0.00)<>0.00 
														GROUP BY user_id
													) CC
													ON CS.user_id = CC.user_id AND CS.seq BETWEEN CC.min_cnt AND CC.max_cnt
													GROUP BY CS.user_id) IRR ','IRR.user_id=R.user_id_fk','left');
						
						$this->db->where('R.dept_id_fk', $dept_opt);												
						$this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						$this->db->where('R.status_nm', 'Active');
					if($pro_sel)
							{
						  $this->db->where_in('P.project_id', $pro_sel);
							}
						if($lvl==0)
						{
						 $this->db->group_by("P.project_name,R.user_id_fk,U.full_name,".$selec_val." ",false);
						 $this->db->order_by("P.project_name,U.full_name,".$selec_val, 'ASC',false); 
						}
						
						if($lvl==1)
						{
						 $this->db->group_by("P.project_name,R.user_id_fk,U.full_name ",false);
						 $this->db->order_by("P.project_name,U.full_name",'ASC',false); 
						}
							$sqlQuery = $this->db->get();
						}
						
		 }
		 
				//$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return json_encode($resultSet);
	  }
	  else{
		return json_encode (json_decode ("{}"));;
	  } 
	 }
	 
	 public function select_leave_reportee($file_nm,$u_id,$pro_sel,$date_start,$date_end,$lvl,$emp,$role_id,$dept_opt){
		 $where='';
		 
		 $lel_val=array("Pending","Approved","Rejected");
		 
	if($file_nm=="Others")
			{
			$sql1='SELECT  U.full_name,"comp_track" AS tbl_nm,E.comp_track_id,
			UNIX_TIMESTAMP(CONVERT_TZ(E.start_date,"+00:00",'.$this->tx_cn1.')) AS start_date ,E.reason,E.conf_status,
				UNIX_TIMESTAMP(CONVERT_TZ(E.end_date,"+00:00",'.$this->tx_cn1.')) AS end_date,LM.l_type_id,LM.leave_type_name ,
					UNIX_TIMESTAMP(E.ins_dt)  AS in_dt ,
					UI.full_name AS in_user,CASE WHEN E.is_half_day=1 THEN "Yes" ELSE "No" END AS is_half_day,
					UNIX_TIMESTAMP(E.upd_dt)  AS up_dt ,
					UP.full_name AS up_user, "N/A" AS comp_dt,"N/A" AS work_dt,"N/A" AS d_week,
					PO.app AS approved_in,
					PO.rej AS rejected_in,
					CONCAT(IFNULL(PPRO.p_name,""),"|",IFNULL(PO.total_ids,"")) AS pending_in
			FROM
				 comp_track E 			
			JOIN `leave_type_mst` LM 
			ON E.leave_type=LM.l_type_id AND LM.status_nm="Active" 
			JOIN user_mst U 
			ON U.user_id=E.user_id_fk 
			LEFT JOIN (SELECT PPPO.comp_track_id_fk,
							   GROUP_CONCAT(CASE WHEN PPPO.conf_status="Approved" THEN PR.project_name ELSE null END) AS app ,
							   GROUP_CONCAT(CASE WHEN PPPO.conf_status="Rejected" THEN PR.project_name ELSE null END) AS rej,
							   GROUP_CONCAT(PR.project_name) AS total_ids
						FROM po_leave_approval PPPO 
						LEFT JOIN project_mst PR 
						ON PPPO.project_id_fk=PR.project_id						
						WHERE PPPO.status_nm = "Active" 
						GROUP BY PPPO.comp_track_id_fk
				)PO 
			ON PO.comp_track_id_fk=E.comp_track_id
			LEFT JOIN (SELECT C.comp_track_id,GROUP_CONCAT(P.project_name) AS p_name 
							FROM `comp_track` C
							INNER JOIN project_mst P 
						ON FIND_IN_SET(P.project_id, C.project_ids_fks)<>0 						
						GROUP BY C.comp_track_id) PPRO
	ON PPRO.comp_track_id=E.comp_track_id 
			LEFT JOIN user_mst UI 
			ON UI.user_id=E.ins_user 
			LEFT JOIN user_mst UP 
			ON UP.user_id=E.upd_user 
			WHERE IFNULL(E.is_comp,0)=0 AND (( E.start_date BETWEEN "'.$date_start.'"  AND "'.$date_end.'" )
			OR ( E.end_date  BETWEEN "'.$date_start.'"  AND "'.$date_end.'"  )) AND E.status_nm="Active" 
			AND E.conf_status="'. $lel_val[$lvl].'"'; 
			if($dept_opt)
			{
				$where=' AND (IFNULL(U.dept_id_fk,0)=0';
				if(is_array($dept_opt))
				{
				foreach($dept_opt AS $dept_it)
				{
				$where=$where.' OR find_in_set('.$dept_it.',U.dept_id_fk)<>0 ';
				}
				}else{
					$where=$where.' OR find_in_set('.$dept_opt.',U.dept_id_fk)<>0 ';
				}
				$where=$where.') ';
			}
				
			if($emp)
			{
			$where =$where.' AND E.user_id_fk IN ('.$emp.') ';
			}
			
			 if($role_id!='1' && $role_id!='2')
			{
			 $where =$where.' AND U.manager_id='.$u_id.' ';
			}
			
			$sql=$sql1.$where;
			}
		
			if($file_nm=="Week Off")
			{
			$sql1='SELECT  U.full_name,"emp_week_off" AS tbl_nm,E.emp_week_off_id AS comp_track_id,
			UNIX_TIMESTAMP(CONVERT_TZ(E.start_dt,"+00:00",'.$this->tx_cn1.')) AS start_date ,"N/A" AS reason,E.conf_status,
			UNIX_TIMESTAMP(CONVERT_TZ(E.end_dt,"+00:00",'.$this->tx_cn1.'))  AS end_date,LM.l_type_id,LM.leave_type_name ,
					UNIX_TIMESTAMP(E.ins_dt)  AS in_dt ,
					UI.full_name AS in_user,
					UNIX_TIMESTAMP(E.upd_dt)  AS up_dt ,
					UP.full_name AS up_user,"No" AS is_half_day,
					"N/A" AS comp_dt,"N/A" AS work_dt,
					DAYNAME(CONCAT("2017-11-2", E.week_day))   AS d_week,
					"" AS approved_in,
					"" AS rejected_in,
					"|" AS pending_in
			FROM
				 emp_week_off E 			   
			JOIN `leave_type_mst` LM 
			ON LM.leave_type_name="Week Off" AND LM.status_nm="Active" 
			JOIN user_mst U 
			ON U.user_id=E.user_id_fk 
			LEFT JOIN user_mst UI 
			ON UI.user_id=E.ins_user 
			LEFT JOIN user_mst UP 
			ON UP.user_id=E.upd_user 
			WHERE  (( E.start_dt BETWEEN "'.$date_start.'"  AND "'.$date_end.'" )
			OR ( IFNULL(E.end_dt,"'.$date_start.'" )  BETWEEN "'.$date_start.'"  AND "'.$date_end.'"  )) AND E.status_nm="Active" 
			AND E.conf_status="'. $lel_val[$lvl].'"';
			if($dept_opt)
				{
					$where=' AND (IFNULL(U.dept_id_fk,0)=0';
					if(is_array($dept_opt))
					{
					foreach($dept_opt AS $dept_it)
					{
					$where=$where.' OR find_in_set('.$dept_it.',U.dept_id_fk)<>0 ';
					}
					}else{
						$where=$where.' OR find_in_set('.$dept_opt.',U.dept_id_fk)<>0 ';
					}
					$where=$where.') ';
				}
			
			if($emp)
			{
			$where =$where.' AND E.user_id_fk IN ('.$emp.') ';
			}
			 if($role_id!='1' && $role_id!='2')
			{
			 $where =$where.' AND U.manager_id='.$u_id.' ';
			}
			
			$sql=$sql1.$where;
			}
			
			
			if($file_nm=="Comp Off")
			{
			$sql1='SELECT  U.full_name,"comp_track" AS tbl_nm,E.comp_track_id,"N/A"  AS start_date ,E.reason,E.conf_status,
				"N/A" AS end_date,
				LM.l_type_id,LM.leave_type_name ,
					UNIX_TIMESTAMP(E.ins_dt)  AS in_dt ,
					UI.full_name AS in_user,
					UNIX_TIMESTAMP(E.upd_dt)  AS up_dt ,
					UP.full_name AS up_user	,"No" AS is_half_day,
					UNIX_TIMESTAMP(CONVERT_TZ(E.start_date,"+00:00",'.$this->tx_cn1.'))  AS comp_dt,
					UNIX_TIMESTAMP(CONVERT_TZ(E.end_date,"+00:00",'.$this->tx_cn1.')) AS work_dt,"N/A" AS d_week,
					PO.app AS approved_in,
					PO.rej AS rejected_in,
					CONCAT(IFNULL(PPRO.p_name,""),"|",IFNULL(PO.total_ids,"")) AS pending_in
			FROM
				 comp_track E 			   
			JOIN `leave_type_mst` LM 
			ON E.leave_type=LM.l_type_id AND LM.status_nm="Active" 			
			JOIN user_mst U 
			ON U.user_id=E.user_id_fk 
			LEFT JOIN (SELECT PPPO.comp_track_id_fk,
							   GROUP_CONCAT(CASE WHEN PPPO.conf_status="Approved" THEN PR.project_name ELSE null END) AS app ,
							   GROUP_CONCAT(CASE WHEN PPPO.conf_status="Rejected" THEN PR.project_name ELSE null END) AS rej,
							   GROUP_CONCAT(PR.project_name) AS total_ids
						FROM po_leave_approval PPPO 
						LEFT JOIN project_mst PR 
						ON PPPO.project_id_fk=PR.project_id						
						WHERE PPPO.status_nm = "Active" 
						GROUP BY PPPO.comp_track_id_fk
				)PO 
			ON PO.comp_track_id_fk=E.comp_track_id
			LEFT JOIN (SELECT C.comp_track_id,GROUP_CONCAT(P.project_name) AS p_name 
							FROM `comp_track` C
							INNER JOIN project_mst P 
						ON FIND_IN_SET(P.project_id, C.project_ids_fks)<>0 						
						GROUP BY C.comp_track_id) PPRO
	ON PPRO.comp_track_id=E.comp_track_id 
			LEFT JOIN user_mst UI 
			ON UI.user_id=E.ins_user 
			LEFT JOIN user_mst UP 
			ON UP.user_id=E.upd_user 
			WHERE  E.is_comp=1 AND  E.start_date  BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND E.status_nm="Active" 
			AND E.conf_status="'.$lel_val[$lvl].'"';	
				if($dept_opt)
				{
					$where=' AND (IFNULL(U.dept_id_fk,0)=0';
					if(is_array($dept_opt))
					{
					foreach($dept_opt AS $dept_it)
					{
					$where=$where.' OR find_in_set('.$dept_it.',U.dept_id_fk)<>0 ';
					}
					}else{
						$where=$where.' OR find_in_set('.$dept_opt.',U.dept_id_fk)<>0 ';
					}
					$where=$where.') ';
				}			
			if($emp)
			{
			$where =' AND E.user_id_fk IN ('.$emp.') ';
			}
			
			 if($role_id!='1' && $role_id!='2')
			{
			 $where =' AND U.manager_id='.$u_id.' ';
			}
			$sql=$sql1.$where;
			
			}
		$sqlQuery = $this->db->query($sql);
		$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  }	
	 }
	 
	 
	  public function select_leave_reportee1($file_nm,$u_id,$pro_sel,$date_start,$date_end,$lvl,$emp,$role_id,$dept_opt){
		 $where='';
		 $pids= $this->session->userdata('proj_fk');
		 $lel_val=array("Pending","Approved","Rejected");
		 $sel_val='';
		 $join_val='';
		// if($lel_val[$lvl]=="Pending")
		 {
			 $sel_val=' ,PM.project_id,PM.project_name';
			 $join_val='  JOIN `project_mst` PM
			ON FIND_IN_SET(PM.project_id, E.project_ids_fks) > 0 ';
		 }
	if($file_nm=="Others")
			{
			$sql1='SELECT  U.full_name,"comp_track" AS tbl_nm,E.comp_track_id,
				DATE_FORMAT(E.start_date, "%d/%b/%y") AS start_date ,E.reason,E.conf_status,
				DATE_FORMAT(E.end_date, "%d/%b/%y") AS end_date,LM.l_type_id,LM.leave_type_name ,
					DATE_FORMAT(CONVERT_TZ(E.ins_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS in_dt ,
					UI.full_name AS in_user,CASE WHEN E.is_half_day=1 THEN "Yes" ELSE "No" END AS is_half_day,
					DATE_FORMAT(CONVERT_TZ(E.upd_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS up_dt ,	
					UP.full_name AS up_user, "N/A" AS comp_dt,"N/A" AS work_dt,"N/A" AS d_week,
					PO.app AS approved_in,
					PO.rej AS rejected_in,
					CONCAT(IFNULL(PPRO.p_name,""),"|",IFNULL(PO.total_ids,"")) AS pending_in
					'.$sel_val.'
			FROM
				 comp_track E 			
			JOIN `leave_type_mst` LM 
			ON E.leave_type=LM.l_type_id AND LM.status_nm="Active" 
			JOIN user_mst U 
			ON U.user_id=E.user_id_fk 
			 '.$join_val.' 
			  LEFT JOIN po_leave_approval POA 
			 ON POA.comp_track_id_fk=E.comp_track_id AND POA.project_id_fk=PM.project_id  
			LEFT JOIN (SELECT PPPO.comp_track_id_fk,
							   GROUP_CONCAT(CASE WHEN PPPO.conf_status="Approved" THEN PR.project_name ELSE null END) AS app ,
							   GROUP_CONCAT(CASE WHEN PPPO.conf_status="Rejected" THEN PR.project_name ELSE null END) AS rej,
							   GROUP_CONCAT(PR.project_name) AS total_ids
						FROM po_leave_approval PPPO 
						LEFT JOIN project_mst PR 
						ON PPPO.project_id_fk=PR.project_id						
						WHERE PPPO.status_nm = "Active" 
						GROUP BY PPPO.comp_track_id_fk
				)PO 
			ON PO.comp_track_id_fk=E.comp_track_id
			LEFT JOIN (SELECT C.comp_track_id,GROUP_CONCAT(P.project_name) AS p_name 
							FROM `comp_track` C
							INNER JOIN project_mst P 
						ON FIND_IN_SET(P.project_id, C.project_ids_fks)<>0 						
						GROUP BY C.comp_track_id) PPRO
	ON PPRO.comp_track_id=E.comp_track_id 
			LEFT JOIN user_mst UI 
			ON UI.user_id=E.ins_user 
			LEFT JOIN user_mst UP 
			ON UP.user_id=E.upd_user 
			WHERE IFNULL(E.is_comp,0)=0 AND (( E.start_date BETWEEN "'.$date_start.'"  AND "'.$date_end.'" )
			OR ( E.end_date  BETWEEN "'.$date_start.'"  AND "'.$date_end.'"  )) AND E.status_nm="Active" 
			AND IFNULL(POA.conf_status,"Pending")="'. $lel_val[$lvl].'"'; 	
			
			if($dept_opt)
			{
				//$where=' AND (IFNULL(U.dept_id_fk,0)=0';
				// if(is_array($dept_opt))
				// {
				// foreach($dept_opt AS $dept_it)
				// {
				// $where=$where.' AND find_in_set('.$dept_it.',U.dept_id_fk)<>0 ';
				// }
				// }else{
					// $where=$where.' AND find_in_set('.$dept_opt.',U.dept_id_fk)<>0 ';
				// }
				//$where=$where.') ';
				 $where=$where.' AND PM.dept_id_fk = "'.$dept_opt.'" ';
			}
				
				if (!empty($pids) && $pids != 0) {
				$where = $where . 'AND find_in_set(PM.project_id,"'.$pids.'") > 0 ';
			}
			
			if (!empty($pro_sel) && $pro_sel != 0) {
				$where = $where . ' AND PM.project_id ="'.$pro_sel.'" ';
			}
			
			
			$sql=$sql1.$where;
			}
		
			
			if($file_nm=="Comp Off")
			{
			$sql1='SELECT  U.full_name,"comp_track" AS tbl_nm,E.comp_track_id,"N/A"  AS start_date ,E.reason,E.conf_status,
				"N/A" AS end_date,
				LM.l_type_id,LM.leave_type_name,
					DATE_FORMAT(CONVERT_TZ(E.ins_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS in_dt ,
					UI.full_name AS in_user,
					DATE_FORMAT(CONVERT_TZ(E.upd_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS up_dt ,	
					UP.full_name AS up_user	,"No" AS is_half_day,
					DATE_FORMAT(E.start_date, "%d/%b/%y") AS comp_dt,
					DATE_FORMAT(E.end_date, "%d/%b/%y") AS work_dt,"N/A" AS d_week,
					PO.app AS approved_in,
					PO.rej AS rejected_in,
					CONCAT(IFNULL(PPRO.p_name,""),"|",IFNULL(PO.total_ids,"")) AS pending_in	'.$sel_val.'
			FROM
				 comp_track E 			
			JOIN `leave_type_mst` LM 
			ON E.leave_type=LM.l_type_id AND LM.status_nm="Active" 
			JOIN user_mst U 
			ON U.user_id=E.user_id_fk 
			 '.$join_val.' 
			 LEFT JOIN po_leave_approval POA 
			 ON POA.comp_track_id_fk=E.comp_track_id AND POA.project_id_fk=PM.project_id 
			LEFT JOIN (SELECT PPPO.comp_track_id_fk,
							   GROUP_CONCAT(CASE WHEN PPPO.conf_status="Approved" THEN PR.project_name ELSE null END) AS app ,
							   GROUP_CONCAT(CASE WHEN PPPO.conf_status="Rejected" THEN PR.project_name ELSE null END) AS rej,
							   GROUP_CONCAT(PR.project_name) AS total_ids
						FROM po_leave_approval PPPO 
						LEFT JOIN project_mst PR 
						ON PPPO.project_id_fk=PR.project_id						
						WHERE PPPO.status_nm = "Active" 
						GROUP BY PPPO.comp_track_id_fk
				)PO 
			ON PO.comp_track_id_fk=E.comp_track_id
			LEFT JOIN (SELECT C.comp_track_id,GROUP_CONCAT(P.project_name) AS p_name 
							FROM `comp_track` C
							INNER JOIN project_mst P 
						ON FIND_IN_SET(P.project_id, C.project_ids_fks)<>0 						
						GROUP BY C.comp_track_id) PPRO
	ON PPRO.comp_track_id=E.comp_track_id 
			LEFT JOIN user_mst UI 
			ON UI.user_id=E.ins_user 
			LEFT JOIN user_mst UP 
			ON UP.user_id=E.upd_user 
			WHERE  E.is_comp=1 AND  E.start_date  BETWEEN "'.$date_start.'" AND "'.$date_end.'" AND E.status_nm="Active" 
			AND IFNULL(POA.conf_status,"Pending")="'.$lel_val[$lvl].'"';	
				if($dept_opt)
				{			//		$where=' AND (IFNULL(U.dept_id_fk,0)=0';
					// if(is_array($dept_opt))
					// {
					// foreach($dept_opt AS $dept_it)
					// {
					// $where=$where.' AND find_in_set('.$dept_it.',U.dept_id_fk)<>0 ';
					// }
					// }else{
						// $where=$where.' AND find_in_set('.$dept_opt.',U.dept_id_fk)<>0 ';
					// }
					//$where=$where.') ';
					 $where=$where.' AND PM.dept_id_fk = "'.$dept_opt.'" ';
				}			
			// if($emp)
			// {
			// $where =' AND E.user_id_fk IN ('.$emp.') ';
			// }
			if (!empty($pids) && $pids != 0) {
				$where = $where . 'AND find_in_set(PM.project_id,"'.$pids.'") > 0';
			}
			
			if (!empty($pro_sel) && $pro_sel != 0) {
				$where = $where . ' AND PM.project_id ="'.$pro_sel.'" ';
			}
			
			
			
			$sql=$sql1.$where;
			
			}
		$sqlQuery = $this->db->query($sql);
		$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  }	
	 }
	 
	 public function select_emp_reportee($file_nm,$pro_sel,$date_start,$date_end,$lvl,$emp){
		if($file_nm=="Reportees")
		 {
			 if($lvl==0)
			 {
				$selec_val='W.wu_name,A.activity_name';
			 }
			 if($lvl==1)
			 {
				$selec_val='P.project_name';
			 }
			 if($lvl==2)
			 {
				$selec_val='T.task_name';
			 }
			 if($lvl==3)
			 {
				$selec_val='ST.sub_name';
			 }
			 if($lvl==4)
			 {
				$selec_val='J.job_name';
			 }
			 if($lvl==5)
			 {
				$selec_val='CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))';
			 }
			 if($lvl==6)
			 {
				$selec_val='W.wu_name,A.activity_name,DATE_FORMAT(R.day_val, "%d/%m/%y")';
			 }
		 $this->db->select('  D.dept_name,R.user_id_fk,U.full_name,'.$selec_val.' AS val_name,
		 IFNULL(SUM(IFNULL(R.audit_work, R.work_comp)),0) AS w_done,
		 ROUND(IFNULL(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),0),2) AS proj_xp, 
		ROUND(IFNULL(SUM(CASE WHEN R.tast_stat="Confirmed"  THEN (IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))) ELSE 0 END ),0),2) AS act_xp ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('work_unit_mst W', 'W.wu_id = A.wu_id_fk AND W.status_nm="Active" ','inner');
						$this->db->join('user_mst U', 'U.user_id = R.user_id_fk','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						$this->db->join('dept_mst D', 'D.dept_id=R.dept_id_fk AND D.status_nm="Active" ','inner');
						if($lvl==2)
						{
						$this->db->join('task_mst T', 'T.task_id = A.task_id_fk AND T.status_nm="Active" ','inner');
						}
						if($lvl==3 || $lvl==5)
						{
						$this->db->join('sub_mst ST', 'ST.sub_id = A.sub_id_fk AND ST.status_nm="Active" ','inner');
						}
						if($lvl==4  || $lvl==5)
						{
						$this->db->join('job_mst J', 'J.job_id = A.job_id_fk AND J.status_nm="Active" ','inner');
						}
						 $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 $this->db->where('R.status_nm', 'Active');
						$this->db->where_in('R.user_id_fk', $emp);
							
						 $this->db->group_by("D.dept_name,R.user_id_fk,U.full_name,".$selec_val." ",false);
						 $this->db->order_by($selec_val, 'ASC',false); 
		 }
		$sqlQuery = $this->db->get();
		$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return json_encode($resultSet);
	  }
	  else{
		return json_encode (json_decode ("{}"));;
	  } 
	 }
	 
		// public function select_emp_reportee($uid,$date_start,$date_end){
		
		// $this->db->select(' IFNULL(R.audit_work,R.work_comp) AS work_comp,
				// IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0))) AS tot_xp,
				// A.activity_name,
				// R.std_tgt_val,DATE_FORMAT(R.day_val, "%d-%b-%Y") AS day_val ,R.tast_stat AS stat_nm,
				// CASE WHEN R.tast_stat="Audit Pending" THEN "t_stat_1"
					// WHEN R.tast_stat="Review Pending" THEN "t_stat_2"
					// WHEN R.tast_stat="Confirmation Pending" THEN "t_stat_3" 
					// WHEN R.tast_stat="Confirmed" THEN "t_stat_4" END AS tast_stat,
				// CASE WHEN R.issue_flag=1 THEN "Y" ELSE "N" END AS issue_flag ',FALSE);
						// $this->db->from('response_mst R');
						// $this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						// $this->db->join('user_mst U', 'U.user_id = R.user_id_fk','inner');
						// $this->db->where('R.user_id_fk', $uid);	
						 // $this->db->where('R.day_val BETWEEN "'.$date_start.'" AND "'.$date_end.'"', null, false);
						 // $this->db->where('R.status_nm', 'Active');	
						 
						 // $sqlQuery = $this->db->get();
				// $resultSet = $sqlQuery->result_array();
	  // if($resultSet)
	  // {
		  // return json_encode($resultSet);
	  // }
	  // else{
		// return json_encode (json_decode ("{}"));;
	  // } 
		 
	 // }
	 
	public function select_resp($t_date1,$u_id){		
		$this->db->select('  R.response_id,R.milestone_id,IFNULL(R.audit_work,R.work_comp) AS work_comp,
				ROUND(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0))),2) AS equiv_xp,
				A.activity_name,R.std_tgt_val,W.wu_name,
				CASE WHEN R.tast_stat="Audit Pending" THEN "t_stat_1"
					WHEN R.tast_stat="Review Pending" THEN "t_stat_2"
					WHEN R.tast_stat="Confirmation Pending" THEN "t_stat_3"
					WHEN R.tast_stat="Confirmed" THEN "t_stat_4" END AS tast_stat,
				CASE WHEN R.issue_flag=1 THEN "Y" ELSE "N" END AS issue_flag ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('work_unit_mst W', 'A.wu_id_fk=W.wu_id AND W.status_nm="Active" ','inner');
						 $this->db->where('R.day_val', $t_date1);
						 $this->db->where('R.user_id_fk',$u_id);
						 $this->db->where('R.status_nm', 'Active');
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();				
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 
	public function task_view($resp_id){		 
		 $this->db->select(' R.response_id,R.milestone_id, R.activity_id_fk,IFNULL(R.audit_work,R.work_comp) AS work_comp,ROUND(R.equiv_xp,2) AS equiv_xp,
		 ROUND(IFNULL(R.comp_xp,0),2) AS comp_xp,
		 ROUND(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0))),2) AS total_xp,
				A.activity_name,R.std_tgt_val,
				CASE WHEN R.tast_stat="Audit Pending" THEN "t_stat_1"
					WHEN R.tast_stat="Review Pending" THEN "t_stat_2"
					WHEN R.tast_stat="Confirmation Pending" THEN "t_stat_3"
					WHEN R.tast_stat="Confirmed" THEN "t_stat_4" END AS tast_color,
					R.tast_stat,
				CASE WHEN R.issue_flag=1 THEN "Y" ELSE "N" END AS issue_flag,
				R.issue_desc AS issue_desc,R.task_desc,P.project_name,TA.task_name,S.sub_name ,J.job_name,	WU.wu_name,	CU.full_name AS comp_user_nm,
				DATE_FORMAT(CONVERT_TZ(R.comp_upd_dt,"+00:00",'.$this->tx_cn.'), "%d-%b-%Y %k:%i:%s") AS comp_upd_dt ,R.review_xp,R.review_desc ,	RU.full_name AS review_user_nm,
				DATE_FORMAT(CONVERT_TZ(R.review_upd_dt,"+00:00",'.$this->tx_cn.'), "%d-%b-%Y %k:%i:%s") AS review_upd_dt,AU.full_name AS audit_user_nm,
				DATE_FORMAT(CONVERT_TZ(R.audit_upd_dt,"+00:00",'.$this->tx_cn.'), "%d-%b-%Y %k:%i:%s") AS audit_upd_dt ',FALSE);
						$this->db->from('response_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('work_unit_mst WU', 'WU.wu_id=A.wu_id_fk AND WU.status_nm="Active" ','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','left');						
						$this->db->join('task_mst TA', 'A.task_id_fk=TA.task_id AND TA.status_nm="Active" ','left');
						$this->db->join('sub_mst S', 'A.sub_id_fk=S.sub_id AND S.status_nm="Active" ','left');
						$this->db->join('job_mst J', 'A.job_id_fk=J.job_id AND J.status_nm="Active" ','left');
						$this->db->join('user_mst CU', 'CU.user_id = R.comp_user','left');
						$this->db->join('user_mst RU', 'RU.user_id = R.review_user','left');
						$this->db->join('user_mst AU', 'AU.user_id = R.audit_user','left');
						 $this->db->where('R.response_id',$resp_id);
						 $this->db->where('R.status_nm', 'Active');						 
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  }
	 }
	 public function std_tgt_modi($resp_id){		 
		 $this->db->select('  A.activity_name,R.std_tgt_id ,
							CASE WHEN R.end_dt IS NULL THEN "" ELSE R.change_desc END AS change_desc,R.tgt_val,A.activity_id,DATE_FORMAT(R.start_dt, "%d/%m/%y") AS start_dt,
						DATE_FORMAT(R.end_dt, "%d/%m/%y") AS end_dt,U.full_name AS ins_user,
						DATE_FORMAT(CONVERT_TZ(R.ins_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS ins_dt ,
						UP.full_name AS upd_user,
						DATE_FORMAT(CONVERT_TZ(R.upd_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS upd_dt  ',FALSE);
						$this->db->from('std_tgt_mst R');
						$this->db->join('activity_mst A', 'R.activity_id_fk=A.activity_id AND A.status_nm="Active" ','inner');
						$this->db->join('user_mst U', 'R.ins_user=U.user_id','inner');
						$this->db->join('user_mst UP', 'R.upd_user=UP.user_id','left');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						$where_key=explode(",",$resp_id);
						$this->db->where_in('R.activity_id_fk', $where_key);
						 $this->db->where('R.status_nm', 'Active');						 
						 $this->db->order_by('start_dt', 'DESC');		
						 $this->db->order_by('upd_dt', 'DESC');		
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
		 
	 }
	 
	 public function task_modi_2($resp_id){
		 $this->db->select('AP.response_id_fk,AP.work_comp,AP.change_desc,AP.update_status,	
		 DATE_FORMAT(CONVERT_TZ(AP.ins_dt,"+00:00",'.$this->tx_cn.'), "%d-%b-%Y %k:%i:%s") AS ins_dt',FALSE);
		 $this->db->from('audit_pend_resp AP');
		 $this->db->where('AP.status_nm', 'Active');
		 $this->db->where('AP.response_id_fk',$resp_id);
		 $this->db->order_by('AP.ins_dt','DESC');
		
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();		
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }


	  public function std_update($ans){
		 	 
			 $this->db->trans_start();
			 
			 $data = array(
				'end_dt' => $ans[4],
				'upd_user' => $ans[2],
				'change_desc' => $ans[6]
				);
				$this->db->set('upd_dt', 'current_timestamp', FALSE);
				$where_key=explode(",",$ans[1]);
				$this->db->where_in('std_tgt_id', $where_key);
				$this->db->update('std_tgt_mst', $data);								
							$act_id=explode(",",$ans[5]);
			foreach($act_id AS $r)
			{
			$data_in = array(
				'activity_id_fk' => $r,
				'tgt_val' => $ans[0],
				'start_dt' => $ans[3],
				'status_nm' => "Active",
				'ins_user' => $ans[2],
				'change_desc' => $ans[6]
				);
				$this->db->set('ins_dt', 'current_timestamp', FALSE);
				$this->db->insert('std_tgt_mst', $data_in);									
			}
			
			 //hist_response_mst
					
			 $data_up = array(
				'std_tgt_val' => $ans[0],
				'upd_user' =>$ans[2]
				);
				$this->db->set('upd_dt', 'current_timestamp', FALSE);
				$this->db->set('equiv_xp','(`work_comp`/'.$ans[0].')*10',false);
				$this->db->set('audit_xp','(`audit_work`/'.$ans[0].')*10',false);
				$this->db->set('final_xp','IF(total_xp is null,IF(audit_xp is null,((`work_comp`/'.$ans[0].')*10),((`audit_work`/'.$ans[0].')*10)),final_xp) ',FALSE);				
				$where_key1=explode(",",$ans[5]);
				$this->db->where('day_val >=',$ans[3]);
				$this->db->where_in('activity_id_fk', $where_key1);
				$this->db->update('response_mst', $data_up);		
				
			$this->db->trans_complete();
			
							if($this->db->trans_status() === FALSE)
							  {
								 return false;
							  }
							  else{
								 return "success";
							  }
	 }		 
	 public function week_off_adju($ans){
			 $this->db->trans_start();
			 $lids=array();
			foreach($ans AS $c)
			{
				if($c[1]=="Update" && $c[0])
				{
				$data_in = array(
				'start_dt' => $c[3],
				'week_day' => $c[2],
				'conf_status' =>$c[6],
				'upd_user' => $c[7]
				);
				$this->db->set('end_dt', $c[4], FALSE);
				$this->db->set('upd_dt', 'current_timestamp', FALSE);
				$this->db->where_in('emp_week_off_id', $c[0]);
				$this->db->update('emp_week_off', $data_in);	
				$lids[] = $c[0];
				}else{
					$data_in = array(
				'start_dt' => $c[3],
				'week_day' => $c[2],
				'conf_status' =>$c[6],
				'status_nm' =>'Active',
				'ins_user' => $c[7],
				'user_id_fk' => $c[7]
				);
				$this->db->set('end_dt', $c[4], FALSE);
				$this->db->set('ins_dt', 'current_timestamp', FALSE);
				$this->db->insert('emp_week_off', $data_in);
				 $lids[] = $this->db->insert_id();
				}
			}
			$this->db->trans_complete();
			if($this->db->trans_status() === FALSE)
			  {
				 return false;
			  }
			  else{
				  return implode(",",$lids);
			  }
	 }
	 
	 public function update_leave($ans){
			 $this->db->trans_start();
			foreach($ans AS $c)
			{
				//print_r($r);
				$data_in = array(
				'leave_type' => $c[1],
				'is_comp' => $c[2],
				'is_half_day' =>(($c[8])?$c[8]:null),
				'start_date' => $c[3],
				'end_date' => $c[4],
				'reason' => $c[5],
				'status_nm' => $c[9] ,
				'conf_status' =>$c[6],
				'upd_user' => $c[7]
				);
				$this->db->set('upd_dt', 'current_timestamp', FALSE);
				$this->db->where_in('comp_track_id', $c[0]);
				$this->db->update('comp_track', $data_in);	
			}
			$this->db->trans_complete();
			if($this->db->trans_status() === FALSE)
			  {
				 return false;
			  }
			  else{
				 return "success";
			  }
	 }		 
	  public function ins_leave($ans){
		  $lids=array();
			 $this->db->trans_start();
			foreach($ans AS $c)
			{
				$pd='';
				if(is_array($c[9]))
						{
						$pd=implode(",",$c[9]);
						}else{
						$pd=$c[9];
						}
						
				//print_r($r);
				$data_in = array(
				'leave_type' => $c[1],
				'is_comp' => $c[2],
				'is_half_day' =>(($c[8])?$c[8]:null),
				'start_date' => $c[3],
				'end_date' => $c[4],
				'reason' => $c[5],
				'user_id_fk' => $c[7],
				'conf_status' =>$c[6],
				'ins_user' => $c[7],
				'status_nm' => "Active" ,
				"project_ids_fks" => $pd
				);
				$this->db->set('ins_dt', 'current_timestamp', FALSE);
				$this->db->insert('comp_track', $data_in);	
				 $lids[] = $this->db->insert_id();
			}
			$this->db->trans_complete();
			if($this->db->trans_status() === FALSE)
			  {
				 return false;
			  }
			  else{
				 return implode(",",$lids);
			  }
	 }	 
	 
	  public function ins_trade($ans,$r_ids,$ans_r){
			 $this->db->trans_start();
			 $data_in=[];
			 //print_r($ans);
			 if($ans)
			 {
				foreach ($ans as $key => $c) {
				$resp = explode(",",$c[2]);
									foreach($resp as $u){
										$data_in[] = array(
													'r_param_id_fk' => $c[0],
													'feedback_val' => $c[1],
													'response_id_fk' => $u,
													'ins_user' => $c[3],
													'status_nm' => "Active" ,
													'ins_dt'=> date('Y-m-d H:i:s')
													);
									}
				}
				
				//$this->db->set_insert_batch('ins_dt', 'current_timestamp', FALSE);
				$this->db->insert_batch('review_log', $data_in);
			 }
				if($ans_r)
				{
			foreach($ans_r AS $c)
			{
				$data_in = array(
								'review_xp' => $c[1],
								'review_desc' => $c[0],
								'review_user' => $c[3],
								'rev_assignee' => $c[3],
								'upd_user' => $c[3],
								'tast_stat'=>'Confirmation Pending'
				);
				$this->db->set('review_upd_dt', 'current_timestamp', FALSE);
				$this->db->set('upd_dt', 'current_timestamp', FALSE);
				$where_key=explode(",",$c[2]);
				$this->db->where_in('response_id', $where_key);
				$this->db->update('response_mst', $data_in);	
				
			}		
			}			
			
			$data = array(
							'review_user' => $ans[0][3],
							'rev_assignee' => $ans[0][3],
							'upd_user' => $ans[0][3],
							'tast_stat'=>'Confirmation Pending'
								);
				$this->db->set('review_upd_dt', 'current_timestamp', FALSE);
				$this->db->set('upd_dt', 'current_timestamp', FALSE);				
				$where_key=explode(",",$r_ids);
				$this->db->where_in('response_id', $where_key);
				$this->db->where('status_nm', 'Active');
				$this->db->update('response_mst', $data);
				
				
			$this->db->trans_complete();
			if($this->db->trans_status() === FALSE)
			  {
				 return false;
			  }
			  else{
				 return "success";
			  }
	 }	 
	 
	public function audit_conf($ans){
		$this->db->trans_start();
		
		 //hist_response_mst
					
					if($ans[4]=='audit_upd')
					{
							if(!$ans[5])
							{
								$ans[5]='Review Pending';
							}
						$sql4='UPDATE `response_mst` R,`audit_pend_resp` AP 
						SET R.work_comp = AP.work_comp, R.equiv_xp = (AP.`work_comp`/R.`std_tgt_val`)*10,
						R.final_xp = (AP.`work_comp`/R.`std_tgt_val`)*10,
						R.audit_upd_dt = current_timestamp,
						R.upd_dt = current_timestamp, 
						R.`audit_user` = '.$ans[2].', R.`upd_user` = '.$ans[2].',
						R.`tast_stat` = "'.$ans[5].'" 
						WHERE R.`status_nm` = "Active" 
						AND AP.response_id_fk=R.response_id AND AP.`status_nm` = "Active" 
						AND AP.`update_status` = "Pending" AND R.`response_id` IN('.$ans[1].')';
						$this->db->query($sql4);
						
					}
					else{
					if($ans[4]=='audit_ins')
					{
						if(!$ans[5])
							{
								$ans[5]='Review Pending';
							}
						$data = array(
								'audit_work' => $ans[0],
								'audit_desc' => $ans[3],
								'audit_user' => $ans[2],
								'upd_user' => $ans[2],
								'tast_stat'=>$ans[5]
								);
						$this->db->set('audit_xp','('.$ans[0].'/`std_tgt_val`)*10',FALSE);
						$this->db->set('final_xp','('.$ans[0].'/`std_tgt_val`)*10',FALSE);
						$this->db->set('audit_upd_dt', 'current_timestamp', FALSE);
					}
					if($ans[4]=='rev_ins')
					{
						$data = array(
								'review_xp' => $ans[0],
								'review_desc' => $ans[3],
								'review_user' => $ans[2],
								'rev_assignee' => $ans[2],
								'upd_user' => $ans[2],
								'tast_stat'=>'Confirmation Pending'
								);
						$this->db->set('review_upd_dt', 'current_timestamp', FALSE);
					}
					// if($ans[4]=='rev_upd')
					// {
						// $data = array(
								// 'review_user' => $ans[2],
								// 'upd_user' => $ans[2],
								// 'tast_stat'=>'Confirmation Pending'
								// );
						// $this->db->set('review_xp','`equiv_xp`',FALSE);
						// $this->db->set('review_upd_dt', 'current_timestamp', FALSE);
					// }
					if($ans[4]=='conf_upd')
					{
						$data = array(
								'conf_user' => $ans[2],
								'upd_user' => $ans[2],
								'tast_stat'=>'Confirmed'
								);
						$this->db->set('conf_upd_dt', 'current_timestamp', FALSE);
					}
					// if($ans[4]=='conf_app')
					// {
						// $data = array(
								// 'conf_user' => $ans[2],
								// 'upd_user' => $ans[2],
								// 'tast_stat'=>'Confirmed'
								// );
						// $this->db->set('total_xp','`review_xp`',FALSE);
						// $this->db->set('conf_upd_dt', 'current_timestamp', FALSE);
					// }
					if($ans[4]=='conf_ins')
					{
						$data = array(
								'total_xp' => $ans[0],
								'final_xp' => $ans[0],
								'conf_user' => $ans[2],
								'upd_user' => $ans[2],
								'tast_stat'=>'Confirmed'
								);
						$this->db->set('conf_upd_dt', 'current_timestamp', FALSE);
					}
					if($ans[4]=='audit_app')
					{
						if(!$ans[5])
							{
								$ans[5]='Review Pending';
							}
						$data = array(
								'audit_user' => $ans[2],
								'upd_user' => $ans[2],
								'tast_stat'=>$ans[5]
								);		
						$this->db->set('audit_upd_dt', 'current_timestamp', FALSE);
						$this->db->set('final_xp','`equiv_xp`',FALSE);
					}
					if($ans[4]=='alloc_upd')
					{
						$data = array(
								'rev_assignee' => $ans[0],
								'assignee_desc' => $ans[3],
								'upd_user' => $ans[2]
								);		
					}
				$this->db->set('upd_dt', 'current_timestamp', FALSE);				
				$where_key=explode(",",$ans[1]);
				$this->db->where_in('response_id', $where_key);
				$this->db->where('status_nm', 'Active');
				$this->db->update('response_mst', $data);
					}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return "success";
		  }
	 }
	 public function iss_approve($ans){
		$this->db->trans_start();
		
		//hist_response_mst
					
		$data = array(
				'issue_chk_flag' => '1',
				'comp_xp' => $ans[0],
				'comp_user' => $ans[2],
				'upd_user' => $ans[2]
				);
				$this->db->set('comp_upd_dt', 'current_timestamp', FALSE);
				$this->db->set('upd_dt', 'current_timestamp', FALSE);
				$where_key=explode(",",$ans[1]);
				$this->db->where_in('response_id', $where_key);
				$this->db->update('response_mst', $data);
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return "success";
		  }
	 }
	 
	 
	 public function leav_upd_rep($ans){
		$this->db->trans_start();
		if($ans[1])
		{
		$data = array(
				'conf_status' => $ans[3],
				'upd_user' => $ans[2]
				);
				$this->db->set('upd_dt', 'current_timestamp', FALSE);
				$where_key=explode(",",$ans[1]);
				$this->db->where_in('comp_track_id', $where_key);
				$this->db->update('comp_track', $data);
		}
			if($ans[4])
			{
				$data1 = array(
				'conf_status' => $ans[3],
				'upd_user' => $ans[2]
				);
				$this->db->set('upd_dt', 'current_timestamp', FALSE);
				$where_key1=explode(",",$ans[4]);
				$this->db->where_in('emp_week_off_id', $where_key1);
				$this->db->update('emp_week_off', $data1);
			}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return "success";
		  }
	 }
	 
	  public function iss_reject($ans){
		 
		 $this->db->trans_start();
		//hist_response_mst
					
		$data = array(
				'issue_chk_flag' => '2',
				'comp_xp' => $ans[0],
				'comp_user' => $ans[2],
				'upd_user' => $ans[2]
				);
				$this->db->set('comp_upd_dt', 'current_timestamp', FALSE);
				$this->db->set('upd_dt', 'current_timestamp', FALSE);
				$where_key=explode(",",$ans[1]);
				$this->db->where_in('response_id', $where_key);
				$this->db->update('response_mst', $data);
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return "success";
		  }
	 }
	 
	 
	 public function insert_resp($ans){
		 
		 $this->db->trans_start();
		 
		 $equiv_xp=($ans[6]*10)/$ans[5];
		 $iss_flag=0;
		 if($ans[8]=='Y')
		 {
			 $iss_flag=1;			 
		 }	 
		 $data_in = array(
				'activity_id_fk' => $ans[0],
				'wu_id_fk' => $ans[1],
				'user_id_fk' => $ans[2],
				'day_val' => $ans[3],
				'task_desc' => $ans[4],
				'std_tgt_val' => $ans[5],
				'work_comp' => $ans[6],
				'issue_flag' => $iss_flag,
				'issue_desc' => $ans[9],
				'iss_cat_id_fk' => (($iss_flag)?$ans[12]:null),
				'equiv_xp' => $equiv_xp,
				'tast_stat' => $ans[7],
				'dept_id_fk' => $ans[10],
				'milestone_id' => $ans[11],
				'status_nm' => "Active",
				'ins_user' => $ans[2]
				);
				$this->db->set('ins_dt', 'current_timestamp', FALSE);
				$result_array =$this->db->insert('response_mst', $data_in);	
				
				if($ans[13])
				{
				$query = 'SELECT HA.hier_act_log_id AS id ,IFNULL(HA.progress,"Not Started") AS progress 	
			FROM hier_act_log HA JOIN (SELECT A.dept_id_fk,A.project_id_fk,A.task_id_fk,A.sub_id_fk,A.job_id_fk,
										H.hierarchy_id 
								FROM activity_mst A 
								JOIN (SELECT hierarchy_id from hierarchy_mst WHERE dept_id_fk='.$ans[10].' 
										AND mid_code="'.$ans[11].'"
										)H
										ON 1=1 
								WHERE activity_id='.$ans[0].' 
								)BA 
						ON BA.dept_id_fk=HA.dept_id_fk AND
							BA.project_id_fk=HA.project_id_fk AND
							BA.task_id_fk=HA.task_id_fk AND
							BA.sub_id_fk=HA.sub_task_id_fk AND
							BA.job_id_fk=HA.job_id_fk AND 
							BA.hierarchy_id=HA.p_hierarchy_id 	
					WHERE HA.dept_id_fk='.$ans[10].' AND FIND_IN_SET('.$ans[2].',HA.alloc_to)>0
					AND HA.status_nm="Active";';
				
				$sqlQuery =$this->db->query($query);
				$resultSet = $sqlQuery->result_array();
				
				$var_id=$resultSet[0]['id'];
				
				
				
				$old_progres=$resultSet[0]['progress'];
				$new_progres=(($ans[13])?$ans[13]:'In Progress');
				//echo $var_id;
					if($var_id)
					{
						$query1 = 'SELECT IFNULL(MIN(R.day_val),"'.$ans[3].'") AS s_dt  ,IFNULL(MAX(R.day_val),"'.$ans[3].'") AS e_dt
			FROM response_mst R 
			JOIN activity_mst A 
			ON R.activity_id_fk=A.activity_id AND A.status_nm="Active" 
			JOIN (SELECT * FROM activity_mst
			WHERE activity_id='.$ans[0].' 
			)BA 
			ON BA.dept_id_fk=R.dept_id_fk AND 
					BA.project_id_fk=A.project_id_fk AND
					BA.task_id_fk=A.task_id_fk AND
					BA.sub_id_fk=A.sub_id_fk AND
					BA.job_id_fk=A.job_id_fk  
					WHERE R.dept_id_fk='.$ans[10].' 
					AND R.milestone_id="'.$ans[11].'"
					AND R.status_nm="Active";';
				
				$sqlQuery1 =$this->db->query($query1);
				$resultSet1 = $sqlQuery1->result_array();
				
				$dd_val_resp=($resultSet1[0]['s_dt'])?($resultSet1[0]['s_dt']):$ans[3];
				$dd_val_resp2='null';
				if($new_progres=='Completed')
					{
					$dd_val_resp2='"'.(($resultSet1[0]['e_dt'])?($resultSet1[0]['e_dt']):$ans[3]).'"';
					}
				
			$sql='UPDATE hier_user_alloc SET progress="'.$new_progres.'",start_dt="'.$dd_val_resp.'",end_dt='.$dd_val_resp2.',upd_user='. $ans[2].',upd_dt=current_timestamp 
						WHERE hier_act_log_id_fk='.$var_id.' AND alloc_user='.$ans[2].' AND status_nm="Active"';
						
						$this->db->query($sql);
						
						$query = 'SELECT `hier_act_log_id_fk`,count(*) AS  cnt ,MIN(start_dt) AS s_dt,MAX(end_dt) AS e_dt,
							SUM(CASE WHEN progress="Completed" THEN 1 ELSE 0 END) AS cnt_comp  
							FROM `hier_user_alloc` 
							WHERE hier_act_log_id_fk='.$var_id.' AND status_nm="Active" 
							GROUP BY hier_act_log_id_fk  ;';
				
				$sqlQuery =$this->db->query($query);
				$resultSet = $sqlQuery->result_array();
				
						// if($old_progres!=$new_progres && $new_progres=='In Progress')
						// {
					// $sql17r='UPDATE hier_act_log HA 						
							// SET HA.progress="'.$new_progres.'",upd_user='. $ans[2].',upd_dt=current_timestamp WHERE HA.hier_act_log_id='.$var_id.';';
					// $this->db->query($sql17r);
						// }
						if($resultSet)
							{
								$asd='null';
						$esd='null';
					$asd=($resultSet[0]['s_dt'])?('"'.$resultSet[0]['s_dt'].'"'):'null';
					if($old_progres!=$new_progres && $new_progres=='Completed' && ($resultSet[0]['cnt']==$resultSet[0]['cnt_comp']))
						{
							$esd=($resultSet[0]['e_dt'])?('"'.$resultSet[0]['e_dt'].'"'):'null';
						}
						if($old_progres!=$new_progres && $new_progres=='Completed' && ($resultSet[0]['cnt']!=$resultSet[0]['cnt_comp']))
						{																		
									$new_progres=$old_progres;								
						}					
						
						$sql17r='UPDATE hier_act_log HA 						
								SET HA.progress="'.$new_progres.'",actual_start_date='.$asd.',actual_end_date='.$esd.' WHERE HA.hier_act_log_id='.$var_id.';';
						$this->db->query($sql17r);
						}
					}
				}
				$this->db->trans_complete();
				
			if($this->db->trans_status() === FALSE)
			  {
				 return false;
			  }
			  else{
				 return "success";
			  }
	 }
	 
	 
	  public function upd_resp($ans){
		   $this->db->trans_start();		   
		    $data = array(
				'update_status' => 'Reject',
				'upd_user' => $ans[3]
				);
				$this->db->set('upd_dt', 'current_timestamp', FALSE);
				$this->db->where('update_status',"Pending");
				$this->db->where('response_id_fk',$ans[0]);
		   $this->db->update('audit_pend_resp', $data);
		   $data_in = array(
				'response_id_fk' => $ans[0],
				'work_comp' => $ans[1],
				'change_desc' => $ans[2],
				'update_status'=>'Pending',
				'status_nm' => "Active",
				'ins_user' => $ans[3]
				);
				$this->db->set('ins_dt', 'current_timestamp', FALSE);
				$this->db->insert('audit_pend_resp', $data_in);							
			$this->db->trans_complete();
			if($this->db->trans_status() === FALSE)
			  {
				 return false;
			  }
			  else{
				 return "success";
			  }
	 }
	 
	 public function getUserRoleInfo()
	{
		$userId = $this->session->userdata('user_id');
		$this->db->join('role_mst', 'role_mst.role_id = user_mst.role_id_fk');
		$this->db->where('user_id', $userId);
		$userModal = $this->db->get('user_mst');
		return $userModal->result_array();
	}

	public function canGiveFeedback()
	{
		$role_id            = $this->session->userdata('role_id');
		//$role = $this->getUserRoleInfo()->role_name;
		if (in_array($role_id, array(1, 2,3, 4))) {
			return true;
		} else {
			return false;
		}
	}
	
	public function  haveAccessworkRequest(){
		//$role = $this->getUserRoleInfo()->role_name;
		$role_id            = $this->session->userdata('role_id');
		if (in_array($role_id, array(1,2))) {
			return true;
		} else {
			return false;
		}
	}

public function getProjectLeaveStatusRecord($compTrackId, $projectId, $status)
	{
		$this->db->where('comp_track_id_fk', $compTrackId);
		$this->db->where('project_id_fk', $projectId);
		$this->db->where('status_nm', "Active");
		return $this->db->get('po_leave_approval')->result_array();
	}

	public function updateLeaveInCompTrack($compTrackId, $status)
	{
		$this->db->where('comp_track_id', $compTrackId);
		if ($this->db->update('comp_track', array('conf_status' => $status))) {
			return true;
		} else {
			return false;
		}
	}

	public function  getUserProjects()
	{
		$userId = $this->session->userdata('user_id');
		$this->db->select('project_id_fk');
		$this->db->where('user_id', $userId);
		return $this->db->get('user_mst')->result_array();
	}

	public function upsertPoLeaveStatus($compTrackId, $projectId, $status)
	{
		$userId = $this->session->userdata('user_id');
        $valo=$this->getProjectLeaveStatusRecord($compTrackId, $projectId, $status);
		if (empty($valo)) {
			$this->db->insert('po_leave_approval', array(
				'project_id_fk' => $projectId,
				'comp_track_id_fk' => $compTrackId,
				'conf_status' => $status,
				'status_nm' => 'Active',
				'ins_user' => $userId,
				'ins_dt' => date('Y-m-d H:i:s')
			));												
		} else {
			$this->db->where('comp_track_id_fk', $compTrackId);
			$this->db->where('project_id_fk', $projectId);
			$this->db->where('status_nm', "Active");
			$this->db->update('po_leave_approval', array(
				'project_id_fk' => $projectId,
				'comp_track_id_fk' => $compTrackId,
				'conf_status' => $status,
				'status_nm' => 'Active',
				'upd_user' => $userId,
				'upd_dt' => date('Y-m-d H:i:s')
			));
		}
	}

	public function getCompLeaveProjects($compTrackId)
	{
		$this->db->where('comp_track_id', $compTrackId);
		$this->db->select('project_ids_fks');
		$leave = $this->db->get('comp_track')->result_array();
		if (!empty($leave)) {
			return  explode(',', $leave[0]['project_ids_fks']);
		} else {
			return null;
		}
	}
	
	public function getAllPoApprovedLeaveProjectsList($compTrackId)
	{
		$this->db->where('comp_track_id_fk', $compTrackId);
		$this->db->where('conf_status', 'Approved');
		$this->db->where('status_nm', 'Active');
		$this->db->select('GROUP_CONCAT(project_id_fk) AS project_id');
		$leave = $this->db->get('po_leave_approval')->result_array();
		if (!empty($leave)) {
			return explode(',', $leave[0]['project_id']);
		} else {
			return null;
		}
	}

	public function getAllPoRejectedLeaveProjectsList($compTrackId)
	{
		$this->db->where('comp_track_id_fk', $compTrackId);
		$this->db->where('conf_status', 'Rejected');
		$this->db->where('status_nm', 'Active');
		$this->db->select('GROUP_CONCAT(project_id_fk) AS project_id');
		$leave = $this->db->get('po_leave_approval')->result_array();
		if (!empty($leave)) {
			return explode(',', $leave[0]['project_id']);
		} else {
			return null;
		}
	}
		public function accessForAllocation($userid){
		$sql='select count(*) as cnt
				from user_permissions
				where user_id_fk='.$userid.' AND status_nm="Active"';
		$sqlQuery=$this->db->query($sql);
		 $resultSet = $sqlQuery->row_array();
			if($resultSet)
			{
				return $resultSet;
			}
			else{
				return false;
			}
	}
	public function userHasAccessToProjectByResponseId($userId, $responseId)
	{
		$projectId = $this->getProjectByResponseId($responseId);
		if (!empty($projectId)) {
			return $this->userHasAccessToProject($userId, $projectId[0]['project_id']);
		}
		return false;
	}

	public function getProjectByResponseId($responseId)
	{
		$this->db->select('pm.*');
		$this->db->from('response_mst rm');
		$this->db->join('activity_mst am', 'am.activity_id = rm.activity_id_fk');
		$this->db->join('project_mst pm', 'pm.project_id = am.project_id_fk');
		$this->db->where('rm.response_id', $responseId);
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}

	// public function userHasAccessToProject($userId, $projectId)
	// {
		// $this->db->select('count(1) AS count');
		// $this->db->from('user_mst');
		// $this->db->where('user_id', $userId);
		// $this->db->where('find_in_set('.$projectId.', project_id_fk)');
		// $sqlQuery = $this->db->get();
		// return $sqlQuery->result_array()[0]['count'] > 0 ? true : false;
	// }
	public function getRoleByName($roleName)
	{
		$this->db->select('*');
		$this->db->from('role_mst');
		$this->db->where('role_name', $roleName);
		return $this->db->get()->row();
	}

	public function getProjectsByNames($projectList)
	{
		$this->db->select('GROUP_CONCAT(project_id) as project_ids');
		$this->db->from('project_mst');
		$this->db->where_in('project_name', $projectList);
		return $this->db->get()->row();
	}

	function getAllAciveUsersEmail(){
		$this->db->select('user_id,user_login_name as email_id');
		$this->db->from('user_mst as um');
		$this->db->where('status_nm','Active');
		return $this->db->get()->result_array();
	}
	public function getUserIdByName($userName)
	{
		$this->db->select('*');
		$this->db->from('user_mst as um');
		$this->db->where('full_name', $userName);
		return $this->db->get()->row();
	}
	
	public function getMultipleUserIdByName($userName)
	{
		$this->db->select('user_id');
		$this->db->from('user_mst as um');
		$this->db->where_in('full_name', $userName);
		$this->db->group_by(array("full_name")); 
		$result = $this->db->get()->result_array();
		
		return $result = $result ? implode(",", array_column($result, 'user_id')) : NULL;
	}

	public function getDepartmentByName($deptName)
	{
		$department = '';
		if (in_array('content', $deptName)) {
			$department .= CONTENT;
		}

		if (in_array('media', $deptName)) {
			$department .= MEDIA;
		}
		return $department;
	}

	public function getUserByEmail($userEmail)
	{
		$this->db->select('*');
		$this->db->from('user_mst as um');
		$this->db->where('user_login_name', $userEmail);
		return $this->db->get()->row();
	}

	public function insertUser($user)
	{
		$this->db->trans_start();
		$this->db->insert('user_mst',$user);
		
		//Add user default week off
		$userId = $this->db->insert_id();
		$loginUserId = $this->session->userdata('user_id');
		$userWeekOff = [
			[
				'user_id_fk' => $userId,
				'week_day' => 5,
				'start_dt' => date("Y-m-d"),
				'end_dt' => date("Y-m-d"),
				'conf_status' => "Approved",
				'status_nm' => "Active",
				'ins_user' => $loginUserId,
				'ins_dt' => date("Y-m-d H:i:s"),
			],
			[
				'user_id_fk' => $userId,
				'week_day' => 6,
				'start_dt' => date("Y-m-d"),
				'end_dt' => date("Y-m-d"),
				'conf_status' => "Approved",
				'status_nm' => "Active",
				'ins_user' => $loginUserId,
				'ins_dt' => date("Y-m-d H:i:s"),
			]
		];
		$this->db->insert_batch('emp_week_off', $userWeekOff); 
		
		$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
							  {
								 return false;
							  }
							  else{
								 return true;
							  }
	}

	public function insertLogistic($logistic)
	{
		$this->db->trans_start();
		$this->db->insert_batch('user_logistic',$logistic);
		$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
							  {
								 return false;
							  }
							  else{
								 return true;
							  }
	}
	public function getProjectOwner($projectId)
	{
		$projectId = explode(',', $projectId);
		$this->db->select('GROUP_CONCAT(user_login_name) as owners');
		$this->db->from('user_mst');
		$this->db->where('role_id_fk', '4');
		foreach ($projectId as $key => $project) {
			if ($key == 0) {
				$projectCondition = '(FIND_IN_SET("'.$project.'", project_id_fk)';
			} else {
				$projectCondition .= ' OR FIND_IN_SET("'.$project.'", project_id_fk)';
			}
		}
		$projectCondition .= ')';
		$this->db->where($projectCondition);
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}

	public function getReportingManager($userId)
	{
		$this->db->select('GROUP_CONCAT(um.user_login_name) as manager');
		$this->db->from('user_mst as um1');
		$this->db->join('user_mst as um', 'um.user_id = um1.manager_id');
		$this->db->where('um1.user_id', $userId);
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}
	
	public function getUserInfo($userId)
	{
		$this->db->where('user_id', $userId);
		$userModal = $this->db->get('user_mst');
		return $userModal->result_array();
	}
	
	public function getUserActivityByResponseId($responseId, $userId)
	{
		$this->db->select('HR.hier_act_log_id as alloc_activity_id,
			HR.actual_start_date,
			HR.actual_end_date,
			HR.progress as activity_progress,
			HU.hier_alloc_id as alloc_user_id,
			HU.start_dt as user_start_date,
			HU.end_dt as user_end_date,
			HU.progress as user_progress'
		);
		$this->db->from('activity_mst AM');
		$this->db->join('hier_act_log HR', 'HR.project_id_fk = AM.project_id_fk
			AND HR.task_id_fk = AM.task_id_fk
			AND HR.sub_task_id_fk = AM.sub_id_fk
			AND HR.job_id_fk = AM.job_id_fk
			AND HR.dept_id_fk = AM.dept_id_fk'
		);
		$this->db->join('hierarchy_mst HM', 'HM.hierarchy_id = HR.p_hierarchy_id');
		$this->db->join('hier_user_alloc HU', 'HU.hier_act_log_id_fk = HR.hier_act_log_id');
		$this->db->join('response_mst RM', 'RM.activity_id_fk = AM.activity_id AND RM.milestone_id = HM.mid_code');
		$this->db->where('RM.response_id',$responseId);
		$this->db->where('HU.status_nm', 'Active');
		$this->db->where(' find_in_set('.$userId.', HU.alloc_user)<>0 ', null,false);
		$sqlQuery = $this->db->get();
		return $sqlQuery->row();

	}

	public function getUserActivity($activityId, $midCode, $userId)
	{
		$this->db->select('HR.hier_act_log_id as alloc_activity_id,
			HR.actual_start_date,
			HR.actual_end_date,
			HR.progress as activity_progress,
			HU.hier_alloc_id as alloc_user_id,
			HU.start_dt as user_start_date,
			HU.end_dt as user_end_date,
			HU.progress as user_progress'
		);
		$this->db->from('activity_mst AM');
		$this->db->join('hier_act_log HR', 'HR.project_id_fk = AM.project_id_fk
			AND HR.task_id_fk = AM.task_id_fk
			AND HR.sub_task_id_fk = AM.sub_id_fk
			AND HR.job_id_fk = AM.job_id_fk
			AND HR.dept_id_fk = AM.dept_id_fk'
		);
		$this->db->join('hierarchy_mst HM', 'HM.hierarchy_id = HR.p_hierarchy_id');
		$this->db->join('hier_user_alloc HU', 'HU.hier_act_log_id_fk = HR.hier_act_log_id');
		$this->db->where('AM.activity_id', $activityId);
		$this->db->where('HM.mid_code',$midCode);
		$this->db->where('HU.status_nm', 'Active');
		$this->db->where(' find_in_set('.$userId.', HU.alloc_user)<>0 ', null,false);
		$sqlQuery = $this->db->get();
		return $sqlQuery->row();

	}



	public function getAllUserProgressOnActivity($activityId)
	{
		$this->db->select('GROUP_CONCAT(HU.progress) as progrerss_list');
		$this->db->from('hier_act_log HR');
		$this->db->join('hier_user_alloc HU', 'HU.hier_act_log_id_fk = HR.hier_act_log_id');
		$this->db->where('HR.hier_act_log_id', $activityId);
		$this->db->where('HU.status_nm', 'Active');
		$sqlQuery = $this->db->get();
		return !empty($sqlQuery->row()->progrerss_list) ?
		explode(',', $sqlQuery->row()->progrerss_list) : [];
	}
}
?>