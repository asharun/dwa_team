<?php
class Alert_model extends CI_model{
	public $tx_cn='';	
	
	public function __construct(){
      $this->tx_cn='"'.$this->config->item('tx_cn').'"';	
	  $this->load->library("PHPMailer_Library");
   }
    public function getWeeklyImpactLossData($deptId)
   {

	   $newDateTime = new DateTime(null, new DateTimeZone('Asia/Kolkata'));
	   $newDate = $newDateTime->format('Y-m-d');
	   // start date
	   $beforeTwoWeeksStartDate = date('Y-m-d', strtotime($newDate . ' -' . (14) . ' days'));
	   // end date
	   $beforeTwoWeeksEndDate = date('Y-m-d', strtotime($newDate . ' -' . (7) . ' days'));
	   $this->db->select('P.project_id, P.project_name, ROUND(IFNULL(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)),0),2) AS proj_xp');
       $this->db->from('response_mst R');
       $this->db->join('activity_mst A', 'A.activity_id = R.activity_id_fk');
       $this->db->join('project_mst P', 'A.project_id_fk = P.project_id');
	   $this->db->where('R.day_val >=', $beforeTwoWeeksStartDate);
	   $this->db->where('R.day_val <=', $beforeTwoWeeksEndDate);
       $this->db->where('R.status_nm', 'Active');
	   $this->db->where('R.dept_id_fk', $deptId);
       $this->db->where_in('R.tast_stat', array('Audit Pending','Review Pending','Confirmation Pending'));
       $this->db->group_by('P.project_id,P.project_name');
	   $sqlQuery = $this->db->get();
	   return $sqlQuery->result_array();

   }

   public function getHighestXpDaily($deptId)
   {
	   $newDateTime = new DateTime(null, new DateTimeZone('Asia/Kolkata'));
	   $todayDate = $newDateTime->format('Y-m-d');

	   $this->db->select('um.full_name, ROUND(IFNULL(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)),0),2) AS proj_xp');
	   $this->db->from('response_mst R');
	   $this->db->join('user_mst um', 'um.user_id = R.user_id_fk');
	   $this->db->where('R.day_val >=', $todayDate);
	   $this->db->where('R.day_val <=', $todayDate);
	   $this->db->where('R.dept_id_fk', $deptId);
	   $this->db->group_by('user_id_fk');
	   $this->db->having('ROUND(IFNULL(SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)),0),2) >= 15');
	   $sqlQuery = $this->db->get();
	   return $sqlQuery->result_array();
   }

	public function getWeeklyGradingPending($deptId)
	{
		$newDateTime = new DateTime(null, new DateTimeZone('Asia/Kolkata'));
		$newDate = $newDateTime->format('Y-m-d');
		// start date
		$beforeTwoWeeksStartDate = date('Y-m-d', strtotime($newDate . ' -' . (14) . ' days'));
		// end date
		$beforeTwoWeeksEndDate = date('Y-m-d', strtotime($newDate . ' -' . (7) . ' days'));
		$this->db->select('pm.project_name');
		$this->db->from('response_mst rm');
		$this->db->join('activity_mst am', 'am.activity_id = rm.activity_id_fk');
		$this->db->join('project_mst pm', 'pm.project_id = am.project_id_fk');
		$this->db->join('user_feedbacks uf', 'uf.project_id_fk = pm.project_id', 'left outer');
		$this->db->where('rm.status_nm', 'Active');
		$this->db->where('rm.dept_id_fk', $deptId);
		$this->db->where('rm.day_val >=', $beforeTwoWeeksStartDate);
		$this->db->where('rm.day_val <=', $beforeTwoWeeksEndDate);
		$this->db->where('uf.project_id_fk IS NULL');
		$this->db->group_by('am.project_id_fk');

		$sqlQuery = $this->db->get();
	  	return $sqlQuery->result_array();
	}

	public function getWeeklyStdTargetChanges()
	{

		$newDateTime = new DateTime(null, new DateTimeZone('Asia/Kolkata'));
		$newDate = $newDateTime->format('Y-m-d');
		// start date
		$startDate = date('Y-m-d', strtotime($newDate . ' -' . (6) . ' days'));
		// end date
		$endDate = date('Y-m-d', strtotime($newDate));
		$query = "SELECT
    A.activity_id,
    (
        CASE WHEN A.dept_id_fk = 1 THEN 'Content' ELSE 'Media'
    END
) AS dept,
P.project_name,
A.activity_name,
W.wu_name,
(
    CASE WHEN S.end_dt IS NULL THEN NULL ELSE S.change_desc
END
) AS prev_change_desc,
S.tgt_val,
U.full_name AS ins_user,
DATE_FORMAT(S.ins_dt, '%d/%m/%y %k:%i:%s') AS ins_dt,
DATE_FORMAT(S.start_dt, '%d/%m/%y') AS start_date,
DATE_FORMAT(S.end_dt, '%d/%m/%y') AS end_date,
U1.full_name AS upd_user,
DATE_FORMAT(S.upd_dt, '%d/%m/%y %k:%i:%s') AS upd_dt
FROM
    `std_tgt_mst` S
INNER JOIN `activity_mst` A ON
    S.`activity_id_fk` = A.`activity_id` AND A.`status_nm` = 'Active'
INNER JOIN `work_unit_mst` W ON
    W.`wu_id` = A.`wu_id_fk` AND W.`status_nm` = 'Active'
INNER JOIN `user_mst` U ON
    U.`user_id` = S.`ins_user`
INNER JOIN `project_mst` P ON
    P.`project_id` = A.`project_id_fk` AND P.`status_nm` = 'Active'
LEFT JOIN `user_mst` U1 ON
    U1.`user_id` = S.`upd_user`
WHERE
    S.`status_nm` = 'Active' AND A.activity_id IN(
    SELECT
        activity_id_fk
    FROM
        std_tgt_mst
    WHERE
        activity_id_fk IN(
        SELECT
            activity_id_fk
        FROM
            std_tgt_mst
        WHERE
            DATE(start_dt) BETWEEN '$startDate' AND '$endDate' OR DATE(end_dt) BETWEEN '$startDate' AND '$endDate'
    ) AND status_nm = 'Active' AND start_dt <= IFNULL(end_dt, start_dt) AND start_dt <= '$endDate'
GROUP BY
    activity_id_fk
HAVING
    COUNT(*) > 1
) AND S.start_dt <= IFNULL(S.end_dt, S.start_dt)
ORDER BY
    A.`activity_name` ASC,
    S.start_dt,
    S.end_dt";

	$sqlQuery =$this->db->query($query);
	return $sqlQuery->result_array();
	}

	public function notifyUserDontHaveAccessToProject($userData, $projectData)
	{
		define('ROLE_ID', '2');
		$template = $this->getUserDontHaveAccessToProjectTemplate($userData['full_name'], $projectData['project_name']);
		$seniorManager = $this->getSeniorManager(0, ROLE_ID);

        $mail = $this->phpmailer_library->load();
        $mail->setFrom('noreply@byjusdwa.com');

        foreach($seniorManager AS $address) {
            $mail->addAddress($address);
        }

		$mail->addCC($userData['user_login_name']);
		$mail->addBCC('vivek.kumar1@byjus.com');

        $mail->isHTML(true);
        $mail->Body = $template;
        if(!$mail->Send()) {
            $sendError = $mail->ErrorInfo;
        } else{
            echo 'Message has been sent';
        }
	}

	public function getUserDontHaveAccessToProjectTemplate($userName, $projectName)
	{
		$message = 'Hi Team, <br> Here is a Information regarding user dont have access to project while being allocated for specfic project activity from allocation secion <br>';
		$message .= '<table><tr><th>User Name</th><th>ProjectName</th></tr>';
		$message .= '<tr><td>'.$userName.'</td><td>'.$projectName.'</td></tr>';
		return $message;
	}
	
	 public function check_hol($t_date1){						
						$this->db->select('  COUNT(*) AS cnt ',FALSE);
						$this->db->from('nation_hols N');						
						 $this->db->where('N.status_nm', 'Active');
						 $this->db->where('N.day_val', $t_date1);
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 
		public function update_streak_mm($t_date){
			 $this->db->trans_start();		   
			 
			 $sql3 = 'CREATE TABLE `streak_table`(
								user_id_fk INT(11) DEFAULT NULL,
								streak INT(11) DEFAULT NULL
							) ENGINE = InnoDB; ';
					$this->db->query($sql3);
			 
			 $sql4 = 'INSERT INTO streak_table(user_id_fk,streak)
SELECT JJ.user_id,IF(IFNULL(JJ.r_user,0)=0,JJ.prev_val,-1*JJ.prev_val) AS final_str
FROM 
(
SELECT KK.user_id,KK.selected_date,KK.r_user,	
	@prevv:=IF(@prev_us = KK.user_id,IF(IFNULL(KK.r_user,0)<>IFNULL(@prev_r_us,0),@num,0),1) AS prev_val,
	@num:=IF(@prev_us = KK.user_id AND IFNULL(KK.r_user,0)=IFNULL(@prev_r_us,0),@num+1,1) AS rownum,
	IF(@prev_us = KK.user_id AND IFNULL(KK.r_user,0)<>IFNULL(@prev_r_us,0),"Break","") AS break,	
	@prev_b_nm:=IF(@prev_us = KK.user_id , IF(IFNULL(KK.r_user,0)<>IFNULL(@prev_r_us,0),@prev_b_nm+1,@prev_b_nm),0) AS nm ,
	@prev_us := KK.user_id,
	@prev_r_us :=KK.r_user AS prev_res_us 
    FROM (
SELECT 
    Main.selected_date,U.user_id,C.user_id_fk AS r_user 
FROM
    (
SELECT V.selected_date
FROM
    (
    SELECT
        ADDDATE("2018-01-01",t2.i * 100 + t1.i * 10 + t0.i) AS selected_date
    FROM
        (SELECT  day_num AS i  FROM calendar cal WHERE cal.day_num <= 9) t0,
		(SELECT  day_num AS i  FROM calendar cal WHERE cal.day_num <= 9) t1,
		(SELECT  day_num AS i  FROM calendar cal WHERE cal.day_num <= 9) t2
) V
WHERE
    V.selected_date BETWEEN "2018-08-06" AND "'.$t_date.'" 
	AND V.selected_date NOT IN (SELECT hol.day_val from nation_hols hol WHERE hol.status_nm="Active") 
)Main 
INNER JOIN user_mst U 
ON U.status_nm="Active" 
INNER JOIN (select @prev_us:=0,@prev_r_us:=0,@num:=0,@prev_b_nm:=0,@prevv:=0)GG
ON 1=1 
  LEFT JOIN (
  SELECT B.user_id_fk,B.day_val 
   FROM (SELECT R.day_val,R.user_id_fk,MIN(DATE(CONVERT_TZ(R.ins_dt,"+00:00",'.$this->tx_cn.'))) AS min_ins_dt
	FROM response_mst R 
	WHERE R.status_nm="Active"
	GROUP BY R.day_val,R.user_id_fk)B 
	WHERE B.day_val>=B.min_ins_dt
	) C ON C.day_val=Main.selected_date AND U.user_id=C.user_id_fk 	
	LEFT JOIN (
		SELECT LT.user_id_fk,LT.start_date, LT.end_date,IFNULL(LT.is_comp,0) = 0 AS is_comp
		FROM comp_track LT 
		WHERE LT.status_nm="Active" AND LT.conf_status="Approved"  AND IFNULL(LT.is_half_day,0)<>1 
	) LT_1
	ON LT_1.user_id_fk=U.user_id 
	AND ((Main.selected_date BETWEEN LT_1.start_date AND LT_1.end_date AND LT_1.is_comp = 0) 
	OR (LT_1.start_date = Main.selected_date AND LT_1.is_comp = 1))
	LEFT JOIN emp_week_off WO ON
	   WO.user_id_fk = U.user_id AND WO.week_day = WEEKDAY(Main.selected_date)	
	   AND Main.selected_date BETWEEN WO.start_dt AND IFNULL(WO.end_dt, Main.selected_date) AND WO.status_nm = "Active" 
	   AND WO.conf_status="Approved" 
	WHERE C.user_id_fk  IS NOT NULL OR (C.user_id_fk  IS NULL AND LT_1.user_id_fk IS NULL AND WO.user_id_fk IS NULL)
ORDER BY  U.user_id,Main.selected_date DESC	
)KK 
)JJ 
WHERE JJ.nm=1 AND JJ.break="Break"; ';
					$this->db->query($sql4);
					
					$sql6 = "UPDATE user_mst A INNER JOIN streak_table P
							ON P.user_id_fk=A.user_id 
						SET A.streak = P.streak;";
					$this->db->query($sql6);
					
					
					$sql5 = 'DROP TABLE `streak_table`; ';
					$this->db->query($sql5);
					
			$this->db->trans_complete();
			if($this->db->trans_status() === FALSE)
			  {
				 return false;
			  }
			  else{
				 return "success";
			  }
	 }
	 
	 
	 public function update_user(){
		 $this->db->trans_start();		   
		    $data = array(
				'mail_flag' => '0'
				);				
		   $this->db->update('user_mst', $data);
		   $this->db->trans_complete();
			if($this->db->trans_status() === FALSE)
			  {
				 return false;
			  }
			  else{
				 return "success";
			  }
	 }
	 
	  public function update_user_flag($u_id){
		 $this->db->trans_start();		   
		    $data = array(
				'mail_flag' => '1'
				);			
			$this->db->where_in('user_id',$u_id);				
		   $this->db->update('user_mst', $data);
		   $this->db->trans_complete();
			if($this->db->trans_status() === FALSE)
			  {
				 return false;
			  }
			  else{
				 return "success";
			  }
	 }
	 
	 public function alert_1($t_date1,$u_ids,$limit){
						$this->db->select('  DISTINCT U.user_id,U.full_name,U.user_login_name AS email ',FALSE);
						$this->db->from('user_mst U');
						$this->db->join('response_mst R', ' R.user_id_fk=U.user_id AND R.status_nm="Active" AND R.day_val="'.$t_date1.'"','left');
						$this->db->join('comp_track LT', 'LT.user_id_fk=U.user_id AND IFNULL(LT.is_comp,0)=0 AND IFNULL(LT.is_half_day,0)<>1 AND "'.$t_date1.'" >= LT.start_date AND  "'.$t_date1.'" <= LT.end_date AND LT.status_nm="Active" AND LT.conf_status="Approved" ','left');
						$this->db->join('emp_week_off WO', 'WO.user_id_fk=U.user_id AND WO.week_day=WEEKDAY("'.$t_date1.'") AND ("'.$t_date1.'" >= WO.start_dt AND "'.$t_date1.'" <=IFNULL(WO.end_dt,"'.$t_date1.'")) AND WO.status_nm="Active" AND WO.conf_status="Approved" ','left');
						$this->db->join('comp_track LT_comp', 'LT_comp.user_id_fk=U.user_id AND LT_comp.is_comp =1  AND LT_comp.end_date="'.$t_date1.'" AND LT_comp.status_nm="Active" AND LT_comp.conf_status="Approved" ','left');
						// $this->db->where(' ( IFNULL(U.dept_id_fk,0)=0 OR find_in_set(1,U.dept_id_fk)<>0 )', null,false);
						 $this->db->where('U.status_nm', 'Active');
						 $this->db->where('U.user_id NOT IN ('.$u_ids.')', null, false);
						 $this->db->where(' R.response_id IS NULL ', null, false);
						 $this->db->where(' LT.comp_track_id IS NULL ', null, false);
						 $this->db->where('U.mail_flag=0',null,false);
						 $this->db->where(' (WO.emp_week_off_id IS NULL OR (WO.emp_week_off_id IS NOT NULL AND LT_comp.comp_track_id IS NOT NULL)) ', null, false);
						  $this->db->order_by('U.full_name', 'ASC');
						 $this->db->limit($limit);
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 public function l_mail($ids){
						$this->db->select(' U.user_id,U.full_name,U.user_login_name AS us_mail,UM.full_name AS man_name,UM.user_login_name AS man_mail,
						L.leave_type_name,(CASE WHEN LT.is_comp=1 THEN "Comp off On|Worked On" ELSE "Start Date|End Date" END) AS date_name,
						DATE_FORMAT(LT.start_date , "%d-%b-%Y") AS st_dt,
						DATE_FORMAT(LT.end_date , "%d-%b-%Y") AS en_dt,
						(CASE WHEN LT.is_comp=1 THEN "" ELSE DATEDIFF(LT.end_date,LT.start_date)+1 END)AS num_days,
						LT.reason,LT.is_comp,LT.conf_status,
						(CASE WHEN LT.is_half_day=1 THEN "Half Day" ELSE "" END) AS h_day',FALSE);
						$this->db->from('comp_track LT');
						$this->db->join('leave_type_mst L', ' L.l_type_id=LT.leave_type AND L.status_nm="Active"','inner');
						$this->db->join('user_mst U', 'LT.user_id_fk=U.user_id ','inner');						
						$this->db->join('user_mst UM', ' UM.user_id=U.manager_id AND UM.status_nm="Active" ','inner');
						 $this->db->where('LT.comp_track_id IN ('.$ids.')', null, false);
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	  public function l_mail_week($ids){
						$this->db->select(' U.user_id,U.full_name,U.user_login_name AS us_mail,UM.full_name AS man_name,
						UM.user_login_name AS man_mail,
						"Week Off" AS leave_type_name,
						("Start Date|End Date") AS date_name,
						DATE_FORMAT(LT.start_dt , "%d-%b-%Y") AS st_dt,
						DATE_FORMAT(LT.end_dt , "%d-%b-%Y") AS en_dt,
						LT.week_day AS num_days,
						"" AS reason,LT.conf_status	',FALSE);
						$this->db->from('emp_week_off LT');						
						$this->db->join('user_mst U', 'LT.user_id_fk=U.user_id ','inner');						
						$this->db->join('user_mst UM', ' UM.user_id=U.manager_id AND UM.status_nm="Active" ','inner');
						 $this->db->where('LT.emp_week_off_id IN ('.$ids.')', null, false);
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 public function alert_2($t_date1,$u_ids){
		 $d1 = date('Y-m-d', strtotime($t_date1.' -2 days'));
						$sql='SELECT DISTINCT CTE.project_name,U.full_name,U.user_login_name AS email
									 FROM 
									user_mst U 
									LEFT JOIN response_mst R 
									ON R.user_id_fk=U.user_id
									AND R.status_nm="Active" AND R.day_val="'.$t_date1.'" 
									INNER JOIN (SELECT project_id_fk AS project_id,project_name,user_id_fk 
																				FROM (
																						SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
																							@type := tab1.user_id_fk AS dummy
																						FROM
																							(
																							SELECT
																								A.project_id_fk,
																								P1.project_name,
																								R.user_id_fk,
																								ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp 
																							FROM
																								`response_mst` `R` 
																								INNER JOIN (SELECT @num := 0,    @type := "") B
																								ON 1=1
																							INNER JOIN `activity_mst` `A` ON
																								`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active" 
																							INNER JOIN `project_mst` `P1` 
																							ON `P1`.`project_id`=`A`.`project_id_fk` 
																							WHERE
																								R.day_val BETWEEN "'.$d1.'" AND "'.$t_date1.'" AND `R`.`status_nm` = "Active"
																							GROUP BY `R`.`user_id_fk`,A.project_id_fk
																							ORDER BY `R`.`user_id_fk`,equiv_xp DESC,P1.project_name
																						) tab1
																						GROUP BY    user_id_fk,project_id_fk,project_name,equiv_xp 
																						HAVING row_number = 1)E 
																						) CTE ON 
																					CTE.user_id_fk=U.user_id  
									LEFT JOIN comp_track LT 
									ON LT.user_id_fk=U.user_id AND "'.$t_date1.'" >=LT.start_date
									AND "'.$t_date1.'" <=LT.end_date 
									AND IFNULL(LT.is_comp,0)=0 AND IFNULL(LT.is_half_day,0)<>1  
									AND LT.status_nm="Active" 
									AND LT.conf_status="Approved"
									LEFT JOIN emp_week_off WO 
									ON WO.user_id_fk=U.user_id AND WO.week_day=WEEKDAY("'.$t_date1.'")
									AND ("'.$t_date1.'" BETWEEN WO.start_dt AND IFNULL(WO.end_dt,"'.$t_date1.'"))
									AND WO.status_nm="Active" 
									AND WO.conf_status="Approved" 
									LEFT JOIN comp_track LT_comp 
									ON LT_comp.user_id_fk=U.user_id AND LT_comp.end_date="'.$t_date1.'" 
									AND LT_comp.is_comp=1 
									AND LT_comp.status_nm="Active" 
									AND LT_comp.conf_status="Approved" 
									WHERE U.status_nm="Active" 
									AND U.user_id NOT IN ('.$u_ids.') 
									AND R.response_id IS NULL 
									AND LT.comp_track_id IS NULL AND 
									(WO.emp_week_off_id IS NULL OR (WO.emp_week_off_id IS NOT NULL AND LT_comp.comp_track_id IS NOT NULL))  ;';
				$sqlQuery =$this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	  public function alert_3($t_date1,$u_ids){
		 $d2 = date('Y-m-d', strtotime($t_date1.' -3 days'));
		 $d1 = date('Y-m-d', strtotime($d2.' -2 days'));
		 
		 $day_w = date('w',strtotime($d2));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d', strtotime($d2.' -'.($day_w-1).' days'));
		
		$sql=' 	SELECT DISTINCT U.user_id,U.user_login_name AS email,U.full_name ,R.m_val,CTE.project_id,
				CTE.project_name  
			  FROM 
			  user_mst U 
			  LEFT JOIN (SELECT user_id_fk,max(day_val) AS m_val 
							from response_mst R 
						 WHERE R.status_nm="Active" AND R.day_val<="'.$t_date1.'" 
						 GROUP BY user_id_fk ) R 
			  ON U.user_id=R.user_id_fk 
			  LEFT JOIN (SELECT project_id_fk AS project_id,project_name,user_id_fk 
														FROM (
																SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
																	@type := tab1.user_id_fk AS dummy
																FROM
																	(
																	SELECT
																		A.project_id_fk,
																		P1.project_name,
																		R.user_id_fk,
																		ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp 
																	FROM
																		`response_mst` `R` 
																		INNER JOIN (SELECT @num := 0,    @type := "") B
																		ON 1=1
																	INNER JOIN `activity_mst` `A` ON
																		`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active" 
																	INNER JOIN `project_mst` `P1` 
																	ON `P1`.`project_id`=`A`.`project_id_fk` 
																	WHERE
																		R.day_val BETWEEN "'.$d1.'"  AND "'.$d2.'"  AND `R`.`status_nm` = "Active"
																	GROUP BY `R`.`user_id_fk`,A.project_id_fk
																	ORDER BY `R`.`user_id_fk`,equiv_xp DESC,P1.project_name
																) tab1
																GROUP BY    user_id_fk,project_id_fk,project_name,equiv_xp 
																HAVING row_number = 1)E 
																) CTE ON 
															CTE.user_id_fk=U.user_id  
									LEFT JOIN comp_track LT 
									ON LT.user_id_fk=U.user_id AND 
									(LT.start_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" 
									OR  LT.end_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" ) 
									AND IFNULL(LT.is_comp,0)=0 AND IFNULL(LT.is_half_day,0)<>1  
									AND LT.status_nm="Active" 
									AND LT.conf_status="Approved"
									LEFT JOIN emp_week_off WO 
									ON WO.user_id_fk=U.user_id 
									AND DATE_ADD("'.$week_start.'", INTERVAL WO.week_day DAY) BETWEEN ("'.$d2.'") AND ("'.$t_date1.'") 
									AND (("'.$d2.'" BETWEEN WO.start_dt AND IFNULL(WO.end_dt,"'.$d2.'"))
									OR ("'.$t_date1.'" BETWEEN WO.start_dt AND IFNULL(WO.end_dt,"'.$t_date1.'")))
									AND WO.status_nm="Active" 
									AND WO.conf_status="Approved" 
									LEFT JOIN comp_track LT_comp 
									ON LT_comp.user_id_fk=U.user_id AND 
									 LT.end_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" 
									AND LT_comp.is_comp=1 
									AND LT_comp.status_nm="Active" 
									AND LT_comp.conf_status="Approved" 
									 WHERE U.status_nm="Active" AND IFNULL(R.m_val,"'.$d2.'" )<="'.$d2.'"  
									AND U.user_id NOT IN ('.$u_ids.') 
									AND LT.comp_track_id IS NULL AND 
									(WO.emp_week_off_id IS NULL OR (WO.emp_week_off_id IS NOT NULL AND LT_comp.comp_track_id IS NOT NULL))  ;';
				$sqlQuery =$this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	  public function alert_3_sm($t_date1,$u_ids){
		 $d2 = date('Y-m-d', strtotime($t_date1.' -3 days'));
		 $d1 = date('Y-m-d', strtotime($d2.' -2 days'));
		 
		 $day_w = date('w',strtotime($d2));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d', strtotime($d2.' -'.($day_w-1).' days'));
		
		$sql=' 	SELECT DISTINCT IFNULL(U.dept_id_fk,0) AS dept_id,U.user_id,U.user_login_name AS email,U.full_name ,R.m_val 
			  FROM 
			  user_mst U 
			  LEFT JOIN (SELECT user_id_fk,max(day_val) AS m_val 
							from response_mst R 
						 WHERE R.status_nm="Active" AND R.day_val<="'.$t_date1.'" 
						 GROUP BY user_id_fk ) R 
			  ON U.user_id=R.user_id_fk 	
			LEFT JOIN comp_track LT 
			ON LT.user_id_fk=U.user_id AND 
			(LT.start_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" 
			OR  LT.end_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" ) 
			AND IFNULL(LT.is_comp,0)=0 AND IFNULL(LT.is_half_day,0)<>1  
			AND LT.status_nm="Active" 
			AND LT.conf_status="Approved"
			LEFT JOIN emp_week_off WO 
			ON WO.user_id_fk=U.user_id 
			AND DATE_ADD("'.$week_start.'", INTERVAL WO.week_day DAY) BETWEEN ("'.$d2.'") AND ("'.$t_date1.'") 
			AND (("'.$d2.'" BETWEEN WO.start_dt AND IFNULL(WO.end_dt,"'.$d2.'"))
			OR ("'.$t_date1.'" BETWEEN WO.start_dt AND IFNULL(WO.end_dt,"'.$t_date1.'")))
			AND WO.status_nm="Active" 
			AND WO.conf_status="Approved" 
			LEFT JOIN comp_track LT_comp 
			ON LT_comp.user_id_fk=U.user_id AND 
			 LT.end_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" 
			AND LT_comp.is_comp=1 
			AND LT_comp.status_nm="Active" 
			AND LT_comp.conf_status="Approved" 
			 WHERE U.status_nm="Active" AND IFNULL(R.m_val,"'.$d2.'" )<="'.$d2.'"  
			AND U.user_id NOT IN ('.$u_ids.') 
			AND LT.comp_track_id IS NULL AND 
			(WO.emp_week_off_id IS NULL OR (WO.emp_week_off_id IS NOT NULL AND LT_comp.comp_track_id IS NOT NULL))  ;';
				$sqlQuery =$this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 public function alert_3_rm($t_date1,$u_ids){
		 $d2 = date('Y-m-d', strtotime($t_date1.' -3 days'));
		 $d1 = date('Y-m-d', strtotime($d2.' -2 days'));
		 
		 $day_w = date('w',strtotime($d2));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d', strtotime($d2.' -'.($day_w-1).' days'));
		
		$sql=' 	SELECT DISTINCT U.user_id,U.user_login_name AS email,U.full_name ,R.m_val,
		UM.full_name AS man_name,UM.user_login_name AS man_mail,UM.user_id AS man_id  
			  FROM 
			  user_mst U 
			  INNER JOIN user_mst UM 
			  ON UM.user_id=U.manager_id AND UM.status_nm="Active" 
			  LEFT JOIN (SELECT user_id_fk,max(day_val) AS m_val 
							from response_mst R 
						 WHERE R.status_nm="Active" AND R.day_val<="'.$t_date1.'" 
						 GROUP BY user_id_fk ) R 
			  ON U.user_id=R.user_id_fk 			  
		   LEFT JOIN comp_track LT 
			ON LT.user_id_fk=U.user_id AND 
			(LT.start_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" 
			OR  LT.end_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" ) 
			AND IFNULL(LT.is_comp,0)=0 AND IFNULL(LT.is_half_day,0)<>1  
			AND LT.status_nm="Active" 
			AND LT.conf_status="Approved"
			LEFT JOIN emp_week_off WO 
			ON WO.user_id_fk=U.user_id AND 
			DATE_ADD("'.$week_start.'", INTERVAL WO.week_day DAY) BETWEEN ("'.$d2.'") AND ("'.$t_date1.'")
			AND (("'.$d2.'" BETWEEN WO.start_dt AND IFNULL(WO.end_dt,"'.$d2.'"))
			OR ("'.$t_date1.'" BETWEEN WO.start_dt AND IFNULL(WO.end_dt,"'.$t_date1.'")))
			AND WO.status_nm="Active" 
			AND WO.conf_status="Approved" 
			LEFT JOIN comp_track LT_comp 
			ON LT_comp.user_id_fk=U.user_id AND 
			 LT.end_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" 
			AND LT_comp.is_comp=1 
			AND LT_comp.status_nm="Active" 
			AND LT_comp.conf_status="Approved" 
			 WHERE U.status_nm="Active" AND IFNULL(R.m_val,"'.$d2.'" )<="'.$d2.'"  
			AND U.user_id NOT IN ('.$u_ids.') 
			AND LT.comp_track_id IS NULL AND 
			(WO.emp_week_off_id IS NULL OR (WO.emp_week_off_id IS NOT NULL AND LT_comp.comp_track_id IS NOT NULL))  ;';
				$sqlQuery =$this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 public function alert_4($t_date1,$u_ids){
		 $d2 = date('Y-m-d', strtotime($t_date1.' -7 days'));
		 
		 $day_w = date('w',strtotime($d2));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d', strtotime($d2.' -'.($day_w-1).' days'));
						$sql=' SELECT U.user_id,U.user_login_name AS email,IFNULL(U.dept_id_fk,0) AS dept_id,U.full_name ,R.m_val 
								  FROM 
								  user_mst U 
								  LEFT JOIN user_mst UM 
								  ON UM.user_id=U.manager_id AND UM.status_nm="Active"
								  LEFT JOIN (SELECT user_id_fk,max(day_val) AS m_val 
												from response_mst R 
											 WHERE R.status_nm="Active" AND R.day_val<="'.$t_date1.'" 
											 GROUP BY user_id_fk ) R 
								  ON U.user_id=R.user_id_fk 
								  LEFT JOIN comp_track LT 
									ON LT.user_id_fk=U.user_id AND 
									(LT.start_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" 
									OR  LT.end_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" ) 
									AND IFNULL(LT.is_comp,0)=0 AND IFNULL(LT.is_half_day,0)<>1  
									AND LT.status_nm="Active" 
									AND LT.conf_status="Approved"
									LEFT JOIN emp_week_off WO 
									ON WO.user_id_fk=U.user_id 
									AND DATE_ADD("'.$week_start.'", INTERVAL WO.week_day DAY) BETWEEN ("'.$d2.'") AND ("'.$t_date1.'") 
									AND (("'.$d2.'" BETWEEN WO.start_dt AND IFNULL(WO.end_dt,"'.$d2.'"))
									OR ("'.$t_date1.'" BETWEEN WO.start_dt AND IFNULL(WO.end_dt,"'.$t_date1.'")))
									AND WO.status_nm="Active" 
									AND WO.conf_status="Approved" 
									LEFT JOIN comp_track LT_comp 
									ON LT_comp.user_id_fk=U.user_id AND 
									 LT.end_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" 
									AND LT_comp.is_comp=1 
									AND LT_comp.status_nm="Active" 
									AND LT_comp.conf_status="Approved" 
									 WHERE U.status_nm="Active" AND IFNULL(R.m_val,"'.$d2.'" )<="'.$d2.'"  
									AND U.user_id NOT IN ('.$u_ids.') 
									AND LT.comp_track_id IS NULL AND 
									(WO.emp_week_off_id IS NULL OR (WO.emp_week_off_id IS NOT NULL AND LT_comp.comp_track_id IS NOT NULL)) ;';
				$sqlQuery =$this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 public function alert_4_rm($t_date1,$u_ids){
		 $d2 = date('Y-m-d', strtotime($t_date1.' -7 days'));
		 $day_w = date('w',strtotime($d2));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d', strtotime($d2.' -'.($day_w-1).' days'));
							
		 
						$sql=' SELECT U.user_id,U.user_login_name AS email,U.full_name ,R.m_val ,
						UM.full_name AS man_name,UM.user_login_name AS man_mail,UM.user_id AS man_id 
								  FROM 
								  user_mst U 
								  LEFT JOIN user_mst UM 
								  ON UM.user_id=U.manager_id AND UM.status_nm="Active"
								  LEFT JOIN (SELECT user_id_fk,max(day_val) AS m_val 
												from response_mst R 
											 WHERE R.status_nm="Active" AND R.day_val<="'.$t_date1.'" 
											 GROUP BY user_id_fk ) R 
								  ON U.user_id=R.user_id_fk 
								  LEFT JOIN comp_track LT 
									ON LT.user_id_fk=U.user_id AND 
									(LT.start_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" 
									OR  LT.end_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" ) 
									AND IFNULL(LT.is_comp,0)=0 AND IFNULL(LT.is_half_day,0)<>1  
									AND LT.status_nm="Active" 
									AND LT.conf_status="Approved"
									LEFT JOIN emp_week_off WO 
									ON WO.user_id_fk=U.user_id 
									AND DATE_ADD("'.$week_start.'", INTERVAL WO.week_day DAY) BETWEEN ("'.$d2.'") AND ("'.$t_date1.'") 
									AND (("'.$d2.'" BETWEEN WO.start_dt AND IFNULL(WO.end_dt,"'.$d2.'"))
									OR ("'.$t_date1.'" BETWEEN WO.start_dt AND IFNULL(WO.end_dt,"'.$t_date1.'")))
									AND WO.status_nm="Active" 
									AND WO.conf_status="Approved" 
									LEFT JOIN comp_track LT_comp 
									ON LT_comp.user_id_fk=U.user_id AND 
									 LT.end_date BETWEEN "'.$d2.'" AND "'.$t_date1.'" 
									AND LT_comp.is_comp=1 
									AND LT_comp.status_nm="Active" 
									AND LT_comp.conf_status="Approved" 
									 WHERE U.status_nm="Active" AND IFNULL(R.m_val,"'.$d2.'" )<="'.$d2.'"  
									AND U.user_id NOT IN ('.$u_ids.') 
									AND LT.comp_track_id IS NULL AND 
									(WO.emp_week_off_id IS NULL OR (WO.emp_week_off_id IS NOT NULL AND LT_comp.comp_track_id IS NOT NULL)) ;';
				$sqlQuery =$this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 public function alert_6($d1,$d2,$action)
	 {
						$this->db->select(' P.project_id,P.project_name,U.full_name,U.user_login_name,UM.user_login_name AS man_mail,UM.full_name AS man_name, COUNT(*) AS total_logs ,
						SUM(CASE WHEN R.tast_stat="'.$action.'" THEN 1 ELSE 0 END) AS audit_pend  ',FALSE);
						$this->db->from('response_mst R');						
						$this->db->join('activity_mst A', 'A.activity_id=R.activity_id_fk AND A.status_nm="Active"','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						$this->db->join('user_mst U', ' U.user_id=R.user_id_fk ','left');
						$this->db->join('user_mst UM', ' UM.user_id=U.manager_id AND UM.status_nm="Active" ','left');
						 $this->db->where('R.status_nm', 'Active');
						 $this->db->where('R.day_val BETWEEN "'.$d1.'" AND "'.$d2.'" ', null, false);
						 $this->db->group_by('P.project_id,P.project_name,U.full_name,U.user_login_name,UM.full_name,UM.user_login_name',null);
						 $this->db->having('audit_pend>0');
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	  
	 
	public function alert_9($st_dt3,$e_dt3)
	{
		$st_dt3_f = date('d-M', strtotime($st_dt3));	
		$e_dt3_f = date('d-M', strtotime($e_dt3));	
		
	$sql="SELECT 
	Num1.dept_id_fk,Num1.project_name,
		Num1.activity_id_fk,
		Num1.activity_name,
		Num1.wu_name,
		CONCAT('".$st_dt3_f."',' to ','".$e_dt3_f."') AS date_3,
		SUM(CASE WHEN Num1.day_val BETWEEN '".$st_dt3."' AND '".$e_dt3."' THEN  Num1.quo ELSE 0 END ) AS final_xp3,
		ROUND(((SUM(CASE WHEN Num1.day_val BETWEEN '".$st_dt3."' AND '".$e_dt3."' THEN w_done ELSE 0 END) * 8) / SUM( (CASE WHEN Num1.day_val BETWEEN '".$st_dt3."' AND '".$e_dt3."' THEN  Num1.quo ELSE 0 END) * (CASE WHEN Num1.day_val BETWEEN '".$st_dt3."' AND '".$e_dt3."' THEN  Num2.DC0 ELSE 0 END) )),2) AS achieved_tgt,
		Num1.std_tgt_val AS std_tgt
	FROM 
		(
		SELECT
			R.day_val,			
			R.user_id_fk,
			R.std_tgt_val,
			P.project_name,R.dept_id_fk,
			A.activity_name,w.wu_name,
			R.activity_id_fk,
			IFNULL(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)),(R.final_xp) + IFNULL(R.comp_xp, 0))),0) AS quo,
			IFNULL(SUM(IFNULL(R.audit_work, R.work_comp)),0) AS w_done
		FROM
			response_mst R
		JOIN activity_mst A ON
			R.activity_id_fk = A.activity_id AND A.status_nm = 'Active' 
		JOIN project_mst P ON
			A.project_id_fk = P.project_id AND P.status_nm = 'Active' 
		JOIN work_unit_mst w ON
			A.`wu_id_fk` = w.wu_id AND w.status_nm = 'Active' 
		WHERE 
			 R.status_nm = 'Active' AND R.day_val BETWEEN '".$st_dt3."' AND '".$e_dt3."' 
			 AND w.is_quantify<>'N' 
		GROUP BY
			R.day_val,P.project_name,R.dept_id_fk,R.user_id_fk,A.activity_name,w.wu_name,R.activity_id_fk ,R.std_tgt_val 
				) Num1
			JOIN(
		SELECT R.day_val,
			R.user_id_fk,
			(8 - 0.8 *(IFNULL(SUM(CASE WHEN w.is_quantify = 'N' 
					THEN IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)),(R.final_xp) + IFNULL(R.comp_xp, 0)) ELSE 0 END),0)))/(IFNULL(SUM(CASE WHEN w.is_quantify = 'Y' THEN IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)),(R.final_xp) + IFNULL(R.comp_xp, 0)) ELSE 0 END),0)) AS DC0
		FROM
			`response_mst` R
		JOIN activity_mst A ON
			R.activity_id_fk = A.activity_id AND A.status_nm = 'Active'
		JOIN work_unit_mst w ON
			A.`wu_id_fk` = w.wu_id AND w.status_nm = 'Active'
		WHERE
			 R.status_nm = 'Active' AND R.day_val BETWEEN '".$st_dt3."' AND '".$e_dt3."' 
		GROUP BY 
			R.day_val,R.user_id_fk) Num2
		ON
			Num2.day_val = Num1.day_val AND Num2.user_id_fk = Num1.user_id_fk 
		GROUP BY
			Num1.dept_id_fk,Num1.project_name,Num1.activity_id_fk,Num1.activity_name,Num1.wu_name,Num1.std_tgt_val 
			HAVING final_xp3>20 AND achieved_tgt/std_tgt >1.5; 
			";
 
					
				$sqlQuery = $this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	}
	 
	 public function alert_8($d1,$d2,$action)
	 {
						$this->db->select(' P.project_id,P.dept_id_fk,P.project_name,U.full_name,U.user_login_name,UM.user_login_name AS man_mail,UM.full_name AS man_name, COUNT(*) AS total_logs ,
						SUM(CASE WHEN R.tast_stat="'.$action.'" THEN 1 ELSE 0 END) AS audit_pend  ',FALSE);
						$this->db->from('response_mst R');						
						$this->db->join('activity_mst A', 'A.activity_id=R.activity_id_fk AND A.status_nm="Active"','inner');
						$this->db->join('project_mst P', 'P.project_id=A.project_id_fk AND P.status_nm="Active" ','inner');
						$this->db->join('user_mst U', ' U.user_id=R.rev_assignee ','left');
						$this->db->join('user_mst UM', ' UM.user_id=U.manager_id AND UM.status_nm="Active" ','left');
						 $this->db->where('R.status_nm', 'Active');
						 $this->db->where('R.day_val BETWEEN "'.$d1.'" AND "'.$d2.'" ', null, false);
						 $this->db->group_by('P.project_id,P.project_name,U.full_name,U.user_login_name,UM.full_name,UM.user_login_name',null);
						 $this->db->having('audit_pend>0');
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 public function alert_5(){
						$sql=' SELECT
									P.dept_id_fk,P.project_name,
									CONCAT(CONCAT_WS("_",P.project_name,T.task_name,S.sub_name,J.job_name)," (",W.wu_name,")") AS a_name
								FROM
									`activity_mst` A
								LEFT JOIN work_unit_mst W ON
									A.wu_id_fk = W.wu_id
								LEFT JOIN project_mst P ON
									A.project_id_fk = P.project_id
								LEFT JOIN task_mst T ON
									A.task_id_fk = T.task_id
								LEFT JOIN sub_mst S ON
									A.sub_id_fk = S.sub_id
								LEFT JOIN job_mst J ON
									A.job_id_fk = J.job_id
								WHERE A.status_nm = "Active" AND  A.is_closed IS NULL AND J.job_name LIKE "%Creation%" 
									AND CONCAT_WS("_",A.project_id_fk,A.task_id_fk,A.sub_id_fk,J.job_name,A.wu_id_fk) 
										NOT IN(
											SELECT REPLACE(CONCAT_WS("_",A.project_id_fk,A.task_id_fk,A.sub_id_fk,J.job_name,A.wu_id_fk),"Reviews","Creation")
											FROM
												`activity_mst` A
											LEFT JOIN work_unit_mst W ON
												A.wu_id_fk = W.wu_id
											LEFT JOIN job_mst J ON
												A.job_id_fk = J.job_id
											WHERE A.status_nm = "Active" AND A.is_closed IS NULL AND J.job_name LIKE "%Reviews%"
										)
								ORDER BY P.project_name;';
				$sqlQuery =$this->db->query($sql);
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 
	 public function last_logged($u_ids){
						$this->db->select('  R.user_id_fk,MAX(R.day_val) AS day_val ',FALSE);
						$this->db->from('response_mst R');						
						 $this->db->where('R.status_nm', 'Active');
						 $this->db->where('R.user_id_fk NOT IN ('.$u_ids.')', null, false);
						 $this->db->group_by('R.user_id_fk');
						 
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	  public function poc_list($u_ids,$rol_id){
						$this->db->select(' P.project_id ,P.project_name,U.user_login_name AS email ',FALSE);
						$this->db->from('project_mst P ');	
						$this->db->join('user_mst U', ' find_in_set(P.project_id,U.project_id_fk )<> 0 AND U.status_nm="Active" AND U.role_id_fk='.$rol_id.' ','left');
						 $this->db->where('P.status_nm', 'Active');
						 $this->db->where(' U.user_id IS NOT NULL ', null, false);						 
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 public function getWorkReqyest($work_request_id){
			$sql="SELECT
					um.full_name, um.user_login_name,um.dept_id_fk,wr.*,P.project_name,wrct.com_type as combos_type ,
					umst.full_name AS req_name 
				FROM
					work_request wr
				JOIN user_mst um ON
					wr.emp_user_id = um.user_id 
				LEFT JOIN user_mst umst 
				ON wr.req_user_id = umst.user_id
				JOIN project_mst P ON
					P.project_id = wr.project_id_fk 
				JOIN work_request_comb_type wrct 
				ON wr.combo_type= wrct.id
				WHERE
					wr.work_request_id IN  (".$work_request_id.");";
			$sqlQuery =$this->db->query($sql);
			$resultSet = $sqlQuery->result_array();
			  if($resultSet)
			  {
				  return $resultSet;
			  }
			  else{
				return false;
			  } 
	}

	public function getSeniorManager($p_id,$rol_id,$dept_id){
			 // $this->db->select(' U.user_login_name AS email,P.project_id ',FALSE);
						// $this->db->from('project_mst P ');	
						// $this->db->join('user_mst U', ' (find_in_set(P.project_id,U.project_id_fk )<> 0 OR IFNULL(U.project_id_fk,0)=0)AND U.status_nm="Active" AND U.role_id_fk='.$rol_id.' ','left');
						 // $this->db->where('P.status_nm', 'Active');
						 // if($p_id)
						 // {
						 // $this->db->where('P.project_id IN ('.$p_id.')', null,false);
						 // }
						 // $this->db->where(' U.user_id IS NOT NULL ', null, false);
						 // $this->db->where(' U.dept_id_fk  IN('.$dept_id.')', null, false);
						 // $this->db->group_by('U.user_login_name');
				$this->db->select(' U.user_login_name AS email',FALSE);
				$this->db->from('user_mst U');
				 $this->db->where(' (find_in_set('.$dept_id.',U.dept_id_fk )<> 0 OR IFNULL(U.dept_id_fk,0)=0) AND U.status_nm="Active" AND U.role_id_fk='.$rol_id.' ',null,false);
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
				 
				
					  if($resultSet)
					  {
						  return $resultSet;
					  }
					  else{
						return false;
					}
	}

	public function getRequesterMail($RequesterId){
		 $this->db->select(' U.user_login_name AS email,U.full_name');
						$this->db->from('user_mst U ');	
						$this->db->join('work_request wr', 'U.user_id=wr.req_user_id');
						$this->db->where('U.status_nm', 'Active');
						$this->db->where('U.user_id IS NOT NULL ', null, false);
						$this->db->where('wr.req_user_id',$RequesterId);
						$this->db->group_by('wr.req_user_id');
						$sqlQuery = $this->db->get();
						//echo $this->db->last_query();die;
				$resultSet = $sqlQuery->result_array();
					  if($resultSet)
					  {
						  return $resultSet;
					  }
					  else{
						return false;
					}
	}
	 public function proj_own_list($p_ids,$rol_id){
						$this->db->select(' P.project_id,U.user_login_name AS email,P.dept_id_fk ',FALSE);
						$this->db->from('project_mst P ');	
						$this->db->join('user_mst U', ' find_in_set(P.project_id,U.project_id_fk )<> 0 AND U.status_nm="Active" AND U.role_id_fk='.$rol_id.' ','left');
						 $this->db->where('P.status_nm', 'Active');
						 if($p_ids)
						 {
						 $this->db->where('P.project_id IN ('.$p_ids.')', null,false);
						 }
						 $this->db->where(' U.user_id IS NOT NULL ', null, false);						 
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
	 
	 public function sm_list($u_ids,$rol_id){
						$this->db->select(' U.full_name,IFNULL(U.dept_id_fk,0) AS dept_id,U.user_login_name AS email ',FALSE);
						$this->db->from('user_mst U ');	
						 $this->db->where('U.status_nm', 'Active');
						 $this->db->where(' U.role_id_fk ='.$rol_id.' ', null, false);						 
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  } 
	 }
}
?>