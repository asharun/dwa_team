<?php
class Report_model extends CI_model{
			
		public function getWeeklyContributors($startDate, $endDate, $type,$dept_id)
		{
			if ($type == '0') {
				$query = 'SELECT E.project_id_fk AS project_id,E.project_name,E.user_id_fk,U.full_name
					FROM (
							SELECT *,  @num := IF(@type = tab1.user_id_fk,  @num + 1, 1 ) AS row_number,
								@type := tab1.user_id_fk AS dummy
							FROM
								(
								SELECT
									A.project_id_fk,
									P1.project_name,
									R.user_id_fk,
									ROUND(SUM(IF(R.final_xp IS NULL,(IFNULL(R.equiv_xp,0)+IFNULL(R.comp_xp,0)),(IFNULL(R.final_xp,0)+IFNULL(R.comp_xp,0)))),2) AS equiv_xp 
								FROM
									`response_mst` `R` 
									INNER JOIN (SELECT @num := 0,    @type := "") B
									ON 1=1
								INNER JOIN `activity_mst` `A` ON
									`R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = "Active" 
								INNER JOIN `project_mst` `P1` 
								ON `P1`.`project_id`=`A`.`project_id_fk` 
								WHERE
									R.day_val BETWEEN "'.$startDate.'" AND "'.$endDate.'" AND `R`.`status_nm` = "Active" 
									AND R.dept_id_fk="'.$dept_id.'"
								GROUP BY `R`.`user_id_fk`,A.project_id_fk
								ORDER BY `R`.`user_id_fk`,equiv_xp DESC,P1.project_name
							) tab1
							GROUP BY    user_id_fk,project_id_fk,project_name,equiv_xp 
							HAVING row_number = 1)E 
				JOIN user_mst U 
				ON U.user_id=E.user_id_fk 
				ORDER BY E.project_name,U.full_name;';
			} else {
				$query = 'SELECT DISTINCT 
					A.project_id_fk, 
					P1.project_name,
					R.user_id_fk,
					U.full_name
				FROM
					`response_mst` `R` 
				INNER JOIN 
					`activity_mst` `A` 
				ON
					`R`.`activity_id_fk` = `A`.`activity_id` 
				AND 
					`A`.`status_nm` = "Active" 
				INNER JOIN 
					`project_mst` `P1` 
				ON 
					`P1`.`project_id`=`A`.`project_id_fk`
				INNER JOIN 
					`user_mst` `U`
				ON  
					`U`.`user_id` = `R`.`user_id_fk` 
				WHERE
					R.day_val BETWEEN "'.$startDate.'" AND "'.$endDate.'" 
				AND `R`.`status_nm` = "Active" AND R.dept_id_fk="'.$dept_id.'" 
				ORDER BY P1.project_name,U.full_name ;';
			}
			
			$sqlQuery = $this->db->query($query);
			$resultSet = $sqlQuery->result_array();             
			
			if($resultSet)
			{
				return $resultSet;
			}
			else{
				return false;
			}
		}
}
?>