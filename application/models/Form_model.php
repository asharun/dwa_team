<?php
class Form_model extends CI_model{
	public $tx_cn='';	
	
	public function __construct(){
      $this->tx_cn='"'.$this->config->item('tx_cn').'"';	
   }
   
  public function fetch_form_list($u_id,$fff){                    
                    $this->db->where('status_nm', 'Active');
					$this->db->where(' FIND_IN_SET(form_code,"'.$fff.'")<>0',null,false);
                    $res = $this->db->get('form_mst');                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }               
        }
		
		public function fetch_slot_list($uids,$fff){     
					
					 $sql='SELECT S.slot_id as s_id, CONCAT(S.slot_name, " ", S.year_nm, " - ", S.quarter) AS s_name 
								FROM `slot_mst` S 
							WHERE S.`status_nm` = "Active" AND S.slot_type="'.$fff.'"  
							AND S.slot_id IN (SELECT AUM.slot_id_fk 
								FROM appraisal_user_mst AUM
								WHERE AUM.user_id_fk='.$uids.' AND AUM.status_nm="Active" 
								UNION SELECT AFB.slot_id_fk 
								FROM appraisal_fb_pro_mst AFB
								WHERE AFB.po_id_fk='.$uids.' AND AFB.status_nm="Active" 
								 UNION SELECT AFM.slot_id_fk FROM  appraisal_fb_pro_mst AFM
								WHERE AFM.mr_id_fk='.$uids.' AND AFM.status_nm="Active" 
								UNION SELECT ADM.slot_id_fk FROM  appraisal_dm_mst ADM
								WHERE ADM.user_id_fk='.$uids.' AND ADM.status_nm="Active" 
								) 
							ORDER BY S.`slot_id` DESC';
					$res = $this->db->query($sql);  				
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }               
        }
		
		public function fetch_fd_section_list($form_code,$u_id,$emp,$pro,$slots_li){ 
			//$slots_li=$this->session->userdata('slot_id');
			if($form_code=="feed_po")
			{
				//$tab_var='L_O_id_fk';
				$section_id=13;
			}else{
				//$tab_var='MR_id_fk';
				$section_id=14;
			}
					$sql='SELECT S.* 
							FROM section_mst S 
						  JOIN form_mst F 
						  ON S.form_id_fk=F.form_id 
                          LEFT JOIN (SELECT DISTINCT section_id_fk FROM sec_response_mst 
                                     WHERE user_id_fk="'.$emp.'" AND slot_id_fk='.$slots_li.' AND IFNULL(project_id_fk,0)='.$pro.' AND status_nm="Active"
									 AND l_o_m_id_fk ="'.$u_id.'")SR 
                          ON SR.section_id_fk=S.section_id 
                          LEFT JOIN (SELECT DISTINCT section_id_fk FROM ques_response_mst 
                                     WHERE l_o_m_id_fk="'.$u_id.'" AND slot_id_fk='.$slots_li.' AND user_id_fk="'.$emp.'" AND status_nm="Active"
									 AND IFNULL(project_id_fk,0)='.$pro.')QR 
                          ON QR.section_id_fk=S.section_id 
						  WHERE F.form_code="'.$form_code.'"  AND S.slot_id_fk='.$slots_li.' 
                          AND (SR.section_id_fk IS NOT NULL OR QR.section_id_fk IS NOT NULL OR S.section_id="'.$section_id.'");';
		
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		public function fetch_fd_us_sec_list($file_nm,$u_id,$emp,$pro,$slots_li)
		{
			
			if($file_nm=="feed_po")
			{
				//$tab_var='L_O_id_fk';
				$section_id=13;
			}else{
				//$tab_var='MR_id_fk';
				$section_id=14;
			}
                    $sql='SELECT
							S.sec_code AS sec_code,
							S.sec_select,
							S.parent_sec,
							IFNULL(SR.sec_resp_code,S.sec_code) AS sec_cd,
                            SR.*
						FROM
							section_mst S 
						JOIN form_mst F 
						ON S.form_id_fk=F.form_id  
						LEFT JOIN sec_response_mst SR ON
							S.`section_id` = SR.section_id_fk AND SR.status_nm = "Active" 
							AND SR.l_o_m_id_fk="'.$u_id.'"  AND SR.slot_id_fk='.$slots_li.' 
						AND SR.user_id_fk="'.$emp.'" AND IFNULL(SR.project_id_fk,0)='.$pro.' 
					WHERE F.form_code="'.$file_nm.'"  AND S.slot_id_fk='.$slots_li.'  AND S.section_id="'.$section_id.'" AND S.status_nm="Active" 
					ORDER BY S.section_id,SR.sec_resp_code;';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		public function fetch_fd_ques_list($file_nm,$u_id,$emp,$pro,$slots_li)
		{
			if($file_nm=="feed_po")
			{
				//$tab_var='L_O_id_fk';
				$section_id=13;
			}else{
				//$tab_var='MR_id_fk';
				$section_id=14;
			}
                    $sql='SELECT
							S.sec_code,
							IFNULL(SR.sec_resp_code, S.sec_code) AS sec_cd,
                            Q.*,
							QR.ques_resp_id,
                            QR.ques_typ_val,
                            QR.ques_resp_desc,
							QR.sec_resp_code 
						FROM
							section_mst S
						JOIN question_mst Q ON
							S.`section_id` = Q.section_id_fk AND Q.status_nm = "Active" 
							 AND Q.slot_id_fk='.$slots_li.' 
						JOIN form_mst F 
						ON S.form_id_fk=F.form_id 
						LEFT JOIN sec_response_mst SR 
						ON S.section_id=SR.section_id_fk  AND SR.slot_id_fk='.$slots_li.'
						AND SR.l_o_m_id_fk="'.$u_id.'" 
						AND SR.user_id_fk="'.$emp.'" AND IFNULL(SR.project_id_fk,0)='.$pro.' 
						AND SR.status_nm = "Active" 
                        LEFT JOIN ques_response_mst QR
                        ON  QR.l_o_m_id_fk="'.$u_id.'"  AND QR.slot_id_fk='.$slots_li.' 
						AND QR.user_id_fk="'.$emp.'" AND IFNULL(QR.project_id_fk,0)='.$pro.' 
                        AND QR.question_id_fk=Q.question_id 
						AND QR.status_nm = "Active" 
					WHERE F.form_code="'.$file_nm.'"  AND S.slot_id_fk='.$slots_li.'  AND S.section_id="'.$section_id.'" AND S.status_nm="Active" 
					ORDER BY S.section_id,Q.question_id,IFNULL(SR.sec_resp_code, S.sec_code);';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
			public function fetch_section_list($form_code,$u_id,$section_id,$slots_li){                    
                    // $sql='SELECT S.* 
							// FROM section_mst S 
						  // JOIN form_mst F 
						  // ON S.form_id_fk=F.form_id 
						  // WHERE F.form_code="'.$form_code.'";';    
					$sql='SELECT S.* 
							FROM section_mst S 
						  JOIN form_mst F 
						  ON S.form_id_fk=F.form_id 
                          LEFT JOIN (SELECT DISTINCT section_id_fk FROM sec_response_mst 
                                     WHERE user_id_fk="'.$u_id.'" AND slot_id_fk='.$slots_li.' AND status_nm="Active")SR 
                          ON SR.section_id_fk=S.section_id 
                          LEFT JOIN (SELECT DISTINCT section_id_fk FROM ques_response_mst 
                                     WHERE user_id_fk="'.$u_id.'" AND slot_id_fk='.$slots_li.' AND status_nm="Active")QR 
                          ON QR.section_id_fk=S.section_id 
						  WHERE F.form_code="'.$form_code.'" AND S.slot_id_fk='.$slots_li.' 
                          AND (SR.section_id_fk IS NOT NULL OR QR.section_id_fk IS NOT NULL OR S.section_id="'.$section_id.'");';
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		public function fetch_sec_stop($form_code,$u_id,$slots_li){   
					$sql='SELECT  MIN(QR.section_id_fk) AS stop_sec
							FROM
								ques_response_mst QR 
								JOIN section_mst S 
								ON QR.section_id_fk=S.section_id AND S.status_nm="Active" 
								AND S.slot_id_fk='.$slots_li.' 
								JOIN form_mst F 
								ON S.form_id_fk=F.form_id AND F.status_nm="Active"
							WHERE F.form_code="'.$form_code.'" AND QR.slot_id_fk='.$slots_li.' AND 
								QR.user_id_fk = "'.$u_id.'" AND QR.status_nm = "Active" 
								AND QR.ques_typ_val IS NOT NULL 
								AND FIND_IN_SET("",QR.`ques_typ_val`)<>0;';
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		public function fetch_section_list_view($form_code,$u_id,$slots_li){ 
					$sql='SELECT S.* 
							FROM section_mst S 
						  JOIN form_mst F 
						  ON S.form_id_fk=F.form_id 
                          LEFT JOIN (SELECT DISTINCT section_id_fk FROM sec_response_mst 
                                     WHERE user_id_fk="'.$u_id.'" AND slot_id_fk='.$slots_li.'  AND status_nm="Active")SR 
                          ON SR.section_id_fk=S.section_id 
                          LEFT JOIN (SELECT DISTINCT section_id_fk FROM ques_response_mst 
                                     WHERE user_id_fk="'.$u_id.'" AND slot_id_fk='.$slots_li.'  AND status_nm="Active")QR 
                          ON QR.section_id_fk=S.section_id 
						  WHERE F.form_code="'.$form_code.'" AND S.slot_id_fk='.$slots_li.' 
                          AND (SR.section_id_fk IS NOT NULL OR QR.section_id_fk IS NOT NULL);';
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		public function prev_sec($file_nm,$u_id,$section_id,$slots_li)
		{
			 $sql='SELECT
							MAX(S.section_id) AS prev_ss
						FROM
							section_mst S 
						JOIN form_mst F 
						ON S.form_id_fk=F.form_id  
						LEFT JOIN (SELECT DISTINCT section_id_fk FROM sec_response_mst 
                                     WHERE user_id_fk="'.$u_id.'" AND slot_id_fk='.$slots_li.' AND status_nm="Active")SR 
                          ON SR.section_id_fk=S.section_id 
                          LEFT JOIN (SELECT DISTINCT section_id_fk FROM ques_response_mst 
                                     WHERE user_id_fk="'.$u_id.'" AND slot_id_fk='.$slots_li.' AND status_nm="Active")QR 
                          ON QR.section_id_fk=S.section_id 
						  WHERE F.form_code="'.$file_nm.'" AND S.slot_id_fk='.$slots_li.' 
                          AND (SR.section_id_fk IS NOT NULL OR QR.section_id_fk IS NOT NULL OR S.section_id="'.$section_id.'") 
						  AND S.section_id<'.$section_id.' ;';         
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }           
			
		}	
		
		public function fetch_f_sub_list($file_nm,$u_id,$sl_id){  
$currentDate = date('Y-m-d');		
                    $sql='SELECT
							FS.form_code_fk 
						FROM
							form_response_mst FS 
						WHERE FS.form_code_fk = "'.$file_nm.'" AND FS.slot_id_fk='.$sl_id.' AND FS.user_id_fk='.$u_id.'  
						AND FS.status_nm = "Active" AND FS.stat_app = "Submitted" 
						UNION 
						SELECT slot_id_fk from feed_start FS 
						WHERE FS.slot_id_fk  ='.$sl_id.' 
						AND "'.$currentDate.'" NOT BETWEEN FS.feed_s AND FS.feed_e 
						;';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		public function fetch_feed_submit($file_nm,$u_id,$emp,$pro,$slots_li){
                    $sql='SELECT
							*
						FROM
							form_response_mst FS 
						WHERE FS.form_code_fk = "'.$file_nm.'"  AND FS.slot_id_fk='.$slots_li.'
						AND FS.l_o_m_id_fk='.$u_id.' AND FS.user_id_fk='.$emp.' 
						AND IFNULL(FS.project_id_fk,0)='.$pro.' 
						AND FS.status_nm = "Active" AND FS.stat_app = "Submitted" ;';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		public function fetch_us_sec_list($form_code,$u_id,$section_id,$slots_li){                    
                    $sql='SELECT
							S.sec_code AS sec_code,
							S.sec_select,
							S.parent_sec,
							IFNULL(SR.sec_resp_code,S.sec_code) AS sec_cd,
                            SR.*
						FROM
							section_mst S 
						JOIN form_mst F 
						ON S.form_id_fk=F.form_id  
						LEFT JOIN sec_response_mst SR ON
							S.`section_id` = SR.section_id_fk AND SR.slot_id_fk='.$slots_li.' AND SR.status_nm = "Active"
							AND SR.user_id_fk="'.$u_id.'"                        
					WHERE F.form_code="'.$form_code.'" AND S.slot_id_fk='.$slots_li.' AND S.section_id="'.$section_id.'" AND S.status_nm="Active" 
					ORDER BY S.section_id,SR.sec_resp_code;';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		public function fetch_us_s_view_list($form_code,$u_id,$slots_li){                    
                    $sql='SELECT S.section_id,
							S.sec_code AS sec_code,
							S.sec_select,
							S.parent_sec,
							IFNULL(SR.sec_resp_code,S.sec_code) AS sec_cd,
                            SR.*,
							P_pro.project_name AS pro_nm,
							U_pro.full_name AS po_name,
							U_man.full_name AS m_name
						FROM
							section_mst S 
						JOIN form_mst F 
						ON S.form_id_fk=F.form_id  
						LEFT JOIN sec_response_mst SR ON
							S.`section_id` = SR.section_id_fk AND SR.slot_id_fk='.$slots_li.' AND SR.status_nm = "Active"
							AND SR.user_id_fk="'.$u_id.'"   
						LEFT JOIN appraisal_projects P_pro ON
							P_pro.`app_pro_id` = SR.project_id_fk  AND P_pro.slot_id_fk='.$slots_li.'
						LEFT JOIN user_mst U_pro ON
							U_pro.`user_id` = SR.l_o_id_fk  
						LEFT JOIN user_mst U_man ON
							U_man.`user_id` = SR.mr_id_fk 
					WHERE F.form_code="'.$form_code.'" AND S.slot_id_fk='.$slots_li.' AND S.status_nm="Active" 
					ORDER BY S.section_id,SR.sec_resp_code;';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		public function fetch_usssec_list($form_code,$u_id,$section_id,$slots_li){                    
                    $sql='SELECT
							S.*,
							S.sec_code AS sec_cd 
						FROM
							section_mst S 
						JOIN form_mst F 
						ON S.form_id_fk=F.form_id                      
					WHERE F.form_code="'.$form_code.'" AND S.slot_id_fk='.$slots_li.' AND S.section_id="'.$section_id.'" AND S.status_nm="Active" 
					ORDER BY S.section_id;';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		
		public function fetch_feed_users($file_nm,$u_id,$slots_li){
			if($file_nm=="feed_po")
			{
				$tab_val='po_id_fk';
			}else{
				$tab_val='MR_id_fk';
			}
                    $sql='SELECT
							DISTINCT U.user_id AS val_id ,
							U.full_name AS val_name 
						FROM
							appraisal_fb_pro_mst S 
						JOIN user_mst U 
						ON S.user_id_fk=U.user_id                      
					WHERE S.'.$tab_val.'="'.$u_id.'" AND S.slot_id_fk='.$slots_li.' AND S.status_nm="Active" 
					ORDER BY U.full_name;';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }

public function fetch_feed_cnt($file_nm,$u_id,$slots_li){
			if($file_nm=="feed_po")
			{
				$tab_val='po_id_fk';
				$form_cd='feed_po';
			}else{
				$tab_val='MR_id_fk';
				$form_cd='feed_mr';
			}
                    $sql='SELECT
							COUNT(*) AS tot,
							COUNT(FR.form_resp_id) AS comp
						FROM
							appraisal_fb_pro_mst S 
						JOIN user_mst U 
						ON S.user_id_fk=U.user_id     
						LEFT JOIN appraisal_projects P 
						ON P.app_pro_id=S.pro_id_fk  	AND P.slot_id_fk='.$slots_li.' 
						LEFT JOIN form_response_mst FR 
						ON FR.form_code_fk="'.$form_cd.'" AND FR.slot_id_fk='.$slots_li.' 
						AND FR.l_o_m_id_fk="'.$u_id.'" 
						AND FR.user_id_fk=S.user_id_fk  AND FR.stat_app = "Submitted" 
						AND IFNULL(FR.project_id_fk,0)=IFNULL(S.pro_id_fk,0) 
					WHERE S.'.$tab_val.'="'.$u_id.'" AND S.slot_id_fk='.$slots_li.' AND S.status_nm="Active";';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		
		public function load_pros($file_nm,$u_id,$emps,$slots_li)
		{
			if($file_nm=="feed_po")
			{
				$tab_val='po_id_fk';
			}else{
				$tab_val='MR_id_fk';
			}
			
			$sql='SELECT
							DISTINCT IFNULL(P.app_pro_id,0) AS val_id ,
							IFNULL(P.project_name,"OverAll") AS val_name 
						FROM
							appraisal_fb_pro_mst S 
						LEFT JOIN appraisal_projects P 
						ON S.pro_id_fk=P.app_pro_id   AND P.slot_id_fk='.$slots_li.'                    
					WHERE S.'.$tab_val.'="'.$u_id.'" AND S.slot_id_fk='.$slots_li.' AND S.status_nm="Active" 
					AND S.user_id_fk='.$emps.' 
					ORDER BY P.project_name;';     
					
			$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }     
			
		}
		public function fetch_sel_val($form_code,$u_id,$section_id,$sel_label,$parent_sec,$slots_li){    
			if($sel_label=='project')
			{
                     $sql='SELECT
							P.app_pro_id AS val_id,
							P.project_name AS val_name
						FROM
							appraisal_projects P 
							LEFT JOIN sec_response_mst SR 
							ON SR.project_id_fk=P.app_pro_id AND SR.slot_id_fk='.$slots_li.' 
							AND SR.user_id_fk="'.$u_id.'" AND SR.sec_resp_code IS NULL 
							AND SR.section_id_fk<=12 
							AND SR.status_nm="Active"
						WHERE P.status_nm="Active" AND P.slot_id_fk='.$slots_li.' 
						AND  (SR.sec_resp_id IS NULL OR SR.section_id_fk='.$section_id.')
					ORDER BY P.project_name;'; 
			}	
			if($sel_label=='owner')
			{
                    $sql='SELECT 
							U.full_name AS val_name,
							U.user_id AS val_id
						FROM
							appraisal_pro_mst A 
							JOIN user_mst U 
							ON U.user_id=A.po_id_fk 
						WHERE A.status_nm="Active" AND A.slot_id_fk='.$slots_li.'
						AND A.pro_id_fk IN (SELECT project_id_fk 
												FROM sec_response_mst 
										WHERE section_id_fk = "'.$parent_sec.'" AND slot_id_fk='.$slots_li.' AND status_nm="Active" 
										AND user_id_fk="'.$u_id.'") 
										AND U.user_id NOT IN ("'.$u_id.'") 
					ORDER BY U.full_name;';  
			}
			
			if($sel_label=='manager')
			{
                  $sql='SELECT
							U.full_name AS val_name,
							U.user_id AS val_id
						FROM
							appraisal_pro_mst A 
							JOIN user_mst U 
							ON U.user_id=A.mr_id_fk 
						WHERE A.status_nm="Active" AND A.slot_id_fk='.$slots_li.' 
						AND A.pro_id_fk IN (SELECT project_id_fk 
												FROM sec_response_mst 
										WHERE section_id_fk = "'.$parent_sec.'" AND slot_id_fk='.$slots_li.' AND status_nm="Active" 
										AND user_id_fk="'.$u_id.'") 
						AND U.user_id NOT IN ("'.$u_id.'") 
					ORDER BY U.full_name;';
			}
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }		
		
		public function fetch_ques_list($form_code,$u_id,$section_id,$slots_li){                    
                    $sql='SELECT
							S.sec_code,
							IFNULL(SR.sec_resp_code, S.sec_code) AS sec_cd,
                            Q.*,
							QR.ques_resp_id,
                            QR.ques_typ_val,
                            QR.ques_resp_desc,
							QR.sec_resp_code 
						FROM
							section_mst S
						JOIN question_mst Q ON
							S.`section_id` = Q.section_id_fk AND Q.status_nm = "Active" 
							 AND Q.slot_id_fk='.$slots_li.' 
						JOIN form_mst F 
						ON S.form_id_fk=F.form_id 
						LEFT JOIN sec_response_mst SR 
						ON S.section_id=SR.section_id_fk  AND SR.user_id_fk="'.$u_id.'"  AND SR.slot_id_fk='.$slots_li.' 
						AND SR.status_nm = "Active" 
                        LEFT JOIN ques_response_mst QR
                        ON QR.user_id_fk="'.$u_id.'"  AND QR.slot_id_fk='.$slots_li.' 
                        AND QR.question_id_fk=Q.question_id AND QR.status_nm = "Active" 
						  AND  
						  (S.sec_code IS NULL OR IFNULL(SR.sec_resp_code, S.sec_code)=IF(SR.sec_resp_code IS NULL,S.sec_code,QR.sec_resp_code))
					WHERE F.form_code="'.$form_code.'"  AND S.slot_id_fk='.$slots_li.' AND S.section_id="'.$section_id.'" AND S.status_nm="Active" 
					ORDER BY S.section_id,Q.question_id,IFNULL(SR.sec_resp_code, S.sec_code);';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		public function fetch_ques_list_view($form_code,$u_id,$slots_li){                    
                    $sql='SELECT
							S.sec_code,
							IFNULL(SR.sec_resp_code, S.sec_code) AS sec_cd,
                            Q.*,
							QR.ques_resp_id,
                            QR.ques_typ_val,
                            QR.ques_resp_desc,
							QR.sec_resp_code 
						FROM
							section_mst S
						JOIN question_mst Q ON
							S.`section_id` = Q.section_id_fk AND Q.status_nm = "Active" 
							 AND Q.slot_id_fk='.$slots_li.' 
						JOIN form_mst F 
						ON S.form_id_fk=F.form_id 
						LEFT JOIN sec_response_mst SR 
						ON S.section_id=SR.section_id_fk  AND SR.slot_id_fk='.$slots_li.' AND SR.user_id_fk="'.$u_id.'" 
						AND SR.status_nm = "Active" 
                        LEFT JOIN ques_response_mst QR
                        ON QR.user_id_fk="'.$u_id.'"  AND QR.slot_id_fk='.$slots_li.' 
                        AND QR.question_id_fk=Q.question_id AND QR.status_nm = "Active" 
						  AND  
						  (S.sec_code IS NULL OR (SR.sec_resp_code=QR.sec_resp_code)) 
					WHERE F.form_code="'.$form_code.'"  AND S.slot_id_fk='.$slots_li.' AND S.status_nm="Active" 
					ORDER BY S.section_id,Q.question_id,IFNULL(QR.sec_resp_code, S.sec_code);';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		public function fetch_qss_list($form_code,$u_id,$section_id,$slots_li){                    
                    $sql='SELECT
							S.sec_code,
							S.sec_code AS sec_cd,
                            Q.* ,
							null as ques_typ_val
						FROM
							section_mst S
						JOIN question_mst Q ON
							S.`section_id` = Q.section_id_fk AND Q.status_nm = "Active"
							 AND Q.slot_id_fk='.$slots_li.' 
						JOIN form_mst F 
						ON S.form_id_fk=F.form_id 
					WHERE F.form_code="'.$form_code.'"  AND S.slot_id_fk='.$slots_li.' AND S.section_id="'.$section_id.'" 
					AND S.status_nm="Active" 
					ORDER BY S.section_id,Q.question_id;';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		public function fetch_opt_val_list($form_code,$u_id){                    
                    $sql='SELECT
							*
						FROM
							opt_val_mst 
							WHERE status_nm="Active";';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		public function ques_final($form_code,$u_id,$slots_li){                    
                    $sql='SELECT * FROM `question_mst` WHERE dec_sec_cd is not null  AND slot_id_fk='.$slots_li.' 
						AND section_id_fk IN (3,4,13);';               
				$res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }                       
        }
		
		public function ins_ques_response_mst($ans){		
				$val=$ans[6];	
				$cs=$ans[7].$ans[8];
					if($ans[8]=='null')
					{
						$cs=null;
					}
		$this->db->trans_start();
					if($ans[0])
					{
						
						$data = array(
								'question_id_fk' => $ans[2],
								'section_id_fk' => $ans[1],
								'user_id_fk' => $ans[3],
								'status_nm' => 'Active',
								'upd_user' => $ans[3],
								'sec_resp_code' => $cs,
								$val => $ans[4]
								);
						$this->db->set('upd_dt', 'current_timestamp', FALSE);				
				$this->db->where('ques_resp_id IN ('.$ans[0].')',null,false);
				$this->db->update('ques_response_mst', $data);
				$lids ='';
					}else{
						$data = array(
								'question_id_fk' => $ans[2],
								'section_id_fk' => $ans[1],
								'user_id_fk' => $ans[3],
								'slot_id_fk' => $ans[12],
								'status_nm' => 'Active',
								'sec_resp_code' => $cs,
								'ins_user' => $ans[3],
								$val => $ans[4]
								);
						$this->db->set('ins_dt', 'current_timestamp', FALSE);
						$this->db->insert('ques_response_mst', $data);
						$lids = $this->db->insert_id();
					}			
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return $lids;
		  }
	 }
	 
	 public function fd_ques_response_mst($ans){
	$val=$ans[6];	
	$cs=$ans[10];
					if($ans[10]==0)
					{
						$cs=null;
					}
		$this->db->trans_start();
					if($ans[0])
					{						
						$data = array(
								'question_id_fk' => $ans[2],
								'section_id_fk' => $ans[1],
								'l_o_m_id_fk' => $ans[3],
								'user_id_fk' => $ans[9],
								'status_nm' => 'Active',
								'upd_user' => $ans[3],
								'project_id_fk' => $cs,
								$val => $ans[4]
								);
						$this->db->set('upd_dt', 'current_timestamp', FALSE);
				$this->db->where('ques_resp_id IN ('.$ans[0].')',null,false);
				$this->db->update('ques_response_mst', $data);
				$lids ='';
					}else{
						$data = array(
								'question_id_fk' => $ans[2],
								'section_id_fk' => $ans[1],
								'l_o_m_id_fk' => $ans[3],
								'slot_id_fk' => $ans[12],
								'user_id_fk' => $ans[9],
								'status_nm' => 'Active',
								'project_id_fk' => $cs,
								'ins_user' => $ans[3],
								$val => $ans[4]
								);
						$this->db->set('ins_dt', 'current_timestamp', FALSE);
						$this->db->insert('ques_response_mst', $data);
						$lids = $this->db->insert_id();
					}			
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return $lids;
		  }
	 }
	 
	 public function ins_form_response_mst($ans){
		$this->db->trans_start();
					$lids ='';
						$data = array(
								'form_code_fk' => $ans[1],
								'user_id_fk' => $ans[3],
								'slot_id_fk' => $ans[12],
								'status_nm' => 'Active',
								'stat_app' => 'Submitted',
								'ins_user' => $ans[3]
								);
						$this->db->set('ins_dt', 'current_timestamp', FALSE);
						$this->db->insert('form_response_mst', $data);
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return $lids;
		  }
	 }
	 
	 public function fb_form_response_mst($ans){
		 $cs=$ans[4];
		 if($ans[4]==0)
					{
						$cs=null;
					}
		$this->db->trans_start();
					
						$data = array(
								'form_code_fk' => $ans[1],
								'user_id_fk' => $ans[3],
								'l_o_m_id_fk' => $ans[2],
								'slot_id_fk' => $ans[12],
								'project_id_fk' =>$cs,
								'status_nm' => 'Active',
								'stat_app' => 'Submitted',
								'ins_user' => $ans[2]
								);
						$this->db->set('ins_dt', 'current_timestamp', FALSE);
						$this->db->insert('form_response_mst', $data);
		$this->db->trans_complete();
		$lids ='';
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return $lids;
		  }
	 }
	 
	 public function del_ques_response_mst($ans){
		$this->db->trans_start();
					if($ans[0])
					{
						$data = array(
								'status_nm' => 'InActive',
								'upd_user' => $ans[3]
								);
								 $this->db->where('status_nm', 'Active');
								$this->db->where('slot_id_fk', $ans[12]);
						$this->db->set('upd_dt', 'current_timestamp', FALSE);				
				$this->db->where('ques_resp_id IN ('.$ans[0].')',null,false);
				$this->db->update('ques_response_mst', $data);
					}
					if($ans[1]){
						$data = array(
								'status_nm' => 'InActive',
								'upd_user' => $ans[3]
								);
						$this->db->set('upd_dt', 'current_timestamp', FALSE);
						 $this->db->where('status_nm', 'Active');
						 $this->db->where('slot_id_fk', $ans[12]);
						$this->db->where('sec_resp_id IN ('.$ans[1].')',null,false);
						$this->db->update('sec_response_mst', $data);
					}			
		$this->db->trans_complete();
		$lids ='';
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return $lids;
		  }
	 }
	 
	 
	  public function del_full_sec_ques_mst($ans){
		$this->db->trans_start();
					if($ans[0])
					{
						$data = array(
								'status_nm' => 'InActive',
								'upd_user' => $ans[1]
								);
						$this->db->set('upd_dt', 'current_timestamp', FALSE);	
					$this->db->where('status_nm', 'Active');						
					$this->db->where('user_id_fk', $ans[1]);
					$this->db->where('slot_id_fk', $ans[12]);
				$this->db->where('section_id_fk IN ('.$ans[0].')',null,false);
				$this->db->update('ques_response_mst', $data);
					}
					if($ans[0]){
						$data = array(
								'status_nm' => 'InActive',
								'upd_user' => $ans[1]
								);
						$this->db->set('upd_dt', 'current_timestamp', FALSE);
						 $this->db->where('status_nm', 'Active');
						 $this->db->where('user_id_fk', $ans[1]);
						 $this->db->where('slot_id_fk', $ans[12]);
						$this->db->where('section_id_fk IN ('.$ans[0].')',null,false);
						$this->db->update('sec_response_mst', $data);
					}			
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return true;
		  }
	 }
	 
	   public function same_mr_ans($ans){
		$this->db->trans_start();
		$cs=$ans[7].$ans[8];
					if($ans[0] && $ans[7])
					{
						$data = array(
								'status_nm' => 'InActive',
								'upd_user' => $ans[1]
								);
						$this->db->set('upd_dt', 'current_timestamp', FALSE);	
					$this->db->where('status_nm', 'Active');						
					$this->db->where('user_id_fk',$ans[1]);	
					$this->db->where('slot_id_fk', $ans[12]);
				$this->db->where('section_id_fk IN ('.$ans[0].')',null,false);
				$this->db->where('sec_resp_code IN ("'.$cs.'")',null,false);
				$this->db->update('ques_response_mst', $data);
				
				
				$sql='INSERT INTO `ques_response_mst`(`section_id_fk`, slot_id_fk,`sec_resp_code`, `user_id_fk`, `opt_code`, `question_id_fk`, `ques_typ_val`, `ques_resp_desc`, `status_nm`, `ins_user`, `ins_dt`)
											SELECT
												'.$ans[0].','.$ans[12].',
												"'.$cs.'",
												QR.`user_id_fk`,
												QR.`opt_code`,
												QSR.`question_id`,
												(CASE WHEN QR.`opt_code`="type_7"  THEN 2 ELSE QR.`ques_typ_val` END),
												QR.`ques_resp_desc`,
												"Active",
												'.$ans[1].',
												CURRENT_TIMESTAMP
											FROM
												ques_response_mst QR
											JOIN question_mst Q ON
												QR.question_id_fk = Q.question_id AND Q.status_nm = "Active"
											JOIN question_mst QSR ON
												QSR.section_id_fk = '.$ans[0].' 
												AND QSR.dec_sec_cd = Q.dec_sec_cd AND QSR.status_nm = "Active"
											WHERE
												QR.user_id_fk = '.$ans[1].' AND QR.section_id_fk = "'.$ans[2].'" 
												AND QR.`sec_resp_code` = "'.$ans[3].'" AND QR.status_nm = "Active"';
												
												$res = $this->db->query($sql); 
					}
								
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return true;
		  }
	 }
	 public function fd_sec_response_mst($ans){
		 $lids ='';
		 $cs=$ans[2];
					if($ans[2]==0)
					{
						$cs=null;
					}
		
		
		$query = $this->db->get_where('sec_response_mst', array('project_id_fk' => $cs,
								'user_id_fk' => $ans[1],
								'slot_id_fk'=> $ans[12],
								'section_id_fk' => $ans[4],
								'l_o_m_id_fk' => $ans[3]));

        $count = $query->num_rows(); //counting result from query

		$this->db->trans_start();
        if ($count === 0) {
						
						$data = array(
								'project_id_fk' => $cs,
								'user_id_fk' => $ans[1],
								'section_id_fk' => $ans[4],
								'l_o_m_id_fk' => $ans[3],
								'status_nm' => 'Active',
								'slot_id_fk'=> $ans[12],
								'ins_user' => $ans[3]
								);
						$this->db->set('ins_dt', 'current_timestamp', FALSE);
						$this->db->insert('sec_response_mst', $data);
					}			
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return $lids;
		  }
	 }
	 
	 public function ins_sec_response_mst($ans){
				$val=$ans[6];			
				$cs=$ans[2].$ans[5];
					if($ans[5]=='null')
					{
						$cs=null;
					}
		$this->db->trans_start();
					if($ans[0])
					{
						
						$data = array(
								'sec_resp_code' => $cs,
								'section_id_fk' => $ans[1],
								'user_id_fk' => $ans[3],
								'status_nm' => 'Active',
								'upd_user' => $ans[3],
								$val => $ans[4]
								);
						$this->db->set('upd_dt', 'current_timestamp', FALSE);	
						$this->db->where('slot_id_fk', $ans[12]);						
				$this->db->where('sec_resp_id IN ('.$ans[0].')',null,false);
				$this->db->update('sec_response_mst', $data);
				$lids ='';
					}else{
						$data = array(
								'sec_resp_code' => $cs,
								'section_id_fk' => $ans[1],
								'slot_id_fk'=> $ans[12],
								'user_id_fk' => $ans[3],
								'status_nm' => 'Active',
								'ins_user' => $ans[3],
								$val => $ans[4]
								);
						$this->db->set('ins_dt', 'current_timestamp', FALSE);
						$this->db->insert('sec_response_mst', $data);
						$lids = $this->db->insert_id();
					}			
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return $lids;
		  }
	 }
	 
		public function get_param_review($r_ids)
		{		
                    $sql='SELECT P.project_name,CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1))  AS trade_name,
							RP.r_param_id,IFNULL(RP.r_param_name,"Quality Score") AS r_param_name,GROUP_CONCAT(R.response_id) AS r_ids
							FROM response_mst R 
							INNER JOIN activity_mst A 
							ON A.activity_id=R.activity_id_fk 
							INNER JOIN project_mst P 
							ON A.project_id_fk=P.project_id 
							INNER JOIN sub_mst ST
							ON A.sub_id_fk=ST.sub_id 
							INNER JOIN job_mst J
							ON A.job_id_fk=J.job_id 
							LEFT JOIN review_param_mst RP 
							ON A.project_id_fk=RP.project_id_fk AND RP.status_nm="Active" 
							AND CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1)) =RP.trade_name
							WHERE R.response_id IN ('.$r_ids.')
							GROUP BY P.project_name,CONCAT(ST.sub_name," ",SUBSTRING_INDEX(J.job_name, "_", 1)),
							RP.r_param_id,RP.r_param_name;';
                    $res = $this->db->query($sql);                
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }            
        }
		
		public function getAppraisalProjectResponse($userId,$slots_li)
		{
			$del='SELECT
    `Q`.`section_id_fk`,
	1 AS sec_step_name,
	`QR`.`sec_resp_code`,
	`Q`.`question_id`,
	`Q`.`dec_sec_cd`,
    `Q`.`ques_name`,
    `Q`.`ques_tbl_val`,
	Q.opt_code,
    COALESCE(
        QR.ques_typ_val,
        QR.ques_resp_desc
    ) AS ans,
    `ap`.`project_name`,
    1 AS `owner_or_manager_name`
FROM
    `section_mst` `S`
JOIN `sec_response_mst` `SRM` ON
    `SRM`.`section_id_fk` = `S`.`section_id`  AND SRM.slot_id_fk='.$slots_li.' AND `SRM`.`user_id_fk` = "'.$userId.'" 
	AND `SRM`.`status_nm` = "Active" 
JOIN `appraisal_projects` `ap` ON
    `SRM`.`project_id_fk` = `ap`.`app_pro_id`  AND ap.slot_id_fk='.$slots_li.' 
JOIN `question_mst` `Q` ON
    `Q`.`section_id_fk` = `S`.`section_id`  AND Q.slot_id_fk='.$slots_li.'
JOIN `ques_response_mst` `QR` ON
    `QR`.`question_id_fk` = `Q`.`question_id`  AND QR.slot_id_fk='.$slots_li.' AND `QR`.`user_id_fk` = "'.$userId.'" 
	AND `QR`.`status_nm` = "Active" 
WHERE
    `S`.`sec_select` IN ("project")  AND S.slot_id_fk='.$slots_li.' AND `QR`.`user_id_fk` = "'.$userId.'" 
	AND `Q`.`dec_sec_cd` IS NOT NULL 
	ORDER BY `Q`.`question_id`,
	`Q`.`dec_sec_cd`;';
	
	  $res = $this->db->query($del);
		          if($res->result_array())
		          {
		             return $res->result_array();
		          }
		          else{
		            return false;
		          }
		}

		public function getAppraisalUserList($slots_li){
				  $sql='SELECT DISTINCT umst.user_id,umst.full_name,umst.emp_id,umst.user_login_name 
				  FROM form_response_mst FR 
				  JOIN user_mst umst 
				  ON (FR.`user_id_fk`=umst.user_id OR FR.`l_o_m_id_fk`=umst.user_id )
				  WHERE FR.`status_nm`="Active"  AND FR.slot_id_fk='.$slots_li.' AND FR.stat_app="Submitted" 
				  ORDER BY umst.full_name ASC';
                    $res = $this->db->query($sql);
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }

		}
		
		public function getSelfAppraisalUserList($slots_li){
				  $sql='SELECT DISTINCT umst.user_id,umst.full_name,umst.emp_id,umst.user_login_name 
				  FROM form_response_mst FR 
				  JOIN user_mst umst 
				  ON (FR.`user_id_fk`=umst.user_id)
				  WHERE FR.`status_nm`="Active"  AND FR.slot_id_fk='.$slots_li.' AND FR.stat_app="Submitted" 
				  ORDER BY umst.full_name ASC';
                    $res = $this->db->query($sql);
          if($res->result_array())
          {
             return $res->result_array();
          }
          else{
            return false;
          }

		}

		public function getAppraisalUserDetails($uid,$slots_li){
			 $sql='SELECT umst.user_id,umst.full_name,parent_user.full_name as managername,umst.emp_id,umst.user_login_name
			 FROM user_mst umst 
			 LEFT JOIN user_mst AS parent_user ON
    		 parent_user.user_id = umst.manager_id
			 where  umst.user_id="'.$uid.'" ';
                    $res = $this->db->query($sql);
		          if($res->result_array())
		          {
		             return $res->result_array();
		          }
		          else{
		            return false;
		          }

		}

		public function getAppraisalTeachersOnlyResponse($userId,$slots_li)
		{
			$this->db->select('qrm.ques_resp_desc AS para_ans, ovm.opt_name AS radio_ans, qm.ques_name, qtm.q_type_code');
			$this->db->from('question_mst qm');
			$this->db->join('ques_response_mst qrm', 'qrm.question_id_fk = qm.question_id  AND qrm.slot_id_fk='.$slots_li.' AND qrm.status_nm="Active"');
			$this->db->join('ques_type_mst qtm', 'qtm.q_type_id = qm.q_type_id_fk');
			$this->db->join('opt_val_mst ovm', 'ovm.opt_code = qm.opt_code AND FIND_IN_SET(ovm.opt_values, qrm.ques_typ_val)', 'left');
			$this->db->where('qm.section_id_fk', '2');
			$this->db->where('qm.slot_id_fk', $slots_li);
			$this->db->where('qrm.user_id_fk', $userId);
			$sqlQuery = $this->db->get();
   		 	return $sqlQuery->result_array();
		}

		public function getAppraisalMoreInsightResponse($userId,$slots_li)
		{
			$ins='SELECT
						`Q`.`ques_name`,
						Q.opt_code,
						COALESCE(
							QRM.ques_typ_val,
							QRM.ques_resp_desc
						) AS ans,
						`Q`.`ques_tbl_val`
					FROM
						`question_mst` `Q`
					JOIN `ques_response_mst` `QRM` 
					  ON `QRM`.`status_nm` = "Active" AND QRM.slot_id_fk='.$slots_li.' AND `QRM`.`question_id_fk` = `Q`.`question_id` 
					WHERE `Q`.`section_id_fk` = "12" AND Q.slot_id_fk='.$slots_li.' AND `QRM`.`user_id_fk` = "'.$userId.'" ;';

			// $this->db->select('qm.ques_name1, GROUP_CONCAT(ovm.opt_name) as radio_ans, qrm.ques_resp_desc AS para_ans, 
					// qtm.q_type_code, qm.ques_tbl_val');
			// $this->db->from('question_mst qm');
			// $this->db->join('ques_response_mst qrm', 'qrm.status_nm="Active" AND qrm.question_id_fk = qm.question_id');
			// $this->db->join('opt_val_mst ovm', 'ovm.opt_code = qm.opt_code', 'left');
			// $this->db->join('ques_type_mst qtm', 'qtm.q_type_id = qm.q_type_id_fk');
			// $this->db->where('qm.section_id_fk', '12');
			// $this->db->where('qrm.user_id_fk', $userId);
			// $this->db->group_by('qm.question_id');
			// $sqlQuery = $this->db->get();
			// return $sqlQuery->result_array();
			$sqlQuery = $this->db->query($ins);
			return $sqlQuery->result_array();
		}
	
		public function getAppraisalFeedbackgiven($userId,$slots_li)
		{
						$dl='SELECT
				`Q`.`opt_code`,
				Q.dec_sec_cd,
				(CASE WHEN Q.section_id_fk=13 THEN "PO" ELSE "MR" END) AS sec_resp_code,
				1 AS sec_step_name,
				`Q`.`ques_tbl_val`,
				QR.l_o_m_id_fk,
				`Q`.`question_id`,
				COALESCE(
					QR.ques_typ_val,
					QR.ques_resp_desc
				) AS ans,
				`um`.`full_name` AS `owner_or_manager_name`,
				IFNULL(`am`.`project_name`,"OverAll") AS project_name
			FROM
				`ques_response_mst` `QR`
			JOIN `question_mst` `Q` ON
				`Q`.`question_id` = `QR`.`question_id_fk` AND Q.slot_id_fk='.$slots_li.' 
			JOIN form_response_mst DR 
			ON DR.`l_o_m_id_fk` =  QR.`l_o_m_id_fk` AND DR.slot_id_fk='.$slots_li.' 
			AND DR.`user_id_fk` =  QR.`user_id_fk` AND 
			IFNULL(DR.`project_id_fk`,0) = IFNULL(QR.`project_id_fk`,0) AND DR.status_nm="Active" 
			JOIN `user_mst` `um` ON
				`um`.`user_id` = `QR`.`user_id_fk`
			LEFT JOIN `appraisal_projects` `am` ON
				`am`.`app_pro_id` = `QR`.`project_id_fk` AND am.slot_id_fk='.$slots_li.'  AND `am`.`status_nm` = "Active" 
			WHERE 
				`Q`.`section_id_fk` IN ("13", "14")  AND QR.slot_id_fk='.$slots_li.'  AND `QR`.`status_nm` = "Active" 
				AND `QR`.`l_o_m_id_fk` = "'.$userId.'" AND `Q`.`status_nm` = "Active" ';
						$sqlQuery = $this->db->query($dl);
						return $sqlQuery->result_array();
		}
		
		public function getAppraisalFeedbackForEmployee($userId,$slots_li)
		{
			$dl='SELECT
    `Q`.`opt_code`,
	Q.dec_sec_cd,
	(CASE WHEN Q.section_id_fk=13 THEN "PO" ELSE "MR" END) AS sec_resp_code,
	1 AS sec_step_name,
    `Q`.`ques_tbl_val`,
	QR.l_o_m_id_fk,
    `Q`.`question_id`,
    COALESCE(
        QR.ques_typ_val,
        QR.ques_resp_desc
    ) AS ans,
    `um`.`full_name` AS `owner_or_manager_name`,
    IFNULL(`am`.`project_name`,"OverAll") AS project_name
FROM
    `ques_response_mst` `QR`
JOIN `question_mst` `Q` ON
    `Q`.`question_id` = `QR`.`question_id_fk` AND Q.slot_id_fk='.$slots_li.' 
	JOIN form_response_mst DR 
			ON DR.`user_id_fk` =  QR.`user_id_fk` AND DR.slot_id_fk='.$slots_li.'  
			AND DR.`l_o_m_id_fk` =  QR.`l_o_m_id_fk` AND
			IFNULL(DR.`project_id_fk`,0) = IFNULL(QR.`project_id_fk`,0) AND DR.status_nm="Active" 
JOIN `user_mst` `um` ON
    `um`.`user_id` = `QR`.`l_o_m_id_fk`
LEFT JOIN `appraisal_projects` `am` ON
    `am`.`app_pro_id` = `QR`.`project_id_fk` AND am.slot_id_fk='.$slots_li.' AND `am`.`status_nm` = "Active" 
WHERE 
    `Q`.`section_id_fk` IN ("13", "14")  AND QR.slot_id_fk='.$slots_li.'  AND `QR`.`status_nm` = "Active" 
	AND `QR`.`user_id_fk` = "'.$userId.'" AND `Q`.`status_nm` = "Active" ';

			// $this->db->select('qm.opt_code1, qm.ques_tbl_val, qm.ques_name, GROUP_CONCAT(vm.opt_name) as radio_ans, qrm.ques_resp_desc as para_ans, qrm.ques_typ_val, um.full_name AS owner_or_manager_name, am.project_name');
			// $this->db->from('ques_response_mst qrm');
			// $this->db->join('question_mst qm', 'qm.question_id = qrm.question_id_fk');
			// $this->db->join('opt_val_mst vm', 'vm.opt_code = qm.opt_code AND FIND_IN_SET(vm.opt_values, qrm.ques_typ_val)', 'left');
			// $this->db->join('user_mst um', 'um.user_id = qrm.l_o_m_id_fk');
			// $this->db->join('appraisal_projects am', 'am.app_pro_id = qrm.project_id_fk');

			// $this->db->where_in('qm.section_id_fk', array('13', '14'));
			// $this->db->where('qrm.user_id_fk', $userId);
			// $this->db->where('qm.status_nm', 'Active');
			// $this->db->where('qm.ques_name !=', '');
			// $this->db->group_by('qrm.ques_resp_id');


			$sqlQuery = $this->db->query($dl);
			return $sqlQuery->result_array();
		}
		
		public function getAppraisalFeedbackFromEmp($userId,$slots_li)
		{
		$sdl='SELECT
    `Q`.`section_id_fk`,
    PS.sec_step_name,
    `QR`.`sec_resp_code`,
    `Q`.`question_id`,
    `Q`.`dec_sec_cd`,
    `Q`.`ques_name`,
    `Q`.`ques_tbl_val`,
    Q.opt_code,
    COALESCE(
        QR.ques_typ_val,
        QR.ques_resp_desc
    ) AS ans,
    `ap`.`project_name`,
    `U`.`full_name` AS `owner_or_manager_name`
FROM `sec_response_mst` `SRm`
JOIN  `section_mst` `S` 
ON  `SRm`.`section_id_fk` = `S`.`section_id` AND `S`.`status_nm` = "Active"   AND S.slot_id_fk='.$slots_li.' 
JOIN form_response_mst DR 
			ON DR.`user_id_fk` =  SRm.`user_id_fk`  AND DR.slot_id_fk='.$slots_li.'  AND 
			DR.form_code_fk="self_form" AND DR.status_nm="Active" 
JOIN `sec_response_mst` `SR` ON
    `SR`.`section_id_fk` = `S`.`parent_sec`  AND SR.slot_id_fk='.$slots_li.'  AND `SR`.`status_nm` = "Active" 
	AND `SRm`.user_id_fk=SR.user_id_fk 
JOIN `section_mst` `PS` ON
    `SR`.`section_id_fk` = `PS`.`section_id`  AND PS.slot_id_fk='.$slots_li.'  AND `PS`.`status_nm` = "Active" 
JOIN `question_mst` `Q` ON
    `Q`.`section_id_fk` = `SRm`.`section_id_fk`  AND Q.slot_id_fk='.$slots_li.' 
JOIN `ques_response_mst` `QR` ON 
    `QR`.`question_id_fk` = `Q`.`question_id` AND `QR`.`status_nm` = "Active" 
	AND `SRm`.`sec_resp_code` = `QR`.`sec_resp_code`  AND QR.slot_id_fk='.$slots_li.' 
	AND `SRm`.`section_id_fk` = `QR`.`section_id_fk` 
	AND `SRm`.user_id_fk=QR.user_id_fk
JOIN `user_mst` `U` ON
    `U`.`user_id` = `SRm`.`user_id_fk` 
	LEFT JOIN `appraisal_projects` `ap` 
	ON   `SR`.`project_id_fk` = `ap`.`app_pro_id`  AND ap.slot_id_fk='.$slots_li.' 
WHERE `SRm`.`status_nm` = "Active"  AND SRm.slot_id_fk='.$slots_li.'  AND `S`.`sec_select` IN ("owner", "manager") 
AND (SRm.L_O_id_fk = "'.$userId.'" OR SRm.MR_id_fk= "'.$userId.'") AND `Q`.`dec_sec_cd` IS NOT NULL';
			$sqlQuery = $this->db->query($sdl);
			return $sqlQuery->result_array();
		}

		public function getAppraisalFeedbackForPoRm($userId,$slots_li)
		{
			$sdl='SELECT
    `Q`.`section_id_fk`,
	PS.sec_step_name,
	`QR`.`sec_resp_code`,
	`Q`.`question_id`,
	`Q`.`dec_sec_cd`,
    `Q`.`ques_name`,
    `Q`.`ques_tbl_val`,
	Q.opt_code,
    COALESCE(
        QR.ques_typ_val,
        QR.ques_resp_desc
    ) AS ans,
    `ap`.`project_name`,
    `U`.`full_name` AS `owner_or_manager_name`
FROM
    `section_mst` `S`
JOIN `sec_response_mst` `SR` ON
    `SR`.`section_id_fk` = `S`.`parent_sec`  AND SR.slot_id_fk='.$slots_li.'  AND `SR`.`user_id_fk` = "'.$userId.'" 
	AND `SR`.`status_nm` = "Active" 
JOIN `section_mst` `PS` ON
    `SR`.`section_id_fk` = `PS`.`section_id`  AND PS.slot_id_fk='.$slots_li.' 
	JOIN form_response_mst DR 
			ON DR.`user_id_fk` =  SR.`user_id_fk`  AND DR.slot_id_fk='.$slots_li.'  AND 
			DR.form_code_fk="self_form" AND DR.status_nm="Active"  
JOIN `appraisal_projects` `ap` ON
    `SR`.`project_id_fk` = `ap`.`app_pro_id`  AND ap.slot_id_fk='.$slots_li.' 
JOIN `question_mst` `Q` ON
    `Q`.`section_id_fk` = `S`.`section_id`  AND Q.slot_id_fk='.$slots_li.' 
JOIN `ques_response_mst` `QR` ON
    `QR`.`question_id_fk` = `Q`.`question_id`  AND QR.slot_id_fk='.$slots_li.'  AND `QR`.`user_id_fk` = "'.$userId.'" 
	AND `QR`.`status_nm` = "Active" 
JOIN `sec_response_mst` `SRm` ON
    `SRm`.`sec_resp_code` = `QR`.`sec_resp_code`  AND SRm.slot_id_fk='.$slots_li.'  AND `SRm`.`user_id_fk` = "'.$userId.'" 
	AND `SRm`.`status_nm` = "Active" 
JOIN `user_mst` `U` ON
    `U`.`user_id` = COALESCE(SRm.L_O_id_fk, SRm.MR_id_fk)
WHERE
    `S`.`sec_select` IN ("owner", "manager")  AND S.slot_id_fk='.$slots_li.'  AND `QR`.`user_id_fk` = "'.$userId.'" 
	AND `Q`.`dec_sec_cd` IS NOT NULL 
	ORDER BY `Q`.`question_id`,
	`Q`.`dec_sec_cd`;';

			

			$sqlQuery = $this->db->query($sdl);
			return $sqlQuery->result_array();
		}
}
?>