<?php
class Import_model extends CI_model{
	public function update_usr_stat($myrows,$user_id,$sel_status)
	{
		$this->db->trans_start();
		$data = array(
							'upd_user' => $user_id,
							'status_nm' => $sel_status
							);
					$this->db->where_in('user_login_name', $myrows);
					$this->db->set('upd_dt', 'current_timestamp', FALSE);
					$this->db->update('user_mst', $data);
			$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return "success";
		  }
	}


	public function import_csvd($ans,$user_id){

		 if( $this->db->table_exists('data_import') == FALSE )
		 {
		 $this->db->trans_start();

				$sql3 = 'CREATE TABLE `data_import`(
								project_name VARCHAR(200) NOT NULL,
								task_name VARCHAR(200) NOT NULL,
								sub_name VARCHAR(500) NOT NULL,
								job_name VARCHAR(500) DEFAULT NULL,
								wu_name VARCHAR(200) DEFAULT NULL,
								is_quantify VARCHAR(10) DEFAULT NULL,
								tgt_val  DECIMAL(10,2) DEFAULT NULL,
								start_dt TIMESTAMP NULL DEFAULT NULL,
								dept_id INT(11),
								project_id INT(11),
								task_id INT(11),
								sub_id INT(11),
								job_id INT(11),
								wu_id INT(11),
								activity_id INT(11)
							) ENGINE = InnoDB; ';
					$this->db->query($sql3);
				$this->db->insert_batch('data_import', $ans);
				$sql4="INSERT INTO project_mst(dept_id_fk,project_name,ins_user,ins_dt,status_nm)
							SELECT DISTINCT A.dept_id,A.project_name,".$user_id.",current_timestamp,'Active' from data_import A
							LEFT JOIN project_mst P
							ON P.dept_id_fk=A.dept_id AND
								A.project_name=P.project_name AND P.status_nm='Active'
							WHERE P.project_id IS NULL AND IFNULL(A.project_name,'')<>'';";
				$this->db->query($sql4);

				$sql5="INSERT INTO task_mst(dept_id_fk,task_name,ins_user,ins_dt,status_nm)
							SELECT DISTINCT A.dept_id,A.task_name,".$user_id.",current_timestamp,'Active' from data_import A
							LEFT JOIN task_mst P
							ON P.dept_id_fk=A.dept_id AND A.task_name=P.task_name AND P.status_nm='Active'
							WHERE P.task_id IS NULL;";
				$this->db->query($sql5);
				$sql6="INSERT INTO sub_mst(dept_id_fk,sub_name,ins_user,ins_dt,status_nm)
							SELECT DISTINCT A.dept_id,A.sub_name,".$user_id.",current_timestamp,'Active' from data_import A
							LEFT JOIN sub_mst P
							ON P.dept_id_fk=A.dept_id AND A.sub_name=P.sub_name AND P.status_nm='Active'
							WHERE P.sub_id IS NULL;";
				$this->db->query($sql6);

				$sql7="INSERT INTO job_mst(dept_id_fk,job_name,ins_user,ins_dt,status_nm)
							SELECT DISTINCT A.dept_id,A.job_name,".$user_id.",current_timestamp,'Active' from data_import A
							LEFT JOIN job_mst P
							ON P.dept_id_fk=A.dept_id AND A.job_name=P.job_name AND P.status_nm='Active'
							WHERE P.job_id IS NULL AND IFNULL(A.job_name,'')<>'' ;";
				$this->db->query($sql7);

				$sql8="INSERT INTO work_unit_mst(dept_id_fk,wu_name,is_quantify,ins_user,ins_dt,status_nm)
							SELECT DISTINCT A.dept_id,A.wu_name,A.is_quantify,".$user_id.",current_timestamp,'Active' from data_import A
							LEFT JOIN work_unit_mst P
							ON P.dept_id_fk=A.dept_id AND A.wu_name=P.wu_name AND P.status_nm='Active'
							WHERE P.wu_id IS NULL;";
				$this->db->query($sql8);

				$sql9="UPDATE data_import A INNER JOIN project_mst P
							ON P.dept_id_fk=A.dept_id AND A.project_name = P.project_name
							AND P.status_nm='Active'
						SET A.project_id = P.project_id;";
				$this->db->query($sql9);

				$sql10="UPDATE data_import A INNER JOIN task_mst P
							ON P.dept_id_fk=A.dept_id AND A.task_name = P.task_name AND P.status_nm='Active'
						SET A.task_id = P.task_id;";
				$this->db->query($sql10);

				$sql11="UPDATE data_import A INNER JOIN sub_mst P
							ON P.dept_id_fk=A.dept_id AND A.sub_name = P.sub_name AND P.status_nm='Active'
						SET A.sub_id = P.sub_id;";
				$this->db->query($sql11);

				$sql12="UPDATE data_import A INNER JOIN job_mst P
							ON P.dept_id_fk=A.dept_id AND A.job_name = P.job_name AND P.status_nm='Active'
						SET A.job_id = P.job_id;";
				$this->db->query($sql12);

				$sql13="UPDATE data_import A INNER JOIN work_unit_mst P
							ON P.dept_id_fk=A.dept_id AND A.wu_name = P.wu_name AND P.status_nm='Active'
						SET A.wu_id = P.wu_id;";
				$this->db->query($sql13);

				$sql14="INSERT INTO activity_mst(dept_id_fk,activity_name,project_id_fk,task_id_fk,sub_id_fk,job_id_fk,wu_id_fk,ins_user,ins_dt,status_nm)
							SELECT DISTINCT A.dept_id,CONCAT_WS('_',A.project_name,A.task_name,A.sub_name,A.job_name),A.project_id,A.task_id,A.sub_id,A.job_id,A.wu_id,".$user_id.",current_timestamp,'Active'
							from data_import A
							LEFT JOIN activity_mst P
							ON P.dept_id_fk=A.dept_id AND
							A.project_id=P.project_id_fk AND
							A.task_id=P.task_id_fk AND
							A.sub_id=P.sub_id_fk AND
							A.job_id=P.job_id_fk AND
							A.wu_id=P.wu_id_fk AND
							P.status_nm='Active'
							WHERE P.activity_id IS NULL AND (A.project_id IS NOT NULL AND A.task_id IS NOT NULL AND 
							A.sub_id IS NOT NULL AND A.job_id IS NOT NULL AND A.wu_id IS NOT NULL);";
				$this->db->query($sql14);

				$sql15="UPDATE data_import A INNER JOIN activity_mst P
							ON P.dept_id_fk=A.dept_id AND
							A.project_id=P.project_id_fk AND
							A.task_id=P.task_id_fk AND
							A.sub_id=P.sub_id_fk AND
							A.job_id=P.job_id_fk AND
							A.wu_id=P.wu_id_fk AND
							P.status_nm='Active'
							SET A.activity_id=P.activity_id;";
				$this->db->query($sql15);

				$sql16="UPDATE std_tgt_mst S INNER JOIN data_import A
			ON S.activity_id_fk=A.activity_id AND S.start_dt<=A.start_dt AND S.end_dt IS NULL AND S.status_nm='Active'
			SET S.end_dt=A.start_dt + INTERVAL (-1) SECOND , upd_user=".$user_id.",upd_dt=current_timestamp;";
				$this->db->query($sql16);

				$sql19="UPDATE response_mst R INNER JOIN data_import A
			ON R.activity_id_fk=A.activity_id AND R.day_val>=A.start_dt AND R.status_nm='Active'
			SET R.std_tgt_val=A.tgt_val,R.equiv_xp=(R.work_comp/A.tgt_val)*10 ,R.upd_user=".$user_id.",R.upd_dt=current_timestamp;";
				$this->db->query($sql19);

				$sql17="INSERT INTO std_tgt_mst(activity_id_fk,tgt_val,start_dt,status_nm,ins_user,ins_dt)
			SELECT DISTINCT A.activity_id,A.tgt_val,A.start_dt,'Active',".$user_id.",current_timestamp
			 FROM data_import A LEFT JOIN std_tgt_mst S
			ON S.activity_id_fk=A.activity_id AND S.end_dt IS NULL AND S.status_nm='Active'
			WHERE S.activity_id_fk IS NULL AND A.activity_id IS NOT NULL ;";
				$this->db->query($sql17);

				$sql18='DROP TABLE data_import;';
				$this->db->query($sql18);

				$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
		  {
			  $sql18='DROP TABLE data_import;';
				$this->db->query($sql18);
			 return false;
		  }
		  else{
			 return "success";
		  }
		 }
		else{
			return "Upload already in use. Please try after some time.";
		}
	 }

	 public function import_io_times($ans,$user_id){
			$return['status'] = false;
		 if( $this->db->table_exists('in_out_csv') == FALSE )
		 {
		 $this->db->trans_start();

				$sql3 = 'CREATE TABLE in_out_csv
						(
						 `date_val` DATE NULL,
						 `email_id` varchar(200) DEFAULT NULL,
						 `department` varchar(255) DEFAULT NULL,
						 `unit` varchar(255) DEFAULT NULL,
						 `in_time` decimal(10,2) DEFAULT NULL,
						 `out_time` decimal(10,2) DEFAULT NULL,
						 `esp_wrk_hrs` decimal(10,2) DEFAULT NULL,
						 `break_hrs` decimal(10,2) DEFAULT NULL,
						 `esp_status` varchar(20) DEFAULT NULL,
						 `late_status` varchar(20) DEFAULT NULL,
						 `total_days` decimal(10,2) DEFAULT NULL,
						 `attendance_cnt` decimal(10,2) DEFAULT NULL,
						 `team` varchar(255) DEFAULT NULL,
						 `week_off` decimal(10,2) DEFAULT NULL,
						 `sapience_on` decimal(10,2) DEFAULT NULL,
						 `sapience_off` decimal(10,2) DEFAULT NULL,
						 `unaccounted_time` decimal(10,2) DEFAULT NULL,
						 `time_on_private` decimal(10,2) DEFAULT NULL,
						 `ins_user` int(11) DEFAULT NULL,
						 `user_id`  int(11) DEFAULT NULL,
						 `median_val`  decimal(10,2) DEFAULT NULL,
						 `is_dup`  int(11) DEFAULT 0 
						)ENGINE=InnoDB; ';
					$this->db->query($sql3);

				$this->db->insert_batch('in_out_csv', $ans);

				
				$sql9="UPDATE in_out_csv A INNER JOIN user_mst P
							ON P.user_login_name=A.email_id
						SET A.user_id = P.user_id;";
				$this->db->query($sql9);

				$sql1184="UPDATE in_out_csv I JOIN (
							select date_val,user_id FROM in_out_csv where date_val IS NOT NULL AND user_id is not null 
							group by date_val,user_id having count(*)>1
								)O 
							   ON I.date_val=O.date_val AND I.user_id=O.user_id 
							 SET I.is_dup=1;";
				$this->db->query($sql1184);
				
				$sql116="UPDATE `in_out_upload` A 
							INNER JOIN (SELECT DISTINCT date_val,user_id FROM in_out_csv 
							WHERE date_val is not null AND IFNULL(is_dup,0)=0  
				  AND IFNULL(email_id,'')<>'' AND user_id IS NOT NULL )B 
							ON A.date_val=B.date_val AND A.user_id=B.user_id 
							SET A.status_nm='InActive'";
				$this->db->query($sql116);
				
				$sql117="DELETE FROM `in_out_upload` WHERE status_nm='InActive'";
				$this->db->query($sql117);
						

				$sql17="INSERT INTO `in_out_upload`(`date_val`, `email_id`, `department`, `unit`, `in_time`, `out_time`, `esp_wrk_hrs`, `break_hrs`, `esp_status`, `late_status`, `total_days`,`attendance_cnt`, `sapience_on`, `sapience_off`, `team`, `week_off`, `unaccounted_time`, `time_on_private`, `status_nm`, `ins_user`, `ins_dt`,`user_id`)
			 SELECT `date_val`, `email_id`, `department`, `unit`, `in_time`, `out_time`, `esp_wrk_hrs`, `break_hrs`, `esp_status`, `late_status`, `total_days`,`attendance_cnt`, `sapience_on`, `sapience_off`, `team`, `week_off`, `unaccounted_time`, `time_on_private`, 'Active',
				 `ins_user`,current_timestamp ,user_id
				  FROM in_out_csv WHERE date_val is not null AND IFNULL(is_dup,0)=0  
				  AND IFNULL(email_id,'')<>'' AND user_id IS NOT NULL;";
				$this->db->query($sql17);

				$qryCsvErrors = "SELECT `date_val` AS Date, `email_id` AS `Email ID`, `department` AS `Department`, `unit` AS `Unit`, `in_time` AS `Punch In Time`, `out_time` AS `Punch Out Time`, `esp_wrk_hrs` AS `Total Wrk Hrs`, `break_hrs` AS `Break hrs`, `esp_status` AS `Attendance Status`, `late_status` AS `Late Staus`, `total_days` AS `Total Days`,`attendance_cnt` AS `Attendance Count`, `team` AS `Team`, `week_off` AS `Weekoff`,`sapience_on` AS `Sapience-On PC`, `sapience_off` AS `Sapience-Off PC`, `unaccounted_time` AS `Unaccounted Time`, `time_on_private` AS `Time on Private (G)`, 
								CONCAT(
								 	(CASE WHEN date_val IS NULL THEN 'Date is blank.' ELSE '' END),
									(CASE WHEN IFNULL(is_dup,0)=1 THEN 'Multiple record exist.' ELSE '' END),
									(CASE WHEN IFNULL(email_id,'')='' THEN 'Email is blank.' ELSE '' END),
									(CASE WHEN user_id IS NULL THEN 'User doesn\'t exists.' ELSE '' END)
								 )
								  AS 'ERRORS'
							FROM in_out_csv
							WHERE date_val is null 
							OR IFNULL(is_dup,0)=1  
							OR IFNULL(email_id,'')='' 
							OR user_id IS NULL;	";

			//Errors of a csv
			$csvErrors = $this->db->query($qryCsvErrors)->result_array();
			
				$sql18='DROP TABLE IF EXISTS in_out_csv;';
				$this->db->query($sql18);

				$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
					  {
						$return['status'] = false;
					  }
					  else{
						$return['status'] = true;
				$return['errors'] = $csvErrors;
					  }
		 }
		else{
			$return['status'] = false;
			$return['errors'] = "Upload already in use. Please try after some time.";
		}
		
			return( $return );
	 }

	 public function import_utility_closer_csv($ary,$userId){
	 	$res['status'] = false;
    		if($this->db->table_exists('data_import_util_clo')==FALSE){
	 			   $this->db->trans_start();
	 			   $sql3 = 'CREATE TABLE `data_import_util_clo`(
								project VARCHAR(200) NOT NULL,
								task VARCHAR(200) NOT NULL,
								sub_task VARCHAR(500) NOT NULL,
								job VARCHAR(500) DEFAULT NULL,
								work_unit VARCHAR(200) DEFAULT NULL,
								is_closed VARCHAR(200) DEFAULT NULL,
								dept_id INT(11),
								project_id INT(11),
								task_id INT(11),
								sub_id INT(11),
								job_id INT(11),
								wu_id INT(11)
							)ENGINE = InnoDB; ';
					$this->db->query($sql3);
				    $this->db->insert_batch('data_import_util_clo', $ary);

	     						$sql9="UPDATE data_import_util_clo A INNER JOIN project_mst P
								ON P.dept_id_fk=A.dept_id AND A.project = P.project_name
								AND P.status_nm='Active'
										SET A.project_id = P.project_id;";
								$this->db->query($sql9);

								$sql10="UPDATE data_import_util_clo A INNER JOIN task_mst P
											ON P.dept_id_fk=A.dept_id AND A.task = P.task_name AND P.status_nm='Active'
										SET A.task_id = P.task_id;";
								$this->db->query($sql10);

								$sql11="UPDATE data_import_util_clo A INNER JOIN sub_mst P
											ON P.dept_id_fk=A.dept_id AND A.sub_task = P.sub_name AND P.status_nm='Active'
										SET A.sub_id = P.sub_id;";
								$this->db->query($sql11);

								$sql12="UPDATE data_import_util_clo A INNER JOIN job_mst P
											ON P.dept_id_fk=A.dept_id AND A.job = P.job_name AND P.status_nm='Active'
										SET A.job_id = P.job_id;";
								$this->db->query($sql12);

								$sql13="UPDATE data_import_util_clo A INNER JOIN work_unit_mst P
											ON P.dept_id_fk=A.dept_id AND A.work_unit = P.wu_name AND P.status_nm='Active'
										SET A.wu_id = P.wu_id;";
								$this->db->query($sql13);


								$sql14="UPDATE activity_mst P INNER JOIN data_import_util_clo A
								ON P.dept_id_fk=A.dept_id AND
								A.project_id=P.project_id_fk AND
								A.task_id=P.task_id_fk AND
								A.sub_id=P.sub_id_fk AND
								A.job_id=P.job_id_fk AND
								A.wu_id=P.wu_id_fk
								SET P.is_closed=A.is_closed,
									P.upd_user=$userId,
									P.upd_dt=current_timestamp;";
								$this->db->query($sql14);

						$sql18='DROP TABLE data_import_util_clo;';
						$this->db->query($sql18);
						$this->db->trans_complete();
					if($this->db->trans_status() === FALSE)
					  {
						$res['message'] = "Some error";
					  }
					  else{
					  	$res['status'] = true;
						$res['message'] = "Status updated successfully.";
					  }
				}
				else{
					$res['message'] = "Upload already in use. Please try after some time.";
				}
				
				return( $res );
    }

	 public function import_utilitycsv($ary,$user_id){
		 $return['status'] = false;
	 			if($this->db->table_exists('data_import_util')==FALSE){
	 			   $this->db->trans_start();
	 			   $sql3 = 'CREATE TABLE `data_import_util`(
								project_name VARCHAR(200) NOT NULL,
								task_name VARCHAR(200) NOT NULL,
								sub_name VARCHAR(500) NOT NULL,
								job_name VARCHAR(500) DEFAULT NULL,
								wu_name VARCHAR(200) DEFAULT NULL,
								is_quantify VARCHAR(10) DEFAULT NULL,
								tgt_val  DECIMAL(10,2) DEFAULT NULL,
								start_dt TIMESTAMP NULL DEFAULT NULL,
								dept_id INT(11),
								project_id INT(11),
								task_id INT(11),
								sub_id INT(11),
								job_id INT(11),
								wu_id INT(11),
								activity_id INT(11)
							) ENGINE = InnoDB; ';
					$this->db->query($sql3);
				    $this->db->insert_batch('data_import_util', $ary);
				    // $this->db->select('*');
					// $this->db->from('data_import_util');
					// $sqlQuery = $this->db->get();
					// $resultSet = $sqlQuery->result_array();

					// foreach($resultSet as $rs){

     						// if ($this->projectExists($rs['project_name'],$rs['task_name'],$rs['sub_name'],$rs['job_name'],$rs['wu_name'])==true){


	     						$sql9="UPDATE data_import_util A INNER JOIN project_mst P
								ON P.dept_id_fk=A.dept_id AND A.project_name = P.project_name
								AND P.status_nm='Active'
										SET A.project_id = P.project_id;";
								$this->db->query($sql9);

								$sql10="UPDATE data_import_util A INNER JOIN task_mst P
											ON P.dept_id_fk=A.dept_id AND A.task_name = P.task_name AND P.status_nm='Active'
										SET A.task_id = P.task_id;";
								$this->db->query($sql10);

								$sql11="UPDATE data_import_util A INNER JOIN sub_mst P
											ON P.dept_id_fk=A.dept_id AND A.sub_name = P.sub_name AND P.status_nm='Active'
										SET A.sub_id = P.sub_id;";
								$this->db->query($sql11);

								$sql12="UPDATE data_import_util A INNER JOIN job_mst P
											ON P.dept_id_fk=A.dept_id AND A.job_name = P.job_name AND P.status_nm='Active'
										SET A.job_id = P.job_id;";
								$this->db->query($sql12);

								$sql13="UPDATE data_import_util A INNER JOIN work_unit_mst P
											ON P.dept_id_fk=A.dept_id AND A.wu_name = P.wu_name AND P.status_nm='Active'
										SET A.wu_id = P.wu_id;";
								$this->db->query($sql13);

     						// }
     						// else{
	     						// $sql111="delete from data_import where project_name='".$rs['project_name']."'";
								// $this->db->query($sql111);

     						// }

     				// }

					// $sql111="DELETE from data_import where project_name='".$rs['project_name']."'";
					// $this->db->query($sql111);

     					$sql14="INSERT INTO activity_mst(dept_id_fk,activity_name,project_id_fk,task_id_fk,sub_id_fk,job_id_fk,wu_id_fk,ins_user,ins_dt,status_nm)
								SELECT DISTINCT A.dept_id,CONCAT_WS('_',A.project_name,A.task_name,A.sub_name,A.job_name),A.project_id,A.task_id,A.sub_id,A.job_id,A.wu_id,".$user_id.",current_timestamp,'Active'
								from data_import_util A
								LEFT JOIN activity_mst P
								ON P.dept_id_fk=A.dept_id AND
								A.project_id=P.project_id_fk AND
								A.task_id=P.task_id_fk AND
								A.sub_id=P.sub_id_fk AND
								A.job_id=P.job_id_fk AND
								A.wu_id=P.wu_id_fk AND
								P.status_nm='Active'
								WHERE P.activity_id IS NULL
								AND A.dept_id IS NOT NULL
								AND A.project_id IS NOT NULL
								AND A.sub_id IS NOT NULL
								AND A.job_id IS NOT NULL
								AND A.wu_id IS NOT NULL
								;";
								$this->db->query($sql14);

								$sql15="UPDATE data_import_util A INNER JOIN activity_mst P
								ON P.dept_id_fk=A.dept_id AND
								A.project_id=P.project_id_fk AND
								A.task_id=P.task_id_fk AND
								A.sub_id=P.sub_id_fk AND
								A.job_id=P.job_id_fk AND
								A.wu_id=P.wu_id_fk AND
								P.status_nm='Active'
								SET A.activity_id=P.activity_id;";
								$this->db->query($sql15);

								$sql16="UPDATE std_tgt_mst S INNER JOIN data_import_util A
								ON S.activity_id_fk=A.activity_id AND S.start_dt<=A.start_dt AND S.end_dt IS NULL AND S.status_nm='Active'
								SET S.end_dt=A.start_dt + INTERVAL (-1) SECOND , upd_user=".$user_id.",upd_dt=current_timestamp;";
									$this->db->query($sql16);

									$sql19="UPDATE response_mst R INNER JOIN data_import_util A
								ON R.activity_id_fk=A.activity_id AND R.day_val>=A.start_dt AND R.status_nm='Active'
								SET R.std_tgt_val=A.tgt_val,R.equiv_xp=(R.work_comp/A.tgt_val)*10 ,R.upd_user=".$user_id.",R.upd_dt=current_timestamp;";
									$this->db->query($sql19);

									$sql17="INSERT INTO std_tgt_mst(activity_id_fk,tgt_val,start_dt,status_nm,ins_user,ins_dt)
								SELECT DISTINCT A.activity_id,A.tgt_val,A.start_dt,'Active',".$user_id.",current_timestamp
								 FROM data_import_util A LEFT JOIN std_tgt_mst S
								ON S.activity_id_fk=A.activity_id AND S.end_dt IS NULL AND S.status_nm='Active'
								WHERE S.activity_id_fk IS NULL;";
									$this->db->query($sql17);
									
									$qryCsvErrors = "SELECT project_name AS 'Project',
									task_name AS 'Task',
									sub_name AS 'Sub Task',
									job_name AS 'Job',
									wu_name AS 'Work Unit',
									is_quantify AS 'Is Quantify',
									tgt_val AS 'Tgt',
									start_dt AS 'Start Date',
								CONCAT(
								 	(CASE WHEN IFNULL(project_id,0)=0 THEN 'Project doesn\'t exists.' ELSE '' END),
									(CASE WHEN IFNULL(task_id,0)=0 THEN 'Task doesn\'t exists.' ELSE '' END),
									(CASE WHEN IFNULL(sub_id,0)=0 THEN 'SubTask doesn\'t exists.' ELSE '' END),
									(CASE WHEN IFNULL(job_id,0)=0 THEN 'Job doesn\'t exists.' ELSE '' END),
									(CASE WHEN IFNULL(wu_id,0)=0 THEN 'Work Unit doesn\'t exists.' ELSE '' END)
								 )
								  AS 'ERRORS'
							FROM data_import_util
							WHERE (IFNULL(project_id,0)=0)
							OR (IFNULL(task_id,0) = 0) 
							OR (IFNULL(sub_id,0) = 0) 
							OR (IFNULL(job_id,0) = 0) 
							OR (IFNULL(wu_id,0) = 0) 
							";

			//Errors of a csv
			$csvErrors = $this->db->query($qryCsvErrors)->result_array();
			
						 $sql18='DROP TABLE data_import_util;';
						 $this->db->query($sql18);
					$this->db->trans_complete();
					if($this->db->trans_status() === FALSE)
					  {
						$return['status'] = false;
					  }
					  else{
						$return['status'] = true;
				$return['errors'] = $csvErrors;
					  }
				}
				else{
					$return['status'] = false;
				}
			return $return;

    }

	 public function  ImportContentMidCsv($ary,$d_id){
		// print_r($this->db->table_exists('contentmids'));
    		//if($this->db->table_exists('contentmids')==False)
			{
				 $sql35='DROP TABLE IF EXISTS contentmids;';
			$this->db->query($sql35);
    			$this->db->trans_start();
    			$sql='CREATE TABLE contentmids
    				(
					`gen_id` int(11) NOT NULL AUTO_INCREMENT,
    					`activity_id` varchar(50)DEFAULT NULL,
    					`project_name`varchar(50)DEFAULT NULL,
    					`task_name`varchar(50)DEFAULT NULL,
    					`sub_name`varchar(50)DEFAULT NULL,
    					`job_name`varchar(50)DEFAULT NULL,
    					`dept_id`int(12)DEFAULT null,
						 project_id INT(11),
						 task_id INT(11),
						 sub_id INT(11),
					   	 job_id INT(11),
    					`year`varchar(50)DEFAULT null,
    					`syllabus`varchar(50)DEFAULT NULL,
    					`grade`varchar(50)DEFAULT NULL,
    					`subject`varchar(50) DEFAULT NULL,
    					`chapter`varchar(250)DEFAULT null,
    					`sub_div` varchar(50)DEFAULT NULL,
    					`div_num` varchar(250)DEFAULT NULL,
						`sec_sub_div` varchar(50)DEFAULT NULL,
    					`sec_div_num` varchar(250)DEFAULT NULL,
    					`third_sub_div` varchar(50)DEFAULT NULL,
    					`third_div_num` varchar(250)DEFAULT NULL,
    					`status_nm` varchar(50)DEFAULT NULL,
    					`ins_user` int(11),
						PRIMARY KEY(`gen_id`)
    				)ENGINE=InnoDB;';
			$this->db->query($sql);
    		$this->db->insert_batch('contentmids',$ary);

			$sql1="UPDATE contentmids A INNER JOIN project_mst P
			ON P.dept_id_fk=A.dept_id AND A.project_name = P.project_name
			AND P.status_nm='Active'
			SET A.project_id = P.project_id;";
			$this->db->query($sql1);

			$sql2="UPDATE contentmids A INNER JOIN task_mst P
						ON P.dept_id_fk=A.dept_id AND A.task_name = P.task_name AND P.status_nm='Active'
					SET A.task_id = P.task_id;";
			$this->db->query($sql2);

			$sql3="UPDATE contentmids A INNER JOIN sub_mst P
						ON P.dept_id_fk=A.dept_id AND A.sub_name = P.sub_name AND P.status_nm='Active'
					SET A.sub_id = P.sub_id;";
			$this->db->query($sql3);

			$sql4="UPDATE contentmids A INNER JOIN job_mst P
						ON P.dept_id_fk=A.dept_id AND A.job_name = P.job_name AND P.status_nm='Active'
					SET A.job_id = P.job_id;";
			$this->db->query($sql4);

			$sql5="UPDATE contentmids A INNER JOIN activity_mst P
							ON P.dept_id_fk=A.dept_id AND
							A.project_id=P.project_id_fk AND
							A.task_id=P.task_id_fk AND
							A.sub_id=P.sub_id_fk AND
							A.job_id=P.job_id_fk AND
							P.status_nm='Active' AND 
							IFNULL(P.is_closed,'N')='N' 
							SET A.activity_id=P.activity_id;";
				$this->db->query($sql5);
				
				$sql63="UPDATE mid_gen A 
							INNER JOIN (SELECT DISTINCT activity_id FROM contentmids WHERE activity_id IS NOT NULL)B 
							ON A.activity_id_fk=B.activity_id 
							SET A.status_nm='InActive' 
							WHERE A.status_nm='Active';";
				$this->db->query($sql63);
				
			
				
    		$sql2="INSERT INTO mid_gen(activity_id_fk,dept_id_fk,year,syllabus,grade,subject ,chapter,sub_div,div_num,sec_sub_div,sec_div_num,third_sub_div,third_div_num,status_nm ,ins_user,ins_dt)
			SELECT DISTINCT A.activity_id,A.dept_id,A.year,A.syllabus,A.grade,A.subject,A.chapter,A.sub_div,A.div_num,A.sec_sub_div,A.sec_div_num,A.third_sub_div,A.third_div_num,A.status_nm,A.ins_user,current_timestamp
			 FROM contentmids A  WHERE A.activity_id IS NOT NULL;";
			$this->db->query($sql2);
			
			$user_id = $this->session->userdata('user_id');
				$sql63="UPDATE hier_act_log A 
							INNER JOIN (SELECT DISTINCT dept_id,project_id,task_id,sub_id,job_id FROM contentmids 
							WHERE activity_id IS NOT NULL)B 
							ON A.dept_id_fk=B.dept_id 
							AND A.project_id_fk=B.project_id 
							AND A.task_id_fk=B.task_id 
							AND A.sub_task_id_fk=B.sub_id 
							AND A.job_id_fk=B.job_id 
							SET A.status_nm='InActive' ,A.upd_dt=current_timestamp,A.upd_user='.$user_id.' 
							WHERE A.status_nm='Active';";
				$this->db->query($sql63);
				
			// FOR ALLOCATION QUERIES
			$sql_1="DROP TABLE IF EXISTS im_temp_mid_conv_act";
				$this->db->query($sql_1);	

			$sql_2="CREATE TABLE im_temp_mid_conv_act
					(
					  `t_id` int(11) NOT NULL AUTO_INCREMENT,
					  `activity_id_fk` int(11) DEFAULT NULL, 
					  `dept_id_fk` int(11) DEFAULT NULL, 
					  `mid_code` varchar(100) DEFAULT NULL,
					  `project_id_fk` int(11) DEFAULT NULL, 
					  `task_id_fk` int(11) DEFAULT NULL, 
					  `sub_id_fk` int(11) DEFAULT NULL, 
					  `job_id_fk` int(11) DEFAULT NULL, 
					  `hierarchy_id_fk` int(11) DEFAULT NULL, 
					  `hier_act_log` int(11) DEFAULT NULL, 
					  PRIMARY KEY(`t_id`)
					) ENGINE=InnoDB ;";
					
			$sql_4='DROP TABLE IF EXISTS im_temp_mid_conv;';
					$this->db->query($sql_4);
					$sql_5='CREATE TABLE im_temp_mid_conv 
							(
							  `t_id` int(11) NOT NULL AUTO_INCREMENT,
							  `dept_id_fk` int(11) DEFAULT NULL, 
							  `mid_code` varchar(100) DEFAULT NULL,
							  `level_id_fk` int(11) DEFAULT NULL, 
							  `hierarchy_id_fk` int(11) DEFAULT NULL,
							  `p_mid_code` int(11) DEFAULT NULL,
							  `p_hierarchy_id` int(11) DEFAULT NULL,
							  PRIMARY KEY(`t_id`)
							) ENGINE=InnoDB ;';
					$this->db->query($sql_5);
			$this->db->query($sql_2);
			$sql_3='INSERT INTO im_temp_mid_conv_act(activity_id_fk,dept_id_fk,mid_code,project_id_fk,task_id_fk,sub_id_fk,job_id_fk)	
					SELECT DISTINCT 
					    A.activity_id,'.$d_id.',
						CONCAT_WS("",A.year_nm,B.syll_name,C.grade_name,D.subj_name,E.chapter_nm,F.sub_div_nm,G.div_no,H.sec_div_nm,I.sec_div_no) AS milestone_id,
						A.project_id,
							A.task_id,
							A.sub_id,
							A.job_id
					FROM
					    (
					    SELECT M.activity_id,
					        M.gen_id,
							M.project_id,
							M.task_id,
							M.sub_id,
							M.job_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.year, ",", Num.n),",",-1) AS year_nm
					    FROM
					        numbers Num
					    INNER JOIN contentmids M 
						ON CHAR_LENGTH(M.year) - CHAR_LENGTH(REPLACE(M.year, ",", "")) >= Num.n -1 
						WHERE `M`.`dept_id` = "'.$d_id.'" AND M.activity_id IS NOT NULL 
					AND `M`.`status_nm` = "Active"

					) A
					JOIN(
					    SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.syllabus, ",", Num.n),",",-1) AS syll_name
					    FROM
					        numbers Num
					    INNER JOIN contentmids M 
						ON CHAR_LENGTH(M.syllabus) - CHAR_LENGTH(REPLACE(M.syllabus, ",", "")) >= Num.n -1 
						WHERE `M`.`dept_id` = "'.$d_id.'"
					AND `M`.`status_nm` = "Active"

						) B
					ON B.gen_id = A.gen_id
					LEFT JOIN(
					    SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.grade, ",", Num.n),",",-1) AS grade_name
					    FROM
					        numbers Num
					    INNER JOIN contentmids M 
						ON CHAR_LENGTH(M.grade) - CHAR_LENGTH(REPLACE(M.grade, ",", "")) >= Num.n -1 
						WHERE `M`.`dept_id` = "'.$d_id.'"
					AND `M`.`status_nm` = "Active"

						) C
					ON C.gen_id = A.gen_id
					LEFT JOIN(
					    SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.subject, ",", Num.n),",",-1) AS subj_name
					    FROM
					        numbers Num
					    INNER JOIN contentmids M 
						ON CHAR_LENGTH(M.subject) - CHAR_LENGTH(REPLACE(M.subject, ",", "")) >= Num.n -1 
						WHERE `M`.`dept_id` = "'.$d_id.'"
					AND `M`.`status_nm` = "Active" 
						) D
					ON D.gen_id = A.gen_id
					LEFT JOIN(
					SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.chapter, ",", Num.n),",",-1) AS chapter_nm
					    FROM
					        numbers Num
					    INNER JOIN contentmids M 
						ON CHAR_LENGTH(M.chapter) - CHAR_LENGTH(REPLACE(M.chapter, ",", "")) >= Num.n -1 
						WHERE `M`.`dept_id` = "'.$d_id.'"
					AND `M`.`status_nm` = "Active" 
					) E 
					ON E.gen_id = A.gen_id 
					LEFT JOIN(
					SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.sub_div,  ",", Num.n),",",-1) AS sub_div_nm
					    FROM
					        numbers Num
					    INNER JOIN contentmids M 
						ON CHAR_LENGTH(M.sub_div) - CHAR_LENGTH(REPLACE(M.sub_div, ",", "")) >= Num.n -1 
						WHERE `M`.`dept_id` = "'.$d_id.'"
					AND `M`.`status_nm` = "Active"

					) F
					ON F.gen_id = A.gen_id
					LEFT JOIN(
					    SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.div_num, ",", Num.n),",",-1) AS div_no
					    FROM
					        numbers Num
					    INNER JOIN contentmids M 
						ON CHAR_LENGTH(M.div_num) - CHAR_LENGTH(REPLACE(M.div_num, ",", "")) >= Num.n -1 
						WHERE `M`.`dept_id` = "'.$d_id.'"
					AND `M`.`status_nm` = "Active" 
					) G
					ON G.gen_id = A.gen_id
					LEFT JOIN(
					SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.sec_sub_div,  ",", Num.n),",",-1) AS sec_div_nm
					    FROM
					        numbers Num
					    INNER JOIN contentmids M 
						ON CHAR_LENGTH(M.sec_sub_div) - CHAR_LENGTH(REPLACE(M.sec_sub_div, ",", "")) >= Num.n -1 
						WHERE `M`.`dept_id` = "'.$d_id.'"
					AND `M`.`status_nm` = "Active" 
					) H
					ON H.gen_id = A.gen_id
					LEFT JOIN(
					 SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.sec_div_num, ",", Num.n),",",-1) AS sec_div_no
					    FROM
					        numbers Num
					    INNER JOIN contentmids M 
						ON CHAR_LENGTH(M.sec_div_num) - CHAR_LENGTH(REPLACE(M.sec_div_num, ",", "")) >= Num.n -1 
						WHERE `M`.`dept_id` = "'.$d_id.'"
					AND `M`.`status_nm` = "Active" 
					) I
					ON I.gen_id = A.gen_id ;';
					$this->db->query($sql_3);
					
					$sql_6='INSERT INTO im_temp_mid_conv(dept_id_fk,mid_code,level_id_fk)	
								SELECT DISTINCT '.$d_id.' AS dept_id,									CONCAT_WS("",B.year_nm,A.syll_name,C.grade_name,D.subj_name,E.chapter_nm,F.sub_div_nm,G.div_no,H.sec_div_nm,I.sec_div_no) AS milestone_id,
									COALESCE(H.l_id,F.l_id,E.l_id,D.l_id,C.l_id,B.l_id,A.l_id) AS l_id 
								FROM
									(
									SELECT M.gen_id,M.activity_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.syllabus, ",", Num.n),",",-1) AS syll_name,
										1 AS l_id 
									FROM
										numbers Num
									INNER JOIN contentmids M 
									ON CHAR_LENGTH(M.syllabus) - CHAR_LENGTH(REPLACE(M.syllabus, ",", "")) >= Num.n -1 
									WHERE `M`.`dept_id` = "'.$d_id.'" AND M.activity_id IS NOT NULL 
								AND `M`.`status_nm` = "Active" 	 
								) A
								LEFT JOIN(
								   SELECT M.activity_id,
										M.gen_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.year, ",", Num.n),",",-1) AS year_nm,
										(CASE WHEN Num.n=0 THEN null ELSE 2 END) AS l_id
									FROM
										numbers_0 Num
									INNER JOIN contentmids M 
									ON CHAR_LENGTH(M.year) - CHAR_LENGTH(REPLACE(M.year, ",", "")) >= Num.n -1 
									WHERE `M`.`dept_id` = "'.$d_id.'"
								AND `M`.`status_nm` = "Active" 	
									) B
								ON B.gen_id = A.gen_id
								LEFT JOIN(
									SELECT M.gen_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.grade, ",", Num.n),",",-1) AS grade_name,
										(CASE WHEN Num.n=0 THEN null ELSE 3 END) AS l_id
									FROM
										numbers_0 Num
									INNER JOIN contentmids M 
									ON CHAR_LENGTH(M.grade) - CHAR_LENGTH(REPLACE(M.grade, ",", "")) >= Num.n -1 
									WHERE `M`.`dept_id` = "'.$d_id.'"
								AND `M`.`status_nm` = "Active" 	 
									) C
								ON C.gen_id = A.gen_id AND IFNULL(B.year_nm,"")<>""
								LEFT JOIN(
									SELECT M.gen_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.subject, ",", Num.n),",",-1) AS subj_name,
										(CASE WHEN Num.n=0 THEN null ELSE 4 END) AS l_id
									FROM
										numbers_0 Num
									INNER JOIN contentmids M 
									ON CHAR_LENGTH(M.subject) - CHAR_LENGTH(REPLACE(M.subject, ",", "")) >= Num.n -1 
									WHERE `M`.`dept_id` = "'.$d_id.'"
								AND `M`.`status_nm` = "Active" 	
									) D
								ON D.gen_id = A.gen_id  AND IFNULL(C.grade_name,"")<>""
								LEFT JOIN(
								
									SELECT M.gen_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.chapter, ",", Num.n),",",-1) AS chapter_nm,
										(CASE WHEN Num.n=0 THEN null ELSE 5 END) AS l_id
									FROM
										numbers_0 Num
									INNER JOIN contentmids M 
									ON CHAR_LENGTH(M.chapter) - CHAR_LENGTH(REPLACE(M.chapter, ",", "")) >= Num.n -1 
									WHERE `M`.`dept_id` = "'.$d_id.'"
								AND `M`.`status_nm` = "Active" 
								) E 
								ON E.gen_id = A.gen_id AND IFNULL(D.subj_name,"")<>""
								LEFT JOIN(
								SELECT M.gen_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.sub_div,  ",", Num.n),",",-1) AS sub_div_nm,
										(CASE WHEN Num.n=0 THEN null ELSE 6 END) AS l_id
									FROM
										numbers_0 Num
									INNER JOIN contentmids M 
									ON CHAR_LENGTH(M.sub_div) - CHAR_LENGTH(REPLACE(M.sub_div, ",", "")) >= Num.n -1 
									WHERE `M`.`dept_id` = "'.$d_id.'"
								AND `M`.`status_nm` = "Active" 	
								) F
								ON F.gen_id = A.gen_id AND IFNULL(E.chapter_nm,"")<>""
							LEFT JOIN(
									SELECT M.gen_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.div_num, ",", Num.n),",",-1) AS div_no,
										(CASE WHEN Num.n=0 THEN null ELSE 6 END) AS l_id
									FROM
										numbers_0 Num
									INNER JOIN contentmids M 
									ON CHAR_LENGTH(M.div_num) - CHAR_LENGTH(REPLACE(M.div_num, ",", "")) >= Num.n -1 
									WHERE `M`.`dept_id` = "'.$d_id.'"
								AND `M`.`status_nm` = "Active" 	 
							) G
							ON G.gen_id = A.gen_id AND IFNULL(F.sub_div_nm,"")<>"" AND IFNULL(G.div_no,0)<>0
							LEFT JOIN(
							SELECT M.gen_id,
							        SUBSTRING_INDEX(SUBSTRING_INDEX(M.sec_sub_div,  ",", Num.n),",",-1) AS sec_div_nm,
									(CASE WHEN Num.n=0 THEN null ELSE 7 END) AS l_id
							    FROM
							        numbers_0 Num
							    INNER JOIN contentmids M 
								ON CHAR_LENGTH(M.sec_sub_div) - CHAR_LENGTH(REPLACE(M.sec_sub_div, ",", "")) >= Num.n -1 
								WHERE `M`.`dept_id` = "'.$d_id.'"
							AND `M`.`status_nm` = "Active"  
							) H
							ON H.gen_id = A.gen_id AND IFNULL(F.sub_div_nm,"")<>"" AND IFNULL(G.div_no,0)<>0
							LEFT JOIN(
							SELECT M.gen_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.sec_div_num, ",", Num.n),",",-1) AS sec_div_no,
										(CASE WHEN Num.n=0 THEN null ELSE 7 END) AS l_id
									FROM
										numbers_0 Num
									INNER JOIN contentmids M 
									ON CHAR_LENGTH(M.sec_div_num) - CHAR_LENGTH(REPLACE(M.sec_div_num, ",", "")) >= Num.n -1 
									WHERE `M`.`dept_id` = "'.$d_id.'"
								AND `M`.`status_nm` = "Active" 	 
							) I
							ON I.gen_id = A.gen_id  AND IFNULL(H.sec_div_nm,"")<>"" AND IFNULL(I.sec_div_no,0)<>0
							  ;';
						$this->db->query($sql_6);

						$sql_7='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`status_nm`, `ins_user`, `ins_dt`)
								SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,"Active",'.$user_id.',current_timestamp 
								FROM `im_temp_mid_conv` P 
								LEFT JOIN hierarchy_mst H 
								ON P.`dept_id_fk`=H.dept_id_fk
								AND P.`level_id_fk`=H.level_id_fk
								AND P.mid_code=H.mid_code AND H.status_nm="Active" 
								WHERE H.hierarchy_id IS NULL AND P.level_id_fk=1;';
						$this->db->query($sql_7);

						$sql_8="UPDATE im_temp_mid_conv A INNER JOIN hierarchy_mst P
								ON P.dept_id_fk=A.dept_id_fk 
								AND A.mid_code = P.mid_code 
								AND A.level_id_fk = P.level_id_fk 
								AND P.status_nm='Active' 
								AND A.level_id_fk=1
							SET A.hierarchy_id_fk = P.hierarchy_id;";
						$this->db->query($sql_8);
						$sql_9="UPDATE im_temp_mid_conv A INNER JOIN	im_temp_mid_conv	P 
								ON P.dept_id_fk=A.dept_id_fk  
								AND A.level_id_fk=2 AND A.level_id_fk = (P.level_id_fk+1)
								AND  SUBSTRING(A.mid_code,3,4) = P.mid_code
								SET A.p_hierarchy_id = P.hierarchy_id_fk
								;";
						$this->db->query($sql_9);

						//-- Year
						$sql_10='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
								SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
								FROM `im_temp_mid_conv` P 
								LEFT JOIN hierarchy_mst H 
								ON P.`dept_id_fk`=H.dept_id_fk
								AND P.`level_id_fk`=H.level_id_fk
								AND P.mid_code=H.mid_code 
								AND H.status_nm="Active" 
								AND P.p_hierarchy_id=H.p_hierarchy_id
								WHERE H.hierarchy_id IS NULL AND P.level_id_fk=2 AND P.p_hierarchy_id iS NOT NULL';
						$this->db->query($sql_10);
						$sql_11="UPDATE im_temp_mid_conv A INNER JOIN hierarchy_mst P
							ON P.dept_id_fk=A.dept_id_fk 
							AND A.mid_code = P.mid_code 
							AND P.status_nm='Active' 
							AND  A.level_id_fk = P.level_id_fk AND A.level_id_fk=2
							SET A.hierarchy_id_fk = P.hierarchy_id;";
						$this->db->query($sql_11);
						$sql_12="UPDATE im_temp_mid_conv A INNER JOIN im_temp_mid_conv	P 
								ON P.dept_id_fk=A.dept_id_fk AND A.level_id_fk = (P.level_id_fk+1)
								AND A.level_id_fk=3 
								AND SUBSTRING(A.mid_code,1,6) = P.mid_code
								SET A.p_hierarchy_id = P.hierarchy_id_fk;";
						$this->db->query($sql_12);
						//-- Grade
						$sql_13='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
						SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
						FROM `im_temp_mid_conv` P 
						LEFT JOIN hierarchy_mst H 
						ON P.`dept_id_fk`=H.dept_id_fk
						AND P.`level_id_fk`=H.level_id_fk
						AND P.mid_code=H.mid_code 
						AND H.status_nm="Active" 
						AND P.p_hierarchy_id=H.p_hierarchy_id
						WHERE H.hierarchy_id IS NULL AND P.level_id_fk=3  AND P.p_hierarchy_id iS NOT NULL';
						$this->db->query($sql_13);
						$sql_14="UPDATE im_temp_mid_conv A INNER JOIN hierarchy_mst P
								ON P.dept_id_fk=A.dept_id_fk 
								AND A.mid_code = P.mid_code 
								AND A.level_id_fk = P.level_id_fk 
								AND P.status_nm='Active'
								AND A.level_id_fk=3
								SET A.hierarchy_id_fk = P.hierarchy_id";
						$this->db->query($sql_14);
						$sql_15="UPDATE im_temp_mid_conv A INNER JOIN im_temp_mid_conv	P 
								ON P.dept_id_fk=A.dept_id_fk AND A.level_id_fk = (P.level_id_fk+1)
								AND A.level_id_fk=4 AND SUBSTRING(A.mid_code,1,8) = P.mid_code
								SET A.p_hierarchy_id = P.hierarchy_id_fk;";
						$this->db->query($sql_15);

						// -- Subject
						$sql_16='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
								SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
								FROM `im_temp_mid_conv` P 
								LEFT JOIN hierarchy_mst H 
								ON P.`dept_id_fk`=H.dept_id_fk
								AND P.`level_id_fk`=H.level_id_fk
								AND P.mid_code=H.mid_code 
								AND H.status_nm="Active" 
								AND P.p_hierarchy_id=H.p_hierarchy_id
								WHERE H.hierarchy_id IS NULL AND P.level_id_fk=4  AND P.p_hierarchy_id iS NOT NULL;';
						$this->db->query($sql_16);

						$sql_17="UPDATE im_temp_mid_conv A INNER JOIN hierarchy_mst P 
								ON P.dept_id_fk=A.dept_id_fk 
														AND A.level_id_fk=4 
														AND A.mid_code = P.mid_code 
														AND A.level_id_fk = P.level_id_fk 
														AND P.status_nm='Active' 							
													SET A.hierarchy_id_fk = P.hierarchy_id";
						$this->db->query($sql_17);							
						$sql_18	="UPDATE im_temp_mid_conv A INNER JOIN im_temp_mid_conv	P 
										ON P.dept_id_fk=A.dept_id_fk AND A.level_id_fk=5 
										AND A.level_id_fk = (P.level_id_fk+1) 
										AND SUBSTRING(A.mid_code,1,11) = P.mid_code
										SET A.p_hierarchy_id = P.hierarchy_id_fk;";
						$this->db->query($sql_18);
						//-- Chapter
						$sql_19='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
							SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
							FROM `im_temp_mid_conv` P 
							LEFT JOIN hierarchy_mst H 
							ON P.`dept_id_fk`=H.dept_id_fk
							AND P.`level_id_fk`=H.level_id_fk
							AND P.mid_code=H.mid_code 
							AND H.status_nm="Active" 
							AND P.p_hierarchy_id=H.p_hierarchy_id
							WHERE H.hierarchy_id IS NULL AND P.level_id_fk=5 AND P.p_hierarchy_id iS NOT NULL;';
							$this->db->query($sql_19);

						$sql_20="UPDATE im_temp_mid_conv A INNER JOIN hierarchy_mst P
							ON P.dept_id_fk=A.dept_id_fk 
							AND A.level_id_fk=5
							AND A.mid_code = P.mid_code 
							AND A.level_id_fk = P.level_id_fk 
							AND P.status_nm='Active' 
							SET A.hierarchy_id_fk = P.hierarchy_id;";
						$this->db->query($sql_20);
						$sql_21='UPDATE im_temp_mid_conv A INNER JOIN im_temp_mid_conv	P 
								ON P.dept_id_fk=A.dept_id_fk 
								AND A.level_id_fk=6 AND A.level_id_fk = (P.level_id_fk+1) 
								AND SUBSTRING(A.mid_code,1,13) = P.mid_code
								SET A.p_hierarchy_id = P.hierarchy_id_fk;';
						$this->db->query($sql_21);
						//-- Sub Topic
					$sql_22='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
							SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
							FROM `im_temp_mid_conv` P 
							LEFT JOIN hierarchy_mst H 
							ON P.`dept_id_fk`=H.dept_id_fk
							AND P.`level_id_fk`=H.level_id_fk
							AND P.mid_code=H.mid_code 
							AND H.status_nm="Active" 
							AND P.p_hierarchy_id=H.p_hierarchy_id
							WHERE H.hierarchy_id IS NULL AND P.level_id_fk=6 AND P.p_hierarchy_id iS NOT NULL;';
					$this->db->query($sql_22);
					$sql_23="UPDATE im_temp_mid_conv A INNER JOIN hierarchy_mst P
								ON P.dept_id_fk=A.dept_id_fk 
								AND A.mid_code = P.mid_code 
								AND A.level_id_fk = P.level_id_fk 
								AND P.status_nm='Active'
								AND A.level_id_fk=6
							SET A.hierarchy_id_fk = P.hierarchy_id;";
					$this->db->query($sql_23);
					$sql_24='UPDATE im_temp_mid_conv A INNER JOIN im_temp_mid_conv	P 
							ON P.dept_id_fk=A.dept_id_fk AND A.level_id_fk = (P.level_id_fk+1)
							AND A.level_id_fk=7 AND SUBSTRING(A.mid_code,1,18) = P.mid_code
							SET A.p_hierarchy_id = P.hierarchy_id_fk;';
					$this->db->query($sql_24);
					$sql_25='UPDATE im_temp_mid_conv A INNER JOIN hierarchy_mst	P 
						ON P.dept_id_fk=A.dept_id_fk AND A.level_id_fk=7 
						AND A.level_id_fk = (P.level_id_fk+1)
						AND  SUBSTRING(A.mid_code,1,18) = P.mid_code 
						SET A.p_hierarchy_id = P.hierarchy_id;';
					$this->db->query($sql_25);
					//-- Video Clip
					$sql_26='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
							SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
							FROM `im_temp_mid_conv` P 
							LEFT JOIN hierarchy_mst H 
							ON P.`dept_id_fk`=H.dept_id_fk
							AND P.`level_id_fk`=H.level_id_fk
							AND P.mid_code=H.mid_code 
							AND H.status_nm="Active" 
							AND P.p_hierarchy_id=H.p_hierarchy_id
							WHERE H.hierarchy_id IS NULL AND P.level_id_fk=7 AND P.p_hierarchy_id iS NOT NULL';
					$this->db->query($sql_26);
					$sql_27="UPDATE im_temp_mid_conv A INNER JOIN hierarchy_mst P
							ON P.dept_id_fk=A.dept_id_fk 
							AND A.mid_code = P.mid_code 
							AND A.level_id_fk = P.level_id_fk 
							AND P.status_nm='Active'
							AND A.level_id_fk=7
						SET A.hierarchy_id_fk = P.hierarchy_id;";
					$this->db->query($sql_27);
					$sql_28='UPDATE im_temp_mid_conv A INNER JOIN im_temp_mid_conv	P 
							ON P.dept_id_fk=A.dept_id_fk AND A.level_id_fk = (P.level_id_fk+1)
							AND A.level_id_fk=8 AND SUBSTRING(A.mid_code,1,23) = P.mid_code
							SET A.p_hierarchy_id = P.hierarchy_id_fk;';
					$this->db->query($sql_28);
					//-- Third Sub
					$sql_29='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
							SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
							FROM `im_temp_mid_conv` P 
							LEFT JOIN hierarchy_mst H 
							ON P.`dept_id_fk`=H.dept_id_fk
							AND P.`level_id_fk`=H.level_id_fk
							AND P.mid_code=H.mid_code 
							AND H.status_nm="Active" 
							AND P.p_hierarchy_id=H.p_hierarchy_id
							WHERE H.hierarchy_id IS NULL AND P.level_id_fk=8 AND P.p_hierarchy_id IS NOT NULL;';
					$this->db->query($sql_29);
					$sql_30="UPDATE im_temp_mid_conv A INNER JOIN hierarchy_mst P
							ON P.dept_id_fk=A.dept_id_fk 
							AND A.mid_code = P.mid_code 
							AND A.level_id_fk = P.level_id_fk 
							AND P.status_nm='Active'
							AND A.level_id_fk=8
						SET A.hierarchy_id_fk = P.hierarchy_id";
					$this->db->query($sql_30);
					//-- ACtivity						
					$sql_31='UPDATE im_temp_mid_conv_act P JOIN activity_mst A 
							ON A.activity_id=P.activity_id_fk 
							AND A.status_nm="Active" 
							SET P.dept_id_fk=A.dept_id_fk 
							AND P.project_id_fk=A.project_id_fk,
							P.task_id_fk=A.task_id_fk,
							P.sub_id_fk=A.sub_id_fk,
							P.job_id_fk=A.job_id_fk;';
						$this->db->query($sql_31);

					$sql_32='UPDATE im_temp_mid_conv_act P JOIN hierarchy_mst A 	
							ON A.mid_code=P.mid_code AND P.hierarchy_id_fk IS NULL 
							AND A.status_nm="Active" 
							SET P.hierarchy_id_fk=A.hierarchy_id;'; 
					$this->db->query($sql_32);

					$sql_33='UPDATE im_temp_mid_conv_act P JOIN hier_act_log A 
							ON P.hierarchy_id_fk=A.p_hierarchy_id 
							AND P.dept_id_fk=A.dept_id_fk 
							AND P.project_id_fk=A.project_id_fk AND
							P.task_id_fk=A.task_id_fk AND 
							P.sub_id_fk=A.sub_task_id_fk AND 	
							P.job_id_fk=A.job_id_fk 
							SET  P.hier_act_log=A.hier_act_log_id';
					$this->db->query($sql_33);

					$sql_335='UPDATE hier_act_log A  join im_temp_mid_conv_act P  
							ON P.hier_act_log=A.hier_act_log_id AND P.hier_act_log IS NOT NULL 
							SET  A.status_nm="Active",A.upd_dt=current_timestamp,A.upd_user='.$user_id.';';
					$this->db->query($sql_335);
					
					$sql_34="INSERT INTO `hier_act_log`(`p_hierarchy_id`, `dept_id_fk`, `project_id_fk`, `task_id_fk`, `sub_task_id_fk`, `job_id_fk`, `status_nm`, `ins_user`, `ins_dt`)
							SELECT hierarchy_id_fk,dept_id_fk,project_id_fk,task_id_fk,
							sub_id_fk,	job_id_fk,'Active' ,".$user_id.", current_timestamp 
							FROM im_temp_mid_conv_act 
							WHERE hierarchy_id_fk IS NOT NULL AND hier_act_log IS NULL;";
					$this->db->query($sql_34);
					
			 $sql35='DROP TABLE  IF EXISTS contentmids;';
			$this->db->query($sql35);
			$sql_1="DROP TABLE IF EXISTS im_temp_mid_conv_act";
				$this->db->query($sql_1);	
				$sql_4='DROP TABLE IF EXISTS im_temp_mid_conv';
					$this->db->query($sql_4);
    		$this->db->trans_complete();
			if($this->db->trans_status() === FALSE)
			  {				 
				 
				 return false;
			  }
			  else{
				 return "success";
			  }
			 }
			// else{
				// return "Upload already in use. Please try after some time.";
			// }
    }

    //import media csv
    public function ImportMediaMidCsv($ary){
    	//print_r($ary);die;
    	if($this->db->table_exists('Mediamids')==False){
    			$this->db->trans_start();
    			$sql='CREATE TABLE Mediamids
    				(
    					`project_name`varchar(50)DEFAULT NULL,
						 `project_id` INT(11) DEFAULT NULL,
    					`status_nm` varchar(50)DEFAULT NULL,
    					`mids`varchar(50)DEFAULT NULL,
    					`dept_id`int(11),
    					`ins_user` int(11)
    				)ENGINE=InnoDB;';
			$this->db->query($sql);
    		$this->db->insert_batch('Mediamids',$ary);
    		$sql1="UPDATE Mediamids A INNER JOIN project_mst P
			ON P.dept_id_fk=A.dept_id AND A.project_name = P.project_name
			AND P.status_nm='Active'
			SET A.project_id = P.project_id;";
			$this->db->query($sql1);
			$sql2="INSERT INTO mile_act_mst(milestone_id,dept_id_fk,status_nm,ins_user,ins_dt ,project_id_fk)
			SELECT DISTINCT A.mids,A.dept_id,A.status_nm,A.ins_user,current_timestamp,A.project_id
			 FROM Mediamids A  WHERE A.project_id IS NOT NULL;";
			 $this->db->query($sql2);
			 $sql3='DROP TABLE Mediamids';
			 $this->db->query($sql3);
			$this->db->trans_complete();
			if($this->db->trans_status() === FALSE)
			  {
				 return false;
			  }
			  else{
				 return "success";
			  }
		}
			else{
				return "Upload already in use. Please try after some time.";
			}

	}
}
?>