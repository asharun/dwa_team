<?php
class Feedback_model extends CI_model{

    public function updateWeeklyFeedback($feed)
    {
		$feedbackGivenDate = strtotime($feed['fb_given_date']);
        $feed['upd_user'] = $this->session->userdata('user_id');
        $feed['upd_dt'] = date("Y-m-d H:i:s");
		 $feed['fb_given_date'] = date("Y-m-d H:i:s", $feedbackGivenDate);
        $this->db->where('user_id_fk', $feed['user_id_fk']);
        $this->db->where('project_id_fk', $feed['project_id_fk']);
        $this->db->update('user_feedbacks', $feed);   
    }

    public function insertWeeklyFeedback($feed)
    {
		 $feedbackGivenDate = strtotime($feed['fb_given_date']);
        $feed['ins_user'] = $this->session->userdata('user_id');
        $feed['ins_dt'] = date("Y-m-d H:i:s");
		$feed['status_nm'] = "Active";
		$feed['fb_given_date'] = date("Y-m-d H:i:s", $feedbackGivenDate);
        $this->db->insert('user_feedbacks', $feed);
    }

    public function getFeedbackGradesData()
    {
		$this->db->where('status_nm', "Active");
        return $this->db->get('feedback_grades');
    }

    public function getActiveFeedbackGradesData()
    {
        $this->db->where('status_nm', "Active");
        return $this->db->get('feedback_grades');
    }

    public function getUsersWeeklyProjectFeedback($input)
    {
		//$date = new DateTime();
		$date = new DateTime($input['dateView']);
        $weekStart = $date->modify('this week')->format('Y-m-d');
        $weekEnd = $date->modify('this week +6 days')->format('Y-m-d');
        
        $this->db->where('project_id_fk', $input['projectId']);
        $this->db->where_in('user_id_fk', $input['users']);
        $this->db->where('fb_given_date >=', $weekStart);
        $this->db->where('fb_given_date <=', $weekEnd);
        return $this->db->get('user_feedbacks');

    }

    public function storeGrade($gradeData)
    {
        $data['grade'] = $gradeData['grade'];
        $data['value'] = $gradeData['value'];
        $data['status_nm'] = "Active";
        $data['ins_user'] = $gradeData['user'];
        $data['ins_dt'] = date("Y-m-d H:i:s");
        $this->db->insert('feedback_grades', $data);
    }

    public function updateGrade($gradeData)
    {
        $data['grade'] = $gradeData['grade'];
        $data['value'] = $gradeData['value'];
        $data['status_nm'] ='Active';
        $data['upd_user'] = $gradeData['user'];
        $data['upd_dt'] = date("Y-m-d H:i:s");
        $this->db->where('id', $gradeData['update_id']);
        $this->db->update('feedback_grades', $data);
    }

    public function deleteGrade($id)
	{
		$this->db->where('id', $id);
		$data['upd_dt'] = date("Y-m-d H:i:s");
		$data['status_nm'] ='InActive';
		  $data['upd_user'] = $this->session->userdata('user_id');
		 $this->db->update('feedback_grades', $data);
      //  $this->db->delete('feedback_grades');
	}
}