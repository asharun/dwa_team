<?php
class Allocation_model extends CI_model{

	public function  GetSideLevel($levelid){
		$this->db->select('*');
		$this->db->from('level_mst');
		if($levelid)
		{
		$this->db->where('level_id <=',$levelid);
		}
		$sqlQuery = $this->db->get();
		$resultSet = $sqlQuery->result_array();
		if($resultSet)
		{
			return $resultSet;
		}
		else{
			return false;
		}
	 }

	  public function getActivity($hierarchyId)
	 {
	     $this->db->select('CONCAT_WS("_",P.project_name, T.task_name, S.sub_name, J.job_name) AS mid_code, ha.*');
	     $this->db->from('hier_act_log as ha');
	     $this->db->join('project_mst as P', 'ha.project_id_fk = P.project_id', 'left');
	     $this->db->join('task_mst as T', 'ha.task_id_fk = T.task_id', 'left');
	     $this->db->join('sub_mst as S', 'ha.sub_task_id_fk = S.sub_id', 'left');
	     $this->db->join('job_mst as J', 'ha.job_id_fk = J.job_id', 'left');
	     $this->db->where('ha.hier_act_log_id', $hierarchyId);
	     $sqlQuery = $this->db->get();
	     return $sqlQuery->result_array();
	 }
	 public function getLevel($hierarchyId)
	 {
		 $this->db->select('*, (SELECT mid_code from hierarchy_mst where hierarchy_id =  HM.p_hierarchy_id) as parent_mid');
		 $this->db->from('hierarchy_mst HM');
		 $this->db->join('level_mst', 'level_id = HM.level_id_fk');
		 $this->db->where('HM.hierarchy_id', $hierarchyId);
		 $sqlQuery = $this->db->get();
		 return $sqlQuery->result_array();
	 }

	//Check mid exists or not in allocation level
	public function checkLevelExists($mid, $deptId,$hierId = NULL){
	 		$this->db->select('mid_code');
	 		$this->db->from('hierarchy_mst HM');
	 		$this->db->where(['mid_code' => $mid,'dept_id_fk' => $deptId]);
	 		if($hierId){
				$this->db->where("hierarchy_id !=",$hierId,FALSE);
	 		}
	 		$level = $this->db->get()->row();
		return $level ? true : false;
	}

	public function checkActivityExists($formData,$hierActId = NULL){
			$checkData = $formData;
			$where = [
				'p_hierarchy_id' => $checkData['p_hierarchy_id'],
				'dept_id_fk'	 => $checkData['dept_id_fk'],
				'project_id_fk' => $checkData['project_id_fk'],
				'task_id_fk' => $checkData['task_id_fk'],
				'sub_task_id_fk' => $checkData['sub_task_id_fk'],
				'job_id_fk' => $checkData['job_id_fk']
			];

 			if($hierActId){
 				$where['hier_act_log_id !='] = $hierActId;
 			}
	 		$this->db->select('hier_act_log_id');
	 		$this->db->from('hier_act_log');
	 		$this->db->where($where);
	 		$level = $this->db->get()->row();
		return $level ? $level : false;
	}

	 public function updateLevelDeepChildMids($oldMidCode, $newMidCode, $deptId)
	 {
		$this->db->trans_start();
			$sql2="UPDATE hierarchy_mst SET mid_code=REPLACE(mid_code,'$oldMidCode','$newMidCode')
			WHERE mid_code like '%".$oldMidCode."%' AND status_nm='Active' AND dept_id_fk='".$deptId."'";
			$sqlQuery=$this->db->query($sql2);

			$this->updateInHierarchyPermission(
				$oldMidCode,
				$newMidCode,
				$deptId
			);
		$this->db->trans_complete();
	 }


	public function getdetailsbox($levelid,$p_herachy_id=NULL,$dept_id=NULL,$u_id){
		$p_herachy_id=!empty($p_herachy_id) ? "and H.p_hierarchy_id=".$p_herachy_id."" : '';
		// $dept_id=!empty($dept_id) ? "and
		// 	H.dept_id_fk=".$dept_id."" : '';
	 $qry="SELECT  MAX(  CASE WHEN P.`level_id_fk` <= H.level_id_fk THEN 1 ELSE 0 END 
	) AS show_p,H.* , (select level_name from level_mst where level_id='".($levelid+1)."' AND status_nm='Active') AS level_name,
	(select count(1) from hierarchy_mst where p_hierarchy_id=H.hierarchy_id AND level_id_fk='".($levelid+1)."' AND status_nm='Active') as child_count
				FROM `hierarchy_mst` H 				
				INNER JOIN(
					SELECT HP.level_id_fk,
							HP.mid_code,
							(CASE WHEN HP.level_id_fk=1 THEN 3 ELSE 1 END) AS l_start,
        					(CASE WHEN HP.level_id_fk=1 THEN 4 ELSE L.length_limit END) AS length_limit,
							L.level_code
					FROM
							user_permissions HP
					LEFT JOIN level_mst L ON
							L.level_id = HP.level_id_fk
					WHERE
							HP.user_id_fk = ".$u_id."  AND IFNULL(HP.is_alloc,0)=1 AND HP.`status_nm` = 'Active' AND HP.dept_id_fk = ".$dept_id."
			) P
			ON
					(
						(P.level_id_fk = -1)
						OR(IFNULL(P.mid_code, '') = '' AND P.level_id_fk <= H.level_id_fk)
						OR(H.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0)
						OR(H.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(SUBSTRING(H.mid_code, P.l_start, P.length_limit),P.mid_code)>0)
					)				
				WHERE H.`level_id_fk` ='".$levelid."'  AND H.`status_nm` = 'Active' ".$p_herachy_id." AND H.dept_id_fk=".$dept_id." GROUP BY H.hierarchy_id";

			$sqlQuery = $this->db->query($qry);
            $resultSet = $sqlQuery->result_array();
              if($resultSet)
			{
				return $resultSet;
			}
			else{
				return false;
			}

	}
	public function  DetailsBoxByNavigation($levelid,$p_herachy_id,$dept_id){
		$userId = $this->session->userdata('user_id');
		$p_herachy_id=!empty($p_herachy_id) ? "and H.hierarchy_id=".$p_herachy_id."" : '';
		$dept_id=!empty($dept_id) ? "and H.dept_id_fk=".$dept_id."" : '';

		 $qry="SELECT DISTINCT (CASE WHEN P.`level_id_fk`<=H.level_id_fk THEN 1 ELSE 0 END) AS show_p,
				H.* ,
				(select level_name from level_mst where level_id='".$levelid."' AND status_nm='Active') as level_name
				FROM `hierarchy_mst` H
				INNER JOIN ( SELECT HP.level_id_fk,HP.mid_code,L.length_limit FROM user_permissions HP
								LEFT JOIN level_mst L ON L.level_id=HP.level_id_fk
								WHERE HP.user_id_fk = $userId AND HP.`status_nm` = 'Active'  AND IFNULL(HP.is_alloc,0)=1 
					)P
			ON (IFNULL(P.mid_code,0)=0 OR ( H.level_id_fk<=P.`level_id_fk` AND INSTR(P.mid_code,H.mid_code)>0) OR ( H.level_id_fk>P.`level_id_fk` AND FIND_IN_SET(SUBSTRING(H.mid_code,1,P.length_limit),P.mid_code)>0))
			WHERE H.`level_id_fk` ='".$levelid."'  AND H.`status_nm` = 'Active' ".$p_herachy_id." ".$dept_id."";
			$sqlQuery = $this->db->query($qry);
            $resultSet = $sqlQuery->result_array();
              if($resultSet)
			{
				return $resultSet;
			}
			else{
				return false;
			}

	}
	public function getparentid($dept_id,$levelid,$parent_id){
	     $sql = "SELECT H.p_hierarchy_id ,H.hierarchy_id,H.level_id_fk
				 FROM
				   (SELECT hierarchy_id,mid_code,p_hierarchy_id,level_id_fk,
				      CASE WHEN hierarchy_id =".$parent_id." THEN @id := IFNULL(p_hierarchy_id,-1)
				           WHEN hierarchy_id = @id THEN @id := IFNULL(p_hierarchy_id,-1)
				           END as checkId

				    FROM hierarchy_mst
					 INNER JOIN (SELECT @id:='')P
					WHERE dept_id_fk=".$dept_id."
				    ORDER BY p_hierarchy_id DESC,hierarchy_id DESC) as H
				WHERE checkId IS NOT NULL AND H.level_id_fk=".$levelid." ;";

	    $query = $this->db->query($sql);
	    $row = $query->result_array();
	    return  $row;
	}

	public function GetDeptUsers($deptid){

			$qry="SELECT user_id,full_name FROM `user_mst` WHERE
			(IFNULL(dept_id_fk,0)=0 OR  FIND_IN_SET(".$deptid.",`dept_id_fk`))
			AND `status_nm`='Active'
			ORDER BY `user_mst`.`user_id` ASC";
			$sqlQuery = $this->db->query($qry);
            $resultSet = $sqlQuery->result_array();
            if($resultSet)
              {
                return $resultSet;
              }
              else{
              	return false;
              }
	}

	public function GetCode($p_hierarchy_id){
		//$this->db->select('mid_code');
		$this->db->select('mid_code,p_hierarchy_id');
		$this->db->from('hierarchy_mst');
		$this->db->where('hierarchy_id',$p_hierarchy_id);
		$sqlQuery = $this->db->get();
		$resultSet = $sqlQuery->result_array();
		if($resultSet)
		{
			return $resultSet[0];
		}
		else{
			return false;
		}
	}

	public function Getparentcode($id){
				$this->db->select('mid_code');
				$this->db->from('hierarchy_mst');
				$this->db->where('hierarchy_id',$id);
				$sqlQuery = $this->db->get();
				// $this->db->last_query();
				$resultSet = $sqlQuery->result_array();
				if($resultSet)
				{
					return $resultSet[0]['mid_code'];
				}
				else{
					return false;
				}
	}

	public function getUserDept($u_id){
		$sql = "SELECT dept_id_fk
				   FROM `user_permissions` WHERE `status_nm` = 'Active'  AND IFNULL(is_alloc,0)=1  AND `user_id_fk` = ".$u_id."";
	    $query = $this->db->query($sql);
	    $row = $query->result_array();
	    return  $row;
	}



	public function insert($tablename,$data){
		$this->db->insert($tablename,$data);
   		 return true;
	}
	public function edit($hierarchy_id){
		$this->db->select('*');
	    $this->db->from('hierarchy_mst');
	    $this->db->where('hierarchy_id',$hierarchy_id);
	    $query = $this->db->get();
	    if($query->num_rows() > 0)
	        return $query->result_array();
	}

	public function update($tableName, $data, $hierarchyId){
			$this->db->where('hierarchy_id',$hierarchyId);
			$this->db->update($tableName, $data);
			return true;
	}

	public function updateInHierarchyPermission($existing_code, $Entered_code, $dept_id)
	{
		    $sql="UPDATE user_permissions SET mid_code=REPLACE(mid_code,'$existing_code','$Entered_code')
			WHERE mid_code like '%".$existing_code."%' AND status_nm='Active' AND dept_id_fk='".$dept_id."'";
			$sqlQuery=$this->db->query($sql);
	}


	public function OverViewBoxUpdate($data, $hierarchyId){

		$this->db->trans_start();
			$sql1="Select mid_code from hierarchy_mst where hierarchy_id=$hierarchyId";
			$sqlQuery=$this->db->query($sql1);
			$resultset=$sqlQuery->result_array();
			$existing_code=$resultset[0]['mid_code'];
			$Entered_code=$data['mid_code'];
			//$this->db->update($tableName, $data);
			if($existing_code!=$Entered_code){
				$this->updateLevelDeepChildMids(
					$existing_code,
					$Entered_code,
					$data['dept_id_fk']
				);
				$this->updateInHierarchyPermission($existing_code, $Entered_code, $data['dept_id_fk']);
			}

			$this->db->where(['hierarchy_id' => $hierarchyId,'status_nm' => 'Active']);
			$this->db->update('hierarchy_mst', $data);

			$this->db->trans_complete();
			if($this->db->trans_status() === FALSE)
			  {
				 return false;
			  }
			  else{
				 return true;
			  }
	}

	public function storeActivity($data){
		$this->db->trans_start();
		$this->db->set('ins_dt', 'current_timestamp', FALSE);
		$this->db->insert('hier_act_log', $data);
		
		$lids = $this->db->insert_id();
				
				$sql17r='INSERT INTO hier_user_alloc(`hier_act_log_id_fk`, `alloc_user`,`status_nm`, `ins_user`, `ins_dt`)
						SELECT  A.hier_act_log_id ,U.user_id,A.status_nm,A.ins_user,A.ins_dt
							from hier_act_log A 
							LEFT JOIN user_mst U 
							ON FIND_IN_SET(U.user_id,A.alloc_to)>0 
							where A.alloc_to is not null AND A.hier_act_log_id='.$lids.';';
				$this->db->query($sql17r);
				
   		 $this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return true;
		  }
	}

	public function updateActivity($actHierarchyId, $data){
		$this->db->trans_start();
		$userId = $this->session->userdata('user_id');
			$this->db->where('hier_act_log_id',$actHierarchyId);
			$this->db->set('upd_dt', 'current_timestamp', FALSE);
			$this->db->update('hier_act_log', $data);
			
			$sql16r='UPDATE hier_user_alloc HA SET HA.status_nm="InActive",HA.upd_user='.$userId.',HA.upd_dt=current_timestamp 
							WHERE HA.hier_act_log_id_fk IN ('.$actHierarchyId.') 
							AND HA.status_nm<>"InActive" ;';
				$this->db->query($sql16r);
				
				$sql16r='UPDATE hier_user_alloc HU JOIN (
									SELECT A.hier_act_log_id ,U.user_id,A.status_nm 
									from hier_act_log A 
									LEFT JOIN user_mst U 
									ON FIND_IN_SET(U.user_id,A.alloc_to)>0 
									where A.alloc_to is not null AND A.hier_act_log_id IN ('.$actHierarchyId.') )HA  
									ON HU.hier_act_log_id_fk=HA.hier_act_log_id AND 
									HU.alloc_user=HA.user_id 
							SET HU.status_nm="Active"  ';
				$this->db->query($sql16r);
				
				$sql17r='INSERT INTO hier_user_alloc(`hier_act_log_id_fk`, `alloc_user`,`status_nm`, `ins_user`, `ins_dt`)
							SELECT A.hier_act_log_id ,U.user_id,"Active",'.$userId.',current_timestamp 
							from hier_act_log A 
							LEFT JOIN user_mst U 
							ON FIND_IN_SET(U.user_id,A.alloc_to)>0 
							LEFT JOIN hier_user_alloc HU 
							ON HU.hier_act_log_id_fk=A.hier_act_log_id AND 
							HU.alloc_user=U.user_id 
							where A.alloc_to is not null AND A.hier_act_log_id IN ('.$actHierarchyId.') 
							AND HU.hier_alloc_id IS NULL';
				$this->db->query($sql17r);
				
			 $this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return true;
		  }
	}

	public function updateLevel($hierarchyId, $data){
		$this->db->trans_start();
			$this->db->where('hierarchy_id',$hierarchyId);
			$this->db->update('hierarchy_mst', $data);
			 $this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return true;
		  }
	}

	public function getAllHierarchyData()
	{
			$this->db->select('HMST.*, LM.level_code, LM.level_id');
			$this->db->from('hierarchy_mst HMST');
			$this->db->join('level_mst LM','HMST.level_id_fk=LM.level_id','inner');
			// $this->db->where('HMST.status_nm','Active');
			$this->db->order_by('HMST.level_id_fk', 'asc');
			$sqlQuery = $this->db->get();
			return $sqlQuery->result_array();
	}

	public function getLevelsData($deptId = 0, $levelId = 0, $hierarchyId = 0, $isAjax)
	{
			// if ($levelId != 0) {
			// 		$hierarchyIdQuery = "p_hierarchy_id";
			// }
			// if ($levelId != 0 && $hierarchyId != 0) {
			// 	$hierarchyIdQuery = "NULL AS p_hierarchy_id";
			// }
			$userId = $this->session->userdata('user_id');
			// var_dump($userId, $deptId, $levelId, $hierarchyId); exit;
			// if ($hierarchyId == 0) {
					$subQuery = "1 = 1";
			// } else {
			// 		$subQuery = "hierarchy_id = $hierarchyId";
			// }
			// $parentIdsQuery = '';
			// $levelIdQuery = '';
			// $hierarchyIdQuery = '';
			//
			// if ($parentIds != "0") {
			// 	$subQuery = "p_hierarchy_id IN ($parentIds)";
			// 	$parentIdsQuery = " AND H.p_hierarchy_id IN ($parentIds)";
			// 	$levelIdQuery = '';
			// } else {
			// 	$levelIdQuery = "AND H.level_id_fk IN ($levelId)";
			// }
			//
			// // $levelIdQuery = "AND H.level_id_fk >= $levelId";
			// if ($hierarchyId != "0" && $parentIds == "0") {
			// 	$hierarchyIdQuery = "AND H.p_hierarchy_id IN ($hierarchyId)";
			// }
			$levelIdOnDemand = $levelId;
			//$searchValue = get_cookie('details_search_value');

			if ($hierarchyId == "0") {
				$subQuery = "";
				$pMid = "NULL as parent_mid";
			} else {
				$subQuery = "AND p_hierarchy_id = $hierarchyId";
				$pMid = "(SELECT mid_code from hierarchy_mst where hierarchy_id = $hierarchyId) as parent_mid";
			}
			if ($isAjax) {
				$levelIdOnDemand = $levelId - 1;
			}

			$query = "SELECT DISTINCT
					H.hierarchy_id,
					H.mid_code,
					$pMid,
					H.mid_name,
					H.owner_mid,
					H.presenter,
					H.storyboard_incharge,
					H.sign_off,
					H.est_end_dt,
					H.mandays,
					H.video_clip_type,
					H.storyboarded_by,
					H.duration,
					H.status_nm,
					H.progress,
					H.start_dt,
					H.end_dt,
					H.hier_desc,
					(CASE WHEN H.level_id_fk=$levelIdOnDemand THEN NULL ELSE H.p_hierarchy_id END) AS p_hierarchy_id,
					H.level_id_fk,
					LMS.level_code,
					LMS.level_name,
					MAX(
					       CASE WHEN P.`level_id_fk` <= H.level_id_fk THEN 1 ELSE 0
					   END
					) AS show_p
			FROM
					(
					SELECT
							hierarchy_id,
							mid_code,
							mid_name,
					        owner_mid,
					        status_nm,
							progress,
					        start_dt,
					        end_dt,
							p_hierarchy_id,
							level_id_fk,
							presenter,
							storyboard_incharge,
							sign_off,
							est_end_dt,
							mandays,
							video_clip_type,
							storyboarded_by,
							duration,
							hier_desc
			FROM
					hierarchy_mst
					where dept_id_fk = ".$deptId." AND level_id_fk <= (".$levelId.") $subQuery
			) AS H
			LEFT JOIN level_mst LMS on LMS.level_id = H.level_id_fk
			INNER JOIN(
					SELECT HP.level_id_fk,
							HP.mid_code,
							(CASE WHEN HP.level_id_fk=1 THEN 3 ELSE 1 END) AS l_start,
        					(CASE WHEN HP.level_id_fk=1 THEN 4 ELSE L.length_limit END) AS length_limit,
							L.level_code
					FROM
							user_permissions HP
					LEFT JOIN level_mst L ON
							L.level_id = HP.level_id_fk
					WHERE
							HP.user_id_fk = ".$userId."   AND IFNULL(HP.is_alloc,0)=1  AND HP.`status_nm` = 'Active' AND HP.dept_id_fk = ".$deptId."
			) P
			ON
					(
						(P.level_id_fk = -1)
						OR(IFNULL(P.mid_code, '') = '' AND P.level_id_fk <= H.level_id_fk)
						OR(H.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0)
						OR(H.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(SUBSTRING(H.mid_code, P.l_start, P.length_limit),P.mid_code)>0)
					) WHERE H.status_nm = 'Active'
			GROUP BY H.hierarchy_id";

			$sqlQuery = $this->db->query($query);
			return $sqlQuery->result_array();

	}


	public function getSummaryLevelsData($levelCode = 0, $levelId = 0, $hierarchyId = 0, $isParent)
	{
		$userId = $this->session->userdata('user_id');
		$parentMidCode = "(SELECT mid_code from hierarchy_mst where hierarchy_id = $hierarchyId) as p_mid_code";

		$levelIdOnDemand = $levelId;
		if ($hierarchyId == "0") {
			$subQuery = "";
		} else {
			$subQuery = "AND HR.p_hierarchy_id = '$hierarchyId'";
			$levelIdOnDemand = $levelId - 1;
			// $subQuery = "AND HR.p_hierarchy_id = '$hierarchyId'";
		}
		// $parentMid = "'$hierarchyId' AS parent";

		if ($isParent) {
			$parent = 'NULL as parent';
			$parentMid = "NULL AS parent";

		} else {
			$parent = "(CASE WHEN H.level_id_fk=$levelIdOnDemand THEN NULL ELSE H.p_hierarchy_id END) AS parent";
			$parentMid = "'$hierarchyId' AS parent";
		}
		$query = "SELECT TE.id, TE.parent, TE.text, TE.owner_mid, TE.deadline, TE.p_mid_code, TE.level_id_fk, DATE_FORMAT(MIN(SS_HM.start_dt), '%d-%m-%Y') AS start_date,
		DATE_FORMAT(MAX(SS_HM.end_dt), '%d-%m-%Y') AS end_date,
		FORMAT(IFNULL(DATEDIFF(NOW(), DATE_FORMAT(MIN(SS_HM.start_dt), '%Y-%m-%d')),0)/ IFNULL(DATEDIFF(DATE_FORMAT(MAX(SS_HM.end_dt), '%Y-%m-%d'), DATE_FORMAT(MIN(SS_HM.start_dt), '%Y-%m-%d')),1), 1) as progress,
		IF(TE.progress = 'Not Started', 'rgba(128, 139, 150)', IF(TE.progress = 'In Progress', 'rgba(52, 152, 219)', IF(TE.progress = 'On Hold', 'rgba(217,83,79)', IF(TE.progress = 'Completed', 'rgba(212, 172, 13)', '')))) as progressColor,
		IF(TE.progress = 'Not Started', 'rgba(128, 139, 150, 0.8)', IF(TE.progress = 'In Progress', 'rgba(52, 152, 219, 0.8)', IF(TE.progress = 'On Hold', 'rgba(217,83,79, 0.8)', IF(TE.progress = 'Completed', 'rgba(212, 172, 13, 0.8)', '')))) as color,
		TE.actual_start_date,
		TE.actual_end_date
		from (SELECT DISTINCT
			H.hierarchy_id AS id,
			$parent,
		H.mid_code AS text,
		H.owner_mid,
		H.progress,
		H.start_dt,
		H.end_dt,
		DATE_FORMAT(H.est_end_dt, '%Y-%m-%d') AS deadline,
		H.level_id_fk,
		H.mid_code AS p_mid_code,
		null as actual_start_date,
		null as actual_end_date
		FROM
			(
			SELECT
				HR.hierarchy_id,
				HR.mid_code,
				HR.mid_name,
				HR.owner_mid,
				HR.status_nm,
				HR.progress,
				HR.start_dt,
				HR.end_dt,
				HR.est_end_dt,
				HR.p_hierarchy_id,
				PR.mid_code AS p_mid_code,
				HR.level_id_fk
			FROM
				hierarchy_mst HR
			LEFT JOIN hierarchy_mst PR ON
				HR.p_hierarchy_id = PR.hierarchy_id
			WHERE
				HR.level_id_fk =($levelId) $subQuery
		) AS H
		LEFT JOIN level_mst LMS ON
			LMS.level_id = H.level_id_fk
		INNER JOIN(
			SELECT
				HP.level_id_fk,
				HP.mid_code,
				(
					CASE WHEN HP.level_id_fk = 1 THEN 3 ELSE 1
				END
		) AS l_start,
		(
			CASE WHEN HP.level_id_fk = 1 THEN 4 ELSE L.length_limit
		END
		) AS length_limit,
		L.level_code
		FROM
			user_permissions HP
		LEFT JOIN level_mst L ON
			L.level_id = HP.level_id_fk
		WHERE
			HP.user_id_fk = $userId AND HP.status_nm = 'Active'  AND IFNULL(HP.is_alloc,0)=1
		) P
		ON
			(
				(P.level_id_fk = -1) OR(
					IFNULL(P.mid_code, '') = '' AND P.level_id_fk <= H.level_id_fk
				) OR(
					H.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0
				) OR(
					H.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(
						SUBSTRING(
							H.mid_code,
							P.l_start,
							P.length_limit
						),
						P.mid_code
					) > 0
				)
			)
			WHERE H.status_nm = 'Active'
			GROUP BY
			H.hierarchy_id

			UNION ALL

			SELECT
						AH.hier_act_log_id AS id,
						$parentMid,
						CONCAT_WS(
						  '_',
						  P.project_name,
						  T.task_name,
						  S.sub_name,
						  J.job_name
					  ) AS text,
					  	AH.alloc_to as owner_mid,
						AH.progress,
						AH.start_dt,
						AH.end_dt,
						DATE_FORMAT(AH.est_end_dt, '%Y-%m-%d') AS deadline,
						NULL AS level_id_fk,
						$parentMidCode,
						AH.actual_start_date,
						AH.actual_end_date
						FROM
						(
						SELECT DISTINCT H.hierarchy_id,P.project_id_fk
			FROM
				(
				SELECT
					hierarchy_id,
					mid_code,
					level_id_fk
			FROM
				hierarchy_mst
			WHERE hierarchy_id = '$hierarchyId'
			) AS H
			INNER JOIN(
						SELECT
							HP.level_id_fk,
							HP.mid_code,
							(CASE WHEN HP.level_id_fk=1 THEN 3 ELSE 1 END) AS l_start,
							(CASE WHEN HP.level_id_fk=1 THEN 4 ELSE L.length_limit END) AS length_limit,
							L.level_code,
							HP.project_id_fk
						FROM
							user_permissions HP
						LEFT JOIN level_mst L ON
							L.level_id = HP.level_id_fk
						WHERE
							HP.user_id_fk = $userId AND HP.`status_nm` = 'Active'  AND IFNULL(HP.is_alloc,0)=1
					) P
					ON (
							(P.level_id_fk = -1)
						OR(IFNULL(P.mid_code, '') = '' AND P.level_id_fk <= H.level_id_fk)
						OR(H.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0)
						OR(H.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(SUBSTRING(H.mid_code, P.l_start, P.length_limit),P.mid_code)>0)
						)
					WHERE P.`level_id_fk` <= H.level_id_fk
						) FI
						INNER JOIN hier_act_log AH ON
						AH.p_hierarchy_id = FI.hierarchy_id AND AH.status_nm = 'Active'
						AND (AH.project_id_fk= FI.project_id_fk OR IFNULL(FI.project_id_fk,0)=0)
						LEFT JOIN project_mst P ON
						AH.project_id_fk = P.project_id
						LEFT JOIN task_mst T ON
						AH.task_id_fk = T.task_id
						LEFT JOIN sub_mst S ON
						AH.sub_task_id_fk = S.sub_id
						LEFT JOIN job_mst J ON
						AH.job_id_fk = J.job_id) TE
						LEFT JOIN hierarchy_mst SS_HM
						ON SS_HM.mid_code LIKE CONCAT('%', TE.p_mid_code, '%') GROUP BY TE.text";

		$sqlQuery = $this->db->query($query);
		// var_dump($this->db->last_query()); exit;
		return $sqlQuery->result_array();
	}

	public function getLevelsDataByMid($levelId = 0, $midCode = 0, $isAjax)
	{
		$levelIdOnDemand = $levelId;
		$userId = $this->session->userdata('user_id');

		if ($midCode == "0") {
			$subQuery = "";
			$pMid = "NULL as parent_mid";
		} else {
			$subQuery = "AND PR.mid_code = '$midCode'";
			$pMid = "'$midCode' as parent_mid";
		}
		if (!$isAjax) {
			$pMid = "NULL as parent_mid";
		}
		if ($isAjax) {
			$levelIdOnDemand = $levelId - 1;
		}
		$query = "SELECT DISTINCT
		    H.hierarchy_id,
		    H.mid_code,
		    $pMid,
		H.mid_name,
		H.owner_mid,
		H.status_nm,
		H.progress,
		H.start_dt,
		H.end_dt,
		(CASE WHEN H.level_id_fk=$levelIdOnDemand THEN NULL ELSE H.p_hierarchy_id END) AS p_hierarchy_id,
		H.level_id_fk,
		LMS.level_code,
		LMS.level_name,
		MAX(
		    CASE WHEN P.`level_id_fk` <= H.level_id_fk THEN 1 ELSE 0
		END
		) AS show_p
		FROM
		    (
		    SELECT
		        HR.hierarchy_id,
		        HR.mid_code,
		        HR.mid_name,
		        HR.owner_mid,
		        HR.status_nm,
		        HR.progress,
		        HR.start_dt,
		        HR.end_dt,
		        HR.p_hierarchy_id,
		        PR.mid_code AS p_mid_code,
		        HR.level_id_fk
		    FROM
		        hierarchy_mst HR
		    LEFT JOIN hierarchy_mst PR ON
		        HR.p_hierarchy_id = PR.hierarchy_id
		    WHERE
		        HR.level_id_fk =($levelId) $subQuery
		) AS H
		LEFT JOIN level_mst LMS ON
		    LMS.level_id = H.level_id_fk
		INNER JOIN(
		    SELECT
		        HP.level_id_fk,
		        HP.mid_code,
		        (
		            CASE WHEN HP.level_id_fk = 1 THEN 3 ELSE 1
		        END
		) AS l_start,
		(
		    CASE WHEN HP.level_id_fk = 1 THEN 4 ELSE L.length_limit
		END
		) AS length_limit,
		L.level_code
		FROM
		    user_permissions HP
		LEFT JOIN level_mst L ON
		    L.level_id = HP.level_id_fk
		WHERE
		    HP.user_id_fk = $userId AND HP.`status_nm` = 'Active'  AND IFNULL(HP.is_alloc,0)=1 
		) P
		ON
		    (
		        (P.level_id_fk = -1) OR(
		            IFNULL(P.mid_code, '') = '' AND P.level_id_fk <= H.level_id_fk
		        ) OR(
		            H.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0
		        ) OR(
		            H.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(
		                SUBSTRING(
		                    H.mid_code,
		                    P.l_start,
		                    P.length_limit
		                ),
		                P.mid_code
		            ) > 0
		        )
		    )
			WHERE H.status_nm = 'Active'
			GROUP BY
		    H.hierarchy_id";

		$sqlQuery = $this->db->query($query);
		return $sqlQuery->result_array();
	}

	public function getActivityData($deptId = 0, $levelId = 0, $hierarchyId = 0, $isAjax)
	{

			$userId = $this->session->userdata('user_id');

			// if ($hierarchyId == 0) {
					$subQuery = "1 = 1";
			// } else {
			// 		$subQuery = "p_hierarchy_id = " . $hierarchyId;
			// }

			if ($hierarchyId == 0) {
				return array();
				$subQuery = "";
			} else {
				$subQuery = "AND hierarchy_id = $hierarchyId";
			}

			// FIX: when comming from overview to any level (from url and not ajax) to show that levels own activity
			$pHierarchyIdCol = 'AH.p_hierarchy_id AS p_hierarchy_id';
			if (!$isAjax) {
				$pHierarchyIdCol = 'NULL AS p_hierarchy_id';
			}

			$query = "SELECT
			AH.hier_act_log_id AS hierarchy_id,
			'hier_act_log' AS TABLE_NAME,
			CONCAT_WS(
			  '_',
			  P.project_name,
			  T.task_name,
			  S.sub_name,
			  J.job_name
			) AS mid_code,
			'Activity' AS level_code,
			NULL AS level_id_fk,
			CONCAT_WS(
			  '_',
			  P.project_name,
			  T.task_name,
			  S.sub_name,
			  J.job_name
			) AS mid_name,
			$pHierarchyIdCol,
			AH.alloc_to as owner_mid,
		    AH.start_dt,
		    AH.end_dt,
			AH.est_end_dt,
		    AH.progress,
			AH.status_nm,
			AH.dept_id_fk,
			AH.project_id_fk,
			AH.task_id_fk,
			AH.sub_task_id_fk,
			AH.job_id_fk,
			AH.task_type,
			1 AS show_p
			FROM
			(
			SELECT DISTINCT
			  H.hierarchy_id,
			  P.project_id_fk 
			FROM
			(
			SELECT
			  hierarchy_id,
			  level_id_fk,
			  mid_code
			FROM
			hierarchy_mst
			WHERE dept_id_fk = ".$deptId." $subQuery
			) AS H
			INNER JOIN(
			SELECT HP.level_id_fk,
			  HP.mid_code,
			  (CASE WHEN HP.level_id_fk=1 THEN 3 ELSE 1 END) AS l_start,
				(CASE WHEN HP.level_id_fk=1 THEN 4 ELSE L.length_limit END) AS length_limit,
				L.level_code,
				HP.project_id_fk 
			FROM
			  user_permissions HP
			LEFT JOIN level_mst L ON
			  L.level_id = HP.level_id_fk
			WHERE
			  HP.user_id_fk = ".$userId." AND HP.status_nm = 'Active' AND HP.dept_id_fk = ".$deptId." 
			   AND IFNULL(HP.is_alloc,0)=1 
			) P
			ON
			(
				(P.level_id_fk = -1)
	  			OR(IFNULL(P.mid_code, '') = '' AND P.level_id_fk <= H.level_id_fk)
	  			OR(H.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0)
	  			OR(H.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(SUBSTRING(H.mid_code, P.l_start, P.length_limit),P.mid_code)>0)

			)
			WHERE P.`level_id_fk` <= H.level_id_fk
			) FI
			INNER JOIN hier_act_log AH ON
			AH.p_hierarchy_id = FI.hierarchy_id AND AH.status_nm = 'Active' 
			AND (AH.project_id_fk= FI.project_id_fk OR IFNULL(FI.project_id_fk,0)=0) 
			INNER JOIN activity_mst A1 
			ON A1.project_id_fk=AH.project_id_fk 
			AND A1.dept_id_fk =AH.dept_id_fk 
			AND A1.task_id_fk=AH.task_id_fk 
			AND A1.sub_id_fk =AH.sub_task_id_fk 
			AND A1.job_id_fk =AH.job_id_fk 
			AND A1.status_nm='Active' AND IFNULL(A1.is_closed,'N')='N' 
			LEFT JOIN project_mst P ON
			AH.project_id_fk = P.project_id
			LEFT JOIN task_mst T ON
			AH.task_id_fk = T.task_id
			LEFT JOIN sub_mst S ON
			AH.sub_task_id_fk = S.sub_id
			LEFT JOIN job_mst J ON
			AH.job_id_fk = J.job_id
			";

			$sqlQuery = $this->db->query($query);
			return $sqlQuery->result_array();
	}

	public function getSummaryActivityData($levelCode = 0, $levelId = 0, $hierarchyId = 0)
	{
		$userId = $this->session->userdata('user_id');


		if ($hierarchyId == 0) {
			return [];
		}
		$parentMid = "'$hierarchyId' AS parent";

		$query = "SELECT 
					AH.hier_act_log_id AS id,
					$parentMid,
					CONCAT_WS(
					  '_',
					  P.project_name,
					  T.task_name,
					  S.sub_name,
					  J.job_name
				  ) AS text,
					NULL AS level_id_fk,
					AH.alloc_to as owner_mid,
					DATE_FORMAT(AH.start_dt, '%d-%m-%Y') AS start_date,
					DATE_FORMAT(AH.end_dt, '%d-%m-%Y') AS end_date,
					AH.progress
					FROM
					(
					SELECT DISTINCT H.hierarchy_id,P.project_id_fk 
		FROM
			(
			SELECT
				hierarchy_id,
				mid_code,
				level_id_fk
		FROM
			hierarchy_mst
		WHERE hierarchy_id = '$hierarchyId'
		) AS H
		INNER JOIN(
					SELECT
						HP.level_id_fk,
						HP.mid_code,
						(CASE WHEN HP.level_id_fk=1 THEN 3 ELSE 1 END) AS l_start,
						(CASE WHEN HP.level_id_fk=1 THEN 4 ELSE L.length_limit END) AS length_limit,
						L.level_code,
						HP.project_id_fk 
					FROM
						user_permissions HP
					LEFT JOIN level_mst L ON
						L.level_id = HP.level_id_fk
					WHERE
						HP.user_id_fk = $userId AND HP.`status_nm` = 'Active'  AND IFNULL(HP.is_alloc,0)=1 
				) P
				ON (
						(P.level_id_fk = -1)
					OR(IFNULL(P.mid_code, '') = '' AND P.level_id_fk <= H.level_id_fk)
					OR(H.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0)
					OR(H.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(SUBSTRING(H.mid_code, P.l_start, P.length_limit),P.mid_code)>0)
					)
				WHERE P.`level_id_fk` <= H.level_id_fk 
					) FI
					INNER JOIN hier_act_log AH ON
					AH.p_hierarchy_id = FI.hierarchy_id AND AH.status_nm = 'Active' 
					AND (AH.project_id_fk= FI.project_id_fk OR IFNULL(FI.project_id_fk,0)=0) 
					LEFT JOIN project_mst P ON
					AH.project_id_fk = P.project_id
					LEFT JOIN task_mst T ON
					AH.task_id_fk = T.task_id
					LEFT JOIN sub_mst S ON
					AH.sub_task_id_fk = S.sub_id
					LEFT JOIN job_mst J ON
					AH.job_id_fk = J.job_id";

					$sqlQuery = $this->db->query($query);
					return $sqlQuery->result_array();
	}

	public function getActivityDataByMid($levelId = 0, $midCode = 0, $isAjax)
	{
		$userId = $this->session->userdata('user_id');

		if ($midCode == 0) {
			return [];
		}
		$pHierarchyIdCol = 'AH.p_hierarchy_id AS p_hierarchy_id';
		if (!$isAjax) {
			$parentMid = "NULL AS parent_mid";
			$pHierarchyIdCol = 'NULL AS p_hierarchy_id';
		} else {
			$parentMid = "'$midCode' AS parent_mid";
		}

		$query = "SELECT
					AH.hier_act_log_id AS hierarchy_id,
					'hier_act_log' AS TABLE_NAME,
					$parentMid,
					CONCAT_WS(
					  '_',
					  P.project_name,
					  T.task_name,
					  S.sub_name,
					  J.job_name
					) AS mid_code,
					'Activity' AS level_code,
					NULL AS level_id_fk,
					CONCAT_WS(
					  '_',
					  P.project_name,
					  T.task_name,
					  S.sub_name,
					  J.job_name
					) AS mid_name,
					$pHierarchyIdCol,
					AH.alloc_to as owner_mid,
				    AH.start_dt,
				    AH.end_dt,
				    AH.progress,
					AH.dept_id_fk,
					AH.project_id_fk,
					AH.task_id_fk,
					AH.sub_task_id_fk,
					AH.job_id_fk,
					1 AS show_p
					FROM
					(
					SELECT DISTINCT H.hierarchy_id,P.project_id_fk 
		FROM
		    (
		    SELECT
		        hierarchy_id,
		        mid_code,
		        level_id_fk
		FROM
		    hierarchy_mst
		WHERE mid_code = '$midCode'
		) AS H
		INNER JOIN(
					SELECT
						HP.level_id_fk,
						HP.mid_code,
						(CASE WHEN HP.level_id_fk=1 THEN 3 ELSE 1 END) AS l_start,
						(CASE WHEN HP.level_id_fk=1 THEN 4 ELSE L.length_limit END) AS length_limit,
						L.level_code,
						HP.project_id_fk 
					FROM
						user_permissions HP
					LEFT JOIN level_mst L ON
						L.level_id = HP.level_id_fk
					WHERE
						HP.user_id_fk = $userId AND HP.`status_nm` = 'Active'  AND IFNULL(HP.is_alloc,0)=1 
				) P
				ON (
						(P.level_id_fk = -1)
					OR(IFNULL(P.mid_code, '') = '' AND P.level_id_fk <= H.level_id_fk)
					OR(H.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0)
					OR(H.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(SUBSTRING(H.mid_code, P.l_start, P.length_limit),P.mid_code)>0)
					)
				WHERE P.`level_id_fk` <= H.level_id_fk
					) FI
					INNER JOIN hier_act_log AH ON
					AH.p_hierarchy_id = FI.hierarchy_id AND AH.status_nm = 'Active' 
					AND (AH.project_id_fk= FI.project_id_fk OR IFNULL(FI.project_id_fk,0)=0) 
					INNER JOIN activity_mst A1 
			ON A1.project_id_fk=AH.project_id_fk 
			AND A1.dept_id_fk =AH.dept_id_fk 
			AND A1.task_id_fk=AH.task_id_fk 
			AND A1.sub_id_fk =AH.sub_task_id_fk 
			AND A1.job_id_fk =AH.job_id_fk 
			AND A1.status_nm='Active' AND IFNULL(A1.is_closed,'N')='N' 
					LEFT JOIN project_mst P ON
					AH.project_id_fk = P.project_id
					LEFT JOIN task_mst T ON
					AH.task_id_fk = T.task_id
					LEFT JOIN sub_mst S ON
					AH.sub_task_id_fk = S.sub_id
					LEFT JOIN job_mst J ON
					AH.job_id_fk = J.job_id";

					$sqlQuery = $this->db->query($query);
					return $sqlQuery->result_array();
	}

	public function getActivityLogs($mid,$activityId)
	{	
	$query = "SELECT
				U.user_id,
				U.full_name,
				WU.wu_name,
				MIN(R.day_val) AS start_date,
				MAX(R.day_val) AS end_date,
				(
				CASE WHEN FIND_IN_SET(U.user_id, HA.alloc_to) > 0 THEN 'Allocated' ELSE 'Not Allocated'
				END
				) AS ind,
				SUM(
				IFNULL(
				R.final_xp,
				IFNULL(R.equiv_xp, 0)
				) + IFNULL(R.comp_xp, 0)
				) AS projected_xp,
				SUM(
				CASE WHEN R.tast_stat = 'Confirmed' THEN IFNULL(
				R.final_xp,
				IFNULL(R.equiv_xp, 0)
				) + IFNULL(R.comp_xp, 0) ELSE 0
				END
				) AS conf_xp,
				IFNULL(
				SUM(
				IFNULL(R.audit_work, R.work_comp)
				),
				0
				) AS w_done
				FROM
				response_mst R
				JOIN activity_mst A ON
				A.activity_id = R.activity_id_fk AND A.status_nm = 'Active'
				JOIN user_mst U ON
				R.user_id_fk = U.user_id
				JOIN work_unit_mst WU ON
				A.wu_id_fk = WU.wu_id
				JOIN(
				SELECT
				'$mid' AS mid_code,
				BA.dept_id_fk,
				BA.alloc_to,
				BA.project_id_fk,
				BA.task_id_fk,
				BA.sub_task_id_fk,
				BA.job_id_fk
				FROM
				hier_act_log BA
				WHERE
				BA.hier_act_log_id = '$activityId'
				) HA
				ON
				A.dept_id_fk = HA.dept_id_fk AND A.project_id_fk = HA.project_id_fk AND A.task_id_fk = HA.task_id_fk AND A.sub_id_fk = HA.sub_task_id_fk AND A.job_id_fk = HA.job_id_fk AND R.milestone_id = HA.mid_code
				WHERE
				R.status_nm = 'Active'
				GROUP BY
				U.user_id,
				U.full_name,
				WU.wu_name
				UNION
				SELECT
				U.user_id,
				U.full_name,
				WU.wu_name,
				NULL AS start_date,
				NULL AS end_date,
				'Not Logged' AS ind,
				0 AS projected_xp,
				0 AS conf_xp,
				0 AS w_done
				FROM
				(
				SELECT
				'$mid' AS mid_code,
				BA.dept_id_fk,
				BA.alloc_to,
				BA.project_id_fk,
				BA.task_id_fk,
				BA.sub_task_id_fk,
				BA.job_id_fk
				FROM
				hier_act_log BA
				WHERE
				BA.hier_act_log_id = '$activityId'
				) HA
				JOIN user_mst U ON
				FIND_IN_SET(U.user_id, HA.alloc_to) > 0
				JOIN activity_mst A ON
				A.dept_id_fk = HA.dept_id_fk AND A.project_id_fk = HA.project_id_fk AND A.task_id_fk = HA.task_id_fk AND A.sub_id_fk = HA.sub_task_id_fk AND A.job_id_fk = HA.job_id_fk AND A.status_nm = 'Active'
				JOIN work_unit_mst WU ON
				A.wu_id_fk = WU.wu_id
				LEFT JOIN response_mst R ON
				A.activity_id = R.activity_id_fk AND R.status_nm = 'Active' AND R.user_id_fk = U.user_id
				WHERE
				R.response_id IS NULL";
		$sqlQuery = $this->db->query($query);

			return $sqlQuery->result_array();
	}


	public function getActivityWithMid($deptId = 0, $levelId = 0, $hierarchyId = 0, $isAjax)
	{
		$userId = $this->session->userdata('user_id');

			$subQuery ="";
			if ($hierarchyId == 0) {
				$subQuery = "AND p_hierarchy_id IS NULL";
			} else {
				$subQuery = "AND hierarchy_id = $hierarchyId";
			}
			// FIX: when comming from overview to any level (from url and not ajax) to show that levels own activity
			$pHierarchyIdCol = 'AH.p_hierarchy_id AS p_hierarchy_id';
			if (!$isAjax) {
				$pHierarchyIdCol = 'NULL AS p_hierarchy_id';
			}

			$query = "SELECT 
			AH.hier_act_log_id AS hierarchy_id,
			'hier_act_log' AS TABLE_NAME,
			CONCAT_WS(
			  '_',
			  P.project_name,
			  T.task_name,
			  S.sub_name,
			  J.job_name
			) AS activity_name,
			'Activity' AS level_code,
			NULL AS level_id_fk,
			CONCAT_WS(
			  '_',
			  P.project_name,
			  T.task_name,
			  S.sub_name,
			  J.job_name
			) AS mid_name,
			$pHierarchyIdCol,
			AH.alloc_to as owner_mid,
		    AH.start_dt AS planned_s_dt,
				AH.end_dt  AS planned_e_dt,
				DATE(AH.actual_start_date) AS act_s_dt,
				DATE(AH. actual_end_date)  AS  act_e_dt,
			AH.est_end_dt,
		    AH.progress,
			AH.status_nm,
			AH.dept_id_fk,
			AH.project_id_fk,
			AH.task_id_fk,
			AH.sub_task_id_fk,
			AH.job_id_fk,
			AH.task_type,
			FI.mid_code as mid,
			1 AS show_p
			FROM
			(
			SELECT DISTINCT
			  H.hierarchy_id,
			  H.mid_code,
			  P.project_id_fk
			FROM
			(
			SELECT
			  hierarchy_id,
			  level_id_fk,
			  mid_code
			FROM
			hierarchy_mst
			WHERE dept_id_fk = ".$deptId." $subQuery
			) AS H
			INNER JOIN(
			SELECT HP.level_id_fk,
			  HP.mid_code,HP.project_id_fk,
			  (CASE WHEN HP.level_id_fk=1 THEN 3 ELSE 1 END) AS l_start,
				(CASE WHEN HP.level_id_fk=1 THEN 4 ELSE L.length_limit END) AS length_limit,
				L.level_code
			FROM
			  user_permissions HP
			LEFT JOIN level_mst L ON
			  L.level_id = HP.level_id_fk
			WHERE
			  HP.user_id_fk = ".$userId." AND HP.status_nm = 'Active' AND HP.dept_id_fk = ".$deptId."
			) P
			ON
			(
				(P.level_id_fk = -1)
	  			OR(IFNULL(P.mid_code, '') = '' AND P.level_id_fk <= H.level_id_fk)
	  			OR(H.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0)
	  			OR(H.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(SUBSTRING(H.mid_code, P.l_start, P.length_limit),P.mid_code)>0)

			)
			WHERE P.`level_id_fk` <= H.level_id_fk
			) FI
			INNER JOIN hier_act_log AH ON
			AH.p_hierarchy_id = FI.hierarchy_id AND AH.status_nm = 'Active' 
			AND (AH.project_id_fk= FI.project_id_fk OR IFNULL(FI.project_id_fk,0)=0) 
			INNER JOIN activity_mst A1 
			ON A1.project_id_fk=AH.project_id_fk 
			AND A1.dept_id_fk =AH.dept_id_fk 
			AND A1.task_id_fk=AH.task_id_fk 
			AND A1.sub_id_fk =AH.sub_task_id_fk 
			AND A1.job_id_fk =AH.job_id_fk 
			AND A1.status_nm='Active' AND IFNULL(A1.is_closed,'N')='N' 
			LEFT JOIN project_mst P ON
			AH.project_id_fk = P.project_id
			LEFT JOIN task_mst T ON
			AH.task_id_fk = T.task_id
			LEFT JOIN sub_mst S ON
			AH.sub_task_id_fk = S.sub_id
			LEFT JOIN job_mst J ON
			AH.job_id_fk = J.job_id";

			$sqlQuery = $this->db->query($query);
			return $sqlQuery->result_array();
	}
	public function getUsers($deptId = 0)
	{
				$this->db->select('UMST.*');
				$this->db->from('user_mst UMST');				
				$this->db->where('UMST.status_nm','Active');
				if ($deptId != 0) {
					$this->db->where(' (IFNULL(UMST.dept_id_fk,0)=0 OR FIND_IN_SET('.$deptId.',UMST.dept_id_fk)>0)',null,false);
				}
				$sqlQuery = $this->db->get();
				return $sqlQuery->result_array();
	}

	public function getLevelsList()
	{
		$this->db->select('LM.level_name, LM.level_id');
		$this->db->from('level_mst LM');
		$this->db->where('LM.status_nm','Active');
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}

	public function getLevelCodesList()
	{
		$this->db->select('LM.level_code, LM.level_id');
		$this->db->from('level_mst LM');
		$this->db->where('LM.status_nm','Active');
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}

	public function checkMidExists($mid,$dept){
		$this->db->distinct('mid_code');
		$this->db->select('mid_code as mid');
		$this->db->from('hierarchy_mst');
		$this->db->where_in('mid_code', $mid);
		$this->db->where('dept_id_fk',$dept);
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}

	/*public function checkActivityExists($activity,$dept){
		$this->db->select('activity_name as activity');
		$this->db->from('hier_act_log');
		$this->db->where_in('activity_name', $activity);
		$this->db->where('dept_id_fk',$dept);
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}*/

	public function getParentHierachyId($mid){
		$this->db->select('HM.p_hierarchy_id');
		$this->db->from('hierarchy_mst HM');
		$this->db->where('HM.mid_code',$mid);
		$result = $this->db->get()->row();

		return $result = $result ? $result->p_hierarchy_id : NULL ;
	}

	public function getMidData($mid,$dept){
		$response = NULL;
		$this->db->select("HM.hierarchy_id as hier_id,CASE WHEN HM.p_hierarchy_id = 0 THEN '' WHEN HM.p_hierarchy_id IS NULL THEN '' ELSE HM.p_hierarchy_id END as p_hier_id,
							HM.level_id_fk as level_id,HM.dept_id_fk as dept",FALSE);
		$this->db->from('hierarchy_mst HM');

		$this->db->where('HM.mid_code',$mid);
		$this->db->where('HM.dept_id_fk',$dept);
		$result = $this->db->get()->row();

		if($result){
			$hire_level_id = $result->level_id;
			$level_data = $this->db->select("level_id,level_name as level")->from("level_mst")
				->where(["level_id"=>$hire_level_id + 1])->get()->row();
			$response = array_merge((array)$result , (array)$level_data);
		}
		return $response ;
	}

	public function getLastId(){
		$this->db->select('HM.hierarchy_id');
		$this->db->order_by('HM.hierarchy_id', 'DESC');
		$this->db->from('hierarchy_mst HM');
		return $this->db->get()->row()->hierarchy_id;
	}

	public function getChildLevelId($parentLevelId)
	{
		$this->db->select('LM.level_id');
		$this->db->from('level_mst LM');
		$this->db->where('LM.p_level_id', $parentLevelId);
		$this->db->where('LM.status_nm','Active');
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}

	//Update bulk of level attributes
	public function updateLevelAttribute($attributeData , $userId ,$dept_id){
		$return['status'] = false;

		$userId = $userId ? $userId : NULL;
		if( $this->db->table_exists('upd_level_attributes') == FALSE && $userId)
		{
			$this->db->trans_start();

			$sql1 = 'CREATE TABLE upd_level_attributes (
				dept_id_fk INT(11) DEFAULT NULL,
				mid_name VARCHAR(500) DEFAULT NULL,
				mid_code VARCHAR(100) DEFAULT NULL,
				hier_desc VARCHAR(500) DEFAULT NULL,
				owner_mid_email VARCHAR(500) DEFAULT NULL,
				owner_mid VARCHAR(500) DEFAULT NULL,
				start_dt DATE NULL DEFAULT NULL,
				end_dt DATE NULL DEFAULT NULL,
				progress VARCHAR(40) DEFAULT NULL,
				status_nm VARCHAR(10) DEFAULT NULL,
				upd_user INT(11) DEFAULT NULL,
				upd_dt TIMESTAMP NULL DEFAULT NULL,
				presenter_email VARCHAR(500) DEFAULT NULL,
				presenter VARCHAR(500) DEFAULT NULL,
				storyboard_incharge_email VARCHAR(500) DEFAULT NULL,
				storyboard_incharge VARCHAR(500) DEFAULT NULL,
				sign_off_email VARCHAR(500) DEFAULT NULL,
				sign_off VARCHAR(500) DEFAULT NULL,
				est_end_dt TIMESTAMP NULL DEFAULT NULL,
				mandays VARCHAR(500) DEFAULT NULL,
				video_clip_type VARCHAR(255) DEFAULT NULL,
				storyboarded_by_email VARCHAR(500) DEFAULT NULL,
				storyboarded_by VARCHAR(500) DEFAULT NULL,
				duration VARCHAR(10) DEFAULT NULL,
				is_mid_exists INT(11) DEFAULT 0,
				show_p INT(11) DEFAULT 0
			)
			ENGINE=InnoDB;';

			$this->db->query($sql1);

			//Add all data of a csv into temp table
			$this->db->insert_batch('upd_level_attributes', $attributeData);

			//Update use id based on email
			$sql2 = "UPDATE upd_level_attributes UA SET owner_mid = ( SELECT DISTINCT GROUP_CONCAT(UM.user_id) FROM user_mst UM WHERE FIND_IN_SET(UM.user_login_name,UA.owner_mid_email) > 0),
			presenter = (SELECT DISTINCT GROUP_CONCAT(UM.user_id) FROM user_mst UM WHERE FIND_IN_SET(UM.user_login_name,UA.presenter_email) > 0),
			storyboard_incharge = (SELECT DISTINCT GROUP_CONCAT(UM.user_id) FROM user_mst UM WHERE FIND_IN_SET(UM.user_login_name,UA.storyboard_incharge_email) > 0),
			sign_off = (SELECT DISTINCT GROUP_CONCAT(UM.user_id) FROM user_mst UM WHERE FIND_IN_SET(UM.user_login_name,UA.sign_off_email) > 0),
			storyboarded_by = (SELECT DISTINCT GROUP_CONCAT(UM.user_id) FROM user_mst UM WHERE FIND_IN_SET(UM.user_login_name,UA.storyboarded_by_email) > 0)";

			$this->db->query($sql2);

			//Check mid access
			$this->midAccessUpdate("upd_level_attributes",$userId);

			//Delete records which doesn't have mid access
			$sql3 = "UPDATE upd_level_attributes UL
							INNER JOIN hierarchy_mst HM
							ON UL.mid_code = HM.mid_code AND
							UL.dept_id_fk = HM.dept_id_fk AND
							HM.status_nm = 'Active'
							SET UL.is_mid_exists = 1";
			$this->db->query($sql3);

			$status_qry =($dept_id == "2") ? ",H.status_nm = UP.status_nm" : '';

			//Updaqte attributes of a level based on csv data
			$sql4 = "UPDATE hierarchy_mst H
						INNER JOIN upd_level_attributes UP
						ON UP.mid_code = H.mid_code
						AND UP.dept_id_fk = H.dept_id_fk
						SET H.mid_name = UP.mid_name,
							H.hier_desc = UP.hier_desc,
							H.owner_mid = UP.owner_mid,
							H.start_dt = UP.start_dt,
							H.end_dt = UP.end_dt,
							H.progress = UP.progress,
							H.upd_user = UP.upd_user,
							H.upd_dt = current_timestamp,
							H.presenter = UP.presenter,
							H.storyboard_incharge = UP.storyboard_incharge,
							H.sign_off = UP.sign_off,
							H.est_end_dt = UP.est_end_dt,
							H.mandays = UP.mandays,
							H.video_clip_type = UP.video_clip_type,
							H.storyboarded_by = UP.storyboarded_by,
							H.duration = UP.duration".$status_qry."
							WHERE is_mid_exists=1 AND
							show_p = 1";
			$this->db->query($sql4);

			$qryCsvErrors = "SELECT mid_code as 'MID',
								mid_name as 'NAME',
								owner_mid_email as 'OWNER',
								start_dt as 'START DATE',
								end_dt as 'END DATE',
								est_end_dt as 'ESTIMATED END DATE',
								progress as 'PROGRESS',
								status_nm as 'STATUS',
								hier_desc	 as 'DESCRIPTION',
								presenter_email as 'PRESENTER',
								storyboard_incharge_email as 'STORYBOARD INCHARGE',
								sign_off_email as 'SIGN OFF',
								mandays as 'MANDAYS',
								video_clip_type as 'VIDEO CLIP TYPE',
								storyboarded_by_email as 'STORYBOARD BY',
								duration as 'DURATION',
								CONCAT_WS( ',',
								 	(CASE WHEN is_mid_exists=0 THEN 'Mid doesn\'t exists.' ELSE '' END),
								 	(CASE WHEN show_p=0 AND is_mid_exists = 1 THEN 'You don\'t have access.' ELSE '' END)
								 )
								  AS 'ATTRIBUTES ERRORS'
							FROM upd_level_attributes
							WHERE is_mid_exists = 0 OR show_p = 0";

			//Errors of a csv
			$csvErrors = $this->db->query($qryCsvErrors)->result_array();
			//Drop table after end of operation
			$sql6='DROP TABLE upd_level_attributes;';
			$this->db->query($sql6);


			$this->db->trans_complete();
			if($this->db->trans_status() === FALSE){
				$return['status'] = false;
			}
			else{
				$return['status'] = true;
				$return['errors'] = $csvErrors;
			}
		}else{
			$return['status'] = false;
		}
		return $return;
	}

	public function allocateActivity($allocateData,$userId){
	 	$return = ['status' => false];
	 	$errors = [];
	 	$userId = $userId ? $userId : NULL;


	 	if( $this->db->table_exists('blk_activity_allocate') == FALSE && $userId)
		{
			$this->db->trans_start();
			//Create temp table of csv
			$create_table = 'CREATE TABLE blk_activity_allocate
							(
								blk_id int(11) NOT NULL AUTO_INCREMENT,
								project_name VARCHAR(200) DEFAULT NULL,
								task_name VARCHAR(200) DEFAULT NULL,
								sub_name VARCHAR(500) DEFAULT NULL,
								job_name VARCHAR(500) DEFAULT NULL,
								task_type VARCHAR(200) DEFAULT NULL,
								mid_code VARCHAR(500) DEFAULT NULL,
								status VARCHAR(20) DEFAULT NULL,
								allocate_email TEXT DEFAULT NULL,
								allocate_to TEXT DEFAULT NULL,
								start_dt TIMESTAMP NULL DEFAULT NULL,
								end_dt TIMESTAMP NULL DEFAULT NULL,
								est_end_dt TIMESTAMP NULL DEFAULT NULL,
								progress VARCHAR(40) NULL DEFAULT NULL,
								dept_id_fk INT(11),
								project_id INT(11),
								task_id INT(11),
								sub_id INT(11),
								job_id INT(11),
								p_hier_id INT(11),
								is_hier_id INT(11) DEFAULT 0,
								is_activity_exists INT(11) DEFAULT 0,
								is_mid_exists INT(11) DEFAULT 0,
								show_p INT(11) DEFAULT 0,
								PRIMARY KEY(`blk_id`)
							)ENGINE=InnoDB;';

				$this->db->query($create_table);
				$this->db->insert_batch('blk_activity_allocate', $allocateData);

				//Update project based on csv data
				$sql9="UPDATE blk_activity_allocate A INNER JOIN project_mst P
							ON P.dept_id_fk=A.dept_id_fk AND A.project_name = P.project_name
							AND P.status_nm='Active'
						SET A.project_id = P.project_id;";
				$this->db->query($sql9);

				//Update task based on csv data
				$sql10="UPDATE blk_activity_allocate A INNER JOIN task_mst P
							ON P.dept_id_fk=A.dept_id_fk AND A.task_name = P.task_name AND P.status_nm='Active'
						SET A.task_id = P.task_id;";
				$this->db->query($sql10);

				//Update sub task based on csv data
				$sql11="UPDATE blk_activity_allocate A INNER JOIN sub_mst P
							ON P.dept_id_fk=A.dept_id_fk AND A.sub_name = P.sub_name AND P.status_nm='Active'
						SET A.sub_id = P.sub_id;";
				$this->db->query($sql11);

				//Update action based on csv data
				$sql12="UPDATE blk_activity_allocate A INNER JOIN job_mst P
							ON P.dept_id_fk=A.dept_id_fk AND A.job_name = P.job_name AND P.status_nm='Active'
						SET A.job_id = P.job_id;";
				$this->db->query($sql12);

				$sql13="UPDATE blk_activity_allocate A INNER JOIN hierarchy_mst P
							ON P.dept_id_fk=A.dept_id_fk AND A.mid_code = P.mid_code AND P.status_nm='Active'
						SET A.p_hier_id = P.hierarchy_id;";
				$this->db->query($sql13);
				
				
				//Update user id by given emails
				$sql14 = "UPDATE blk_activity_allocate BA 
				SET allocate_to = ( SELECT DISTINCT GROUP_CONCAT(UM.user_id) FROM user_mst UM WHERE FIND_IN_SET(UM.user_login_name,BA.allocate_email) > 0)";

				$this->db->query($sql14);
				
				
				
				//Check activity exists or not
				$sql15 = "UPDATE blk_activity_allocate BA
							INNER JOIN activity_mst A
							ON BA.project_id = A.project_id_fk AND
							BA.task_id = A.task_id_fk AND
							BA.sub_id = A.sub_id_fk AND
							BA.job_id = A.job_id_fk AND
							BA.dept_id_fk = A.dept_id_fk AND
							A.status_nm = 'Active'
							SET BA.is_activity_exists = 1";

				$this->db->query($sql15);

				//Check mid exists or not
				$sql16 = "UPDATE blk_activity_allocate BA
							INNER JOIN hierarchy_mst HM
							ON BA.mid_code = HM.mid_code AND
							BA.dept_id_fk = HM.dept_id_fk AND
							HM.status_nm = 'Active'
							SET BA.is_mid_exists = 1";
				$this->db->query($sql16);

				//Check mid access
				$this->midAccessUpdate("blk_activity_allocate",$userId);
				
				$sql16q = "UPDATE blk_activity_allocate BA
							INNER JOIN hier_act_log HA
							ON BA.dept_id_fk=HA.dept_id_fk AND
							BA.project_id=HA.project_id_fk AND
							BA.task_id=HA.task_id_fk AND
							BA.sub_id=HA.sub_task_id_fk AND
							BA.job_id=HA.job_id_fk AND
							BA.p_hier_id=HA.p_hierarchy_id
							SET BA.is_hier_id = HA.hier_act_log_id";
				$this->db->query($sql16q);
				
				

				//Update attributes of an activity based on csv
				$sql17="UPDATE hier_act_log HA INNER JOIN blk_activity_allocate BA
							ON BA.is_hier_id = HA.hier_act_log_id
							SET HA.alloc_to=BA.allocate_to,
								HA.start_dt = BA.start_dt,
								HA.end_dt = BA.end_dt,
								HA.est_end_dt = BA.est_end_dt,
								HA.status_nm = BA.status,
								HA.task_type = BA.task_type,
								HA.progress = BA.progress,
								HA.upd_user=$userId,
								HA.upd_dt=current_timestamp 
							WHERE BA.is_activity_exists = 1 AND
									BA.is_mid_exists = 1 AND
									BA.show_p = 1;";
				$this->db->query($sql17);

				$sql18 = "INSERT INTO hier_act_log (
							    p_hierarchy_id,
							    dept_id_fk,
							    project_id_fk,
							    task_id_fk,
							    sub_task_id_fk,
							    job_id_fk,
							    alloc_to,
							    start_dt,
							    end_dt,
							    progress,
							    status_nm,
							    ins_user,
							    ins_dt,
							    task_type,
							    est_end_dt
							)
							SELECT
							    p_hier_id,
							    dept_id_fk,
							    project_id,
							    task_id,
							    sub_id,
							    job_id,
							    allocate_to,
							    start_dt,
							    end_dt,
							    progress,
							    'Active' as status_nm,
							    $userId as user_id,
							    current_timestamp as ins_dt,
							    task_type,
							    est_end_dt
							FROM
							    blk_activity_allocate
							WHERE
							    is_hier_id =0 AND
							    is_activity_exists = 1 AND
							    is_mid_exists = 1 AND
							    show_p = 1;";

				$this->db->query($sql18);
				
				
				
				$sql16q = "UPDATE blk_activity_allocate BA
							INNER JOIN hier_act_log HA
							ON BA.dept_id_fk=HA.dept_id_fk AND
							BA.project_id=HA.project_id_fk AND
							BA.task_id=HA.task_id_fk AND
							BA.sub_id=HA.sub_task_id_fk AND
							BA.job_id=HA.job_id_fk AND
							BA.p_hier_id=HA.p_hierarchy_id
							SET BA.is_hier_id = HA.hier_act_log_id 
							WHERE BA.is_hier_id=0;";
				$this->db->query($sql16q);
				
				
				$sql15r='CREATE TABLE temp_bk_allocate
							(
								temp_blk_id int(11) NOT NULL AUTO_INCREMENT,
								status VARCHAR(20) DEFAULT NULL,
								allocate_us INT(11) DEFAULT NULL,
								blk_id_fk INT(11),
								p_hier_act_log_id INT(11),								
								p_hier_allc_ud_id INT(11),
								PRIMARY KEY(`temp_blk_id`)
							)ENGINE=InnoDB;';

				$this->db->query($sql15r);
				
				$sql16r='INSERT INTO temp_bk_allocate(allocate_us,blk_id_fk,p_hier_act_log_id,p_hier_allc_ud_id)
							SELECT U.user_id,blk_id,is_hier_id,HU.hier_alloc_id
							 FROM blk_activity_allocate A 
							 LEFT JOIN user_mst U 
								ON FIND_IN_SET(U.user_id,A.allocate_to)>0 
							LEFT JOIN hier_user_alloc HU 
							ON HU.hier_act_log_id_fk=A.is_hier_id
							AND HU.alloc_user=U.user_id 
								where IFNULL(A.allocate_to,"")<>"" AND IFNULL(A.is_hier_id,0)<>0 
								AND  A.show_p = 1
							';
				$this->db->query($sql16r);
				
				
				$sql16r='UPDATE hier_user_alloc HA SET HA.status_nm="InActive",HA.upd_user='.$userId.',HA.upd_dt=current_timestamp 
							WHERE HA.hier_act_log_id_fk IN (SELECT is_hier_id FROM blk_activity_allocate WHERE IFNULL(is_hier_id,0)<>0 ) 
							AND HA.status_nm<>"InActive" ;';
				$this->db->query($sql16r);
				
				$sql16r='UPDATE hier_user_alloc HA SET HA.status_nm="Active" 
							WHERE HA.hier_alloc_id IN (SELECT p_hier_allc_ud_id FROM temp_bk_allocate WHERE p_hier_allc_ud_id IS NOT NULL);
							 ';
				$this->db->query($sql16r);
				
				$sql17r='INSERT INTO hier_user_alloc(`hier_act_log_id_fk`, `alloc_user`,`status_nm`, `ins_user`, `ins_dt`)
						SELECT  p_hier_act_log_id,allocate_us,"Active", '.$userId.',current_timestamp
						FROM temp_bk_allocate 
						WHERE p_hier_allc_ud_id IS NULL;';
				$this->db->query($sql17r);
				
				$errors = "SELECT project_name as 'PROJECT',
								 task_name as 'TASK',
								 sub_name as 'SUB TASK',
								 job_name as 'JOB',
								 allocate_email as 'ALLOCATED TO',
								 mid_code as 'MID',
								 start_dt as 'START DATE',
								 end_dt  as 'END DATE',
								 est_end_dt as 'ESTIMATED END DATE',
								 progress as 'PROGRESS',
								 status as 'STATUS',
								 task_type as 'TASK TYPE',
								 CONCAT_WS( ',',
								 	(CASE WHEN is_activity_exists=0 THEN 'Activity not exists' ELSE '' END),
								 	(CASE WHEN is_mid_exists=0 THEN 'Mid not exists' ELSE '' END),
								 	(CASE WHEN show_p=0 AND is_mid_exists=1 THEN 'You don\'t have permission.' ELSE '' END)
								 )
								  AS 'ACTIVITY ERROR'
							FROM blk_activity_allocate
							WHERE
								show_p = 0 OR
							    is_activity_exists = 0 OR
							    is_mid_exists = 0;" ;

			$csvErrors = $this->db->query($errors)->result_array();

			//Drop table after end of operation
			$drop_table='DROP TABLE blk_activity_allocate;';
			$this->db->query($drop_table);
			
			$drop_table2='DROP TABLE temp_bk_allocate;';
			$this->db->query($drop_table2);
			
			$this->db->trans_complete();
			if($this->db->trans_status() === FALSE){
				$return['status'] = false;
			}
			else{
				$return['status'] = true;
				$return['errors'] = $csvErrors;
			}
		}else{
			/*$drop_table='DROP TABLE blk_activity_allocate;';
			$this->db->query($drop_table);*/
			//Table already exists error show.
			$return = false;
		}
		return $return;
	}

	public function midAccessUpdate($table,$userId){
		$updPermission = "UPDATE $table H
				INNER JOIN hierarchy_mst HM ON
				HM.mid_code = H.mid_code
				AND HM.dept_id_fk=H.dept_id_fk AND
				HM.status_nm='Active'
				INNER JOIN(
				SELECT
				HP.level_id_fk,
				HP.dept_id_fk,
				HP.mid_code,
				(CASE WHEN HP.level_id_fk=1 THEN 3 ELSE 1 END) AS l_start,
				(CASE WHEN HP.level_id_fk=1 THEN 4 ELSE L.length_limit END) AS length_limit,
				L.level_code
				FROM
				user_permissions HP
				LEFT JOIN level_mst L ON
				L.level_id = HP.level_id_fk
				WHERE
				HP.user_id_fk = $userId AND HP.`status_nm` = 'Active'  AND IFNULL(HP.is_alloc,0)=1 
				) P
				ON (
				(P.level_id_fk = -1)
				OR(IFNULL(P.mid_code, '') = '' AND P.level_id_fk <= HM.level_id_fk)
				OR(HM.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0)
				OR(HM.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(SUBSTRING(H.mid_code, P.l_start, P.length_limit),P.mid_code)>0)
				) AND H.dept_id_fk=P.dept_id_fk
				SET H.show_p=(CASE WHEN P.`level_id_fk` <= HM.level_id_fk THEN 1 ELSE 0 END);";

			//Update status of mid based on permission
			$this->db->query($updPermission);
	}

	public function fetchPermissionDept($u_id){
		$sql="SELECT D.dept_id,D.dept_name  FROM dept_mst D
			JOIN
			(SELECT hp.dept_id_fk,`user_id_fk`
			FROM `user_permissions` as hp
			join
			user_mst umst ON umst.user_id= hp.user_id_fk
			WHERE  `user_id_fk`='$u_id'  AND IFNULL(hp.is_alloc,0)=1  and  hp.status_nm='Active' GROUP BY hp.dept_id_fk) H
			on H.dept_id_fk=D.dept_id";
			$sqlQuery = $this->db->query($sql);
			return $sqlQuery->result_array();
	}

	public function getAllLevelData()
	{
		$this->db->select('*');
		$this->db->from('level_mst');
		$this->db->where('status_nm','Active');
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}

	public function getTopicChpater($topicId)
	{
		$this->db->select('*');
		$this->db->from('hierarchy_mst');
		$this->db->where('hierarchy_id',$topicId);
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}

	public function getVideoClipSummationByChapter($chapterId, $videoType)
	{
		$this->db->select('video_clip_type, SUM(duration) as submitted');
		$this->db->from('hierarchy_mst');
		$this->db->where('p_hierarchy_id in (select hierarchy_id from hierarchy_mst where p_hierarchy_id = "'.$chapterId.'")');
		$this->db->where('level_id_fk', '7');
		$this->db->where('video_clip_type', $videoType);
		$this->db->group_by('video_clip_type');
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}

	public function getChapterDurationInfoByVideoType($chapterId, $videoType)
	{
		$this->db->select('*');
		$this->db->from('chapter_duration_info');
		$this->db->where('chapter_id_fk',$chapterId);
		$this->db->where('video_clip_type',$videoType);
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}

	public function getChapterDurationInfoByChapterId($chapterId)
	{
		$this->db->select('*');
		$this->db->from('chapter_duration_info');
		$this->db->where('chapter_id_fk',$chapterId);
		$sqlQuery = $this->db->get();
		return $sqlQuery->result_array();
	}

	public function insertChapterDurationInfo($data)
	{
		$this->db->insert('chapter_duration_info', $data);
	}

	public function updateChapterDurationInfo($id, $data){
			$this->db->where('id',$id);
			$this->db->update('chapter_duration_info', $data);
	}

	public function getMidAlongWithChilds($hierarchyIds)
	{
		$hierarchyIdList = implode(',', $hierarchyIds);
		$query = "SELECT
			    T.mid_code,
			    T.mid_name,
			    GROUP_CONCAT(U.user_login_name) AS OWNER,
			    T.start_dt,
			    T.end_dt,
			    T.est_end_dt,
			    T.progress,
			    T.status_nm,
			    T.hier_desc,
			    GROUP_CONCAT(U1.user_login_name) AS presenter,
			    GROUP_CONCAT(U2.user_login_name) AS storyboard_incharge,
			    GROUP_CONCAT(U3.user_login_name) AS sign_off,
			    T.mandays,
			    T.video_clip_type,
			    GROUP_CONCAT(U4.user_login_name) AS storyboarded_by,
			    T.duration
			FROM
			    (
				SELECT HM.*
				FROM hierarchy_mst HM 
				INNER JOIN (SELECT 
							H.mid_code,
							H.dept_id_fk,
							(CASE WHEN H.level_id_fk=1 THEN 3 ELSE 1 END) AS l_start,
							(CASE WHEN H.level_id_fk=1 THEN 4 ELSE L.length_limit END) AS length_limit 
							FROM
							hierarchy_mst H
							LEFT JOIN level_mst L ON
							L.level_id = H.level_id_fk
							WHERE H.`status_nm` = 'Active' AND H.hierarchy_id IN (".$hierarchyIdList.") 
							)P 
				ON HM.dept_id_fk=P.dept_id_fk 
				AND FIND_IN_SET(SUBSTRING(HM.mid_code, P.l_start, P.length_limit),P.mid_code)>0 
				WHERE HM.status_nm='Active' 
			) AS T
			LEFT JOIN user_mst U ON
			    FIND_IN_SET(U.user_id, T.owner_mid)
			LEFT JOIN user_mst U1 ON
			    FIND_IN_SET(U1.user_id, T.presenter)
			LEFT JOIN user_mst U2 ON
			    FIND_IN_SET(U2.user_id, T.storyboard_incharge)
			LEFT JOIN user_mst U3 ON
			    FIND_IN_SET(U3.user_id, T.sign_off)
			LEFT JOIN user_mst U4 ON
			    FIND_IN_SET(U4.user_id, T.storyboarded_by) 
			GROUP BY
			    hierarchy_id";

			$sqlQuery = $this->db->query($query);
			return $sqlQuery->result_array();
	}

	public function getActivitybyPhierIds($hierarchyIds)
	{
	$hierarchyIdList = implode(',', $hierarchyIds);
   $userId= $this->session->userdata('user_id');
		$query = "SELECT
		        `P`.`project_name`,
		        `T`.`task_name`,
		        `S`.`sub_name`,
		         `J`.job_name,
		    GROUP_CONCAT(U.user_login_name) as alloc_to,
		    (SELECT mid_code from hierarchy_mst where hierarchy_id = ha.p_hierarchy_id) as mid,
		    ha.start_dt,
		    ha.end_dt,
		    ha.est_end_dt,
		    ha.progress,
		    ha.status_nm,
		    ha.task_type
			FROM
			(
			SELECT DISTINCT
			  H.hierarchy_id,
			  P.project_id_fk 
			FROM
			(
			SELECT HM.hierarchy_id,HM.level_id_fk,HM.mid_code ,
			HM.dept_id_fk 
				FROM hierarchy_mst HM 
				INNER JOIN (SELECT 
							H1.mid_code,
							H1.dept_id_fk,
							(CASE WHEN H1.level_id_fk=1 THEN 3 ELSE 1 END) AS l_start,
							(CASE WHEN H1.level_id_fk=1 THEN 4 ELSE L.length_limit END) AS length_limit 
							FROM
							hierarchy_mst H1
							LEFT JOIN level_mst L ON
							L.level_id = H1.level_id_fk
							WHERE H1.`status_nm` = 'Active' AND H1.hierarchy_id IN (".$hierarchyIdList.") 
							)P 
				ON HM.dept_id_fk=P.dept_id_fk 
				AND FIND_IN_SET(SUBSTRING(HM.mid_code, P.l_start, P.length_limit),P.mid_code)>0 
				WHERE HM.status_nm='Active' 
			
			) AS H
			INNER JOIN(
			SELECT HP.level_id_fk,
			  HP.mid_code,
			  (CASE WHEN HP.level_id_fk=1 THEN 3 ELSE 1 END) AS l_start,
				(CASE WHEN HP.level_id_fk=1 THEN 4 ELSE L.length_limit END) AS length_limit,
				L.level_code,
				HP.project_id_fk ,
				HP.dept_id_fk 
			FROM
			  user_permissions HP
			LEFT JOIN level_mst L ON
			  L.level_id = HP.level_id_fk
			WHERE
			  HP.user_id_fk = ".$userId." AND HP.status_nm = 'Active'   AND IFNULL(HP.is_alloc,0)=1 
			) P
			ON H.dept_id_fk=P.dept_id_fk  AND 
			(
				(P.level_id_fk = -1)
	  			OR(IFNULL(P.mid_code, '') = '' AND P.level_id_fk <= H.level_id_fk)
	  			OR(H.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0)
	  			OR(H.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(SUBSTRING(H.mid_code, P.l_start, P.length_limit),P.mid_code)>0)
			)
			WHERE P.`level_id_fk` <= H.level_id_fk
			) FI
			INNER JOIN hier_act_log ha ON
			ha.p_hierarchy_id = FI.hierarchy_id AND ha.status_nm = 'Active' 
			AND (ha.project_id_fk= FI.project_id_fk OR IFNULL(FI.project_id_fk,0)=0) 
			INNER JOIN activity_mst A1 
			ON A1.project_id_fk=ha.project_id_fk 
			AND A1.dept_id_fk =ha.dept_id_fk 
			AND A1.task_id_fk=ha.task_id_fk 
			AND A1.sub_id_fk =ha.sub_task_id_fk 
			AND A1.job_id_fk =ha.job_id_fk 
			AND A1.status_nm='Active' AND IFNULL(A1.is_closed,'N')='N'  
		LEFT JOIN `project_mst` AS `P`
		ON
		    `ha`.`project_id_fk` = `P`.`project_id`
		LEFT JOIN `task_mst` AS `T`
		ON
		    `ha`.`task_id_fk` = `T`.`task_id`
		LEFT JOIN `sub_mst` AS `S`
		ON
		    `ha`.`sub_task_id_fk` = `S`.`sub_id`
		LEFT JOIN `job_mst` AS `J`
		ON
		    `ha`.`job_id_fk` = `J`.`job_id`
		LEFT JOIN user_mst U on find_in_set(U.user_id, ha.alloc_to)>0
		GROUP BY ha.hier_act_log_id 
		";

		$sqlQuery = $this->db->query($query);
		return $sqlQuery->result_array();
	}

	public function getActivitybyActivityIds($activityIds)
	{
	$activityIdsList = implode(',', $activityIds);

		$query = "SELECT
		        `P`.`project_name`,
		        `T`.`task_name`,
		        `S`.`sub_name`,
		         `J`.job_name,
		    GROUP_CONCAT(U.user_login_name) as alloc_to,
		    (SELECT mid_code from hierarchy_mst where hierarchy_id = ha.p_hierarchy_id) as mid,
		    ha.start_dt,
		    ha.end_dt,
		    ha.est_end_dt,
		    ha.progress,
		    ha.status_nm,
		    ha.task_type
		FROM
		    `hier_act_log` AS `ha` 
			INNER JOIN activity_mst A1 
			ON A1.project_id_fk=ha.project_id_fk 
			AND A1.dept_id_fk =ha.dept_id_fk 
			AND A1.task_id_fk=ha.task_id_fk 
			AND A1.sub_id_fk =ha.sub_task_id_fk 
			AND A1.job_id_fk =ha.job_id_fk 
			AND A1.status_nm='Active' AND IFNULL(A1.is_closed,'N')='N'  
		LEFT JOIN `project_mst` AS `P`
		ON
		    `ha`.`project_id_fk` = `P`.`project_id`
		LEFT JOIN `task_mst` AS `T`
		ON
		    `ha`.`task_id_fk` = `T`.`task_id`
		LEFT JOIN `sub_mst` AS `S`
		ON
		    `ha`.`sub_task_id_fk` = `S`.`sub_id`
		LEFT JOIN `job_mst` AS `J`
		ON
		    `ha`.`job_id_fk` = `J`.`job_id`
		LEFT JOIN user_mst U on find_in_set(U.user_id, ha.alloc_to)
		WHERE
		    `ha`.`hier_act_log_id` IN ($activityIdsList)
		GROUP BY ha.hier_act_log_id";

		$sqlQuery = $this->db->query($query);
		return $sqlQuery->result_array();
	}
	
	public function updateActivityRow($id, $data)
	{
		$this->db->where('hier_act_log_id', $id);
		$this->db->update('hier_act_log', $data);
	}

	public function updateAllocUserRow($id, $data)
	{
		$this->db->where('hier_alloc_id', $id);
		$this->db->update('hier_user_alloc', $data);
	}
}
?>