<?php
class Crud_model extends CI_model{
		public $tx_cn='';	
	
	public function __construct(){
      $this->tx_cn='"'.$this->config->item('tx_cn').'"';	
   }
	public function view_tbl($file_nm,$tab_id)
	{
		if($file_nm=='create_mid')
		{ 	
		$this->db->select('U.activity_id AS tab_id,U.activity_id AS activity_id,
						U.activity_name AS activity_name,U.project_id_fk,U.task_id_fk,U.sub_id_fk,U.job_id_fk,
						U.wu_id_fk,IFNULL(U.is_closed,"N") AS closed,U.dept_id_fk,
						DATE_FORMAT(CONVERT_TZ(mg.ins_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS ins_dt,
						UP.full_name AS upd_user,			
						DATE_FORMAT(CONVERT_TZ(mg.upd_dt,"+00:00",'.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS upd_dt,
						mg.year,mg.gen_id,						
						mg.syllabus,mg.grade,mg.subject,mg.chapter,mg.sub_div,mg.div_num,
						mg.sec_sub_div,mg.sec_div_num,mg.third_sub_div,mg.third_div_num,mg.status_nm,
						mg.ins_user',FALSE);
						$this->db->from('activity_mst U');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
						$this->db->join('mid_gen mg', 'U.activity_id = mg.activity_id_fk','left');
						//$this->db->where('U.status_nm', 'Active');
						$this->db->where('mg.gen_id',$tab_id);
		}
		if($file_nm=='MediaMids')
		{ 	
		$this->db->select('mile_act_id,milestone_id,dept_id_fk,status_nm,project_id_fk');
						$this->db->from('mile_act_mst');
						$this->db->where('mile_act_id',$tab_id);
		}
		if($file_nm=='activity_mst')
		{
		$this->db->select('   U.activity_id AS tab_id,U.activity_id AS activity_id,
						U.activity_name AS activity_name,U.project_id_fk,U.task_id_fk,U.sub_id_fk,U.job_id_fk,
						U.wu_id_fk,IFNULL(U.is_closed,"N") AS closed,
						U.status_nm,U.dept_id_fk,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(U.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(U.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('activity_mst U');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
						 //$this->db->where('U.status_nm', 'Active');
						$this->db->where_in('U.activity_id', $tab_id);
		}
		if($file_nm=='work_unit_mst')
		{
		$this->db->select('   U.wu_id AS tab_id,U.wu_id AS work_unit_id,
						U.wu_name AS work_unit_name,U.is_quantify,
						U.status_nm,U.dept_id_fk,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(U.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(U.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('work_unit_mst U');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
						 //$this->db->where('U.status_nm', 'Active');
						$this->db->where_in('U.wu_id', $tab_id);
		}
		if($file_nm=='job_mst')
		{
		$this->db->select('   U.job_id AS tab_id,U.job_id,
						U.job_name,
						U.status_nm,U.dept_id_fk,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(U.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(U.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('job_mst U');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
						 //$this->db->where('U.status_nm', 'Active');
						$this->db->where_in('U.job_id', $tab_id);
		}
		if($file_nm=='dept_mst')
		{
		$this->db->select('   U.dept_id AS tab_id,U.dept_id,
						U.dept_name,
						U.status_nm,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(U.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(U.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('dept_mst U');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
						$this->db->where_in('U.dept_id', $tab_id);
		}
		if($file_nm=='sub_mst')
		{
		$this->db->select('   U.sub_id AS tab_id,U.sub_id,
						U.sub_name,
						U.status_nm,U.dept_id_fk,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(U.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(U.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('sub_mst U');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
						 //$this->db->where('U.status_nm', 'Active');
						$this->db->where_in('U.sub_id', $tab_id);
		}
		if($file_nm=='task_mst')
		{
		$this->db->select('   U.task_id AS tab_id,U.task_id,
						U.task_name,
						U.status_nm,U.dept_id_fk,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(U.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(U.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('task_mst U');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
						 //$this->db->where('U.status_nm', 'Active');
						$this->db->where_in('U.task_id', $tab_id);
		}
		if($file_nm=='project_mst')
		{
		$this->db->select('   U.project_id AS tab_id,U.project_id,
						U.project_name,
						U.status_nm,U.dept_id_fk,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(U.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(U.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('project_mst U');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
						// $this->db->where('U.status_nm', 'Active');
						$this->db->where_in('U.project_id', $tab_id);
		}
		if($file_nm=='access_mst')
		{
		$this->db->select('   U.access_id AS tab_id,U.access_id,
						U.icon,
						U.access_name,
						U.status_nm,
						U.tab_name,
						U.tab_color,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(U.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(U.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('access_mst U');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
						// $this->db->where('U.status_nm', 'Active');
						$this->db->where_in('U.access_id', $tab_id);
		}
		if($file_nm=='user_mst')
		{
		$this->db->select('   U.user_id AS tab_id,U.user_id AS id,
						U.user_login_name,
						U.emp_id,
						U.full_name,
						U.role_id_fk,
						U.project_id_fk,
						IFNULL(U.dept_id_fk,0) AS dept_id_fk,
						U.week_off,
						U.streak,
						U.access_name,
						U.manager_id AS manager,
						U.status_nm,
						DATE_FORMAT(CONVERT_TZ(U.joining_date_intern,"+00:00",'.$this->tx_cn.'), "%d-%m-%Y")  AS joining_date_intern,
						DATE_FORMAT(CONVERT_TZ(U.joining_date_permanant,"+00:00",'.$this->tx_cn.'), "%d-%m-%Y")  AS joining_date_permanant,
						U.slot,
						U.sub_department,
						U.unit_name,
						U.vertical,
						U.designation,
						U.manager_id_fk,
						U.project_owner_id_fk,
						U.poc_id_fk,
						IFNULL(U.is_grading,"N") AS is_greading,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(U.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(U.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('user_mst U');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
						// $this->db->where('U.status_nm', 'Active');
						$this->db->where_in('U.user_id', $tab_id);
		}
		
		if($file_nm=='role_mst')
		{
		$this->db->select('   U.role_id AS tab_id,U.role_id,
						U.role_name,
						U.access_name,
						U.status_nm,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(U.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(U.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('role_mst U');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
						 //$this->db->where('U.status_nm', 'Active');
						$this->db->where_in('U.role_id', $tab_id);
		}
				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return $resultSet;
	  }
	  else{
		return false;
	  }
	}
	
	public function fetchdept_data($u_id){
		$this->db->select(' D.dept_id,D.dept_name ',FALSE);
		$this->db->from('dept_mst D');						
		$this->db->join('user_mst U', 'U.user_id='.$u_id.' AND (IFNULL(U.dept_id_fk,0)=0 OR find_in_set(D.dept_id,U.dept_id_fk)<>0 ) ','inner');
		$this->db->where('D.status_nm', 'Active');
		$sqlQuery = $this->db->get();
		$resultSet = $sqlQuery->result_array();             
		
		if($resultSet)
          {
             return $resultSet;
          }
          else{
            return false;
          }
	}
 public function fetch_full_access($tbl_nm,$dept_oo=null)
 {
		$this->db->where('status_nm', 'Active');
			if($dept_oo)
			{
			 $this->db->where('dept_id_fk',$dept_oo);  
			}
			$res = $this->db->get($tbl_nm);                
          if($res->result_array())
          {
            return $res->result_array();
          }
          else{
            return false;
          } 
 }
 
  public function load_select_c($param,$dept_oo)
 {
		$this->db->where('status_nm', 'Active');
			if($dept_oo)
			{
			 $this->db->where('dept_id_fk',$dept_oo);  
			}
			if($param=="project_mst")
			{
			$this->db->select(' project_id AS p_id ,IFNULL(project_name,"")  AS p_nm  ',FALSE);		
			}
			if($param=="task_mst")
			{
			$this->db->select(' task_id AS p_id ,IFNULL(task_name,"")  AS p_nm  ',FALSE);		
			}
			if($param=="sub_mst")
			{
			$this->db->select(' sub_id AS p_id ,IFNULL(sub_name,"")  AS p_nm  ',FALSE);		
			}
			if($param=="job_mst")
			{
			$this->db->select(' job_id AS p_id ,IFNULL(job_name,"")  AS p_nm  ',FALSE);		
			}
			if($param=="work_unit_mst")
			{
			$this->db->select(' wu_id AS p_id ,IFNULL(wu_name,"")  AS p_nm  ',FALSE);		
			}
			
			$res = $this->db->get($param);                
          if($res->result_array())
          {
            return $res->result_array();
          }
          else{
            return false;
          } 
 }
	 public function fetch_user($id=null)
	 {
		 $this->db->select('U.user_id,U.full_name',FALSE);
		  $this->db->where('U.status_nm', 'Active');
		  if($id)
			  {
				$this->db->where('U.user_id!='.$id, null, false);
			  }
						$res = $this->db->get('user_mst U');                
				 if($res->result_array())
			  {
				 return $res->result_array();
			  }
			  else{
				return false;
			  } 
	 }
	 
	 public function update_tbl($ans){
		 $this->db->trans_start();
		 if($ans[12]=='activity_update')
				{
					$data = array(
							'activity_name' => $ans[1],
							'project_id_fk'=>$ans[2],
							'task_id_fk'=>$ans[3],
							'sub_id_fk'=>$ans[4],
							'job_id_fk'=>$ans[5],
							'wu_id_fk'=>$ans[6],
							'is_closed'=>($ans[7]=='N')? null:$ans[7],
							'upd_user' => $ans[8],
							'status_nm' => $ans[10],
							'dept_id_fk' => $ans[15]							
							);
					$this->db->where_in('activity_id', $ans[0]);
				}
		 if($ans[12]=='work_unit_update')
				{				 
					$data = array(
							'wu_name' => $ans[1],
							'is_quantify'=>$ans[7],
							'upd_user' => $ans[8],
							'status_nm' => $ans[10],
							'dept_id_fk' => $ans[15]							
							);
					$this->db->where_in('wu_id', $ans[0]);
				}
		 if($ans[12]=='job_update')
				{
					$data = array(
							'job_name' => $ans[1],
							'upd_user' => $ans[8],
							'status_nm' => $ans[10],
							'dept_id_fk' => $ans[15]							
							);
					$this->db->where_in('job_id', $ans[0]);
					
				}
		if($ans[12]=='dept_update')
				{
					$data = array(
							'dept_name' => $ans[1],
							'upd_user' => $ans[8],
							'status_nm' => $ans[10]						
							);
					$this->db->where_in('dept_id', $ans[0]);
				}		
		 if($ans[12]=='sub_update')
				{				 
					$data = array(
							'sub_name' => $ans[1],
							'upd_user' => $ans[8],
							'status_nm' => $ans[10],
							'dept_id_fk' => $ans[15]							
							);
					$this->db->where_in('sub_id', $ans[0]);
				}
		
		if($ans[12]=='task_update')
				{				 
					$data = array(
							'task_name' => $ans[1],
							'upd_user' => $ans[8],
							'status_nm' => $ans[10],
							'dept_id_fk' => $ans[15]							
							);
					$this->db->where_in('task_id', $ans[0]);
				}		 
		 if($ans[12]=='project_update')
				{				 
					$data = array(
							'project_name' => $ans[1],
							'upd_user' => $ans[8],
							'status_nm' => $ans[10],
							'dept_id_fk' => $ans[15]							
							);
					$this->db->where_in('project_id', $ans[0]);
				}
		
		 if($ans[12]=='access_update')
				{				 
					$data = array(
							'access_name' => $ans[1],
							'icon' => $ans[7],
							'tab_name' => $ans[3],
							'tab_color' => $ans[4],
							'upd_user' => $ans[8],
							'status_nm' => $ans[10]							
							);
					$this->db->where_in('access_id', $ans[0]);
				}
		
		if($ans[12]=='user_update')
				{				 
					$data = array(
							'user_login_name' => $ans[1],
							'emp_id' => $ans[2],
							//'week_off' => $ans[3],
							'full_name' => $ans[4],
							'role_id_fk' => $ans[5],
							'streak' => $ans[6],
							'access_name' => $ans[7],
							'upd_user' => $ans[8],
							'manager_id' => $ans[9],
							'status_nm' => $ans[10],
							'project_id_fk' => $ans[11],
							'dept_id_fk' => ($ans[15]=0?null:$ans[15]),
							'joining_date_intern' => $ans[16] ? date('Y-m-d',strtotime($ans[16])) : NULL,
							'joining_date_permanant' => $ans[17] ? date("Y-m-d",strtotime($ans[17])) : NULL,
							'slot' => $ans[18],
							'sub_department' => $ans[19],
							'unit_name' => $ans[20],
							'vertical' => $ans[21],
							'designation' => $ans[22],
							'manager_id_fk' => $ans[23],
							'project_owner_id_fk' => $ans[24],
							'poc_id_fk' => $ans[25],
							'is_grading' => $ans[26]
							);
					$this->db->where_in('user_id', $ans[0]);
				}
			if($ans[12]=='role_update')
				{				 
					$data = array(
							'role_name' => $ans[1],
							'access_name' => $ans[7],
							'upd_user' => $ans[8],
							'status_nm' => $ans[10]							
							);
					$this->db->where_in('role_id', $ans[0]);
				}
				$this->db->set('upd_dt', 'current_timestamp', FALSE);				
				$this->db->update($ans[13], $data);	
				
				if($ans[13]=="project_mst"||$ans[13]=="task_mst"||$ans[13]=="sub_mst"||$ans[13]=="job_mst")
				{
					$id=str_replace("mst","id_fk",$ans[13]);
						$sql1='UPDATE
									activity_mst A
								JOIN project_mst P ON
									A.project_id_fk = P.project_id 
								JOIN task_mst T ON
									A.task_id_fk = T.task_id 
								JOIN sub_mst S ON
									A.sub_id_fk = S.sub_id 
								LEFT JOIN job_mst J ON
									A.job_id_fk = J.job_id 
								 SET A.activity_name=CONCAT_WS("_",P.project_name,T.task_name,S.sub_name,J.job_name) 
								WHERE
									A.'.$id.' IN ('.$ans[0].') ;'; 
									
							$this->db->query($sql1); 
				}


				if($ans[12]=="work_unit_update"||$ans[12]=="project_update"||$ans[12]=="task_update"||$ans[12]=="sub_update"||$ans[12]=="job_update")
				{
					if((strtolower($ans[10])=="inactive" && strtolower($ans[14])=='active')||(strtolower($ans[10])=="active" && strtolower($ans[14])=='inactive'))
					{
						$id_st=str_replace("update","id_fk",$ans[12]);
						if($ans[12]=="work_unit_update")
						{
							$id_st="wu_id_fk";
						}
						//$tabll=str_replace("update","",$ans[12]);
						$sql1='UPDATE
									activity_mst A 
								 SET A.status_nm="'.$ans[10].'" ,upd_user='.$ans[8].',upd_dt=current_timestamp 
								WHERE
									A.'.$id_st.' IN ('.$ans[0].') ;'; 
									
							$this->db->query($sql1); 
							
							
						$sql1='UPDATE
									response_mst A 								
								 SET A.status_nm="'.$ans[10].'"  ,upd_user='.$ans[8].',upd_dt=current_timestamp 
								WHERE
									A.activity_id_fk IN 
									(SELECT P.activity_id FROM activity_mst P WHERE P.'.$id_st.' IN ('.$ans[0].')) ;';
									
							$this->db->query($sql1); 
					}				

				}			
				
				if($ans[12]=='user_update' && strtolower($ans[10])!='active' && strtolower($ans[14])=='active')
				{
					//hist_response_mst
					
					$data = array(							
							'upd_user' => $ans[8],
							'rev_assignee'=>null,
							'assignee_desc'=>'User made Inactive'
							);
					$this->db->set('upd_dt', 'current_timestamp', FALSE);				
					$this->db->where_in('rev_assignee', $ans[0]);
					$this->db->where_in('tast_stat', 'Review Pending');
					$this->db->where_in('status_nm', 'Active');
					$this->db->update('response_mst', $data);	
				}
				$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return "success";
		  }					
	 }
	 	 
	 public function delete_tbl($ans){
		 
		 if($ans[12]=='std_tgt_delete' && $ans[1])
				{
					$this->db->trans_start();
					
					$key=explode(",",$ans[1]);
					
					$this->db->where_in('std_tgt_id',$key );
					$this->db->delete($ans[13]);
					
						$this->db->trans_complete();
						if($this->db->trans_status() === FALSE)
						  {
							 return false;
						  }
						  else{
							 return "success";
						  }
				}					
	 }	 
	 
	  public function ins_tbl($ans){
		 $this->db->trans_start();
		 
		 if($ans[12]=='create_mid')
				{				 
					if($this->db->table_exists('Contentmids')==False){
	    			$sql='CREATE TABLE Contentmids
	    				(
						`gen_id` int(11) NOT NULL AUTO_INCREMENT,				
    					`activity_id` varchar(50)DEFAULT NULL,
    					`dept_id`int(12)DEFAULT null,
						 project_id INT(11),
						 task_id INT(11),
						 sub_id INT(11),
					   	 job_id INT(11),
    					`year`varchar(50)DEFAULT null,
    					`syllabus`varchar(50)DEFAULT NULL,
    					`grade`varchar(50)DEFAULT NULL,
    					`subject`varchar(50) DEFAULT NULL,
    					`chapter`varchar(50)DEFAULT null,
    					`sub_div` varchar(50)DEFAULT NULL,
    					`div_num` varchar(50)DEFAULT NULL,
						`sec_sub_div` varchar(50)DEFAULT NULL,
    					`sec_div_num` varchar(50)DEFAULT NULL,
    					`third_sub_div` varchar(50)DEFAULT NULL,
    					`third_div_num` varchar(50)DEFAULT NULL,
    					`status_nm` varchar(50)DEFAULT NULL,
    					`ins_user` int(11),
						PRIMARY KEY(`gen_id`)
    				)ENGINE=InnoDB;';
					$this->db->query($sql);
					$data = array(
							'project_id' =>$ans[13],
							'task_id'=>$ans[14],
							'sub_id'=>$ans[15],
							'job_id'=>$ans[16],
							'year'=>$ans[17],
							'sec_sub_div'=>!empty($ans[18]) ?$ans[18] : NULL,
							'sec_div_num'=>!empty($ans[19]) ?$ans[19] : NULL,
							'third_sub_div'=>!empty($ans[20]) ?$ans[20] : NULL,
							'third_div_num'=>!empty($ans[21]) ?$ans[21] : NULL,
							'dept_id'=>$ans[2],
							'status_nm'=>$ans[3],
							'syllabus'=>!empty($ans[4]) ? $ans[4] : NULL,
							'subject'=>!empty($ans[5]) ? $ans[5] : NULL,
							'chapter'=>!empty($ans[6]) ? $ans[6] : NULL,
							'sub_div'=>!empty($ans[7]) ? $ans[7] : NULL,
							'div_num'=>!empty($ans[8]) ? $ans[8] : NULL,
							'grade'=>!empty($ans[9]) ? $ans[9] : NULL,
							'ins_user'=>$ans[10]
							);	
						//print_r($data);die;
		    		$this->db->insert('Contentmids',$data);
		    		$sql5="UPDATE Contentmids A INNER JOIN activity_mst P 
							ON P.dept_id_fk=A.dept_id AND 
							A.project_id=P.project_id_fk AND 
							A.task_id=P.task_id_fk AND 
							A.sub_id=P.sub_id_fk AND 
							A.job_id=P.job_id_fk AND 
							P.status_nm='Active' AND
							IFNULL(P.is_closed,'N')='N'
							SET A.activity_id=P.activity_id;";
					$this->db->query($sql5);
					
					$sql45="UPDATE mid_gen MG INNER JOIN Contentmids A
							ON MG.activity_id_fk=A.activity_id 
					 SET MG.status_nm='InActive' WHERE MG.status_nm='Active';";
					$this->db->query($sql45);					
					
					$sql2="INSERT INTO mid_gen(activity_id_fk,dept_id_fk,year,syllabus,grade,subject ,chapter,sub_div,div_num,sec_sub_div,sec_div_num,third_sub_div,third_div_num,status_nm,ins_user,ins_dt)
					SELECT DISTINCT A.activity_id,A.dept_id,A.year,A.syllabus,A.grade,A.subject,A.chapter,A.sub_div,A.div_num,A.sec_sub_div,A.sec_div_num,A.third_sub_div,A.third_div_num,A.status_nm,A.ins_user,current_timestamp
					 FROM Contentmids A  WHERE A.activity_id IS NOT NULL;";
					$this->db->query($sql2);
					
			
			$sql35='DROP TABLE Contentmids;';
			$this->db->query($sql35);
				}
				}else{
		 if($ans[12]=='activity_insert')
				{				 
					$data = array(
							'activity_name' => $ans[1],
							'project_id_fk'=>$ans[2],
							'task_id_fk'=>$ans[3],
							'sub_id_fk'=>$ans[4],
							'job_id_fk'=>$ans[5],
							'wu_id_fk'=>$ans[6],
							'ins_user' => $ans[8],
							'status_nm' => $ans[10],
							'dept_id_fk' => $ans[15]							
							);
				}
		 if($ans[12]=='work_unit_insert')
				{				 
					$data = array(
							'wu_name' => $ans[1],
							'is_quantify'=>$ans[7],
							'ins_user' => $ans[8],
							'status_nm' => $ans[10],
							'dept_id_fk' => $ans[15]							
							);
				}
		 if($ans[12]=='job_insert')
				{				 
					$data = array(
							'job_name' => $ans[1],
							'ins_user' => $ans[8],
							'status_nm' => $ans[10],
							'dept_id_fk' => $ans[15]
							);
				}
		if($ans[12]=='dept_insert')
				{				 
					$data = array(
							'dept_name' => $ans[1],
							'ins_user' => $ans[8],
							'status_nm' => $ans[10]
							);
				}
		 if($ans[12]=='sub_insert')
				{				 
					$data = array(
							'sub_name' => $ans[1],
							'ins_user' => $ans[8],
							'status_nm' => $ans[10],
							'dept_id_fk' => $ans[15]
							);
				}
		 if($ans[12]=='task_insert')
				{				 
					$data = array(
							'task_name' => $ans[1],
							'ins_user' => $ans[8],
							'status_nm' => $ans[10],
							'dept_id_fk' => $ans[15]
							);
				}
		 if($ans[12]=='project_insert')
				{				 
					$data = array(
							'project_name' => $ans[1],
							'ins_user' => $ans[8],
							'status_nm' => $ans[10],
							'dept_id_fk' => $ans[15]
							);
				}
		 if($ans[12]=='access_insert')
				{				 
					$data = array(
							'access_name' => $ans[1],
							'icon' => $ans[7],
							'tab_name' => $ans[3],
							'tab_color' => $ans[4],
							'ins_user' => $ans[8],
							'status_nm' => $ans[10]							
							);
				}
		if($ans[12]=='user_insert')
				{				 
					$data = array(
							'user_login_name' => $ans[1],
							'emp_id' => $ans[2],
							//'week_off' => $ans[3],
							'full_name' => $ans[4],
							'role_id_fk' => $ans[5],
							'streak' => $ans[6],
							'access_name' => $ans[7],
							'ins_user' => $ans[8],
							'manager_id' => $ans[9],
							'status_nm' => $ans[10],
							'project_id_fk' => $ans[11],
							'dept_id_fk' => ($ans[15]=0?null:$ans[15]),
							'joining_date_intern' => $ans[16] ? date('Y-m-d',strtotime($ans[16])) : NULL,
							'joining_date_permanant' => $ans[17] ? date("Y-m-d",strtotime($ans[17])) : NULL,
							'slot' => $ans[18],
							'sub_department' => $ans[19],
							'unit_name' => $ans[20],
							'vertical' => $ans[21],
							'designation' => $ans[22],
							'manager_id_fk' => $ans[23],
							'project_owner_id_fk' => $ans[24],
							'poc_id_fk' => $ans[25],
							'is_grading' => $ans[26]
							);
				}
				if($ans[12]=='role_insert')
				{				 
					$data = array(
							'role_name' => $ans[1],
							'access_name' => $ans[7],
							'ins_user' => $ans[8],
							'status_nm' => $ans[10]							
							);
				}
				
				$this->db->set('ins_dt', 'current_timestamp', FALSE);				
				$this->db->insert($ans[13], $data);		
				
				if($ans[12]=='activity_insert')
				{				 
					$a=$this->db->insert_id();
				$day= date('Y-m-d');
				$day_w  = date('w', strtotime($day));
				if ($day_w == 0) {
					$day_w = 7;					
				}				
				$st_dt_0 = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
					$data = array(
							'tgt_val' => $ans[7],
							'activity_id_fk'=>$a,
							'start_dt'=>$st_dt_0,
							'ins_user' => $ans[8],
							'status_nm' => $ans[10]							
							);				
				$this->db->set('ins_dt', 'current_timestamp', FALSE);
				$this->db->insert('std_tgt_mst', $data);		
				}
				//Add user default week off
				if($ans[12]=='user_insert'){
					$userId = $this->db->insert_id();
					$loginUserId = $this->session->userdata('user_id');
					$userWeekOff = [
						[
							'user_id_fk' => $userId,
							'week_day' => 5,
							'start_dt' => date("Y-m-d"),
							'end_dt' => date("Y-m-d"),
							'conf_status' => "Approved",
							'status_nm' => "Active",
							'ins_user' => $loginUserId,
							'ins_dt' => date("Y-m-d H:i:s"),
						],
						[
							'user_id_fk' => $userId,
							'week_day' => 6,
							'start_dt' => date("Y-m-d"),
							'end_dt' => date("Y-m-d"),
							'conf_status' => "Approved",
							'status_nm' => "Active",
							'ins_user' => $loginUserId,
							'ins_dt' => date("Y-m-d H:i:s"),
						]
					];

					$this->db->insert_batch('emp_week_off', $userWeekOff); 
				}
				
	  }
				$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return "success";
		  }					
	 }
	 
	 public function content_mid_update($ans){
				//print_r($ans);die;
				if($this->db->table_exists('Contentmids_edit')==False){
	    			$sql='CREATE TABLE Contentmids_edit
	    				(
	    				gen_id	int(12) DEFAULT null,
    					`activity_id` varchar(50)DEFAULT NULL,
    					`dept_id`int(12)DEFAULT null,
						 project_id INT(11),
						 task_id INT(11),
						 sub_id INT(11),
					   	 job_id INT(11),
    					`year`varchar(50)DEFAULT null,
    					`syllabus`varchar(50)DEFAULT NULL,
    					`grade`varchar(50)DEFAULT NULL,
    					`subject`varchar(50) DEFAULT NULL,
    					`chapter`varchar(50)DEFAULT null,
    					`sub_div` varchar(50)DEFAULT NULL,
    					`div_num` varchar(50)DEFAULT NULL,
						`sec_sub_div` varchar(50)DEFAULT NULL,
    					`sec_div_num` varchar(50)DEFAULT NULL,
    					`third_sub_div` varchar(50)DEFAULT NULL,
    					`third_div_num` varchar(50)DEFAULT NULL,
    					`status_nm` varchar(50)DEFAULT NULL,
    					`ins_user` int(11)
    				)ENGINE=InnoDB;';
					$this->db->query($sql);
					
					$d_id=$ans[1];
					$user_id=$ans[13];
					$data = array(
							'gen_id'=>$ans[0],
							'dept_id'=>$ans[1],
							'project_id' =>$ans[2],
							'task_id'=>$ans[3],
							'sub_id'=>$ans[4],
							'job_id'=>$ans[5],
							'year'=>$ans[16],
							'status_nm'=>$ans[14],
							'syllabus'=>!empty($ans[8]) ? $ans[8] : NULL,
							'subject'=>!empty($ans[15]) ? $ans[15] : NULL,
							'chapter'=>!empty($ans[9]) ? $ans[9] : NULL,
							'sub_div'=>!empty($ans[6]) ? $ans[6] : NULL,
							'div_num'=>!empty($ans[7]) ? $ans[7] : NULL,
							'sec_sub_div'=>!empty($ans[17]) ?$ans[17] : NULL,
							'sec_div_num'=>!empty($ans[18]) ?$ans[18] : NULL,
							'third_sub_div'=>!empty($ans[19]) ?$ans[19] : NULL,
							'third_div_num'=>!empty($ans[20]) ?$ans[20] : NULL,
							'grade'=>!empty($ans[10]) ?$ans[10] : NULL,
							'ins_user'=>$ans[13]
							);	
						//print_r($data);die;
		    		$this->db->insert('Contentmids_edit',$data);
					
		    		$sql5="UPDATE Contentmids_edit A INNER JOIN activity_mst P 
							ON P.dept_id_fk=A.dept_id AND 
							A.project_id=P.project_id_fk AND 
							A.task_id=P.task_id_fk AND 
							A.sub_id=P.sub_id_fk AND 
							A.job_id=P.job_id_fk AND 
							P.status_nm='Active' AND IFNULL(P.is_closed,'N')='N' 
							SET A.activity_id=P.activity_id;";
					$this->db->query($sql5);
					
					
					$sql6="UPDATE mid_gen A 
								INNER JOIN Contentmids_edit P on
								A.gen_id=P.gen_id   
							SET A.activity_id_fk=P.activity_id,A.dept_id_fk=P.dept_id,A.year=P.year,
							A.syllabus=P.syllabus,A.grade=P.grade,A.subject=P.subject,A.chapter=P.chapter,A.sub_div=P.sub_div,A.div_num=P.div_num,A.sec_sub_div=P.sec_sub_div,A.sec_div_num=P.sec_div_num,A.third_sub_div=P.third_sub_div,A.third_div_num=P.third_div_num,
							A.status_nm=P.status_nm,A.upd_user=P.ins_user,A.upd_dt=current_timestamp
							 WHERE P.activity_id IS NOT NULL and A.gen_id=".$ans[0]."";
					$this->db->query($sql6);
					$affected_rows=$this->db->affected_rows();
					
								$sql_1="DROP TABLE IF EXISTS temp_mid_conv_act";
				$this->db->query($sql_1);	

			$sql_2="CREATE TABLE temp_mid_conv_act
					(
					  `t_id` int(11) NOT NULL AUTO_INCREMENT,
					  `activity_id_fk` int(11) DEFAULT NULL, 
					  `dept_id_fk` int(11) DEFAULT NULL, 
					  `mid_code` varchar(100) DEFAULT NULL,
					  `project_id_fk` int(11) DEFAULT NULL, 
					  `task_id_fk` int(11) DEFAULT NULL, 
					  `sub_id_fk` int(11) DEFAULT NULL, 
					  `job_id_fk` int(11) DEFAULT NULL, 
					  `hierarchy_id_fk` int(11) DEFAULT NULL, 
					  `hier_act_log` int(11) DEFAULT NULL, 
					  PRIMARY KEY(`t_id`)
					) ENGINE=InnoDB ;";
			$this->db->query($sql_2);
			$sql_3='INSERT INTO temp_mid_conv_act(activity_id_fk,dept_id_fk,mid_code,project_id_fk,task_id_fk,sub_id_fk,job_id_fk)	
					SELECT DISTINCT 
					    A.activity_id,A.dept_id,
						CONCAT_WS("",A.year_nm,B.syll_name,C.grade_name,D.subj_name,E.chapter_nm,F.sub_div_nm,G.div_no,H.sec_div_nm,I.sec_div_no) AS milestone_id,
						A.project_id,
							A.task_id,
							A.sub_id,
							A.job_id
					FROM
					    (
					    SELECT M.activity_id,
							M.dept_id,
					        M.gen_id,
							M.project_id,
							M.task_id,
							M.sub_id,
							M.job_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.year, ",", Num.n),",",-1) AS year_nm
					    FROM
					        numbers Num
					    INNER JOIN Contentmids_edit M 
						ON CHAR_LENGTH(M.year) - CHAR_LENGTH(REPLACE(M.year, ",", "")) >= Num.n -1 
						WHERE `M`.`status_nm` = "Active"

					) A
					JOIN(
					    SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.syllabus, ",", Num.n),",",-1) AS syll_name
					    FROM
					        numbers Num
					    INNER JOIN Contentmids_edit M 
						ON CHAR_LENGTH(M.syllabus) - CHAR_LENGTH(REPLACE(M.syllabus, ",", "")) >= Num.n -1 
						WHERE  `M`.`status_nm` = "Active" 
						) B
					ON B.gen_id = A.gen_id
					LEFT JOIN(
					    SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.grade, ",", Num.n),",",-1) AS grade_name
					    FROM
					        numbers Num
					    INNER JOIN Contentmids_edit M 
						ON CHAR_LENGTH(M.grade) - CHAR_LENGTH(REPLACE(M.grade, ",", "")) >= Num.n -1 
						WHERE  `M`.`status_nm` = "Active"

						) C
					ON C.gen_id = A.gen_id
					LEFT JOIN(
					    SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.subject, ",", Num.n),",",-1) AS subj_name
					    FROM
					        numbers Num
					    INNER JOIN Contentmids_edit M 
						ON CHAR_LENGTH(M.subject) - CHAR_LENGTH(REPLACE(M.subject, ",", "")) >= Num.n -1 
						WHERE `M`.`status_nm` = "Active" 
						) D
					ON D.gen_id = A.gen_id
					LEFT JOIN(
					    SELECT M.gen_id,
						CASE WHEN M.chapter="XX" THEN  M.chapter ELSE LPAD(Num.n, 2, "0") END  AS chapter_nm 
					    FROM
					        numbers Num
					    INNER JOIN Contentmids_edit M 
					       ON (M.chapter="XX" AND Num.n=1) 
					    OR  ( M.chapter<>"XX" AND Num.n <= M.chapter  )
							WHERE `M`.`status_nm` = "Active"

					) E 
					ON E.gen_id = A.gen_id 
					LEFT JOIN(
					SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.sub_div,  ",", Num.n),",",-1) AS sub_div_nm
					    FROM
					        numbers Num
					    INNER JOIN Contentmids_edit M 
						ON CHAR_LENGTH(M.sub_div) - CHAR_LENGTH(REPLACE(M.sub_div, ",", "")) >= Num.n -1 
						WHERE  `M`.`status_nm` = "Active"

					) F
					ON F.gen_id = A.gen_id
					LEFT JOIN(
						SELECT M.gen_id,
					        LPAD(Num.n, 3, "0") AS div_no
					    FROM
					        numbers Num
					    INNER JOIN Contentmids_edit M ON
					        Num.n <= M.div_num 
							WHERE  `M`.`status_nm` = "Active"

					) G
					ON G.gen_id = A.gen_id
					LEFT JOIN(
					SELECT M.gen_id,
					        SUBSTRING_INDEX(SUBSTRING_INDEX(M.sec_sub_div,  ",", Num.n),",",-1) AS sec_div_nm
					    FROM
					        numbers Num
					    INNER JOIN Contentmids_edit M 
						ON CHAR_LENGTH(M.sec_sub_div) - CHAR_LENGTH(REPLACE(M.sec_sub_div, ",", "")) >= Num.n -1 
						WHERE  `M`.`status_nm` = "Active"

					) H
					ON H.gen_id = A.gen_id
					LEFT JOIN(
						SELECT M.gen_id,
					        LPAD(Num.n, 3, "0") AS sec_div_no
					    FROM
					        numbers Num
					    INNER JOIN Contentmids_edit M ON
					        Num.n <= M.sec_div_num 
							WHERE `M`.`status_nm` = "Active"

					) I
					ON I.gen_id = A.gen_id ;';
					$this->db->query($sql_3);
					$sql_4='DROP TABLE IF EXISTS temp_mid_conv';
					$this->db->query($sql_4);
					$sql_5='CREATE TABLE temp_mid_conv 
							(
							  `t_id` int(11) NOT NULL AUTO_INCREMENT,
							  `dept_id_fk` int(11) DEFAULT NULL, 
							  `mid_code` varchar(100) DEFAULT NULL,
							  `level_id_fk` int(11) DEFAULT NULL, 
							  `hierarchy_id_fk` int(11) DEFAULT NULL,
							  `p_mid_code` int(11) DEFAULT NULL,
							  `p_hierarchy_id` int(11) DEFAULT NULL,
							  PRIMARY KEY(`t_id`)
							) ENGINE=InnoDB ;';
					$this->db->query($sql_5);
					$sql_6='INSERT INTO temp_mid_conv(dept_id_fk,mid_code,level_id_fk)	
								SELECT DISTINCT A.dept_id AS dept_id,									
								CONCAT_WS("",B.year_nm,A.syll_name,C.grade_name,D.subj_name,E.chapter_nm,F.sub_div_nm,G.div_no,H.sec_div_nm,I.sec_div_no) AS milestone_id,
									COALESCE(H.l_id,F.l_id,E.l_id,D.l_id,C.l_id,B.l_id,A.l_id) AS l_id 
								FROM
									(
									SELECT M.gen_id,M.activity_id,M.dept_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.syllabus, ",", Num.n),",",-1) AS syll_name,
										1 AS l_id 
									FROM
										numbers Num
									INNER JOIN Contentmids_edit M 
									ON CHAR_LENGTH(M.syllabus) - CHAR_LENGTH(REPLACE(M.syllabus, ",", "")) >= Num.n -1 
									WHERE `M`.`status_nm` = "Active" 	 
								) A
								LEFT JOIN(
								   SELECT M.activity_id,
										M.gen_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.year, ",", Num.n),",",-1) AS year_nm,
										(CASE WHEN Num.n=0 THEN null ELSE 2 END) AS l_id
									FROM
										numbers_0 Num
									INNER JOIN Contentmids_edit M 
									ON CHAR_LENGTH(M.year) - CHAR_LENGTH(REPLACE(M.year, ",", "")) >= Num.n -1 
									WHERE `M`.`status_nm` = "Active" 	
									) B
								ON B.gen_id = A.gen_id
								LEFT JOIN(
									SELECT M.gen_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.grade, ",", Num.n),",",-1) AS grade_name,
										(CASE WHEN Num.n=0 THEN null ELSE 3 END) AS l_id
									FROM
										numbers_0 Num
									INNER JOIN Contentmids_edit M 
									ON CHAR_LENGTH(M.grade) - CHAR_LENGTH(REPLACE(M.grade, ",", "")) >= Num.n -1 
									WHERE  `M`.`status_nm` = "Active" 	 
									) C
								ON C.gen_id = A.gen_id AND IFNULL(B.year_nm,"")<>""
								LEFT JOIN(
									SELECT M.gen_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.subject, ",", Num.n),",",-1) AS subj_name,
										(CASE WHEN Num.n=0 THEN null ELSE 4 END) AS l_id
									FROM
										numbers_0 Num
									INNER JOIN Contentmids_edit M 
									ON CHAR_LENGTH(M.subject) - CHAR_LENGTH(REPLACE(M.subject, ",", "")) >= Num.n -1 
									WHERE `M`.`status_nm` = "Active" 	
									) D
								ON D.gen_id = A.gen_id  AND IFNULL(C.grade_name,"")<>""
								LEFT JOIN(
									SELECT M.gen_id,
									(CASE WHEN Num.n=0 THEN "" WHEN M.chapter="XX" THEN  M.chapter ELSE LPAD(Num.n, 2, "0") END)  AS chapter_nm ,
										(CASE WHEN Num.n=0 THEN null ELSE 5 END) AS l_id
									FROM
										numbers_0 Num
									INNER JOIN Contentmids_edit M 
									ON (M.chapter="XX" AND Num.n=1) 
							    OR  ( M.chapter<>"XX" AND Num.n <= M.chapter  )
										WHERE `M`.`status_nm` = "Active" 	
								) E 
								ON E.gen_id = A.gen_id AND IFNULL(D.subj_name,"")<>""
								LEFT JOIN(
								SELECT M.gen_id,
										SUBSTRING_INDEX(SUBSTRING_INDEX(M.sub_div,  ",", Num.n),",",-1) AS sub_div_nm,
										(CASE WHEN Num.n=0 THEN null ELSE 6 END) AS l_id
									FROM
										numbers_0 Num
									INNER JOIN Contentmids_edit M 
									ON CHAR_LENGTH(M.sub_div) - CHAR_LENGTH(REPLACE(M.sub_div, ",", "")) >= Num.n -1 
									WHERE `M`.`status_nm` = "Active" 	
								) F
								ON F.gen_id = A.gen_id AND IFNULL(E.chapter_nm,"")<>""
							LEFT JOIN(
								SELECT M.gen_id,
							        LPAD(Num.n, 3, "0") AS div_no,
									null AS l_id
							    FROM
							        numbers Num
							    INNER JOIN Contentmids_edit M ON
							        Num.n <= M.div_num 
									WHERE  `M`.`status_nm` = "Active"   
							) G
							ON G.gen_id = A.gen_id AND IFNULL(F.sub_div_nm,"")<>"" AND IFNULL(G.div_no,0)<>0
							LEFT JOIN(
							SELECT M.gen_id,
							        SUBSTRING_INDEX(SUBSTRING_INDEX(M.sec_sub_div,  ",", Num.n),",",-1) AS sec_div_nm,
									(CASE WHEN Num.n=0 THEN null ELSE 7 END) AS l_id
							    FROM
							        numbers_0 Num
							    INNER JOIN Contentmids_edit M 
								ON CHAR_LENGTH(M.sec_sub_div) - CHAR_LENGTH(REPLACE(M.sec_sub_div, ",", "")) >= Num.n -1 
								WHERE  `M`.`status_nm` = "Active"  
							) H
							ON H.gen_id = A.gen_id AND IFNULL(F.sub_div_nm,"")<>"" AND IFNULL(G.div_no,0)<>0
							LEFT JOIN(
								SELECT M.gen_id,
							        LPAD(Num.n, 3, "0") AS sec_div_no,
									null AS l_id
							    FROM
							        numbers Num
							    INNER JOIN Contentmids_edit M ON
							        Num.n <= M.sec_div_num 
									WHERE `M`.`status_nm` = "Active"   
							) I
							ON I.gen_id = A.gen_id  AND IFNULL(H.sec_div_nm,"")<>"" AND IFNULL(I.sec_div_no,0)<>0
							 ORDER BY milestone_id ;';
						$this->db->query($sql_6);

						$sql_7='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`status_nm`, `ins_user`, `ins_dt`)
								SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,"Active",'.$user_id.',current_timestamp 
								FROM `temp_mid_conv` P 
								LEFT JOIN hierarchy_mst H 
								ON P.`dept_id_fk`=H.dept_id_fk
								AND P.`level_id_fk`=H.level_id_fk
								AND P.mid_code=H.mid_code AND H.status_nm="Active" 
								WHERE H.hierarchy_id IS NULL AND P.level_id_fk=1;';
						$this->db->query($sql_7);

						$sql_8="UPDATE temp_mid_conv A INNER JOIN hierarchy_mst P
								ON P.dept_id_fk=A.dept_id_fk 
								AND A.mid_code = P.mid_code 
								AND A.level_id_fk = P.level_id_fk 
								AND P.status_nm='Active' 
								AND A.level_id_fk=1
							SET A.hierarchy_id_fk = P.hierarchy_id;";
						$this->db->query($sql_8);
						$sql_9="UPDATE temp_mid_conv A INNER JOIN	temp_mid_conv	P 
								ON P.dept_id_fk=A.dept_id_fk  
								AND A.level_id_fk=2 AND A.level_id_fk = (P.level_id_fk+1)
								AND  SUBSTRING(A.mid_code,3,4) = P.mid_code
								SET A.p_hierarchy_id = P.hierarchy_id_fk
								;";
						$this->db->query($sql_9);

						//-- Year
						$sql_10='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
								SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
								FROM `temp_mid_conv` P 
								LEFT JOIN hierarchy_mst H 
								ON P.`dept_id_fk`=H.dept_id_fk
								AND P.`level_id_fk`=H.level_id_fk
								AND P.mid_code=H.mid_code 
								AND H.status_nm="Active" 
								AND P.p_hierarchy_id=H.p_hierarchy_id
								WHERE H.hierarchy_id IS NULL AND P.level_id_fk=2 AND P.p_hierarchy_id iS NOT NULL';
						$this->db->query($sql_10);
						$sql_11="UPDATE temp_mid_conv A INNER JOIN hierarchy_mst P
							ON P.dept_id_fk=A.dept_id_fk 
							AND A.mid_code = P.mid_code 
							AND P.status_nm='Active' 
							AND  A.level_id_fk = P.level_id_fk AND A.level_id_fk=2
							SET A.hierarchy_id_fk = P.hierarchy_id;";
						$this->db->query($sql_11);
						$sql_12="UPDATE temp_mid_conv A INNER JOIN temp_mid_conv	P 
								ON P.dept_id_fk=A.dept_id_fk AND A.level_id_fk = (P.level_id_fk+1)
								AND A.level_id_fk=3 
								AND SUBSTRING(A.mid_code,1,6) = P.mid_code
								SET A.p_hierarchy_id = P.hierarchy_id_fk;";
						$this->db->query($sql_12);
						//-- Grade
						$sql_13='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
						SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
						FROM `temp_mid_conv` P 
						LEFT JOIN hierarchy_mst H 
						ON P.`dept_id_fk`=H.dept_id_fk
						AND P.`level_id_fk`=H.level_id_fk
						AND P.mid_code=H.mid_code 
						AND H.status_nm="Active" 
						AND P.p_hierarchy_id=H.p_hierarchy_id
						WHERE H.hierarchy_id IS NULL AND P.level_id_fk=3  AND P.p_hierarchy_id iS NOT NULL';
						$this->db->query($sql_13);
						$sql_14="UPDATE temp_mid_conv A INNER JOIN hierarchy_mst P
								ON P.dept_id_fk=A.dept_id_fk 
								AND A.mid_code = P.mid_code 
								AND A.level_id_fk = P.level_id_fk 
								AND P.status_nm='Active'
								AND A.level_id_fk=3
								SET A.hierarchy_id_fk = P.hierarchy_id";
						$this->db->query($sql_14);
						$sql_15="UPDATE temp_mid_conv A INNER JOIN temp_mid_conv	P 
								ON P.dept_id_fk=A.dept_id_fk AND A.level_id_fk = (P.level_id_fk+1)
								AND A.level_id_fk=4 AND SUBSTRING(A.mid_code,1,8) = P.mid_code
								SET A.p_hierarchy_id = P.hierarchy_id_fk;";
						$this->db->query($sql_15);

						// -- Subject
						$sql_16='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
								SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
								FROM `temp_mid_conv` P 
								LEFT JOIN hierarchy_mst H 
								ON P.`dept_id_fk`=H.dept_id_fk
								AND P.`level_id_fk`=H.level_id_fk
								AND P.mid_code=H.mid_code 
								AND H.status_nm="Active" 
								AND P.p_hierarchy_id=H.p_hierarchy_id
								WHERE H.hierarchy_id IS NULL AND P.level_id_fk=4  AND P.p_hierarchy_id iS NOT NULL;';
						$this->db->query($sql_16);

						$sql_17="UPDATE temp_mid_conv A INNER JOIN hierarchy_mst P 
								ON P.dept_id_fk=A.dept_id_fk 
														AND A.level_id_fk=4 
														AND A.mid_code = P.mid_code 
														AND A.level_id_fk = P.level_id_fk 
														AND P.status_nm='Active' 							
													SET A.hierarchy_id_fk = P.hierarchy_id";
						$this->db->query($sql_17);							
						$sql_18	="UPDATE temp_mid_conv A INNER JOIN temp_mid_conv	P 
										ON P.dept_id_fk=A.dept_id_fk AND A.level_id_fk=5 
										AND A.level_id_fk = (P.level_id_fk+1) 
										AND SUBSTRING(A.mid_code,1,11) = P.mid_code
										SET A.p_hierarchy_id = P.hierarchy_id_fk;";
						$this->db->query($sql_18);
						//-- Chapter
						$sql_19='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
							SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
							FROM `temp_mid_conv` P 
							LEFT JOIN hierarchy_mst H 
							ON P.`dept_id_fk`=H.dept_id_fk
							AND P.`level_id_fk`=H.level_id_fk
							AND P.mid_code=H.mid_code 
							AND H.status_nm="Active" 
							AND P.p_hierarchy_id=H.p_hierarchy_id
							WHERE H.hierarchy_id IS NULL AND P.level_id_fk=5 AND P.p_hierarchy_id iS NOT NULL;';
							$this->db->query($sql_19);

						$sql_20="UPDATE temp_mid_conv A INNER JOIN hierarchy_mst P
							ON P.dept_id_fk=A.dept_id_fk 
							AND A.level_id_fk=5
							AND A.mid_code = P.mid_code 
							AND A.level_id_fk = P.level_id_fk 
							AND P.status_nm='Active' 
							SET A.hierarchy_id_fk = P.hierarchy_id;";
						$this->db->query($sql_20);
						$sql_21='UPDATE temp_mid_conv A INNER JOIN temp_mid_conv	P 
								ON P.dept_id_fk=A.dept_id_fk 
								AND A.level_id_fk=6 AND A.level_id_fk = (P.level_id_fk+1) 
								AND SUBSTRING(A.mid_code,1,13) = P.mid_code
								SET A.p_hierarchy_id = P.hierarchy_id_fk;';
						$this->db->query($sql_21);
						//-- Sub Topic
					$sql_22='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
							SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
							FROM `temp_mid_conv` P 
							LEFT JOIN hierarchy_mst H 
							ON P.`dept_id_fk`=H.dept_id_fk
							AND P.`level_id_fk`=H.level_id_fk
							AND P.mid_code=H.mid_code 
							AND H.status_nm="Active" 
							AND P.p_hierarchy_id=H.p_hierarchy_id
							WHERE H.hierarchy_id IS NULL AND P.level_id_fk=6 AND P.p_hierarchy_id iS NOT NULL;';
					$this->db->query($sql_22);
					$sql_23="UPDATE temp_mid_conv A INNER JOIN hierarchy_mst P
								ON P.dept_id_fk=A.dept_id_fk 
								AND A.mid_code = P.mid_code 
								AND A.level_id_fk = P.level_id_fk 
								AND P.status_nm='Active'
								AND A.level_id_fk=6
							SET A.hierarchy_id_fk = P.hierarchy_id;";
					$this->db->query($sql_23);
					$sql_24='UPDATE temp_mid_conv A INNER JOIN temp_mid_conv	P 
							ON P.dept_id_fk=A.dept_id_fk AND A.level_id_fk = (P.level_id_fk+1)
							AND A.level_id_fk=7 AND SUBSTRING(A.mid_code,1,18) = P.mid_code
							SET A.p_hierarchy_id = P.hierarchy_id_fk;';
					$this->db->query($sql_24);
					$sql_25='UPDATE temp_mid_conv A INNER JOIN hierarchy_mst	P 
						ON P.dept_id_fk=A.dept_id_fk AND A.level_id_fk=7 
						AND A.level_id_fk = (P.level_id_fk+1)
						AND  SUBSTRING(A.mid_code,1,18) = P.mid_code 
			            -- AND A.t_id <=32000 AND A.t_id>26000
						SET A.p_hierarchy_id = P.hierarchy_id;';
					$this->db->query($sql_25);
					//-- Video Clip
					$sql_26='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
							SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
							FROM `temp_mid_conv` P 
							LEFT JOIN hierarchy_mst H 
							ON P.`dept_id_fk`=H.dept_id_fk
							AND P.`level_id_fk`=H.level_id_fk
							AND P.mid_code=H.mid_code 
							AND H.status_nm="Active" 
							AND P.p_hierarchy_id=H.p_hierarchy_id
							WHERE H.hierarchy_id IS NULL AND P.level_id_fk=7 AND P.p_hierarchy_id iS NOT NULL';
					$this->db->query($sql_26);
					$sql_27="UPDATE temp_mid_conv A INNER JOIN hierarchy_mst P
							ON P.dept_id_fk=A.dept_id_fk 
							AND A.mid_code = P.mid_code 
							AND A.level_id_fk = P.level_id_fk 
							AND P.status_nm='Active'
							AND A.level_id_fk=7
						SET A.hierarchy_id_fk = P.hierarchy_id;";
					$this->db->query($sql_27);
					$sql_28='UPDATE temp_mid_conv A INNER JOIN temp_mid_conv	P 
							ON P.dept_id_fk=A.dept_id_fk AND A.level_id_fk = (P.level_id_fk+1)
							AND A.level_id_fk=8 AND SUBSTRING(A.mid_code,1,23) = P.mid_code
							SET A.p_hierarchy_id = P.hierarchy_id_fk;';
					$this->db->query($sql_28);
					//-- Third Sub
					$sql_29='INSERT INTO hierarchy_mst(`dept_id_fk`,`mid_code`, `level_id_fk`,`p_hierarchy_id`,`status_nm`, `ins_user`, `ins_dt`)
							SELECT P.`dept_id_fk`,P.`mid_code`, P.`level_id_fk`,P.p_hierarchy_id,"Active",'.$user_id.',current_timestamp 
							FROM `temp_mid_conv` P 
							LEFT JOIN hierarchy_mst H 
							ON P.`dept_id_fk`=H.dept_id_fk
							AND P.`level_id_fk`=H.level_id_fk
							AND P.mid_code=H.mid_code 
							AND H.status_nm="Active" 
							AND P.p_hierarchy_id=H.p_hierarchy_id
							WHERE H.hierarchy_id IS NULL AND P.level_id_fk=8 AND P.p_hierarchy_id IS NOT NULL;';
					$this->db->query($sql_29);
					$sql_30="UPDATE temp_mid_conv A INNER JOIN hierarchy_mst P
							ON P.dept_id_fk=A.dept_id_fk 
							AND A.mid_code = P.mid_code 
							AND A.level_id_fk = P.level_id_fk 
							AND P.status_nm='Active'
							AND A.level_id_fk=8
						SET A.hierarchy_id_fk = P.hierarchy_id";
					$this->db->query($sql_30);
					//-- ACtivity						
					$sql_31='UPDATE temp_mid_conv_act P JOIN activity_mst A 
							ON A.activity_id=P.activity_id_fk 
							AND A.status_nm="Active" 
							SET P.project_id_fk=A.project_id_fk,
							P.task_id_fk=A.task_id_fk,
							P.sub_id_fk=A.sub_id_fk,
							P.job_id_fk=A.job_id_fk;';
						$this->db->query($sql_31);

					$sql_32='UPDATE temp_mid_conv_act P JOIN hierarchy_mst A 	
							ON A.mid_code=P.mid_code AND P.hierarchy_id_fk IS NULL 
							AND A.status_nm="Active" 
							SET P.hierarchy_id_fk=A.hierarchy_id;'; 
					$this->db->query($sql_32);

					$sql_33='UPDATE temp_mid_conv_act P JOIN hier_act_log A 
							ON P.hierarchy_id_fk=A.p_hierarchy_id 
							AND P.project_id_fk=A.project_id_fk AND
							P.task_id_fk=A.task_id_fk AND 
							P.sub_id_fk=A.sub_task_id_fk AND 	
							P.job_id_fk=A.job_id_fk AND A.status_nm="Active" 
							SET  P.hier_act_log=A.hier_act_log_id';
					$this->db->query($sql_33);

					$sql_34="INSERT INTO `hier_act_log`( `p_hierarchy_id`, `dept_id_fk`, `project_id_fk`, `task_id_fk`, `sub_task_id_fk`, `job_id_fk`, `status_nm`, `ins_user`, `ins_dt`)
							SELECT hierarchy_id_fk,dept_id_fk,project_id_fk,task_id_fk,
							sub_id_fk,	job_id_fk,'Active' ,".$user_id.", current_timestamp 
							FROM temp_mid_conv_act 
							WHERE hierarchy_id_fk IS NOT NULL AND hier_act_log IS NULL;";
					$this->db->query($sql_34);
				
			$sql_1="DROP TABLE IF EXISTS temp_mid_conv_act";
				$this->db->query($sql_1);	
				$sql_4='DROP TABLE IF EXISTS temp_mid_conv';
					$this->db->query($sql_4);
				 $sql8='DROP TABLE Contentmids_edit;';
				$this->db->query($sql8);
					$this->db->trans_complete();
					if($this->db->trans_status() === FALSE)
					  {
						 return false;
					  }
					  else{
					  		if($affected_rows>0){
					  			return true;
					  		}else{
					  			echo "activity not exits";
					  		}
						
					  }
				}
		}
		//update media  update
		public function mediamidupdate($ans){
						$data = array(
							'dept_id_fk'=>$ans[1],
				       		'milestone_id'=>$ans[2],
						   	'project_id_fk'=>$ans[3],
							'upd_user'=>$ans[4],
							'status_nm'=>$ans[5],
							'upd_dt'=> date('Y-m-d H:i:s')
			    );
						//print_r($data);die;
					 $this->db->where('mile_act_id',$ans[0]);
    				$suc=$this->db->update('mile_act_mst',$data );
    				if($suc==true){
						return true;
					}
		}

		//insert media mids
		public function insert_mid_midia($ans){
			$data = array(
							'milestone_id'=>$ans[0],
							'project_id_fk'=>$ans[1],
							'dept_id_fk'=>$ans[2],
							'ins_user'=>$ans[3],
							'status_nm'=>$ans[4],
							'ins_dt'=> date('Y-m-d H:i:s')
						);	
			$suc=$this->db->insert('mile_act_mst',$data);
			if($suc==true){
				return true;
			}
		}
		
		public function fetchSingleLogisticData($id){
		return  $this->db->select(
				'ul.*,
				DATE_FORMAT(CONVERT_TZ(ul.start_date,"+00:00",'.$this->tx_cn.'), "%d-%m-%Y") AS start_date,
				DATE_FORMAT(CONVERT_TZ(ul.end_date,"+00:00",'.$this->tx_cn.'), "%d-%m-%Y") AS end_date,
				um.user_login_name')->from('user_logistic ul')
					->join('user_mst um','um.user_id  = ul.user_id_fk')
					->where('ul.logistics_id',$id)->get()->row();
	}
	 public function select_tbl($file_nm,$dept)
	{
		if($file_nm=='mid_gen')
		{				if($dept==1){
								$this->db->select('mg.`gen_id`, mg.`activity_id_fk`, mg.`dept_id_fk`, mg.`year`, mg.`syllabus`,
								mg.`grade`, mg.`subject`, mg.`chapter`, mg.`sub_div`, mg.`div_num`, 
								mg.`sec_sub_div`, mg.`sec_div_num`, mg.`third_sub_div`, mg.`third_div_num`,
								mg.`status_nm`, 
								mg.`ins_user`,
								UNIX_TIMESTAMP(mg.ins_dt)  AS ins_dt,								
								mg.`upd_user`,
								UNIX_TIMESTAMP(mg.upd_dt)  AS upd_dt,
								am.activity_name,um.full_name as inst_user,umt.full_name as updt_user');
								$this->db->from('mid_gen mg');
								$this->db->join('activity_mst am', 'mg.activity_id_fk = am.activity_id AND IFNULL(am.is_closed,"N")="N"','inner');
								$this->db->join('user_mst um', 'mg.ins_user = um.user_id','left');
								$this->db->join('user_mst umt', 'mg.upd_user = umt.user_id','left');
								$this->db->where('mg.status_nm', 'Active');
						}else{
								$this->db->select('mg.`mile_act_id`, mg.`milestone_id`, mg.`activity_id_fk`, mg.`dept_id_fk`, 
								mg.`status_nm`, mg.`ins_user`, 
								UNIX_TIMESTAMP(mg.ins_dt)  AS ins_dt, 
								mg.`upd_user`, 
								UNIX_TIMESTAMP(mg.upd_dt)  AS upd_dt,
								mg.`project_id_fk`,dm.dept_name,pm.project_name');
								$this->db->from('mile_act_mst mg');
								$this->db->join(' dept_mst dm', 'mg.dept_id_fk = dm.dept_id','left');
								$this->db->join(' project_mst pm', 'mg.project_id_fk = pm.project_id','left');
								$this->db->where('mg.status_nm', 'Active');
							
						}			
		}
		if($file_nm=='std_tgt_mst')
		{
			$this->db->select('  1 AS id, R.std_tgt_id AS tab_id,
						A.activity_name AS activity_name,W.wu_name,
						R.status_nm,R.tgt_val,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(R.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(R.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('std_tgt_mst R');
						$this->db->join('user_mst I', 'I.user_id = R.ins_user','left');
						$this->db->join('activity_mst A', 'R.activity_id_fk = A.activity_id','left');
						$this->db->join('work_unit_mst W', 'W.wu_id = A.wu_id_fk','left');
						$this->db->join('user_mst UP', 'UP.user_id = R.upd_user','left');
						$this->db->where('A.dept_id_fk', $dept);
					 $this->db->where(' R.end_dt IS NULL ',null,FALSE);			
		}
		if($file_nm=='activity_mst')
		{
			$this->db->select('  1 AS id, R.activity_id AS tab_id,@row_number:=@row_number+1 as activity_id,
						R.activity_name AS activity_name,P.project_name,T.task_name,
						S.sub_name,
						J.job_name,W.wu_name,
						IFNULL(R.is_closed,"N") AS is_closed,
						R.status_nm,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(R.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(R.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('activity_mst R');
						$this->db->join('(SELECT @row_number:=0)RO', '1=1','left');
						$this->db->join('user_mst I', 'I.user_id = R.ins_user','left');
						$this->db->join('project_mst P', 'P.project_id = R.project_id_fk','left');
						$this->db->join('task_mst T', 'T.task_id = R.task_id_fk','left');
						$this->db->join('sub_mst S', 'S.sub_id = R.sub_id_fk','left');
						$this->db->join('job_mst J', 'J.job_id = R.job_id_fk','left');
						$this->db->join('work_unit_mst W', 'W.wu_id = R.wu_id_fk','left');
						//$this->db->join('std_tgt_mst ST', 'ST.activity_id_fk = R.activity_id AND ST.end_dt IS NULL','left');
						$this->db->join('user_mst UP', 'UP.user_id = R.upd_user','left');
						$this->db->where('R.dept_id_fk', $dept);
						// $this->db->where('R.status_nm', 'Active');			
		}
		if($file_nm=='work_unit_mst')
		{
			$this->db->select('  1 AS id, R.wu_id AS tab_id,R.wu_id AS work_unit_id,
						R.wu_name AS work_unit_name,R.is_quantify,
						R.status_nm,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(R.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(R.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('work_unit_mst R');
						$this->db->join('user_mst I', 'I.user_id = R.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = R.upd_user','left');
						$this->db->where('R.dept_id_fk', $dept);
						// $this->db->where('R.status_nm', 'Active');			
		}
		if($file_nm=='job_mst')
		{
			$this->db->select('  1 AS id, R.job_id AS tab_id,R.job_id,
						R.job_name,
						R.status_nm,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(R.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(R.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('job_mst R');
						$this->db->join('user_mst I', 'I.user_id = R.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = R.upd_user','left');
						// $this->db->where('R.status_nm', 'Active');
						$this->db->where('R.dept_id_fk', $dept);						
		}
		
		if($file_nm=='dept_mst')
		{
		$this->db->select('   1 AS id,U.dept_id AS tab_id,U.dept_id,
						U.dept_name,
						U.status_nm,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(U.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(U.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('dept_mst U');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
		}
		if($file_nm=='sub_mst')
		{
			$this->db->select('  1 AS id, R.sub_id AS tab_id,R.sub_id,
						R.sub_name,
						R.status_nm,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(R.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(R.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('sub_mst R');
						$this->db->join('user_mst I', 'I.user_id = R.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = R.upd_user','left');
						// $this->db->where('R.status_nm', 'Active');	
						$this->db->where('R.dept_id_fk', $dept);
		}
		if($file_nm=='task_mst')
		{
			$this->db->select('  1 AS id, R.task_id AS tab_id,R.task_id,
						R.task_name,
						R.status_nm,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(R.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(R.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('task_mst R');
						$this->db->join('user_mst I', 'I.user_id = R.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = R.upd_user','left');
						// $this->db->where('R.status_nm', 'Active');	
						$this->db->where('R.dept_id_fk', $dept);
		}
		if($file_nm=='project_mst')
		{
			$this->db->select('  1 AS id, R.project_id AS tab_id,R.project_id,
						R.project_name,
						R.status_nm,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(R.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(R.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('project_mst R');
						$this->db->join('user_mst I', 'I.user_id = R.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = R.upd_user','left');
						 //$this->db->where('R.status_nm', 'Active');
						 $this->db->where('R.dept_id_fk', $dept);				
		}
		
		if($file_nm=='access_mst'){
			$this->db->select('  1 AS id, R.access_id AS tab_id,R.access_id,
						R.access_name,R.icon,
						R.status_nm,
						R.tab_name,
						R.tab_color,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(R.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(R.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('access_mst R');
						$this->db->join('user_mst I', 'I.user_id = R.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = R.upd_user','left');
						// $this->db->where('R.status_nm', 'Active');
				
		}
		if($file_nm=='role_mst')
		{
			$this->db->select('  1 AS id, R.role_id AS tab_id,R.role_id,
						R.role_name,R.access_name,
						R.status_nm,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(R.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(R.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('role_mst R');
						$this->db->join('user_mst I', 'I.user_id = R.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = R.upd_user','left');
						 //$this->db->where('R.status_nm', 'Active');
				
		}
		if($file_nm=='user_mst')
		{
			$status = $this->input->get('status');
		$this->db->select('  1 AS id, U.user_id AS tab_id,U.user_id,
						U.user_login_name,
						U.emp_id,
						U.full_name,
						R.role_name,
						GROUP_CONCAT(CASE WHEN U.project_id_fk IS NULL THEN "-" WHEN U.project_id_fk=0 THEN "All Projects" ELSE (P.project_name) END) AS project_id_fk,
						U.week_off,
						U.streak,
						CASE WHEN (U.access_name like "%Review%") THEN "Y" ELSE "N" END AS access_name,
						M.full_name AS manager,
						U.status_nm,
						IFNULL(U.is_grading,"N") AS is_grad,
						I.full_name AS ins_user,
						UNIX_TIMESTAMP(U.ins_dt)  AS ins_dt,
						UP.full_name AS upd_user,		
						UNIX_TIMESTAMP(U.upd_dt)  AS upd_dt ',FALSE);
						$this->db->from('user_mst U');
						$this->db->join('role_mst R', 'R.role_id=U.role_id_fk AND R.status_nm="Active" ','left');
						$this->db->join('user_mst I', 'I.user_id = U.ins_user','left');
						$this->db->join('user_mst UP', 'UP.user_id = U.upd_user','left');
						$this->db->join('project_mst P', ' find_in_set(P.project_id,U.project_id_fk)<>0 AND P.status_nm="Active" ','left');
						$this->db->join('user_mst M', 'M.user_id = U.manager_id','left');
						$this->db->where(' (IFNULL(U.dept_id_fk,0)=0 OR  find_in_set('.$dept.',U.dept_id_fk)<>0 ) ', null, false);
						if($status){
							$this->db->where(['U.status_nm' => $status]);
						}
						$this->db->group_by('U.user_id');
		}
		if($file_nm=='user_logistic'){
			$current_status=$this->input->get('cur_status');
			$this->db->select('ul.*,
				um.user_login_name as email_id,
				I.full_name AS ins_user,
				UP.full_name AS upd_user,
				um.emp_id');
			// $where_qry=['ul.status_nm'=>'Active'];
			
			if($current_status){
				$where_qry['ul.is_current']=$current_status;
			}
			
			// $this->db->where($where_qry);
			$this->db->from('user_logistic ul');
			$this->db->join('user_mst um', 'um.user_id = ul.user_id_fk');
			$this->db->join('user_mst I', 'I.user_id = um.ins_user','left');
			$this->db->join('user_mst UP', 'UP.user_id = um.upd_user','left');
			$this->db->order_by('ul.logistics_id', 'DESC');
		}

				$sqlQuery = $this->db->get();
				$resultSet = $sqlQuery->result_array();
	  if($resultSet)
	  {
		  return json_encode($resultSet);
	  }
	  else{
		return json_encode (json_decode ("{}"));;
	  }
	 }	 
	 
	 public function  save_work_request($ary){
	 	$this->db->insert_batch('work_request',$ary);
	 	if($this->db->affected_rows() > 0)
		{
		   return true; 
		}
	 }
	 //save work request

	 public function insOdRequest($data){
	 	$this->db->insert_batch('work_request_od',$data);
	 	if($this->db->affected_rows() > 0)
		{
		   return true; 
		}
	 }
	 
	 public function  get_work_request_stat($pids,$userid=null,$stat=null,$dept){		
			
				$wh='';
				$wh2='';
		  if($pids!='0' && ($pids))
				 {
					 $wh=' AND WR.project_id_fk IN ('.$pids.')';							
					  $wh2=' AND ur.project_id_fk IN ('.$pids.')';							
				 }
			
			$add='';
			$add2='';
			if($stat){
				$add=' AND `WR`.`conf_status` = "'.$stat.'"';
				$add2=' AND `ur`.`conf_status` = "'.$stat.'"';
			}
				$sql='SELECT `ur`.*, `P`.`project_name`, `um`.`full_name`, `umst`.`full_name` as `requester_Name`, 
			`wrct`.`com_type` as `combos_type`,ROUND(RT.equiv_xps,2) AS equiv_xps ,
					ROUND(RT.conf_xps,2) AS act_xp,
					DATE_FORMAT(CONVERT_TZ(ur.conf_dt, "+00:00", '.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS conf_dts, 
					DATE_FORMAT(CONVERT_TZ(ur.approve_dt, "+00:00", '.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS approve_dts, 
					`CO`.`full_name` AS `conf_users`, `AP`.`full_name` AS `approve_users`,
					IOR.in_time, IOR.esp_wrk_hrs ,IOR.break_hrs,IOR.sapience_on 
				FROM work_request `ur` 						
				INNER JOIN `user_mst` `um` ON `ur`.`emp_user_id` = `um`.`user_id`
				INNER JOIN `user_mst` `umst` ON `ur`.`req_user_id` = `umst`.`user_id`
				INNER JOIN `project_mst` `P` ON `P`.`project_id` = `ur`.`project_id_fk`
				INNER JOIN `work_request_comb_type` `wrct` ON `ur`.`combo_type`= `wrct`.`id`
				LEFT JOIN `user_mst` `CO` ON `ur`.`conf_user` = `CO`.`user_id`
				LEFT JOIN `user_mst` `AP` ON `ur`.`approve_user` = `AP`.`user_id` 
				LEFT JOIN (SELECT  R.`user_id_fk`, R.day_val,
							SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xps ,
							SUM(CASE WHEN R.tast_stat="Confirmed" THEN IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)
							ELSE 0 END) AS conf_xps
							FROM `work_request` WR 
                            INNER JOIN response_mst R 
							ON WR.emp_user_id=R.user_id_fk 
							AND R.day_val=DATE(WR.called_dt)                         
							WHERE R.status_nm="Active" 
 							AND WR.`dept_id_fk` = "'.$dept.'" '.$add.' '.$wh.' 
							GROUP BY  R.`user_id_fk`, R.day_val) RT 
				ON RT.user_id_fk=ur.emp_user_id AND RT.day_val=DATE(ur.called_dt)
				LEFT JOIN in_out_upload IOR 
				ON IOR.user_id=ur.emp_user_id AND IOR.date_val=DATE(ur.called_dt) 
				WHERE ur.`dept_id_fk` = "'.$dept.'" '.$add2.' '.$wh2.' 
				ORDER BY `ur`.`work_request_id` DESC'; 
				$sqlQuery = $this->db->query($sql);	 		
			$resultSet = $sqlQuery->result_array();
		  if($resultSet)
		  {
			  return json_encode($resultSet);
		  }
		  else{
			return json_encode (json_decode ("{}"));;
		  }
				
			
	 }
	 public function  get_work_request($pids,$userid=null,$stat=null,$dept){		 
			if($stat=='Confirmed' || !($stat))
			{
				$wh='';
				$wh2='';
		  if($pids!='0' && ($pids))
				 {
					 $wh=' AND WR.project_id_fk IN ('.$pids.')';							
					  $wh2=' AND ur.project_id_fk IN ('.$pids.')';							
				 }
			
			$add='';
			$add2='';
			if($stat){
				$add=' AND `WR`.`conf_status` = "'.$stat.'"';
				$add2=' AND `ur`.`conf_status` = "'.$stat.'"';
			}
				$sql='SELECT `ur`.*, `P`.`project_name`, `um`.`full_name`, `umst`.`full_name` as `requester_Name`, 
			`wrct`.`com_type` as `combos_type`,ROUND(RT.equiv_xps,2) AS equiv_xps ,
					ROUND(RT.conf_xps,2) AS act_xp,
					DATE_FORMAT(CONVERT_TZ(ur.conf_dt, "+00:00", '.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS conf_dts, 
					DATE_FORMAT(CONVERT_TZ(ur.approve_dt, "+00:00", '.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS approve_dts, 
					`CO`.`full_name` AS `conf_users`, `AP`.`full_name` AS `approve_users`,
					IOR.in_time, IOR.esp_wrk_hrs ,IOR.break_hrs,IOR.sapience_on 
				FROM work_request `ur` 						
				INNER JOIN `user_mst` `um` ON `ur`.`emp_user_id` = `um`.`user_id`
				INNER JOIN `user_mst` `umst` ON `ur`.`req_user_id` = `umst`.`user_id`
				INNER JOIN `project_mst` `P` ON `P`.`project_id` = `ur`.`project_id_fk`
				INNER JOIN `work_request_comb_type` `wrct` ON `ur`.`combo_type`= `wrct`.`id`
				LEFT JOIN `user_mst` `CO` ON `ur`.`conf_user` = `CO`.`user_id`
				LEFT JOIN `user_mst` `AP` ON `ur`.`approve_user` = `AP`.`user_id` 
				LEFT JOIN (SELECT  R.`user_id_fk`, R.day_val,
							SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xps ,
							SUM(CASE WHEN R.tast_stat="Confirmed" THEN IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)
							ELSE 0 END) AS conf_xps
							FROM `work_request` WR  
                            INNER JOIN response_mst R 
							ON WR.emp_user_id=R.user_id_fk 
							AND R.day_val=DATE(WR.called_dt)                         
							WHERE R.status_nm="Active" 
 							AND WR.`dept_id_fk` = "'.$dept.'" '.$add.' '.$wh.' 
							GROUP BY  R.`user_id_fk`, R.day_val) RT 
				ON RT.user_id_fk=ur.emp_user_id AND RT.day_val=DATE(ur.called_dt)
				LEFT JOIN in_out_upload IOR 
				ON IOR.user_id=ur.emp_user_id AND IOR.date_val=DATE(ur.called_dt) 
				WHERE ur.`dept_id_fk` = "'.$dept.'" '.$add2.' '.$wh2.' 
				ORDER BY `ur`.`work_request_id` DESC'; 
				
			}else{	
				$wh='';
		  if($pids!='0' && ($pids))
				 {
					 $wh=' AND ur.project_id_fk IN ('.$pids.')';							
				 }
			$add='';
			if($stat=='Confirmed' || $stat=='Pending'){
				$add='AND `ur`.`conf_status` = "'.$stat.'"';
			}
			$sql='SELECT `ur`.*, `P`.`project_name`, `um`.`full_name`, `umst`.`full_name` as `requester_Name`, 
			`wrct`.`com_type` as `combos_type`, 
					DATE_FORMAT(CONVERT_TZ(ur.conf_dt, "+00:00", '.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS conf_dts, 
					DATE_FORMAT(CONVERT_TZ(ur.approve_dt, "+00:00", '.$this->tx_cn.'), "%d/%m/%y %k:%i:%s")  AS approve_dts, 
					`CO`.`full_name` AS `conf_users`, `AP`.`full_name` AS `approve_users`
				FROM  `work_request` `ur` 						
				INNER JOIN `user_mst` `um` ON `ur`.`emp_user_id` = `um`.`user_id`
				INNER JOIN `user_mst` `umst` ON `ur`.`req_user_id` = `umst`.`user_id`
				INNER JOIN `project_mst` `P` ON `P`.`project_id` = `ur`.`project_id_fk`
				INNER JOIN `work_request_comb_type` `wrct` ON `ur`.`combo_type`= `wrct`.`id`
				LEFT JOIN `user_mst` `CO` ON `ur`.`conf_user` = `CO`.`user_id`
				LEFT JOIN `user_mst` `AP` ON `ur`.`approve_user` = `AP`.`user_id` 
				WHERE `ur`.`dept_id_fk` = "'.$dept.'" '.$add.' '.$wh.'				
				ORDER BY `ur`.`work_request_id` DESC'; 
			}
	 		$sqlQuery = $this->db->query($sql);	 		
			$resultSet = $sqlQuery->result_array();
		  if($resultSet)
		  {
			  return json_encode($resultSet);
		  }
		  else{
			return json_encode (json_decode ("{}"));;
		  }
    }

    public function get_od_request($pids,$userid=null,$stat=null,$dept,$final_approve = false){	
    	$where = "";
    	$whereExtra = "";
    	if(!empty($stat)){
    		$where.= "AND WO.conf_status='$stat'";
    		$whereExtra.= "AND WOD.conf_status='$stat'";
		}
		if($pids){
			$where.= "AND WO.project_id_fk IN ('$pids')";
			$whereExtra.= "AND WOD.project_id_fk IN ('$pids')";
		}
		$join = "";
		$exSelect = ""; 
		if($final_approve){
			$exSelect = ",ROUND(RT.equiv_xps,2) AS equiv_xps,
					ROUND(RT.conf_xps,2) AS act_xp";

			$join = "LEFT JOIN (SELECT  R.`user_id_fk`, R.day_val,
							SUM(IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)) AS equiv_xps ,
							SUM(CASE WHEN R.tast_stat='Confirmed' THEN IFNULL(R.final_xp,IFNULL(R.equiv_xp, 0)) + IFNULL(R.comp_xp, 0)
							ELSE 0 END) AS conf_xps
							FROM `work_request_od` WOD  
                            INNER JOIN response_mst R 
							ON WOD.emp_user_id=R.user_id_fk 
							AND R.day_val=DATE(WOD.emp_request_date)                         
							WHERE R.status_nm='Active'
 							AND WOD.`dept_id_fk` = $dept $whereExtra
							GROUP BY  R.`user_id_fk`, R.day_val) RT 
				ON RT.user_id_fk=WO.emp_user_id AND RT.day_val=DATE(WO.emp_request_date)";
		}
    	$sql = "SELECT
				    RE.full_name AS user_name,
				    RQ.full_name AS requester_name,
				    WO.emp_request_date,
				    P.project_name AS project,
				    WO.requester_requesting_date AS request_date,
				    WO.requester_punch_in_time AS punch_in,
				    WO.requester_punch_out_time AS punch_out,
				    WO.reason,
				    WO.conf_status,
				    work_request_od_id AS wo_id,
				    `WO`.`conf_dt` AS `confirm_date`,
				    `WO`.`approve_dt` AS `approve_date`,
				    `AP`.`full_name` AS `approve_user_name`,
				    `CO`.`full_name` AS `conf_user_name`
				    $exSelect
				FROM
				    `work_request_od` `WO`
				LEFT JOIN `user_mst` `CO` ON
				    `CO`.`user_id` = `WO`.`conf_user`
				LEFT JOIN `user_mst` `AP` ON
				    `AP`.`user_id` = `WO`.`approve_user`
				JOIN `user_mst` `RE` ON
				    `RE`.`user_id` = `WO`.`emp_user_id`
				JOIN `user_mst` `RQ` ON
				    `RQ`.`user_id` = `WO`.`req_user_id`
				JOIN `project_mst` `P` ON
				    `P`.`project_id` = `WO`.`project_id_fk`
				$join
				WHERE
				    `WO`.`dept_id_fk` = $dept $where
				ORDER BY `WO`.`work_request_od_id` DESC"; ;
		$sqlQuery = $this->db->query($sql);
		return $sqlQuery->result_array();
	}	
    public function update_work_stat($myrows,$user_id,$sel_status,$rema)
	{
		$this->db->trans_start();
		//print_r($myrows);
		if($sel_status=="Confirmed" || $sel_status=="Rejected")
		{
			$data = array(
							'upd_user' => $user_id,
							'conf_user' => $user_id,
							'conf_status' => $sel_status						
					);
			$this->db->set('conf_dt', 'current_timestamp', FALSE);
		}else if($sel_status=="Approved" || $sel_status=="UnApproved")
		{
			$data = array(
							'upd_user' => $user_id,
							'approve_user' => $user_id,
							'conf_status' => $sel_status,
							'approve_remarks' => $rema	
					);
			$this->db->set('approve_dt', 'current_timestamp', FALSE);
		}else{
			$data = array(
							'upd_user' => $user_id,
							'conf_status' => $sel_status						
					);
		}
		
					$this->db->where_in('work_request_id', $myrows);
					$this->db->set('upd_dt', 'current_timestamp', FALSE);				
					$this->db->update('work_request', $data);
			$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return "success";
		  }
	}
	//Update od request status
	public function update_od_request($updateUserId,$user_id,$sel_status,$remark){
		$this->db->trans_start();
		if($sel_status=="Confirmed" || $sel_status=="Rejected")
		{
			$data = array(
					'upd_user' => $user_id,
					'conf_user' => $user_id,
					'conf_status' => $sel_status						
			);
			$this->db->set('conf_dt', 'current_timestamp', FALSE);
		}else if($sel_status=="Approved" || $sel_status=="UnApproved")
		{
			$data = array(
					'upd_user' => $user_id,
					'approve_user' => $user_id,
					'conf_status' => $sel_status,
					'approve_remarks' => $remark	
			);
			$this->db->set('approve_dt', 'current_timestamp', FALSE);
		}else{
			$data = array(
				'upd_user' => $user_id,
				'conf_status' => $sel_status						
			);
		}

		$this->db->where_in('work_request_od_id', $updateUserId);
		$this->db->set('upd_dt', 'current_timestamp', FALSE);				
		$this->db->update('work_request_od', $data);
		$this->db->trans_complete();
		if($this->db->trans_status() === FALSE){
		 	return false;
	  	}
	  	else{
		 	return true;
	  	}
	}
	public function approve_combotype_stat($user_row_id,$user_id,$combotype)
	{
		$this->db->trans_start();
		//print_r($myrows);
		$data = array(
							'upd_user' => $user_id,
							'conf_status' => 'Pending',
							'combo_type'=>$combotype					
					);
					$this->db->where_in('work_request_id', $user_row_id);
					$this->db->set('upd_dt', 'current_timestamp', FALSE);				
					$this->db->update('work_request', $data);
			$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
		  {
			 return false;
		  }
		  else{
			 return "success";
		  }
	}
}
?>