<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 Class Upload extends CI_Controller {

      public function __construct() {
         parent::__construct();
         //$this->load->helper(array('form', 'url'));
		 $this->load->helper(array('form', 'url','assestsurl_helper'));
		 $this->load->model('import_model');
		 $this->load->model('user_model');
		 $this->load->library('session');
		 $this->load->library('CSVReader');
		 
		 $this->userImportHeaderList = [
             'Full Name',
             'Email',
             'Role',
             'Project',
             'Week Off',
             'Streak',
             'Access Name',
             'Manager',
             'Status',
             'Mail Flag',
             'Department',
             'Tnl ID',
             'Joining Date Intern',
             'Joining Date Permanent',
             'Slot',
             'Sub Department',
             'Unit Name',
             'Vertical',
             'Designation',
             'Other Manager',
             'Project Owner',
             'POC',
			 'Grading Access'
         ];

         $this->logisticImportHeaderList = [
             'Email',
             'Deputed Location',
             'Office',
             'Role Center',
             'Sapience Status',
             'Ftrack',
             'Adobe ID',
             'Domain Login',
             'System No',
             'Work Station',
             'Employment Type',
             'Employment Status',
             'Start Date',
             'End Date',
             'Currently Active',
         ];
		  $this->IOImportHeaderList = [
             'DATE','EMAIL ID','DEPARTMENT','UNIT','PUNCH IN TIME','PUNCH OUT TIME','TOTAL WRK HRS','BREAK HRS','ATTENDANCE STATUS','LATE STAUS','TOTAL DAYS','ATTENDANCE COUNT','TEAM','WEEKOFF','SAPIENCE-ON PC','SAPIENCE-OFF PC','UNACCOUNTED TIME','TIME ON PRIVATE (G)'
         ];
      }

	  	function do_upload(){
        $config['upload_path']="./uploads/";
        $config['allowed_types']='csv';
		$d_id=$this->input->get("d");
		//echo "dn".$d_id;
        $this->load->library('upload',$config);
		if($d_id)
		{
	    if($this->upload->do_upload("file")){
	        $data = array('upload_data' => $this->upload->data());
	        $filename=$data['upload_data']['file_path']."/".$data['upload_data']['file_name'];
			$row[]=array();
                    $is_header_removed = FALSE;
					if (($file = fopen($filename, "r")) !== FALSE) {
						$i=0;
                    while(($importdata = fgetcsv($file,'', ",")) !== FALSE)
                    {
						if(!$is_header_removed)
						{
							$is_header_removed = TRUE;
							continue;
						}
						
						//Skip empty values
                        if(empty($importdata[0]) || empty($importdata[1]) || empty($importdata[2]) || empty($importdata[3]) || empty($importdata[4])){
                            continue;
                        }
						
						$week_start ='';
						$da=date('Y-m-d H:i:s', strtotime(TRIM($importdata[7])));
						$day_w = date('w',strtotime($da));

							if($day_w==0)
							{
								$day_w=7;
							}
							$week_start = date('Y-m-d', strtotime($da.' -'.($day_w-1).' days'));
							
							$week_start1 ='';
						$da1=date('Y-m-d H:i:s');
						$day_w1 = date('w',strtotime($da1));

							if($day_w1==0)
							{
								$day_w1=7;
							}
							$week_start1 = date('Y-m-d', strtotime($da1.' -'.($day_w1-1).' days'));
							
                        $row[$i++] = array(
                            'project_name'    =>  !empty($importdata[0])?TRIM($importdata[0]):null,
                            'task_name'     =>  !empty($importdata[1])?TRIM($importdata[1]):null,
                            'sub_name'         =>  !empty($importdata[2])?TRIM($importdata[2]):null,
                            'job_name'        =>  !empty($importdata[3])?TRIM($importdata[3]):null,
                            'wu_name'       =>  !empty($importdata[4])?TRIM($importdata[4]):null,
							'is_quantify'       =>  !empty($importdata[5])?TRIM($importdata[5]):null,
							'tgt_val'       =>  !empty($importdata[6])?TRIM($importdata[6]):null,
							'start_dt'       =>  !empty($importdata[7])?$week_start:$week_start1,
							'dept_id' => $d_id
                        );
					}
					$i--;
			fclose($file);
			$user_id            = $this->session->userdata('user_id');

			$tab_dta = $this->import_model->import_csvd($row,$user_id);
				if($tab_dta=='success')
				{
					echo "success";

				}else{
					echo "Error-".$tab_dta;
				}
			}
		       }else {
		$error =$this->upload->display_errors();
			echo $error;
		}
		}
     }

		function do_upload_io_feed(){
			$response['status'] = false;
        $config['upload_path']="./uploads/io_metrics/";
        $config['allowed_types']='csv';
		$d_id=$this->input->get("d");
		//echo "dn".$d_id;
        $this->load->library('upload',$config);

	    if ($this->upload->do_upload("file")) {
	        $data = array('upload_data' => $this->upload->data());
	        $filename=$data['upload_data']['file_path']."/".$data['upload_data']['file_name'];
			$row=array();
			$user_id            = $this->session->userdata('user_id');
            //$file = $_FILES['level_file']['tmp_name']; //path to csv file
			$fileHandler = fopen($filename, "r");
			/*array_walk($csvData, function(&$v) { $v['dept_id_fk'] = 'f'; });*/
			//Check the header of an uploaded file
			$this->isHeaderCorrect($fileHandler);

			fclose($fileHandler);
    		$csvData = $this->csvreader->parse_file($filename); //path to csv file
               	foreach ($csvData as $key => $importdata) {
                
                $row[$key] = array(
                	'date_val'    =>  !empty($importdata['Date'])?date('Y-m-d H:i:s', strtotime(TRIM($importdata['Date']))):null,
                	'email_id'     =>  !empty($importdata['Email ID'])?TRIM($importdata['Email ID']):null,
                    'department'     =>  !empty($importdata['Department'])?TRIM($importdata['Department']):null,
                    'unit'     =>  !empty($importdata['Unit'])?TRIM($importdata['Unit']):null,
                	'in_time'         =>  !empty($importdata['Punch In Time'])?TRIM($importdata['Punch In Time']):null,
                	'out_time'         =>  !empty($importdata['Punch Out Time'])?TRIM($importdata['Punch Out Time']):null,
                	'esp_wrk_hrs'        =>  !empty($importdata['Total Wrk Hrs'])?TRIM($importdata['Total Wrk Hrs']):null,
                    'break_hrs'        =>  !empty($importdata['Break hrs'])?TRIM($importdata['Break hrs']):null,
                	'esp_status'       =>  !empty($importdata['Attendance Status'])?TRIM($importdata['Attendance Status']):null,
                    'late_status'       =>  !empty($importdata['Late Staus'])?TRIM($importdata['Late Staus']):null,
                    'total_days'       =>  !empty($importdata['Total Days'])?TRIM($importdata['Total Days']):null,
                    'attendance_cnt'       =>  !empty($importdata['Attendance Count'])?TRIM($importdata['Attendance Count']):null,
                    'team'       =>  !empty($importdata['Team'])?TRIM($importdata['Team']):null,
                    'week_off'       =>  !empty($importdata['Weekoff'])?TRIM($importdata['Weekoff']):null,
                	'sapience_on'       =>  !empty($importdata['Sapience-On PC'])?TRIM($importdata['Sapience-On PC']):null,
                	'sapience_off'       =>  !empty($importdata['Sapience-Off PC'])?TRIM($importdata['Sapience-Off PC']):null,
                    'unaccounted_time'       =>  !empty($importdata['Unaccounted Time'])?TRIM($importdata['Unaccounted Time']):null,
                    'time_on_private'       =>  !empty($importdata['Time on Private (G)'])?TRIM($importdata['Time on Private (G)']):null,
                	'ins_user'       =>  $user_id
                );
                }
              
                $tab_dta = $this->import_model->import_io_times($row,$user_id);
              
				  if($tab_dta && $tab_dta['status']){
							$response['status'] = true;
							$response['errors'] = $tab_dta['errors'];
							$message = (isset($response['errors']) && count((array)$response['errors']) > 0) ? "ESP data inserted partially.Some error has occurred. Please check error file." : "Uploaded successfully.";
							$response['message'] = $message;
						}else{
							$response['message'] = "Already in use.Please try after some time.";
						}
						
            
		   } else {
		                 $response['message'] = $this->upload->display_errors();
		}
		echo json_encode($response);
			exit;
     }

	  public function upate_user_status(){
     	$sel_status=$this->input->post('sel_status');
     	$checkedRows=$this->input->post('checkedRows');
     	$user_id            = $this->session->userdata('user_id');
     	if($sel_status!='' && !empty($checkedRows)){
     		$myrows=array_column($checkedRows, 'user_login_name');
			$tab_dta = $this->import_model->update_usr_stat($myrows,$user_id,$sel_status);
     		if($tab_dta){
	           		 echo "Successfully";
	          		}
     	}

     	if(isset($_FILES['file'])  && empty($checkedRows) && $sel_status==''){
     		$config['upload_path']="./uploads/utility/users/";
    		$config['allowed_types']="csv";
	   		$config['max_size'] = '1000';
   			$this->load->library('upload', $config);
   		    $this->upload->initialize($config);
    	// If upload failed, display error
	    if (!$this->upload->do_upload('file'))
	    {
	        $data['error'] = $this->upload->display_errors();

	      	print_r(strip_tags($data['error']));
	      	//echo "error";
	    }
    	else
    	{
       		 $file_data = $this->upload->data();
	        $file_path =  './uploads/utility/users/'.$file_data['file_name'];

	        if ($this->csvreader->parse_file($file_path))
	        {	 $csvData = $this->csvreader->parse_file($file_path); //path to csv file
	        	if (($handle = fopen($file_path, 'r')) !== FALSE)
						{
						    $rowheading_chk = fgetcsv($handle, 1000, ",");
						    // print_r($rowheading_chk);

						}
					if(!in_array( "Status", $rowheading_chk) ){
						header('Content-Type: application/json');
                        echo json_encode(array(
                            'success' => false,
                            'message' => 'Your csv file is not correct.'
                        ));
                        die;
					}
						$i=2;
	            foreach ($csvData as $row){
		      		$data1['status_nm']=$row['Status'];

					//$data1['upd_dt']='current_timestamp';

		      		// $this->db->where('user_login_name', $row['user_login_name']);
		       	     // $q=$this->db->get('user_mst');
		            // if( $q->num_rows() > 0 )
		            // {
						$data1['upd_user']=$user_id;
						$this->db->set('upd_dt', 'current_timestamp', FALSE);
		               $this->db->where('user_login_name',$row['Email']);
		               $done=$this->db->update('user_mst',$data1);

		           //}
					// else{
			            	// echo "Please correct error on line number-".$i;
			            	// die;
				       // }
				      $i++;
	            }
				  if($done){
	           			header('Content-Type: application/json');
                        echo json_encode(array(
                            'success' => true,
                            'message' => 'Users updated successfully.'
                        ));
	          		 }
			}

    	 }
     }
	 // else{
     			// echo "Choose either csv update or checkbox";
     // }


 	}

	public function importLogistic()
    {
        $file = $_FILES['file']['tmp_name'];
        $fileHandler = fopen($file, "r");

        // CHECK HEADER
        $this->isLogisticImportCorrect($fileHandler);
        $rowKey = 1;
        $erroredRowList = [];
        $userData = [];
        while (($row = fgetcsv($fileHandler, 1000, ",")) != FALSE) {
            $rowKey++;
            $dbDataByRowVal = $this->getLogisticDbDataByRowVal($row);
            // VALIDATE DATA
            $errorMessages = $this->logisticImportDataIsValid($dbDataByRowVal);
            if (!empty($errorMessages)) {
                $erroredRowList[] = array ('key' => $rowKey, 'error' => $errorMessages);
                continue;
            }
            // STORE USRE DATA
            $userData[]= $this->getLogisticDataObjectFromCsvRow($row, $dbDataByRowVal);
        }
        if($userData){
            $this->user_model->insertLogistic($userData);
        }
        if (!empty($erroredRowList)) {
            $this->userImportSuccessWithErrors($erroredRowList);
        } else {
            $this->userImportSuccess();
        }
    }

    public function importUsers()
    {
        $file = $_FILES['file']['tmp_name'];
        $fileHandler = fopen($file, "r");
        // CHECK HEADER
        $this->isUserImportHeaderCorrect($fileHandler);
        $rowKey = 1;
        $erroredRowList = [];
        while (($row = fgetcsv($fileHandler, 1000, ",")) != FALSE) {
            $rowKey++;
            $dbDataByRowVal = $this->getUserImportDbDataByRowVal($row);
            // VALIDATE DATA
            $errorMessages = $this->userImportDataIsValid($dbDataByRowVal);
            if (!empty($errorMessages)) {
                $erroredRowList[] = array ('key' => $rowKey, 'error' => $errorMessages);
                continue;
            }
            // STORE USRE DATA
            $userData = $this->getUserDataObjectFromCsvRow($row, $dbDataByRowVal);
            $this->user_model->insertUser($userData);
        }
        fclose($fileHandler);

        if (!empty($erroredRowList)) {
            $this->userImportSuccessWithErrors($erroredRowList);
        } else {
            $this->userImportSuccess();
        }
    }

    public function userImportSuccessWithErrors($erroredRowList)
    {
        header('Content-Type: application/json');
        echo json_encode(array(
            'success' => false,
            'errors' => $erroredRowList,
            'message' => 'Data Imported Successfully but, Some rows have errors'
        ));
    }

    public function userImportSuccess()
    {
        header('Content-Type: application/json');
        echo json_encode(array(
            'success' => true,
            'message' => 'Data Imported Successfully'
        ));
    }

    public function getUserImportDbDataByRowVal($row)
    {
        $data = [];
        $data['email'] = $this->user_model->getUserByEmail($row[1]);
        $data['role'] = $this->user_model->getRoleByName($row[2]);
        $data['projects'] = $this->user_model->getProjectsByNames(explode(',', $row[3]));
        $data['manager'] = $this->user_model->getUserByEmail($row[7]);
        $data['department'] = $this->user_model->getDepartmentByName(explode(',', $row[10]));
        $data['otherManager'] = $this->user_model->getUserByEmail($row[19]);
        $data['projectOwner'] = $this->user_model->getUserByEmail($row[20]);
        $data['poc'] = $this->user_model->getUserByEmail($row[21]);
        return $data;
    }

    public function getLogisticDbDataByRowVal($row)
    {
        $data = [];
        $data['email'] = $this->user_model->getUserByEmail($row[0]);
        $data['startDate'] = $row[12];
        $data['endDate'] = $row[13];
        return $data;
    }

    public function isUserImportHeaderCorrect($fileHandler)
    {
        if (fgetcsv($fileHandler) != $this->userImportHeaderList) {
            header('Content-Type: application/json');
        	echo json_encode(array(
    			'success' => false,
    			'message' => 'Columns in the header might be wrong or some columns might be missing'
    		));
            exit;
        }
    }

    public function isLogisticImportCorrect($fileHandler)
    {
        if (fgetcsv($fileHandler) != $this->logisticImportHeaderList) {
            header('Content-Type: application/json');
        	echo json_encode(array(
    			'success' => false,
    			'message' => 'Columns in the header might be wrong or some columns might be missing'
    		));
            exit;
        }
    }
	
	 private function isHeaderCorrect($fileHandler){
		$headerList = $this->IOImportHeaderList;
		$fileHeader = fgetcsv($fileHandler);
		
		$fileHeader = array_map('strtoupper', $fileHeader);

		$diffHeader = array_diff($headerList, $fileHeader);
		if (count($diffHeader) > 0) {
            header('Content-Type: application/json');
        	echo json_encode(array(
    			'status' => false,
    			'message' => 'Columns in the header might be wrong or some columns might be missing'
    		));
            exit;
        }
	}
	
    public function getUserDataObjectFromCsvRow($row, $dbDataByRowVal)
    {
        $userId = $this->session->userdata('user_id');

        $userData = array();
        $userData['full_name'] = $row[0];
        $userData['user_login_name'] = $row[1];
        $userData['role_id_fk'] = $dbDataByRowVal['role']->role_id;
        $userData['project_id_fk'] = $dbDataByRowVal['projects']->project_ids;
        $userData['week_off'] = $row[4];
        $userData['streak'] = $row[5];
        $userData['access_name'] = $row[6];
        $userData['manager_id'] = $dbDataByRowVal['manager']->user_id;
        $userData['status_nm'] = $row[8];
        $userData['mail_flag'] = $row[9];
        $userData['dept_id_fk'] = $dbDataByRowVal['department'];
        $userData['emp_id'] = $row[11];
        $userData['ins_user'] = $userId;
        $userData['ins_dt'] = date('Y-m-d H:i:s');
        $userData['joining_date_intern'] = date('Y-m-d H:i:s', strtotime($row[12]));
        $userData['joining_date_permanant'] = date('Y-m-d H:i:s', strtotime($row[13]));
        $userData['slot'] = $row[14];
        $userData['sub_department'] = $row[15];
        $userData['unit_name'] = $row[16];
        $userData['vertical'] = $row[17];
        $userData['designation'] = $row[18];
		$userData['is_grading'] = $row[22];
        $userData['manager_id_fk'] = $dbDataByRowVal['otherManager']->user_id;
        $userData['project_owner_id_fk'] = $dbDataByRowVal['projectOwner']->user_id;
        $userData['poc_id_fk'] = $dbDataByRowVal['poc']->user_id;

        return $userData;
    }

    public function getLogisticDataObjectFromCsvRow($row, $dbDataByRowVal)
    {
        $userId = $this->session->userdata('user_id');
        $userData = [];
        $userData['user_id_fk'] = $dbDataByRowVal['email']->user_id;
        $userData['deputed_location'] = $row[1];
        $userData['office'] = $row[2];
        $userData['role_center'] = $row[3];
        $userData['sapience_status'] = $row[4];
        $userData['f_track'] = $row[5];
        $userData['adobe_id'] = $row[6];
        $userData['domain_login'] = $row[7];
        $userData['system_no'] = $row[8];
        $userData['workstation'] = $row[9];
        $userData['employment_type'] = $row[10];
        $userData['employment_status'] = $row[11];
        $userData['start_date'] = $row[12] ? date("Y-m-d", strtotime($row[12])) : NULL;
        $userData['end_date'] = $row[13] ? date("Y-m-d", strtotime($row[13])) : NULL;
        $userData['status_nm'] = $row[11] === "employee" ? "Active" : "InActive";
        $userData['is_current'] = $row[14] === "Yes" ? "Y" : "N";
        $userData['ins_user'] = $userId;
        $userData['ins_dt'] = date('Y-m-d H:i:s');
        return $userData;
    }

    public function userImportDataIsValid($dbDataByRowVal)
    {
        $message = '';

        if (!empty($dbDataByRowVal['email'])) {
            $message .= 'Email already existed, ';
        }
        if (empty($dbDataByRowVal['role'])) {
            $message .= 'Role is Invalid, ';
        }
        if (empty($dbDataByRowVal['projects']->project_ids)) {
            $message .= 'Project List is not correct, ';
        }
        if (empty($dbDataByRowVal['manager'])) {
            $message .= 'Manager does not exist ';
        }
        if (empty($dbDataByRowVal['otherManager'])) {
            $message .= 'Other Manager does not exist ';
        }
        if (empty($dbDataByRowVal['projectOwner'])) {
            $message .= 'Project owner does not exist ';
        }
        if (empty($dbDataByRowVal['poc'])) {
            $message .= 'POC does not exist ';
        }
        return $message;
    }

    public function logisticImportDataIsValid($dbDataByRowVal)
    {
        $message = '';
        if (empty($dbDataByRowVal['email'])) {
            $message .= 'User does not exists with given email, ';
        }
        /*if (empty($dbDataByRowVal['startDate'])) {
            $message .= 'Start date is empty, ';
        }
        if (empty($dbDataByRowVal['endDate'])) {
            $message .= 'End date is empty ';
        }*/
        return $message;
    }
 	//do_utility_activity for closing
 	public function do_utility_activity(){
		//$d_id=$this->input->post("sel_dept");
		$d_id=$this->input->get("d");
		$response['status'] = false;
 		if(isset($_FILES['file'])){
 		 		$config['upload_path']="./uploads/utility/actvities/";
	    		$config['allowed_types']="csv";
		   		$config['max_size'] = '1000';
	   			$this->load->library('upload', $config);
	   		    $this->upload->initialize($config);
		    	// If upload failed, display error
			    if (!$this->upload->do_upload('file'))
			    {
			        $data['error'] = $this->upload->display_errors();
			       	strip_tags($this->upload->display_errors());
			      	$response['message'] = strip_tags($this->upload->display_errors());
			      	header('Content-Type: application/json');
    				echo json_encode( $response ); exit;
			    }else{
			    	 $file_data = $this->upload->data();
	       			 $file_path =  './uploads/utility/actvities/'.$file_data['file_name'];
	       			 if ($this->csvreader->parse_file($file_path)){
	       			 	$csvData = $this->csvreader->parse_file($file_path);
	       			 	$i=0;
						if (($handle = fopen($file_path, 'r')) !== FALSE)
						{
						    $rowheading_chk = fgetcsv($handle, 1000, ",");
						    // print_r($rowheading_chk);
                        }

						$ckary=array('Project','Task','Sub Task','Job','Work Unit','Is Closed');
						foreach($ckary as $v){
							if(!in_array($v, $rowheading_chk)){
								header('Content-Type: application/json');
								$response['message'] = "Your csv file is not correct";
    							echo json_encode( $response ); exit;
							}
						}

	       			 $is_closed_st =array_column($csvData, 'Is Closed');
						if(count(array_diff($is_closed_st, ["Y","N"])) > 0){
							header('Content-Type: application/json');
								$response['message'] = "Closed status must be Y or N";
    							echo json_encode( $response ); exit;
						}
						array_walk_recursive($csvData, function(&$value){
							return $value = $value === "N" ? NULL : $value;
						}); 

						// foreach ($csvData as $key => $value) {
						// 	$csvData[$key]['dept_id']=$d_id;
						// }
                        

                        $csvData = array_map(function($csv) use($d_id){
                            return [
                                'project' => $csv['Project'],
                                'task' => $csv['Task'],
                                'sub_task' => $csv['Sub Task'],
                                'job' => $csv['Job'],
                                'work_unit' => $csv['Work Unit'],
                                'is_closed' => $csv['Is Closed'],
                                'dept_id' => $d_id
                            ];
                        }, $csvData);
                        
                        //Call to update activity status
						$user_id = $this->session->userdata('user_id');
	       			 	$tab_dta = $this->import_model->import_utility_closer_csv($csvData,$user_id);
						
						if($tab_dta && $tab_dta['status']){
							$response['status'] = true;							
							$response['message'] = $tab_dta['message'];
						}else{
							$response['message'] = "Already in use.Please try after some time.";
						}
						
	       			}
			    }
 		}else{
 		 echo "Please select file";
 		}
		
		echo json_encode($response);
			exit;
 	}
 	//bulk upload
 	public function do_utility_act(){
		$response['status'] = false;
 			$d_id=$this->input->get("d");
 			if(isset($_FILES['file'])){
 				$config['upload_path']="./uploads/utility/actvities/";
	    		$config['allowed_types']="csv";
		   		$config['max_size'] = '1000';
	   			$this->load->library('upload', $config);
	   		    $this->upload->initialize($config);
		    	// If upload failed, display error
			    if (!$this->upload->do_upload('file'))
			    {
			        $data['error'] = $this->upload->display_errors();
			       echo strip_tags($this->upload->display_errors());
			      	//echo "error";
			    }else{
			    	 $file_data = $this->upload->data();
	       			 $file_path =  './uploads/utility/actvities/'.$file_data['file_name'];
	       			if ($this->csvreader->parse_file($file_path)){
	       			 	$csvData = $this->csvreader->parse_file($file_path);
	       			 	//print_r($csvData);
	       			 	$ary=array();
	       			 	$i=0;
	       			 	$week_start ='';
						if (($handle = fopen($file_path, 'r')) !== FALSE)
						{
						    $rowheading_chk = fgetcsv($handle, 1000, ",");
						    // print_r($rowheading_chk);die;

						}
						$ckary=array('Project','Task','Sub Task','Job','Work Unit','Is Quanitfy','Tgt','Start Date');
						foreach($ckary as $v){
							if(!in_array($v, $rowheading_chk)){
								$response['message'] ="Your csv file is not correct";
								echo json_encode($response);
								exit;
							}
						}

	       			 	foreach($csvData as $csv){
	       			 		$da=date('Y-m-d H:i:s', strtotime(TRIM( $csv['Start Date'])));
							$day_w = date('w',strtotime($da));

							if($day_w==0)
							{
								$day_w=7;
							}
							//Skip empty values
                            if(empty($csv['Project']) || empty($csv['Task']) || empty($csv['Sub Task']) || empty($csv['Job']) || empty($csv['Work Unit'])){
                                continue;
                            }
							$week_start = date('Y-m-d', strtotime($da.' -'.($day_w-1).' days'));
	       			 			$ary[$i]['project_name']= $csv['Project'];
	       			 			$ary[$i]['task_name']= $csv['Task'];
	       			 			$ary[$i]['sub_name']= $csv['Sub Task'];
	       			 			$ary[$i]['job_name']= $csv['Job'];
	       			 			$ary[$i]['wu_name']= $csv['Work Unit'];
	       			 			$ary[$i]['tgt_val']= $csv['Tgt'];
	       			 			$ary[$i]['start_dt']=$week_start;
	       			 			$ary[$i]['dept_id']= $d_id;
	       			 	$i++;
	       			 	}

	       			 	//print_r($ary);
	       			 	$user_id = $this->session->userdata('user_id');
			            $tab_dta = $this->import_model->import_utilitycsv($ary,$user_id);
			            
						if($tab_dta && $tab_dta['status']){
							$response['status'] = true;
							$response['errors'] = $tab_dta['errors'];
							$message = (isset($response['errors']) && count((array)$response['errors']) > 0) ? "Activity inserted partially.Some error has occurred. Please check error file." : "Updated successfully.";
							$response['message'] = $message;
						}else{
							$response['message'] = "Already in use.Please try after some time.";
						}
					}
				}
 			}else{
 				$response['message'] ="Please select file";
 			}
			echo json_encode($response);
			exit;
 	}

	function DownloadUerStatusTem(){
 		 $tab=$_GET['tab'];
 		if($tab=='utility_user'){
 			$file_path =  './uploads/templates/Userstatus.csv';
 			$filename='Update_user_status.csv';

 		}
 		if($tab=='utiltiy_activity'){
 			$file_path =  './uploads/templates/activity_project.csv';
 			$filename='Update_activity.csv';
 		}
		if($tab=='activity'){
 			$file_path =  './uploads/templates/activity_project.csv';
 			$filename='Update_activity.csv';
 		}
 		if($tab=='utility_closing_activity'){
 			$file_path =  './uploads/templates/activity_close.csv';
 			$filename='Update_closing_activity.csv';
 		}
		if($tab=='contentmid'){
 			$file_path =  './uploads/templates/content.csv';
 			$filename='ContentMids.csv';
 		}
 		if($tab=='mediamid'){
 			$file_path =  './uploads/templates/media.csv';
 			$filename='MediaMids.csv';
 		}
		if ($tab == 'import_user_csv') {
            $file_path =  './uploads/templates/import-user.csv';
 			$filename='import-user.csv';
        }
        if ($tab == 'import_logistic_csv') {
             $file_path =  './uploads/templates/import-user-logistics.csv';
 			$filename='import-user-logistics.csv';
        }


		if (file_exists($file_path)) {
	       	header('HTTP/1.1 200 OK');
	        header('Cache-Control: no-cache, must-revalidate');
	        header("Pragma: no-cache");
	        header("Expires: 0");
		    header('Content-Type: text/csv');
		    header('Content-Disposition: attachment; filename="' . $filename . '";');
		    readfile($file_path);
	        exit;
	    }

	}

	// upload mids
	public function do_mid_gen(){
 			$d_id=$this->input->get("d");
 			$user_id = $this->session->userdata('user_id');
 			if($d_id=='1' || $d_id=='2'){
	 			if(isset($_FILES['file'])){
	 				$config['upload_path']="./uploads/mids/";
		    		$config['allowed_types']="csv";
			   		$config['max_size'] = '1000';
		   			$this->load->library('upload', $config);
		   		    $this->upload->initialize($config);
		   		    if(!is_dir($config['upload_path'])) mkdir($config['upload_path'], 0777, TRUE);
			    	// If upload failed, display error
				    if (!$this->upload->do_upload('file'))
				    {
				        $data['error'] = $this->upload->display_errors();
				       echo strip_tags($this->upload->display_errors());
				      	//echo "error";
				    }else{
				    	 $file_data = $this->upload->data();
		       			 $file_path =  './uploads/mids/'.$file_data['file_name'];
		       			if ($this->csvreader->parse_file($file_path)){
		       			 	$csvData = $this->csvreader->parse_file($file_path);
		       			 	//print_r($csvData);die;
		       			 	$ary=array();
		       			 	//$i=0;
							$i=1;
		       			 	$week_start ='';
							if (($handle = fopen($file_path, 'r')) !== FALSE)
							{
							    $rowheading_chk = fgetcsv($handle, 1000, ",");
							  // print_r($rowheading_chk);

							}
							//$ckary=array('project','task','subtask','job','year','syllabus','grade','subject','chapter');
							$ckary=array('Project','Task','Sub Task','Job','Department','Year','Syllabus','Grade','Subject','Chapter','Sub Division','Div Num','Sec Sub Division','Sec Div Num','Third Sub Division','Third Div Num');
							foreach($ckary as $v){
								if(!in_array($v, $rowheading_chk)){
									echo "Your csv file is not correct";
									die;
								}
							}
		       			 	foreach($csvData as $csv){
		       			 	if(chechcntsyll($csv['Syllabus'])==true &&  chechcntgrade($csv['Syllabus'],$csv['Grade'],$csv['Subject'])==true && chechcntsubject($csv['Grade'],$csv['Subject'],$csv['Chapter'])==true && chechcntchapter($csv['Subject'],
		       			 		$csv['Chapter'],$csv['Sub Division'])==true && chechcntsub_div($csv['Chapter'],$csv['Sub Division'],$csv['Div Num'])==true && chechcntdiv_num($csv['Sub Division'],$csv['Div Num'],$csv['Sec Sub Division'])==true
		       			 			&& check_sec_sub_div($csv['Div Num'],$csv['Sec Sub Division'],$csv['Sec Div Num'])==true &&check_sec_div_num($csv['Sec Sub Division'],$csv['Sec Div Num'],$csv['Third Sub Division'])==true
		       			 			&& check_third_sub_div($csv['Sec Div Num'],$csv['Third Sub Division'],$csv['Third Div Num'])==true && check_third_div_num($csv['Third Sub Division'],$csv['Third Div Num'])==true
		       			 ){

			       			 				$ary[$i]['project_name']= $csv['Project'];
			       			 				$ary[$i]['task_name']= $csv['Task'];
			       			 				$ary[$i]['sub_name']= $csv['Sub Task'];
			       			 				$ary[$i]['job_name']= $csv['Job'];
				       			 			$ary[$i]['dept_id']=$d_id;
				       			 			$ary[$i]['year']= $csv['Year'];
				       			 			$ary[$i]['syllabus']= str_replace(' ','',$csv['Syllabus']);
				       			 			$ary[$i]['grade']= !empty($csv['Grade']) ?appendzero($csv['Grade'],2): NULL;
				       			 			$ary[$i]['subject']= !empty($csv['Subject']) ?str_replace(' ','',$csv['Subject']) : NULL;
				       			 			$ary[$i]['chapter']=!empty($csv['Chapter']) ?appendzero($csv['Chapter'],2) : NULL;
				       			 			$ary[$i]['sub_div']=!empty(str_replace(' ','',$csv['Sub Division'])) ?str_replace(' ','',$csv['Sub Division']) : NULL;
				       			 			$ary[$i]['div_num']=!empty($csv['Div Num']) ?appendzero($csv['Div Num'],3) : NULL;
											$ary[$i]['sec_sub_div']=!empty(str_replace(' ','',$csv['Sec Sub Division'])) ?str_replace(' ','',$csv['Sec Sub Division']) : NULL;
				       			 			$ary[$i]['sec_div_num']=!empty($csv['Sec Div Num']) ?appendzero($csv['Sec Div Num'],3) : NULL;
				       			 			$ary[$i]['third_sub_div']=!empty(str_replace(' ','',$csv['Third Sub Division'])) ?str_replace(' ','',$csv['Third Sub Division']) : NULL;
				       			 			$ary[$i]['third_div_num']=!empty($csv['Third Div Num']) ?appendzero($csv['Third Div Num'],3) : NULL;
				       			 			$ary[$i]['status_nm']='Active';
				       			 			$ary[$i]['ins_user']=$user_id;

		       			 				}else{
		       			 					//	echo "Something wrong  in csv line-".$i."";
											echo "Something wrong  in csv line-".$i."";die;
		       			 				}

		       			 	$i++;
		       			 	}
		       			 	//print_r($ary);die;
				            $tab_dta = $this->import_model->ImportContentMidCsv($ary,$d_id);
				            if($tab_dta){echo "Successfully inserted";}
						}
					}
	 			}else{
	 				echo "Please select file";
	 			}
	 		}
			else{
	 			if(isset($_FILES['file'])){
	 				$config['upload_path']="./uploads/mids/";
		    		$config['allowed_types']="csv";
			   		$config['max_size'] = '1000';
		   			$this->load->library('upload', $config);
		   		    $this->upload->initialize($config);
		   		    if(!is_dir($config['upload_path'])) mkdir($config['upload_path'], 0777, TRUE);
			    	// If upload failed, display error
				    if (!$this->upload->do_upload('file'))
				    {
				        $data['error'] = $this->upload->display_errors();
				       echo strip_tags($this->upload->display_errors());
				      	//echo "error";
				    }else{
				    	 $file_data = $this->upload->data();
		       			 $file_path =  './uploads/mids/'.$file_data['file_name'];
		       			if ($this->csvreader->parse_file($file_path)){
		       			 	$csvData = $this->csvreader->parse_file($file_path);
		       			 	//print_r($csvData);die;
		       			 	$ary=array();
		       			 	$i=0;
		       			 	$week_start ='';
							if (($handle = fopen($file_path, 'r')) !== FALSE)
							{
							    $rowheading_chk = fgetcsv($handle, 1000, ",");
							 // print_r($rowheading_chk);die;

							}
							$ckary=array('Projects','Mids');
							foreach($ckary as $v){
								if(!in_array($v, $rowheading_chk)){
									echo "Your csv file is not correct";
									die;
								}
							}
		       			 	foreach($csvData as $csv){
		       			 				$ary[$i]['project_name']= $csv['Projects'];
			       			 				$ary[$i]['mids']= $csv['Mids'];
				       			 			$ary[$i]['status_nm']='Active';
				       			 			$ary[$i]['ins_user']=$user_id;
				       			 			$ary[$i]['dept_id']=2;

		       			 	$i++;
		       			 	}
		       			 //	print_r($ary);die;
				            $tab_dta = $this->import_model->ImportMediaMidCsv($ary);
				            if($tab_dta){echo "Successfully inserted";}
						}
					}
	 			}else{
	 				echo "Please select file";
	 			}
	 		}
	}

   }
?>