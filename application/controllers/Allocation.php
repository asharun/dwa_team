<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Allocation extends CI_Controller {

	private $default_date;
	public function __construct()
	{
		parent::__construct();
        $this->load->helper(['url','assestsurl_helper','cookie']);
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->model('allocation_model');
		$this->load->library("PHPMailer_Library");
        $this->load->library('CSVReader');
        $this->default_date = NULL; //date("Y-m-d",strtotime("01-01-2019"))
		define('VIDEOCLIP', '7');
        $this->load->library("pagination");

        //delete_cookie("search_value");
		$this->levelImportHeaderList = [
				'MID','NAME','OWNER','START DATE','END DATE','PROGRESS',
				'STATUS','DESCRIPTION','PRESENTER','STORYBOARD INCHARGE',
				'SIGN OFF','ESTIMATED END DATE','MANDAYS','VIDEO CLIP TYPE',
				'STORYBOARD BY','DURATION'
		];

		$this->activityUpHeaderList = [
			'PROJECT','TASK','SUB TASK','JOB','ALLOCATED TO','MID',
			'START DATE','END DATE','STATUS','TASK TYPE','PROGRESS','ESTIMATED END DATE'
		];

	}

	// public function getDetailsPage()
	// {
		// $data = array();
		// $allocationLevel = $this->input->get('allocationLevel');

		// $data['allocations'] = $this->getAllocationData($allocationLevel);

		// $data['users'] = $this->getusersForAllocation();

	// 	$this->load->view("allocations/allocation-details.php",$data);
	// }


        // $this->load->view("allocations/allocation-details.php",$data);
	// }

	public function getLevelsList()
	{
		return $this->allocation_model->getLevelsList();
	}
	public function getDetailsPage($levelCode = 0, $deptId = 0, $levelId = 0, $hierarchyId = 0)
	{
		define('MEDIA_ID', '2');


		$data = array();
		$currentDate = date("Y-m-d");
		$data['mediaCategories'] = array();
		$data['allLevelData'] = $this->allocation_model->getAllLevelData();
		$u_id = $this->session->userdata('user_id');
		$role_id  = $this->session->userdata('role_id');
		$dept_val = $this->allocation_model->fetchPermissionDept($u_id);
		// $dept_val = $this->user_model->fetchdept_data($u_id);
		$data["dept_val"]=$dept_val;
		$dept_opt=$dept_val[0]['dept_id'];
		$data["metrics"] = $this->user_model->fetchMetrics($u_id,$role_id,$dept_opt);

		$data['deptProjects'] = $this->user_model->sel1_data($currentDate, $deptId);
		$data['deptId'] = $deptId;
		$data['currentDate'] = $currentDate;
		$data['users'] = $this->getusersForAllocation($deptId);
		$data['allocationAjaxUrl'] = base_url() . 'index.php/Allocation/getAllocationData/' . $levelCode . '/' . $deptId .'/'. $levelId . '/' . $hierarchyId;

		$data["level_mst"] = $this->allocation_model->GetSideLevel($levelId);
		$deptIds = array_column($dept_val, 'dept_id');

		if ($deptId == MEDIA_ID) {
			$data['mediaCategories'] = $this->user_model->getAllCategories($deptId);
		}
		// var_dump($data['mediaCategories']); exit;
		if (in_array($deptId, $deptIds)) {
			$this->load->view("allocations/allocation-details.php",$data);
		} else {
			  redirect('user/login_view');
		}
	}

	public function getAllocationData($levelCode = 0, $deptId = 0, $levelId = 0, $hierarchyId = 0, $isAjax = false)
	{

		$levelData = $this->allocation_model->getLevelsData($deptId, $levelId, $hierarchyId, $isAjax);
		$activityData = array();
		// if ( $parentIds != "0" ) {
		$activityData = $this->allocation_model->getActivityData($deptId, $levelId, $hierarchyId, $isAjax);
		// }

		$resetActivityDataset = array();
		foreach ($activityData as $key => $data) {
				$data['hierarchy_id'] = 'A-' . $data['hierarchy_id'];
				$resetActivityDataset[] = $data;
		}
		$allHierarchyData = array_merge($levelData, $resetActivityDataset);

		// var_dump($allHierarchyData); exit;
		// Merge both of them in one single list and send it as a response

			// $allHierarchyData = $this->allocation_model->getAllHierarchyData();
			header('Content-Type: application/json');
	    echo json_encode(array('data' => $allHierarchyData, 'parentIds' => '') );
	}

	public function getAllocationDataByMid($levelId = 0, $midCode = 0, $isAjax = false)
	{
		$levelData = $this->allocation_model->getLevelsDataByMid($levelId, $midCode, $isAjax);
		// if ($hierarchyId != 0) {
		// 	foreach ($levelData as  $key => $data) {
		// 		if ($data['hierarchy_id'] == $hierarchyId) {
		// 			$levelData[$key]['p_hierarchy_id'] = NULL ;
		// 			break;
		// 		}
		// 	}
		// }

		$activityData = array();
		// if ( $parentIds != "0" ) {

			$activityData = $this->allocation_model->getActivityDataByMid($levelId, $midCode, $isAjax);
		// }

		//print_r($activityData);
		// $resetActivityDataset = array();
		// foreach ($activityData as $key => $data) {
		// 		$data['hierarchy_id'] = 'A-' . $data['hierarchy_id'];
		// 		$resetActivityDataset[] = $data;
		// }
		$allHierarchyData = array_merge($levelData, $activityData);

		// var_dump($allHierarchyData); exit;
		// Merge both of them in one single list and send it as a response

			// $allHierarchyData = $this->allocation_model->getAllHierarchyData();
			header('Content-Type: application/json');
	    echo json_encode(array('data' => $allHierarchyData) );
	}
	
	public function getMidWiseActivity($levelCode = 0, $deptId = 0, $levelId = 0, $hierarchyId = 0, $isAjax = false)
	{
		try{
			$activityData = array();
			$activityData = $this->allocation_model->getActivityWithMid($deptId, $levelId, $hierarchyId, $isAjax);
			
			header('Content-Type: application/json');
		    echo json_encode(['items' => $activityData]);
		}catch(Exception $e){
			
		}
	}

	public function getActivityLog()
	{
		try{
			$mid = $this->input->get("mid");
			$activityId = $this->input->get("activity_id");
			$activityLogs = $this->allocation_model->getActivityLogs($mid,$activityId);
			header('Content-Type: application/json');
			echo json_encode(['items' => $activityLogs]);
		}catch(Exception $e){

		}
	}

	public function getusersForAllocation($deptId = 0)
	{
		return $this->allocation_model->getUsers($deptId);
	}

	public function updateLevel($hierarchyId = 0)
	{
		if ($hierarchyId == 0) return false;

		$response['status'] = false;

		$levelData = $this->input->post('data');
		$deptId = $this->input->post('deptId');

		$midCode = $levelData['mid_code'];

		$hierarchyIdData = $this->allocation_model->getLevel($hierarchyId);
		$levelPreviousMidCode = $hierarchyIdData[0]['mid_code'];
		$levelLatestMidCode = !empty($levelData['mid_code']) ? $levelData['mid_code'] : '';
		// var_dump($deptId, $levelPreviousMidCode, $levelLatestMidCode); exit;
		$levelData['start_dt'] = $levelData['start_dt'] ? date('Y-m-d',strtotime($levelData['start_dt'])) : $this->default_date;

		$levelData['end_dt'] = $levelData['end_dt'] ? date('Y-m-d',strtotime($levelData['end_dt'])) : $this->default_date;

		$levelData['est_end_dt'] = $levelData['est_end_dt'] ? date('Y-m-d',strtotime($levelData['est_end_dt'])) : $this->default_date;
		$levelData['upd_dt'] = date("Y-m-d H:i:s");

		try {
			//Check any level of a mid already exists or not
			if($levelPreviousMidCode != $levelLatestMidCode && $this->allocation_model->checkLevelExists($midCode,$deptId,$hierarchyId)){
				$response['message'] = "Level already exists.";
				header('Content-Type: application/json');
				echo json_encode($response);
				exit;
			}

			$this->allocation_model->update('hierarchy_mst', $levelData, $hierarchyId);

			if (!empty($levelLatestMidCode) && $levelPreviousMidCode != $levelLatestMidCode) {
				$this->allocation_model->updateLevelDeepChildMids(
					$levelPreviousMidCode,
					$levelLatestMidCode,
					$deptId
				);
			}

			$levelData['p_hierarchy_id'] = $hierarchyIdData[0]['p_hierarchy_id'];
			if (!empty($levelData['video_clip_type'])) {
				$this->updateVCDuration(
					$levelData['p_hierarchy_id'],
					$levelData['video_clip_type']
				);
			}

			$previousLevelData = $this->allocation_model->getLevel($hierarchyId)[0];
			$this->allocationNotified("Level",$levelData, $previousLevelData);

			$response['status'] = true;
			$response['message'] = "Level updated successfully.";


			header('Content-Type: application/json');
			echo json_encode($response);
		} catch (Exception $e) {
			header('Content-Type: application/json');
			echo json_encode($response);
		}
	}

	/**
	* storeLevel for ajax callback
	*/
	public function storeLevel()
	{
		$parentData = $this->input->post('parentData');
		$levelData = $this->input->post('data');
		try {
			$response['status'] = false;

			$deptId = $this->input->post('dept_id_fk');
			$l_ddtt = $this->allocation_model->getChildLevelId($parentData['level_id_fk']);
			$levelId =$l_ddtt[0]['level_id'];
			$pHierarchyId = $parentData['hierarchy_id'];

			$levelData['start_dt'] = $levelData['start_dt'] ? date('Y-m-d',strtotime($levelData['start_dt'])) : $this->default_date;
			$levelData['end_dt'] = $levelData['end_dt'] ? date('Y-m-d',strtotime($levelData['end_dt'])) : $this->default_date;
			$levelData['est_end_dt'] = $levelData['est_end_dt'] ? date('Y-m-d',strtotime($levelData['est_end_dt'])) : $this->default_date;

			$levelData['level_id_fk'] = $levelId;
			$levelData['p_hierarchy_id'] = $pHierarchyId;
			$levelData['dept_id_fk'] = $deptId;
			$levelData['status_nm'] = 'Active';
			$levelData['ins_dt'] = date("Y-m-d H:i:s");

			$midCode = $levelData['mid_code'];

			//Check any level of a mid already exists or not
			if($this->allocation_model->checkLevelExists($midCode , $deptId)){
				$response['message'] = "Level already exists.";
				header('Content-Type: application/json');
				echo json_encode($response);
				exit;
			}
			$this->allocation_model->insert('hierarchy_mst', $levelData);

			if (!empty($levelData['video_clip_type'])) {
				$this->updateVCDuration(
					$levelData['p_hierarchy_id'],
					$levelData['video_clip_type']
				);
			}
			$this->allocationNotified("Level",$levelData);


			header('Content-Type: application/json');
			$response['status'] = true;
			$response['message'] = "Level added successfully.";
			echo json_encode($response);
		} catch (Exception $e) {
			header('Content-Type: application/json');
			echo json_encode( $response );
		}
	}


	public function allocationNotified($param,$levelData, $previousLevelData = null)
	{
	    $previousOwnerList = '';
	    if (!empty($previousLevelData)) {
	        $previousOwnerList = $previousLevelData['owner_mid'];
	    }
	    $latestAllocation = array_diff(explode(',', $levelData['owner_mid']), explode(',', $previousOwnerList));
	    if (!empty($latestAllocation)) {
	        $userId = $this->session->userdata('user_id');
	        $assignedBy = $this->user_model->getUserInfo($userId);
	        if (!empty($previousLevelData)) {
	            $levelData = $previousLevelData;
	        }
	        foreach ($latestAllocation as $key => $allocation) {
	            $assignedTo = $this->user_model->getUserInfo($allocation);
	            $this->sendAllocationNotification($param,$levelData, $assignedBy, $assignedTo);
	        }

	    }

	}

	public function sendAllocationNotification($param,$levelData, $assignedBy, $assignedTo)
	{
		if($assignedTo[0]['user_login_name'])
		{
		if($param=="Level")
		{
			$midCode = $levelData['mid_code'];
	    $message = "Hi " . $assignedTo[0]['full_name'] . ",<br>";
	    $message .= $assignedBy[0]['full_name'] .  " has just made you owner of <strong>" . $midCode;
	    $message .= "</strong>.";
	    $mail = $this->phpmailer_library->load();
	    $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
	    $mail->addAddress($assignedTo[0]['user_login_name']);
	    $mail->isHTML(true);
	    $mail->Subject = 'DWA: MiD Ownership';
	    $mail->Body = $message;
		}else{
		$midCode = $levelData['mid_code'];
	    $message = "Hi " . $assignedTo[0]['full_name'] . ",<br>";
	    $message .= $assignedBy[0]['full_name'] .  " has just assigned you <strong>" . $midCode;
	    $message .= "</strong>. Pls check your daily logs section for the allocated task.";
	    $mail = $this->phpmailer_library->load();
	    $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
	    $mail->addAddress($assignedTo[0]['user_login_name']);
	    $mail->isHTML(true);
	    $mail->Subject = 'DWA: Activity Allocation';
	    $mail->Body = $message;
		}
	    //print_r($message);
	    if (!$mail->Send()) {
	        $sendError = $mail->ErrorInfo;
	    }
		}
	}
	/**
	* updateActivity
	*/
	public function updateActivity($actHierarchyId)
	{
		$oldAcitvityData = $this->allocation_model->getActivity($actHierarchyId);
		$activityData = $this->input->post('data');
		$response['status'] = false;
		try {
			$activityData['start_dt'] = $activityData['start_dt'] ? date('Y-m-d',strtotime($activityData['start_dt'])) : $this->default_date;
			$activityData['end_dt'] = $activityData['end_dt'] ? date('Y-m-d',strtotime($activityData['end_dt'])) : $this->default_date;			
			$activityData['est_end_dt'] = isset($activityData['est_end_dt']) ? date('Y-m-d',strtotime($activityData['est_end_dt'])) : $this->default_date;
			$activityData['p_hierarchy_id'] = $oldAcitvityData[0]['p_hierarchy_id'];
			$activityData['dept_id_fk'] = $oldAcitvityData[0]['dept_id_fk'];


			if($oldAcitvityData[0]['dept_id_fk'] != '1' && $this->allocation_model->checkActivityExists($activityData, $actHierarchyId)){
				header('Content-Type: application/json');
				$response['message'] = "Activity already exists.";
				echo json_encode($response);
				exit;
			}
			
			$response['status']=$this->allocation_model->updateActivity($actHierarchyId, $activityData);

			$previousActivityData = $oldAcitvityData[0];
			$previousActivityData['owner_mid'] = $previousActivityData['alloc_to'];
			$activityData['owner_mid'] = $activityData['alloc_to'];
			$this->allocationNotified("Act",$activityData,$previousActivityData);

			//$response['status'] = true;
			$response['message'] = "Activity updated successfully.";
			header('Content-Type: application/json');
			echo json_encode($response);
		} catch (Exception $e) {
			header('Content-Type: application/json');
			echo json_encode($response);
		}
	}

	public function storeActivity()
	{

		$parentData = $this->input->post('parentData');
		$activityData = $this->input->post('data');
		try {
				$activityData['start_dt'] = $activityData['start_dt'] ? date('Y-m-d',strtotime($activityData['start_dt'])) : $this->default_date;
				$activityData['end_dt'] = $activityData['end_dt'] ? date('Y-m-d',strtotime($activityData['end_dt'])) : $this->default_date;
				$activityData['est_end_dt'] = isset($activityData['est_end_dt']) ? date('Y-m-d',strtotime($activityData['est_end_dt'])) : $this->default_date;


				$pHierarchyId = $parentData['hierarchy_id'];

				$activityData['level_id_fk'] = NULL;
				$activityData['p_hierarchy_id'] = $pHierarchyId;
				$activityData['status_nm'] = 'Active';

				//Check activity exists or not
				if($activityId =$this->allocation_model->checkActivityExists($activityData)){
					//Update activity
					$activityId = $activityId->hier_act_log_id;
					$response['status'] = $this->allocation_model->updateActivity($activityId, $activityData);
					$response['message'] = "Activity updated successfully.";
				}else{
					$this->allocation_model->storeActivity($activityData);
					$response['status'] = true;
					$response['message'] = "Activity added successfully.";
				}
				$activityData['owner_mid'] = $activityData['alloc_to'];
				// $this->allocationNotified("Act",$activityData);
				$response['data'] = $activityData;
				header('Content-Type: application/json');
				echo json_encode($response);
		} catch (Exception $e) {
				header('Content-Type: application/json');
				echo json_encode($response);
		}

	}


	/*
	  @author Pragyesh
	*/
	public function LoadAllocationOverview(){


		$page=$this->uri->segment(3);
		$dept_id=$this->uri->segment(4);
		$levelid=($this->uri->segment(5))?($this->uri->segment(5)):null;
		$p_herachy_id=($this->uri->segment(6))?($this->uri->segment(6)):null;
		$u_id = $this->session->userdata('user_id');
		$role_id  = $this->session->userdata('role_id');
		$dept_val = $this->allocation_model->fetchPermissionDept($u_id);
		$data["dept_val"]=$dept_val;
		$dept_opt=$dept_val[0]['dept_id'];
		$data['dept_id_nav']=$dept_opt;
		$userdeptid= $this->allocation_model->getUserDept($u_id);
		$data["metrics"] = $this->user_model->fetchMetrics($u_id,$role_id,$dept_opt);
		$data["level_mst"] = $this->allocation_model->GetSideLevel($levelid);

		$overview_data =[];
		$data["detailsbox"] = $this->allocation_model->getdetailsbox($levelid,$p_herachy_id,$dept_id, $u_id);

		$data["deptusers"] = $this->allocation_model->GetDeptUsers($dept_id);
		$fixcode = $this->allocation_model->GetCode($p_herachy_id);
		$data["Code"]=$fixcode['mid_code'];
		$arychk=array_column($dept_val, 'dept_id');
		if(in_array($dept_id,$arychk)){
			$this->load->view('allocations/allocation_overview',$data);
		}else{
			  redirect('user/login_view');
		}
	}

	public function getMidOverview(){
		$response['status']=false;
		$mid = $this->input->post('mid');
		$dept_id=$this->input->post('dept_id');
		//$search_value = $this->input->post('s_value');

		if($data = $this->allocation_model->getMidData($mid,$dept_id)){
			$response['status'] = true;
			$response['data']= $data;
		}
		header('Content-Type: application/json');
		echo json_encode($response);
		exit;
	}
	/*
	  @author Pragyesh
	*/
	public function getparentid(){
			$dept_id=$this->input->post('dept_id');
			$levelid=$this->input->post('levelid');
			$herachy_id=$this->input->post('parentid');
			if($herachy_id)
			{
		$data= $this->allocation_model->getparentid($dept_id,$levelid,$herachy_id);

			if($data!=NULL){
				print_r(json_encode($data));
			}
			}
	}
	/*
	  @author Pragyesh
	*/
	public function SaveAllocationOverview(){
		$response['status'] = false;
		$formData =array();
		$user_id  = $this->session->userdata('user_id');
		parse_str($this->input->post('data'), $formData);
		if($formData['level_id_fk']==2){
			$mid_code=$formData['mid_main_code'].$formData['mid_fix_code'];
		}
		elseif($formData['level_id_fk']==3){
				//$mid_main_code=sprintf("%02d",$formData['mid_main_code']);
				$mid_main_code=str_pad($formData['mid_main_code'], 2, '0', STR_PAD_LEFT);
				$mid_code=$formData['mid_fix_code'].$mid_main_code;
		}else{
			$mid_code=$formData['mid_fix_code'].$formData['mid_main_code'];
		}
		$department = $this->input->post('dept_id');

		$formDataAry['mid_code']=$mid_code;
		$formDataAry['mid_name']=$formData['mid_name'];
		$formDataAry['owner_mid']=isset($formData['owner_mid'])?implode(',',$formData['owner_mid']):NULL;
		$formDataAry['start_dt']=$formData['start_dt'] ? date('Y-m-d',strtotime($formData['start_dt'])) : $this->default_date;
		$formDataAry['end_dt']=$formData['end_dt'] ? date('Y-m-d',strtotime($formData['end_dt'])) : $this->default_date;
		$formDataAry['status_nm']=$formData['status_nm'];
		$formDataAry['hier_desc']=$formData['hier_desc'];
		$formDataAry['level_id_fk']=$formData['level_id_fk'];
		$formDataAry['p_hierarchy_id']=!empty($formData['p_hierarchy_id']) ? $formData['p_hierarchy_id']: NULL;
		$formDataAry['dept_id_fk'] = $department;
		$formDataAry['ins_user'] =$user_id;
		$formDataAry['ins_dt'] =date('Y-m-d H:i:s');

		// ADDITIOANLINFO
		$formDataAry['presenter'] = isset($formData['presenter'])?implode(',',$formData['presenter']):NULL;
		$formDataAry['storyboard_incharge'] = isset($formData['storyboard_incharge'])?implode(',',$formData['storyboard_incharge']):NULL;
		$formDataAry['sign_off'] = isset($formData['sign_off'])?implode(',',$formData['sign_off']):NULL;
		$formDataAry['est_end_dt'] = $formData['est_end_dt'] ? date('Y-m-d',strtotime($formData['est_end_dt'])) : $this->default_date;
		$formDataAry['mandays'] = isset($formData['mandays']) ? $formData['mandays'] : NULL;

		$formDataAry['storyboarded_by'] = isset($formData['storyboarded_by'])?implode(',',$formData['storyboarded_by']):NULL;
		$formDataAry['video_clip_type'] = isset($formData['video_clip_type']) ? $formData['video_clip_type'] : NULL;
		$formDataAry['duration'] = isset($formData['duration']) ? $formData['duration'] : NULL;


		if($this->allocation_model->checkLevelExists($mid_code , $department)){
			$response['message'] = "Level already exists.";
			header('Content-Type: application/json');
			echo json_encode($response);
			exit;
		}

		$result=$this->allocation_model->insert('hierarchy_mst',$formDataAry);
		if($result){
			if (!empty($formData['video_clip_type'])) {
				$this->updateVCDuration(
					$formData['p_hierarchy_id'],
					$formData['video_clip_type']
				);
			}
			$response['status'] = true;
			$response['message'] = "Level added successfully.";
			header('Content-Type: application/json');
			echo json_encode($response);
		}

	}
	/*
	  @author Pragyesh
	*/
	public function EditAllocationOverview(){
		$hierarchy_id=$this->input->post('editid');
		$result=$this->allocation_model->edit($hierarchy_id);
		print_r(json_encode($result));

	}
	/*
	  @author Pragyesh
	*/
	public function UpdateAllocationOverview(){
		$response['status'] =false;
		$formData =array();
		$user_id  = $this->session->userdata('user_id');
		parse_str($this->input->post('data'), $formData);
		$hierarchyid=$formData['hierarchyid'];
		if($formData['level_id_fk']==2){
			$mid_code=$formData['mid_main_code'].$formData['mid_fix_code'];
		}
		elseif($formData['level_id_fk']==3){
			//$mid_main_code=sprintf("%02d",$formData['mid_main_code']);
				 $mid_main_code=str_pad($formData['mid_main_code'], 2, '0', STR_PAD_LEFT);
				$mid_code=$formData['mid_fix_code'].$mid_main_code;
		}else{
			$mid_code=$formData['mid_fix_code'].$formData['mid_main_code'];
		}

		$department = $this->input->post('dept_id');

		$formDataAry['mid_code']=$mid_code;
		$formDataAry['owner_mid']=isset($formData['owner_mid'])?implode(',',$formData['owner_mid']):NULL;
		$formDataAry['mid_name']=$formData['mid_name'];
		$formDataAry['start_dt']=$formData['start_dt'] ? date('Y-m-d',strtotime($formData['start_dt'])) : $this->default_date;
		$formDataAry['end_dt']=$formData['end_dt'] ? date('Y-m-d',strtotime($formData['end_dt'])) : $this->default_date;
		$formDataAry['status_nm']=$formData['status_nm'];
		$formDataAry['hier_desc']=$formData['hier_desc'];
		$formDataAry['level_id_fk']=$formData['level_id_fk'];
		$formDataAry['p_hierarchy_id']=!empty($formData['p_hierarchy_id']) ? $formData['p_hierarchy_id']: NULL;
		$formDataAry['dept_id_fk'] = $department;
		$formDataAry['upd_user'] =$user_id;
		$formDataAry['upd_dt'] =date('Y-m-d H:i:s');

		// ADDITIOANLINFO
		$formDataAry['presenter'] = isset($formData['presenter'])?implode(',',$formData['presenter']):NULL;
		$formDataAry['storyboard_incharge'] = isset($formData['storyboard_incharge'])?implode(',',$formData['storyboard_incharge']):NULL;
		$formDataAry['sign_off'] = isset($formData['sign_off'])?implode(',',$formData['sign_off']):NULL;
		$formDataAry['est_end_dt'] = $formData['est_end_dt'] ? date('Y-m-d',strtotime($formData['est_end_dt'])) : $this->default_date;
		$formDataAry['mandays'] = isset($formData['mandays']) ? $formData['mandays'] : NULL;

		$formDataAry['storyboarded_by'] = isset($formData['storyboarded_by'])?implode(',',$formData['storyboarded_by']):NULL;
		$formDataAry['video_clip_type'] = isset($formData['video_clip_type']) ? $formData['video_clip_type'] : NULL;
		$formDataAry['duration'] = isset($formData['duration']) ? $formData['duration'] : NULL;

		if($this->allocation_model->checkLevelExists($mid_code,$department,$hierarchyid)){
			$response['message'] = "Level already exists.";
			header('Content-Type: application/json');
			echo json_encode($response);
			exit;
		}

		// print_r($formDataAry);die;
		$result=$this->allocation_model->OverViewBoxUpdate($formDataAry,$hierarchyid);
		if($result){
			if (!empty($formData['video_clip_type'])) {
				$this->updateVCDuration(
					$formData['p_hierarchy_id'],
					$formData['video_clip_type']
				);
			}
			$response['status'] = true;
			$response['message'] = "Level updated successfully.";
			header('Content-Type: application/json');
			echo json_encode($response);
			exit;
		}
	}

	public function getDetailsMidPage($levelCode = 0, $deptId = 0, $levelId = 0, $midCode = 0)
	{
		define('CONTENT_ID', '1');
		define('MEDIA_ID', '2');

		if (empty($levelId)) {
			$levelId = '1';
		}

		if (!empty($midCode)) {
			$midCode = $this->allocation_model->getLevel($midCode)[0]['mid_code'];
		}

		$data = array();
		$currentDate = date("Y-m-d");
		$data['mediaCategories'] = array();
		$data['allLevelData'] = $this->allocation_model->getAllLevelData();
		$u_id = $this->session->userdata('user_id');
		$role_id  = $this->session->userdata('role_id');
		$dept_val = $this->allocation_model->fetchPermissionDept($u_id);
		// $dept_val = $this->user_model->fetchdept_data($u_id);
		$data["dept_val"]=$dept_val;
		$dept_opt=$dept_val[0]['dept_id'];
		$data["metrics"] = $this->user_model->fetchMetrics($u_id,$role_id,$dept_opt);

		$data['deptProjects'] = array();
		$data['deptId'] = '';
		$data['currentDate'] = $currentDate;
		$data['users'] = array_merge( $this->getusersForAllocation(CONTENT_ID), $this->getusersForAllocation(MEDIA_ID));
		$data['allocationAjaxUrl'] = base_url() . 'index.php/Allocation/getAllocationDataByMid/' . $levelId . '/' . $midCode;
		$data["level_mst"] = $this->allocation_model->GetSideLevel($levelId);
		$deptIds = array_column($dept_val, 'dept_id');

		// if ($deptId == MEDIA_ID) {
		// 	$data['mediaCategories'] = array();
		// }
		// var_dump($data['mediaCategories']); exit;
		// if (in_array($deptId, $deptIds)) {
			$this->load->view("allocations/allocation-mid-overview.php",$data);
		// } else {
		// 	  redirect('user/login_view');
		// }
	}

	public function getActivityLogData($levelCode = 0, $deptId = 0, $levelId = 0, $hierarchyId = 0)
	{
		define('CONTENT_ID', '1');
		define('MEDIA_ID', '2');

		if (empty($levelId)) {
			$levelId = '1';
		}

		if (!empty($midCode)) {
			$midCode = $this->allocation_model->getLevel($midCode)[0]['mid_code'];
		}

		$data = array();
		$currentDate = date("Y-m-d");
		$data['mediaCategories'] = array();
		$data['allLevelData'] = $this->allocation_model->getAllLevelData();
		$u_id = $this->session->userdata('user_id');
		$role_id  = $this->session->userdata('role_id');
		$dept_val = $this->allocation_model->fetchPermissionDept($u_id);
		// $dept_val = $this->user_model->fetchdept_data($u_id);
		$data["dept_val"]=$dept_val;
		$dept_opt=$dept_val[0]['dept_id'];
		$data["metrics"] = $this->user_model->fetchMetrics($u_id,$role_id,$dept_opt);

		$data['deptProjects'] = array();
		$data['deptId'] = '';
		$data['currentDate'] = $currentDate;
		$data['users'] = array_merge( $this->getusersForAllocation(CONTENT_ID), $this->getusersForAllocation(MEDIA_ID));
		$data['allocationAjaxUrl'] = base_url() . 'index.php/Allocation/getMidWiseActivity/' . $levelCode . '/' . $deptId .'/'. $levelId . '/' . $hierarchyId;
		$data["level_mst"] = $this->allocation_model->GetSideLevel($levelId);
		$deptIds = array_column($dept_val, 'dept_id');

		$this->load->view("allocations/allocation-activity-log.php",$data);
	}
	
	public function getAllocationDeepChildsData($levelId = 0, $hierarchyId = 0)
	{
		set_time_limit(1000);
		ini_set('memory_limit', '-1');

		// var_dump("Archito Testing"); exit;

		// if ($levelId != 0) {
		// 		$hierarchyIdQuery = "p_hierarchy_id";
		// }
		// if ($levelId != 0 && $hierarchyId != 0) {
		// 	$hierarchyIdQuery = "NULL AS p_hierarchy_id";
		// }
		$userId = $this->session->userdata('user_id');
		// var_dump($userId, $deptId, $levelId, $hierarchyId); exit;
		// if ($hierarchyId == 0) {
				$subQuery = "1 = 1";
				$subQuery = "hierarchy_id = $hierarchyId";
		// } else {
		// 		$subQuery = "hierarchy_id = $hierarchyId";
		// }
		// $parentIdsQuery = '';
		// $levelIdQuery = '';
		// $hierarchyIdQuery = '';
		//
		// if ($parentIds != "0") {
		// 	$subQuery = "p_hierarchy_id IN ($parentIds)";
		// 	$parentIdsQuery = " AND H.p_hierarchy_id IN ($parentIds)";
		// 	$levelIdQuery = '';
		// } else {
		// 	$levelIdQuery = "AND H.level_id_fk IN ($levelId)";
		// }
		//
		// // $levelIdQuery = "AND H.level_id_fk >= $levelId";
		// if ($hierarchyId != "0" && $parentIds == "0") {
		// 	$hierarchyIdQuery = "AND H.p_hierarchy_id IN ($hierarchyId)";
		// }
		$query = "SELECT DISTINCT
				H.hierarchy_id,
				H.mid_code,
				H.mid_name,
				(SELECT mid_code from hierarchy_mst AS hm where hm.hierarchy_id = H.p_hierarchy_id) as parent_mid,
				H.owner_mid,
				H.status_nm,
				H.progress,
				H.start_dt,
				H.end_dt,
				LMS.level_code
		FROM
				(
				SELECT
						hierarchy_id,
						mid_code,
						mid_name,
						owner_mid,
						status_nm,
						progress,
						start_dt,
						end_dt,
						p_hierarchy_id,
						level_id_fk,
						CASE WHEN ".$subQuery." THEN @idlist := CONCAT(hierarchy_id) WHEN FIND_IN_SET(p_hierarchy_id, @idlist) THEN @idlist := CONCAT(@idlist, ',', hierarchy_id)
		END AS checkId
		FROM
				hierarchy_mst
		WHERE level_id_fk<=(".$levelId."+5)
		ORDER BY
				hierarchy_id ASC
		) AS H
		LEFT JOIN level_mst LMS on LMS.level_id = H.level_id_fk
		INNER JOIN(
				SELECT HP.level_id_fk,
						HP.mid_code,
						L.length_limit,
						L.level_code
				FROM
						user_permissions HP
				LEFT JOIN level_mst L ON
						L.level_id = HP.level_id_fk
				WHERE
						HP.user_id_fk = ".$userId."  AND HP.`status_nm` = 'Active'
		) P
		ON
				(
						IFNULL(P.mid_code, 0) = 0 OR(
								H.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0
						) OR(
								H.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(
										SUBSTRING(H.mid_code, 1, P.length_limit),
										P.mid_code
								) > 0
						)
				)
		WHERE
				checkId IS NOT NULL GROUP BY H.hierarchy_id";

		$sqlQuery = $this->db->query($query);
		$levelData = $sqlQuery->result_array();

		$query = "SELECT
    AH.hier_act_log_id AS hierarchy_id,
	CONCAT_WS(
        '_',
        P.project_name,
        T.task_name,
        S.sub_name,
        J.job_name
    ) AS mid_code,
    CONCAT_WS(
        '_',
        P.project_name,
        T.task_name,
        S.sub_name,
        J.job_name
    ) AS mid_name,
	(SELECT mid_code from hierarchy_mst where hierarchy_id = AH.p_hierarchy_id) as parent_mid,
	AH.alloc_to AS owner_mid,
	AH.status_nm,
	AH.progress,
	AH.start_dt,
	AH.end_dt,
	NULL AS level_code
FROM
    (
    SELECT DISTINCT
        H.hierarchy_id,
        (
            CASE WHEN P.`level_id_fk` <= H.level_id_fk THEN 1 ELSE 0
        END
) AS show_p
FROM
    (
    SELECT
        hierarchy_id,
        level_id_fk,
        mid_code,
        CASE WHEN ".$subQuery." THEN @idlist := CONCAT(hierarchy_id) WHEN FIND_IN_SET(p_hierarchy_id, @idlist) THEN @idlist := CONCAT(@idlist, ',', hierarchy_id)
END AS checkId
FROM
    hierarchy_mst
ORDER BY
    hierarchy_id ASC
) AS H
INNER JOIN(
    SELECT HP.level_id_fk,
        HP.mid_code,
        L.length_limit,
		project_id_fk 
    FROM
        user_permissions HP
    LEFT JOIN level_mst L ON
        L.level_id = HP.level_id_fk
    WHERE
        HP.user_id_fk = '1' AND HP.`status_nm` = 'Active'
) P
ON
    (
        IFNULL(P.mid_code, 0) = 0 OR(
            H.level_id_fk <= P.`level_id_fk` AND INSTR(P.mid_code, H.mid_code) > 0
        ) OR(
            H.level_id_fk > P.`level_id_fk` AND FIND_IN_SET(
                SUBSTRING(H.mid_code, 1, P.length_limit),
                P.mid_code
            ) > 0
        )
    )
WHERE
    checkId IS NOT NULL
) FI
INNER JOIN hier_act_log AH ON
    AH.p_hierarchy_id = FI.hierarchy_id AND FI.show_p = 1 AND AH.status_nm = 'Active' 
	AND (AH.project_id_fk= FI.project_id_fk OR IFNULL(FI.project_id_fk,0)=0) 
LEFT JOIN project_mst P ON
    AH.project_id_fk = P.project_id
LEFT JOIN task_mst T ON
    AH.task_id_fk = T.task_id
LEFT JOIN sub_mst S ON
    AH.sub_task_id_fk = S.sub_id
LEFT JOIN job_mst J ON
    AH.job_id_fk = J.job_id";

	$sqlQuery = $this->db->query($query);
	$activityData = $sqlQuery->result_array();

	header('Content-Type: application/json');
	echo json_encode(array(
		'success' => true,
		'message' => '',
		'data' => array_merge($levelData, $activityData)
	));

	}

	public function getSummaryPage($levelCode = 0, $deptId = 0, $levelId = 0, $hierarchyId = 0)
	{
		// var_dump($levelId); exit;
		$data = array();
		$data["level_mst"] = $this->allocation_model->GetSideLevel($levelId);
		$u_id = $this->session->userdata('user_id');
		$role_id  = $this->session->userdata('role_id');
		$dept_val = $this->allocation_model->fetchPermissionDept($u_id);
		// $dept_val = $this->user_model->fetchdept_data($u_id);
		$data["dept_val"]=$dept_val;
		$dept_opt=$dept_val[0]['dept_id'];
		$data["metrics"] = $this->user_model->fetchMetrics($u_id,$role_id,$dept_opt);
		$this->load->view("allocations/allocation-summary.php",$data);
		// var_dump("Archito Testing"); exit;
	}

	public function getSummaryResourcePage($levelCode = 0, $deptId = 0, $levelId = 0, $hierarchyId = 0)
	{
		// var_dump($levelId); exit;
		$data = array();
		$data["level_mst"] = $this->allocation_model->GetSideLevel($levelId);
		$data['usersList'] = $this->getusersForAllocation($deptId);

		$u_id = $this->session->userdata('user_id');
		$role_id  = $this->session->userdata('role_id');
		$dept_val = $this->allocation_model->fetchPermissionDept($u_id);
		// $dept_val = $this->user_model->fetchdept_data($u_id);
		$data["dept_val"]=$dept_val;
		$dept_opt=$dept_val[0]['dept_id'];
		$data["metrics"] = $this->user_model->fetchMetrics($u_id,$role_id,$dept_opt);
		$this->load->view("allocations/allocation-summary-resource.php",$data);
		// var_dump("Archito Testing"); exit;
	}

	public function getSummaryResourcePageData()
	{
		header('Content-Type: application/json');

		$data = $this->input->get('data');
		$userIds = $data['userId'];

		if (empty($userIds)) {
			echo json_encode(array(
				'success' => false,
				'message' => 'Please select users',
				'data' => ''
			));
			exit;
		}
		$curr_Dt_op=date("Y-m-d");
		$startDate = !empty($data['startDate']) ? date("Y-m-d",strtotime($data['startDate'])) : date("Y-m-d",strtotime('-1 week'));
		$endDate = !empty($data['endDate']) ? date("Y-m-d",strtotime($data['endDate'])) : date("Y-m-d",strtotime('+1 week', strtotime(date('Y-m-d'))));
//print_r($startDate);
		$query = 'SELECT
					    T.has_child, T.open, T.full_name, T.id, T.parent, T.type, T.start_date, T.end_date, T.progress, T.text, T.color, T.progressColor, T.status, T.actual_start_date, T.actual_end_date
					FROM
					    (
					    SELECT
							1 as has_child,
							1 as open,
							"project" as type,
					        U.user_id,
					        U.full_name,
					        U.full_name AS text,
							DATE_FORMAT(MIN(M.start_dt),"%d-%m-%Y") AS start_date,
							DATE_FORMAT(MAX(M.end_dt),"%d-%m-%Y") AS end_date,
					        FORMAT(
					            IFNULL(
					                DATEDIFF(
					                    "'.$curr_Dt_op.'", DATE_FORMAT(MIN(M.start_dt),
					                    "%Y-%m-%d")),
					                    0
					                ) / IFNULL(
					                    DATEDIFF(
					                        DATE_FORMAT(MAX(M.end_dt),
					                        "%Y-%m-%d"),
					                        DATE_FORMAT(MIN(M.start_dt),
					                        "%Y-%m-%d")
					                    ),
					                    1
					                ),
					                1
					            ) AS progress,
								0 AS oop,
					            CONCAT("p", U.user_id) AS id,
					            NULL AS parent,
								"" as color,
								"" as progressColor,
								"" as status,
								"" as actual_start_date,
								"" as actual_end_date
					        FROM
					            (SELECT hier_act_log_id_fk,alloc_user FROM hier_user_alloc
									WHERE alloc_user IN ('.implode(',',$userIds).'  )
								AND status_nm="Active"
								)HU
								INNER JOIN hier_act_log M
								ON M.hier_act_log_id=HU.hier_act_log_id_fk
								 AND ( M.start_dt BETWEEN "'.$startDate.'"  AND "'.$endDate.'"
									OR M.end_dt BETWEEN "'.$startDate.'"  AND "'.$endDate.'"  )
                            AND M.status_nm="Active"
								LEFT JOIN user_mst U ON
						   U.user_id = HU.alloc_user
					        GROUP BY
					            U.user_id
					        UNION
					    SELECT
							null as has_child,
							null as open,
							"task" as type,
					        U.user_id,
					        U.full_name,
					        CONCAT_WS(
					            "_",
					            P.project_name,
					            T.task_name,
					            S.sub_name,
					            J.job_name
					        ) AS text,
							DATE_FORMAT(M.start_dt,"%d-%m-%Y") AS start_date,
							DATE_FORMAT(M.end_dt,"%d-%m-%Y") AS end_date,
					        FORMAT(
					            IFNULL(
					                DATEDIFF(
					                    "'.$curr_Dt_op.'", DATE_FORMAT(M.start_dt, "%Y-%m-%d")),
					                    0
					                ) / IFNULL(
					                    DATEDIFF(
					                        DATE_FORMAT(M.end_dt, "%Y-%m-%d"),
					                        DATE_FORMAT(M.start_dt, "%Y-%m-%d")
					                    ),
					                    1
					                ),
					                1
					            ) AS progress,
								@i:=@i+1,
								  @i as id,
					            CONCAT("p", U.user_id) AS parent,
								IF(progress = "Not Started", "rgba(128, 139, 150, 0.8)", IF(progress = "In Progress", "rgba(52, 152, 219, 0.8)", IF(progress = "On Hold", "rgba(217,83,79, 0.8)", IF(progress = "Completed", "rgba(212, 172, 13, 0.8)", "rgba(128, 139, 150, 0.8)")))) as color,
								IF(progress = "Not Started", "rgba(128, 139, 150)", IF(progress = "In Progress", "rgba(52, 152, 219)", IF(progress = "On Hold", "rgba(217,83,79)", IF(progress = "Completed", "rgba(212, 172, 13)", "rgba(128, 139, 150)")))) as progressColor,
								progress as status,
								actual_start_date,
								actual_end_date
					        FROM
					            (SELECT hier_act_log_id_fk,alloc_user FROM hier_user_alloc
									WHERE alloc_user IN ('.implode(',',$userIds).'  )
								AND status_nm="Active"
								)HU
								INNER JOIN hier_act_log M
								ON M.hier_act_log_id=HU.hier_act_log_id_fk
								AND ( M.start_dt BETWEEN "'.$startDate.'"  AND "'.$endDate.'"
									OR M.end_dt BETWEEN "'.$startDate.'"  AND "'.$endDate.'"  )
                            AND M.status_nm="Active"
					   LEFT JOIN user_mst U ON
						   U.user_id = HU.alloc_user
					        LEFT JOIN project_mst P ON
					            M.project_id_fk = P.project_id
					        LEFT JOIN task_mst T ON
					            M.task_id_fk = T.task_id
					        LEFT JOIN sub_mst S ON
					            M.sub_task_id_fk = S.sub_id
					        LEFT JOIN job_mst J ON
					            M.job_id_fk = J.job_id
							LEFT JOIN (SELECT @i:=1)O
			  					ON 1=1
					        ) T
					    ';

		$sqlQuery = $this->db->query($query);
		// var_dump($this->db->last_query());
		$data = $sqlQuery->result_array();

		echo json_encode(array(
			'success' => true,
			'message' => '',
			'data' => $data
		));

	}

	public function getSummaryPageData($levelCode = 0, $levelId = 0, $hierarchyId = 0, $isParent = false)
	{
		$allLevelData = [];
		$levelData = $this->allocation_model->getSummaryLevelsData($levelCode, $levelId, $hierarchyId, $isParent);
		foreach ($levelData as $key => $data) {
			$data['$has_child'] = '1';
				$allLevelData[] = $data;
		}
		
		$activityData = array();
		$activityData = $this->allocation_model->getSummaryActivityData($levelCode, $levelId, $hierarchyId);


		$allHierarchyData = array_merge($allLevelData, $activityData);

		header('Content-Type: application/json');
	    echo json_encode(array('data' => $allHierarchyData, 'links' => []) );
	}

	public function updateVCDuration($topicId, $videoType)
	{
		$chapterId = $this->allocation_model->getTopicChpater($topicId)[0]['p_hierarchy_id'];

		$durationSummedData = $this->allocation_model->getVideoClipSummationByChapter(
			$chapterId,
			$videoType
		);
		if (empty($durationSummedData)) {
			return false;
		}
		$durationSummedData[0]['chapter_id_fk'] = $chapterId;

		$durationInfoData = $this->allocation_model->getChapterDurationInfoByVideoType(
			$chapterId,
			$videoType
		);

		if (!empty($durationInfoData)) { // Edit record
			$this->allocation_model->updateChapterDurationInfo(
				$durationInfoData[0]['id'],
				$durationSummedData[0]
			);
		} else {						// Add record
			$this->allocation_model->insertChapterDurationInfo(
				$durationSummedData[0]
			);
		}
	}

	public function updateChapterDurationInfo($id)
	{
		$durationData = $this->input->post('data');
		$this->allocation_model->updateChapterDurationInfo(
			$id,
			$durationData
		);
	}

	public function getChapterDurationGrid($chapterId)
	{
		$chapterDurations = $this->allocation_model->getChapterDurationInfoByChapterId($chapterId);
		$data['chapterDurations'] = $chapterDurations;
		$this->load->view("allocations/chapter-duration-grid.php", $data);
	}

	public function udpateLevelAllocation($id, $allocatedTo)
	{
		$data['alloc_to'] = implode(',', $allocatedTo);
		$this->allocation_model->update('hierarchy_mst', $data, $id);

	}

	public function updateActivityAllocation($id, $allocatedTo)
	{
		$data['alloc_to'] = implode(',', $allocatedTo);
		$this->allocation_model->updateActivity($id, $data);
	}

	public function updateLevelAllocation($id, $allocatedTo)
	{
		$data['owner_mid'] = implode(',', $allocatedTo);
		$this->allocation_model->updateLevel($id, $data);
	}

	public function bulkAllocate()
	{
		try {
			$updatedDataSet = [];
			$userList = $this->input->post('userList');
			$allocationData = $this->input->post('allocationData');
			foreach ($allocationData as $key => $value) {
				$allocatedTo = [];
				$owners = '';
				if (!empty($value['level_id_fk'])) {
					$owners = $this->allocation_model->getLevel($value['hierarchy_id'])[0]['owner_mid'];
					$allocatedTo = array_filter(array_unique(array_merge(explode(',', $owners), $userList)));
					$this->updateLevelAllocation($value['hierarchy_id'], $allocatedTo); // Update in hierarchy_mst for levels
					$updatedDataSet[] = ['id' => $value['hierarchy_id'], 'userList' => $allocatedTo, 'dataSet' => $value ];
				} else {
					$owners = $this->allocation_model->getActivity(explode('-', $value['hierarchy_id'])[1])[0]['alloc_to'];
					$allocatedTo = array_filter(array_unique(array_merge(explode(',', $owners), $userList)));
					$this->updateActivityAllocation(explode('-', $value['hierarchy_id'])[1], $allocatedTo); // upadte in hier_act_log for activity
					$updatedDataSet[] = ['id' => $value['hierarchy_id'], 'userList' => $allocatedTo, 'dataSet' => $value ];
				}
			}
			header('Content-Type: application/json');
	    	echo json_encode(array('success' => true, 'message' => '', 'data' => $updatedDataSet));
		} catch(\Exception $e) {
			header('Content-Type: application/json');
	    	echo json_encode(array('success' => false, 'message' => $e->getMessage()));
		}

	}
	//Update level attributes based on mid
	public function updateLevelCSV(){
		$response['status'] = false;
		if(isset($_FILES['level_file'])){
			$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
			$maxsize    = 2097152;
			$department = $this->input->post('department');

			if(!in_array($_FILES['level_file']['type'],$mimes)){
			  header('Content-Type: application/json');
	        	echo json_encode(array(
	    			'status' => false,
	    			'message' => 'Only csv file allowed.'
	    		));
	            exit;
			}

			$file = $_FILES['level_file']['tmp_name']; //path to csv file
			$fileHandler = fopen($file, "r");
			/*array_walk($csvData, function(&$v) { $v['dept_id_fk'] = 'f'; });*/
			//Check the header of an uploaded file
			$this->isHeaderCorrect($fileHandler,"level");

			fclose($fileHandler);
    		$csvData = $this->csvreader->parse_file($file); //path to csv file

    		$message = "";
    		$csvInsData = [];
		   	foreach ($csvData as $key => $value) {
		   		$mid = preg_replace('/\s+/', '', $value['MID']);
		   		// $getLevelData = $this->generateMidLevel($mid,$levelList,$csvData);

		   		$csvInsData[$key]['dept_id_fk'] = $department;
		   		$csvInsData[$key]['mid_code'] = $mid;
		   		$csvInsData[$key]['mid_name'] = $csvData[$key]['NAME'] ? $csvData[$key]['NAME'] : NULL;
		   		// $csvInsData[$key]['level_id_fk'] = $getLevelData['level'];
		   		// $csvInsData[$key]['p_hierarchy_id'] = $getLevelData['p_level'];
		   		$csvInsData[$key]['hier_desc'] = $csvData[$key]['DESCRIPTION'] ? $csvData[$key]['DESCRIPTION'] : NULL;
		   		$csvInsData[$key]['start_dt'] = $csvData[$key]['START DATE'] ? date("Y-m-d",strtotime($csvData[$key]['START DATE'])) : NULL;
		   		$csvInsData[$key]['end_dt'] = $csvData[$key]['END DATE'] ? date("Y-m-d",strtotime($csvData[$key]['END DATE'])) : NULL;
		   		$csvInsData[$key]['est_end_dt'] = $csvData[$key]['ESTIMATED END DATE'] ? date("Y-m-d",strtotime($csvData[$key]['ESTIMATED END DATE'])) : NULL;
		   		$csvInsData[$key]['progress'] = $csvData[$key]['PROGRESS'] ? $csvData[$key]['PROGRESS'] : NULL;
		   		$csvInsData[$key]['status_nm'] = $csvData[$key]['STATUS'] ? $csvData[$key]['STATUS'] : "InActive";
		   		$csvInsData[$key]['mandays'] = $csvData[$key]['MANDAYS'] ? $csvData[$key]['MANDAYS'] : NULL;
		   		$csvInsData[$key]['video_clip_type'] = $csvData[$key]['VIDEO CLIP TYPE'] ? $csvData[$key]['VIDEO CLIP TYPE'] : NULL;
		   		$csvInsData[$key]['duration'] = $csvData[$key]['DURATION'] ? $csvData[$key]['DURATION'] : NULL;
		   		$csvInsData[$key]['owner_mid_email'] = $csvData[$key]['OWNER'];
		   		$csvInsData[$key]['presenter_email'] = $csvData[$key]['PRESENTER'];
		   		$csvInsData[$key]['storyboard_incharge_email'] = $csvData[$key]['STORYBOARD INCHARGE'];
		   		$csvInsData[$key]['sign_off_email'] = $csvData[$key]['SIGN OFF'];
		   		$csvInsData[$key]['storyboarded_by_email'] = $csvData[$key]['STORYBOARD BY'];
		   		$csvInsData[$key]['upd_user'] = $this->session->userdata('user_id');
		   		$csvInsData[$key]['upd_dt'] = date("Y-m-d H:i:s");
		   	}

		   	$u_id = $this->session->userdata('user_id');
		   	$updateAttributes = $this->allocation_model->updateLevelAttribute($csvInsData,$u_id,$department);
		   	if($updateAttributes && $updateAttributes['status']){
				$response['status'] = true;
				$response['errors'] = $updateAttributes['errors'];
				$message = (isset($response['errors']) && count((array)$response['errors']) > 0) ? "Attributes updated partially.Some errors occurs please check error excel for that." : "Attributes updated successfully.";
				$response['message'] = $message;
			}else{
				$response['message'] = "Attributes updation already in use.Please try after some time.";
			}
		}
		echo json_encode($response);
		exit;
	}



	//Allocate activities to users
	public function activitiesAllocate(){

	 // $config['upload_path']="./uploads/";
        // $config['allowed_types']='csv';
		 // $this->load->library('upload',$config);
		 // if($this->upload->do_upload("activity_file")){
	        // $data1 = array('upload_data' => $this->upload->data());
	        // $filename=$data1['upload_data']['file_path']."/".$data1['upload_data']['file_name'];
			// }
			
		$response['status'] = false;
		if(isset($_FILES['activity_file'])){
			$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
			$maxsize    = 2097152;
			$department = $this->input->post('department');
			/*if(($_FILES['activity_file']['size'] >= $maxsize) || ($_FILES["activity_file"]["size"] == 0)) {
				echo json_encode(array(
	    			'status' => false,
	    			'message' => 'File too large. File must be less than 2 megabytes.'
	    		));
	            exit;
		    }*/

			if(!in_array($_FILES['activity_file']['type'],$mimes)){
			  header('Content-Type: application/json');
	        	echo json_encode(array(
	    			'status' => false,
	    			'message' => 'Only csv file allowed.'
	    		));
	            exit;
			}

			$this->db->trans_start();

			$file = $_FILES['activity_file']['tmp_name']; //path to csv file
			
			
			
			$fileHandler = fopen($file, "r");

			//Check the header of an uploaded file
			$this->isHeaderCorrect($fileHandler,"actvity");
			fclose($fileHandler);
			$department = $this->input->post('department');

			$csvData = $this->csvreader->parse_file($file);
			$csvUpData = array_map(function($csv) use($department) {
			    return array(
			        'project_name' 	=> $csv['PROJECT'],
			        'task_name'		=> $csv['TASK'],
			        'sub_name'		=> $csv['SUB TASK'],
			        'job_name'		=> $csv['JOB'],
			        'start_dt'		=> $csv['START DATE'] ? date("Y-m-d",strtotime($csv['START DATE'])) : NULL,
			        'end_dt'		=> $csv['END DATE'] ? date("Y-m-d",strtotime($csv['END DATE'])) : NULL,
			        'est_end_dt'	=> $csv['ESTIMATED END DATE'] ? date("Y-m-d",strtotime($csv['ESTIMATED END DATE'])) : NULL,
			        'allocate_email'=> preg_replace('/\s/', '',$csv['ALLOCATED TO']),
			        'progress'		=> $csv['PROGRESS'],
			        'status'		=> (strtolower($csv['STATUS']) == "inactive")? "InActive" : "Active",
			        'dept_id_fk' 	=> $department,
			        'mid_code' 		=> $csv['MID'],
			        'task_type'		=> $csv['TASK TYPE'] ? $csv['TASK TYPE'] : NULL
			    );
			}, $csvData);

			$u_id = $this->session->userdata('user_id');
			//Update allocate data
			$allocateActivity = $this->allocation_model->allocateActivity($csvUpData,$u_id);

			if($allocateActivity && $allocateActivity['status']){
				$response['status'] = true;
				$response['errors'] = $allocateActivity['errors'];
				$message = (isset($response['errors']) && count((array)$response['errors']) > 0) ? "Activity updated partially.Some errors occurs please check error excel for that." : "Activity updated successfully.";
				$response['message'] = $message;
			}else{
				$response['message'] = "Activity updation already in use.Please try after some time.";
			}
			$this->db->trans_complete();
		}
		echo json_encode($response);
		exit;
	}

	//Check the header of an excel file
	private function isHeaderCorrect($fileHandler,$type){
		$headerList = $type === "level" ? $this->levelImportHeaderList : $this->activityUpHeaderList;
		$fileHeader = fgetcsv($fileHandler);

		$diffHeader = array_diff($headerList, $fileHeader);
		if (count($diffHeader) > 0) {
            header('Content-Type: application/json');
        	echo json_encode(array(
    			'status' => false,
    			'message' => 'Columns in the header might be wrong or some columns might be missing'
    		));
            exit;
        }
	}




	private function generateMidLevel($mid,$level,$csvData,$validate = false){
		$res = ['level' => NULL , 'p_level' => NULL];
		$message = NULL;
		switch (strlen($mid)) {
			case 4:
				if($validate){
					if(ctype_alpha($mid) === false){
						$message = $mid . " Must be 4 Characters for Syllabus level.";
					}
				}else{
					$res['level'] = isset($level['SY']) ? $level['SY'] : NULL;
					$res['p_level'] = NULL;
				}
				break;
			case 6:
				if($validate){
					if(!is_numeric(substr($mid, 0,2))){
						$message = $mid . " Must be 2 Digits for Year level.";
					}
				}else{
					$res['level'] = isset($level['YE']) ? $level['YE'] : NULL;
					$res['p_level'] = $this->getHierarchyLevelId(substr($mid,2),$csvData);
				}
				break;

			case 8:
				if($validate){
					if (!is_numeric(substr($mid, 6,2)) ){
						$message = $mid . " Must be 2 digits for Grade level.";
					}
				}else{
					$res['level'] = isset($level['GR']) ? $level['GR'] : NULL;
					$res['p_level'] = $this->getHierarchyLevelId(substr($mid,0,6),$csvData);
				}
				break;

			case 11:
				if($validate){
					if (ctype_alpha(substr($mid, 8,3)) === false) {
						$message = $mid . " Must be 3 Characters for Subject level.";
					}
				}else{
					$res['level'] = isset($level['SU']) ? $level['SU'] : NULL;
					$res['p_level'] = $this->getHierarchyLevelId(substr($mid,0,8),$csvData);
				}
				break;

			case 13:
				if($validate){
					if(!is_numeric(substr($mid, 11,2))){
						$message = $mid . " Must be 2 digits for Chapter level.";
					}
				}else{
					$res['level'] = isset($level['CH']) ? $level['CH'] : NULL;
					$res['p_level'] = $this->getHierarchyLevelId(substr($mid,0,11),$csvData);
				}
				break;

			case 18:
				if($validate){
					if(!is_numeric(substr($mid, 13,5))){
						$message = $mid . " Must be 5 digits for SubTopic level.";
					}
				}else{
					$res['level'] = isset($level['ST']) ? $level['ST'] : NULL;
					$res['p_level'] = $this->getHierarchyLevelId(substr($mid,0,13),$csvData);
				}
				break;

			case 23:
				if($validate){
					if(!is_numeric(substr($mid, 18,5))){
						$message = $mid . " Must be 5 digits for Video Clip level.";
					}
				}else{
					$res['level'] = isset($level['VC']) ? $level['VC'] : NULL;
					$res['p_level'] = $this->getHierarchyLevelId(substr($mid,0,18),$csvData);
				}
				break;

			case 28:
				if($validate){
					if(!is_numeric(substr($mid, 23,5))){
						$message = $mid . " Must be 5 digits for Sec Part level.";
					}
				}else{
					$res['level'] = isset($level['SP']) ? $level['SP'] : NULL;
					$res['p_level'] = $this->getHierarchyLevelId(substr($mid,0,23),$csvData);
				}
				break;
			default:
				$message = $mid . "Invalid MID.";
				break;
		}

		return $validate ? $message : $res;
	}

	private function getHierarchyLevelId($mid,$csvData){
		$midMatchArray = array_values(array_column(array_filter($csvData, function($var) use ($mid){
		    return ($var['mid'] === $mid);
		}), 'hierarchy_id')) ;
		$hier_id = NULL;
		if($midMatchArray){
			$hier_id = $midMatchArray[0];
		}else{
			$hier_id = $this->allocation_model->getParentHierachyId($mid);
		}
		return $hier_id;
	}

	public function getLevel($hierarchyId)
	{
		if (strpos($hierarchyId, 'A-') !== false) {
			$hierarchyId = str_replace("A-","", $hierarchyId);
			echo json_encode($this->allocation_model->getActivity($hierarchyId)[0]);
			exit;
		}
		echo json_encode($this->allocation_model->getLevel($hierarchyId)[0]);
		exit;
	}

	public function bulkAllocationImport()
	{
		$importTypes = $this->input->post('importTypes');
		$hierarchyIds = array_column(
			array_filter(
				$this->input->post('allocationData'),
				function($val, $key) {
					return !empty($val['level_id_fk']);
				},
				ARRAY_FILTER_USE_BOTH
			),
			'hierarchy_id'
		);

		$activityIds = array_map(function($val) {return str_replace("A-","", $val); }, array_column(
			array_filter(
				$this->input->post('allocationData'),
				function($val, $key) {
					return empty($val['level_id_fk']);
				},
				ARRAY_FILTER_USE_BOTH
			),
			'hierarchy_id'
		));
		$importedData = [];

		if (in_array('level', $importTypes)) {
			$importedData['level'] = $this->allocation_model->getMidAlongWithChilds($hierarchyIds);
		}

		if (in_array('activity', $importTypes) || !empty($activityIds)) {
			$dataByPhierId = $dataByActivityId = [];
			if (!empty($hierarchyIds)) {
				$dataByPhierId = $this->allocation_model->getActivitybyPhierIds($hierarchyIds);
			}
			if (!empty($activityIds)) {
				$dataByActivityId = $this->allocation_model->getActivitybyActivityIds($activityIds);
			}

			$activityData = array_merge($dataByPhierId, $dataByActivityId);
			$importedData['activity'] = $activityData;
		}

		header('Content-Type: application/json');
		echo json_encode(array('data' => $importedData) );
	}
}