<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
    {       
		
        parent::__construct();
        $this->load->helper(['url','cookie']);
        $this->load->model('user_model');
		$this->load->model('feedback_model');
		$this->load->model('alert_model');
		$this->load->model('allocation_model');
        $this->load->library('session');
		//print_r($time_ch);

$listWhereToShowTimeSpanFilters = array(
			'Team_Member_Info', 
			'Graph_Breakdown', 
			'Graph_Drilldown',
			'Team_Graphs',
			'Team_Split_Graph',
			'Employee_Info',
			//'Graph_Trends',
			'NonQ_Share',
			'Graph_Summary',
			'Graph_Sum_Sc',
			'Emp_io_upload',
			'Employee_Leave',
			'Reports'
		);

		// Getting start date and end date if exist in the url
		$this->startDateFilter = $this->input->get('start_date');
		$this->endDateFilter = $this->input->get('end_date');
		$this->timeSpanType = $this->input->get('time_span');
		$fileName = $this->input->get('a');
		$this->useDateFilters = false;
		if (in_array($fileName, $listWhereToShowTimeSpanFilters) 
		&& !empty($this->startDateFilter) && !empty($this->endDateFilter)) {
			$this->useDateFilters = true;
		}		
    }
	
    public function index()
    {
		$contents['login_url'] = $this->googleplus->loginURL();		
        $this->load->view("Login.php",$contents);
    }
    
    function login_user()
    {		
		if (isset($_GET['code'])) {		
			$this->googleplus->getAuthenticate();
			$u_info=$this->googleplus->getUserInfo();	
			$this->session->set_userdata('token',$this->googleplus->getAccessToken());
            $user_login = array(
                'user_email' => $u_info['email']
            );   
			$hd_val='';
			$user_dt='';
			if(array_key_exists('hd',$u_info))
			{
				$hd_val=$u_info['hd'];	
			}
			
			$user_dt_chk = $this->user_model->login_suser_check($user_login['user_email']);            
			if($user_dt_chk)
			{
				redirect('https://www.dwastaging.xyz/dwa_test/index.php/User/login_change?token='.$this->googleplus->getAccessToken().'&email='. $u_info['email']);
				
			}else{
			if($hd_val=="byjus.com")
			{
            $user_dt = $this->user_model->login_suser($user_login['user_email']);            
            if ($user_dt) {
                foreach ($user_dt as $row) {
					$acc='';
                    $this->session->set_userdata('user_id', $row['user_id']);
                    $this->session->set_userdata('user_email', $row['user_login_name']);
                    $this->session->set_userdata('user_name', $row['full_name']);
					$this->session->set_userdata('role_id', $row['role_id_fk']);
					$this->session->set_userdata('is_grading', $row['is_grad']);
					$acc=$row['access_name_mod'];
					$day_chk= date('Y-m-d');
					//print_r($day_chk);
					$this->session->set_userdata('forms_list','');
					$this->session->set_userdata('forms_list_q','');
					$uids_sec=$this->session->userdata('user_id');
					$chek_form_access=$this->user_model->fetchformAccess($uids_sec,'Yearly');
					$chek_qr_access=$this->user_model->fetchQRAccess($uids_sec,'Quarterly');
					if($chek_form_access[0]['accss'])
					{						
							$acc="Yearly_Form|".str_replace(",","|",$chek_form_access[0]['accss'])."|".$acc;
							$this->session->set_userdata('forms_list',$chek_form_access[0]['accss']);		
					}
					if($chek_qr_access[0]['accss'])
					{						
							$acc="Quarterly_Form|".str_replace(",","|",$chek_qr_access[0]['accss'])."|".$acc;
							$this->session->set_userdata('forms_list_q',$chek_qr_access[0]['accss']);		
					}
					//print_r($acc);
					$usd=$this->session->userdata('user_id');
					 $us_acc_arry=array(1,79);
					if(in_array($usd,$us_acc_arry))
					  {
						if($chek_form_access[0]['accss'])
						{
							$acc="self_response|".$acc;	
						}
						if($chek_qr_access[0]['accss'])
						{
							$acc="self_response_q|".$acc;								
						}						
					  }
					$this->session->set_userdata('sec_id',1);
					$this->session->set_userdata('fb_emp_id', '');
					$this->session->set_userdata('fb_pro_id', '');
					$this->session->set_userdata('slot_id', '');
					$this->session->set_userdata('q_fb_emp_id', '');
					$this->session->set_userdata('q_fb_pro_id', '');
					$this->session->set_userdata('q_slot_id', '');
					//print_r($acc);
                    $this->session->set_userdata('access', $acc);
                    $this->session->set_userdata('proj_fk', $row['project_id_fk']);
					$this->session->set_userdata('streak', $row['streak']);
					$this->session->set_userdata('emp_id', $row['emp_id']);
					}		
					//$data["user_allocation_access"] = $this->user_model->accessForAllocation($user_id);
					$access_mst = $this->user_model->fetchAccess();
					$user_id            = $this->session->userdata('user_id');
						$role_id            = $this->session->userdata('role_id');
						$dept_val = $this->user_model->fetchdept_data($user_id);
						$data["dept_val"]=$dept_val;
						$dept_opt=$dept_val[0]['dept_id'];
						$data['dept_opt']=$dept_opt;
					$this->session->set_userdata('access_mst', $access_mst);
                   redirect(site_url('User/load_view_f?a=Reports'));
				}
			}else {
               // $this->session->set_flashdata('error_msg', '*Invalid UserName/Password');
				header('Location: https://accounts.google.com/Logout?hl=en');
            }
        }
		}
    }
	
	
	function login_change()
    {			
	$ttok = $this->input->get('token');
	$u_info['email'] = $this->input->get('email');
			$this->session->set_userdata('token',$ttok);
            $user_login = array(
                'user_email' => $u_info['email']
            );   
			
			print_r($u_info);
            $user_dt = $this->user_model->login_suser($user_login['user_email']);            
            if ($user_dt) {
                foreach ($user_dt as $row) {
					$acc='';
                    $this->session->set_userdata('user_id', $row['user_id']);
                    $this->session->set_userdata('user_email', $row['user_login_name']);
                    $this->session->set_userdata('user_name', $row['full_name']);
					$this->session->set_userdata('role_id', $row['role_id_fk']);
					$acc=$row['access_name_mod'];
					$day_chk= date('Y-m-d');
					//print_r($day_chk);
					$this->session->set_userdata('forms_list','');
					$chek_form_access=$this->user_model->fetchformAccess($this->session->userdata('user_id'));
					$chek_dm_access=$this->user_model->fetchDMAccess($this->session->userdata('user_id'));
					if($chek_form_access[0]['accss'])
					{
						$df_dates=$this->user_model->fetchfeedstart();
						if($day_chk>=$df_dates[0]['feed_s'] && $day_chk<=$df_dates[0]['feed_e'])
						{
							$acc="Yearly_Form|".str_replace(",","|",$chek_form_access[0]['accss'])."|".$acc;
							$this->session->set_userdata('forms_list',$chek_form_access[0]['accss']);
							
						}
					}
					if($chek_dm_access[0]['accss'])
					{
						$df_dates=$this->user_model->fetchfeedstart();
						if($day_chk>=$df_dates[0]['feed_s'] && $day_chk<=$df_dates[0]['feed_e'])
						{
							$acc=str_replace(",","|",$chek_dm_access[0]['accss'])."|".$acc;
							
						}else{
							$acc="Yearly_Form|".str_replace(",","|",$chek_dm_access[0]['accss'])."|".$acc;
						}
					}
					$usd=$this->session->userdata('user_id');
					 $us_acc_arry=array(1,79);
					if(in_array($usd,$us_acc_arry))
					  {
						 if($day_chk>=$df_dates[0]['feed_s'] && $day_chk<=$df_dates[0]['feed_e'])
						{
							$acc="self_response|".$acc;
							
						}else{
							$acc="Yearly_Form|self_response|".$acc;
						}
					  }
					$this->session->set_userdata('sec_id',1);
					$this->session->set_userdata('fb_emp_id', '');
					$this->session->set_userdata('fb_pro_id', '');
					//print_r($acc);
                    $this->session->set_userdata('access', $acc);
                    $this->session->set_userdata('proj_fk', $row['project_id_fk']);
					$this->session->set_userdata('streak', $row['streak']);
					$this->session->set_userdata('emp_id', $row['emp_id']);
					}		
					//$data["user_allocation_access"] = $this->user_model->accessForAllocation($user_id);
					$access_mst = $this->user_model->fetchAccess();
					$user_id            = $this->session->userdata('user_id');
						$role_id            = $this->session->userdata('role_id');
						$dept_val = $this->user_model->fetchdept_data($user_id);
						$data["dept_val"]=$dept_val;
						$dept_opt=$dept_val[0]['dept_id'];
						$data['dept_opt']=$dept_opt;
					$this->session->set_userdata('access_mst', $access_mst);
                   redirect(site_url('User/load_view_f?a=Reports'));
				}else {
               // $this->session->set_flashdata('error_msg', '*Invalid UserName/Password');
				header('Location: https://accounts.google.com/Logout?hl=en');
            }
    }
	
	function home_page()
    {
		 $data["access_mst"] = $this->user_model->fetchAccess();
		$this->session->set_userdata('access_mst', $data["access_mst"]);
        $user_id            = $this->session->userdata('user_id');
		$role_id            = $this->session->userdata('role_id');
		$dept_val = $this->user_model->fetchdept_data($user_id);
		$data["dept_val"]=$dept_val;
		$dept_opt=$dept_val[0]['dept_id'];
		$data['dept_opt']=$dept_opt;
		$allocation_dept_val = $this->allocation_model->fetchPermissionDept($user_id);
		$data['allocation_dept_val']=!empty($allocation_dept_val)?$allocation_dept_val[0]['dept_id']:'';
		$data["metrics"] = $this->user_model->fetchMetrics($user_id,$role_id,$dept_opt);
		//Delete search value of an allocation
		//delete_cookie('search_value');
        if ($user_id) {
            $this->load->view('user_profile.php', $data);
        }
	}
    
    public function user_logout()
    { 
		$toekn_rem = $this->session->userdata('token');	
		$json_a=json_decode($toekn_rem,true);
		 $this->session->sess_destroy();
		 //Delete search value of an allocation
		//delete_cookie('search_value');
		 $gf=$this->googleplus->revokeToken($json_a['access_token']);		 
		header('Location: https://accounts.google.com/Logout?hl=en');
    }
    
    public function login_view()
    {
		$contents['login_url'] = $this->googleplus->loginURL();	
        $this->load->view("Login.php",$contents);   
		
    }       
		
	public function load_emp_string()
    {
		$pt_emp           = $this->input->post('pt_emp');
		$this->session->set_userdata('pt_emp', $pt_emp);		
    }
        
    public function task_edit()
    {	
        $t_date1           = $this->input->get('t_date');
        $u_id              = $this->session->userdata('user_id');
       
		$dept_val = $this->user_model->fetchdept_data($u_id);
		$data["dept_val"]=$dept_val;
		$dept_opt=$dept_val[0]['dept_id'];
		$data['dept_opt']=$dept_opt;
		
		 $sel1           = $this->user_model->sel1_data($t_date1,$dept_opt);				
        $data['response']  = $this->user_model->select_resp($t_date1, $u_id);
        $data['sel_1']     = $sel1;
        $data['date_task'] = $t_date1;
		$role_id            = $this->session->userdata('role_id');
		$data["metrics"] = $this->user_model->fetchMetrics($u_id,$role_id,$dept_opt);
		$data['allocs']  = $this->user_model->select_allocations($u_id,$t_date1);
		//print_r($data['allocs'] );
        $this->load->view("task_edit.php", $data);   
    }
	
	public function week_off_new()
	{
		 $s_dt = $this->input->get('t_date');
	     $day_fm=date('d-M-Y',strtotime($s_dt));
		 $w_name=date("l",strtotime($day_fm));
		 $u_id = $this->session->userdata('user_id');		 
		 		$week_di=array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
				echo "<div class='row row_style_1 l_sty' style='padding:2%;border:1px solid #ccc;'>"; 
			echo '<div class="col-md-12"><input class="week_chn" checked type="checkbox" style="zoom: 1.5;"></div>';				
			echo "<div class='col-md-2'>";
			echo "<label class='l_font_fix_3 comp_s'>Start Date</label>";
			echo "<input id='t_dtpicker1' class='l_s_dt date_fm date-picker' style='width:100%;' />";
			echo "</div>";			
			echo "<div class='col-md-2'>";
			echo "<label class='l_font_fix_3 comp_e'>End Date</label>";	
				echo "<input id='t_dtpicker2' class='l_e_dt date_fm date-picker' style='width:100%;' />";
			echo "</div>";		
			
			echo "<div class='col-md-2'>
				<label class='l_font_fix_3'>Day</label>";
				echo "<select class='selectpicker form-control day_typ' title='Nothing Selected'>";				
					foreach($week_di AS $key=>$value)
					{
						echo "<option value='" .$key. "'>" . $value . "</option>";
					}
				echo "</select>";
			echo "</div>";
			
			echo "<div class='col-md-2'>
				<label class='l_font_fix_3'>Inserted By</label>															
				<h6 class='h6_sty' ></h6>
			</div>";
			
			echo "<div class='col-md-2'>
				<label class='l_font_fix_3'>Updated By</label>
				<h6 class='h6_sty' ></h6>
			</div>";
			echo "<div class='col-md-2'>
				<label class='l_font_fix_3'>Status</label>															
				<h6 class='h6_sty' >Pending</h6>
			</div>";
			echo "</div>";		
	}
	public function week_off()
    {
		 $s_dt = $this->input->get('t_date');
			$day_fm=date('d-M-Y',strtotime($s_dt));
		 $w_name=date("l",strtotime($day_fm));
		 $u_id = $this->session->userdata('user_id');		 
		 $l_data=array();
			$le_values1= $this->user_model->sel_emp_w_off($u_id);
			 if($le_values1)
				 {
					 $l_data = array_merge($l_data,$le_values1);
				 }
			  echo "<div class='row row_style_1'>
					<div class='col-md-12 text-center'>
					<span class='badge add_prio pull-left week_ins' value='0'>+Add</span>
					<label>WEEK OFFS</label>		
					</div>
				</div>";	
				$week_di=array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
		if ($l_data) {
            foreach ($l_data AS $row) {
				echo "<div class='row row_style_1 l_sty' tab_id='".$row['emp_week_off_id']."' style='padding:2%;border:1px solid #ccc;'>";
				echo '<div class="col-md-12"><input class="week_chn" type="checkbox" style="zoom: 1.5;"></div>';
			echo "<div class='col-md-2'>";
			echo "<label class='l_font_fix_3 comp_s'>Start Date</label>";
			echo "<h6 class='h6_sty l_s_dt' >" . $row['start_dt'] . "</h6>";
			echo "</div>";			
			echo "<div class='col-md-2'>";
			echo "<label class='l_font_fix_3 comp_e'>End Date</label>";	
				echo "<input id='t_dtpicker2' class='l_e_dt date_fm date-picker' style='width:100%;'  value='".$row['end_dt']."' />";
			echo "</div>";		
			
			echo "<div class='col-md-2'>
				<label class='l_font_fix_3'>Day</label>";
				echo "<select class='selectpicker form-control day_typ' title='Nothing Selected'>";				
					foreach($week_di AS $key=>$value)
					{
						$sel='';
						if($key==$row['week_day'])
						{
							$sel='selected';
						}
						echo "<option value='" .$key. "' ".$sel.">" . $value . "</option>";
					}
				echo "</select>";
			echo "</div>";
			
			echo "<div class='col-md-2'>
				<label class='l_font_fix_3'>Inserted By</label>															
				<h6 class='h6_sty' >" . $row['ins_user'] . "<br>" . $row['ins_dt'] . "</h6>
			</div>";
			
			echo "<div class='col-md-2'>
				<label class='l_font_fix_3'>Updated By</label>
				<h6 class='h6_sty' >" . $row['upd_user'] . "<br>" . $row['upd_dt'] . "</h6>
			</div>";
			echo "<div class='col-md-2'>
				<label class='l_font_fix_3'>Status</label>															
				<h6 class='h6_sty' >" . $row['conf_status'] . "</h6>
			</div>";
			echo "</div>";
			}			
			
		}
		echo "<div class='row row_style_1' id='app_marker'>			
			<div class='col-md-12 text-center'>
			<button class='btn add_but week_off_sub' type='button'>Submit</button>
			</div>
			</div>";
		 
	}	
	
	
	public function load_task_alloc()
    {
		 $param    = $this->input->post('param');
		 $sel_0_ID    = $this->input->post('sel_0_ID');
        $sel_1_ID    = $this->input->post('sel_1_ID');
        $sel_2_ID     = $this->input->post('sel_2_ID');
        $sel_3_ID    = $this->input->post('sel_3_ID');
        $sel_4_ID = $this->input->post('sel_4_ID');
		$dateID = $this->input->post('dateID');		
		$load_du   = $this->user_model->load_task_sel($param,$sel_0_ID, $sel_1_ID, $sel_2_ID, $sel_3_ID, $sel_4_ID, $dateID);	
		$output    = '';
            if ($load_du) {
                foreach ($load_du as $roww) {
                    $output .= "<option value='" . $roww['p_id'] . "'>" . $roww['p_nm'] . "</option>";
                }
            }
            echo $output;
	}
	public function alloc_con_media()
    {
		 $sel_0_ID    = $this->input->post('sel_0_ID');
        $sel_1_ID    = $this->input->post('sel_1_ID');
        $sel_2_ID     = $this->input->post('sel_2_ID');
        $sel_3_ID    = $this->input->post('sel_3_ID');
        $sel_4_ID = $this->input->post('sel_4_ID');
		$dateID = $this->input->post('dateID');
		
		$sel_0_NM    = $this->input->post('sel_0_NM');
       
		$p_array=array();
		$t_array=array();
		$s_array=array();
		$j_array=array();
		$load_du   = $this->user_model->load_select("a_sel_cont",$sel_0_ID, $sel_1_ID, $sel_2_ID, $sel_3_ID, $sel_4_ID, $dateID);
		if ($load_du) {
                foreach ($load_du as $roww) {
                    $p_array[$roww['project_id']]=$roww['project_name'];
					$t_array[$roww['task_id']]=$roww['task_name'];
					$s_array[$roww['sub_id']]=$roww['sub_name'];
					$j_array[$roww['job_id']]=$roww['job_name'];
	                }
            }
        $dateID    = $this->input->post('dateID');
echo '<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="forresetonly">
				  <div class="modal-body">	
					<div id="a_load_ac">						
					<div class="row row_style_1">
						<div class="col-md-3">
						<label class="l_font_fix_3">Choose Dept:</label>
							<select id="a_sel_dept_1" class="selectpicker form-control" disabled title="Nothing Selected" data-live-search="true">';
								echo "<option value='".$sel_0_ID."' selected >".$sel_0_NM."</option>";							
						echo '</select>
						</div>
						<div class="col-md-3">
							<label class="l_font_fix_3">Projects</label>
							<select id="a_sel_1" class="selectpicker form-control" title="Nothing Selected" data-live-search="true">';							
							if($p_array)
							{
							foreach ($p_array as $row2=>$val)
							{
								$sel="";
								if($sel_1_ID==$row2)
								{
									$sel="selected";																		
								}
								echo "<option value='". $row2 .  "' ".$sel.">" . $val . "</option>";
							}
							}
							
							echo '</select>
						</div>
						<div class="col-md-6">
							<label class="l_font_fix_3">Category</label>
							<select id="a_sel_2" class="selectpicker form-control"  title="Nothing Selected" data-live-search="true">';
							if($t_array)
							{
							foreach ($t_array as $row2=>$val)
							{
								$sel="";
								if($sel_2_ID==$row2)
								{
									$sel="selected";																		
								}
								echo "<option value='". $row2 .  "' ".$sel.">" . $val . "</option>";
							}
							}
							echo '</select>
						</div>	
						</div>
						<div class="row row_style_1">
						<div class="col-md-6">
						<label class="l_font_fix_3">Element</label>
							<select id="a_sel_3" class="selectpicker form-control"  title="Nothing Selected" data-live-search="true">';
							if($s_array)
							{
							foreach ($s_array as $row2=>$val)
							{
								$sel="";
								if($sel_3_ID==$row2)
								{
									$sel="selected";																		
								}
								echo "<option value='". $row2 .  "' ".$sel.">" . $val . "</option>";
							}
							}
							echo '</select>
						</div>	
						<div class="col-md-6">
						<label class="l_font_fix_3">Action</label>
							<select id="a_sel_4" class="selectpicker form-control"  title="Nothing Selected" data-live-search="true">';
							if($j_array)
							{
							foreach ($j_array as $row2=>$val)
							{
								$sel="";
								if($sel_4_ID==$row2)
								{
									$sel="selected";																		
								}
								echo "<option value='". $row2 .  "' ".$sel.">" . $val . "</option>";
							}
							}
							echo '</select>
						</div>	
						</div>
						<div class="row row_style">
							<div class="col-md-2 col-md-offset-5">
								<button class="btn add_but a_load_tsk" type="button">Load Activity</button>
							</div>
						</div>
						</div>
					</form>
						<div id="a_load_tsk_info">
						</div>														
				 </div>								  
				</div>
			  </div>';
	}
	public function leave_add()
    {
		 $s_dt = $this->input->get('t_date');
			$day_fm=date('d-M-Y',strtotime($s_dt));
		
			 $w_name=date("l",strtotime($day_fm));
		 $u_id = $this->session->userdata('user_id');		 
		 
		 $l_data=array();			
		 $lev_type= $this->user_model->sel_leave_type('leave_type_mst',"dropdown");
			$le_values1= $this->user_model->sel_emp_view($u_id,$s_dt,"Declared Hols");
			 if($le_values1)
				 {
					 $l_data = array_merge($l_data,$le_values1);
				 }
				$le_values2= $this->user_model->sel_emp_view($u_id,$s_dt,"Others");
			 if($le_values2)
				 { 	
					$l_data = array_merge($l_data,$le_values2);			 
				 }
				$le_values3= $this->user_model->sel_emp_view($u_id,$s_dt,"Comp Off");
			  if($le_values3)
				 { 	
					$l_data = array_merge($l_data,$le_values3);			 
				 }
        			
			  echo "<div class='row row_style_1'>
					<div class='col-md-12 text-center'>
					<span class='badge add_prio pull-left' value='0'>".$w_name."</span>
					<label>Logs for ".$day_fm." </label>		
					</div>
				</div>";	
		if ($l_data) {
			$editable=0;	
				$radio_cnt=0;
            foreach ($l_data AS $row) {
				if($row['edit']==1)
				{
					echo "<div class='row row_style_1 l_sty' tab_id='".$row['comp_track_id']."' style='padding:2%;border:1px solid #ccc;'>			";
				}else{
				echo "<div class='row row_style_1' style='border:1px solid #ccc;'>			";	
				}
               	
			echo "<div class='col-md-3'>
				<label class='l_font_fix_3'>Leave</label>";
				if($row['edit']==1)
				{
					echo "<select class='selectpicker form-control ll_typ' title='Nothing Selected'>";
					foreach($lev_type AS $roww)
					{
						$sel='';
						if( $roww['l_type_id'] ==$row['l_type_id'])
						{
							$sel='selected';
						}
						echo "<option value='" . $roww['l_type_id'] . "' ".$sel.">" . $roww['leave_type_name'] . "</option>";
					}
					echo "</select>";
				}else{
					echo "<h6 class='h6_sty' >".$row['w_val']."</h6>";
				}
			echo "</div>";
			if($row['action']=="Others" )
			{
			echo "<div class='col-md-2 half_d_bl'>";			
			$cc_val="No";
			echo "<label class='l_font_fix_3' style='display:block;'>Half Day</label>";	
			if($row['edit']==1)
				{
					$ch_ll=array('checked','');
					$cc_val="No";
					if($row['is_half_day']==1)
					{
						$ch_ll=array('','checked');
						$cc_val="Yes";
					}
					echo "<label class='l_font_fix_2' style='display:block;'>";
					echo "<input type='radio' name='option_38_".$radio_cnt."'  ".$ch_ll[0]." val='0' class='status_ch' style='vertical-align: text-bottom;'/><span> No</span></label>";
					echo " <label class='l_font_fix_2'>";
					echo "<input type='radio' name='option_38_".$radio_cnt."' ".$ch_ll[1]." val='1' class='status_ch' style='vertical-align: text-bottom;'/><span> Yes</span></label>";
				}else{
					echo "<h6 class='h6_sty' >".$cc_val."</h6>";
				}
			echo "</div>";
			}else{
				echo "<div class='col-md-2 half_d_bl hide'>";			
			echo "<label class='l_font_fix_3' style='display:block;'>Half Day</label>";	
					$ch_ll=array('checked','');
					echo "<label class='l_font_fix_2' style='display:block;'>";
					echo "<input type='radio' name='option_38_".$radio_cnt."'  ".$ch_ll[0]." val='0' class='status_ch' style='vertical-align: text-bottom;'/><span> No</span></label>";
					echo " <label class='l_font_fix_2'>";
					echo "<input type='radio' name='option_38_".$radio_cnt."' ".$ch_ll[1]." val='1' class='status_ch' style='vertical-align: text-bottom;'/><span> Yes</span></label>";
				echo "</div>";
			}
			echo "<div class='col-md-3'>";
			if($row['w_val']=="Comp Off")
			{
			echo "<label class='l_font_fix_3 comp_s'>Comp Off On</label>";	
			}else{
			echo "<label class='l_font_fix_3 comp_s'>Start Date</label>";		
			}
				if($row['edit']==1)
				{
				echo "<input id='t_dtpicker' class='l_s_dt date_fm date-picker' style='width:100%;' value='".$row['start_date']."' />";
				}else{
				echo "<h6 class='h6_sty'>" . $row['start_date'] . "</h6>";
				}
			echo "</div>";			
			echo "<div class='col-md-3'>";
			if($row['w_val']=="Comp Off")
			{
			echo "<label class='l_font_fix_3 comp_e'>Worked On</label>";	
			}else{
			echo "<label class='l_font_fix_3 comp_e'>End Date</label>";	
			}
			
				if($row['edit']==1)
				{
				echo "<input id='t_dtpicker2' class='l_e_dt date_fm date-picker' style='width:100%;'  value='".$row['end_date']."' />";
				}else{
				echo "<h6 class='h6_sty' >" . $row['end_date'] . "</h6>";
				}
			echo "</div>";		
			echo "<div class='col-md-12'>
				<label class='l_font_fix_3'>Reason</label>";
				$editable=$editable+$row['edit'];
				if($row['edit']==1)
				{
				echo "<input type='text' value= '" . $row['reason'] . "' class='reason form-control'/>";
				}else{
				echo "<h6 class='h6_sty' >" . $row['reason'] . "</h6>";
				}
			echo "</div>";
			echo "<div class='col-md-2'>
				<label class='l_font_fix_3'>Status</label>															
				<h6 class='h6_sty' >" . $row['conf_status'] . "</h6>
			</div>";
			echo "<div class='col-md-2'>
				<label class='l_font_fix_3'>Inserted By</label>															
				<h6 class='h6_sty' >" . $row['ins_user'] . "<br>" . $row['ins_dt'] . "</h6>
			</div>";
			
			echo "<div class='col-md-2'>
				<label class='l_font_fix_3'>Updated By</label>
				<h6 class='h6_sty' >" . $row['upd_user'] . "<br>" . $row['upd_dt'] . "</h6>
			</div>";
			if($row['edit']==1)
				{
			echo "<div class='col-md-2 cncl_lv'>
				<label class='l_font_fix_3'>Cancel Leave</label>";
				echo "<label class='l_font_fix_2' style='display:block;'>";
					echo "<input type='radio' name='option_138_".$radio_cnt."'  checked val='Active' class='kl_stl' style='vertical-align: text-bottom;'/><span> No</span></label>";
					echo " <label class='l_font_fix_2'>";
					echo "<input type='radio' name='option_138_".$radio_cnt."'  val='InActive' class='kl_stl' style='vertical-align: text-bottom;'/><span> Yes</span></label>";
			echo "</div>";	
				}			
			echo "</div>";
			$radio_cnt++;
			}
			if($editable!=0)
			{
			echo "<div class='row row_style_1'>			
			<div class='col-md-12 text-center'>
			<button class='btn add_but leave_sub_log' type='button'>Submit</button>
			</div>
			</div>";
			}
		}
		 
	}	
	
	public function leave_new()
    {
			$s_dt = $this->input->get('t_date');
			$day_fm=date('d-M-Y',strtotime($s_dt));
			$w_name=date("l",strtotime($day_fm));
			$u_id = $this->session->userdata('user_id');		 
			$lev_type= $this->user_model->sel_leave_type('leave_type_mst',"dropdown");
			  echo "<div class='row row_style_1'>
					<div class='col-md-12 text-center'>
					<span class='badge add_prio pull-left' value='0'>".$w_name."</span>
					<label>Logs for ".$day_fm." </label>		
					</div>
				</div>";		
			echo "<div class='row row_style_1 l_sty'  style='padding:2%;border:1px solid #ccc;'>";				
			echo "<div class='col-md-2'>
				<label class='l_font_fix_3'>Leave</label>";				
			echo "<select class='selectpicker form-control ll_typ' title='Nothing Selected'>";
					foreach($lev_type AS $roww)
					{
						echo "<option value='" . $roww['l_type_id'] . "'>" . $roww['leave_type_name'] . "</option>";
					}
					echo "</select>";
			echo "</div>";
			echo "<div class='col-md-2 half_d_bl'>";			
			echo "<label class='l_font_fix_3' style='display:block;'>Half Day</label>";	
			echo "<label class='l_font_fix_2' style='display:block;'>";
			echo "<input type='radio' name='option_38'  checked val='0' class='status_ch' style='vertical-align: text-bottom;'/><span> No</span></label>";
			echo " <label class='l_font_fix_2'>";
			echo "<input type='radio' name='option_38' val='1' class='status_ch' style='vertical-align: text-bottom;'/><span> Yes</span></label>";
			echo "</div>";		
			echo "<div class='col-md-3'>";			
			echo "<label class='l_font_fix_3 comp_s'>Start Date</label>";				
				echo "<input id='t_dtpicker' class='l_s_dt date_fm date-picker' style='width:100%;' value='".$day_fm."' />";
			echo "</div>";			
			echo "<div class='col-md-3'>";			
			echo "<label class='l_font_fix_3 comp_e'>End Date</label>";	
				echo "<input id='t_dtpicker2' class='l_e_dt date_fm date-picker' style='width:100%;'  value='".$day_fm."' />";
			echo "</div>";		
			echo "<div class='col-md-12'>
				<label class='l_font_fix_3'>Reason</label>";
				echo "<input type='text' class='reason form-control'/>";
			echo "</div>";
			echo "<div class='col-md-12'>
				<label class='l_font_fix_3' style='width: 100%;padding-top: 20px;'>Send mail to Project Owners</label>";
				echo "<div class='col-md-6'>
				<label class='l_font_fix_3'>Choose Dept:</label>
					<select id='sel_dept_1' class='selectpicker form-control' title='Nothing Selected' data-live-search='true'>";
					$dept_val = $this->user_model->fetchdept_data($u_id);
					$dept_opt=$dept_val[0]['dept_id'];
					foreach ($dept_val as $row)
					{
						$sel='';
							if($dept_opt==$row['dept_id'])
							{
								$sel='selected';																		
							}
						echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
					}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-6'>
					<label class='l_font_fix_3' style='width: 100%;'>Choose Project:</label>";
					$d_pro_sel_dta = $this->user_model->sel_pro_data(0,$dept_opt);
					//print_r($d_pro_sel_dta);
				echo "<select class='form-control pro_po_sel' multiple='multiple'>";
				//echo '<option data-hidden="true"></option>';	
					foreach ($d_pro_sel_dta as $row)
					{
						echo "<option value='".$row['sel_1_id']."'>".$row['sel_1_name']."</option>";
					}
				echo "</select>";
			echo "</div>";
			echo "</div>";
			echo "</div>";
			echo "<div class='row row_style_1'>			
			<div class='col-md-12 text-center'>
			<button class='btn add_but leave_new_log' type='button'>Submit</button>
			</div>
			</div>";
		}
		
	public function ins_trade_review()
	{
		 $ans = $this->input->post("C");        
		 $ans_r = $this->input->post("C_r");     
		 $r_ids = $this->input->post("r_ids");        
        $resp = $this->user_model->ins_trade($ans,$r_ids,$ans_r);
        echo $resp;
	}
	
	public function trade_review()
    {
			$r_ids = $this->input->post('C');
			$u_id = $this->session->userdata('user_id');
			$params= $this->user_model->get_param_review($r_ids);			
			$p_vals= $this->user_model->get_p_val_rev();			
			//print_r($params);
			  echo "<div class='row row_style_1'>
					<div class='col-md-12 text-center'>
					<label>Review Feedback</label>		
					</div>
				</div>";	
			if(!empty($params))
			{
				$rl=array();
			foreach($params AS $row)
			{
				$rl[$row['project_name']."|".$row['trade_name']]=$row['r_ids'];
			}
			foreach($rl AS $key=>$value)
			{
				echo "<div class='row row_style_1 lrf_sty' r_id='".$value."' style='padding:2%;border:1px solid #ccc;'>";
				
				echo "<div class='row row_style_1'>";
				echo "<div class='col-md-12 text-center'>";
				echo "<label class='l_font_fix_3' style='text-decoration:underline;'>".str_replace("|"," - ",$key)."</label>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row'>";
				foreach($params AS $row)
				{
					if($key==($row['project_name']."|".$row['trade_name']))
					{
						if(!($row['r_param_id']))
						{
						echo "<div class='col-md-4 lll_sty_feed' rp_id='null'>";	
						echo "<label class='l_font_fix_3'>".$row['r_param_name']."</label>";
						echo "<select class='selectpicker form-control prs_val'  title='Nothing Selected'>";	
						}else{
						echo "<div class='col-md-2 lll_sty_feed' rp_id='".$row['r_param_id']."'>";	
						echo "<label class='l_font_fix_3'>".$row['r_param_name']."</label>";
						echo "<select class='selectpicker form-control p_val'  title='Nothing Selected'>";	
						echo '<option value="null">N/A</option>';
						}					
						
						
						foreach($p_vals AS $v1)
						{
							echo "<option value='" .$v1['r_val_id']. "'>" . $v1['r_val_name'] . "</option>";
						}
						echo "</select>";
						//echo $row['r_param_val'];
						echo "</div>";
						
						if(!($row['r_param_id']))
						{
							echo "<div class='col-md-4'>";
							echo "<label class='l_font_fix_3'>Review Desc</label>";
							echo "<input type='text' class='rev_qs_desc form-control'/>";
							echo "</div>";
						}
					}					
				}
				echo "</div>";
				echo "</div>";
			}
			echo "<div class='row row_style_1'>			
			<div class='col-md-12 text-center'>
			<button class='btn add_but ins_rev_feed' type='button'>Submit</button>
			</div>
			</div>";			
			}			
		
							
			// echo "<div class='col-md-2'>
				// <label class='l_font_fix_3'>Leave</label>";				
			// // echo "<select class='selectpicker form-control ll_typ' title='Nothing Selected'>";
					// // foreach($lev_type AS $roww)
					// // {
						// // echo "<option value='" . $roww['l_type_id'] . "'>" . $roww['leave_type_name'] . "</option>";
					// // }
					// // echo "</select>";
			// echo "</div>";
			// echo "<div class='col-md-2 half_d_bl'>";			
			// echo "<label class='l_font_fix_3' style='display:block;'>Half Day</label>";	
			// echo "<label class='l_font_fix_2' style='display:block;'>";
			// echo "<input type='radio' name='option_38'  checked val='0' class='status_ch' style='vertical-align: text-bottom;'/><span> No</span></label>";
			// echo " <label class='l_font_fix_2'>";
			// echo "<input type='radio' name='option_38' val='1' class='status_ch' style='vertical-align: text-bottom;'/><span> Yes</span></label>";
			// echo "</div>";		
			// echo "<div class='col-md-3'>";			
			// echo "<label class='l_font_fix_3 comp_s'>Start Date</label>";				
				// echo "<input id='t_dtpicker' class='l_s_dt date_fm date-picker' style='width:100%;' value='".$day_fm."' />";
			// echo "</div>";			
			// echo "<div class='col-md-3'>";			
			// echo "<label class='l_font_fix_3 comp_e'>End Date</label>";	
				// echo "<input id='t_dtpicker2' class='l_e_dt date_fm date-picker' style='width:100%;'  value='".$day_fm."' />";
			// echo "</div>";		
			// echo "<div class='col-md-12'>
				// <label class='l_font_fix_3'>Reason</label>";
				// echo "<input type='text' class='reason form-control'/>";
			// echo "</div>";
			// echo "<div class='col-md-12'>
				// <label class='l_font_fix_3' style='width: 100%;padding-top: 20px;'>Send mail to Project Owners</label>";
				// echo "<div class='col-md-6'>
				// <label class='l_font_fix_3'>Choose Dept:</label>
					// <select id='sel_dept_1' class='selectpicker form-control' title='Nothing Selected' data-live-search='true'>";
					// $dept_val = $this->user_model->fetchdept_data($u_id);
					// $dept_opt=$dept_val[0]['dept_id'];
					// foreach ($dept_val as $row)
					// {
						// $sel='';
							// if($dept_opt==$row['dept_id'])
							// {
								// $sel='selected';																		
							// }
						// echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
					// }
				// echo "</select>";
				// echo "</div>";
				// echo "<div class='col-md-6'>
					// <label class='l_font_fix_3' style='width: 100%;'>Choose Project:</label>";
					// $d_pro_sel_dta = $this->user_model->sel_pro_data(0,$dept_opt);
					// //print_r($d_pro_sel_dta);
				// echo "<select class='form-control pro_po_sel' multiple='multiple'>";
				// //echo '<option data-hidden="true"></option>';	
					// foreach ($d_pro_sel_dta as $row)
					// {
						// echo "<option value='".$row['sel_1_id']."'>".$row['sel_1_name']."</option>";
					// }
				// echo "</select>";
			// echo "</div>";
			// echo "</div>";
			// echo "</div>";
			
		}
		
    public function task_view()
    {
        $resp_id   = $this->input->get('resp_id');
        $resp_view = $this->user_model->task_view($resp_id);
        if ($resp_view) {
            foreach ($resp_view AS $row) {
                echo "<div class='row row_style_1'>
			<div class='col-md-12 text-center'>
			<span class='badge add_prio pull-left " . $row['tast_color'] . "' value='0'>" . $row['tast_stat'] . "</span>
			<label>" . $row['activity_name'] . "</label>		
			</div>
		</div>
	<div class='row row_style_1'>
			<div class='col-md-3'>
				<label class='l_font_fix_3'>Project</label>	
					<h6 class='h6_sty' >" . $row['project_name'] . "</h6>				
			</div>
			<div class='col-md-3'>
				<label class='l_font_fix_3'>Task</label>															
				<h6 class='h6_sty' >" . $row['task_name'] . "</h6>
			</div>
			<div class='col-md-3'>
				<label class='l_font_fix_3'>SubTask</label>															
				<h6 class='h6_sty' >" . $row['sub_name'] . "</h6>
			</div>
			<div class='col-md-3'>
				<label class='l_font_fix_3'>Job</label>
				<h6 class='h6_sty' >" . $row['job_name'] . "</h6>
			</div>
			</div>";
                echo "<div class='row row_style_1'>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Work Unit</label>
				<h6 class='h6_sty' >" . $row['wu_name'] . "</h6>
			</div>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Qty Done</label>
				<h6 class='h6_sty' >" . $row['work_comp'] . "</h6>
			</div>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Std Target</label>
				<h6 class='h6_sty' >" . $row['std_tgt_val'] . "</h6>
			</div>			
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Equiv XP</label>
				<h6 class='h6_sty' >" . $row['equiv_xp'] . "</h6>
			</div>
			<div class='col-md-4'>
				<label class='l_font_fix_3'>Description</label>
				<h6 class='h6_sty' >" . $row['task_desc'] . "</h6>
			</div>	
		</div>
		<div class='row row_style_1'>
		<div class='col-md-2'>
				<label class='l_font_fix_3'>Total XP</label>
				<h6 class='h6_sty' >" . $row['total_xp'] . "</h6>
			</div>
		</div>
		<div class='row hid1'>	
			<div class='col-md-12'>
				<button class='tab_stages tab_dis_selec' ch='iss_toggle'>Issues</button>
				<button class='tab_stages' ch='audit_toggle'>Audit</button>
				<button class='tab_stages' ch='rev_toggle'>Review</button>
			</div>
		</div>
		
		<div class='row hid' id='iss_toggle'>		
		<div class='col-md-2'>
				<label class='l_font_fix_3'>Issue</label>
				<h6 class='h6_sty' >" . $row['issue_flag'] . "</h6>
			</div>
			<div class='col-md-4'>
				<label class='l_font_fix_3'>Issue Desc</label>
				<h6 class='h6_sty' >" . $row['issue_desc'] . "</h6>
			</div>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Comp XP</label>
				<h6 class='h6_sty' >" . $row['comp_xp'] . "</h6>
			</div>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Awarded By</label>
				<h6 class='h6_sty' >" . $row['comp_user_nm'] . "</h6>
			</div>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Updated On</label>
				<h6 class='h6_sty' >" . $row['comp_upd_dt'] . "</h6>
			</div>
			</div>
		<div class='row hid' id='audit_toggle' style='display:none;'>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Audit By</label>
				<h6 class='h6_sty' >" . $row['audit_user_nm'] . "</h6>
			</div>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Audit On</label>
				<h6 class='h6_sty' >" . $row['audit_upd_dt'] . "</h6>
			</div>
			</div>
		<div class='row hid' id='rev_toggle' style='display:none;'>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Review By</label>
				<h6 class='h6_sty' >" . $row['review_user_nm'] . "</h6>
			</div>
					<div class='col-md-2'>
				<label class='l_font_fix_3'>Review On</label>
				<h6 class='h6_sty' >" . $row['review_upd_dt'] . "</h6>
			</div>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Review Desc</label>
				<h6 class='h6_sty' >" . $row['review_desc'] . "</h6>
			</div>
		</div>";
                
            }
        }        
    }   
    
    public function tgt_modi()
    {
        $resp_id = $this->input->get('resp_id');
        $act_nm  = $this->input->get('act_nm');
        
        $resp_modi = $this->user_model->std_tgt_modi($resp_id);
        if ($resp_modi) {
            echo "<div class='row row_style_1'>
			<div class='col-md-12 text-center'>
			<label>" . $act_nm . "</label>		
			</div>
		</div>";
            $j = 1;
            echo "<div class='row text-center'>
									<div class='col-md-1'>
										<label class='l_font_fix_3 hidden-xs hidden-sm'>#</label>
									</div>
									<div class='col-md-1'>
										<label class='l_font_fix_3 hidden-xs hidden-sm'>Std Tgt</label>
									</div>
									<div class='col-md-2'>
										<label class='l_font_fix_3 hidden-xs hidden-sm'>Start Date</label>
									</div>
									<div class='col-md-2'>
									<label class='l_font_fix_3 hidden-xs hidden-sm'>End Date</label>
									</div>
									<div class='col-md-2'>
									<label class='l_font_fix_3 hidden-xs hidden-sm'>Change Desc</label>
									</div>
									<div class='col-md-2'>
									<label class='l_font_fix_3 hidden-xs hidden-sm'>Insert Details</label>
									</div>
									<div class='col-md-2'>
										<label class='l_font_fix_3 hidden-xs hidden-sm'>Update Details</label>
									</div>
								</div>";
            foreach ($resp_modi AS $row) {
                echo "<div class='row text-center sty_llk'>
									<div class='col-md-1'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>#</label>
										<h6>" . $j . "</h6>
									</div>
									<div class='col-md-1'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Std Tgt</label>
										<h6>" . $row['tgt_val'] . "</h6>
									</div>
									<div class='col-md-2'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Start Date</label>
										<h6>" . $row['start_dt'] . "</h6>
									</div>
									<div class='col-md-2'>
									<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>End Date</label>
									<h6>" . $row['end_dt'] . "</h6>
									</div>	
									<div class='col-md-2'>
									<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Change Desc</label>
									<h6>" . $row['change_desc'] . "</h6>
									</div>											
									<div class='col-md-2'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Insert Details</label>
										<h6>" . $row['ins_user'] . " ( " . $row['ins_dt'] . " )</h6>
									</div>
									<div class='col-md-2'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Update Details</label>";
                if ($row['upd_user']) {
                    echo "<h6>" . $row['upd_user'] . " ( " . $row['upd_dt'] . " )</h6>";
                }
                echo "</div>
								</div>";
                $j++;
                
            }
        }
        
    }
    

    public function task_modi()
    {
        $resp_id = $this->input->get('resp_id');
        $sh      = $this->input->get('sh');
         $userId = $this->session->userdata('user_id');
        // $userProgresss = $this->user_model->getUserActivityByResponseId(
          // $resp_id,
          // $userId
        // );
        // $progressList = ['In Progress','Completed'];
        // $progressOption = '';
        // $userProgresss = !empty($userProgresss->user_progress) ? $userProgresss->user_progress : 'In Progress';
        // foreach ($progressList as $key => $progress) {
          // if ($userProgresss == $progress) {
            // $progressOption .= '<option value="'.$progress.'" selected>'.$progress.'</option>';
          // } else {
            // $progressOption .= '<option value="'.$progress.'">'.$progress.'</option>';
          // }
        // }
        $resp_modi = $this->user_model->task_view($resp_id);
        if ($resp_modi) {
            foreach ($resp_modi AS $row) {
                $stkl = $row['tast_stat'];
                echo "<div class='row row_style_1'>
			<div class='col-md-12 text-center'>
			<span class='badge add_prio pull-left " . $row['tast_color'] . "' value='0'>" . $row['tast_stat'] . "</span>
			<label>" . $row['activity_name'] . "</label>		
			</div>
		</div>
	<div class='row row_style_1 mod_sty'>			
			<div class='col-md-3'>
				<label class='l_font_fix_3'>Project</label>	
					<h6 class='h6_sty' >" . $row['project_name'] . "</h6>				
			</div>
			<div class='col-md-3'>
				<label class='l_font_fix_3'>Task</label>															
				<h6 class='h6_sty' >" . $row['task_name'] . "</h6>
			</div>
			<div class='col-md-3'>
				<label class='l_font_fix_3'>SubTask</label>															
				<h6 class='h6_sty' >" . $row['sub_name'] . "</h6>
			</div>
			<div class='col-md-3'>
				<label class='l_font_fix_3'>Job</label>
				<h6 class='h6_sty' >" . $row['job_name'] . "</h6>
			</div>
			</div>";
                echo "<div class='row row_style_1 mod_sty'>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Work Unit</label>
				<h6 class='h6_sty' >" . $row['wu_name'] . "</h6>
			</div>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Qty Done</label>
				<h6 class='h6_sty' >" . $row['work_comp'] . "</h6>
			</div>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Std Target</label>
				<h6 class='h6_sty' >" . $row['std_tgt_val'] . "</h6>
			</div>			
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Equiv XP</label>
				<h6 class='h6_sty' >" . $row['equiv_xp'] . "</h6>
			</div>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Comp XP</label>
				<h6 class='h6_sty' >" . $row['comp_xp'] . "</h6>
			</div>
			<div class='col-md-2'>
				<label class='l_font_fix_3'>Total XP</label>
				<h6 class='h6_sty' >" . $row['total_xp'] . "</h6>
			</div>			
		</div>";
                
            }
        }
        $stat_values = array(
            'Corrections',
            'Audit Pending'
        );
        if ($sh == 1 && in_array($stkl, $stat_values)) {
            echo "<hr class='st_hr2'>";
            echo "<div class='row row_style_1 mod_sty'>
			<div class='col-md-2 col-md-offset-3'>
				<label class='l_font_fix_3'>Qty Update</label>
				<input type='number' resp_id='" . $resp_id . "' class='upd_qty form-control'/>	
			</div>
			<div class='col-md-4'>
				<label class='l_font_fix_3'>Update Desc</label>	
				<input type='text' class='upd_Desc form-control'/>	
			</div>
			<div class='col-md-2'>
			<label class='l_font_fix_3 invisible'>Update</label>	
			<button class='btn add_but upd_ap' type='button'>Submit</button>
			</div>
			</div>";
        }
        $resp_view_2 = $this->user_model->task_modi_2($resp_id);
        if ($resp_view_2) {
            echo "<hr/>";
            
            echo "			<div class='row text-center'>
									<div class='col-md-1'>
										<label class='l_font_fix_3 hidden-xs hidden-sm'>#</label>
									</div>
									<div class='col-md-3'>
										<label class='l_font_fix_3 hidden-xs hidden-sm'>Quantity Update</label>
									</div>
									<div class='col-md-2'>
										<label class='l_font_fix_3 hidden-xs hidden-sm'>Status</label>
									</div>
									<div class='col-md-4'>
									<label class='l_font_fix_3 hidden-xs hidden-sm'>Description</label>
									</div>
									<div class='col-md-2'>
										<label class='l_font_fix_3 hidden-xs hidden-sm'>Update On</label>
									</div>
								</div>

								<hr><div class='sol'>";
            $j = 1;
            foreach ($resp_view_2 AS $row) {
                echo "<div class='row text-center sty_llk'>
									<div class='col-md-1'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>#</label>
										<h6>" . $j . "</h6>
									</div>
									<div class='col-md-3'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Quantity Update</label>
										<h6>" . $row['work_comp'] . "</h6>
									</div>
									<div class='col-md-2'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Status</label>
										<h6>" . $row['update_status'] . "</h6>
									</div>
									<div class='col-md-4'>
									<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Description</label>
									<h6>" . $row['change_desc'] . "</h6>
									</div>									
									<div class='col-md-2'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Update On</label>
										<h6>" . $row['ins_dt'] . "</h6>
									</div>
								</div>";
                $j++;
            }
            echo "</div>";
        }
    }
    
    
    public function insert_resp()
    {
        $ans = $this->input->post("C");
        $resp = $this->user_model->insert_resp($ans);
        echo $resp;
    }
    
    public function iss_approve()
    {
        $ans = $this->input->post("C");
        $resp = $this->user_model->iss_approve($ans);
        echo $resp;
    }

    public function updateIssueXp(){
    	$response['status'] = false;

    	$xp = $this->input->post("conf_xp");
    	$issueStatus = $this->input->post("issue_status");
    	$description = $this->input->post("description");
    	$oldXps =  $this->input->post("oldXp") ? explode(",", $this->input->post("oldXp")) : NULL;
    	$responseId  = explode(",", $this->input->post("r_id"));
		$date = date("Y-m-d  H:i:s");
    	$userId = $this->session->userdata('user_id');

    	$data = [
    		'upd_user' => $userId,
    		'upd_dt' => $date,
    		'comp_user' => $userId,
    		'comp_upd_dt' => $date,
    		'issue_chk_flag' => $issueStatus
    	];

    	foreach ($responseId as $key => $value) {
    		$hData=[
    			'response_id_fk' => $value,
    			'reason' => $description,
    			'ins_user' => $userId,
    			'ins_dt' => $date
    		];
			$hData['comp_xp'] = isset($oldXps[$key]) ? $oldXps[$key] : NULL;
			$hData['status'] = $oldXps ? "1" : "2";
    		$historyData[] = $hData;
    	}

    	if($issueStatus === "1"){
    		$data['comp_xp'] = $xp;
    	}
		$resp = $this->user_model->updateXP($data,$responseId);

    	$addHistoryData = $this->user_model->insertIssueHistory($historyData);

    	if($resp && $addHistoryData){
    		$response['status'] = true;
     	}

     	header('Content-Type: application/json');
     	echo json_encode($response);
     	exit();
    }
		
	public function leav_upd_rep()
    {
        $ans = $this->input->post("C");
        $resp = $this->user_model->leav_upd_rep($ans);
        echo $resp;
    }
	
	public function audit_conf()
    {
        $ans = $this->input->post("C");
        $resp = $this->user_model->audit_conf($ans);
			// $pp = $this->user_model->getProjectByResponseId($ans[1]);
			// $project=$pp[0];

			// // check if user being allocated dont have access to the project
			// if (!$this->user_model->userHasAccessToProject($ans[0], $project['project_id'])) {
				// $user = $this->user_model->getUserInfo($ans[0])[0];
				// $this->alert_model->notifyUserDontHaveAccessToProject($user, $project);
			// }

        echo $resp;
    }
    
    public function std_update()
    {
        $ans = $this->input->post("C");        
        $resp = $this->user_model->std_update($ans);
        echo $resp;   
    }
    
	  public function week_off_adju()
    {
        $ans = $this->input->post("C");        
        $resp = $this->user_model->week_off_adju($ans);
        echo $resp;   
    }
	
   public function update_leave()
    {
        $ans = $this->input->post("C");        
        $resp = $this->user_model->update_leave($ans);
        echo $resp;   
    }
	public function ins_leave()
    {
		//$resp=array();
        $ans = $this->input->post("C");        
        $resp = $this->user_model->ins_leave($ans);
        echo $resp;   
    }
    public function iss_reject()
    {
        $ans = $this->input->post("C");
        
        //print_r($ans);
        $resp = $this->user_model->iss_reject($ans);
        echo $resp;
    }
    public function upd_resp()
    {
        $ans  = $this->input->post("C");
        $resp = $this->user_model->upd_resp($ans);
        echo $resp;
    }
  
  public function load_data_pull()
  {
	  $file_nm            = $this->input->get('a');
	  if($file_nm=="Week_Targets")
		{
			$day= $this->input->get('date_view');			
			$day_w  = date('w', strtotime($day));
				if ($day_w == 0) {
					$day_w = 7;
				}				
				$st_dt = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
				$e_dt   = date('Y-m-d', strtotime($day . ' +' . (7 - $day_w) . ' days'));
            $tab_dta = $this->user_model->load_a_data_pull($file_nm,$st_dt,$e_dt);
		}
		
		if($file_nm=="All_Targets")
		{			
            $tab_dta = $this->user_model->load_a_data_pull($file_nm,null,null);	
		}
		echo $tab_dta;
  }
	public function load_table_boots()
	{
		$file_nm            = $this->input->get('a');
		$pro= $this->input->get('pro');				
		$dept_opt = $this->input->get('dept');
		$pids= $this->session->userdata('proj_fk');
		$iss_pages_var = array('Issues','Issues_comp','Issues_reject');

		$iStatus = $this->input->get("issue_status");
		$issueStatus = $iStatus ? $iStatus : "1";
		if($pids!='0' && ($pids))
			{
			$pids1=explode(",",$pids);	
			}else{
			$pids1=array($pro);	
			}
        if(in_array($file_nm, $iss_pages_var)) {
			$st_dt='';
			$e_dt='';
			$tab_dta = $this->user_model->load_issue_boots($file_nm, $pids,$st_dt,$e_dt,$dept_opt,$issueStatus);			
			$iss_cnt_tab = $this->user_model->load_issue_cnt($file_nm, $pids,$st_dt,$e_dt,$dept_opt);			
		}
		else if($file_nm=="Targets")
		{			
			$stat=$this->input->get('stat');	
			if($pro && in_array($pro,$pids1))
			{
            $tab_dta = $this->user_model->load_target_boots($file_nm, $pro,$dept_opt,$stat);
			}else{
				$tab_dta = json_encode (json_decode ("{}"));
			}
			
		}
		else if($file_nm=="Po_Leave_Confirm")
		{
			$st_dt= $this->input->get('s_dt');			
			$e_dt= $this->input->get('e_dt');		
			$lvl= $this->input->get('lvl');						
			$uid= $this->input->get('pro');
			$d_id= $this->input->get('dept');
			 $u_id              = $this->session->userdata('user_id');
			 $role_id            = $this->session->userdata('role_id');
			 // if(!$d_id)
			 // {
			 // $dept_val = $this->user_model->fetchdept_data($u_id);
			// $dept_opt1=array_column($dept_val, 'dept_id');	
			// $d_id=$dept_opt1[0];
			 // }
			//$pro_sel_dta1= $this->user_model->sel_emp_data($file_nm,$st_dt, $e_dt, $u_id,$pids,$role_id,$dept_opt1);
			$l_data=array();
				$le_values2= $this->user_model->select_leave_reportee1("Others",$u_id,$uid,$st_dt,$e_dt,$lvl,$uid,$role_id,$d_id);
			 if($le_values2)
				 { 	
					$l_data = array_merge($l_data,$le_values2);
				 }
				$le_values3= $this->user_model->select_leave_reportee1("Comp Off",$u_id,$uid,$st_dt,$e_dt,$lvl,$uid,$role_id,$d_id);
			  if($le_values3)
				 { 	
					$l_data = array_merge($l_data,$le_values3);
				 }			
		 $tab_dta=json_encode( $l_data );
		}		
		else if($file_nm=="Status_Report")
		{
			$day= $this->input->get('date_view');			
			$day_w  = date('w', strtotime($day));
				if ($day_w == 0) {
					$day_w = 7;
				}
				
				if ($this->useDateFilters) {
					$st_dt = $this->startDateFilter;
					$e_dt   = $this->endDateFilter;
				} else {
				$st_dt = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
				$e_dt   = date('Y-m-d', strtotime($day . ' +' . (7 - $day_w) . ' days'));
				}
			if($pro && in_array($pro,$pids1))
			{
            $tab_dta = $this->user_model->load_stat_boots($pro,$st_dt,$e_dt,$dept_opt);
			}else{
				$tab_dta = json_encode (json_decode ("{}"));
			}
		}
		else if($file_nm=="Reportees")
		{
			$st_dt= $this->input->get('s_dt');			
			$e_dt= $this->input->get('e_dt');		
			$lvl= $this->input->get('lvl');						
			$uid= $this->input->get('pro');
			 $u_id              = $this->session->userdata('user_id');
			 $role_id            = $this->session->userdata('role_id');
			 $dept_val = $this->user_model->fetchdept_data($u_id);
		$dept_opt1=array_column($dept_val, 'dept_id');
		
			$pro_sel_dta1= $this->user_model->sel_emp_data($file_nm,$st_dt, $e_dt, $u_id,$pids,$role_id,$dept_opt1);
				if($uid && in_array($uid,array_column($pro_sel_dta1, 'sel_1_id')))
				{
				$tab_dta = $this->user_model->select_emp_reportee($file_nm,$pro,$st_dt,$e_dt,$lvl,$uid);
				}else{
					$tab_dta = json_encode (json_decode ("{}"));
				}
		}
		else if($file_nm=="R_Leave_Confirm")
		{
			$st_dt= $this->input->get('s_dt');			
			$e_dt= $this->input->get('e_dt');		
			$lvl= $this->input->get('lvl');						
			$uid= $this->input->get('pro');
			 $u_id              = $this->session->userdata('user_id');
			 $role_id            = $this->session->userdata('role_id');
			 $dept_val = $this->user_model->fetchdept_data($u_id);
			//$dept_opt1=implode(",",array_column($dept_val, 'dept_id'));	
			$dept_opt1=array_column($dept_val, 'dept_id');	
			$pro_sel_dta1= $this->user_model->sel_emp_data($file_nm,$st_dt, $e_dt, $u_id,$pids,$role_id,$dept_opt1);
			$l_data=array();
				$le_values2= $this->user_model->select_leave_reportee("Others",$u_id,$pro,$st_dt,$e_dt,$lvl,$uid,$role_id,$dept_opt1);
			 if($le_values2)
				 { 	
					$l_data = array_merge($l_data,$le_values2);
				 }
				 
				 $le_values1= $this->user_model->select_leave_reportee("Week Off",$u_id,$pro,$st_dt,$e_dt,$lvl,$uid,$role_id,$dept_opt1);
			 if($le_values1)
				 { 	
					$l_data = array_merge($l_data,$le_values1);
				 }
				
				$le_values3= $this->user_model->select_leave_reportee("Comp Off",$u_id,$pro,$st_dt,$e_dt,$lvl,$uid,$role_id,$dept_opt1);
			  if($le_values3)
				 { 	
					$l_data = array_merge($l_data,$le_values3);
				 }
				 
				// print_r($l_data);
		 $tab_dta=json_encode( $l_data );
		}
		else if($file_nm=="Team_Member_Info")
		{
			$day= $this->input->get('date_view');			
			$lvl= $this->input->get('lvl');			
			$day_w  = date('w', strtotime($day));
				if ($day_w == 0) {
					$day_w = 7;
				}
				// To Use Time Span Filters
				if ($this->useDateFilters) {
					$st_dt = $this->startDateFilter;
					$e_dt   = $this->endDateFilter;
				} else {
				$st_dt = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
				$e_dt   = date('Y-m-d', strtotime($day . ' +' . (7 - $day_w) . ' days'));
				}
				if($pro && in_array($pro,$pids1))
					{
					$tab_dta = $this->user_model->select_mem_reports($file_nm,$pro,$st_dt,$e_dt,$lvl,0,$dept_opt);
					}else{
						$role_id            = $this->session->userdata('role_id');
						//echo $role_id ;
						if($role_id==1)
						{
							$tab_dta = $this->user_model->select_mem_reports($file_nm,$pro,$st_dt,$e_dt,$lvl,0,$dept_opt);
						}else{
						$tab_dta = json_encode (json_decode ("{}"));
						}
					}
		}
		else if($file_nm=="PT_Report_Card")
		{
			$st_dt= $this->input->get('s_dt');			
			$e_dt= $this->input->get('e_dt');			
			$lvl= $this->input->get('lvl');			
			$pids= $this->session->userdata('proj_fk');	
			$pt_emp=$this->session->userdata('pt_emp');
			$role_id=$this->session->userdata('role_id');
			$u_id              = $this->session->userdata('user_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			//print_r($dept_val);
			$dept_opt=array_column($dept_val,"dept_id");

			if($role_id==6 || $role_id==5 || empty($pt_emp))
			{
				$pt_emp=$u_id;
			}				
			
			$sel_p=array();
			$final_sel_p=array();
			//$tab_dta1 = $this->user_model->pt_select_mem_reports($file_nm,$pids,$st_dt,$e_dt,$lvl,$pt_emp,$role_id,$dept_opt);
				if($lvl==1)
				{
					$tab_dta1 = $this->user_model->pt_select_mem_reports($file_nm,$pids,$st_dt,$e_dt,$lvl,$pt_emp,$role_id,$dept_opt);
					if($tab_dta1)
					{
						foreach($tab_dta1 AS $val)
						{
							$sel_p[]=$val['project_id'];
						}
						$final_sel_p=array_unique($sel_p);
						$pp=implode(",",$final_sel_p);
						//print_r($pp);
						$tab_dta2=array();
						$tab_dta2 = $this->user_model->pt_buddy_reports($file_nm,$st_dt,$e_dt,$role_id,$dept_opt,$pp);
						//print_r($tab_dta2);
						$tab_dta_f = array_merge($tab_dta1,$tab_dta2);
						$volume  =array();
						$edition  =array();
						$volume  = array_column($tab_dta_f, 'val_name');
						$edition = array_column($tab_dta_f, 'dis_nm');
						array_multisort($volume, SORT_ASC,$edition,SORT_ASC, $tab_dta_f);
						$tab_dta =json_encode($tab_dta_f);
						
					}else{
						$tab_dta =json_encode (json_decode ("{}"));
					}
				}else{
					$tab_dta = $this->user_model->pt_select_mem_reports($file_nm,$pids,$st_dt,$e_dt,$lvl,$pt_emp,$role_id,$dept_opt);
					$tab_dta =json_encode($tab_dta);
				}
		}
		else if($file_nm=="PT_Feedback")
		{
			$st_dt= $this->input->get('s_dt');			
			$e_dt= $this->input->get('e_dt');			
			$lvl= $this->input->get('lvl');			
			$pids= $this->session->userdata('proj_fk');	
			$pt_emp=$this->session->userdata('pt_emp');
			$role_id=$this->session->userdata('role_id');
			$u_id              = $this->session->userdata('user_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			//print_r($dept_val);
			$dept_opt=array_column($dept_val,"dept_id");

			if($role_id==6 || $role_id==5 || empty($pt_emp))
			{
				$pt_emp=$u_id;
			}				
			
			$sel_p=array();
			$final_sel_p=array();
			$tab_dta = $this->user_model->pt_select_mem_reports($file_nm,$pids,$st_dt,$e_dt,$lvl,$pt_emp,$role_id,$dept_opt);
					$tab_dta =json_encode($tab_dta);
		}
		else if($file_nm=="FV_Member_Info")
		{
			$day= $this->input->get('date_view');			
			$lvl= $this->input->get('lvl');			
			
			$day_w  = date('w', strtotime($day));
				if ($day_w == 0) {
					$day_w = 7;
				}
				$st_dt = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
				$e_dt   = date('Y-m-d', strtotime($day . ' +' . (7 - $day_w) . ' days'));
				
			$tab_dta = $this->user_model->select_mem_reports($file_nm,$pro,$st_dt,$e_dt,$lvl,0,$dept_opt);
		}
		else if($file_nm=="Emp_io_upload")
		{
			$day= $this->input->get('date_view');			
			$lvl= $this->input->get('lvl');					
			$day_w  = date('w', strtotime($day));
				if ($day_w == 0) {
					$day_w = 7;
				}
				if ($this->useDateFilters) {
					$st_dt = $this->startDateFilter;
					$e_dt   = $this->endDateFilter;
				} else {
				$st_dt = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
				$e_dt   = date('Y-m-d', strtotime($day . ' +' . (7 - $day_w) . ' days'));
				}
				//echo $pro;
					if($pro)
					{
					$tab_dta = $this->user_model->select_io_tbl($file_nm,$pro,$st_dt,$e_dt,$lvl,0,$dept_opt);
					}else{
						$tab_dta = json_encode (json_decode ("{}"));
					}
		}
		else if($file_nm=="Employee_Info")
		{
			$day= $this->input->get('date_view');			
			$lvl= $this->input->get('lvl');		
			$emp= $this->input->get('emp');
			$day_w  = date('w', strtotime($day));
				if ($day_w == 0) {
					$day_w = 7;
				}
				
				if ($this->useDateFilters) {
					$st_dt = $this->startDateFilter;
					$e_dt  = $this->endDateFilter;
				} else {
				$st_dt = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
				$e_dt   = date('Y-m-d', strtotime($day . ' +' . (7 - $day_w) . ' days'));
				}
				if($emp)
					{
					$tab_dta = $this->user_model->select_mem_reports($file_nm,$pro,$st_dt,$e_dt,$lvl,$emp,$dept_opt);
					}else{
						$tab_dta = json_encode (json_decode ("{}"));
					}
		}
		else if($file_nm=="Status_Report_37")
		{
			$day= date('Y-m-d');
			$day_w  = date('w', strtotime($day));
				if ($day_w == 0) {
					$day_w = 7;					
				}
				
				$st_dt_0 = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
				if($day_w == 1)
				{
					$tab_dta = json_encode (json_decode ("{}"));
				}else{
					$st_dt3 = date('Y-m-d', strtotime($day . ' -' . (3) . ' days'));
					$e_dt3   = date('Y-m-d', strtotime($day . '-' . (1) . ' days'));
					if($day_w <= 3)
					{
						$st_dt3 = $st_dt_0;
					}
				
				$st_dt7 = $st_dt_0;
				$e_dt7  = date('Y-m-d', strtotime($day . '-' . (1) . ' days'));
				if($pro && in_array($pro,$pids1))
					{
				$tab_dta = $this->user_model->l_stat_boots($pro,$st_dt3,$e_dt3,$st_dt7,$e_dt7,$dept_opt);
				}else{
						$tab_dta = json_encode (json_decode ("{}"));
					}
				}
		}
		echo ($tab_dta);				
	}
	public function load_proj_data()
	{
		$pids= $this->session->userdata('proj_fk');
		$dept_opt = $this->input->post('dept');
		$param = $this->input->post('param');
	
		if($param=="Project" || $param=="ProjectTM")
		{
		$load_du=$this->user_model->sel_pro_data($pids,$dept_opt);
		}
		
		if($param=="Project_overall")
		{
		$load_du=$this->user_model->sel_pro_data(0,$dept_opt);
		}
		if($param=="Spread" || $param=="Feed")
		{
		$load_du=$this->user_model->sel_pro_data($pids,$dept_opt);
		}
		if($param=="Emp")
		{
			$file_nm = $this->input->post('file_name');
			$date_view = $this->input->post('date_view');
			$day_w             = date('w', strtotime($date_view));
        if ($day_w == 0) {
            $day_w = 7;
        }
		
		 $st_dt = date('Y-m-d', strtotime($date_view . ' -' . ($day_w - 1) . ' days'));
		 $e_dt   = date('Y-m-d', strtotime($date_view . ' +' . (7 - $day_w) . ' days'));
		$load_du=$this->user_model->sel_emp_data($file_nm,$st_dt, $e_dt, 0,$pids,3,$dept_opt);
		}
		$output    = '';
            if ($load_du) {
				if($param=="Spread")
				{
					$output .= "<option value='0'>OverAll</option>";
				}
				if($param=="ProjectTM")
				{
					$rol=$this->session->userdata('role_id');
					if($rol==1)
					{
					$output .= "<option value='0'>All Projects</option>";
					}
				}
                foreach ($load_du as $roww) {
                    $output .= "<option value='" . $roww['sel_1_id'] . "'>" . $roww['sel_1_name'] . "</option>";
                }
            }
            echo $output;
	}
	
	public function load_conf_boots()
	{
		$file_nm            = $this->input->get('a');
		$tab            = $this->input->get('tab');
		$dept_opt = $this->input->get('dept');
		$pids= $this->session->userdata('proj_fk');
		$user_id            = $this->session->userdata('user_id');
		$iss_pages_var = array('Confirmation_Process','Audit','Allocation','Review','Confirm');
        if(in_array($file_nm, $iss_pages_var)) {
			$st_dt=$this->input->get('st');
			$e_dt=$this->input->get('en');
			if(!($st_dt && $e_dt))
			{
				$date_chk = new DateTime(null, new DateTimeZone('Asia/Kolkata'));
			    $ch_d=$date_chk->format('Y-m-d');
				$ch_t=$date_chk->format('H');
				$day_w  = date('w', strtotime($ch_d));				
				//$s_dt =$date_chk->format('Y-m-d');
				$s_dt = date('Y-m-d', strtotime($ch_d . ' -' . ($day_w - 1) . ' days'));
				$minus_val=array("Audit"=>array(6,1),"Allocation"=>array(13,-6),"Review"=>array(13,-6),"Confirm"=>array(13,-6));
				// if(in_array($user_id,array(1,82,14,130)))
				// {
					// //echo "dsfjsd";
					// $minus_val=array("Audit"=>array(13,-6),"Allocation"=>array(13,-6),"Review"=>array(13,-13),"Confirm"=>array(13,-13));
				// }
				$time_ch=$this->config->item('time_check');
				if(array_key_exists($file_nm,$minus_val))
				{
					if($s_dt==$ch_d && $ch_t<=$time_ch[$file_nm])
					{
						$st_dt = date('Y-m-d', strtotime($ch_d . ' -' . ($day_w + $minus_val[$file_nm][0]) . ' days'));
					}else{
						if ($day_w == 0) {
							$day_w = 7;
						}
						$st_dt = date('Y-m-d', strtotime($ch_d . ' -' . ($day_w - $minus_val[$file_nm][1]) . ' days'));
					}
				}
			}			
			//echo $st_dt;
			$tab_dta = $this->user_model->load_conf_boots($file_nm, $pids,$st_dt,$e_dt,$user_id,$tab,$dept_opt);			
		}			
		echo $tab_dta;				
	}
   	
    public function load_view_f()
    {
		
		// Get date filter if any or default dates
		$dateFilters = $this->getDateFilters();
		$data['startDate'] = $dateFilters['startDate'];
		$data['endDate'] = $dateFilters['endDate'];
		$data['startDateFilter'] = $this->startDateFilter;
		$data['endDateFilter'] = $this->endDateFilter;
		$data['weekPrv'] = $dateFilters['weekPrv'];
		$data['weekNxt'] = $dateFilters['weekNxt'];
		$data['timeSpanType'] = $this->timeSpanType;
		$data['useDateFilters'] = $this->useDateFilters;
		//
		
		
        $file_nm            = $this->input->get('a');
		$data['tab']=$this->input->get('tab');
		$date_view          = $this->input->get('date_view');
		$date_s          = $this->input->get('s_dt');
		$date_e          = $this->input->get('e_dt');
		$pro_sel_val          = $this->input->get('pro');	
        $dept_opt = $this->input->get('dept');
		$dept_session  = $this->session->userdata('dept_id');
		$u_id = $this->session->userdata('user_id');
		$allocation_dept_val = $this->allocation_model->fetchPermissionDept($u_id);
		$data['allocation_dept_val']=!empty($allocation_dept_val)?$allocation_dept_val[0]['dept_id']:'';
		if(!$u_id){
			redirect('user/login_view');
		}	
		$dept_val = $this->user_model->fetchdept_data($u_id);
		$data["dept_val"]=$dept_val;
		if($dept_opt)
		{
			$this->session->set_userdata('dept_id',$dept_opt);
		}else{
		if(!$dept_opt && !$dept_session)
		{
			$dept_opt=$dept_val[0]['dept_id'];
		}else if($dept_session){
			$dept_opt=$dept_session;
		}
		}
		$data['dept_opt']=$dept_opt;
				
        $data['access_mst'] = $this->session->userdata('access_mst');
        $data['projects']   = $this->session->userdata('proj_fk');
        if($date_view)
		{
			$data['date_view']  = $date_view;
			$day               = date('Y-m-d H:i:s', strtotime($date_view));
		}else{
        $data['date_view'] = '';
        $day               = date('Y-m-d H:i:s');
		}
        $day_w             = date('w', strtotime($day));
        if ($day_w == 0) {
            $day_w = 7;
        }
		if($file_nm == 'Floor_View' || $file_nm == 'FV_Breakdown' || $file_nm == 'FV_Drilldown' || $file_nm == 'FV_Summary' || $file_nm == 'FV_Feed' || $file_nm == 'WPR_Feed')
		{
			$date_s='';
			$date_e='';
		}
		//$this->output->enable_profiler(TRUE);
        if($date_s && $date_e)
		{
			$date_start=$date_s;
			$date_end=$date_e;
			
			
			// Handling dates when timeSpan Filters are avialble in url
			if ($this->useDateFilters) {
				$date_start = date('Y-m-d', strtotime($this->startDateFilter));
				$date_end = date('Y-m-d', strtotime($this->endDateFilter));
			}
			
			$data['s_dt']=$date_start;
			$data['e_dt']=$date_end;
			
		}else{
        $date_start = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
        $date_end   = date('Y-m-d', strtotime($day . ' +' . (7 - $day_w) . ' days'));
			
			if ($file_nm == 'Reportees' || $file_nm == 'Graph_Trends') {
				$data['s_dt']='';
				$data['e_dt']='';
			}
			if ($file_nm == 'R_Team_Graphs') {
				$data['s_dt']='';
				$data['e_dt']='';
			}
			if ($file_nm == 'R_Breakdown') {
				$data['s_dt']='';
				$data['e_dt']='';
			}
			if ($file_nm == 'R_Leave_Confirm') {
				$data['s_dt']='';
				$data['e_dt']='';
			}
			if ($file_nm == 'Po_Leave_Confirm') {
				$data['s_dt']='';
				$data['e_dt']='';
			}
			if ($file_nm == 'FV_Spread' || $file_nm == 'Trend_Report' ||  $file_nm == 'PT_Stats' ||$file_nm == 'PT_Trend_Report' ||  $file_nm == 'PT_NonQ_Share' ||$file_nm == 'NonQ_Share' || $file_nm == 'FV_Wow' || $file_nm == 'Top_Bottom') {
				$day = date('Y-m-d H:i:s');
				$day_w = date('w',strtotime($day));
				if($day_w==0)
				{
					$day_w=7;								
				}							
				$week_end = date('Y-m-d', strtotime($day.' +'.(7-$day_w).' days'));
				
				$week_start = date('Y-m-d', strtotime($week_end.' -1 month'));
				$week_st_w = date('w',strtotime($week_start));
				if($week_st_w==0)
				{
					$week_st_w=7;								
				}							
				$start_dt = date('Y-m-d', strtotime($week_start.' -'.($week_st_w-1).' days'));
				//$end_dt = date('d-M-Y',strtotime($week_end));
				$data['s_dt']=$start_dt;
				 $date_start =$start_dt;
				$data['e_dt']=$week_end;
				$date_end  =$week_end;
			}
        }
		$data['st_dt']=$date_start;
		$data['en_dt']=$date_end;
        
		if($file_nm == 'Graph_Trends'  || $file_nm== 'PT_Trends'||  $file_nm==  'Status_Report'|| $file_nm==  'Emp_io_upload'|| $file_nm==  'Employee_Leave'|| $file_nm==  'Employee_Info' || $file_nm==  'Team_Member_Info'  || $file_nm==  'Team_Split_Graph' || $file_nm==  'Team_Graphs' ||  $file_nm== 'Graph_Sum_Sc' || $file_nm== 'Graph_Summary' || $file_nm== 'Graph_Breakdown' || $file_nm== 'Graph_Drilldown' ) {
				$data['s_dt']='';
				$data['e_dt']='';
				//echo $date_start;
			if ($this->useDateFilters) {
				$date_start = $this->startDateFilter;
				$date_end   = $this->endDateFilter;
			}else{
				$date_start = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
				$date_end   = date('Y-m-d', strtotime($day . ' +' . (7 - $day_w) . ' days'));
			}
			
		}
        
        if ($file_nm == 'Daily_Tasks') {
            $data['cal_data'] = $this->user_model->select_cal_view($date_start, $date_end, $u_id);
        }
		
		if ($file_nm == 'PT_NonQ_Share') {
			$u_id = $this->session->userdata('user_id');	
			$pids= $this->session->userdata('proj_fk');
			$role_id            = $this->session->userdata('role_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$data["dept_val"]=$dept_val;
			$dept_opt1=array_column($dept_val,"dept_id");
			$data['dept_opt']=$dept_opt1;
			$data['dept_opt']=$dept_opt1[0];
			$pt_emp=$this->session->userdata('pt_emp');
			if( strpos($pt_emp, ",") !== false ) {
					 $pt_emp=$u_id;
					 $this->session->set_userdata('pt_emp', $pt_emp);
				}
			if(!$pt_emp || $role_id==6 || $role_id==5)
			{
				$pt_emp=$u_id;
			}		
			$data['pro_sel_val']=$pt_emp;
			$data['pro_sel_dta'] = $this->user_model->pt_sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			
			
			$data2 = $this->user_model->pt_get_not_quantifiable($file_nm,$date_start,$date_end,$pt_emp);
			$data1=array();
				   if($data2){				   		
					 foreach ($data2 as $row1) {
						 $data1[$row1['week_val']]=round(($row1['total_x']/$row1['all_total_x'])*100,2);
					 }
				   }
				   $data['avg']=$data1;			
		}
		if ($file_nm == 'PT_Stats') {
			$u_id = $this->session->userdata('user_id');	
			$pids= $this->session->userdata('proj_fk');
			$role_id            = $this->session->userdata('role_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$data["dept_val"]=$dept_val;
			$dept_opt1=array_column($dept_val,"dept_id");
			$data['dept_opt']=$dept_opt1;
			$data['dept_opt']=$dept_opt1[0];
			$pt_emp=$this->session->userdata('pt_emp');
			if( strpos($pt_emp, ",") !== false ) {
					 $pt_emp=$u_id;
					 $this->session->set_userdata('pt_emp', $pt_emp);
				}
			if(!$pt_emp || $role_id==6 || $role_id==5)
			{
				$pt_emp=$u_id;
			}		
			$data['pro_sel_val']=$pt_emp;
			$data['pro_sel_dta'] = $this->user_model->pt_sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			
				$data['avg']='';
				$data['min'] ='';
				$data['max'] ='';
				$data['sum'] ='';
				
				$min_max = $this->user_model->pt_team_member_select($file_nm, $date_start, $pt_emp, $date_end);
				 foreach ($min_max as $row1) {
					 $data['min']=round($row1['Max_val'],2);
					 $data['max']=round($row1['Min_val'],2);
					 $data['sum']=round($row1['Sum_val'],2);
					 $data['avg']=round($row1['Avg_val'],2);
				 }
				$data['act_n']=array();
				$data['act_n2']=array();
				$act_hj = $this->user_model->pt_team_member_select3($file_nm, $date_start, $pt_emp, $date_end,"xp");
				if($act_hj)
				{
				 foreach ($act_hj as $row1) {
					 $data['act_n'][]=array("a_name"=>$row1['act_nm'],"xp_val"=>round($row1['totl_xp'],2));
				 }	
				}
				$act_hj2 = $this->user_model->pt_team_member_select3($file_nm, $date_start, $pt_emp, $date_end,"score");
				if($act_hj2)
				{
				 foreach ($act_hj2 as $row1) {
					 $data['act_n2'][]=array("a_name"=>$row1['act_nm'],"score"=>$row1['totl_xp']);
				 }	
				}
		}
		// if ($file_nm == 'PT_Feedback') {
			// $u_id = $this->session->userdata('user_id');	
			// $pids= $this->session->userdata('proj_fk');
			// $role_id            = $this->session->userdata('role_id');
			// $dept_val = $this->user_model->fetchdept_data($u_id);
			// $data["dept_val"]=$dept_val;
			// $dept_opt1=array_column($dept_val,"dept_id");
			// $data['dept_opt']=$dept_opt1;
			// $data['dept_opt']=$dept_opt1[0];
			// $pt_emp=$this->session->userdata('pt_emp');
			// if( strpos($pt_emp, ",") !== false ) {
					 // $pt_emp=$u_id;
					 // $this->session->set_userdata('pt_emp', $pt_emp);
				// }
			// if(!$pt_emp || $role_id==6 || $role_id==5)
			// {
				// $pt_emp=$u_id;
			// }		
			// $data['pro_sel_val']=$pt_emp;
			// $data['pro_sel_dta'] = $this->user_model->pt_sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
						
				// $data['user_plot']=array();
				// $act_hj = $this->user_model->pt_scatter_plot($file_nm, $date_start, $pt_emp, $date_end);
				// if($act_hj)
				// {
				 // foreach ($act_hj as $row1) {
					 // $data['user_plot'][]=array("x"=>$row1['esp_wrk_hrs'],"y"=>$row1['act_xp'],"value"=>3,"format"=>$row1['val_name']);
				 // }	
				// }
					
		// }
		if ($file_nm == 'PT_Trend_Report') {
			$u_id = $this->session->userdata('user_id');	
			$pids= $this->session->userdata('proj_fk');
			$role_id            = $this->session->userdata('role_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$data["dept_val"]=$dept_val;
			$dept_opt1=array_column($dept_val,"dept_id");
			//$data['dept_opt']=$dept_opt1;
			$data['dept_opt']=$dept_opt1[0];
			$pt_emp=$this->session->userdata('pt_emp');
			if( strpos($pt_emp, ",") !== false ) {
					 $pt_emp=$u_id;
					 $this->session->set_userdata('pt_emp', $pt_emp);
				}
			if(!$pt_emp || $role_id==6 || $role_id==5)
			{
				$pt_emp=$u_id;
			}		
			$data['pro_sel_val']=$pt_emp;
			$data['pro_sel_dta'] = $this->user_model->pt_sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			
			$get_avg_exp= $this->user_model->pt_get_emp_xps($date_start,$date_end,$pt_emp);
			$data1=array();
			if(!empty($get_avg_exp)){
			 foreach ($get_avg_exp as $row1) {
				 $data1[$row1['week_val']]['Avg_val']=round($row1['Avg_val'],2);
				  $data1[$row1['week_val']]['v1']=round($row1['v1'],2);
				  $data1[$row1['week_val']]['v2']=round($row1['v2'],2);
				  $data1[$row1['week_val']]['tem_cnt']=$row1['tem_cnt'];
					//$data1[$row1['week_val']]['Avg_per_day']=round($row1['Avg_per_day'],2);
			 }			 
			}

			$get_avg_exp2= $this->user_model->pt_get_emp_xps2($date_start,$date_end,$pt_emp);	

			if(!empty($get_avg_exp2)){
			 foreach ($get_avg_exp2 as $row1) {
					$data1[$row1['week_val']]['Avg_per_day']=round($row1['Avg_per_day'],2);
			 }			 
			}			
			$data['get_avg_exp']=$data1;
		}
		if ($file_nm== 'PT_Feedback') {
			$data['level_opt'] =$this->input->get('lvl');
			$data['start_dt'] = ($date_start);
			$data['end_dt'] = ($date_end);
			$u_id = $this->session->userdata('user_id');	
			$pids= $this->session->userdata('proj_fk');
			$role_id            = $this->session->userdata('role_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$data["dept_val"]=$dept_val;
			$dept_opt1=array_column($dept_val,"dept_id");
			$data['dept_opt']=$dept_opt1;
			$data['dept_opt']=$dept_opt1[0];
			$pt_emp=$this->session->userdata('pt_emp');
			if(!$pt_emp || $role_id==6 || $role_id==5)
			{
				$pt_emp=$u_id;
			}
			
			$data['pro_sel_val']=$pt_emp;
			$data['pro_sel_dta'] = $this->user_model->pt_sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			
        }
		if ($file_nm== 'PT_Trends') {
			
			$u_id = $this->session->userdata('user_id');	
			$pids= $this->session->userdata('proj_fk');
			$role_id            = $this->session->userdata('role_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$data["dept_val"]=$dept_val;
			$dept_opt1=array_column($dept_val,"dept_id");
			$data['dept_opt']=$dept_opt1[0];
			$pt_emp=$this->session->userdata('pt_emp');
			if( strpos($pt_emp, ",") !== false ) {
					 $pt_emp=$u_id;
					 $this->session->set_userdata('pt_emp', $pt_emp);
				}
			if(!$pt_emp || $role_id==6 || $role_id==5)
			{
				$pt_emp=$u_id;
			}		
			$sset = $this->user_model->pt_trend_status($file_nm, $date_start,$pt_emp, $date_end,$dept_opt1);  
			$data['avg']=$sset;
			$data['pro_sel_val']=$pt_emp;
			$data['pro_sel_dta'] = $this->user_model->pt_sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
        }
		 if ($file_nm== 'PT_Pies') {
			$data['s_dt'] = ($date_start);
			$data['e_dt'] = ($date_end);
			
			$u_id = $this->session->userdata('user_id');	
			$pids= $this->session->userdata('proj_fk');
			$role_id            = $this->session->userdata('role_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$data["dept_val"]=$dept_val;
			$dept_opt1=array_column($dept_val,"dept_id");
			$data['dept_opt']=$dept_opt1[0];
			$pt_emp=$this->session->userdata('pt_emp');
			if(!$pt_emp || $role_id==6 || $role_id==5)
			{
				$pt_emp=$u_id;
			}
			
			$data['pro_sel_val']=$pt_emp;
			$data['pro_sel_dta'] = $this->user_model->pt_sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			$data['tm_data_all'] = $this->user_model->pt_team_member_select2($file_nm, $date_start, $pt_emp, $date_end, $u_id,"all",$dept_opt1);
        }
		if ($file_nm== 'PT_Pie_Grading') {
			$data['s_dt'] = ($date_start);
			$data['e_dt'] = ($date_end);
			
			$u_id = $this->session->userdata('user_id');	
			$pids= $this->session->userdata('proj_fk');
			$role_id            = $this->session->userdata('role_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$data["dept_val"]=$dept_val;
			$dept_opt1=array_column($dept_val,"dept_id");
			$data['dept_opt']=$dept_opt1[0];
			$pt_emp=$this->session->userdata('pt_emp');
			if(!$pt_emp || $role_id==6 || $role_id==5)
			{
				$pt_emp=$u_id;
			}
			
			$data['pro_sel_val']=$pt_emp;
			$data['pro_sel_dta'] = $this->user_model->pt_sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			$data['tm_data_all'] = $this->user_model->pt_team_member_select2($file_nm, $date_start, $pt_emp, $date_end, $u_id,"all",$dept_opt1);
        }
		if ($file_nm== 'PT_Pie_Reviews') {
			$data['s_dt'] = ($date_start);
			$data['e_dt'] = ($date_end);
			
			$u_id = $this->session->userdata('user_id');	
			$pids= $this->session->userdata('proj_fk');
			$role_id            = $this->session->userdata('role_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$data["dept_val"]=$dept_val;
			$dept_opt1=array_column($dept_val,"dept_id");
			$data['dept_opt']=$dept_opt1[0];
			$pt_emp=$this->session->userdata('pt_emp');
			if(!$pt_emp || $role_id==6 || $role_id==5)
			{
				$pt_emp=$u_id;
			}
			
			$data['pro_sel_val']=$pt_emp;
			$data['pro_sel_dta'] = $this->user_model->pt_sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			$data['tm_data_all'] = $this->user_model->pt_team_member_select2($file_nm, $date_start, $pt_emp, $date_end, $u_id,"all",$dept_opt1);
        }
		if ($file_nm== 'PT_Report_Card') {
			$data['level_opt'] =$this->input->get('lvl');
			$data['start_dt'] = ($date_start);
			$data['end_dt'] = ($date_end);
			$u_id = $this->session->userdata('user_id');	
			$pids= $this->session->userdata('proj_fk');
			$role_id            = $this->session->userdata('role_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$data["dept_val"]=$dept_val;
			$dept_opt1=array_column($dept_val,"dept_id");
			$data['dept_opt']=$dept_opt1;
			$data['dept_opt']=$dept_opt1[0];
			$pt_emp=$this->session->userdata('pt_emp');
			if(!$pt_emp || $role_id==6 || $role_id==5)
			{
				$pt_emp=$u_id;
			}
			
			$data['pro_sel_val']=$pt_emp;
			$data['pro_sel_dta'] = $this->user_model->pt_sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			
        }
        if ($file_nm == 'Reports') {
			//$this->output->enable_profiler(TRUE); 
			$u_id = $this->session->userdata('user_id');	
			$pids= $this->session->userdata('proj_fk');
			$role_id            = $this->session->userdata('role_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$data["dept_val"]=$dept_val;
			$dept_opt1=array_column($dept_val,"dept_id");
			$data['dept_opt']=$dept_opt1;
			$data['dept_opt']=$dept_opt1[0];
			$pt_emp=$this->session->userdata('pt_emp');
			if(!$pt_emp || $role_id==6 || $role_id==5)
			{
				$pt_emp=$u_id;
				$this->session->set_userdata('pt_emp', $pt_emp);
			}
			
			$data['pro_sel_val']=$pt_emp;
			$data['pro_sel_dta'] = $this->user_model->pt_sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			
            $graph_data = $this->user_model->select_proj_xp($date_start, $date_end, $pt_emp);
			//$in_out_data = $this->user_model->select_in_out_data($date_start, $date_end, $pt_emp);
            $proj_xp            = array();
            $actual_xp          = array();
			$in_time          = array();
			$wrk_hrs          = array();
			$sapience_on          = array();
			$user_1=array();
			$dates_values=array();
			$final_user=array();
			$sctter=array();
            if ($graph_data) {
                foreach ($graph_data AS $row) {
                    $proj_xp[$row['user_id']][$row['day_val']]   = $row['proj_xp'];
                    $actual_xp[$row['user_id']][$row['day_val']] = $row['act_xp'];
					$in_time[$row['user_id']][$row['day_val']]   = $row['in_time_f'];
                    $wrk_hrs[$row['user_id']][$row['day_val']] = $row['esp_wrk_hrs_f'];
					$sapience_on[$row['user_id']][$row['day_val']] = $row['sapience_on_f'];
					$user_1[$row['user_id']]=$row['full_name'];
					$dates_values[]=$row['day_val'];
	$sctter[]=array("x"=>$row['esp_wrk_h'],"dt"=>$row['dt_val'],"y"=>$row['act_xp'],"y2"=>$row['proj_xp'],"format"=>$row['user_id']);
                }
            }
			$dates_values=array_unique($dates_values);
			$data['sctter']=$sctter;
					
			$grades          = array();
			$f_grades          = array();
			$grades_dt       = array();
			
			$f_grades=array("1"=>"Needs Awareness","2"=>"Needs Improvement","3"=>"Meets Expectation",
			"4"=>"Exceeds Expectation","5"=>"Strongly Exceeds Expectation");
			
			
			$grade_data = $this->user_model->select_grade_dt($date_start, $date_end, $pt_emp);
			if ($grade_data) {
                foreach ($grade_data AS $row) {
                    //$grades_dt[]=array("act"=>$row['week_val'],"work"=>$row['fg_val'],"user"=>$row['full_name']);
					$grades_dt[$row['full_name']][$row['week_val']]=$row['fg_val'];
					$user_1[$row['user_id']]=$row['full_name'];
					//$grades[]=$row['value'];
                }
            }
			//print_r($grades_dt);
			$final_user=array_unique($user_1);
			//print_r($grades_dt);
			
			$data['start_dt'] = ($date_start);
			$data['end_dt'] = ($date_end);
            $data['actual_xp'] = ($actual_xp);
            $data['proj_xp']   = ($proj_xp);
			$data['in_time']   = ($in_time);
			$data['wrk_hrs']   = ($wrk_hrs);
			$data['sapience_on']   = ($sapience_on);
			$data['final_user']   = ($final_user);
			$data['f_grades']   = ($f_grades);
			$data['dates_values']=$dates_values;
			$data['grades_dt']   = ($grades_dt);
        }
        $pids = $this->session->userdata('proj_fk');
		$iss_pages_var = array('Issues','Issues_comp','Issues_reject');
        if(in_array($file_nm, $iss_pages_var)) {
        	$iStatus = $this->input->get("issue_status");
			$issueStatus = $iStatus ? $iStatus : "1";
			$st_dt='';
			$e_dt='';
			$data["issueStatus"] =$issueStatus;
			$data['iss_cnt_val'] = $this->user_model->load_issue_cnt($file_nm, $pids,$st_dt,$e_dt,$dept_opt);
		}
		
		$conf_pages_var = array('Confirmation_Process','Audit','Allocation','Review','Confirm');
        if(in_array($file_nm, $conf_pages_var)) {
			$st_dt='';
			$e_dt='';
			
			if(!($st_dt && $e_dt))
			{
				$date_chk = new DateTime(null, new DateTimeZone('Asia/Kolkata'));
			    $ch_d=$date_chk->format('Y-m-d');
				$ch_t=$date_chk->format('H');
				$day_w  = date('w', strtotime($ch_d));		
				//$s_dt =$date_chk->format('Y-m-d');				
				$s_dt = date('Y-m-d', strtotime($ch_d . ' -' . ($day_w - 1) . ' days'));
				$minus_val=array(13,-6,6,1);
				
				// if($u_id ==3)
				// {
					// $minus_val=array(13,-6,13,-6);
				// }
				
				// if($dept_opt ==2)
				// {
					 // $minus_val=array(13,-6,13,-6);
				// }
				
				// if(in_array($u_id,array(1,82,14,130)))
				// {
					// $minus_val=array(0,-13,0,-6);
					// //echo "dsfjsd";
					// //$minus_val=array("Audit"=>array(13,-6),"Allocation"=>array(13,-6),"Review"=>array(13,-13),"Confirm"=>array(13,-13));
				// }
				$time_ch=$this->config->item('time_check');
					if($s_dt==$ch_d && $ch_t<=$time_ch[$file_nm])
					{
						$st_dt = date('Y-m-d', strtotime($ch_d . ' -' . ($day_w + $minus_val[0]) . ' days'));
						$e_dt = date('Y-m-d', strtotime($ch_d . ' -' . ($day_w + $minus_val[2]) . ' days'));
					}else{
						if ($day_w == 0) {
							$day_w = 7;
						}
						$st_dt = date('Y-m-d', strtotime($ch_d . ' -' . ($day_w - $minus_val[1]) . ' days'));
						$e_dt = date('Y-m-d', strtotime($ch_d . ' -' . ($day_w - $minus_val[3]) . ' days'));
					}
			//print_r($st_dt." | ".$e_dt);
			}
			$data['cnf_cnt_dta'] = $this->user_model->load_conf_cnt($file_nm, $pids,$st_dt,$e_dt, $u_id,$dept_opt);
				if($file_nm=='Allocation')
				{
					$data['ch_reviewer']=$this->user_model->load_rev_data($file_nm, $pids,$dept_opt);
					//print_r($data['ch_reviewer']);
				}
		}
		
        $tm_pages = array(
            'Team_Graphs',
            'Team_Split_Graph');
			if($pids!='0' && ($pids))
			{
			$pids1=explode(",",$pids);	
			}else{
			$pids1=array($pro_sel_val);	
			}
        if (in_array($file_nm, $tm_pages)) {
		$lvl=$this->input->get('lvl');
		if($lvl>=6)
		{
			$lvl=1;
		}
		$data['level_opt'] =$lvl;
			if($pro_sel_val && in_array($pro_sel_val,$pids1))
			{
				$data['tm_data'] = $this->user_model->team_member_select($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,$lvl,$dept_opt);   
			}else{
              $data['tm_data'] ='';
			}
            $data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);   
        }
		if ($file_nm == 'Trend_Report') {
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
			$get_avg_exp= $this->user_model->get_emp_xps($date_start,$date_end,$dept_opt,$pro_sel_val);
			//print_r($get_avg_exp);
			$data1=array();
			$eff_max=0;
			if(!empty($get_avg_exp)){
			 foreach ($get_avg_exp as $row1) {
				 $data1[$row1['week_val']]['Avg_val']=round($row1['Avg_val'],2);
				  $data1[$row1['week_val']]['v1']=round($row1['v1'],2);
				  $data1[$row1['week_val']]['v2']=round($row1['v2'],2);
				  $data1[$row1['week_val']]['tem_cnt']=$row1['tem_cnt'];
				  $data1[$row1['week_val']]['Avg_per_day']=0;
				  $eff_max = max( array( $eff_max, round($row1['Avg_val'],2),round($row1['v1'],2), round($row1['v2'],2),round($row1['tem_cnt'],2)) );
					//$data1[$row1['week_val']]['Avg_per_day']=round($row1['Avg_per_day'],2);
			 }			 
			}

			$get_avg_exp2= $this->user_model->get_emp_xps2($date_start,$date_end,$dept_opt,$pro_sel_val);	
			//print_r($get_avg_exp2);

			if(!empty($get_avg_exp2)){
			 foreach ($get_avg_exp2 as $row1) {
					$data1[$row1['week_val']]['Avg_per_day']=round($row1['Avg_per_day'],2);
					$eff_max = max( array( $eff_max,round(($row1['Avg_per_day']/0.2),2)));
			 }			 
			}			
			$data['get_avg_exp']=$data1;
			$data['grap_max']=$eff_max;
		}
		if ($file_nm == 'NonQ_Share') {
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
			$data2 = $this->user_model->get_not_quantifiable($file_nm,$date_start,$date_end,$dept_opt,$pro_sel_val);
			$data1=array();
				   if($data2){
				   		
					 foreach ($data2 as $row1) {
						  if($row1['all_total_x']!=0)
					     {
						 $data1[$row1['week_val']]=round(($row1['total_x']/$row1['all_total_x'])*100,2);
					     }else{
					      $data1[$row1['week_val']]=0;   
					     }
						// $data1[$row1['week_val']]=round(($row1['total_x']/$row1['all_total_x'])*100,2);
					 }
				   }
				   $data['avg']=$data1;			
		}
		 if ($file_nm == 'Top_Bottom') {
				$lvl = $this->input->get('lvl');
			if($lvl>=6 || !($lvl))
				{
					$lvl=0;
				}
				$data['level_opt'] =$lvl;
				$data1=array();
				if($pro_sel_val)
				{
				// $min_max  = $this->user_model->fv_summ_top_b($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"1",$dept_opt,$lvl);
				
				// if($min_max)
				// {
				 // foreach ($min_max as $row1) {					 
					 // $data1[$row1['week_val']]['Max']=round($row1['v1'],2);
					 // $data1[$row1['week_val']]['Min']=round($row1['v2'],2);
					 // // $data1[$row1['week_val']]['Cont']=round($row1['team_cont'],0);
					 // // $data1[$row1['week_val']]['Avg']=round($row1['Avg_val'],0);
					 // // $data1[$row1['week_val']]['Mem']=$row1['team_mem'];
				 // }
				// }

					$sset = $this->user_model->fv_summ_top_b($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"2",$dept_opt,$lvl); 
				  
				 if($sset)
				 { 				
					$values=array();
					foreach($sset as $roo)
					{
						$values[$roo['week_val']][]=round($roo['equiv_xp'],2);
					}
					//print_r($values);
					foreach ($values as $key=>$subarr) {
						$data1[$key]['Max']=max($subarr);
						$data1[$key]['Min']=min($subarr);
					}
					foreach($values AS $key1=>$value1)
					{
						$data1[$key1]['Q1']=$this->user_model->get_percentile(25, $value1);
						$data1[$key1]['Q3']=$this->user_model->get_percentile(75, $value1);	
					}
				 }
				}
				
			$data['avg']=$data1;
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);   
        }
		if ($file_nm== 'Targets') {
            $data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
			$ty=$this->input->get('d');	
			if(!$ty)
			{
				$ty=1;
			}
			$data['t_status']=$ty;
        }
		// if ($file_nm== 'Week_Targets') {
            // // $data['pro_sel_val']=$pro_sel_val;
			// // $data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
			// $ty=$this->input->get('d');	
			// if(!$ty)
			// {
				// $ty=1;
			// }
			// $data['t_status']=$ty;
        // }
		if ($file_nm== 'Status_Report_37') {
            $data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);   
        }
		
		$data['checkaccess'] = $this->user_model->haveAccessworkRequest();
        if ($file_nm== 'apply_work_request' || $file_nm== 'apply_od_request') {
			$pids= $this->session->userdata('proj_fk');
            $data['pro_sel_va']=$pro_sel_val;
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$dept_opt=$dept_val[0]['dept_id'];
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
			$role_id            = $this->session->userdata('role_id');			
			$data['pro_sel'] = $this->user_model->sel_emp_data($file_nm,$date_start, $date_end,$u_id,$pids,$role_id,$dept_opt);			  
        }        
		if ($file_nm== 'Graph_Sum_Sc') {
				$data['avg']='';
				$data['Q1per']='';
				$data['Q3per']='';
				$data['Quart1']=array();
				$data['Quart3']=array();
				$data['tm_data_min'] =array();
				$data['tm_data_max'] =array();
			if($pro_sel_val && in_array($pro_sel_val,$pids1))
			{
				$min_max = $this->user_model->tm_stats($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,0,$dept_opt);
				 foreach ($min_max as $row1) {					 
					 $data['avg']=round($row1['Avg_val'],2);
					 $Sum=$row1['Sum_val'];
				 }
				 				
					$sset = $this->user_model->get_data_perc($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,$dept_opt); 
					//print_r($sset);
					$values=array();
					if($sset)
					{
					foreach($sset as $roo)
					{
						array_push($values,round($roo['equiv_xp'],2));	
					}
					
					$Q1=$this->user_model->get_percentile(25, $values);
					$Q3=$this->user_model->get_percentile(75, $values);
						$data['Quart1']=array();
						$data['Quart3']=array();
					 $sum_Q1=0;
					 $sum_Q3=0;
					$min=min($values);
					
					$max=max($values);
					
					foreach($sset as $row2)
					{
						if($row2['equiv_xp']==$min)
						{
							$data['tm_data_min'][$row2['full_name']]=array('e_xp'=>round($min,2),'a_c'=>$row2['att_cnt']);
						}
						if($row2['equiv_xp']==$max)
						{
							$data['tm_data_max'][$row2['full_name']]=array('e_xp'=>round($max,2),'a_c'=>$row2['att_cnt']);
						}						
						if($row2['equiv_xp']<$Q1)
						{
						$data['Quart1'][$row2['full_name']]=array('e_xp'=>round($row2['equiv_xp'],2),'a_c'=>$row2['att_cnt']);
						$sum_Q1=$sum_Q1+$row2['equiv_xp'];
						}
						if($row2['equiv_xp']>=$Q3)
						{
							$data['Quart3'][$row2['full_name']]=array('e_xp'=>round($row2['equiv_xp'],2),'a_c'=>$row2['att_cnt']);
							$sum_Q3=$sum_Q3+$row2['equiv_xp'];
						}
					}
					$data['Q1per']=$Sum==0 ? 0 : round(($sum_Q1/$Sum)*100,2);
					$data['Q3per']=$Sum==0 ? 0 : round(($sum_Q3/$Sum)*100,2);
			}
			}
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
        }
			if ($file_nm== 'Graph_Summary') {
				$data['avg']='';
				$data['Q1per']='';
				$data['Q3per']='';
				$data['Quart1']=array();
				$data['Quart3']=array();
				$data['Quart1_mem']=array();
				$data['Quart3_mem']=array();
				$data['tm_data_min'] =array();
				$data['tm_data_max'] =array();
			if($pro_sel_val && in_array($pro_sel_val,$pids1))
			{
				$min_max = $this->user_model->tm_stats($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,0,$dept_opt);
				 foreach ($min_max as $row1) {
					 $data['avg']=round($row1['Avg_val'],2);
					 $Sum=$row1['Sum_val'];
				 }
				 
				 $min_max1 = $this->user_model->tm_stats("Graph_Sum_Sc", $date_start, $pro_sel_val, $date_end, $u_id,0,$dept_opt);
				 foreach ($min_max1 as $row1) {
					 $Sum_m=$row1['Sum_val'];
				 }
				 
					$sset = $this->user_model->get_data_perc($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,$dept_opt);
					$sset1 = $this->user_model->get_data_perc("Graph_Sum_Sc", $date_start, $pro_sel_val, $date_end, $u_id,$dept_opt);
				if($sset)
				 {					
					$values=array();
					foreach($sset as $roo)
					{
						array_push($values,round($roo['equiv_xp'],2));
					}
					$Q1=$this->user_model->get_percentile(25, $values);
					$Q3=$this->user_model->get_percentile(75, $values);
					 $data['Quart1']=array();
					 $data['Quart3']=array();
					 $sum_Q1=0;
					 $sum_Q3=0;
					 $min=min($values);
					
					$max=max($values);
					
					foreach($sset as $row2)
					{
						if($row2['equiv_xp']==$min)
						{
							$data['tm_data_min'][$row2['full_name']]=array('e_xp'=>round($min,2),'c_xp'=>round($row2['total_cp'],2),'a_c'=>$row2['att_cnt']);
						}
						if($row2['equiv_xp']==$max)
						{
							$data['tm_data_max'][$row2['full_name']]=array('e_xp'=>round($max,2),'c_xp'=>round($row2['total_cp'],2),'a_c'=>$row2['att_cnt']);
						}						
						if($row2['equiv_xp']<$Q1)
						{
						$data['Quart1'][$row2['full_name']]=array('e_xp'=>round($row2['equiv_xp'],2),'c_xp'=>round($row2['total_cp'],2),'a_c'=>$row2['att_cnt']);
						//$sum_Q1=$sum_Q1+$row2['equiv_xp'];
						}
						if($row2['equiv_xp']>=$Q3)
						{
							$data['Quart3'][$row2['full_name']]=array('e_xp'=>round($row2['equiv_xp'],2),'c_xp'=>round($row2['total_cp'],2),'a_c'=>$row2['att_cnt']);
							//$sum_Q3=$sum_Q3+$row2['equiv_xp'];
						}
					}
					// $data['Q1per']=$Sum==0 ? 0 : round(($sum_Q1/$Sum)*100,2);
					// $data['Q3per']=$Sum==0 ? 0 : round(($sum_Q3/$Sum)*100,2);
				 }
				 
				 if($sset1)
				 {					
					$values1=array();
					foreach($sset1 as $roo)
					{
						array_push($values1,round($roo['equiv_xp'],2));
					}
					$Q1_mem=$this->user_model->get_percentile(25, $values1);
					$Q3_mem=$this->user_model->get_percentile(75, $values1);
					 $data['Quart1_mem']=array();
					 $data['Quart3_mem']=array();
					 $sum_Q1_m=0;
					 $sum_Q3_m=0;
					
					foreach($sset1 as $row2)
					{					
						if($row2['equiv_xp']<$Q1_mem)
						{
						$data['Quart1_mem'][$row2['full_name']]=array('e_xp'=>round($row2['equiv_xp'],2),'a_c'=>$row2['att_cnt']);
						$sum_Q1_m=$sum_Q1_m+$row2['equiv_xp'];
						}
						if($row2['equiv_xp']>=$Q3_mem)
						{
							$data['Quart3_mem'][$row2['full_name']]=array('e_xp'=>round($row2['equiv_xp'],2),'a_c'=>$row2['att_cnt']);
							$sum_Q3_m=$sum_Q3_m+$row2['equiv_xp'];
						}
					}
					$data['Q1per']=$Sum==0 ? 0 : round(($sum_Q1_m/$Sum_m)*100,2);
					$data['Q3per']=$Sum==0 ? 0 : round(($sum_Q3_m/$Sum_m)*100,2);
				 }
			}
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
        }
		if ($file_nm== 'R_Team_Graphs')
		{
			$lvl=$this->input->get('lvl');
			$data['level_opt'] =$lvl;
			
			$role_id            = $this->session->userdata('role_id');
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$dept_opt1=array_column($dept_val, 'dept_id');	
			$data['pro_sel_val']=$pro_sel_val;
			$pro_sel_dta1= $this->user_model->sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			$data['la_cnt_val'] = $this->user_model->load_la_cnt($file_nm,$u_id,1,$date_start,$date_end,0,$pro_sel_val,$role_id,$dept_opt1);
			$data['la_wee_cnt_val'] = $this->user_model->load_la_cnt($file_nm,$u_id,2,$date_start,$date_end,0,$pro_sel_val,$role_id,$dept_opt1);
			$data['pro_sel_dta']=$pro_sel_dta1;
			if($pro_sel_val && in_array($pro_sel_val,array_column($pro_sel_dta1, 'sel_1_id')))
			{
				$data['tm_data'] = $this->user_model->emp_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,$lvl);   
			}else{
              $data['tm_data'] ='';
			}
		}
		if ($file_nm== 'R_Leave_Confirm')
		{
			$role_id            = $this->session->userdata('role_id');
			$data['level_opt'] =$this->input->get('lvl');
			$data['pro_sel_val']=$pro_sel_val;
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$dept_opt1=array_column($dept_val, 'dept_id');	
			$data['pro_sel_dta'] = $this->user_model->sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			$data['la_cnt_val'] = $this->user_model->load_la_cnt($file_nm,$u_id,1,$date_start,$date_end,0,$pro_sel_val,$role_id,$dept_opt1);
			$data['la_wee_cnt_val'] = $this->user_model->load_la_cnt($file_nm,$u_id,2,$date_start,$date_end,0,$pro_sel_val,$role_id,$dept_opt1);
		}
		if ($file_nm== 'Po_Leave_Confirm')
		{
			$role_id            = $this->session->userdata('role_id');
			$data['level_opt'] =$this->input->get('lvl');
			$data['pro_sel_val']=$pro_sel_val;
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$dept_opt1=array_column($dept_val, 'dept_id');	
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
			//$data['pro_sel_dta'] = $this->user_model->sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			$data['la_cnt_val'] = $this->user_model->load_la_cnt($file_nm,$u_id,1,$date_start,$date_end,0,$pro_sel_val,$role_id,$dept_opt1);
			$data['la_wee_cnt_val'] = $this->user_model->load_la_cnt($file_nm,$u_id,2,$date_start,$date_end,0,$pro_sel_val,$role_id,$dept_opt1);
		}
		if ($file_nm== 'R_Breakdown') {
			$role_id            = $this->session->userdata('role_id');
			$data['pro_sel_val']=$pro_sel_val;
			$dept_val = $this->user_model->fetchdept_data($u_id);
			$dept_opt1=array_column($dept_val, 'dept_id');	
			$pro_sel_dta1= $this->user_model->sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			$data['pro_sel_dta']=$pro_sel_dta1;
			
			$data['la_cnt_val'] = $this->user_model->load_la_cnt($file_nm,$u_id,1,$date_start,$date_end,0,$pro_sel_val,$role_id,$dept_opt1);
			$data['la_wee_cnt_val'] = $this->user_model->load_la_cnt($file_nm,$u_id,2,$date_start,$date_end,0,$pro_sel_val,$role_id,$dept_opt1);
			if($pro_sel_val && in_array($pro_sel_val,array_column($pro_sel_dta1, 'sel_1_id')))
			{
				$data['tm_data1'] = $this->user_model->emp_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"task");   
				$data['tm_data2'] = $this->user_model->emp_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"sub");   
				$data['tm_data3'] = $this->user_model->emp_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"job");   
				$data['tm_data4'] = $this->user_model->emp_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"trade");   
			}else{
              $data['tm_data1'] ='';
			  $data['tm_data2'] ='';
			  $data['tm_data3'] ='';
			  $data['tm_data4'] ='';
			}
        }		
        if ($file_nm== 'Graph_Breakdown' || $file_nm== 'Graph_Drilldown') {
			if($pro_sel_val && in_array($pro_sel_val,$pids1))
			{
				if($file_nm== 'Graph_Breakdown')
				{
				$data['tm_data1'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"task",$dept_opt);   
				$data['tm_data2'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"sub",$dept_opt);   
				$data['tm_data3'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"job",$dept_opt);   
				$data['tm_data4'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"trade",$dept_opt);   
				$data['tm_data5'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"mid",$dept_opt);   
				$data['tm_data6'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"user",$dept_opt); 
				}else{
				$data['tm_data_all'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"all",$dept_opt);	
				}
			}else{
              $data['tm_data1'] ='';
			  $data['tm_data2'] ='';
			  $data['tm_data3'] ='';
			  $data['tm_data4'] ='';
			  $data['tm_data5'] ='';
			  $data['tm_data6'] ='';
			  $data['tm_data_all'] ='';
			}
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
        }
		 if ($file_nm== 'FV_Breakdown' || $file_nm== 'FV_Drilldown') {
			 $pro_sel_val='';
					if($file_nm== 'FV_Breakdown')
					{
				$data['tm_data1'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"project",$dept_opt);   
				$data['tm_data2'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"trade",$dept_opt);   
				 $data['tm_data3'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"sub",$dept_opt);   
				 $data['tm_data4'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"job",$dept_opt);
				 $data['tm_data5'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"mid",$dept_opt);
				$data['tm_data6'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"user",$dept_opt);
					}else{
				$data['tm_data_all'] = $this->user_model->team_member_select2($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"all",$dept_opt);
					}
				
        }
		if ($file_nm== 'FV_Summary') {
			 $pro_sel_val='';
				$proj1=array();
				$max1=array();
				$min1=array();
				$data1=array();
				$Sum1=array();
				$sum_Q1=array();
				$v1=array();
				$v2=array();
					 $sum_Q3=array();
					 $data['tm_data1']=array();
				$min_max  = $this->user_model->fv_summ($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"1",$dept_opt);
				$data['avg']=array() ;
				if($min_max)
				{
				 foreach ($min_max as $row1) {
					 array_push($proj1,$row1['project_name']);
					 $sum_Q1[$row1['project_name']]=0;
					 $sum_Q3[$row1['project_name']]=0;
					 $data['tm_data1'][$row1['project_name']]=array('v1'=>0,'v2'=>0);
					 $Q13per[$row1['project_name']]['Bottom']=0;
					 $Q13per[$row1['project_name']]['Top']=0;
					// $v2[$row1['project_name']]=0;
					 $data1[$row1['project_name']]['Avg']=round($row1['Avg_val'],2);
					 //$Sum1[$row1['project_name']]=$row1['Sum_val'];
					 $data1[$row1['project_name']]['Mem']=$row1['team_mem'];
				 }
				}
				
				
				$get_avg_exp2= $this->user_model->get_em_descriptives($date_start,$date_end,$dept_opt);	

			if(!empty($get_avg_exp2)){
			 foreach ($get_avg_exp2 as $row1) {
					$data1[$row1['project_name']]['Avg_per_day']=round($row1['Avg_per_day'],2);
					$data1[$row1['project_name']]['nq_su']=round($row1['nq_sum']*100,2);
			 }			 
			}	
			
				 $sset = $this->user_model->fv_summ($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"2",$dept_opt);   
				 if($sset)
				 { 				
					$values=array();
					foreach($sset as $roo)
					{
						$values[$roo['project_name']][]=round($roo['equiv_xp'],2);
					}
					//print_r($values);
					$Q1=array();
					$Q3=array();
					foreach($values AS $key1=>$value1)
					{
						$Q1[$key1]=$this->user_model->get_percentile(25, $value1);
						$Q3[$key1]=$this->user_model->get_percentile(75, $value1);	
					}
					
					$Quart1=array();
					$Quart3=array();
					$Q13per=array();
										 
					foreach($sset as $ky2)
					{
						if($ky2['equiv_xp']<$Q1[$ky2['project_name']])
						{
						//$Quart1[$ky2['project_name']][$ky2['full_name']]=$ky2['equiv_xp'];
						$sum_Q1[$ky2['project_name']]=$sum_Q1[$ky2['project_name']]+$ky2['equiv_xp'];
						}
						if($ky2['equiv_xp']>=$Q3[$ky2['project_name']])
						{
						//$Quart3[$ky2['project_name']][$ky2['full_name']]=$ky2['equiv_xp'];
						$sum_Q3[$ky2['project_name']]=$sum_Q3[$ky2['project_name']]+$ky2['equiv_xp'];
						}
														
					}
					
					foreach($proj1 as $ro)
					{
						if(array_key_exists($ro,$values))
						{
						$Sum1[$ro]=array_sum($values[$ro]);												
						$Q13per[$ro]['Bottom']=$Sum1[$ro]==0 ? 0 : round(($sum_Q1[$ro]/$Sum1[$ro])*100,2);
						$Q13per[$ro]['Top']=$Sum1[$ro]==0 ? 0 : round(($sum_Q3[$ro]/$Sum1[$ro])*100,2);						
						
						$data['tm_data1'][$ro]['v1']=min($values[$ro]);
						$data['tm_data1'][$ro]['v2']=max($values[$ro]);
						}
					}
					//print_r($data['tm_data1']);
					
					  $data['avg']=$data1;
					  $data['Q13per']=$Q13per;					
				 }else
				  {
					  $data['avg']=array();
					  $data['Q13per']=array();
				  }				
        }
		
		
		if ($file_nm== 'Floor_View') {
			//$this->output->enable_profiler(TRUE);
				$pro_sel_val='';
				$proj1=array();
				$proj2=array();
				$data1=array();
				$sum_Q1=array();
				$sum_Q3=array();
				$high_s=array();
				$low_s=array();
				$contrib=array();
				$high_c=array();
				$low_c=array();
				$min_max_c=array();
				$min_max =array();
				$o_min_xp=array();
				$o_max_xp=array();
				array_push($proj2,'OverAll');
				$min_max_c  = $this->user_model->fv_summ_c($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"1",$dept_opt);
				if($min_max_c)
				{				
					 $data1['OverAll']['Sum_c']=0;
					 $data1['OverAll']['Sum']=0;
					 $data1['OverAll']['Mem']=0;
					 $data1['OverAll']['Efficiency']=0;
					 $data1['OverAll']['effden']=0;
					 $data1['OverAll']['Mem_c']=0;
					 $sum_Q1['OverAll']=0;
					 $sum_Q3['OverAll']=0;
					 $data1['OverAll']['o_num']=0;
					 $data1['OverAll']['o_denom']=0;
					 $data1['OverAll']['A_stat']=0;
					 $data1['OverAll']['R_stat']=0;
					 $data1['OverAll']['D_stat']=0;
					 $data1['OverAll']['C_stat']=0;
					 $data1['OverAll']['nonq_sum']=0;
					 $data1['OverAll']['InOutAvg']=0;
					 $data1['OverAll']['A_grade']=0;
					 $data1['OverAll']['mediantime']=0;
					 //$data1['OverAll']['G_pending']=0;
					 $data1['OverAll']['G_done']=0;
					 
					 $avg_gde=array();
					 
				 foreach ($min_max_c as $row1) {
					 array_push($proj2,$row1['project_name']);
					 $high_c[$row1['project_name']]='';
					 $low_c[$row1['project_name']]='';
					 $data1[$row1['project_name']]['Max_c']=$row1['v1'];
					 $data1[$row1['project_name']]['A_stat']=$row1['A_stat'];
					 $data1[$row1['project_name']]['R_stat']=$row1['R_stat'];
					 $data1[$row1['project_name']]['C_stat']=$row1['C_stat'];
					 $data1[$row1['project_name']]['D_stat']=$row1['D_stat'];
					 
					 $data1['OverAll']['effden']=$data1['OverAll']['effden']+$row1['ov_eff_den'];
					  
					 $data1[$row1['project_name']]['Efficiency']=$row1['efficiency'];
					 $data1[$row1['project_name']]['nonq_sum']=$row1['nonq_sum'];
					// $data1[$row1['project_name']]['G_pending']=$row1['G_pending'];
					 $data1[$row1['project_name']]['G_done']=$row1['G_done'];
					 $data1[$row1['project_name']]['A_grade']=$row1['a_grade'];
					 if($row1['a_grade'])
					 {
					 $avg_gde[]=$row1['a_grade'];
					 }
					  $data1['OverAll']['A_stat']=$data1['OverAll']['A_stat']+$row1['A_stat'];
					 $data1['OverAll']['R_stat']=$data1['OverAll']['R_stat']+$row1['R_stat'];
					 $data1['OverAll']['D_stat']=$data1['OverAll']['D_stat']+$row1['D_stat'];
					 $data1['OverAll']['nonq_sum']=$data1['OverAll']['nonq_sum']+$row1['nonq_sum'];
					 $data1['OverAll']['C_stat']=$data1['OverAll']['C_stat']+$row1['C_stat'];
					 //$data1['OverAll']['G_pending']=$data1['OverAll']['G_pending']+$row1['G_pending'];
					 $data1['OverAll']['Mem_c']=$data1['OverAll']['Mem_c']+$row1['Mem_c'];
					 $data1['OverAll']['G_done']=$data1['OverAll']['G_done']+$row1['G_done'];
					 $o_max_xp[]=$row1['v1'];
					 $data1[$row1['project_name']]['Min_c']=$row1['v2'];
					 $o_min_xp[]=$row1['v2'];
					 $data1[$row1['project_name']]['Sum_c']=round($row1['Sum_val'],2);
					 $data1['OverAll']['Sum_c']=$data1['OverAll']['Sum_c']+ $data1[$row1['project_name']]['Sum_c'];
					 $data1[$row1['project_name']]['Max']='';
					 $data1[$row1['project_name']]['Mem']=$row1['team_mem'];
					 $data1[$row1['project_name']]['Mem_c']=$row1['Mem_c'];
					 $data1['OverAll']['Mem']=$data1['OverAll']['Mem']+$data1[$row1['project_name']]['Mem'];					 
					 $data1[$row1['project_name']]['Min']='';
					 $data1[$row1['project_name']]['Avg']='';
					 $data1[$row1['project_name']]['Sum']='';
					 $data1[$row1['project_name']]['Top']='';
					 $data1[$row1['project_name']]['Bottom']='';
					 $data1[$row1['project_name']]['High_sc']='';
					 $data1[$row1['project_name']]['Low_sc']='';
					 $data1[$row1['project_name']]['NQShare']='';
					 $data1[$row1['project_name']]['InOutAvg']='';
					 $data1[$row1['project_name']]['mediantime']='';
					 $sum_Q1[$row1['project_name']]=0;
					 $sum_Q3[$row1['project_name']]=0;
					 $high_s[$row1['project_name']]='';
					 $low_s[$row1['project_name']]='';
				 }
					if($avg_gde)
					{
					$data1['OverAll']['A_grade']=round(array_sum($avg_gde)/count($avg_gde),0);
					}
					//print_r($data1['OverAll']['A_grade']);
					 $data1['OverAll']['Max_c']=max($o_max_xp);
					 $data1['OverAll']['Min_c']=min($o_min_xp);
					 $oo_max_xp = array();
					 $oo_min_xp = array();
					 $data1['OverAll']['High_sc']='';
					 $data1['OverAll']['Low_sc']='';
					 $data1['OverAll']['NQShare']='';
			//$sset_c  = $this->user_model->fv_summ_c($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"2",$dept_opt);
					 
			 $min_max  = $this->user_model->fv_summ($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"1",$dept_opt);
				if($min_max)
				{					
					$no_pr=0;
				 foreach ($min_max as $row1) {
					 array_push($proj1,$row1['project_name']);					 
					 $data1[$row1['project_name']]['Max']=$row1['v1'];
					 $oo_max_xp[]=$row1['v1'];
					 $data1[$row1['project_name']]['Min']=$row1['v2'];
					 $oo_min_xp[]=$row1['v2'];
					 $data1[$row1['project_name']]['Avg']=round($row1['Avg_val'],2);
					 $data1[$row1['project_name']]['Sum']=round($row1['Sum_val'],2);					 
					  $data1[$row1['project_name']]['InOutAvg']=round($row1['workhrs'],2);
					  $data1[$row1['project_name']]['mediantime']=round($row1['mediantime'],2);
					 $data1['OverAll']['Sum']=$data1['OverAll']['Sum']+$data1[$row1['project_name']]['Sum'];
					 $data1['OverAll']['o_num']=$data1['OverAll']['o_num']+$row1['num'];
					 $data1['OverAll']['o_denom']=$data1['OverAll']['o_denom']+$row1['denom'];					 
					 $data1['OverAll']['mediantime']= $data1['OverAll']['mediantime']+$data1[$row1['project_name']]['mediantime'];
					 $no_pr++;
				 }
				 $data1['OverAll']['mediantime']=round(($data1['OverAll']['mediantime']/$no_pr),2);
				}
				
				
				if($data1['OverAll']['o_denom'])
				{
				$data1['OverAll']['InOutAvg']= round(($data1['OverAll']['o_num']/$data1['OverAll']['o_denom']),2);
				}else{
				$data1['OverAll']['InOutAvg']= 0;	
				}
				if($oo_max_xp && $oo_min_xp)
				{
					$data1['OverAll']['Max']=max($oo_max_xp);
					 $data1['OverAll']['Min']=min($oo_min_xp);				
				 }else{
					$data1['OverAll']['Max']=0;
					 $data1['OverAll']['Min']=0;				
				}
					 if($data1['OverAll']['effden'])
					 {
						$data1['OverAll']['Efficiency']=$data1['OverAll']['Sum']/$data1['OverAll']['effden'];
					 }else{
						 $data1['OverAll']['Efficiency']=0;
					 }
				 $data1['OverAll']['Avg']=$data1['OverAll']['Mem']==0 ? 0 : round(($data1['OverAll']['Sum']/$data1['OverAll']['Mem']),2);
				 $sset = $this->user_model->fv_summ($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"2",$dept_opt);
				 $over_values=array();
				 $values=array();
				if($sset)
				 {
					foreach($sset as $roo)
					{
						$values[$roo['project_name']][]=round($roo['equiv_xp'],2);
						$over_values[]=round($roo['equiv_xp'],2);
						if($roo['equiv_xp']==$data1[$roo['project_name']]['Min'])
						 {
							 $low_s[$roo['project_name']]=$low_s[$roo['project_name']].",".$roo['full_name'];
						 }
						 if($roo['equiv_xp']==$data1[$roo['project_name']]['Max'])
						 {
							 $high_s[$roo['project_name']]=$high_s[$roo['project_name']].",".$roo['full_name'];
						 }
						 
						 if($roo['equiv_xp']==$data1['OverAll']['Min'])
						 {
							 $data1['OverAll']['Low_sc']=$data1['OverAll']['Low_sc'].",".$roo['full_name'];
						 }
						 if($roo['equiv_xp']==$data1['OverAll']['Max'])
						 {
							 $data1['OverAll']['High_sc']=$data1['OverAll']['High_sc'].",".$roo['full_name'];
						 }						 
					}
				 }
				 
				 $Q1=array();
					$Q3=array();
					if($values)
					{
						foreach($values AS $key1=>$value1)
						{
							$Q1[$key1]=$this->user_model->get_percentile(25, $value1);
							$Q3[$key1]=$this->user_model->get_percentile(75, $value1);	
						}
					}
					$Q1['OverAll']=$this->user_model->get_percentile(25, $over_values);
					$Q3['OverAll']=$this->user_model->get_percentile(75, $over_values);	
					foreach($sset as $ky2)
					{
						if($ky2['equiv_xp']<$Q1[$ky2['project_name']])
						{
						$sum_Q1[$ky2['project_name']]=$sum_Q1[$ky2['project_name']]+$ky2['equiv_xp'];
						}
						if($ky2['equiv_xp']>=$Q3[$ky2['project_name']])
						{
						$sum_Q3[$ky2['project_name']]=$sum_Q3[$ky2['project_name']]+$ky2['equiv_xp'];
						}
						if($ky2['equiv_xp']<$Q1['OverAll'])
						{
						$sum_Q1['OverAll']=$sum_Q1['OverAll']+$ky2['equiv_xp'];
						}
						if($ky2['equiv_xp']>=$Q3['OverAll'])
						{
						$sum_Q3['OverAll']=$sum_Q3['OverAll']+$ky2['equiv_xp'];
						}
					}
					
					foreach($proj1 as $ro)
					{
						$data1[$ro]['Bottom']=$data1[$ro]['Sum']==0 ? '' : round(($sum_Q1[$ro]/$data1[$ro]['Sum'])*100,2)."%";
						$data1[$ro]['Top']=$data1[$ro]['Sum']==0 ? '' : round(($sum_Q3[$ro]/$data1[$ro]['Sum'])*100,2)."%";
						$data1[$ro]['High_sc']=substr($high_s[$ro],1);
						$data1[$ro]['Low_sc']=substr($low_s[$ro],1);
					}
					foreach($proj2 as $ro)
					{
					$data1[$ro]['NQShare']=$data1[$ro]['Sum_c']==0 ? '' : round(($data1[$ro]['nonq_sum']/$data1[$ro]['Sum_c'])*100,2)."%";
					}
					$data1['OverAll']['Bottom']=$data1['OverAll']['Sum']==0 ? '' : round(($sum_Q1['OverAll']/$data1['OverAll']['Sum'])*100,2)."%";
					$data1['OverAll']['Top']=$data1['OverAll']['Sum']==0 ? '' : round(($sum_Q3['OverAll']/$data1['OverAll']['Sum'])*100,2)."%";
					$data1['OverAll']['Low_sc']=substr($data1['OverAll']['Low_sc'],1);
					$data1['OverAll']['High_sc']=substr($data1['OverAll']['High_sc'],1);
$data1['OverAll']['NQShare']=$data1['OverAll']['Sum_c']==0 ? '':round(($data1['OverAll']['nonq_sum']/$data1['OverAll']['Sum_c'])*100,2)."%";
					  $data['avg']=$data1;					
					   $data['proj']=$proj2;	
						//print_r($data1);
				 }else
				  {
					  $data['avg']=array();
					  $data['proj']=array();					
				  }				
        }
		if ($file_nm== 'FV_Spread') {
				$proj1=array();
				$data1=array();
				$min_max  = $this->user_model->fv_summ($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"1",$dept_opt);
				//print_r($min_max);
				if($min_max)
				{
				 foreach ($min_max as $row1) {
					 array_push($proj1,$row1['week_val']);
					 $data1[$row1['week_val']]['Max']=round($row1['v1'],2);
					 $data1[$row1['week_val']]['Min']=round($row1['v2'],2);
					 $data1[$row1['week_val']]['Avg']=round($row1['Avg_val'],2);
					 $data1[$row1['week_val']]['Mem']=$row1['team_mem'];
					 $data1[$row1['week_val']]['Avg_per_day']='';
				 }
				}
				
				// $data['user_plot']=array();
				// if($pro_sel_val)
				// {
				// $act_hj = $this->user_model->pt_scatter_plot($file_nm, $date_start,$pro_sel_val, $date_end);
				// if($act_hj)
				// {
				 // foreach ($act_hj as $row1) {
					 // $data['user_plot'][]=array("x"=>$row1['esp_wrk_hrs'],"y"=>$row1['act_xp'],"value"=>3,"format"=>$row1['val_name']);
				 // }	
				// }
				// }
				
				$get_avg_exp2= $this->user_model->get_emp_xps2_dh($date_start,$date_end,$dept_opt,$pro_sel_val);	

			if(!empty($get_avg_exp2)){
			 foreach ($get_avg_exp2 as $row1) {
					$data1[$row1['week_val']]['Avg_per_day']=round($row1['Avg_per_day'],2);
			 }			 
			}	
			
				 $sset = $this->user_model->fv_summ($file_nm, $date_start, $pro_sel_val, $date_end, $u_id,"2",$dept_opt);   
				 if($sset)
				 { 				
					$values=array();
					foreach($sset as $roo)
					{
						$values[$roo['week_val']][]=round($roo['equiv_xp'],2);
					}
					foreach($values AS $key1=>$value1)
					{
						$data1[$key1]['Q1']=$this->user_model->get_percentile(25, $value1);
						$data1[$key1]['Q3']=$this->user_model->get_percentile(75, $value1);	
					}
				 }
				 //print_r($data1);
					$data['avg']=$data1;
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
        }
		
		
		if ($file_nm== 'FV_Feed') {
				$proj1=array();
				$data1=array();		
				$input_val=$this->input->get('input_val');
				$output_val=$this->input->get('output_val');
				$data1=array();
				if(!$input_val)
				{
					$input_val='esp_wrk_hrs';
				}
				if(!$output_val)
				{
					$output_val='act';
				}
				if($pro_sel_val)
				{
				//$act_hj = $this->user_model->pt_scatter_plot($file_nm, $date_start,$pro_sel_val, $date_end);
				$act_hj = $this->user_model->pt_scatter_plot($file_nm, $date_start,$pro_sel_val,$date_end,$input_val,$output_val);
				//print_r($act_hj);
				// if($act_hj)
				// {
				 // foreach ($act_hj as $row1) {
					 // $data1[]=array("x"=>$row1['esp_wrk_hrs'],"y"=>$row1['act_xp'],"y2"=>$row1['proj_xp'],"format"=>$row1['full_name']);
				 // }	
				// }
				
				if($act_hj & $input_val=='esp_wrk_hrs' & $output_val=='act')
				{
				 foreach ($act_hj as $row1) {
					 $data1[]=array("x"=>$row1['esp_wrk_hrs'],"y"=>$row1['act_xp'],"format"=>$row1['full_name'],"popovertextxp"=>'Confirmed XPS',"popovertexttype"=>"Work Hours");
				 }	
				}else if($act_hj & $input_val=='esp_wrk_hrs' & $output_val=='proj_xp'){ 
					foreach ($act_hj as $row1) {
					 $data1[]=array("x"=>$row1['esp_wrk_hrs'],"y"=>$row1['proj_xp'],"format"=>$row1['full_name'],"popovertextxp"=>'Projected XPS',
					 	"popovertexttype"=>"Work Hours");
					}
				}
				else if($act_hj & $input_val=='sapience_on' & $output_val=='act'){
					foreach ($act_hj as $row1) {
					 $data1[]=array("x"=>$row1['sapience_on'],"y"=>$row1['act_xp'],"format"=>$row1['full_name'],"popovertextxp"=>'Confirmed XPS',
					 	"popovertexttype"=>"Sapience Hours");
					}
				}
				else if($act_hj & $input_val=='sapience_on' & $output_val=='proj_xp'){
					foreach ($act_hj as $row1) {
					 $data1[]=array("x"=>$row1['sapience_on'],"y"=>$row1['proj_xp'],"format"=>$row1['full_name'],"popovertextxp"=>'Projected XPS',
					 	"popovertexttype"=>"Sapience Hours");
					}
				}
				}
				//print_r($data1);
					$data['avg']=$data1;
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
        }
		
		if ($file_nm== 'WPR_Feed') {
				$proj1=array();
				$data1=array();		
				$input_val=$this->input->get('input_val');
				$output_val=$this->input->get('output_val');
				if(!$input_val)
				{
					$input_val='esp_wrk_hrs';
				}
				if(!$output_val)
				{
					$output_val='act';
				}
				$data1=array();
				if($pro_sel_val)
				{
				//$act_hj = $this->user_model->pt_scatter_plot($file_nm, $date_start,$pro_sel_val, $date_end);
				$act_hj = $this->user_model->pt_scatter_plot($file_nm, $date_start,$pro_sel_val,$date_end,$input_val,$output_val);
				//print_r($act_hj);
				// if($act_hj)
				// {
				 // foreach ($act_hj as $row1) {
					 // $data1[]=array("x"=>$row1['esp_wrk_hrs'],"y"=>$row1['act_xp'],"y2"=>$row1['proj_xp'],"format"=>$row1['full_name']);
				 // }	
				// }
				
				if($act_hj & $input_val=='esp_wrk_hrs' & $output_val=='act')
				{
				 foreach ($act_hj as $row1) {
					 $data1[]=array("x"=>$row1['esp_wrk_hrs'],"y"=>$row1['act_xp'],"format"=>$row1['full_name'],"popovertextxp"=>'Confirmed XPS',"popovertexttype"=>"Work Hours");
				 }	
				}else if($act_hj & $input_val=='esp_wrk_hrs' & $output_val=='proj_xp'){ 
					foreach ($act_hj as $row1) {
					 $data1[]=array("x"=>$row1['esp_wrk_hrs'],"y"=>$row1['proj_xp'],"format"=>$row1['full_name'],"popovertextxp"=>'Projected XPS',
					 	"popovertexttype"=>"Work Hours");
					}
				}
				else if($act_hj & $input_val=='sapience_on' & $output_val=='act'){
					foreach ($act_hj as $row1) {
					 $data1[]=array("x"=>$row1['sapience_on'],"y"=>$row1['act_xp'],"format"=>$row1['full_name'],"popovertextxp"=>'Confirmed XPS',
					 	"popovertexttype"=>"Sapience Hours");
					}
				}
				else if($act_hj & $input_val=='sapience_on' & $output_val=='proj_xp'){
					foreach ($act_hj as $row1) {
					 $data1[]=array("x"=>$row1['sapience_on'],"y"=>$row1['proj_xp'],"format"=>$row1['full_name'],"popovertextxp"=>'Projected XPS',
					 	"popovertexttype"=>"Sapience Hours");
					}
				}
				}
				//print_r($data1);
					$data['avg']=$data1;
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
        }
		if ($file_nm== 'Graph_Trends') {
			if($pro_sel_val)
			{
			$sset = $this->user_model->trend_status($file_nm, $date_start,$pro_sel_val, $date_end,$dept_opt);  
			$data['avg']=$sset;
			}else{
				$data['avg']='';
			}
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
        }
		
		if ($file_nm == 'FV_Contributor') {
		$data['projectList'] = $data['contributorList'] = '';	
			$lvl = $this->input->get('lvl');
			if($lvl>=6 || !($lvl))
				{
					$lvl=0;
				}
				
				$data['level_opt'] =$lvl;
		
			$projectList = $contributorList =array();;
			$p_list=array();
			// first fetch all data according to type of contributer and week dates 
			$contributors = $this->user_model->getWeeklyContributors($date_start, $date_end, $lvl,$dept_opt);
			if($contributors)
			{
				foreach($contributors AS $wer)
				{
					$p_list[$wer['project_id_fk']] = $wer['project_name'];
				}
			}
			
			$my_array=array();
			if($p_list)
			{
			foreach($p_list AS $p_id=>$p_name)
			{
				foreach($contributors AS $row)
				{
					if($p_id==$row['project_id_fk'])
					{
					$my_array[$p_id][]= $row['full_name'];
					}
				}

			}
			}
			
			
			$counts=array();
			$max=0;
			if($my_array)
			{
			foreach($my_array AS $row)
			{
				$counts[] = sizeof($row);
			}
			$max=max($counts);
			}
			
			
			$data['projectList'] = $p_list;
			$data['contributorList'] = $my_array;
			$data['max_cnt'] = $max;
		}
		
		if ($file_nm== 'FV_Wow') {
			//$this->output->enable_profiler(TRUE);
			$cn_dt          = $this->input->get('cn_dt');
			$nq          = $this->input->get('pro');
			$o_n_c  = $this->input->get('const');
			if(!$nq || $nq>1)
			{
				$nq=0;
			}
			if(!$cn_dt)
			{
				$res_cn = $this->user_model->fv_cn_dt($file_nm, $date_start,$date_end,'Tgt_Date');
				foreach ($res_cn AS $row) {
					$cn_dt=$row['def_val'];
				}
			}
			$data["cn_dt"]=$cn_dt;
			$data["nq"]=$nq;
			$data["o_n_c"]=$o_n_c;
				$data1=array();	
				 $sset = $this->user_model->fv_wo($file_nm, $date_start,$date_end, $cn_dt,$nq,$dept_opt);
				// print_r( $sset);
				 if($sset)
				 {				
					$values=array();
					$tot_working_days=array();
					$q_dgf = $this->user_model->fv_cn_dt($file_nm, $date_start,$date_end,'Day_Count');					
					if($q_dgf)
					{
						foreach ($q_dgf AS $row) {
						$tot_working_days[$row['w_val']]=$row['def_val'];
						}
					}
					
					foreach($sset as $roo)
					{
						$we=$roo['week_val'];
						$pr=$roo['project_name'];
						$values[$we][$pr]=$roo['equ_x'];
						$values[$we]['OverAll']=(array_key_exists('OverAll',$values[$we])?$values[$we]['OverAll']:0)+$roo['equ_x'];
					}
				 }
				  $memset = $this->user_model->fv_wo_mem($file_nm, $date_start,$date_end,$cn_dt,$nq,1,$dept_opt,$o_n_c);
				 if($memset)
				 { 
					foreach($memset as $roo)
					{
						$we=$roo['week_val'];
						$pr=$roo['project_name'];
						//print_r($values[$we][$pr]."  | ".$roo['mem']."=");
						if(array_key_exists($we,$values))
						{	
						if(array_key_exists($pr,$values[$we]) && $roo['mem']!=0.0 && $tot_working_days[$we]!=0)
						{
							if($o_n_c==1)
							{
							$data1[$we][$pr]=($values[$we][$pr])/($roo['mem']);
							}else{
							$data1[$we][$pr]=($values[$we][$pr]/$roo['mem'])/$tot_working_days[$we];
							}
						}else{
						$data1[$we][$pr]=0;
						}
						}else{
						$data1[$we][$pr]=0;
						}
					}
				 }
				// echo $o_n_c;
				 $memset_o= $this->user_model->fv_wo_mem($file_nm, $date_start,$date_end,$cn_dt,$nq,2,$dept_opt,$o_n_c);
				 if($memset_o)
				 { 				
					
					foreach($memset_o as $roo)
					{
						$we=$roo['week_val'];
						//print_r($values[$we]['OverAll']."  | ".$roo['mem']."\n");
						if(array_key_exists($we,$values))
						{
						if($roo['mem']!=0.0 && $tot_working_days[$we]!=0)
						{
							if($o_n_c==1)
							{
							$data1[$we]['OverAll']=($values[$we]['OverAll'])/($roo['mem']) ;
							}else{
							$data1[$we]['OverAll']=($values[$we]['OverAll']/$roo['mem'])/$tot_working_days[$we];
							}
						}else{
							$data1[$we]['OverAll']=0;
						}			
						}else{
							$data1[$we]['OverAll']=0;
						}							
					}
				 }
					$data['avg']=$data1;
        }
		if ($file_nm== 'Status_Report') {
			   $data['f'] ='0';	
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
        }
		if ($file_nm== 'Reportees') {
			$role_id            = $this->session->userdata('role_id');
			$data['level_opt'] =$this->input->get('lvl');
			//$lvl=$data['level_opt'];
			$data['pro_sel_val']=$pro_sel_val;
			 $dept_val = $this->user_model->fetchdept_data($u_id);
			$dept_opt1=array_column($dept_val, 'dept_id');		
			$data['pro_sel_dta'] = $this->user_model->sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,$role_id,$dept_opt1);
			$data['la_cnt_val'] = $this->user_model->load_la_cnt($file_nm,$u_id,1,$date_start,$date_end,0,$pro_sel_val,$role_id,$dept_opt1);
			$data['la_wee_cnt_val'] = $this->user_model->load_la_cnt($file_nm,$u_id,2,$date_start,$date_end,0,$pro_sel_val,$role_id,$dept_opt1);
        }
		if ($file_nm== 'Team_Member_Info') {
			$data['level_opt'] =$this->input->get('lvl');
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
        }
		if ($file_nm== 'Employee_Leave') {
			$data['level_opt'] =$this->input->get('lvl');
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
        }
		if ($file_nm== 'FV_Member_Info') {
			$data['level_opt'] =$this->input->get('lvl');
			$data['pro_sel_val']=$pro_sel_val;
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
        }
		if ($file_nm== 'Employee_Info') {
			$data['level_opt'] =$this->input->get('lvl');			
			//$empsd=$this->input->get('emp');
			$data['pro_sel_val']=$this->input->get('emp');
			$data['pro_sel_dta'] = $this->user_model->sel_emp_data($file_nm,$date_start, $date_end, $u_id,$pids,3,$dept_opt);
        }
		
		// if ($file_nm== 'Emp_Present') {
			// $data['level_opt'] =$this->input->get('lvl');
			// if($data['level_opt']>=2)
			// {
				// $data['level_opt']=1;
			// }
			// $data['pro_sel_val']=$pro_sel_val;
			// $data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
			// $data['cal_data'] = $this->user_model->pa_cal_view($date_start, $date_end, $u_id,$pro_sel_val);
        // }
		
		if ($file_nm== 'Emp_io_upload') {
			$data['level_opt'] =$this->input->get('lvl');
			//$role_id            = $this->session->userdata('role_id');
			if($data['level_opt']>=2)
			{
				$data['level_opt']=1;
			}
			$data['pro_sel_val']=$pro_sel_val;
			
			if($data['level_opt']==1)
			{
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
			}else{
			//$data['pro_sel_val']=$this->input->get('emp');
			$data['pro_sel_dta'] = $this->user_model->sel_emp_data("Employee_Info",$date_start, $date_end, $u_id,$pids,3,$dept_opt);
			}
			//$data['cal_data'] = $this->user_model->pa_cal_view($date_start, $date_end, $u_id,$pro_sel_val);
        }
		
		if ($file_nm== 'Emp_io_upload_utility') {
			$data['level_opt'] =$this->input->get('lvl');
			//$role_id            = $this->session->userdata('role_id');
			if($data['level_opt']>=2)
			{
				$data['level_opt']=1;
			}
			$data['pro_sel_val']=$pro_sel_val;
			
			if($data['level_opt']==1)
			{
			$data['pro_sel_dta'] = $this->user_model->sel_pro_data($pids,$dept_opt);
			}else{
			//$data['pro_sel_val']=$this->input->get('emp');
			$data['pro_sel_dta'] = $this->user_model->sel_emp_data("Employee_Info",$date_start, $date_end, $u_id,$pids,3,$dept_opt);
			}
			//$data['cal_data'] = $this->user_model->pa_cal_view($date_start, $date_end, $u_id,$pro_sel_val);
        }
		
		$role_id            = $this->session->userdata('role_id');
		//$data['r_id']=$role_id;
		$data["metrics"] = $this->user_model->fetchMetrics($u_id,$role_id,$dept_opt);
		
		
		if ($file_nm== 'Leave_Tracker') {
			$data['lev_type'] = $this->user_model->sel_leave_type('leave_type_mst');
			//$s_dt='';
			$l_day = date('Y-m-d H:i:s',strtotime($day));
			$s_dt=date('Y-m-01',strtotime($l_day));
			
			$l_data=array();
			
			$le_values= $this->user_model->sel_emp_leaves($u_id,$s_dt,"Week Off");
			 if($le_values)
				 { 				
					foreach($le_values as $roo)
					{
						$we=$roo['w_day'];
						$l_data[$we][]=array("w_id"=>$roo['l_type_id'],"w_val"=>$roo['w_val'],"w_b_code"=>$roo['w_b_code'],"w_color_code"=>$roo['w_color_code']);
						
					}
				 }
			$le_values1= $this->user_model->sel_emp_leaves($u_id,$s_dt,"Declared Hols");
			 if($le_values1)
				 { 				
					foreach($le_values1 as $roo)
					{
						$we=$roo['w_day'];
						$l_data[$we][]=array("w_id"=>$roo['l_type_id'],"w_val"=>$roo['w_val'],"w_b_code"=>$roo['w_b_code'],"w_color_code"=>$roo['w_color_code']);
						
					}
				 }
				
				$le_values2= $this->user_model->sel_emp_leaves($u_id,$s_dt,"Others");
			 if($le_values2)
				 { 				
					foreach($le_values2 as $roo)
					{
						$we=$roo['w_day'];
						$l_data[$we][]=array("w_id"=>$roo['l_type_id'],"w_val"=>$roo['w_val'],"w_b_code"=>$roo['w_b_code'],"w_color_code"=>$roo['w_color_code']);
						
					}
				 }
				 
				 $le_values3= $this->user_model->sel_emp_leaves($u_id,$s_dt,"Comp Off");
			 if($le_values3)
				 { 				
					foreach($le_values3 as $roo)
					{
						$we=$roo['w_day'];
						$l_data[$we][]=array("w_id"=>$roo['l_type_id'],"w_val"=>$roo['w_val'],"w_b_code"=>$roo['w_b_code'],"w_color_code"=>$roo['w_color_code']);
						
					}
				 }
				 $data['l_data']=$l_data;
        }
		
		$loc_val          = $this->input->get('loc');
		if ($file_nm== 'Operations') {
			$data['check']=array("Department"=>"dept_mst","User"=>"user_mst","Roles"=>"role_mst","Access"=>"access_mst","Projects"=>"project_mst","Task"=>"task_mst",
								"Sub Task"=>"sub_mst","Job"=>"job_mst","Work Units"=>"work_unit_mst","Activity"=>"activity_mst","Std Target"=>"std_tgt_mst","MIDs"=>"create_mid");
		}
		if ($file_nm== 'Work_Request'){
			$data['check']=array("Raise Work Request"=>"apply_work_request","Confirm Work Request"=>"confirm_work_request","Approve Work Request"=>"final_work_request","View Work Request"=>"view_work_request"); 
		}
		if ($file_nm== 'Utility') {
			$data['check']=array("User"=>"utility_user","Activity"=>"utility_activity", "In/Out Time"=>"Emp_io_upload_utility");
		}		
		if($loc_val=='crud')
		{
			$data['check']=array("Department"=>"dept_mst","User"=>"user_mst","Roles"=>"role_mst","Access"=>"access_mst","Projects"=>"project_mst","Task"=>"task_mst",
								"Sub Task"=>"sub_mst","Job"=>"job_mst","Work Units"=>"work_unit_mst","Activity"=>"activity_mst","Std Target"=>"std_tgt_mst","MIDs"=>"create_mid",'Logistics'=>'logistics');
			$data['check_tab']=array("User"=>"utility_user","Activity"=>"utility_activity", "In/Out Time"=>"Emp_io_upload_utility","Logistics"=>'utility_logistics');
			$data['check_work_tab']=array("Raise Work Request"=>"apply_work_request","Confirm Work Request"=>"confirm_work_request","Approve Work Request"=>"final_work_request","View Work Request"=>"view_work_request");
			$data['check_od_tab']=array("Raise Request"=>"apply_od_request","Confirm  Request"=>"confirm_od_request","Approve Request"=>"final_od_request","View Request"=>"view_od_request");
		$this->load->view('crud/'.$file_nm . ".php", $data);	
		}else{
        $this->load->view($file_nm . ".php", $data);
		}
    }
	
	public function getUsersWeeklyProjectFeedback() {
		$input = $this->input->get();
		try {
			if (empty( $input['projectId'])) throw new Exception('ProjetctId is missing');
			if (empty( $input['users'])) throw new Exception('User(s) are missing');
			if (empty( $input['dateView'])) throw new Exception('Date range in address bar is missing');

			$weeklyProjectFeedback = $this->feedback_model->getUsersWeeklyProjectFeedback($input);
			
			header('Content-Type: application/json');
			echo json_encode( array('weeklyProjectFeedback' => $weeklyProjectFeedback->result()));
		} catch (Exception $e) {
			header('Content-Type: application/json');
			echo json_encode( array('success' => false, 'error' => $e->getMessage()) );
		}
	}

	public function upsertFeedback() 
	{
		$feedbackData = $this->input->post('formdata');

		foreach ($feedbackData as $index => $feed) {
			// here we have to check flag if feed have already inserted 
			$feedbackAlreadyGiven = $feed['feedback_already_given'];
			unset($feed['feedback_already_given']);
			if ($feedbackAlreadyGiven == 'true') {
				$this->feedback_model->updateWeeklyFeedback($feed);
			} 
			// else insert new row
			else {
				$this->feedback_model->insertWeeklyFeedback($feed);
			}
		}
		header('Content-Type: application/json');
    	echo json_encode( array('formData' => $feedbackData) );
	}

	public function getFeedbackGradesData() 
	{
		$gradesData = $this->feedback_model->getFeedbackGradesData();
		header('Content-Type: application/json');
    	echo json_encode( array('gradesData' => $gradesData->result()) );
	}
	
	public function getActiveFeedbackGradesData() 
	{	
		header('Content-Type: application/json');
    	echo json_encode( array('gradesData' => $this->feedback_model->getActiveFeedbackGradesData()->result()) );
	}

	public function upsertGrade() 
	{
		$gradeData = $this->input->post('formdata');
		try {
			if (!empty($gradeData['update_id'])) {
				$this->feedback_model->updateGrade($gradeData);
			} else {
				$this->feedback_model->storeGrade($gradeData);
			}
			header('Content-Type: application/json');
    		echo json_encode( array('success' => true));
		} catch (Exception $e) {
			header('Content-Type: application/json');
    		echo json_encode( array('success' => false) );
		}
		
	}

	public function deleteGrade()
	{
		$id = $this->input->post('id');
		$this->feedback_model->deleteGrade($id);
	}

	public function getGrade()
	{
		$id = $this->input->get('id');
		return $this->feedback_model->getGrade($id);
	}
	
	public function getDateFilters()
	{
		$date_view = $this->input->get('date_view');

		if (!empty($date_view)) {
			$day = date('Y-m-d H:i:s',strtotime($date_view));
		} else {
			$day = date('Y-m-d H:i:s');
		}
		$day_w = date('w',strtotime($day));
		$day_fm=date('d-M-Y',strtotime($day));
		if ($day_w==0) {
			$day_w=7;								
		}

		$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
		$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));
		
		$startDate = date('d-M', strtotime($day.' -'.($day_w-1).' days'));
		$endDate = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));

		if ($this->useDateFilters) {
			$startDate = date('d-M', strtotime($this->startDateFilter));
			$endDate = date('d-M', strtotime($this->endDateFilter));
			if ($this->timeSpanType == 'monthly') {
				$w_prev = date('Y-m-d', strtotime($date_view . ' -1 month'));
				$w_nxt = date('Y-m-d', strtotime($date_view . ' +1 month'));
			} else if ($this->timeSpanType == 'quarterly') {
				$w_prev = date('Y-m-d',strtotime($date_view . ' -3 month'));
				$w_nxt = date('Y-m-d', strtotime($date_view . ' +3 month'));
			} else if ($this->timeSpanType == 'half-yearly') {
				$w_prev = date('Y-m-d',strtotime($date_view . ' -6 month'));
				$w_nxt = date('Y-m-d',strtotime($date_view . ' +6 month'));
			} else if ($this->timeSpanType == 'yearly') {
				$w_prev = date('Y-m-d',strtotime($date_view . ' -12 month'));
				$w_nxt = date('Y-m-d',strtotime($date_view . ' +12 month'));
			}
		}
		return array('startDate' => $startDate, 'endDate' => $endDate, 'weekPrv' => $w_prev, 'weekNxt' => $w_nxt);
	}

	public function upsertPoLeaveStatus()
	{
		$listOfLeave = $this->input->post('listOfLeave');
		$status = $this->input->post('status');
		$isFinalChange = $this->input->post('isFinalChange');

		$approvedLeaveList =  array();
		$rejectedLeaveList =  array();
		
		// Now here we have to loop through the comming list of leaves to be approved
		foreach($listOfLeave as $leave) {
			// Here we are going to loop through the list project 
			if (!empty($leave['leavesProjectList'])) {
			//	foreach($leave['leavesProjectList'] as $project) {
				$project=$leave['leavesProjectList'];
					if ($isFinalChange == "true") {
						if ($this->user_model->updateLeaveInCompTrack($leave['compTrackId'], $status)) {
							// and add that comptrack id to Final Rejected List
							if ($status == "Approved") {
								$approvedLeaveList[] = $leave['compTrackId'];
							} else if ($status == "Rejected") {
								$rejectedLeaveList[] = $leave['compTrackId'];
							}
						}
					} else {
						// First upsert leave approval row
						$this->user_model->upsertPoLeaveStatus($leave['compTrackId'], $project, $status);

						$LeaveApprovedForProjects = $this->user_model->getAllPoApprovedLeaveProjectsList($leave['compTrackId']); 
						$LeaveRejectedForProjects = $this->user_model->getAllPoRejectedLeaveProjectsList($leave['compTrackId']); 
						
						$leaveForProjectList = $this->user_model->getCompLeaveProjects($leave['compTrackId']);

						// Check if all leave approved from all project owners id if yes then
						if ($LeaveApprovedForProjects == $leaveForProjectList) {
							// Update final approval in comp_track table
							if ($this->user_model->updateLeaveInCompTrack($leave['compTrackId'], 'Approved')) {
								// and add that comptrack id to Final Approved List
								$approvedLeaveList[] = $leave['compTrackId'];
							}
						}

						// Check if all leave rejected from all project owners id if yes then
						if ($LeaveRejectedForProjects == $leaveForProjectList) {
							// Update final rejection in comp_track table
							if ($this->user_model->updateLeaveInCompTrack($leave['compTrackId'], 'Rejected')) {
								// and add that comptrack id to Final Rejected List
								$rejectedLeaveList[] = $leave['compTrackId'];
							}
						}
					}
				//}
				$successStatus = true;
				$message = '';
			} else {
				// If user selects the record from table which owner is different then this message will show in page
				$successStatus = false;
				$message = 'Sorry, You are not the owner of the project';
			}
		}
		
		header('Content-Type: application/json');
    	echo json_encode(array(
			'success' => $successStatus, 
			'message' => $message,
			'approvedLeaveList' => array_unique($approvedLeaveList), 
			'rejectedLeaveList' => array_unique($rejectedLeaveList) 
		));
	}
	public function listenActivityLogUpsertEvents($activityLogData)
	{
		$userId = $this->session->userdata('user_id');

		$userActivityData = $this->user_model->getUserActivity(
			$activityLogData['activity_id'], //ACTIVITY ID
			$activityLogData['mid_code'], // MID CODE
			$userId
		);

	    if (!empty($userActivityData)) {
            $this->handleUserAllocDatesNProgress(
                $userActivityData,
                $activityLogData
            );

            $this->handleActivityAllocDatesNProgress(
                $userActivityData,
                $activityLogData
            );
        }


	}
	
	public function handleActivityAllocDatesNProgress($userActivityData, $activityLogData)
	{
		if (empty($userActivityData->actual_start_date)) {
			$this->allocation_model->updateActivityRow(
				$userActivityData->alloc_activity_id,
			['actual_start_date' => date('Y-m-d')]
			);
		}

		$allUserProgressOnActivity = $this->user_model->getAllUserProgressOnActivity(
			$userActivityData->alloc_activity_id
		);

		// CHECK HERE IF ALL USXRS COMPLETED THE ACTIVITY AND ACCORDINLY UPDATE END DATE
		if (count(array_unique($allUserProgressOnActivity)) === 1 &&
		 	end($allUserProgressOnActivity) === 'Completed') {
			$this->allocation_model->updateActivityRow(
				$userActivityData->alloc_activity_id,
				[
					'actual_end_date' => date('Y-m-d'),
					'progress' => 'Completed'
				]
			);
		} else {
			$this->allocation_model->updateActivityRow(
				$userActivityData->alloc_activity_id,
				[
					'actual_end_date' => NULL,
					'progress' => 'In Progress'
				]
			);
		}
	}
	
	public function handleUserAllocDatesNProgress($userActivityData, $activityLogData)
	{
		// Update activity progress for current user
		if (!empty($activityLogData['progress'])) {
			if (empty($userActivityData->user_start_date)) {
				$this->allocation_model->updateAllocUserRow(
					$userActivityData->alloc_user_id,
					['start_dt' => date('Y-m-d')]
				);
			}
			$this->allocation_model->updateAllocUserRow(
				$userActivityData->alloc_user_id,
				['progress' => $activityLogData['progress']]
			);
			if ($activityLogData['progress'] == 'Completed') {
				$this->allocation_model->updateAllocUserRow(
					$userActivityData->alloc_user_id,
					['end_dt' => date('Y-m-d')]
				);
			} else {
				$this->allocation_model->updateAllocUserRow(
					$userActivityData->alloc_user_id,
					['end_dt' => NULL]
				);
			}
		}
	}
}
?>