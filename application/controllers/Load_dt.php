<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Load_dt extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->helper('url');
        $this->load->model('user_model');
        $this->load->library('session');
    }

    public function index()
    {
        $this->load->view("login.php");
    }



	  public function getActivityFormData() {
        define('MEDIA_ID', '2');

        $currentDate = date("Y-m-d");
        $data = $this->input->get('activityData');
        $deptId = $this->input->get('deptId');
        $formData = array();

        $projectId = $data['project_id_fk'];
        if ($deptId == MEDIA_ID && ($projectId == '' || $projectId == '0')) {
            $projectId = 'no-project-id';
        }
        // load Categories
        $categories = $this->user_model->load_select(
            "sel_2",
            $data['dept_id_fk'],
            $projectId,
            '',
            '',
            '',
            $currentDate
        );
        
        $output = '';
        foreach ($categories as $roww) {
            $output .= "<option value='" . $roww['p_id'] . "'>" . $roww['p_nm'] . "</option>";
        }
        $formData['categories'] = $output;

        // Load elements
        $elements = $this->user_model->load_select(
            "sel_3",
            $data['dept_id_fk'],
            $projectId,
            $data['task_id_fk'],
            '',
            '',
            $currentDate
        );

        $output = '';
        foreach ($elements as $roww) {
            $output .= "<option value='" . $roww['p_id'] . "'>" . $roww['p_nm'] . "</option>";
        }
        $formData['elements'] = $output;

        // Load Actions
        $actions = $this->user_model->load_select(
            "sel_4",
            $data['dept_id_fk'],
            $projectId,
            $data['task_id_fk'],
            $data['sub_task_id_fk'],
            '',
            $currentDate
        );

        $output = '';
        foreach ($actions as $roww) {
            $output .= "<option value='" . $roww['p_id'] . "'>" . $roww['p_nm'] . "</option>";
        }
        $formData['actions'] = $output;

        header('Content-Type: application/json');
    	echo json_encode(array(
			'success' => true,
            'data' => $formData
		));
    }

    public function load_data()
    {
        $param     = $this->input->post('param');
		$sel_0_ID    = $this->input->post('sel_0_ID');
        $sel_1_ID    = $this->input->post('sel_1_ID');
        $sel_2_ID     = $this->input->post('sel_2_ID');
        $sel_3_ID    = $this->input->post('sel_3_ID');
        $sel_4_ID = $this->input->post('sel_4_ID');
        $dateID    = $this->input->post('dateID');
        $load_du   = $this->user_model->load_select($param,$sel_0_ID, $sel_1_ID, $sel_2_ID, $sel_3_ID, $sel_4_ID, $dateID);
        $output    = '';
        if ($param != 'load_info') {
            if ($load_du) {
                foreach ($load_du as $roww) {
                    $output .= "<option value='" . $roww['p_id'] . "'>" . $roww['p_nm'] . "</option>";

                }
            }
            echo $output;
        } else {
            //print_r($load_du);
            if (sizeof($load_du) == 1 && ($load_du)) {
                foreach ($load_du as $rowq) {
					$act_id=$rowq['p_id'];
					$issues_cat_mst = $this->user_model->load_cate_issues();
				$milestone_id = $this->user_model->load_milestone($sel_0_ID,$act_id,$sel_1_ID);

                echo "<hr class='st_hr2'>
						<div class='row row_style_1'>
							<div class='col-md-4'>
								<label class='l_font_fix_3'>Work Unit</label>
								<h6 class='wu' act_id='" . $rowq['p_id'] . "' id_v='" . $rowq['wu_id'] . "'>" . $rowq['wu_name'] . "</h6>
							</div>
							<div class='col-md-2'>
								<label class='l_font_fix_3'>Std. Target</label>
								<h6 class='std_tgt'>" . $rowq['tgt_val'] . "</h6>
							</div>
							<div class='col-md-2'>
								<label class='l_font_fix_3'>Quantity Done</label>
								<input type='number' class='word_comp form-control'/>
							</div>
							<div class='col-md-2'>
								<label class='l_font_fix_3'>Issues (Y/N)</label>
								<div>
								<input type='radio' class='stage_ch stage_opt_sel' name='ch_optionval0' value='N' checked='checked' id='cb_1'>
								<span class='f_fnt'>No</span>
								<input type='radio' class='stage_ch ' name='ch_optionval0' value='Y' id='cb_2'>
								<span class='f_fnt'>Yes</span>
								</div>
							</div>
							<div class='col-md-2'>
							<label class='l_font_fix_3'>Status</label>
								<h6 class='task_stat'>
								<i class='glyphicon glyphicon-stop t_stat_1'></i>
								<span class='fil_val t_status' style='font-size: 12px;'>Audit Pending</span>
								</h6>
							</div>
						</div>
						<div class='row row_style_1 issue-fields' style='display:none;'>
							<div class='col-md-3'>
								<label class='l_font_fix_3'>Issue Category</label>								
							<select class='selectpicker form-control iss_cat' id='iss_cat' title='Nothing Selected' data-live-search='true'>";							
							if($issues_cat_mst)
							{
								
								foreach($issues_cat_mst AS $value)
								{
									echo "<option value='" .$value['iss_cat_id']. "'>" . $value['category_name'] . "</option>";
								}
							}
							echo "</select>
							</div>";
							echo "<div class='col-md-9'>
								<label class='l_font_fix_3'>Issue Desc</label>
								<input type='text' class='form-control iss_desc'/>
							</div>
						</div>
						<div class='row row_style_1'>
						<div class='col-md-4'>
							<label class='l_font_fix_3'>Milestone ID</label>
							<select class='selectpicker form-control mile_st' id='mile_id' title='Nothing Selected' data-live-search='true'>";
							//echo "<option value='None' selected>None</option>";
							if($milestone_id)
							{
								foreach($milestone_id AS $value)
								{
									echo "<option value='" .$value['milestone_id']. "'>" . $value['milestone_id'] . "</option>";
								}
							}

							echo "</select>";
						echo "</div>
							<div class='col-md-8'>
								<label class='l_font_fix_3'>Task Desc</label>
								<input type='text' class='tas_desc form-control'/>
							</div>
						</div>

						<div class='row row_style'>
							<div class='col-md-2 col-md-offset-4'>
								<button class='btn add_but task_ref' type='button'>Refresh</button>
							</div>
							<div class='col-md-2'>
								<button class='btn add_but task_sub' type='button'>Submit</button>
							</div>
						</div>";
                }
            } else {
                echo "Error";
            }
        }

    }
	
	public function load_data_con_media()
    {
        $mid_code     = $this->input->post('mid_code');
		$param     = $this->input->post('param');
        $sel_0_ID    = $this->input->post('sel_0_ID');
        $sel_1_ID    = $this->input->post('sel_1_ID');
        $sel_2_ID     = $this->input->post('sel_2_ID');
        $sel_3_ID    = $this->input->post('sel_3_ID');
        $sel_4_ID = $this->input->post('sel_4_ID');
		$sel_4_ID = $this->input->post('sel_4_ID');
        $dateID    = $this->input->post('dateID');
        $load_du   = $this->user_model->load_select_media($param,$sel_0_ID, $sel_1_ID, $sel_2_ID, $sel_3_ID, $sel_4_ID, $dateID);
        $output    = '';
           
            //print_r($load_du);
            if (sizeof($load_du) == 1 && ($load_du)) {
                foreach ($load_du as $rowq) {
					$act_id=$rowq['p_id'];
					$issues_cat_mst = $this->user_model->load_cate_issues();
				$milestone_id = explode(",",$mid_code);
                echo "<hr class='st_hr2'>
						<div class='row row_style_1'>
							<div class='col-md-4'>
								<label class='l_font_fix_3'>Work Unit</label>
								<h6 class='a_wu' act_id='" . $rowq['p_id'] . "' id_v='" . $rowq['wu_id'] . "'>" . $rowq['wu_name'] . "</h6>
							</div>
							<div class='col-md-2'>
								<label class='l_font_fix_3'>Std. Target</label>
								<h6 class='a_std_tgt'>" . $rowq['tgt_val'] . "</h6>
							</div>
							<div class='col-md-2'>
								<label class='l_font_fix_3'>Quantity Done</label>
								<input type='number' class='a_word_comp form-control'/>
							</div>
							<div class='col-md-2'>
								<label class='l_font_fix_3'>Issues (Y/N)</label>
								<div>
								<input type='radio' class='stage_ch stage_opt_sel' name='a_ch_optionval0' value='N' checked='checked' id='cb_1'>
								<span class='f_fnt'>No</span>
								<input type='radio' class='stage_ch ' name='a_ch_optionval0' value='Y' id='cb_2'>
								<span class='f_fnt'>Yes</span>
								</div>
							</div>
							<div class='col-md-2'>
							<label class='l_font_fix_3'>Status</label>
								<h6 class='task_stat'>
								<i class='glyphicon glyphicon-stop t_stat_1'></i>
								<span class='fil_val a_t_status' style='font-size: 12px;'>Audit Pending</span>
								</h6>
							</div>
						</div>
						<div class='row row_style_1 a_issue-fields' style='display:none;'>
							<div class='col-md-3'>
								<label class='l_font_fix_3'>Issue Category</label>								
							<select class='selectpicker form-control a_iss_cat' id='a_iss_cat' title='Nothing Selected' data-live-search='true'>";							
							if($issues_cat_mst)
							{
								
								foreach($issues_cat_mst AS $value)
								{
									echo "<option value='" .$value['iss_cat_id']. "'>" . $value['category_name'] . "</option>";
								}
							}
							echo "</select>
							</div>";
							echo "<div class='col-md-9'>
								<label class='l_font_fix_3'>Issue Desc</label>
								<input type='text' class='form-control a_iss_desc'/>
							</div>
						</div>
						<div class='row row_style_1'>
						<div class='col-md-4'>
							<label class='l_font_fix_3'>Milestone ID</label>
							<select class='selectpicker form-control mile_st' id='a_mile_id' title='Nothing Selected' data-live-search='true'>";
							//echo "<option value='None' selected>None</option>";
							if($milestone_id)
							{
								if(!is_array($milestone_id))
								{
									echo "<option value='" .$milestone_id. "'>" . $milestone_id . "</option>";
								}else{
								foreach($milestone_id AS $value)
								{
									echo "<option value='" .$value. "'>" . $value . "</option>";
								}
								}
							}

							echo "</select>";
						echo "</div>
							<div class='col-md-5'>
								<label class='l_font_fix_3'>Task Desc</label>
								<input type='text' class='a_tas_desc form-control'/>
							</div>
							<div class='col-md-3'>
                       				<label class='l_font_fix_3 form-check-label' >Task Completed: </label>
                       				<select id='team_name' class='selectpicker form-control' name='user-progress-on-activity' title='Nothing Selected'>
                                 <option value='N'>No</option>                                
                                 <option value='Y'>Yes</option>
                               </select>
                       		</div>
						</div>

						<div class='row row_style'>
							<div class='col-md-2 col-md-offset-4'>
								<button class='btn add_but a_task_ref' type='button'>Refresh</button>
							</div>
							<div class='col-md-2'>
								<button class='btn add_but a_task_sub' type='button'>Submit</button>
							</div>
						</div>";
                }
            } else {
                echo "Error";
            }      

    }

}
?>