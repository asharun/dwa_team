<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller
{
    public function __construct()
    {       
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('user_model');
        $this->load->library('session');
		//$this->session->set_flashdata('error_msg', '');
    }
	
    public function emp_leave()
    {	
    	$date_v=$this->input->post('date_v');
		$pro_v=$this->input->post('pro_v');	
    	$dept=$this->input->post('dept'); 
		   
		   $day_chk1 = date('Y-m-d');
		   $day_w23 = date('w',strtotime($day_chk1));
			$current_week_st = date('Y-m-d', strtotime($day_chk1.' -'.($day_w23-1).' days'));
			$current_week_en = date('Y-m-d', strtotime($day_chk1.' +'.(8-$day_w23).' days'));
					
					 $day_w  = date('w', strtotime($date_v));
				if ($day_w == 0) {
					$day_w = 7;
				}
				
			if($date_v >= $current_week_st && $date_v <= $current_week_en){
				$start_dt=date('Y-m-d', strtotime($date_v . ' -' . ($day_w + 6) . ' days'));
			}else{
				$start_dt=date('Y-m-d', strtotime($date_v . ' -' . ($day_w - 1) . ' days'));
			}
						
		  
				$week_start = date('Y-m-d', strtotime($date_v . ' -' . ($day_w - 1) . ' days'));
				$end_dt   = date('Y-m-d', strtotime($date_v . ' +' . (7 - $day_w) . ' days'));
				
				
	$emp_data=$this->user_model->get_emp_leave($week_start,$pro_v,$dept,$start_dt,$end_dt);

echo "<div class='row'>				
		<div class='col-md-12 week_log seven-cols hidden-xs hidden-sm'>			
			<div class='col-md-1 week_cell'>MONDAY</div>
			<div class='col-md-1 week_cell'>TUESDAY</div>
			<div class='col-md-1 week_cell'>WEDNESDAY</div>
			<div class='col-md-1 week_cell'>THURSDAY</div>
			<div class='col-md-1 week_cell'>FRIDAY</div>
			<div class='col-md-1 week_cell'>SATURDAY</div>	
			<div class='col-md-1 week_cell'>SUNDAY</div>
		</div>
	</div>";
	echo "<div class='row'><div class='col-md-12 week_log seven-cols'>";
							
		$curr_day=date('Y-m-d');

	for($i=0;$i<=6;$i++)
	{											
		$date_wek = date('Y-m-d H:i:s', strtotime($week_start . ' +'.$i.' day'));	
			
		$date_chk = date('Y-m-d', strtotime($date_wek));
		
		$day1=date("d",strtotime($date_wek));											
		$week_name=date("D", strtotime($date_wek));
		$highl='';
		$high2='';
			//echo $date_chk." ".$curr_day;
		if($date_chk==$curr_day)
		{
			$highl='highl';
			$high2='highl_mob';
		}
		
		//echo $date_chk;
		echo "<div class='col-md-1 emp-cell'>";
		echo "<div class='day-num-sm visible-xs-inline-block visible-sm-inline-block'>
		 <span class='pull-left ".$high2."'>".$day1." | ".$week_name."</span>
		 <span class='count_list' id='cnt_r'></span></div>";											
			
		echo "<div class='day-num hidden-xs hidden-sm'><span class='".$highl."'>".$day1 . "</span>
		</div>";
											
				echo "<div class='month_tab'>";
			echo "<div class='count_list align-text' id='cnt_r'></div>";

			if($emp_data)
			{
				foreach($emp_data AS $rewo)
				{
					if($rewo['day_val']==$date_chk)
					{
				echo "<table class='tbldata' id='tblData'>";
					echo "<tbody>";
					echo "<tr><td class='td_al'>";
					$acc='#F44336';
					if($rewo['l_stat']=="Present|")
					{
						$acc='#4caf50';
					}
					if($rewo['l_stat']=="Present|Half Day")
					{
						$acc='#FF9800';
					}
					if($rewo['l_stat']=="Absent|Week Off")
					{
						$acc=' #03A9F4';
					}
					$ary=explode('|',$rewo['l_stat']);

					 $name=$ary[0];
					 $type=$ary[1];
					 if($type==null){
					 	$ty='';
					 }else{
					 	$ty='('.$type.').';
					 }
					echo "<button type='button' class='btn event-lt' b_c='' data-container = '.ic_cont' style='border-left-color:".$acc."' data-placement='auto bottom' loc='".$rewo['l_stat']."' col='".$rewo['full_name']."' data-toggle='popover' data-html='true'  date_val='".date("M d,Y", strtotime($date_wek))."'data-content=\"";
							echo "<b><center class='act_nm_break'>"  . str_replace("\xA0", " ", $rewo['full_name']) . "</center></b><br>";
							echo "<b>Status: </b>" .$name.$ty. "<br>";
							echo "\">";	
																										
					echo $rewo['full_name']."</button>";
					echo "</td></tr>";
				echo "</tbody>";
				echo "</table>";
					}
				}
			}
				echo"</div>";			
			echo "</div>";
			}
		echo "</div>
		</div>";	
	}

}