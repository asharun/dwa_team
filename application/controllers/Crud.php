<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud extends CI_Controller
{
	private $status;
    public function __construct()
    {
        parent::__construct();
		$this->status=['Active'=>'Active','InActive'=>'InActive'];
        $this->load->helper(array('url','assestsurl_helper'));
        $this->load->model('crud_model');
        $this->load->library(['session',"mailsend"]);
		$this->load->model('user_model');	
		require_once(APPPATH."controllers/Notify.php");
	}	

	 public function contentmidupdate() {
    	$ans = $this->input->post("C");
    	//print_r($ans);die;
		if($ans[1]==1){
		$grade=appendzero($ans[10],2);
		
    	$ans[10] =$grade;    	
	        if( chechcntsyll($ans[8])==true && commonchkcontent($grade)==true && editchechcntsubject($ans[15])==true &&
	        	chkforchapter($ans[9])==true && commonchkcontent($ans[6])==true && chkfordivnum($ans[7])==true
	        	&& commonchkcontent($ans[17])==true && chkfordivnum($ans[18])==true&& commonchkcontent($ans[19])==true && chkfordivnum($ans[20])==true
	        ){
	        	$resp = $this->crud_model->content_mid_update($ans);
	       		 echo "Successfully updated";
	        }else{
	        	 echo "Not valid pattern";
	        }
	    }else{
	    	$resp = $this->crud_model->mediamidupdate($ans);
	       		 echo "Successfully updated";
	    }
    }

	public function contentmidinsert() {
    $ans = $this->input->post("C");	
   	 if($ans[2]==1){
		 $grade=appendzero($ans[9],2);
    $ans[9] =$grade;
   	 			 if(chechcntsyll($ans[4])==true && commonchkcontent($grade)==true && editchechcntsubject($ans[5])==true &&
        	chkforchapter($ans[6])==true && commonchkcontent($ans[7])==true && chkfordivnum($ans[8])==true &&commonchkcontent($ans[18])==true && chkfordivnum($ans[19])==true && commonchkcontent($ans[20])==true && chkfordivnum($ans[21])==true){
        	$resp = $this->crud_model->ins_tbl($ans);
       		if($resp){echo  "done";}
       		}else{
       			echo"Pattern not matching in fields";
       		}
   	 }else{
   	 		$resp = $this->crud_model->insert_mid_midia($ans);
       		if($resp){
				echo "done";
				}
   	 }
    }

	public function upload_form()
	{
		$u_id = $this->session->userdata('user_id');
		$dept_val = $this->crud_model->fetchdept_data($u_id);
		echo "<form class='form-horizontal'  id='upload_file'>
						<div class='row row_style_1'>";
				echo "<div class='col-md-4'>";
					echo "<label class='l_font_fix_3'>Choose Dept</label>";
					echo "<select id='sel_dept_imp' class='selectpicker form-control' title='Nothing Selected' data-live-search='true'>";
						$k=1;
						foreach ($dept_val as $row)
						{
							$sel='';
								if($k==1)
								{
									$sel='selected';
								}
							echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
							$k++;
						}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-8'>";
					echo "<label class='l_font_fix_3'>Select a file to upload</label>";
					echo '<input type = "file" id="file" name = "file" />';
				echo "</div>";
			echo "</div>";
			echo "<div class='row row_style_1'>";
				echo "<div class='col-md-12'>";
					//echo "<button class='btn add_but up_csv_sub' id='submit' type='submit'>Upload</button>";
					echo    "<div class='col-md-2'>
						 	<button class='btn add_but up_csv_sub' id='submit' type='submit'>Upload</button>
						</div>";
				echo    "<div class='col-md-2'>
						 	<input type ='button' id='downloadactTem' class='btn add_but' name ='downloadactTem' value='Download csv template'/>
						</div>";
				echo "</div>";
			echo "</div>";
			echo "</form>";
			echo "<div class='row row_style_1'>";
			echo "<div class='col-md-12'>";
				echo "</div>";
				echo "<div class='col-md-12'>";
					echo "<div id='files'></div>";
				echo "</div>";
			echo "</div>";
	}

	public function upload_form_io()
	{
		$u_id = $this->session->userdata('user_id');
		$dept_val = $this->crud_model->fetchdept_data($u_id);
		echo "<form class='form-horizontal'  id='upload_file'>
						<div class='row row_style_1'>";
				echo "<div class='col-md-8'>";
					echo "<label class='l_font_fix_3'>Select a file to upload</label>";
					echo '<input type = "file" id="file" name = "file" />';
				echo "</div>";
			echo "</div>";
			echo "<div class='row row_style_1'>";
				echo "<div class='col-md-12'>";
					echo "<button class='btn add_but up_csv_sub' id='submit' type='submit'>Upload</button>";
				echo "</div>";
			echo "</div>";
			echo "</form>";
			echo "<div class='row row_style_1'>";
			echo "<div class='col-md-12'>";
				echo "</div>";
				echo "<div class='col-md-12'>";
					echo "<div id='files'></div>";
				echo "</div>";
			echo "</div>";
	}

    public function load_tab_data()
	{
		$file_nm            = $this->input->get('a');
		$dept            = $this->input->get('dept');
		$pids= $this->session->userdata('proj_fk');
        $tab_dta = $this->crud_model->select_tbl($file_nm,$dept);
		echo  $tab_dta ;
	}
	public function load_utility_data()
	{
		$file_nm            = $this->input->get('a');
		$dept            = $this->input->get('dept');
		$pids= $this->session->userdata('proj_fk');
        $tab_dta = $this->crud_model->select_tbl('user_mst',$dept);
		echo  $tab_dta ;
	}
	public function update()
    {
        $ans = $this->input->post("C");
        $resp = $this->crud_model->update_tbl($ans);
        echo $resp;
    }
	public function delete_crud()
    {
        $ans = $this->input->post("C");
        $resp = $this->crud_model->delete_tbl($ans);
        echo $resp;
    }

	public function insert()
    {
        $ans = $this->input->post("C");
        $resp = $this->crud_model->ins_tbl($ans);
        echo $resp;
    }

	 public function load_data_c()
    {
		$param    = $this->input->post('param');
		$sel_0_ID    = $this->input->post('sel_0_ID');
        $load_du   = $this->crud_model->load_select_c($param,$sel_0_ID);
        $output    = '';
            if ($load_du) {
                foreach ($load_du as $roww) {
                    $output .= "<option value='" . $roww['p_id'] . "'>" . $roww['p_nm'] . "</option>";

                }
            }
            echo $output;
    }

	public function loadLogisticData(){
    	$type=$this->input->get('type');
    	//Set hidden field
    	$hidden['type']=$type;
    	$logistic_data=NULL;
    	if($type === "edit"){
    		$l_id=$type=$this->input->get('l_id');
			$logistic_data=$this->crud_model->fetchSingleLogisticData($l_id);
			$hidden['l_id']=$logistic_data->logistics_id;
			$hidden['old_file']=$logistic_data->image;
    	}

    	$user=$this->user_model->getAllAciveUsersEmail();
		$user_list=array_combine(array_column($user, 'user_id'), array_column($user, 'email_id'));
    	
    	$response="";
    	$response.= form_open('frm_logistics','class="frm_logistics" id="frm_logistics"',$hidden);
    		$response.= "<div class='row row_style_1'>";
    			$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='email'>Email : </label>";
					if(!$logistic_data){
						$response.=  form_dropdown('user_id_fk',$user_list,'',['class'=>'selectpicker form-control','data-live-search'=>'true']);
					}else{
						$response .= "<h6 class>".  $logistic_data->user_login_name."</h6>";
					}
				$response.= "</div>";
				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='deputed_location'>Deputed Location: </label>";
					$response.=  form_input(['type'=>'text','name'=>'deputed_location','id'=>'deputed_location','class'=>'form-control','value'=>$logistic_data ? $logistic_data->deputed_location:NULL]);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='office'>Office: </label>";
					$response.=  form_dropdown('office',['banglore'=>'Banglore','mumbai'=>'Mumbai','kolkata'=>'Kolkata','delhi'=>'Delhi'],$logistic_data ? $logistic_data->office:NULL,['class'=>'selectpicker form-control','data-live-search'=>'true']);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='role_center'>Role Center: </label>";
					$response.=  form_input(['type'=>'text','name'=>'role_center','id'=>'role_center','class'=>'form-control','value'=>$logistic_data ? $logistic_data->role_center:NULL]);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='sapience_status'>Sapience Status: </label>";
					$response.=  form_input(['type'=>'text','name'=>'sapience_status','id'=>'sapience_status','class'=>'form-control','value'=>$logistic_data ? $logistic_data->sapience_status:NULL]);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='f_track'>FTrack: </label>";
					$response.=  form_input(['type'=>'text','name'=>'f_track','id'=>'f_track','class'=>'form-control','value'=>$logistic_data ? $logistic_data->f_track:NULL]);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='adobe_id'>Adobe ID: </label>";
					$response.=  form_input(['type'=>'text','name'=>'adobe_id','id'=>'adobe_id','class'=>'form-control','value'=>$logistic_data ? $logistic_data->adobe_id:NULL]);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='domain_login'>Domain Login: </label>";
					$response.=  form_input(['type'=>'text','name'=>'domain_login','id'=>'domain_login','class'=>'form-control','value'=>$logistic_data ? $logistic_data->domain_login:NULL]);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='system_no'>System No: </label>";
					$response.=  form_input(['type'=>'text','name'=>'system_no','id'=>'system_no','class'=>'form-control','value'=>$logistic_data ? $logistic_data->system_no:NULL]);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='workstation'>Work Station: </label>";
					$response.=  form_input(['type'=>'text','name'=>'workstation','id'=>'workstation','class'=>'form-control','value'=>$logistic_data ? $logistic_data->workstation:NULL]);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='employment_type'>Employment Type: </label>";
					$response.=  form_dropdown('employment_type',['trainee'=>'Trainee','permanent'=>'Permanent'],$logistic_data ? $logistic_data->employment_type:NULL,['class'=>'selectpicker form-control','id'=>'employment_type']);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='employment_status'>Employment Status: </label>";
					$response.=  form_dropdown('employment_status',['employee'=>'Employee','left'=>'Left'],$logistic_data ? $logistic_data->employment_status:NULL,['class'=>'selectpicker form-control','id'=>'employment_status']);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='start_date'>Start Date: </label>";
					$response.=  form_input(['type'=>'text','name'=>'start_date','id'=>'start_date','class'=>'form-control','value'=>$logistic_data ? $logistic_data->start_date:NULL]);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='end_date'>End Date: </label>";
					$response.=  form_input(['type'=>'text','name'=>'end_date','id'=>'end_date','class'=>'form-control','value'=>$logistic_data ? $logistic_data->end_date:NULL]);
				$response.= "</div>";

				/*$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='deputed_location'>Status: </label>";
					$response.=  form_dropdown('status_nm', $this->status, $logistic_data ? $logistic_data->status_nm:NULL, ['class'=>'selectpicker form-control','id'=>'deputed_location']);
				$response.= "</div>";*/

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='is_current'>Currently Active: </label>";
					$response.=  form_dropdown('is_current', ['Y'=>'Yes','N'=>'No'], $logistic_data ? $logistic_data->is_current:NULL, ['class'=>'selectpicker form-control','id'=>'is_current']);
				$response.= "</div>";

				$response.= "<div class='col-md-3'>";
					$response.=  "<label class='l_font_fix_3 form-check-label' for='image'>Image: </label>";
					$response.=  form_upload('image',[],['class'=>'form-control']);
				$response.= "</div>";
			$response.= "</div>";

			$response.='<div class="row row_style_1">';
				$response.= "<div class='col-md-12 text-center'>";
					$btn_text = $type === "insert" ? "Insert" : "Update";  
					$response.= '<button class="btn add_but" id="btn_add_logistic" type="submit">'.$btn_text.'</button>';
				$response.= "</div></div>";
    		$response.= form_close();
    	echo $response;
    }

    //Add logistics data
    public function insLogistics(){
    	$response['status']=false;
		
		$lg_data=$this->input->post();
		$empStatus = $this->input->post("employment_status");
		$startDate = $this->input->post("start_date");
		$endDate = $this->input->post("end_date");
		
		$type=$lg_data['type'];

		$userId = $this->session->userdata('user_id');

    	if(!empty($_FILES['image']['name'])){
    		$config['upload_path'] = './uploads/logistics';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '2048000';
			
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('image')){
                $uploadData = $this->upload->data();
                $lg_data['image'] = $uploadData['file_name'];
                //Update time delete old file
                if($type === "edit" && !empty($lg_data['old_file'])){
	               	$old_file = $lg_data['old_file'];
	               	$file_a_path="./uploads/logistics/".$old_file;
	               	if(file_exists($file_a_path)){
						unlink($file_a_path);
	               	}
                }
            }else{
            	$response['message'] = strip_tags($this->upload->display_errors());
            	header('Content-Type: application/json');
				echo json_encode($response);
				exit;
            }
    	}

    	$lg_data['ins_user'] = $userId;
    	$lg_data['start_date'] = $startDate ? date("Y-m-d", strtotime($startDate)) : NULL;
		$lg_data['end_date'] = $endDate ? date("Y-m-d", strtotime($endDate)) : NULL;
		$lg_data['status_nm'] = $empStatus === "employee" ? "Active" : "InActive";
    	
    	unset($lg_data['type'],$lg_data['old_file']);

		if($type === "insert"){
			$this->db->insert('user_logistic',$lg_data);
    	}else{
    		$logistic_id=$lg_data['l_id'];
    		$lg_data['upd_user']=$userId;
    		unset($lg_data['l_id']);
    		$this->db->update('user_logistic',$lg_data,['logistics_id'=>$logistic_id]);
    	}
	 	
		$response['status']=true;
		$response['message']=$type==="insert"?"Logistics added successfully.":"Logistics updated successfully.";
		
		header('Content-Type: application/json');
		echo json_encode($response);
		exit;
	 }
	public function ins_tab_data()
	{
		$file_nm  = $this->input->get('a');
		$pids= $this->session->userdata('proj_fk');
		$u_id = $this->session->userdata('user_id');
		if($file_nm=='activity_mst')
		{
			$dept=$this->crud_model->fetchdept_data($u_id);
			$dept_opt=$dept[0]['dept_id'];
		$project=$this->crud_model->fetch_full_access('project_mst',$dept_opt);
		$task=$this->crud_model->fetch_full_access('task_mst',$dept_opt);
		$sub=$this->crud_model->fetch_full_access('sub_mst',$dept_opt);
		$job=$this->crud_model->fetch_full_access('job_mst',$dept_opt);
		$wu=$this->crud_model->fetch_full_access('work_unit_mst',$dept_opt);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'></h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Activity Name: </label> ";
					echo "<h6 class='h6_sty'></h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					if($dept_opt==$rw['dept_id'])
					{
					 echo "<option value='".$rw['dept_id']."' selected>".$rw['dept_name']."</option>";
					}else{
					echo "<option value='".$rw['dept_id']."'>".$rw['dept_name']."</option>";
					}
				}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, '', ['class'=>'selectpicker form-control','id'=>'us_8']);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_1'>
						<div class='col-md-3 col-md-offset-1''>";
					echo "<label class='l_font_fix_3 form-check-label' >Projects: </label> ";
					echo "<select class='selectpicker form-control sel_1' id='us_2' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($project AS $row)
					{
					echo "<option value='".$row['project_id']."'>".$row['project_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Tasks: </label> ";
					echo "<select class='selectpicker form-control sel_2' id='us_3' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($task AS $row)
					{
					echo "<option value='".$row['task_id']."'>".$row['task_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Sub Task: </label> ";
						echo "<select class='selectpicker form-control sel_3' id='us_4' title='Nothing Selected' data-live-search='true'>";
						echo '<option data-hidden="true"></option>';
					foreach($sub AS $row)
					{
					echo "<option value='".$row['sub_id']."'>".$row['sub_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_1'>";
				echo "<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Job: </label> ";
					echo "<select class='selectpicker form-control sel_4' id='us_5' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($job AS $row)
					{
					echo "<option value='".$row['job_id']."'>".$row['job_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Work Unit: </label> ";
					echo "<select class='selectpicker form-control sel_5' id='us_6' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($wu AS $row)
					{
					echo "<option value='".$row['wu_id']."'>".$row['wu_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Tgt: </label> ";
					echo "<input class='form-control us_7'/>";
				echo "</div>";

				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but in_crud_activity' type='button'>Insert</button>
					</div></div>";
			echo "</div>";
		}
		if($file_nm=='work_unit_mst')
		{
		$dept=$this->crud_model->fetchdept_data($u_id);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'></h6>";
				echo "</div>";
				echo "<div class='col-md-4'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Work Unit: </label> ";
					echo "<input class='form-control us_1'/>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Quantifiable(Y/N): </label> ";
					echo "<input class='form-control us_2'/>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, '', ['class'=>'selectpicker form-control','id'=>'us_8']);
				echo "</div>";
				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					echo "<option value='".$rw['dept_id']."'>".$rw['dept_name']."</option>";
				}
				echo "</select>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but in_crud_work_unit' type='button'>Insert</button>
					</div></div>";
			echo "</div>";
		}
		if($file_nm=='job_mst')
		{
			$dept=$this->crud_model->fetchdept_data($u_id);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'></h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Job Name : </label> ";
					echo "<input class='form-control us_1'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					echo "<option value='".$rw['dept_id']."'>".$rw['dept_name']."</option>";
				}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, '', ['class'=>'selectpicker form-control','id'=>'us_8']);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but in_crud_job' type='button'>Insert</button>
					</div></div>";
			echo "</div>";
		}
		if($file_nm=='sub_mst')
		{
			$dept=$this->crud_model->fetchdept_data($u_id);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'></h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >sub Name : </label> ";
					echo "<input class='form-control us_1'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					echo "<option value='".$rw['dept_id']."'>".$rw['dept_name']."</option>";
				}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, '', ['class'=>'selectpicker form-control','id'=>'us_8']);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but in_crud_sub' type='button'>Insert</button>
					</div></div>";
			echo "</div>";

		}
		if($file_nm=='task_mst')
		{
			$dept=$this->crud_model->fetchdept_data($u_id);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'></h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Task Name : </label> ";
					echo "<input class='form-control us_1'/>";
				echo "</div>";

				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					echo "<option value='".$rw['dept_id']."'>".$rw['dept_name']."</option>";
				}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, '', ['class'=>'selectpicker form-control','id'=>'us_8']);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but in_crud_task' type='button'>Insert</button>
					</div></div>";
			echo "</div>";

		}
		if($file_nm=='project_mst')
		{
			$dept=$this->crud_model->fetchdept_data($u_id);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'></h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Project Name : </label> ";
					echo "<input class='form-control us_1'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					echo "<option value='".$rw['dept_id']."'>".$rw['dept_name']."</option>";
				}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, '', ['class'=>'selectpicker form-control','id'=>'us_8']);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but in_crud_project' type='button'>Insert</button>
					</div></div>";
			echo "</div>";
		}

		if($file_nm=='dept_mst')
		{
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'></h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Dept Name : </label> ";
					echo "<input class='form-control us_1'/>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, '', ['class'=>'selectpicker form-control','id'=>'us_8']);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>dlllll</label>
					<button class='btn add_but in_crud_dept' type='button'>Insert</button>
					</div></div>";
			echo "</div>";
		}
		if($file_nm=='access_mst')
		{
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'></h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Access Name : </label> ";
					echo "<input class='form-control us_1'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Icon : </label> ";
					echo "<input class='form-control us_2'/>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_1'>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Display Name : </label> ";
					echo "<input class='form-control us_3'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Tab Color : </label> ";
					echo "<input class='form-control us_4'/>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, '', ['class'=>'selectpicker form-control','id'=>'us_8']);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but in_crud_access' type='button'>Insert</button>
					</div></div>";
			echo "</div>";

		}
		if($file_nm=='role_mst')
		{
			$acc=$this->crud_model->fetch_full_access('access_mst');
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'></h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Role Name : </label> ";
					echo "<input class='form-control us_1''/>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, '', ['class'=>'selectpicker form-control','id'=>'us_8']);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-5 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Access : </label> ";

				$j=1;
				foreach($acc AS $rw)
				{
					$selv_val=$rw['access_name'];
					$add_arr=array("Audit","Allocation","Review","Confirm");
					$n='';
					if($j%2==0)
					{
						$n="<br>";
					}
					echo "<input name='jjj' class='ch_tog' type='checkbox' at='".$selv_val."'> ".$selv_val." ".$n;
					$j++;
					if($rw['access_name']=='Confirmation Process')
					{
						foreach($add_arr AS $re)
						{
							$selv_val= $re;
								$n='';
								if($j%2==0)
								{
									$n="<br>";
								}
								echo "<input name='jjj' class='ch_tog' type='checkbox' at='".$selv_val."'> ".$selv_val." ".$n;
								$j++;
						}
					}
				}
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but in_crud_role' type='button'>Insert</button>
					</div></div>";
			echo "</div>";

		}
		if($file_nm=='user_mst')
		{
			$acc=$this->crud_model->fetch_full_access('access_mst');
			$role=$this->crud_model->fetch_full_access('role_mst');
			$user_manager=$this->crud_model->fetch_user();
			$proj=$this->crud_model->fetch_full_access('project_mst');
			$dept=$this->crud_model->fetch_full_access('dept_mst');
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'></h6>";
				echo "</div>";
				echo "<div class='col-md-5 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Email : </label> ";
					echo "<input class='form-control us_1' type='email'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Employee ID : </label> ";
					echo "<input class='form-control us_2'/>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				$j=2;
				echo "<input name='dept_dd' class='ch_tog dept_ch1' value='0' type='checkbox'><span> All Depts </span>";
				foreach($dept AS $rw)
				{
					$selv_val=$rw['dept_id'];
					$n='';
					if($j%2==0)
					{
						$n="<br>";
					}
					echo "<input name='dept_dd' class='ch_tog dept_ch'  value='".$selv_val."' type='checkbox'><span> ".$rw['dept_name']." </span>".$n;
					$j++;
				}
				echo "</div>";
					echo "<div class='col-md-4'>";
						echo "<label class='l_font_fix_3 form-check-label' >User Name : </label> ";
						echo "<input class='form-control us_4'/>";
					echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Role : </label> ";
					echo "<select class='selectpicker form-control' id='us_5' title='Nothing Selected' data-live-search='true'>";
					foreach($role AS $row)
					{
					echo "<option value='".$row['role_id']."'>".$row['role_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Streak : </label> ";
					echo "<input class='form-control us_6' value='0'/>";
				echo "</div>";
				echo "<div class='col-md-6'>";
					echo "<label class='l_font_fix_3 form-check-label' >Reporting Manager : </label> ";
					echo "<select class='selectpicker form-control' id='us_7' title='Nothing Selected' data-live-search='true'>";
					echo '<option '.$se.' value="">None</option>';
					foreach($user_manager AS $row1)
					{
					echo "<option value='".$row1['user_id']."'>".$row1['full_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label'>Status : </label> ";
					echo form_dropdown('status', $this->status, '', ['class'=>'selectpicker form-control','id'=>'us_8']);
				echo "</div>";
				echo "</div>";

				echo "<div class='row row_style_4'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
						echo "<label class='l_font_fix_3 form-check-label'>Joining Date (Intern / Trainee) : </label> ";
						echo form_input(['type'=>'text','name'=>'joining_date_intern','id'=>'joining_date_intern','class'=>'form-control join-date','value'=>'']);
					echo "</div>";

					echo "<div class='col-md-4'>";
						echo "<label class='l_font_fix_3 form-check-label'>Joining Date Permanent : </label> ";
						echo form_input(['type'=>'text','name'=>'joining_date_permanant','id'=>'joining_date_permanant','class'=>'form-control join-date','value'=>'']);
					echo "</div>";

					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label'>Slot : </label> ";
						echo form_input(['type'=>'text','name'=>'slot','id'=>'slot','class'=>'form-control','value'=>'']);
					echo "</div>";

				echo "</div>";

				echo "<div class='row row_style_4'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
						echo "<label class='l_font_fix_3 form-check-label'>Sub Department : </label> ";
						echo form_input(['type'=>'text','name'=>'sub_department','id'=>'sub_department','class'=>'form-control','value'=>'']);
					echo "</div>";

					echo "<div class='col-md-4'>";
						echo "<label class='l_font_fix_3 form-check-label'>Unit Name : </label> ";
						echo form_input(['type'=>'text','name'=>'unit_name','id'=>'unit_name','class'=>'form-control','value'=>'']);
					echo "</div>";

					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label'>Vertical : </label> ";
						echo form_input(['type'=>'text','name'=>'vertical','id'=>'vertical','class'=>'form-control','value'=>'']);
					echo "</div>";

					
				echo "</div>";

				echo "<div class='row row_style_4'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
						echo "<label class='l_font_fix_3 form-check-label'>Designation : </label> ";
						echo form_input(['type'=>'text','name'=>'designation','id'=>'designation','class'=>'form-control','value'=>'']);
					echo "</div>";
					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label'>Grading Access : </label> ";
						echo form_dropdown('is_greading',["N" => "No","Y" => "Yes"],'',['class'=>'selectpicker form-control','id'=>'is_greading']);
					echo "</div>";
				echo "</div>";

				echo "<div class='row row_style_4'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
						$user_manager_data=array_combine(array_column($user_manager, 'user_id'), array_column($user_manager, 'full_name'));
						echo "<label class='l_font_fix_3 form-check-label' >Manager : </label> ";
						echo form_dropdown('manager',[NULL => "Nothing Selected"]+$user_manager_data,'',['class'=>'selectpicker form-control','id'=>'manager','data-live-search'=>'true']);
					echo "</div>";

					echo "<div class='col-md-4'>";
						echo "<label class='l_font_fix_3 form-check-label' >Project Owner : </label> ";
						echo form_dropdown('project_owner',[NULL => "Nothing Selected"]+$user_manager_data,'',['class'=>'selectpicker form-control','id'=>'project_owner','data-live-search'=>'true']);
					echo "</div>";

					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label' >POC : </label> ";
						echo form_dropdown('poc',[NULL => "Nothing Selected"]+$user_manager_data,'',['class'=>'selectpicker form-control','id'=>'poc','data-live-search'=>'true']);
						
					echo "</div>";
				echo "</div>";

				echo "<div class='row row_style_4'>
						<div class='col-md-5 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Access : </label> ";
				$j=1;
				foreach($acc AS $rw)
				{
					$ch='';
					$selv_val=$rw['access_name'];
					$add_arr=array("Audit","Allocation","Review","Confirm");
					$n='';
					if($j%2==0)
					{
						$n="<br>";
					}
					echo "<input name='jjj' class='ch_tog' type='checkbox' at='".$selv_val."'> ".$selv_val." ".$n;
					$j++;
					if($rw['access_name']=='Confirmation Process')
					{
						foreach($add_arr AS $re)
						{
							$selv_val= $re;

								$n='';
								if($j%2==0)
								{
									$n="<br>";
								}
								echo "<input name='jjj' class='ch_tog' type='checkbox' at='".$selv_val."'> ".$selv_val." ".$n;
								$j++;
						}
					}
				}
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Projects : </label> ";
				$j=1;

				echo "<span class='pro_user'><input name='ppp' class='ch_tog' value='0' type='checkbox'><span> All Projects </span></span>";

				foreach($proj AS $rw)
				{
					$selv_val=$rw['project_id'];

					$n='';
					if($j%4==0)
					{
						$n="<br>";
					}
					echo "<span class='pro_user hide'>
					<input name='ppp' class='ch_tog' dep='".$rw['dept_id_fk']."' value='".$selv_val."' type='checkbox'>
					<span> ".$rw['project_name']." </span>".$n."</span>";
					$j++;
				}
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but in_crud_user' type='button'>Insert</button>
					</div></div>";
			echo "</div>";

		}

		if($file_nm=='create_mid')
		{	$ckdept= $this->input->get('dept');
			if($ckdept==1){
				$dept=$this->crud_model->fetchdept_data($u_id);
				$dept_opt=$dept[0]['dept_id'];
				$project=$this->crud_model->fetch_full_access('project_mst',$dept_opt);
				$task=$this->crud_model->fetch_full_access('task_mst',$dept_opt);
				$sub=$this->crud_model->fetch_full_access('sub_mst',$dept_opt);
				$job=$this->crud_model->fetch_full_access('job_mst',$dept_opt);
				$wu=$this->crud_model->fetch_full_access('work_unit_mst',$dept_opt);
				echo "<div class='row'>";
					echo "<div class='row row_style_1'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
						echo "<label class='l_font_fix_3 form-check-label'>Status :</label>";
						echo "<input class='form-control status'id='status' value='Active'/>";
					echo "</div>";
					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label' >Syllabus : </label> ";
						echo'<a class="cck"  title="Field Patter" onmouseover="" style="cursor: pointer;"data-content="Must have 4 character-SBKL"><i class="glyphicon glyphicon-info-sign"></i></a>';
						echo "<input class='form-control syllabus' id='syllabus' value=''/>";
					echo "</div>";
					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label' >Subject : </label> ";
						echo'<a class="cck"  title="Field Patter" onmouseover="" style="cursor: pointer;"data-content="Must have 3 character -MAT"><i class="glyphicon glyphicon-info-sign"></i></a>';
						echo "<input class='form-control subject' id='subject' value=''/>";
					echo "</div>";

					echo "</div>";
					echo "<div class='row row_style_1'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
						echo "<label class='l_font_fix_3 form-check-label' >Chapter : </label> ";
						echo'<a class="cck"  title="Field Patter" onmouseover="" style="cursor: pointer;"data-content="Must have 2 character -10"><i class="glyphicon glyphicon-info-sign"></i></a>';
						echo "<input class='form-control chapter'id='chapter' value=''/>";
					echo "</div>";
					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label' >Grade : </label> ";
						echo'<a class="cck"  title="Field Patter" onmouseover="" style="cursor: pointer;"data-content="Must have 2 character -06"><i class="glyphicon glyphicon-info-sign"></i></a>';
						echo "<input class='form-control grade'id='grade'  value=''/>";
					echo "</div>";
					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label' >SubDiv : </label> ";
						echo'<a class="cck"  title="Field Patter" onmouseover="" style="cursor: pointer;"data-content="Must have 2 character -XC"><i class="glyphicon glyphicon-info-sign"></i></a>';
						echo "<input class='form-control subdiv'id='subdiv' value=''/>";
					echo "</div>";

					echo "</div>";
					echo "<div class='row row_style_1'>";
						echo "<div class='col-md-3 col-md-offset-1'>";
							echo "<label class='l_font_fix_3 form-check-label' >DivNum : </label> ";
							echo'<a class="cck"  title="Field Patter" onmouseover="" style="cursor: pointer;"data-content="Must have 2 character -15"><i class="glyphicon glyphicon-info-sign"></i></a>';
							echo "<input class='form-control divnum'id='divnum' value=''/>";
						echo "</div>";
						echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label' >SecSubDiv : </label> ";
							echo'<a class="cck"  title="Field Patter" onmouseover="" style="cursor: pointer;"data-content="Must have 2 character -XC"><i class="glyphicon glyphicon-info-sign"></i></a>';
							echo "<input class='form-control secsubdiv'id='secsubdiv' value=''/>";
						echo "</div>";
						echo "<div class='col-md-3'>";
							echo "<label class='l_font_fix_3 form-check-label' >SecDivNum: </label> ";
							echo'<a class="cck"  title="Field Patter" onmouseover="" style="cursor: pointer;"data-content="Must have 2 character -17"><i class="glyphicon glyphicon-info-sign"></i></a>';
								echo "<input class='form-control secdivnum'id='secdivnum' value=''/>";
						echo "</div>";
					echo "</div>";
					echo "<div class='row row_style_1'>";


						echo "<div class='col-md-3 col-md-offset-1'>";
							echo "<label class='l_font_fix_3 form-check-label' >ThirdSubDiv: </label> ";
							echo'<a class="cck"  title="Field Patter" onmouseover="" style="cursor: pointer;"data-content="Must have 2 character -XC"><i class="glyphicon glyphicon-info-sign"></i></a>';
							echo "<input class='form-control thirdsubdiv'id='thirdsubdiv' value=''/>";
						echo "</div>";
						echo "<div class='col-md-3'>";
							echo "<label class='l_font_fix_3 form-check-label' >ThirdDivNum: </label> ";
							echo'<a class="cck"  title="Field Patter" onmouseover="" style="cursor: pointer;"data-content="Must have 2 character -15"><i class="glyphicon glyphicon-info-sign"></i></a>';
							echo "<input class='form-control thirddivnum'id='thirddivnum' value=''/>";
						echo "</div>";
						echo "<div class='col-md-3'>";
							echo "<label class='l_font_fix_3 form-check-label' >Projects: </label> ";
								echo "<select class='selectpicker form-control sel_1' id='project' title='Nothing Selected' data-live-search='true'>";
								echo '<option data-hidden="true"></option>';
								foreach($project AS $row)
								{
								echo "<option value='".$row['project_id']."'>".$row['project_name']."</option>";
								}
								echo "</select>";
						echo "</div>";
						echo "</div>";
					echo "<div class='row row_style_1'>";
					echo "<div class='col-md-3  col-md-offset-1'>";
							echo "<label class='l_font_fix_3 form-check-label' >Tasks: </label> ";
						echo "<select class='selectpicker form-control sel_2' id='task' title='Nothing Selected' data-live-search='true'>";
						echo '<option data-hidden="true"></option>';
						foreach($task AS $row)
						{
						echo "<option value='".$row['task_id']."'>".$row['task_name']."</option>";
						}
						echo "</select>";
						echo "</div>";
					echo "<div class='col-md-3 '>";
						echo "<label class='l_font_fix_3 form-check-label' >Sub Task: </label> ";
							echo "<select class='selectpicker form-control sel_3' id='subtask' title='Nothing Selected' data-live-search='true'>";
							echo '<option data-hidden="true"></option>';
						foreach($sub AS $row)
						{
						echo "<option value='".$row['sub_id']."'>".$row['sub_name']."</option>";
						}
						echo "</select>";
					echo "</div>";
					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label' >Job: </label> ";
						echo "<select class='selectpicker form-control sel_4' id='job' title='Nothing Selected' data-live-search='true'>";
						echo '<option data-hidden="true"></option>';
						foreach($job AS $row)
						{
						echo "<option value='".$row['job_id']."'>".$row['job_name']."</option>";
						}
						echo "</select>";
					echo "</div>";
					echo "</div>";
					echo "<div class='row row_style_1'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
						echo "<label class='l_font_fix_3 form-check-label' >Year: </label> ";
						echo "<input class='form-control year' id='year' value=''/>";
					echo "</div>";
					echo "</div>";
					echo "<div class='row row_style_2'><div class='col-md-12'>
						<label class='l_font_fix_3 invisible'>Update</label>
						<button class='btn add_but in_crud_content_mid' type='button'>Insert</button>
						</div></div>";
				echo "</div>";
		  	 }else{
					$project=$this->crud_model->fetch_full_access('project_mst',2);
					echo "<div class='row'>";
						echo "<div class='row row_style_1'>
							<div class='col-md-3 col-md-offset-1''>";
						echo "<label class='l_font_fix_3 form-check-label' >Projects: </label> ";
						echo "<select class='selectpicker form-control sel_1' id='midproject' title='Nothing Selected' data-live-search='true'>";
						echo '<option data-hidden="true"></option>';
						foreach($project AS $row)
						{
						echo "<option value='".$row['project_id']."'>".$row['project_name']."</option>";
						}
						echo "</select>";
					echo "</div>";
						echo "<div class='col-md-3'>";
							echo "<label class='l_font_fix_3 form-check-label' >MIDS : </label> ";
							echo "<input class='form-control status'id='media_mid' value=''/>";
						echo "</div>";
						echo "<div class='col-md-3'>";
							echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
							echo form_dropdown('status', $this->status, '', ['class'=>'selectpicker form-control status','id'=>'sta_tus']);
						echo "</div>";
					echo "</div>";
						echo "<div class='row row_style_2'><div class='col-md-12'>
						<label class='l_font_fix_3 invisible'>Update</label>
						<button class='btn add_but inst_media_mid_indus' type='button'>Insert</button>
						</div></div>";

					echo "</div>";
		  	 }
		}
	}
	public function edit_tab_data()
	{
		$file_nm  = $this->input->get('a');
		$pids= $this->session->userdata('proj_fk');
		$tab_id= $this->input->get('tab_id');
        $fields = $this->crud_model->view_tbl($file_nm,$tab_id);
		$u_id = $this->session->userdata('user_id');
		if($file_nm=='create_mid')
		{
			$dept=$this->crud_model->fetchdept_data($u_id);
			$dept_opt=$fields[0]['dept_id_fk'];
			$project=$this->crud_model->fetch_full_access('project_mst',$dept_opt);
			$task=$this->crud_model->fetch_full_access('task_mst',$dept_opt);
			$sub=$this->crud_model->fetch_full_access('sub_mst',$dept_opt);
			$job=$this->crud_model->fetch_full_access('job_mst',$dept_opt);
			$wu=$this->crud_model->fetch_full_access('work_unit_mst',$dept_opt);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['activity_id']."</h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Activity Name : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['activity_name']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					$slec="";
					if($fields[0]['dept_id_fk']==$rw['dept_id'])
					{
						$slec="selected";
					}
					echo "<option value='".$rw['dept_id']."' ".$slec.">".$rw['dept_name']."</option>";
				}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo "<input class='form-control status' value='".$fields[0]['status_nm']."'/>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_1'>
						<div class='col-md-3 col-md-offset-1''>";
					echo "<label class='l_font_fix_3 form-check-label' >Projects: </label> ";
					echo "<select class='selectpicker form-control' id='project' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($project AS $row)
					{
					$sel='';
						if($fields[0]['project_id_fk']==$row['project_id'])
						{
							$sel='selected';
						}
					echo "<option ".$sel." value='".$row['project_id']."'>".$row['project_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Tasks: </label> ";
					echo "<select class='selectpicker form-control' id='task' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($task AS $row)
					{
					$sel='';
						if($fields[0]['task_id_fk']==$row['task_id'])
						{
							$sel='selected';
						}
					echo "<option ".$sel." value='".$row['task_id']."'>".$row['task_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Sub Task: </label> ";
					echo "<select class='selectpicker form-control' id='subtask' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($sub AS $row)
					{
					$sel='';
						if($fields[0]['sub_id_fk']==$row['sub_id'])
						{
							$sel='selected';
						}
					echo "<option ".$sel." value='".$row['sub_id']."'>".$row['sub_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_1'>
						<div class='col-md-3 col-md-offset-1''>";
					echo "<label class='l_font_fix_3 form-check-label' >Job: </label> ";
					echo "<select class='selectpicker form-control' id='job' title='Nothing Selected' data-live-search='true' >";
					echo '<option data-hidden="true"></option>';
					foreach($job AS $row)
					{
					$sel='';
						if($fields[0]['job_id_fk']==$row['job_id'])
						{
							$sel='selected';
						}
					echo "<option ".$sel." value='".$row['job_id']."'>".$row['job_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo"<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Sub Div: </label> ";
					echo'<a class="cck" title="" onmouseover="" style="cursor: pointer;" data-content="Must have 2 character-XC" data-original-title="Field Patter"><i class="glyphicon glyphicon-info-sign"></i></a>';
					echo "<input class='form-control subdiv' value='".$fields[0]['sub_div']."'/>";
				echo "</div>";

				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Div Num  : </label> ";
					echo'<a class="cck" title="" onmouseover="" style="cursor: pointer;" data-content="Must have 2 character-15" data-original-title="Field Patter"><i class="glyphicon glyphicon-info-sign"></i></a>';
				echo "<input class='form-control divnum' value='".$fields[0]['div_num']."'/>";
				echo "</div>";
				echo"<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Year: </label> ";
					echo "<input class='form-control year' value='".$fields[0]['year']."'/>";
				echo "</div>";
				echo "</div>";
				// echo "<div class='row row_style_4'>
				// 		<div class='col-md-3 col-md-offset-1'>";
				// 	echo "<label class='l_font_fix_3 form-check-label' >Inserted By : </label> ";
				// 	echo "<h6 class='h6_sty' >".$fields[0]['ins_user']."</h6>";
				// echo "</div>";
				// echo "<div class='col-md-2'>";
				// 	echo "<label class='l_font_fix_3 form-check-label' >Inserted On : </label> ";
				// echo "<h6 class='h6_sty' >".$fields[0]['ins_dt']."</h6>";
				// echo "</div>";
				// echo "<div class='col-md-3'>";
				// 	echo "<label class='l_font_fix_3 form-check-label' >Updated By : </label> ";
				// 	echo "<h6 class='h6_sty' >".$fields[0]['upd_user']."</h6>";
				// echo "</div>";
				// echo "<div class='col-md-2'>";
				// 	echo "<label class='l_font_fix_3 form-check-label' >Updated On : </label> ";
				// 	echo "<h6 class='h6_sty' >".$fields[0]['upd_dt']."</h6>";
				// echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >SecSubDiv: </label> ";
					echo'<a class="cck" title="" onmouseover="" style="cursor: pointer;" data-content="Must have 2 character-CS" data-original-title="Field Patter"><i class="glyphicon glyphicon-info-sign"></i></a>';
					echo "<input class='form-control SecSubDiv' value='".$fields[0]['sec_sub_div']."'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >SecDivNum  : </label> ";
					echo'<a class="cck" title="" onmouseover="" style="cursor: pointer;" data-content="Must have 2 character-10" data-original-title="Field Patter"><i class="glyphicon glyphicon-info-sign"></i></a>';
				echo "<input class='form-control SecDivNum' value='".$fields[0]['sec_div_num']."'/>";
				echo "</div>";

				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >ThirdSubDiv : </label> ";
					echo'<a class="cck" title="" onmouseover="" style="cursor: pointer;" data-content="Must have 2 character-MA" data-original-title="Field Patter"><i class="glyphicon glyphicon-info-sign"></i></a>';
				echo "<input class='form-control ThirdSubDiv' value='".$fields[0]['third_sub_div']."'/>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >ThirdDivNum : </label> ";
					echo'<a class="cck" title="" onmouseover="" style="cursor: pointer;" data-content="Must have 2 character-06" data-original-title="Field Patter"><i class="glyphicon glyphicon-info-sign"></i></a>';
				echo "<input class='form-control ThirdDivNum' value='".$fields[0]['third_div_num']."'/>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Syllabus: </label> ";
					echo'<a class="cck" title="" onmouseover="" style="cursor: pointer;" data-content="Must have 4 character-SBKL" data-original-title="Field Patter"><i class="glyphicon glyphicon-info-sign"></i></a>';
					echo "<input class='form-control syllabus' value='".$fields[0]['syllabus']."'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Chapter  : </label> ";
					echo'<a class="cck" title="" onmouseover="" style="cursor: pointer;" data-content="Must have 2 character-10" data-original-title="Field Patter"><i class="glyphicon glyphicon-info-sign"></i></a>';
				echo "<input class='form-control chapter' value='".$fields[0]['chapter']."'/>";
				echo "</div>";

				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Subject : </label> ";
					echo'<a class="cck" title="" onmouseover="" style="cursor: pointer;" data-content="Must have 3 character-MAT" data-original-title="Field Patter"><i class="glyphicon glyphicon-info-sign"></i></a>';
				echo "<input class='form-control subject' value='".$fields[0]['subject']."'/>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Grade : </label> ";
					echo'<a class="cck" title="" onmouseover="" style="cursor: pointer;" data-content="Must have 2 character-06" data-original-title="Field Patter"><i class="glyphicon glyphicon-info-sign"></i></a>';
				echo "<input class='form-control grade' value='".$fields[0]['grade']."'/>";
				echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but up_crud_activity' tab_id='".$fields[0]['gen_id']."' type='button'>Update</button>
					</div></div>";
			echo "</div>";

		}
		if($file_nm=='MediaMids')
		{	//print_r( $fields);
			$depttype  = $this->input->get('mid_dept');
				$project=$this->crud_model->fetch_full_access('project_mst',2);
				echo "<div class='row'>";
					echo "<div class='row row_style_1'>
						<div class='col-md-3 col-md-offset-1''>";
					echo "<label class='l_font_fix_3 form-check-label' >Projects: </label> ";
					echo "<select class='selectpicker form-control' id='project_mid_id' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($project AS $row)
					{
					$sel='';
						if($fields[0]['project_id_fk']==$row['project_id'])
						{
							$sel='selected';
						}
					echo "<option ".$sel." value='".$row['project_id']."'>".$row['project_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Mids: </label> ";
					echo "<input class='form-control milestone_id' value='".$fields[0]['milestone_id']."'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status: </label> ";
					echo "<input class='form-control status_nm' value='".$fields[0]['status_nm']."'/>";
				echo "</div>";
				echo"</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but up_crud_mediamids' tab_id='".$fields[0]['mile_act_id']."' type='button'>Update</button>
					</div>";
		}

		if($file_nm=='activity_mst')
		{
			$dept=$this->crud_model->fetchdept_data($u_id);
			$dept_opt=$fields[0]['dept_id_fk'];
		$project=$this->crud_model->fetch_full_access('project_mst',$dept_opt);
		$task=$this->crud_model->fetch_full_access('task_mst',$dept_opt);
		$sub=$this->crud_model->fetch_full_access('sub_mst',$dept_opt);
		$job=$this->crud_model->fetch_full_access('job_mst',$dept_opt);
		$wu=$this->crud_model->fetch_full_access('work_unit_mst',$dept_opt);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['activity_id']."</h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Activity Name : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['activity_name']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					$slec="";
					if($fields[0]['dept_id_fk']==$rw['dept_id'])
					{
						$slec="selected";
					}
					echo "<option value='".$rw['dept_id']."' ".$slec.">".$rw['dept_name']."</option>";
				}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, $fields[0]['status_nm'], ['class'=>'selectpicker form-control','id'=>'us_8','old'=>$fields[0]['status_nm']]);
					
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_1'>
						<div class='col-md-3 col-md-offset-1''>";
					echo "<label class='l_font_fix_3 form-check-label' >Projects: </label> ";
					echo "<select class='selectpicker form-control' id='us_2' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($project AS $row)
					{
					$sel='';
						if($fields[0]['project_id_fk']==$row['project_id'])
						{
							$sel='selected';
						}
					echo "<option ".$sel." value='".$row['project_id']."'>".$row['project_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Tasks: </label> ";
					echo "<select class='selectpicker form-control' id='us_3' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($task AS $row)
					{
					$sel='';
						if($fields[0]['task_id_fk']==$row['task_id'])
						{
							$sel='selected';
						}
					echo "<option ".$sel." value='".$row['task_id']."'>".$row['task_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Sub Task: </label> ";
					echo "<select class='selectpicker form-control' id='us_4' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($sub AS $row)
					{
					$sel='';
						if($fields[0]['sub_id_fk']==$row['sub_id'])
						{
							$sel='selected';
						}
					echo "<option ".$sel." value='".$row['sub_id']."'>".$row['sub_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_1'>
						<div class='col-md-3 col-md-offset-1''>";
					echo "<label class='l_font_fix_3 form-check-label' >Job: </label> ";
					echo "<select class='selectpicker form-control' id='us_5' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($job AS $row)
					{
					$sel='';
						if($fields[0]['job_id_fk']==$row['job_id'])
						{
							$sel='selected';
						}
					echo "<option ".$sel." value='".$row['job_id']."'>".$row['job_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Work Unit: </label> ";
					echo "<select class='selectpicker form-control' id='us_6' title='Nothing Selected' data-live-search='true'>";
					echo '<option data-hidden="true"></option>';
					foreach($wu AS $row)
					{
					$sel='';
						if($fields[0]['wu_id_fk']==$row['wu_id'])
						{
							$sel='selected';
						}
					echo "<option ".$sel." value='".$row['wu_id']."'>".$row['wu_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Closed(Y/N): </label> ";
					echo "<select class='selectpicker form-control' id='us_62'>";
					$se1=array('selected','');
					if($fields[0]['closed']=='Y')
					{
						$se1=array('','selected');
					}
					echo "<option ".$se1[0]." value='N'>No</option>";
					echo "<option ".$se1[1]." value='Y'>Yes</option>";
				echo "</select>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['ins_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted On : </label> ";
				echo "<h6 class='h6_sty' >".$fields[0]['ins_dt']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated On : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_dt']."</h6>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but up_crud_activity' tab_id='".$fields[0]['activity_id']."' type='button'>Update</button>
					</div></div>";
			echo "</div>";

		}

		if($file_nm=='work_unit_mst')
		{
			$dept=$this->crud_model->fetchdept_data($u_id);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['work_unit_id']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Work Unit : </label> ";
					echo "<input class='form-control us_1' value='".$fields[0]['work_unit_name']."'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					$slec="";
					if($fields[0]['dept_id_fk']==$rw['dept_id'])
					{
						$slec="selected";
					}
					echo "<option value='".$rw['dept_id']."' ".$slec.">".$rw['dept_name']."</option>";
				}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label'>Work Unit : </label> ";
					echo "<input class='form-control us_2' value='".$fields[0]['is_quantify']."'/>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, $fields[0]['status_nm'], ['class'=>'selectpicker form-control','id'=>'us_8','old'=>$fields[0]['status_nm']]);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['ins_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted On : </label> ";
				echo "<h6 class='h6_sty' >".$fields[0]['ins_dt']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated On : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_dt']."</h6>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but up_crud_work_unit' tab_id='".$fields[0]['work_unit_id']."' type='button'>Update</button>
					</div></div>";
			echo "</div>";

		}

		if($file_nm=='dept_mst')
		{
			//$dept=$this->crud_model->fetchdept_data($u_id);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['dept_id']."</h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Dept Name : </label> ";
					echo "<input class='form-control us_1' value='".$fields[0]['dept_name']."'/>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, $fields[0]['status_nm'], ['class'=>'selectpicker form-control','id'=>'us_8','old'=>$fields[0]['status_nm']]);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['ins_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted On : </label> ";
				echo "<h6 class='h6_sty' >".$fields[0]['ins_dt']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated On : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_dt']."</h6>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but up_crud_dept' tab_id='".$fields[0]['dept_id']."' type='button'>Update</button>
					</div></div>";
			echo "</div>";

		}

		if($file_nm=='job_mst')
		{
			$dept=$this->crud_model->fetchdept_data($u_id);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['job_id']."</h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Job Name : </label> ";
					echo "<input class='form-control us_1' value='".$fields[0]['job_name']."'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					$slec="";
					if($fields[0]['dept_id_fk']==$rw['dept_id'])
					{
						$slec="selected";
					}
					echo "<option value='".$rw['dept_id']."' ".$slec.">".$rw['dept_name']."</option>";
				}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, $fields[0]['status_nm'], ['class'=>'selectpicker form-control','id'=>'us_8','old'=>$fields[0]['status_nm']]);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['ins_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted On : </label> ";
				echo "<h6 class='h6_sty' >".$fields[0]['ins_dt']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated On : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_dt']."</h6>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but up_crud_job' tab_id='".$fields[0]['job_id']."' type='button'>Update</button>
					</div></div>";
			echo "</div>";

		}
		if($file_nm=='sub_mst')
		{
			$dept=$this->crud_model->fetchdept_data($u_id);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['sub_id']."</h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >sub Name : </label> ";
					echo "<input class='form-control us_1' value='".$fields[0]['sub_name']."'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					$slec="";
					if($fields[0]['dept_id_fk']==$rw['dept_id'])
					{
						$slec="selected";
					}
					echo "<option value='".$rw['dept_id']."' ".$slec.">".$rw['dept_name']."</option>";
				}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, $fields[0]['status_nm'], ['class'=>'selectpicker form-control','id'=>'us_8','old'=>$fields[0]['status_nm']]);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['ins_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted On : </label> ";
				echo "<h6 class='h6_sty' >".$fields[0]['ins_dt']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated On : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_dt']."</h6>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but up_crud_sub' tab_id='".$fields[0]['sub_id']."' type='button'>Update</button>
					</div></div>";
			echo "</div>";

		}
		if($file_nm=='task_mst')
		{
			$dept=$this->crud_model->fetchdept_data($u_id);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['task_id']."</h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Task Name : </label> ";
					echo "<input class='form-control us_1' value='".$fields[0]['task_name']."'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					$slec="";
					if($fields[0]['dept_id_fk']==$rw['dept_id'])
					{
						$slec="selected";
					}
					echo "<option value='".$rw['dept_id']."' ".$slec.">".$rw['dept_name']."</option>";
				}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, $fields[0]['status_nm'], ['class'=>'selectpicker form-control','id'=>'us_8','old'=>$fields[0]['status_nm']]);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['ins_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted On : </label> ";
				echo "<h6 class='h6_sty' >".$fields[0]['ins_dt']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated On : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_dt']."</h6>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but up_crud_task' tab_id='".$fields[0]['task_id']."' type='button'>Update</button>
					</div></div>";
			echo "</div>";

		}
		if($file_nm=='project_mst')
		{
			$dept=$this->crud_model->fetchdept_data($u_id);
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['project_id']."</h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Project Name : </label> ";
					echo "<input class='form-control us_1' value='".$fields[0]['project_name']."'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
				echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Depts : </label> ";
				echo "<select class='form-control dep_1'>";
				foreach($dept AS $rw)
				{
					$slec="";
					if($fields[0]['dept_id_fk']==$rw['dept_id'])
					{
						$slec="selected";
					}
					echo "<option value='".$rw['dept_id']."' ".$slec.">".$rw['dept_name']."</option>";
				}
				echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, $fields[0]['status_nm'], ['class'=>'selectpicker form-control','id'=>'us_8','old'=>$fields[0]['status_nm']]);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['ins_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted On : </label> ";
				echo "<h6 class='h6_sty' >".$fields[0]['ins_dt']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated On : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_dt']."</h6>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but up_crud_project' tab_id='".$fields[0]['project_id']."' type='button'>Update</button>
					</div></div>";
			echo "</div>";

		}
		if($file_nm=='access_mst')
		{
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['access_id']."</h6>";
				echo "</div>";
				echo "<div class='col-md-4'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Access Name : </label> ";
					echo "<input class='form-control us_1' value='".$fields[0]['access_name']."'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Icon : </label> ";
					echo "<input class='form-control us_2' value='".$fields[0]['icon']."' />";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Display Name : </label> ";
					echo "<input class='form-control us_3' value='".$fields[0]['tab_name']."' />";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_1'>";
				echo "<div class='col-md-3  col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Tab Color : </label> ";
					echo "<input class='form-control us_4' value='".$fields[0]['tab_color']."' />";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, $fields[0]['status_nm'], ['class'=>'selectpicker form-control','id'=>'us_8','old'=>$fields[0]['status_nm']]);
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['ins_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted On : </label> ";
				echo "<h6 class='h6_sty' >".$fields[0]['ins_dt']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated On : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_dt']."</h6>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but up_crud_access' tab_id='".$fields[0]['access_id']."' type='button'>Update</button>
					</div></div>";
			echo "</div>";

		}
		if($file_nm=='role_mst')
		{
			$acc=$this->crud_model->fetch_full_access('access_mst');
			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['role_id']."</h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Role Name : </label> ";
					echo "<input class='form-control us_1' value='".$fields[0]['role_name']."'/>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, $fields[0]['status_nm'], ['class'=>'selectpicker form-control','id'=>'us_8','old'=>$fields[0]['status_nm']]);
					
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-5 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Access : </label> ";
				$a_nm=explode("|",$fields[0]['access_name']);
				$j=1;
				foreach($acc AS $rw)
				{
					$ch='';
					$selv_val=$rw['access_name'];
					$add_arr=array("Audit","Allocation","Review","Confirm");
					if(in_array($selv_val,$a_nm))
					{
					$ch='checked="checked"';
					}
					$n='';
					if($j%2==0)
					{
						$n="<br>";
					}
					echo "<input name='jjj' class='ch_tog' ".$ch." type='checkbox' at='".$selv_val."'> ".$selv_val." ".$n;
					$j++;
					if($rw['access_name']=='Confirmation Process')
					{
						foreach($add_arr AS $re)
						{
							$selv_val= $re;
							$ch='';
							if(in_array($selv_val,$a_nm))
								{
								$ch='checked="checked"';
								}
								$n='';
								if($j%2==0)
								{
									$n="<br>";
								}
								echo "<input name='jjj' class='ch_tog' ".$ch." type='checkbox' at='".$selv_val."'> ".$selv_val." ".$n;
								$j++;
						}
					}
				}
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['ins_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted On : </label> ";
				echo "<h6 class='h6_sty' >".$fields[0]['ins_dt']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated On : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_dt']."</h6>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but up_crud_role' tab_id='".$fields[0]['role_id']."' type='button'>Update</button>
					</div></div>";
			echo "</div>";

		}
		if($file_nm=='user_mst')
		{
			$acc=$this->crud_model->fetch_full_access('access_mst');
			$role=$this->crud_model->fetch_full_access('role_mst');
			$user_manager=$this->crud_model->fetch_user($fields[0]['id']);
			$dept=$this->crud_model->fetch_full_access('dept_mst');
			$proj=$this->crud_model->fetch_full_access('project_mst');

			echo "<div class='row'>";
				echo "<div class='row row_style_1'>
				<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >ID : </label> ";
					echo "<h6 class='h6_sty'>".$fields[0]['id']."</h6>";
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' required >Email : </label> ";
					echo "<input class='form-control us_1' type='email' value='".$fields[0]['user_login_name']."'/>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Employee ID : </label> ";
					echo "<input class='form-control us_2' value='".$fields[0]['emp_id']."'/>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
					$d_nm=explode(",",$fields[0]['dept_id_fk']);
				$j=2;
				$ch='';
				if($d_nm[0]==0)
					 {
						$ch='checked="checked"';
					 }
				echo "<input name='dept_dd' class='ch_tog dept_ch1' ".$ch." value='0' type='checkbox'><span> All Depts </span>";
				
				foreach($dept AS $rw)
				{
					$ch='';
					$selv_d_val=$rw['dept_id'];
					if(in_array($selv_d_val,$d_nm))
					{
					$ch='checked="checked"';
					}
					$n='';
					if($j%2==0)
					{
						$n="<br>";
					}
					echo "<input name='dept_dd' class='ch_tog dept_ch' ".$ch." value='".$selv_d_val."' type='checkbox'><span> ".$rw['dept_name']." </span>".$n;
					$j++;
				}
				echo "</div>";
					echo "<div class='col-md-4'>";
						echo "<label class='l_font_fix_3 form-check-label' >User Name : </label> ";
						echo "<input class='form-control us_4' value='".$fields[0]['full_name']."'/>";
					echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Role : </label> ";
					echo "<select class='selectpicker form-control' id='us_5' title='Nothing Selected' data-live-search='true'>";

					foreach($role AS $row)
					{
						$se='';
						if($fields[0]['role_id_fk']==$row['role_id'])
						{
							$se='selected';
						}
					echo "<option ".$se." value='".$row['role_id']."'>".$row['role_name']."</option>";
					}
					echo "</select>";

				echo "</div>";

				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-1 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Streak : </label> ";
					echo "<input class='form-control us_6' value='".$fields[0]['streak']."'/>";
				echo "</div>";
				echo "<div class='col-md-6'>";
					echo "<label class='l_font_fix_3 form-check-label' >Reporting Manager : </label> ";
					echo "<select class='selectpicker form-control' id='us_7' title='Nothing Selected' data-live-search='true'>";
					$se='';
						if(!($fields[0]['manager']))
						{
							$se='selected';
						}
						echo '<option '.$se.' value="">None</option>';
					foreach($user_manager AS $row1)
					{
						$se='';
						if($fields[0]['manager']==$row1['user_id'])
						{
							$se='selected';
						}
					echo "<option ".$se." value='".$row1['user_id']."'>".$row1['full_name']."</option>";
					}
					echo "</select>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Status : </label> ";
					echo form_dropdown('status', $this->status, $fields[0]['status_nm'], ['class'=>'selectpicker form-control','id'=>'us_8','old'=>$fields[0]['status_nm']]);
				echo "</div>";
				echo "</div>";

				echo "<div class='row row_style_4'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
						echo "<label class='l_font_fix_3 form-check-label'>Joining Date (Intern / Trainee) : </label> ";
						echo form_input(['type'=>'text','name'=>'joining_date_intern','id'=>'joining_date_intern','class'=>'form-control join-date','value'=>$fields[0]['joining_date_intern']]);
					echo "</div>";

					echo "<div class='col-md-4'>";
						echo "<label class='l_font_fix_3 form-check-label'>Joining Date Permanent : </label> ";
						echo form_input(['type'=>'text','name'=>'joining_date_permanant','id'=>'joining_date_permanant','class'=>'form-control join-date','value'=>$fields[0]['joining_date_permanant']]);
					echo "</div>";

					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label'>Slot : </label> ";
						echo form_input(['type'=>'text','name'=>'slot','id'=>'slot','class'=>'form-control','value'=>$fields[0]['slot']]);
					echo "</div>";

				echo "</div>";

				echo "<div class='row row_style_4'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
						echo "<label class='l_font_fix_3 form-check-label'>Sub Department : </label> ";
						echo form_input(['type'=>'text','name'=>'sub_department','id'=>'sub_department','class'=>'form-control','value'=>$fields[0]['sub_department']]);
					echo "</div>";

					echo "<div class='col-md-4'>";
						echo "<label class='l_font_fix_3 form-check-label'>Unit Name : </label> ";
						echo form_input(['type'=>'text','name'=>'unit_name','id'=>'unit_name','class'=>'form-control','value'=>$fields[0]['unit_name']]);
					echo "</div>";

					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label'>Vertical : </label> ";
						echo form_input(['type'=>'text','name'=>'vertical','id'=>'vertical','class'=>'form-control','value'=>$fields[0]['vertical']]);
					echo "</div>";

					
				echo "</div>";

				echo "<div class='row row_style_4'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
						echo "<label class='l_font_fix_3 form-check-label'>Designation : </label> ";
						echo form_input(['type'=>'text','name'=>'designation','id'=>'designation','class'=>'form-control','value'=>$fields[0]['designation']]);
					echo "</div>";
					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label'>Grading Access : </label> ";
						echo form_dropdown('is_greading',["N" => "No","Y" => "Yes"],$fields[0]['is_greading'],['class'=>'selectpicker form-control','id'=>'is_greading']);
					echo "</div>";
				echo "</div>";

				echo "<div class='row row_style_4'>";
					echo "<div class='col-md-3 col-md-offset-1'>";
						$user_manager_data=array_combine(array_column($user_manager, 'user_id'), array_column($user_manager, 'full_name'));
						echo "<label class='l_font_fix_3 form-check-label' >Manager : </label> ";
						echo form_dropdown('manager',[NULL => "Nothing Selected"]+$user_manager_data,$fields[0]['manager_id_fk'],['class'=>'selectpicker form-control','id'=>'manager','data-live-search'=>'true']);
					echo "</div>";

					echo "<div class='col-md-4'>";
						echo "<label class='l_font_fix_3 form-check-label' >Project Owner : </label> ";
						echo form_dropdown('project_owner',[NULL => "Nothing Selected"] + $user_manager_data,$fields[0]['project_owner_id_fk'],['class'=>'selectpicker form-control','id'=>'project_owner','data-live-search'=>'true']);
					echo "</div>";

					echo "<div class='col-md-3'>";
						echo "<label class='l_font_fix_3 form-check-label' >POC : </label> ";
						echo form_dropdown('poc',[NULL => "Nothing Selected"] + $user_manager_data,$fields[0]['poc_id_fk'],['class'=>'selectpicker form-control','id'=>'poc','data-live-search'=>'true']);
						
					echo "</div>";
				echo "</div>";

				echo "<div class='row row_style_4'>
						<div class='col-md-5 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Access : </label> ";
				$a_nm=explode("|",$fields[0]['access_name']);
				$j=1;
				foreach($acc AS $rw)
				{
					$ch='';
					$selv_val=$rw['access_name'];
					$add_arr=array("Audit","Allocation","Review","Confirm");

					if(in_array($selv_val,$a_nm))
					{
					$ch='checked="checked"';
					}
					$n='';
					if($j%2==0)
					{
						$n="<br>";
					}
					echo "<input name='jjj' class='ch_tog' ".$ch." type='checkbox' at='".$selv_val."'> ".$selv_val." ".$n;
					$j++;
					if($rw['access_name']=='Confirmation Process')
					{
						foreach($add_arr AS $re)
						{
							$selv_val= $re;
							$ch='';
							if(in_array($selv_val,$a_nm))
								{
								$ch='checked="checked"';
								}
								$n='';
								if($j%2==0)
								{
									$n="<br>";
								}
								echo "<input name='jjj' class='ch_tog' ".$ch." type='checkbox' at='".$selv_val."'> ".$selv_val." ".$n;
								$j++;
						}
					}
				}
				echo "</div>";
				echo "<div class='col-md-5'>";
					echo "<label class='l_font_fix_3 form-check-label' style='width:100%;' >Projects : </label> ";
					$p_nm=explode(",",$fields[0]['project_id_fk']);
				$j=1;
				$ch='';
				if($p_nm[0]==0)
					 {
						$ch='checked="checked"';
					 }

				echo "<input name='ppp' class='ch_tog' ".$ch." value='0' type='checkbox'><span> All Projects </span>";

				foreach($proj AS $rw)
				{
					$ch='';
					$hide=' hide ';
					$selv_val=$rw['project_id'];
					if(in_array($rw['dept_id_fk'],$d_nm) || $d_nm[0]==0)
					{
						if(in_array($selv_val,$p_nm))
						{
							$ch='checked="checked"';
						}
					$hide='';
					}

					$n='';
					if($j%4==0)
					{
						$n="<br>";
					}
					echo "<span class='pro_user ".$hide."'>
					<input name='ppp' class='ch_tog' dep='".$rw['dept_id_fk']."' ".$ch." value='".$selv_val."' type='checkbox'>
					<span> ".$rw['project_name']." </span>".$n."</span>";
					$j++;
				}
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_4'>
						<div class='col-md-3 col-md-offset-1'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['ins_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Inserted On : </label> ";
				echo "<h6 class='h6_sty' >".$fields[0]['ins_dt']."</h6>";
				echo "</div>";
				echo "<div class='col-md-3'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated By : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_user']."</h6>";
				echo "</div>";
				echo "<div class='col-md-2'>";
					echo "<label class='l_font_fix_3 form-check-label' >Updated On : </label> ";
					echo "<h6 class='h6_sty' >".$fields[0]['upd_dt']."</h6>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_2'><div class='col-md-12'>
					<label class='l_font_fix_3 invisible'>Update</label>
					<button class='btn add_but up_crud_user' tab_id='".$fields[0]['id']."' type='button'>Update</button>
					</div></div>";
			echo "</div>";

		}
	}

	public function work_request(){
		$postedData = $_POST["TableDa"];
		$ary=array();
		$i=0;
		$RequesterId=$this->input->post('RequesterId');
		$RequesterTlId=$this->input->post('RequesterTlId');
		$Requesterdt=$this->input->post('Requesterdt');
		$Requesterdt=date('Y-m-d',strtotime($Requesterdt));
		$pro_id		=$this->input->post('pro_id');
		$dept_id	=$this->input->post('dept_id');
		$tempData = str_replace("\\", "",$postedData);
		$TableData = json_decode($tempData);
		$user_id            = $this->session->userdata('user_id');
		$role_id            = $this->session->userdata('role_id');
		// print_r($TableData);
			foreach($TableData as $td){

					$ary[$i]['emp_user_id']=$td->Employee_user_id;
					//$ary[$i]['emp_id']=$td->Tl_no;
					$ary[$i]['called_dt']=date('Y-m-d H:i:s',strtotime($td->Date));
					$ary[$i]['project_id_fk']=$pro_id;
					$ary[$i]['dept_id_fk']=$dept_id;
					$ary[$i]['combo_type']=$td->radioselectid;
					$ary[$i]['req_user_id']=$RequesterId;
					$ary[$i]['requester_emp_id']=$RequesterTlId;
					$ary[$i]['requester_requesting_date']=$Requesterdt;
					$ary[$i]['reason']=$td->Reason;
					$ary[$i]['conf_status']='Pending';
					$ary[$i]['ins_user']=$user_id ;
					$ary[$i]['role_id']=$role_id ;
					$ary[$i]['ins_dt']=date('Y-m-d H:i:s');
					$ary[$i]['mail_flag']=0;

			$i++;
			}
			$result=$this->crud_model->save_work_request($ary);
			 if($result){
			 	echo "Sucessfully applied for work request";
			 }


	}

	//Work request od send
	public function workRequestOd(){
		$response['status'] = false;
		$data = $this->input->post();

		$requestDepartment 	= $this->input->post("department");	
		$requestDepartmentName 	= $this->input->post("dept_name");	
		$requestProject = $this->input->post("project");	
		$requestProjectName = $this->input->post("project_name");	
		$requestUser = $this->session->userdata('user_id');
		$requestUserDate = date("Y-m-d");
		$requesterOdEmp = $this->input->post("requester");
		$requesterDate = $this->input->post("s_dt");
		$requesterPin = $this->input->post("punch_in");
		$requesterPout = $this->input->post("punch_out");
		$requesterOdReason = $this->input->post("reason");
		$requesterName = $this->input->post("requesterName");

		$requestOdDtata = [];
		$requestEmailData = [];
		if($requesterOdEmp){
			foreach ($requesterOdEmp as $key => $value) {
				$requestOdDtata[] = [
					'emp_user_id' => $requestUser,
					'emp_request_date' => $requestUserDate,
					'dept_id_fk' => $requestDepartment,
					'project_id_fk' => $requestProject,
					'req_user_id' => $value,
					'requester_requesting_date' => ($requesterDate[$key]) ? date("Y-m-d",strtotime($requesterDate[$key])): NULL,
					'requester_punch_in_time' => $requesterPin[$key],
					'requester_punch_out_time' => $requesterPout[$key],
					'reason' => $requesterOdReason[$key],
					'ins_user' => $requestUser
				];

				$requestEmailData[] = [
					'requesterName' => $requesterName[$key],
					'project_name' => $requestProjectName,
					'emp_request_date' => $requestUserDate,
					'requester_requesting_date' => ($requesterDate[$key]) ? date("d-m-Y",strtotime($requesterDate[$key])): NULL,
					'requester_punch_in_time' => $requesterPin[$key] ? $requesterPin[$key] : "-",
					'requester_punch_out_time' => $requesterPout[$key] ? $requesterPout[$key] : "-",
					'reason' => $requesterOdReason[$key]
				];
			}
			$this->db->trans_start();
			// echo "<pre>";
			// print_r($requestEmailData); die;
			if($this->crud_model->insOdRequest($requestOdDtata)){
				if($this->mailsend->sendOdRequestMail($requestEmailData,$requestProject,$requestUser,$requestDepartment)){
					$this->db->trans_complete();
					if($this->db->trans_status() !== FALSE){
						$response['status'] =  true;
						$response['message'] =  "OD requested successfully.";
					}
				}
				else{
					$response['message'] =  "Unable to send mail.";
				}
				
			}else{
				$response['message'] =  "Something went wrong to od request.";
			}
		}
		header('Content-Type: application/json');
		echo json_encode($response);
		exit;
	}

	// GET WORK REQUEST
	public function load_work_request(){
		$dept            = $this->input->get('dept');
		 $rol            = $this->session->userdata('role_id');
		 $stat='Pending';
		 $user_id=$this->session->userdata('user_id');
		 $pids=0;
		 if($rol==4){
		 $pids=$this->session->userdata('proj_fk');
		 }
			$tab_dta = $this->crud_model->get_work_request($pids,$user_id,$stat,$dept);

		print_r($tab_dta);die;
	}

	//Get Pending OD Requests
	public function load_od_request(){
		$dept = $this->input->get('dept');
		$role = $this->session->userdata('role_id');
		$stat= $this->input->get("status");
		$final_approve = $this->input->get("f_approve");
		$user_id=$this->session->userdata('user_id');
		$pids=0;
		if($role==4){
		 	$pids=$this->session->userdata('proj_fk');
		}
		$odRequestData = $this->crud_model->get_od_request($pids,$user_id,$stat,$dept,$final_approve);
		header('Content-Type: application/json');
		echo json_encode($odRequestData);
		die;
	}
	
	public function load_final_work_request(){
		$dept            = $this->input->get('dept');
			 $rol            = $this->session->userdata('role_id');
			 $stat='Confirmed';
			 $user_id=$this->session->userdata('user_id');
			 $pids=0;
			 if($rol==4){
			 $pids=$this->session->userdata('proj_fk');
			 }
				$tab_dta = $this->crud_model->get_work_request($pids,$user_id,$stat,$dept);

			print_r($tab_dta);
	}

	// view work request
	public function view_work_request(){
		$dept            = $this->input->get('dept');
			 //$rol=$this->user_model->getUserRoleInfo();
			 $rol            = $this->session->userdata('role_id');
			 $stat= $this->input->get('stat');
			$pids=0;
			$user_id=$this->session->userdata('user_id');
			 if($rol==4){
			 $pids=$this->session->userdata('proj_fk');
			 }
			 if($stat=="all")
			 {
				 $stat='';
			 }
				$tab_dta = $this->crud_model->get_work_request_stat($pids,$user_id,$stat,$dept); 
						
			print_r($tab_dta);
	}



	// Update work request status
	public function upate_work_request_status(){
  		$sel_status=$this->input->post('sel_status');
     	$checkedRows=$this->input->post('checkedRows');
		$rema=$this->input->post('rem');
     	$user_id            = $this->session->userdata('user_id');
     	if($sel_status!='' && !empty($checkedRows)){
     		$myrows=array_column($checkedRows, 'work_request_id');
			$tab_dta = $this->crud_model->update_work_stat($myrows,$user_id,$sel_status,$rema);
     		if($tab_dta){
	           		 echo "Successfully";
	          		}
		}
	}



	//Update od request status
	public function update_od_request_status(){
		$response['status'] = false;
		$sel_status=$this->input->post('sel_status');
     	$checkedRows=$this->input->post('checkedRows');
     	$remark=$this->input->post('remark');
     	$user_id  = $this->session->userdata('user_id');
     	if($sel_status!='' && !empty($checkedRows)){
     		$update_user_id=array_column($checkedRows, 'wo_id');
     		$updateStatus = $this->crud_model->update_od_request($update_user_id,$user_id,$sel_status,$remark);
     		if($updateStatus){
     			$response['status'] = true;
     		}
     	}
     	header('Content-Type: application/json');
		echo json_encode($response);
		die;
	}

	//approve_with_combotype
	public function approve_with_combotype(){
		$combotype=$this->input->post('combotype');
     	$user_row_id=$this->input->post('user_row_id');
     	$user_id    = $this->session->userdata('user_id');
     	if($combotype!='' && !empty($user_row_id)){
     		//$myrows=array_column($checkedRows, 'work_request_id');
     		//print_r($myrows);die;
			$tab_dta = $this->crud_model->approve_combotype_stat($user_row_id,$user_id,$combotype);
     		if($tab_dta){
	           		 echo "Successfully";
	          		}
		}
	}


}
?>