<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notify extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('alert_model');
		 $this->load->model('user_model');
        $this->load->library('session');
        $this->load->library("PHPMailer_Library");
    }

	public function leave_mail()
    {
			$ans = $this->input->post("C");
			$param = $this->input->post("param");

			$po_id = $this->input->post("po");

			if($ans)
			{
			$mail_user=array();
			if($param=='W')
			{
			$c2=$this->alert_model->l_mail_week($ans);
			$week_di=array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
			}else{
			$c2=$this->alert_model->l_mail($ans);
			}
			//print_r($c2);
			if($c2)
			{
			foreach($c2 AS $row1)
			{
					$mail = $this->phpmailer_library->load();
					$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
					$mail->addAddress($row1['man_mail']);
					if($po_id )
					{
						$pid=implode(",",$po_id);
					$c3=$this->alert_model->proj_own_list($pid,'4');
					//print_r($c3);
						foreach($c3 AS $ma)
						{
						$mail->addAddress($ma['email']);
						}
					}
					$mail->addCC($row1['us_mail']);
					$mail->isHTML(true);
					$mail->Subject = 'DWA: Leave Tracker Update '.$row1['full_name'].' !!';
					$ct=explode("|",$row1['date_name']);
					$message= 'Hi '.$row1['man_name'].',<br>Please approve the below pending leave:<br>';
					if($param=="W")
					{
					$message=$message."<table border='1'><thead><th>Employee</th><th>Leave Type</th><th>".$ct[0]."</th><th>".$ct[1]."</th><th>Day of Week</th><th>Status</th></thead>";
					$message=$message.'<tr><td>'.$row1['full_name'].'</td>'.'<td>'.$row1['leave_type_name'].'</td>'.'<td>'.$row1['st_dt'].'</td>'.'<td>'.$row1['en_dt'].'</td>'.'<td>'.$week_di[$row1['num_days']].'</td>'.'<td>'.$row1['conf_status'].'</td>'.'</tr>';
					$message=$message."</table>";
					}else{
						$message=$message."<table border='1'><thead><th>Employee</th><th>Leave Type</th><th>".$ct[0]."</th><th>".$ct[1]."</th><th>No of Days</th><th>Reason</th><th>Desc</th><th>Status</th></thead>";
					$message=$message.'<tr><td>'.$row1['full_name'].'</td>'.'<td>'.$row1['leave_type_name'].'</td>'.'<td>'.$row1['st_dt'].'</td>'.'<td>'.$row1['en_dt'].'</td>'.'<td>'.$row1['num_days'].'</td>'.'<td>'.$row1['reason'].'</td>'.'<td>'.$row1['h_day'].'</td>'.'<td>'.$row1['conf_status'].'</td>'.'</tr>';
					$message=$message."</table>";
					}
				$mail->Body    = $message;
				//print_r($message);
				if(!$mail->Send())
				{
					$sendError = $mail->ErrorInfo;
				}
				else{
					return "success";
					}
				}
			}
		}
	}

	public function leave_mail_status()
    {
			$ans = $this->input->post("C");
			$param = $this->input->post("param");
			if($ans)
			{
			$mail_user=array();
			if($param=='W')
			{
			$c2=$this->alert_model->l_mail_week($ans);
			$week_di=array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
			}else{
			$c2=$this->alert_model->l_mail($ans);
			}

			if($c2)
			{
			foreach($c2 AS $row1)
			{
					$mail = $this->phpmailer_library->load();
					$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
					$mail->addCC($row1['man_mail']);
					$mail->addAddress($row1['us_mail']);
					$mail->isHTML(true);
					$mail->Subject = 'DWA: Leave Tracker Update '.$row1['full_name'].' !!';
					$ct=explode("|",$row1['date_name']);
					$message= 'Hi '.$row1['full_name'].',<br>Please find below the leave approval status:<br>';
					if($param=="W")
					{
					$message=$message."<table border='1'><thead><th>Employee</th><th>Leave Type</th><th>".$ct[0]."</th><th>".$ct[1]."</th><th>Day of Week</th><th>Status</th></thead>";
					$message=$message.'<tr><td>'.$row1['full_name'].'</td>'.'<td>'.$row1['leave_type_name'].'</td>'.'<td>'.$row1['st_dt'].'</td>'.'<td>'.$row1['en_dt'].'</td>'.'<td>'.$week_di[$row1['num_days']].'</td>'.'<td>'.$row1['conf_status'].'</td>'.'</tr>';
					$message=$message."</table>";
					}else{
						$message=$message."<table border='1'><thead><th>Employee</th><th>Leave Type</th><th>".$ct[0]."</th><th>".$ct[1]."</th><th>No of Days</th><th>Reason</th><th>Desc</th><th>Status</th></thead>";
					$message=$message.'<tr><td>'.$row1['full_name'].'</td>'.'<td>'.$row1['leave_type_name'].'</td>'.'<td>'.$row1['st_dt'].'</td>'.'<td>'.$row1['en_dt'].'</td>'.'<td>'.$row1['num_days'].'</td>'.'<td>'.$row1['reason'].'</td>'.'<td>'.$row1['h_day'].'</td>'.'<td>'.$row1['conf_status'].'</td>'.'</tr>';
					$message=$message."</table>";
					}

				$mail->Body    = $message;
				//print_r($message);
					if(!$mail->Send())
					{
						$sendError = $mail->ErrorInfo;
					}
					else{
					return "success";
					}
				}
			}
		}
	}


	public function work_confirm_mail(){
		$checkedRows=$this->input->post('checkedRows');
		if(!empty($checkedRows))
		{
		$myrows=array_column($checkedRows, 'work_request_id'); // will get the selected work request id
		$i=0;
		$j=0;
		$k=0;
		$proj=array();
		$proj1=array();


			$ck=implode(",",$myrows);
		    $work_req_users=$this->alert_model->getWorkReqyest($ck);
			$dep=array();
			$user=array();
			foreach($work_req_users as $val){
						$proj1[]=$val['project_id_fk'];
						$dep[]=$val['dept_id_fk'];
						$user[$val['project_id_fk']][] = array('user_login_name'=>$val['user_login_name'],
																'full_name'=>$val['full_name'],
																'called_dt'=>date('d-m-Y',strtotime($val['called_dt'])),
																'project_name'=>$val['project_name'],
																'combos_type'=>$val['combos_type'],
																'conf_status'=>$val['conf_status'],
																'req_name'=>$val['req_name']);
			}
			$proj=array_unique($proj1);
			// print_r($proj);
			// print_r($user);
			$smgr=array();
			if(!empty($dep))
			{
				foreach($dep AS $v1)
				{
					$smgr[$v1]=$this->alert_model->getSeniorManager(0,2,$v1);
				}
			}
				//print_r($smgr);
			if(!empty($proj)){
					foreach($proj as $key=>$val){
					$mail = $this->phpmailer_library->load();
					$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
					$mail->Subject = 'Work request status';
					$mail->isHTML(true);
						$message='Hi,<br><br>Please find the status of Work Request: <br>
					<table  border="1">
						<thead>
							<th>S.NO</th>
							<th>Employee</th>
							<th>Requested Date</th>
							<th>Requested Project</th>
							<th>Options</th>
							<th>Request Status</th>
							<th>Requested By</th>
						</thead>';

						$dept_i='';
						$c3=$this->alert_model->proj_own_list($val,'4');
							if($c3){
								foreach($c3 AS $ma){
										$mail->addAddress($ma['email']);
										$dept_i=$ma['dept_id_fk'];
										//echo $ma['email'];
								}
							}

								if(array_key_exists($dept_i,$smgr)){
									foreach($smgr[$dept_i] as $sm){
										$mail->addCC($sm['email']);
										//echo $sm['email'];
									}
								}
								if(array_key_exists($val,$user)){
								foreach($user[$val] as $key=>$va){
										//echo $va['full_name'];
										$message=$message.'<tr>';
										$message=$message.'<td>'.(1+$key).'</td>';
										$message=$message.'<td>'.$va['full_name'].'</td>';
										$message=$message.'<td>'.$va['called_dt'].'</td>';
										$message=$message.'<td>'.$va['project_name'].'</td>';
										$message=$message.'<td>'.$va['combos_type'].'</td>';
										$message=$message.'<td>'.$va['conf_status'].'</td>';
										$message=$message.'<td>'.$va['req_name'].'</td>';
										$message=$message.'</tr>';
								}
								}

								$message=$message."</table><br><br>";

						//print_r($message);
					if(!$mail->Send())
					{
						$sendError = $mail->ErrorInfo;
					}
					else{
					return "success";
					}
					}
			}


					foreach($work_req_users as $val){
						if($val['conf_status']=="Approved")
						{
				    		$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
						   $mail->Subject = 'Work Request confirmed';
				 		$mail->isHTML(true);
				 		$mail->addAddress($val['user_login_name']);
				 		$message="Hi ".$val['full_name'].",<br>Your Work Request has been confirmed-<br>
				 			<table border='1'>
				 				<thead>
									<th>Employee Name</th>
									<th>Requested Date</th>
									<th>Requested Project</th>
				 					<th>Option</th>
				 					<th>Request Status</th>
									<th>Request By</th>
				 				</thead>
				 				<tr>
				 					<td>".$val['full_name']."</td>
				 					<td>".date('d-m-Y',strtotime($val['called_dt']))."</td>
									<td>".$val['project_name']."</td>
				 					<td>".$val['combos_type']."</td>
				 					<td>".$val['conf_status']."</td>
									<td>".$val['req_name']."</td>
				 				</tr>
							</table>";
						$mail->Body=$message;
						if(!$mail->Send())
					{
						$sendError = $mail->ErrorInfo;
					}
					else{
					return "success";
					}
					//print_r($message);
			  	}
					}
			}
    }


	// mail function for work request
	public function  work_request_mail(){
			$postedData = $_POST["TableDa"];
			$ary=array();
			$RequesterId=$this->input->post('RequesterId');
			$Requesterdt=$this->input->post('Requesterdt');
			$Requesterdt=date('d-m-Y',strtotime($Requesterdt));
			$tempData = str_replace("\\", "",$postedData);
			$TableData = json_decode($tempData);
			$pro_id		=$this->input->post('pro_id');
			$dept_id	=$this->input->post('dept_id');
			$user_id            = $this->session->userdata('user_id');
			$role_id            = $this->session->userdata('role_id');
			$mail = $this->phpmailer_library->load();
			$smgr=$this->alert_model->getSeniorManager($pro_id,2,$dept_id);
			$pow=$this->alert_model->getRequesterMail($RequesterId);
			//print_r($pow);
			$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
			$mail->Subject = 'Work request intiated';
			$mail->isHTML(true);
			$i=1;
			if($dept_id == "1"){
				$mail->addAddress("sarosh.kb@byjus.com");
			}
			if($dept_id == "2"){
				$mail->addAddress("gaurav.rai@byjus.com");
			}
			if($smgr){
				foreach($smgr as $sm){
					$mail->addAddress($sm['email']);
				}
			}
			$c3=$this->alert_model->proj_own_list($pro_id,'4');
				if($c3){
					foreach($c3 AS $ma){
						$mail->addAddress($ma['email']);
					}
				}
			if($pow){
				$mail->addCC($pow[0]['email']);
			}
			$message='Hi,<br>Please find the list of employees requested for work by -"'.$pow[0]['full_name'].'"<br><br>
				<table border="1" style=" border-collapse: collapse;width: 100%;">
					<thead>
						<th style="text-align:left;padding:8px;background-color: #813588;color:white;">NO</th>
						<th style="text-align:left;padding:8px;background-color: #813588;color:white;">Employee Name</th>
						<th style="text-align:left;padding:8px;background-color: #813588;color:white;">Project Requested</th>
						<th style="text-align:left;padding:8px;background-color: #813588;color:white;">Requested Date</th>
						<th style="text-align:left;padding:8px;background-color: #813588;color:white;">Option</th>
					</thead>';
					foreach($TableData as $tb){
						$message=$message.'<tr>
											<td style="text-align:left;padding:8px;">'.$i++.'</td>
											<td style="text-align:left;padding:8px;">'.$tb->Name.'</td>
											<td style="text-align:left;padding:8px;">'.$tb->Project.'</td>
											<td style="text-align:left;padding:8px;">'.$Requesterdt.'</td>
											<td style="text-align:left;padding:8px;">'.$tb->Combo_type.'</td>
										</tr>';
					}
			$message=$message.'</table><br><br>';
			$mail->Body=$message;
				if(!$mail->Send())
				{
				  $sendError = $mail->ErrorInfo;
				}else{
				   echo 'Message has been sent';
				}
				//print_r($message);
	}

	public function sendLeaveStatusNotificationToRM()
	{
		$data = array();
		// Get the list of leaves from 26th of previous month to 25th of current month along with the Reporting Manager
		$startDate = date('Y-m-d', strtotime('+25 days', strtotime('first day of previous month')));
		$endDate = date('Y-m-d', strtotime('+24 days', strtotime('first day of this month')));

		$leaveData = $this->user_model->getLeavesByDateRange(
			$startDate,
			$endDate
		);

		$managerList = array_unique(
			array_map(function ($ar) {return $ar['manager_email'];}, $leaveData)
		);

		// Now loop through managerList to send email individually
		foreach ($managerList as $key => $manager) {
			$emailMessageData = array_filter(array_map(function ($ar) use($manager) {if ($ar['manager_email'] == $manager) return $ar;}, $leaveData));

			$pendingLeave = array_values(array_filter(array_map(function ($ar) use($manager) {if ($ar['conf_status'] == 'Pending') return $ar;}, $emailMessageData)));
			$approvedLeave = array_values(array_filter(array_map(function ($ar) use($manager) {if ($ar['conf_status'] == 'Approved') return $ar;}, $emailMessageData)));
			$rejectedLeave = array_values(array_filter(array_map(function ($ar) use($manager) {if ($ar['conf_status'] == 'Rejected') return $ar;}, $emailMessageData)));

			// Now here get the html of email message from view
			$emailMessage = $this->load->view("notifications/leave_status_notif_to_rm",array(
				'pendingLeave' => $pendingLeave,
				'approvedLeave' => $approvedLeave,
				'rejectedLeave' => $rejectedLeave,
				'startDate' => $startDate,
				'endDate' => $endDate), TRUE
			);

			if (!empty($emailMessage)) {
				// Then send email
				$mail = $this->phpmailer_library->load();
				$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
				$mail->Subject = 'Reportee Leave(s) Status';
				$mail->isHTML(true);
				$mail->addAddress($manager);
				$mail->Body = $emailMessage;
				$mail->Send();
			}
		}
	}

    /**
     * Sending list memeber who did logging after given time to PO's
     */
    public function sendMemberDelayInLogAlert()
    {
        $memberDelayLogData = $this->getMemberDelayLogData();
        $projectOwnerWisedata = [];
        foreach ($memberDelayLogData as $key => $logRatio) {
            if (!empty($logRatio['owner'])) {
                $projectOwnerWisedata[$logRatio['owner']][] = $logRatio;
            } else {
                $projectOwnerWisedata['vivek.kumar1@byjus.com'][] = $logRatio;
            }
        }

        foreach ($projectOwnerWisedata as $poEmail => $logs) {
            $message = 'Hi Team,<br>';
            $message .= 'Below is the list of members who have been delayed in activity logging. <br>';
            $message .= '<table><tr><th>Project</th><th>Member Email</th><th>Weekly Log Ratio</th></tr>';
            foreach ($logs as $key => $log) {
                $message .= '<tr><td>'.$log['project_name'].'</td><td>'.$log['member'].'</td><td>'.$log['ratio'].'</td></tr>';
            }
            $message .= '</table>';
            $this->sendEmail(
                'DWA: Member Delay In Logs',
                $message,
                $poEmail
            );
        }
    }

    public function getMemberDelayLogData()
    {

        $thisWeekStart = date('Y-m-d', strtotime('monday this week'));
        $thisWeekEnd = date('Y-m-d', strtotime($thisWeekStart . '+ 4 day'));
        $query = "SELECT
        M.project_id,
        pm.project_name,
        um1.user_login_name as owner,
        M.user_id_fk,
        um2.user_login_name as member,
        SUM((CASE WHEN DATE(CONVERT_TZ(rm.ins_dt,'+00:00', '".$this->config->item('tx_cn')."')) <= DATE(rm.day_val) THEN 1 ELSE 0 END)) / COUNT(rm.response_id) AS ratio
        FROM
        (
        SELECT
        E.project_id_fk AS project_id,
        E.project_name,
        COUNT(DISTINCT E.user_id_fk) AS team_num,
        E.user_id_fk

        FROM
        (
        SELECT
        *,
        @num := IF(
        @type = tab1.user_id_fk,
        @num + 1,
        1
        ) AS row_number,
        @type := tab1.user_id_fk AS dummy
        FROM
        (
        SELECT
        A.project_id_fk,
        P1.project_name,
        R.user_id_fk,
        ROUND(
        SUM(
        IF(
        R.final_xp IS NULL,
        (
        IFNULL(R.equiv_xp, 0) + IFNULL(R.comp_xp, 0)
        ),
        (
        IFNULL(R.final_xp, 0) + IFNULL(R.comp_xp, 0)
        )
        )
        ),
        2
        ) AS equiv_xp
        FROM
        `response_mst` `R`
        INNER JOIN(
        SELECT
        @num := 0,
        @type := ''
        ) B
        ON 1 = 1
        INNER JOIN `activity_mst` `A` ON
        `R`.`activity_id_fk` = `A`.`activity_id` AND `A`.`status_nm` = 'Active'
        INNER JOIN `project_mst` `P1` ON
        `P1`.`project_id` = `A`.`project_id_fk`
        WHERE
        R.day_val BETWEEN $thisWeekStart AND $thisWeekEnd AND `R`.`status_nm` = 'Active'
        GROUP BY
        `R`.`user_id_fk`,
        A.project_id_fk
        ORDER BY
        `R`.`user_id_fk`,
        equiv_xp
        DESC
        ,
        P1.project_name
        ) tab1
        GROUP BY
        user_id_fk,
        project_id_fk,
        project_name,
        equiv_xp
        HAVING
        row_number = 1
        ) E
        GROUP BY
        E.project_id_fk,
        E.project_name
        ) M
        JOIN response_mst AS rm
        ON
        rm.user_id_fk = M.user_id_fk
        AND rm.day_val BETWEEN $thisWeekStart AND $thisWeekEnd
        AND rm.`status_nm` = 'Active'
        LEFT JOIN user_mst as um1 on um1.role_id_fk = '4' AND FIND_IN_SET(M.project_id, um1.project_id_fk)
        LEFT JOIN user_mst as um2 on um2.user_id = M.user_id_fk
        LEFT JOIN project_mst as pm on pm.project_id = M.project_id

        GROUP BY
        M.user_id_fk HAVING ratio <= 50";


    	$sqlQuery = $this->db->query($query);
    	return $sqlQuery->result_array();
    }

    /**
     * @desc: Send employee leave alert to PO's and RM before teo days
     */
    public function sendTwoDayBeforeLeaveAlert()
    {
        $aftertwodayLeavesData = $this->getTwoDayBeforeLeaveData();

        // Loop thrugh the found records
        // var_dump($this->user_model->getReportingManager('917')); exit;
        foreach ($aftertwodayLeavesData as $key => $leave) {
            $projectOwners = [];
            $reportingManager = [];
            // Find the projects owners
            $projectOwners = $this->user_model->getProjectOwner($leave['project_ids_fks']);
            if (!empty($projectOwners)) {
                $projectOwners = explode(',', $projectOwners[0]['owners']);
            }
            // Find the manager
            $reportingManager = $this->user_model->getReportingManager($leave['user_id_fk']);
            if (!empty($reportingManager)) {
                $reportingManager = explode(',', $reportingManager[0]['manager']);
            }

            $message = $this->getTwoDaysBeforeLeaveMessage($leave);

            // Send them email with specfic given template
            if (!empty($reportingManager)) {
                $this->sendEmail(
                    'DWA: Two days before leave notification',
                    $message,
                    $reportingManager,
                    $projectOwners
                );
            }
        }
    }

    public function getTwoDaysBeforeLeaveMessage($leave)
    {
        $message = '';
        $message = 'Hi Team, <br>' . $leave['full_name'] . ' is going on leave from date ' . date("F jS, Y", strtotime($leave['start_date'])) . '.<br>';
        $message .= 'Following is the status of his/her Leave request.';
        $message .= '<table><tr><th>Name</th><th>Start Date</th><th>End Date</th><th>Reason</th><th>Status</th></tr>';
        $message .= '<tr><td>'.$leave['full_name'].'</td><td>'.date("F jS, Y", strtotime($leave['start_date'])).'</td><td>'.date("F jS, Y", strtotime($leave['end_date'])).'</td><td>'.$leave['reason'].'</td><td>'.$leave['conf_status'].'</td></tr></table>';
        return $message;
    }

    public function getTwoDayBeforeLeaveData()
    {
        // Find date after two days from Now
        $dateAfterTwoDays = date('Y-m-d', strtotime(' + 2 day'));
        $this->db->select('um.full_name, ct.*');
        $this->db->where('start_date', $dateAfterTwoDays);
        $this->db->join('user_mst as um', 'um.user_id = ct.user_id_fk');
        // Get all the leave records which matches to above date
        return $aftertwodayLeavesData = $this->db->get('comp_track as ct')->result_array();
    }


    /**
     * @desc This runs on 24th to send alert to employess that their work request have been approved
     */
    public function sendApprovedWorkRequestAlert()
    {

        $approvedWorkRequestData = $this->getApprovedWorkRequestAlertData();

        // Loop through all the records
        foreach ($approvedWorkRequestData as $key => $workRequest) {
            if (!empty($workRequest['employee_email']) && !empty($workRequest['requester_email'])) {
                // Get mesage template
                $message = 'Hi ' . $workRequest['employee_name'] . ',<br>';
                $message .= 'Your work request has been approved by ' . $workRequest['requester_name'] . ' ';
                $message .= 'Below is the status of your work request.<br>';
                $message .= '<table><tr><th>Employee</th><th>Requester</th><th>Comp Type</th><th>Reason</th><th>Called On</th><th>Status</th></tr></table>';
                $message .= '<tr><td>'.$workRequest['employee_name'].'</td><td>'.$workRequest['requester_name'].'</td><td>'.$workRequest['com_type'].'</td><td>'.$workRequest['reason'].'</td><td>'.date("F jS, Y", strtotime($workRequest['called_dt'])).'</td><td>'.$workRequest['conf_status'].'</td></tr>';
                return $message;

                // Send email
                $this->sendEmail(
                    'DWA: Work Request Approval',
                    $message,
                    $workRequest['employee_email'],
                    $workRequest['requester_email']
                );
            }

        }
    }

    public function getApprovedWorkRequestAlertData()
    {
        // get the todays date
        $todaysDate = date('Y-m-d');
        // get date of 24 days before
        $twentyFourDaysBack = date('Y-m-d', strtotime("first day of previous month"));
        $twentyFourDaysBack = date('Y-m-d', strtotime($twentyFourDaysBack . '+ 24 day'));

        // get all the approved work request within above date range
        $this->db->select('employee.full_name as employee_name, employee.user_login_name as employee_email, requester.full_name as requester_name, requester.user_login_name as requester_email, wrct.com_type, wr.*');
        $this->db->where('wr.ins_dt >=', $twentyFourDaysBack);
        $this->db->where('wr.ins_dt <=', $todaysDate);
        $this->db->where('wr.conf_status', 'Approved');
        $this->db->join('user_mst as employee', 'wr.emp_user_id = employee.user_id', 'left');
        $this->db->join('user_mst as requester', 'wr.req_user_id = requester.user_id', 'left');
        $this->db->join('work_request_comb_type as wrct', 'wrct.id = wr.combo_type', 'left');
        return $this->db->get('work_request as wr')->result_array();
    }

    /**
     * @desc This runs on every friday and send emails to RM's the list of next weeks approved and pending leaves
     */
    public function sendWeelyLeaveAlert()
    {
        $dataAccordingToRm = [];
        $weeklyLeaveAlertdata = $this->getWeeklyLeaveAlertData();

        foreach ($weeklyLeaveAlertdata as $key => $leave) {
            $dataAccordingToRm[$leave['manager_email']][] = $leave;
        }


        foreach ($dataAccordingToRm as $rmEmail => $leaves) {
            $message = 'Hi Team, <br> Below is the list of pending and approved leaves of next week.<br>';
            $message .= '<table><tr><th>Name</th><th>Start Date</th><th>End Date</th><th>Reason</th><th>Status</th></tr>';
            foreach ($leaves as $key => $leave) {
                $message .= '<tr><td>'.$leave['full_name'].'</td><td>'.date("F jS, Y", strtotime($leave['start_date'])).'</td><td>'.date("F jS, Y", strtotime($leave['end_date'])).'</td><td>'.$leave['reason'].'</td><td>'.$leave['conf_status'].'</td></tr>';
            }
            $message .= '</table>';

            $this->sendEmail(
                'DWA: Next Week Leave Status',
                $message,
                $rmEmail
            );

        }
    }

    public function getWeeklyLeaveAlertData()
    {

        $nextWeekStart = date('Y-m-d', strtotime('next monday'));
        $nextWeekEnd = date('Y-m-d', strtotime($nextWeekStart . '+ 6 day'));

        $this->db->select('um.full_name, um.user_login_name, rm.full_name as manager_name, rm.user_login_name as manager_email, ct.*');
        $this->db->join('user_mst as um', 'um.user_id = ct.user_id_fk', 'left');
        $this->db->join('user_mst as rm', 'rm.user_id = um.manager_id', 'left');
        $this->db->where('start_date >=', $nextWeekStart);
        $this->db->where('start_date <=', $nextWeekEnd);
        $this->db->where_in('conf_status', ['Pending', 'Approved']);
        // Get all the leave records which matches to above date
        return $this->db->get('comp_track as ct')->result_array();
        // var_dump($this->db->last_query()); exit;
    }

    /**
     * @desc This runs every friday and send details to Member, PO and POC about thier pending and audited logs
     */
    public function weeklyLogAlert()
    {
        $weeklyLogAlertData = $this->getWeeklyLogAlertData();
        $dataAccordingtoRole = [];
        foreach ($weeklyLogAlertData as $key => $logs) {
            $dataAccordingtoRole[$logs['member_email']][] = $logs;
            $dataAccordingtoRole[$logs['poc_email']][] = $logs;
            $dataAccordingtoRole[$logs['po_email']][] = $logs;
        }

        foreach ($dataAccordingtoRole as $peopleEmail => $logs) {
            $message = $this->getWeeklyLogAlertMessage($logs);

            $this->sendEmail(
                'DWA: Next Week Leave Status',
                $message,
                $peopleEmail
            );

        }

    }

    public function getWeeklyLogAlertMessage($logs)
    {
        $message = 'Hi Team, <br> Below is the list of all logs this week.<br>';
        $message .= '<table><tr><th>Project Name</th><th>Activity Name</th><th>Log Date</th><th>Description</th><th>Std Target</th><th>Final Xp</th></tr>';
        foreach ($logs as $key => $log) {
            $message .= '<tr><td>'.$log['project_name'].'</td><td>'.$log['activity_name'].'</td><td>'.date("F jS, Y", strtotime($log['day_val'])).'</td><td>'.$log['task_desc'].'</td><td>'.$log['std_tgt_val'].'</td><td>'.$log['final_xp'].'</td></tr>';
        }
        $message .= '</table>';
        return $message;
    }

    public function getWeeklyLogAlertData()
    {

            $thisWeekStart = date('Y-m-d', strtotime('monday this week'));
            $thisWeekEnd = date('Y-m-d', strtotime($thisWeekStart . '+ 4 day'));

            // Here fetch the log of within above date range with respective member, po and poc
            $this->db->select('pm.project_name, am.activity_name, member.full_name as member_name, member.user_login_name as member_email, poc.full_name as poc_name, poc.user_login_name as poc_email, po.full_name as po_name, po.user_login_name as po_email, rm.*');
            $this->db->where('rm.ins_dt >=', $thisWeekStart);
            $this->db->where('rm.ins_dt <=', $thisWeekEnd);
            $this->db->join('activity_mst as am', 'am.activity_id = rm.activity_id_fk', 'left');
            $this->db->join('project_mst as pm', 'pm.project_id = am.project_id_fk', 'left');
            $this->db->join('user_mst as member', 'member.user_id = rm.user_id_fk');
            $this->db->join('user_mst as poc', 'FIND_IN_SET(am.project_id_fk, poc.project_id_fk) AND poc.role_id_fk = "5"', 'left');
            $this->db->join('user_mst as po', 'FIND_IN_SET(am.project_id_fk, po.project_id_fk) AND po.role_id_fk = "4"', 'left');
            $this->db->group_by('rm.response_id');
            return $this->db->get('response_mst as rm')->result_array();
    }

    public function weeklyLogDelayAlert()
    {
        $thisWeekStart = date('Y-m-d', strtotime('monday this week'));
        $thisWeekEnd = date('Y-m-d', strtotime($thisWeekStart . '+ 4 day'));
        // var_dump($thisWeekStart, $thisWeekEnd); exit;
        $this->db->select('rm.*');
        $this->db->where('rm.ins_dt >=', $thisWeekStart);
        $this->db->where('rm.ins_dt <=', $thisWeekEnd);
        // $this->db->join('activity_mst as am', 'am.activity_id = rm.activity_id_fk', 'left');
        // $this->db->join('project_mst as pm', 'pm.project_id = am.project_id_fk', 'left');
        // $this->db->join('user_mst as member', 'member.user_id = rm.user_id_fk');
        // $this->db->join('user_mst as poc', 'FIND_IN_SET(am.project_id_fk, poc.project_id_fk) AND poc.role_id_fk = "5"', 'left');
        // $this->db->join('user_mst as po', 'FIND_IN_SET(am.project_id_fk, po.project_id_fk) AND po.role_id_fk = "4"', 'left');
        // $this->db->group_by('rm.response_id');
        var_dump($this->db->get('response_mst as rm')->result_array()); exit;
    }

    public function sendEmail($subject, $message, $addresses, $cc = 0, $bcc = array())
    {
        $bcc[] = 'vivek.kumar1@byjus.com';
        $mail = $this->phpmailer_library->load();
        $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');

        foreach($addresses AS $address) {
            $mail->addAddress($address);
        }

        if ($cc != "0") {
            foreach($cc AS $address) {
                $mail->addCC($address);
            }
        }

        foreach($bcc AS $address) {
            $mail->addBCC($address);
        }

        $mail->isHTML(true);
        $mail->Body = $message;
        if(!$mail->Send()) {
            $sendError = $mail->ErrorInfo;
        } else{
            echo 'Message has been sent';
        }
    }


}
//}
?>
