<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//if (!isset($_SERVER['REMOTE_ADDR']))
{
class Alert extends CI_Controller
{    
    public function __construct()
    {       
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('alert_model');
        $this->load->library('session');
        $this->load->library("PHPMailer_Library");
		define('ROLE_ID', '2');
        define('CONTENT', '1');
        define('MEDIA', '2');
    }
    
	 public function notifyWeeklyImpactLoss()
    {
        // Get impact loss data
        $contentImpactLossData = $this->alert_model->getWeeklyImpactLossData(CONTENT);
        $mediaImpactLossData = $this->alert_model->getWeeklyImpactLossData(MEDIA);

        // Send email
        $this->sendWeeklyImpactLossEmail($contentImpactLossData, CONTENT);
        $this->sendWeeklyImpactLossEmail($mediaImpactLossData, MEDIA);
    }

    public function notifyHighestXpDaily()
    {
        $contentHighestXpDaily = $this->alert_model->getHighestXpDaily(CONTENT);
        $mediaHighestXpDaily = $this->alert_model->getHighestXpDaily(MEDIA);
        $this->sendHighestXpDailyEmail($contentHighestXpDaily, CONTENT);
        $this->sendHighestXpDailyEmail($mediaHighestXpDaily, MEDIA);

    }

    public function notifyWeeklyGradingPending()
    {
        // Get impact loss data
        $contentGradingPending = $this->alert_model->getWeeklyGradingPending(CONTENT);
        $mediaGradingPending = $this->alert_model->getWeeklyGradingPending(MEDIA);
        // Send email
        $this->sendGradingPendingEmail($contentGradingPending, CONTENT);
        $this->sendGradingPendingEmail($mediaGradingPending, MEDIA);
    }

    public function notifyWeeklyStdTargetChanges()
    {
        $contentWeeklyStdTargetChanges = $this->alert_model->getWeeklyStdTargetChanges();
        $this->sendWeeklyStdTargetChangesEmail($contentWeeklyStdTargetChanges);
    }

    public function sendWeeklyStdTargetChangesEmail($data)
    {
        if (!empty($data)) {
            $weeklyStdTargetChangesTemplate = $this->getWeeklyStdTargetChangesTemplate($data);
            $seniorManager = $this->alert_model->getSeniorManager(0, ROLE_ID);
            $this->sendEmail(
                'DWA: Weekly Std Target Changes',
                $weeklyStdTargetChangesTemplate,
                $seniorManager
            );
        }
    }

    public function sendGradingPendingEmail($data, $department)
    {
        if (!empty($data)) {
            $gradingPendingTemplate = $this->getGradingPendingTemplate($data);
            $seniorManager = $this->alert_model->getSeniorManager(0, ROLE_ID, $department);

            $this->sendEmail(
                'DWA: Weekly Grading Pending',
                $gradingPendingTemplate,
                $seniorManager
            );
        }
    }

    public function sendHighestXpDailyEmail($data, $department)
    {
        if (!empty($data)) {
            $highestXpDailyTemplate = $this->getHighestXpDailyTemplate($data);
            $seniorManager = $this->alert_model->getSeniorManager(0, ROLE_ID, $department);

            $this->sendEmail(
                'DWA: Daily Higest XP(s)',
                $highestXpDailyTemplate,
                $seniorManager
            );
        }
    }

    public function sendWeeklyImpactLossEmail($data, $department)
    {
        if (!empty($data)) {
            // Build html email template
            $weeklyImpactLossTemplate = $this->getWeeklyImpactLossTemplate($data);

            // Get senior managers
            $seniorManager = $this->alert_model->getSeniorManager(0, ROLE_ID, $department);

            $this->sendEmail(
                'DWA: Weekly Impact Loss',
                $weeklyImpactLossTemplate,
                $seniorManager
            );
        }
    }

    public function sendEmail($subject, $message, $addresses, $cc = 0, $bcc = array())
    {
        $bcc[] = 'vivek.kumar1@byjus.com';
        $mail = $this->phpmailer_library->load();
        $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');

        foreach($addresses AS $address) {
            $mail->addAddress($address);
        }

        if ($cc != "0") {
            foreach($cc AS $address) {
                $mail->addCC($address);
            }
        }

        foreach($bcc AS $address) {
            $mail->addBCC($address);
        }

        $mail->isHTML(true);
        $mail->Body = $message;
        if(!$mail->Send()) {
            $sendError = $mail->ErrorInfo;
        } else{
            echo 'Message has been sent';
        }
    }

    public function getWeeklyStdTargetChangesTemplate($data)
    {
        $template = 'Hi, Team <br> Following is the information regarding weekly standard target changes <br><br><br>';
        $template .= '<table><tr><th>Department</th><th>Activity Name</th><th>Work unit</th><th>Previous Changes Description</th><th>Target Value</th><th>Start Date</th><th>End Date</th></tr>';
        foreach ($data as $key => $value) {
            $template .= '<tr><td>'.$value['dept'].'</td><td>'.$value['activity_name'].'</td><td>'.$value['wu_name'].'</td><td>'.$value['prev_change_desc'].'</td><td>'.$value['tgt_val'].'</td><td>'.$value['start_date'].'</td><td>'.$value['end_date'].'</td></tr>';
        }
        $template .= '</table>';
        return $template;
    }

    public function getWeeklyImpactLossTemplate($data)
    {
        $template = 'Hi, Team <br> Following is the information regarding weekly impact loss on XP\'s <br><br><br>';
        $template .= '<table><tr><th>Project Name</th><th>Lost XP</th></tr>';
        foreach ($data as $key => $value) {
            $template .= '<tr><td>'.$value['project_name'].'</td><td>'.$value['proj_xp'].'</td></tr>';
        }
        $template .= '</table>';
        return $template;
    }

    public function getHighestXpDailyTemplate($data)
    {
        $template = 'Hi, Team <br> Following is the information regarding daily highest XP\'s<br><br><br>';
        $template .= '<table><tr><th>User Name</th><th>Today\'s XP</th></tr>';
        foreach ($data as $key => $value) {
            $template .= '<tr><td>'.$value['full_name'].'</td><td>'.$value['proj_xp'].'</td></tr>';
        }
        $template .= '</table>';
        return $template;
    }

    public function getGradingPendingTemplate($data)
    {
        $template = 'Hi, Team <br> Following is the information regarding weekly grading pending<br><br><br>';
        $template .= '<table><tr><th>Project Name</th></tr>';
        foreach ($data as $key => $value) {
            $template .= '<tr><td>'.$value['project_name'].'</td></tr>';
        }
        $template .= '</table>';
        return $template;
    }

	// public function leave_mail()
    // {
			// $ans = $this->input->post("C");
			// $param = $this->input->post("param");
			
			// $po_id = $this->input->post("po");		
			
			// if($ans)
			// {
			// $mail_user=array();	
			// if($param=='W')
			// {
			// $c2=$this->alert_model->l_mail_week($ans);
			// $week_di=array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
			// }else{			
			// $c2=$this->alert_model->l_mail($ans);
			// }
			// //print_r($c2);
			// if($c2)
			// {
			// foreach($c2 AS $row1)
			// {
					// $mail = $this->phpmailer_library->load();
					// $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
					// $mail->addAddress($row1['man_mail']);
					// if($po_id )
					// {
						// $pid=implode(",",$po_id);						
					// $c3=$this->alert_model->proj_own_list($pid,'4');
					// print_r($c3);
						// foreach($c3 AS $ma)
						// {
						// $mail->addAddress($ma['email']);
						// }
					// }
					// $mail->addCC($row1['us_mail']);
					// $mail->isHTML(true);        
					// $mail->Subject = 'DWA: Leave Tracker Update '.$row1['full_name'].' !!';
					// $ct=explode("|",$row1['date_name']);					
					// $message= 'Hi '.$row1['man_name'].',<br>Please approve the below pending leave:<br>';
					// if($param=="W")
					// {
					// $message=$message."<table border='1'><thead><th>Employee</th><th>Leave Type</th><th>".$ct[0]."</th><th>".$ct[1]."</th><th>Day of Week</th><th>Status</th></thead>";
					// $message=$message.'<tr><td>'.$row1['full_name'].'</td>'.'<td>'.$row1['leave_type_name'].'</td>'.'<td>'.$row1['st_dt'].'</td>'.'<td>'.$row1['en_dt'].'</td>'.'<td>'.$week_di[$row1['num_days']].'</td>'.'<td>'.$row1['conf_status'].'</td>'.'</tr>';
					// $message=$message."</table>";
					// }else{
						// $message=$message."<table border='1'><thead><th>Employee</th><th>Leave Type</th><th>".$ct[0]."</th><th>".$ct[1]."</th><th>No of Days</th><th>Reason</th><th>Desc</th><th>Status</th></thead>";
					// $message=$message.'<tr><td>'.$row1['full_name'].'</td>'.'<td>'.$row1['leave_type_name'].'</td>'.'<td>'.$row1['st_dt'].'</td>'.'<td>'.$row1['en_dt'].'</td>'.'<td>'.$row1['num_days'].'</td>'.'<td>'.$row1['reason'].'</td>'.'<td>'.$row1['h_day'].'</td>'.'<td>'.$row1['conf_status'].'</td>'.'</tr>';
					// $message=$message."</table>";
					// }			
				// $mail->Body    = $message;
				// //print_r($message);
				// if(!$mail->Send())
				// {
					// $sendError = $mail->ErrorInfo;
				// }
				// else{
					// return "success";
					// }
				// }
			// }
		// }
	// }
	
	// public function leave_mail_status()
    // {
			// $ans = $this->input->post("C");
			// $param = $this->input->post("param");
			// if($ans)
			// {
			// $mail_user=array();	
			// if($param=='W')
			// {
			// $c2=$this->alert_model->l_mail_week($ans);
			// $week_di=array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
			// }else{
			// $c2=$this->alert_model->l_mail($ans);	
			// }
			
			// if($c2)
			// {
			// foreach($c2 AS $row1)
			// {
					// $mail = $this->phpmailer_library->load();
					// $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
					// $mail->addCC($row1['man_mail']);								
					// $mail->addAddress($row1['us_mail']);
					// $mail->isHTML(true);        
					// $mail->Subject = 'DWA: Leave Tracker Update '.$row1['full_name'].' !!';
					// $ct=explode("|",$row1['date_name']);
					// $message= 'Hi '.$row1['full_name'].',<br>Please find below the leave approval status:<br>';
					// if($param=="W")
					// {
					// $message=$message."<table border='1'><thead><th>Employee</th><th>Leave Type</th><th>".$ct[0]."</th><th>".$ct[1]."</th><th>Day of Week</th><th>Status</th></thead>";
					// $message=$message.'<tr><td>'.$row1['full_name'].'</td>'.'<td>'.$row1['leave_type_name'].'</td>'.'<td>'.$row1['st_dt'].'</td>'.'<td>'.$row1['en_dt'].'</td>'.'<td>'.$week_di[$row1['num_days']].'</td>'.'<td>'.$row1['conf_status'].'</td>'.'</tr>';
					// $message=$message."</table>";
					// }else{
						// $message=$message."<table border='1'><thead><th>Employee</th><th>Leave Type</th><th>".$ct[0]."</th><th>".$ct[1]."</th><th>No of Days</th><th>Reason</th><th>Desc</th><th>Status</th></thead>";
					// $message=$message.'<tr><td>'.$row1['full_name'].'</td>'.'<td>'.$row1['leave_type_name'].'</td>'.'<td>'.$row1['st_dt'].'</td>'.'<td>'.$row1['en_dt'].'</td>'.'<td>'.$row1['num_days'].'</td>'.'<td>'.$row1['reason'].'</td>'.'<td>'.$row1['h_day'].'</td>'.'<td>'.$row1['conf_status'].'</td>'.'</tr>';
					// $message=$message."</table>";
					// }
								
				// $mail->Body    = $message;
				// //print_r($message);
					// if(!$mail->Send())
					// {
						// $sendError = $mail->ErrorInfo;
					// }
					// else{
					// return "success";
					// }
				// }
			// }
		// }
	// }
	
	public function alert_1()
    {
			$u_ids='1,2,71,737,220,221,222,224,225,226,227,228,381,1080';
			$day1= date('Y-m-d');
			$limit=500;
			$day = date('Y-m-d', strtotime($day1.' -1 days'));
			$day_fm= date('d M Y',strtotime($day));
			$c12=$this->alert_model->update_user();
			$c1=$this->alert_model->check_hol($day);
				foreach($c1 AS $ch)
				{
					if($ch['cnt']==0)
					{
						$mail_user=array();						 
						$mail_user_id=array();
						 $last_log=array();
						 $c3=$this->alert_model->last_logged($u_ids);
							foreach($c3 AS $ro2)
							{
								$last_log[$ro2['user_id_fk']]=date('d M Y',strtotime($ro2['day_val']));
							}							
						$c2=$this->alert_model->alert_1($day,$u_ids,$limit);
						if($c2)
						{
						foreach($c2 AS $row1)
						{
								$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								$mail->addAddress($row1['email']);								
								////$mail->addBCC('noreply@byjusdwa.com');
							   //Content
								$mail->isHTML(true);        
								$mail->Subject = 'DWA: Work Log Not Filled for '.$day_fm.' !!';
								if(array_key_exists($row1['user_id'],$last_log))
								{
								$message= 'Hi '.$row1['full_name'].',<br>Seems that you last logged into DWA on '.$last_log[$row1['user_id']].'<br>Please log all your activities for the default period by e.o.d<br>';
								}else{
								$message = 'Hi '.$row1['full_name'].',<br>Seems that you have not filled DWA for some time.<br>Please log all your activities for the default period by e.o.d <br>';	
								}
								
								//print_r($message);
								$mail->Body    = $message;
							if(!$mail->Send())
							{
								$sendError = $mail->ErrorInfo;
							}else{
								$mail_user[]=$row1['email'];
								$mail_user_id[]=$row1['user_id'];
							  // echo 'Message has been sent'; 
							}
						}
						}
						if($mail_user)
						{
						$c12=$this->alert_model->update_user_flag($mail_user_id);
						}
					}			
			}    
	}
	
	public function alert_1_off()
    {
			$u_ids='1,2,71,737,220,221,222,224,225,226,227,228,381,1080';
			$day1= date('Y-m-d');
			$limit=500;
			$day = date('Y-m-d', strtotime($day1.' -1 days'));
			$day_fm= date('d M Y',strtotime($day));
			$c1=$this->alert_model->check_hol($day);
				foreach($c1 AS $ch)
				{
					if($ch['cnt']==0)
					{
						$mail_user=array(); 
						$mail_user_id=array();
						 $last_log=array();
						 $c3=$this->alert_model->last_logged($u_ids);
							foreach($c3 AS $ro2)
							{
								$last_log[$ro2['user_id_fk']]=date('d M Y',strtotime($ro2['day_val']));
							}							
						$c2=$this->alert_model->alert_1($day,$u_ids,$limit);
						if($c2)
						{
						foreach($c2 AS $row1)
						{
								$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								$mail->addAddress($row1['email']);								
								////$mail->addBCC('noreply@byjusdwa.com');
							   //Content
								$mail->isHTML(true);        
								$mail->Subject = 'DWA: Work Log Not Filled for '.$day_fm.' !!';
								if(array_key_exists($row1['user_id'],$last_log))
								{
								$message= 'Hi '.$row1['full_name'].',<br>Seems that you last logged into DWA on '.$last_log[$row1['user_id']].'<br>Please log all your activities for the default period by e.o.d<br>';
								}else{
								$message = 'Hi '.$row1['full_name'].',<br>Seems that you have not filled DWA for some time.<br>Please log all your activities for the default period by e.o.d <br>';	
								}
								//print_r($message);
								//$mail_user[]=$row1['email'];
								$mail->Body    = $message;
							if(!$mail->Send())
							{
								$sendError = $mail->ErrorInfo;
							}else{
								$mail_user[]=$row1['email'];
								$mail_user_id[]=$row1['user_id'];
							   //echo $row1['email']; 
							}
						}
						}
						if($mail_user)
						{
						$c12=$this->alert_model->update_user_flag($mail_user_id);
						}
					}			
			}    
	}
	
	
	public function update_streak()
    {
		//$this->output->enable_profiler(TRUE);
			$day1= date('Y-m-d');
			$day = date('Y-m-d', strtotime($day1.' -1 days'));
			$c1=$this->alert_model->update_streak_mm($day);			
	}
	
	 public function alert_2()
    {
			$u_ids='1,2,71,737,220,221,222,224,225,226,227,228,381,1080';
			$day1= date('Y-m-d');
			$day = date('Y-m-d', strtotime($day1.' -1 days'));
			$day_fm= date('d M Y',strtotime($day));
			$c1=$this->alert_model->check_hol($day);
				foreach($c1 AS $ch)
				{
					if($ch['cnt']==0)
					{
						$c2=$this->alert_model->alert_2($day,$u_ids);
						$lst_def=array();
						$poc=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
								$lst_def[$row1['project_name']][]=$row1['full_name'];
							}
							$c3=$this->alert_model->poc_list($u_ids,'5');
							if($c3)
							{
								foreach($c3 AS $row1)
								{
									if(array_key_exists($row1['project_name'],$lst_def))
									{
									$poc[$row1['project_name']][]=$row1['email'];
									}
								}
							}
							if($poc)
							{
								foreach($poc AS $key=>$value)
								{
									$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								foreach($value AS $ma)
								{
								$mail->addAddress($ma);
								}								
								//$mail->addBCC('noreply@byjusdwa.com');
							   //Content
								$mail->isHTML(true);        
								$mail->Subject = 'DWA: '.$key.'- Logs not filled for yesterday '.$day_fm.' !!';								
								$message= 'Hi '.$key.' Poc, <br> Logs of following employees were not found for '.$day_fm.' in DWA:<br>';	
								foreach($lst_def[$key] AS $def_l)
								{
									$message=$message.$def_l.'<br>';
								}
								//print_r($message);
								$mail->Body    = $message;
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								  // echo 'Message has been sent'; 
								}
									
								}
							}
						}
					}					
			}    
	}
	
	public function alert_3()
    {
			//$u_ids='1,2,71,737';
			$u_ids='1,2,71,737,220,221,222,224,225,226,227,228,381,1080';
			$day1= date('Y-m-d');
			$day_jfk= date('d M Y');
			$day = date('Y-m-d', strtotime($day1.' -1 days'));
			$day_fm= date('d M Y',strtotime($day .' -2 days'));
			
						$c2=$this->alert_model->alert_3($day,$u_ids);
						$lst_def=array();
						$lst_sm=array();
						$sm=array();
						$po=array();
						$lst_rep=array();
						$man_rep=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
								if($row1['project_name'])
								{
								$lst_def[$row1['project_name']][]=$row1['full_name'];
								}
								// if($row1['man_mail'])
								// {
									// $lst_rep[$row1['man_mail']][]=$row1['full_name'];
									// $man_rep[$row1['man_mail']]=$row1['man_name'];
								// }
								//$lst_sm[]=$row1['full_name'];
							}
							$c3=$this->alert_model->poc_list($u_ids,'4');
							if($c3)
							{
								foreach($c3 AS $row1)
								{
									if(array_key_exists($row1['project_name'],$lst_def))
									{
									$po[$row1['project_name']][]=$row1['email'];
									}
								}
							}
							
							if($po)
							{
								foreach($po AS $key=>$value)
								{
									$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								foreach($value AS $ma)
								{
								$mail->addAddress($ma);
								}								
								//$mail->addBCC('noreply@byjusdwa.com');
								$mail->isHTML(true);        
								$mail->Subject = 'DWA: '.$key.'- Logs not filled for past 3 days - '.$day_fm.'!!';								
								$message= 'Hi '.$key.' Project Owner, <br> Logs of following employees were not found for around 3 days in DWA: (from '.$day_fm.') <br>';	
								foreach($lst_def[$key] AS $def_l)
								{
									$message=$message.$def_l.'<br>';
								}
								//print_r($message);
								$mail->Body    = $message;
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}
									
								}
							}							
				}    
	}

	
	public function alert_3_rm()
    {
			$u_ids='1,2,71,737,220,221,222,224,225,226,227,228,381,1080';
			$day1= date('Y-m-d');
			$day_jfk= date('d M Y');
			$day = date('Y-m-d', strtotime($day1.' -1 days'));
			$day_fm= date('d M Y',strtotime($day .' -2 days'));
			
						$c2=$this->alert_model->alert_3_rm($day,$u_ids);
						$lst_def=array();
						$lst_sm=array();
						$sm=array();
						$po=array();
						$lst_rep=array();
						$man_rep=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
								if($row1['man_mail'])
								{
									$last_log='';
								$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');								
								$mail->addAddress($row1['email']);																
								$mail->addCC($row1['man_mail']);
							   //Content
								$mail->isHTML(true);        
								$mail->Subject = 'DWA:- Logs not filled for past 3 days - '.$day_fm.' !!';	
								$last_log=date('d M Y',strtotime($row1['m_val']));								
								if($row1['m_val'])
								{
								$message= 'Hi '.$row1['full_name'].',<br>Seems that you last logged into DWA on '.$last_log.'<br>Please log all your activities for the default period by e.o.d<br>';
								}else{
								$message = 'Hi '.$row1['full_name'].',<br>Seems that you have not filled DWA for some time.<br>Please log all your activities for the default period by e.o.d <br>';	
								}
								//print_r($message);
								$mail->Body    = $message;
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}									
								}
							}
						}
			}
	
	public function alert_3_sm()
    {
			$u_ids='1,2,71,737,220,221,222,224,225,226,227,228,381,1080';
			$day1= date('Y-m-d');
			$day_jfk= date('d M Y');
			$day = date('Y-m-d', strtotime($day1.' -1 days'));
			$day_fm= date('d M Y',strtotime($day .' -2 days'));
			
						$c2=$this->alert_model->alert_3_sm($day,$u_ids);
						$lst_def=array();
						$lst_sm=array();$lst_sm1=array();
						$sm=array();
						$po=array();
						$lst_rep=array();
						$man_rep=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
								$lst_sm1[$row1['dept_id']][]=$row1['full_name'];
							}
							$c4=$this->alert_model->sm_list($u_ids,'2');
							if($c4)
							{
								foreach($c4 AS $row1)
								{
									$sm[$row1['full_name']]=$row1['email'];		
									$lst_sm[$row1['email']]=array();
									foreach($lst_sm1 AS $key=>$row2)
									{								
										if($row1['dept_id']==0)
										{
											$lst_sm[$row1['email']] = array_merge($lst_sm[$row1['email']],$row2);
										}
										else 
										{
											$key1=explode(",",$key);
												if(in_array($row1['dept_id'],$key1))
												{	
												$lst_sm[$row1['email']] = array_merge($lst_sm[$row1['email']],$row2);
												}										
										}
									}
								}
							}
							//print_r($lst_sm);
							if($sm)
							{
								foreach($sm AS $key=>$value)
								{
									$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								$mail->addAddress($value);							
								//$mail->addBCC('noreply@byjusdwa.com');
							   //Content
								$mail->isHTML(true);        
								if($lst_sm[$value])
								{
								$mail->Subject = 'DWA:- Logs not filled for past 3 days - '.$day_fm.' !!';								
								$message= 'Hi '.$key.', <br> Logs of following employees were not found for around 3 days in DWA: (from '.$day_fm.') <br>';	
								foreach($lst_sm[$value] AS $def_l)
								{
									$message=$message.$def_l.'<br>';
								}
								$mail->Body    = $message;
								//print_r($message);
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}
								}
								}
							}							
						
			}    
	}
	// public function alert_3_sm()
    // {
			// $u_ids='1,2,71,737';
			// $day1= date('Y-m-d');
			// $day_jfk= date('d M Y');
			// $day = date('Y-m-d', strtotime($day1.' -1 days'));
			// $day_fm= date('d M Y',strtotime($day .' -2 days'));
			
						// $c2=$this->alert_model->alert_3_sm($day,$u_ids);
						// $lst_def=array();
						// $lst_sm=array();
						// $sm=array();
						// $po=array();
						// $lst_rep=array();
						// $man_rep=array();
						// if($c2)
						// {
							// foreach($c2 AS $row1)
							// {
								// if($row1['project_name'])
								// {
								// $lst_def[$row1['project_name']][]=$row1['full_name'];
								// }
								// if($row1['man_mail'])
								// {
									// $lst_rep[$row1['man_mail']][]=$row1['full_name'];
									// $man_rep[$row1['man_mail']]=$row1['man_name'];
								// }
								// $lst_sm[]=$row1['full_name'];
							// }
							// $c3=$this->alert_model->poc_list($u_ids,'4');
							// if($c3)
							// {
								// foreach($c3 AS $row1)
								// {
									// if(array_key_exists($row1['project_name'],$lst_def))
									// {
									// $po[$row1['project_name']][]=$row1['email'];
									// }
								// }
							// }
							// $c4=$this->alert_model->sm_list($u_ids,'2');
							// if($c4)
							// {
								// foreach($c4 AS $row1)
								// {
									// $sm[$row1['full_name']]=$row1['email'];
								// }
							// }
							// if($po)
							// {
								// foreach($po AS $key=>$value)
								// {
									// $mail = $this->phpmailer_library->load();
								// $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								// foreach($value AS $ma)
								// {
								// $mail->addAddress($ma);
								// }								
								// //$mail->addBCC('noreply@byjusdwa.com');
							   // //Content
								// $mail->isHTML(true);        
								// $mail->Subject = 'DWA: '.$key.'- Logs not filled for past 3 days - '.$day_fm.'!!';								
								// $message= 'Hi '.$key.' Project Owner, <br> Logs of following employees were not found for around 3 days in DWA: (from '.$day_fm.') <br>';	
								// foreach($lst_def[$key] AS $def_l)
								// {
									// $message=$message.$def_l.'<br>';
								// }
								// //print_r($message);
								// $mail->Body    = $message;
								// if(!$mail->Send())
								// {
									// $sendError = $mail->ErrorInfo;
								// }else{
								   // echo 'Message has been sent'; 
								// }
									
								// }
							// }
							// if($man_rep)
							// {
								// foreach($man_rep AS $key=>$value)
								// {
								// $mail = $this->phpmailer_library->load();
								// $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');								
								// $mail->addAddress($key);																
								// //$mail->addBCC('noreply@byjusdwa.com');
								
							   // //Content
								// $mail->isHTML(true);        
								// $mail->Subject = 'DWA:- Logs not filled for past 3 days - '.$day_fm.' !!';								
								// $message= 'Hi '.$value.', <br> Logs of following employees were not found for around 3 days in DWA: (from '.$day_fm.') <br>';	
								// foreach($lst_rep[$key] AS $def_l)
								// {
									// $message=$message.$def_l.'<br>';
								// }
								// //print_r($message);
								// $mail->Body    = $message;
								// if(!$mail->Send())
								// {
									// $sendError = $mail->ErrorInfo;
								// }else{
								   // echo 'Message has been sent'; 
								// }									
								// }
							// }
							// if($sm)
							// {
								// foreach($sm AS $key=>$value)
								// {
									// $mail = $this->phpmailer_library->load();
								// $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								// $mail->addAddress($value);							
								// //$mail->addBCC('noreply@byjusdwa.com');
							   // //Content
								// $mail->isHTML(true);        
								// $mail->Subject = 'DWA:- Logs not filled for past 3 days - '.$day_fm.' !!';								
								// $message= 'Hi '.$key.', <br> Logs of following employees were not found for around 3 days in DWA: (from '.$day_fm.') <br>';	
								// foreach($lst_sm AS $def_l)
								// {
									// $message=$message.$def_l.'<br>';
								// }
								// $mail->Body    = $message;
								// //print_r($message);
								// if(!$mail->Send())
								// {
									// $sendError = $mail->ErrorInfo;
								// }else{
								   // echo 'Message has been sent'; 
								// }
									
								// }
							// }							
						
			// }    
	// }
	
	public function alert_4()
    {
			//$u_ids='1,2,71,737';
			$u_ids='1,2,71,737,220,221,222,224,225,226,227,228,381,1080';
			$day1= date('Y-m-d');
			$day_jfk= date('d M Y');
			$day = date('Y-m-d', strtotime($day1.' -1 days'));
			$day_fm= date('d M Y',strtotime($day .' -6 days'));
			
						$c2=$this->alert_model->alert_4($day,$u_ids);
						$lst_sm1=array();
						$sm=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
							$lst_sm1[$row1['dept_id']][]=$row1['full_name'];
							}
							
							$c4=$this->alert_model->sm_list($u_ids,'2');
							if($c4)
							{
								foreach($c4 AS $row1)
								{
									$sm[$row1['full_name']]=$row1['email'];		
									$lst_sm[$row1['email']]=array();
									foreach($lst_sm1 AS $key=>$row2)
									{								
										if($row1['dept_id']==0)
										{
											$lst_sm[$row1['email']] = array_merge($lst_sm[$row1['email']],$row2);
										}
										else 
										{
											$key1=explode(",",$key);
												if(in_array($row1['dept_id'],$key1))
												{	
												$lst_sm[$row1['email']] = array_merge($lst_sm[$row1['email']],$row2);
												}										
										}
									}
								}
							}
							if($sm)
							{
								foreach($sm AS $key=>$value)
								{
									$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								$mail->addAddress($value);							
								$mail->addBCC('vivek.kumar1@byjus.com');
							   //Content
								$mail->isHTML(true);   
								if($lst_sm[$value])
								{								
								$mail->Subject = 'DWA:- Logs not filled for past 7 days !!';								
								$message= 'Hi '.$key.', <br> Logs of below employees were not found for around 7 days in DWA: (from '.$day_fm.') <br>';	
								foreach($lst_sm[$value] AS $def_l)
								{
									$message=$message.$def_l.'<br>';
								}
								$mail->Body    = $message;
								//print_r($message);
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}
								}	
								}
							}							
						
			}    
	}
	
	public function alert_4_rm()
    {
			$u_ids='1,2,71,737,220,221,222,224,225,226,227,228,381,1080';
			$day1= date('Y-m-d');
			$day_jfk= date('d M Y');
			$day = date('Y-m-d', strtotime($day1.' -1 days'));
			$day_fm= date('d M Y',strtotime($day .' -6 days'));
			
						$c2=$this->alert_model->alert_4_rm($day,$u_ids);
						$lst_def=array();
						$lst_sm=array();
						$sm=array();
						$po=array();
						$lst_rep=array();
						$man_rep=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
								if($row1['man_mail'])
								{
									$lst_rep[$row1['man_mail']][]=$row1['full_name'];
									$man_rep[$row1['man_mail']]=$row1['man_name'];
								}
							}					
						
						if($man_rep)
							{
								foreach($man_rep AS $key=>$value)
								{
								$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');								
								$mail->addAddress($key);								
								
								$mail->isHTML(true);        
								$mail->Subject = 'DWA:- Logs not filled for past 7 days - '.$day_fm.' !!';								
								$message= 'Hi '.$value.', <br> Logs of following employees were not found for around 7 days in DWA: (from '.$day_fm.') <br>';	
								foreach($lst_rep[$key] AS $def_l)
								{
									$message=$message.$def_l.'<br>';
								}
								//print_r($message);
								$mail->Body    = $message;
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}									
								}
							}
							}
						
	}
	public function alert_5()
    {
		$u_ids='1,2,71,737';
			$day_fm=  date('d M Y');
						$c2=$this->alert_model->alert_5();
						$lst_sm=array();
						$lst_sm1=array();
						$sm=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
								$lst_sm1[$row1['dept_id_fk']][$row1['project_name']][]=$row1['a_name'];
							}
							
							$c4=$this->alert_model->sm_list($u_ids,'2');
							if($c4)
							{								
								foreach($c4 AS $row1)
								{
									$sm[$row1['full_name']]=$row1['email'];									
									$lst_sm[$row1['email']]=array();
									foreach($lst_sm1 AS $key=>$row2)
									{
										$chkk=explode(",",$row1['dept_id']);
										if(in_array($key,$chkk))
										{	
										$lst_sm[$row1['email']] = array_merge($lst_sm[$row1['email']],$row2);
										}										
									}
								}
							}
							//print_r($lst_sm);
							if($sm)
							{
								foreach($sm AS $key=>$value)
								{
								$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								$mail->addAddress($value);					
								$mail->addBCC('vivek.kumar1@byjus.com');								
								//$mail->addBCC('noreply@byjusdwa.com');
							   //Content
								$mail->isHTML(true);        
								if($lst_sm[$value])
								{
								$mail->Subject = 'DWA:- No Sister Review Activity as checked on '.$day_fm.' !!';								
								$message= 'Hi '.$key.', <br> Following activities have no sister Review activities in DWA <br>';	
								foreach($lst_sm[$value] AS $key1=>$value1)
								{
									$message=$message."<br><b>".$key1.":</b><br>";
									foreach($value1 AS $def_l)
									{
									$message=$message.$def_l.'<br>';
									}
								}
								$mail->Body    = $message;
								//print_r($message);
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}
								}	
								}
							}							
						
			}    
	}
	
	public function alert_6()
    {
			$u_ids='1,2,71,737';
			$day= date('Y-m-d');
			$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d', strtotime($day.' -'.($day_w-1).' days'));							
							$week_end = date('Y-m-d', strtotime($day.' +'.(7-$day_w).' days'));
							$d3 = date('d-M', strtotime($week_start));							
							$d4 = date('d-M Y', strtotime($week_end));
		
						$c2=$this->alert_model->alert_6($week_start,$week_end,"Audit Pending");
						$lst_def=array();
						$lst_sm=array();
						$sm=array();
						$po=array();
						$poc=array();
						$lst_rep=array();
						$proj=array();
						$man_rep=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
								if($row1['project_name'])
								{
								$lst_def[$row1['project_name']][$row1['full_name']]['Total']=$row1['total_logs'];
								$lst_def[$row1['project_name']][$row1['full_name']]['Pending']=$row1['audit_pend'];
								}
								// if($row1['man_mail'])
								// {
									// $lst_rep[$row1['man_mail']][$row1['full_name']][$row1['project_name']]['Total']=$row1['total_logs'];
									// $lst_rep[$row1['man_mail']][$row1['full_name']][$row1['project_name']]['Pending']=$row1['audit_pend'];
									// $man_rep[$row1['man_mail']]=$row1['man_name'];
								// }
								$proj[$row1['project_name']]=1;
								//$lst_sm[$row1['full_name']][$row1['project_name']]['Total']=$row1['total_logs'];
								//$lst_sm[$row1['full_name']][$row1['project_name']]['Pending']=$row1['audit_pend'];
							}
							
							$c3=$this->alert_model->poc_list($u_ids,'4');
							if($c3)
							{
								foreach($c3 AS $row1)
								{
									if(array_key_exists($row1['project_name'],$lst_def))
									{
									$po[$row1['project_name']][]=$row1['email'];
									}
								}
							}
							$c8=$this->alert_model->poc_list($u_ids,'5');
							if($c8)
							{
								foreach($c8 AS $row1)
								{
									if(array_key_exists($row1['project_name'],$lst_def))
									{
									$poc[$row1['project_name']][]=$row1['email'];
									}
								}
							}
							// $c4=$this->alert_model->sm_list($u_ids,'2');
							// if($c4)
							// {
								// foreach($c4 AS $row1)
								// {
									// $sm[$row1['full_name']]=$row1['email'];
								// }
							// }
							if($proj)
							{
								foreach($proj AS $key=>$value)
								{
									
								if(array_key_exists($key,$po) || array_key_exists($key,$poc))
								{
									$mail = $this->phpmailer_library->load();
									$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
									
									if(array_key_exists($key,$poc))
									{
										foreach($poc[$key] AS $ma)
										{
										$mail->addAddress($ma);
										}	
									}									
									if(array_key_exists($key,$po))
									{
										foreach($po[$key] AS $ma)
										{
										$mail->addCC($ma);
										}
									}
								$mail->addBCC('vivek.kumar1@byjus.com');									
								//$mail->addBCC('noreply@byjusdwa.com');
							   //Content
								$mail->isHTML(true);        
								$mail->Subject = 'DWA: '.$key.'- Audit Pending for upcoming week ('.$d3.' to '.$d4.') !!';								
								$message= 'Hi '.$key.' Team, <br> Following team member(s) have unaudited logs for the current week in DWA:<br>';	
								$message=$message."<table border='1'><thead><th>Employee</th><th>Total Logs</th><th>Audit Pending</th></thead>";
								foreach($lst_def[$key] AS $f1=>$def_l)
								{
									$message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.$def_l['Total'].'</td>'.'<td>'.$def_l['Pending'].'</td></tr>';
								}
								$message=$message."</table>";
								//print_r($message);
								$mail->Body    = $message;
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}
								}	
								}
							}
							// if($man_rep)
							// {
								// foreach($man_rep AS $key=>$value)
								// {
								// $mail = $this->phpmailer_library->load();
								// $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');								
								// $mail->addAddress($key);																
								// //$mail->addBCC('noreply@byjusdwa.com');
								
							   // //Content
								// $mail->isHTML(true);        
								// $mail->Subject = 'DWA: '.$key.'- Audit Pending for upcoming week ('.$d3.' to '.$d4.') !!';								
								// $message= 'Hi '.$value.', <br> Following team member(s) have unaudited logs for the current week in DWA:<br>';	
								// $message=$message."<table border='1'><thead><th>Employee</th><th>Project</th><th>Total Logs</th><th>Audit Pending</th></thead>";
								// foreach($lst_rep[$key] AS $f1=>$def_l)
								// {
									// foreach($def_l AS $f2=>$def_23)
									// {
										// $message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.$f2.'</td>'.'<td>'.$def_23['Total'].'</td>'.'<td>'.$def_23['Pending'].'</td></tr>';
									// }
										
								// }
								// $message=$message."</table>";
								// $mail->Body    = $message;
								// //print_r($message);
								// if(!$mail->Send())
								// {
									// $sendError = $mail->ErrorInfo;
								// }else{
								   // echo 'Message has been sent'; 
								// }
									
								// }
							// }
							// if($sm)
							// {
								// foreach($sm AS $key=>$value)
								// {
									// $mail = $this->phpmailer_library->load();
								// $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								// $mail->addAddress($value);							
								// //$mail->addBCC('noreply@byjusdwa.com');
							   // //Content
								// $mail->isHTML(true);        
								// $mail->Subject = 'DWA:- Audit Pending for upcoming week ('.$d3.' to '.$d4.') !!';								
								// $message= 'Hi '.$key.', <br> Following team member(s) have unaudited logs for the current week in DWA:<br>';	
								// $message=$message."<table border='1'><thead><th>Employee</th><th>Project</th><th>Total Logs</th><th>Audit Pending</th></thead>";
								// foreach($lst_sm AS $f1=>$def_l)
								// {
									// foreach($def_l AS $f2=>$def_23)
									// {
									// $message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.$f2.'</td>'.'<td>'.$def_23['Total'].'</td>'.'<td>'.$def_23['Pending'].'</td></tr>';
									// }
								// }
								// $message=$message."</table>";
								// //print_r($message);
								// $mail->Body    = $message;
								
								// if(!$mail->Send())
								// {
									// $sendError = $mail->ErrorInfo;
								// }else{
								   // echo 'Message has been sent'; 
								// }
									
								// }
							// }							
						
			}    
	}
	
	public function alert_6_rm()
    {
			$u_ids='1,2,71,737';
			$day= date('Y-m-d');
			$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d', strtotime($day.' -'.($day_w-1).' days'));							
							$week_end = date('Y-m-d', strtotime($day.' +'.(7-$day_w).' days'));
							$d3 = date('d-M', strtotime($week_start));							
							$d4 = date('d-M Y', strtotime($week_end));
		
						$c2=$this->alert_model->alert_6($week_start,$week_end,"Audit Pending");
						$lst_def=array();
						$lst_sm=array();
						$sm=array();
						$po=array();
						$poc=array();
						$lst_rep=array();
						$proj=array();
						$man_rep=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
								
								if($row1['man_mail'])
								{
									$lst_rep[$row1['man_mail']][$row1['full_name']][$row1['project_name']]['Total']=$row1['total_logs'];
									$lst_rep[$row1['man_mail']][$row1['full_name']][$row1['project_name']]['Pending']=$row1['audit_pend'];
									$man_rep[$row1['man_mail']]=$row1['man_name'];
								}
								$proj[$row1['project_name']]=1;
							}
							
							
							if($man_rep)
							{
								foreach($man_rep AS $key=>$value)
								{
								$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');								
								$mail->addAddress($key);																
								//$mail->addBCC('noreply@byjusdwa.com');
								$mail->addBCC('vivek.kumar1@byjus.com');
							   //Content
								$mail->isHTML(true);        
								$mail->Subject = 'DWA: '.$key.'- Audit Pending for upcoming week ('.$d3.' to '.$d4.') !!';								
								$message= 'Hi '.$value.', <br> Following team member(s) have unaudited logs for the current week in DWA:<br>';	
								$message=$message."<table border='1'><thead><th>Employee</th><th>Project</th><th>Total Logs</th><th>Audit Pending</th></thead>";
								foreach($lst_rep[$key] AS $f1=>$def_l)
								{
									foreach($def_l AS $f2=>$def_23)
									{
										$message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.$f2.'</td>'.'<td>'.$def_23['Total'].'</td>'.'<td>'.$def_23['Pending'].'</td></tr>';
									}
										
								}
								$message=$message."</table>";
								$mail->Body    = $message;
								//print_r($message);
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}
									
								}
							}					
			}    
	}
	
	public function alert_7()
    {
			$u_ids='1,2,71,737';
			$day= date('Y-m-d');
			$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d', strtotime($day.' -'.($day_w+6).' days'));							
							$week_end = date('Y-m-d', strtotime($week_start.' +6 days'));
							$d3 = date('d-M', strtotime($week_start));							
							$d4 = date('d-M Y', strtotime($week_end));
		
						$c2=$this->alert_model->alert_6($week_start,$week_end,"Review Pending");
						$lst_def=array();
						$lst_sm=array();
						$sm=array();
						$po=array();
						$poc=array();
						$lst_rep=array();
						$proj=array();
						$man_rep=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
								if($row1['project_name'])
								{
								$lst_def[$row1['project_name']][$row1['full_name']]['Total']=$row1['total_logs'];
								$lst_def[$row1['project_name']][$row1['full_name']]['Pending']=$row1['audit_pend'];
								}
								if($row1['man_mail'])
								{
									$lst_rep[$row1['man_mail']][$row1['full_name']][$row1['project_name']]['Total']=$row1['total_logs'];
									$lst_rep[$row1['man_mail']][$row1['full_name']][$row1['project_name']]['Pending']=$row1['audit_pend'];
									$man_rep[$row1['man_mail']]=$row1['man_name'];
								}
								$proj[$row1['project_name']]=1;
								// $lst_sm[$row1['full_name']][$row1['project_name']]['Total']=$row1['total_logs'];
								// $lst_sm[$row1['full_name']][$row1['project_name']]['Pending']=$row1['audit_pend'];
							}
							
							$c3=$this->alert_model->poc_list($u_ids,'4');
							if($c3)
							{
								foreach($c3 AS $row1)
								{
									if(array_key_exists($row1['project_name'],$lst_def))
									{
									$po[$row1['project_name']][]=$row1['email'];
									}
								}
							}
							$c8=$this->alert_model->poc_list($u_ids,'5');
							if($c8)
							{
								foreach($c8 AS $row1)
								{
									if(array_key_exists($row1['project_name'],$lst_def))
									{
									$poc[$row1['project_name']][]=$row1['email'];
									}
								}
							}
							$c4=$this->alert_model->sm_list($u_ids,'2');
							if($c4)
							{
								foreach($c4 AS $row1)
								{
									$sm[$row1['full_name']]=$row1['email'];
								}
							}
							if($proj)
							{
								foreach($proj AS $key=>$value)
								{
									
								if(array_key_exists($key,$po) || array_key_exists($key,$poc))
								{
									$mail = $this->phpmailer_library->load();
									$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
									
									if(array_key_exists($key,$poc))
									{
										foreach($poc[$key] AS $ma)
										{
										$mail->addAddress($ma);
										}	
									}									
									if(array_key_exists($key,$po))
									{
										foreach($po[$key] AS $ma)
										{
										$mail->addCC($ma);
										}
									}
								$mail->addBCC('vivek.kumar1@byjus.com');									
								//$mail->addBCC('noreply@byjusdwa.com');
							   //Content
								$mail->isHTML(true);        
								$mail->Subject = 'DWA: '.$key.'- Reviews Pending for previous week ('.$d3.' to '.$d4.') !!';								
								$message= 'Hi '.$key.' Team, <br> Following team member(s) have unreviewed logs for the previous week in DWA:<br>';	
								$message=$message."<table border='1'><thead><th>Employee</th><th>Total Logs</th><th>Reviews Pending</th></thead>";
								foreach($lst_def[$key] AS $f1=>$def_l)
								{
									$message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.$def_l['Total'].'</td>'.'<td>'.$def_l['Pending'].'</td></tr>';
								}
								$message=$message."</table>";
								//print_r($message);
								$mail->Body    = $message;
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}
								}	
								}
							}
							if($man_rep)
							{
								foreach($man_rep AS $key=>$value)
								{
								$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');								
								$mail->addAddress($key);	
								$mail->addBCC('vivek.kumar1@byjus.com');
								//$mail->addBCC('noreply@byjusdwa.com');
								
							   //Content
								$mail->isHTML(true);        
								$mail->Subject = 'DWA: '.$key.'- Reviews Pending for previous week ('.$d3.' to '.$d4.') !!';								
								$message= 'Hi '.$value.', <br> Following team member(s) have unreviewed logs for the previous week in DWA:<br>';	
								$message=$message."<table border='1'><thead><th>Employee</th><th>Project</th><th>Total Logs</th><th>Reviews Pending</th></thead>";
								foreach($lst_rep[$key] AS $f1=>$def_l)
								{
									foreach($def_l AS $f2=>$def_23)
									{
										$message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.$f2.'</td>'.'<td>'.$def_23['Total'].'</td>'.'<td>'.$def_23['Pending'].'</td></tr>';
									}
										
								}
								$message=$message."</table>";
								$mail->Body    = $message;
								//print_r($message);
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}
									
								}
							}
							// if($sm)
							// {
								// foreach($sm AS $key=>$value)
								// {
									// $mail = $this->phpmailer_library->load();
								// $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								// $mail->addAddress($value);							
								// //$mail->addBCC('noreply@byjusdwa.com');
							   // //Content
								// $mail->isHTML(true);        
								// $mail->Subject = 'DWA:- Reviews Pending for previous week ('.$d3.' to '.$d4.') !!';								
								// $message= 'Hi '.$key.', <br> Following team member(s) have unreviewed logs for the previous week in DWA:<br>';	
								// $message=$message."<table border='1'><thead><th>Employee</th><th>Project</th><th>Total Logs</th><th>Reviews Pending</th></thead>";
								// foreach($lst_sm AS $f1=>$def_l)
								// {
									// foreach($def_l AS $f2=>$def_23)
									// {
									// $message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.$f2.'</td>'.'<td>'.$def_23['Total'].'</td>'.'<td>'.$def_23['Pending'].'</td></tr>';
									// }
								// }
								// $message=$message."</table>";
								// print_r($message);
								// $mail->Body    = $message;
								
								// // if(!$mail->Send())
								// // {
									// // $sendError = $mail->ErrorInfo;
								// // }else{
								   // // echo 'Message has been sent'; 
								// // }
									
								// }
							// }
			}    
	}
	
	
	
	public function alert_9()
    {
			$u_ids='1,2,71,737';
			$day1= date('Y-m-d');
			$day = date('Y-m-d', strtotime($day1.' -1 days'));
			$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}
														
							$week_start = date('Y-m-d', strtotime($day.' -'.($day_w+6).' days'));							
							$week_end = date('Y-m-d', strtotime($week_start.' +2 days'));		
							$d3 = date('d-M', strtotime($week_start));
							$d4 = date('d-M Y', strtotime($week_end));
						$c2=$this->alert_model->alert_9($week_start,$week_end);
						$lst_def=array();
						$lst_sm1=array();
						$sm=array();
						$po=array();
						$poc=array();
						$lst_rep=array();
						$proj=array();
						$man_rep=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
								if($row1['project_name'])
								{
								$lst_def[$row1['project_name']][]=array('a_name'=>$row1['activity_name'],'wu_name'=>$row1['wu_name'],'std_tgt'=>$row1['std_tgt'],
								'achieved_tgt'=>$row1['achieved_tgt']);
								}
								$proj[$row1['project_name']]=1;
								$lst_sm1[$row1['dept_id_fk']][$row1['project_name']][]=array('a_name'=>$row1['activity_name'],'wu_name'=>$row1['wu_name'],'std_tgt'=>$row1['std_tgt'],
								'achieved_tgt'=>$row1['achieved_tgt']);
								
							}
							//print_r($lst_def);
							$c3=$this->alert_model->poc_list($u_ids,'4');
							if($c3)
							{
								foreach($c3 AS $row1)
								{
									if(array_key_exists($row1['project_name'],$lst_def))
									{
									$po[$row1['project_name']][]=$row1['email'];
									}
								}
							}
							$c8=$this->alert_model->poc_list($u_ids,'5');
							if($c8)
							{
								foreach($c8 AS $row1)
								{
									if(array_key_exists($row1['project_name'],$lst_def))
									{
									$poc[$row1['project_name']][]=$row1['email'];
									}
								}
							}
							
							$c4=$this->alert_model->sm_list($u_ids,'2');
							if($c4)
							{
								foreach($c4 AS $row1)
								{
									$sm[$row1['full_name']]=$row1['email'];		
									$lst_sm[$row1['email']]=array();
									foreach($lst_sm1 AS $key=>$row2)
									{								
										if($row1['dept_id']==0)
										{
											$lst_sm[$row1['email']] = array_merge($lst_sm[$row1['email']],$row2);
										}
										else 
										{
											$key1=explode(",",$key);
												if(in_array($row1['dept_id'],$key1))
												{	
												 $lst_sm[$row1['email']] = array_merge($lst_sm[$row1['email']],$row2);
												}										
										}
									}
								}
							}
							
							if($proj)
							{
								foreach($proj AS $key=>$value)
								{
									
								if(array_key_exists($key,$po) || array_key_exists($key,$poc))
								{
									$mail = $this->phpmailer_library->load();
									$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
									
									if(array_key_exists($key,$poc))
									{
										foreach($poc[$key] AS $ma)
										{
										$mail->addAddress($ma);
										}	
									}									
									if(array_key_exists($key,$po))
									{
										foreach($po[$key] AS $ma)
										{
										$mail->addCC($ma);
										}
									}	
									$mail->addBCC('vivek.kumar1@byjus.com');
								//$mail->addBCC('noreply@byjusdwa.com');
							   //Content
								$mail->isHTML(true);        
								$mail->Subject = 'DWA: '.$key.'- 	Activities require attention ('.$d3.' to '.$d4.') !!';								
								$message= 'Hi '.$key.' Team, <br> Following activities require your attention:<br>';	
								$message=$message."<table border='1'><thead><th>Activity Name</th><th>Work Units</th><th>Set Target</th><th>Suggested Target</th></thead>";
								foreach($lst_def[$key] AS $f1=>$def_l)
								{
									//$message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.array_values($def_l['a_name']).'</td>'.'<td>'.array_values($def_l['std_tgt']).'</td>'.'<td>'.array_values($def_l['achieved_tgt']).'</td></tr>';
									$message=$message.'<tr><td>'.$def_l['a_name'].'</td>'.'<td>'.$def_l['wu_name'].'</td>'.'<td>'.$def_l['std_tgt'].'</td>'.'<td>'.$def_l['achieved_tgt'].'</td></tr>';
								}
								$message=$message."</table>";
								//print_r($message);
								$mail->Body    = $message;
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}
								}	
								}
							}
							if($sm)
							{
								foreach($sm AS $key=>$value)
								{
									$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								$mail->addAddress($value);							
								//$mail->addBCC('noreply@byjusdwa.com');
								$mail->addBCC('vivek.kumar1@byjus.com');
							   //Content
								$mail->isHTML(true);        
								if($lst_sm[$value])
								{
									$mail->Subject = 'DWA: '.$key.'- 	Activities require attention ('.$d3.' to '.$d4.') !!';								
								$message= 'Hi '.$key.' , <br> Following activities require your attention:<br>';	
								$message=$message."<table border='1'><thead><th>Project Name</th><th>Activity Name</th><th>Work Units</th><th>Set Target</th><th>Suggested Target</th></thead>";
								
								foreach($lst_sm[$value] AS $f1=>$def_l2)
								{
									foreach($def_l2 AS $def_l)
									{
									//$message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.array_values($def_l['a_name']).'</td>'.'<td>'.array_values($def_l['std_tgt']).'</td>'.'<td>'.array_values($def_l['achieved_tgt']).'</td></tr>';
									$message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.$def_l['a_name'].'</td>'.'<td>'.$def_l['wu_name'].'</td>'.'<td>'.$def_l['std_tgt'].'</td>'.'<td>'.$def_l['achieved_tgt'].'</td></tr>';
									}
								}
								$message=$message."</table>";
								$mail->Body    = $message;
								//print_r($message);
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}
								}
								}
							}		
			}    
	}
		
	public function alert_8()
    {
			$u_ids='1,2,71,737';
			$day= date('Y-m-d');
			$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d', strtotime($day.' -'.($day_w+6).' days'));							
							$week_end = date('Y-m-d', strtotime($week_start.' +6 days'));
							$d3 = date('d-M', strtotime($week_start));							
							$d4 = date('d-M Y', strtotime($week_end));
		
						$c2=$this->alert_model->alert_8($week_start,$week_end,"Review Pending");
						$lst_def=array();
						$lst_sm=array();
						$lst_sm1=array();
						$sm=array();
						$po=array();
						$poc=array();
						$lst_rep=array();
						$proj=array();
						$man_rep=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
								if($row1['project_name'])
								{
								$lst_def[$row1['project_name']][$row1['full_name']]['Total']=$row1['total_logs'];
								$lst_def[$row1['project_name']][$row1['full_name']]['Pending']=$row1['audit_pend'];
								}
								// if($row1['man_mail'])
								// {
									// $lst_rep[$row1['man_mail']][$row1['full_name']][$row1['project_name']]['Total']=$row1['total_logs'];
									// $lst_rep[$row1['man_mail']][$row1['full_name']][$row1['project_name']]['Pending']=$row1['audit_pend'];
									// $man_rep[$row1['man_mail']]=$row1['man_name'];
								// }
								$proj[$row1['project_name']]=1;
							//	$lst_sm1[$row1['dept_id_fk']][$row1['full_name']][$row1['project_name']]['Total']=$row1['total_logs'];
								//$lst_sm1[$row1['dept_id_fk']][$row1['full_name']][$row1['project_name']]['Pending']=$row1['audit_pend'];
							}
							
							$c3=$this->alert_model->poc_list($u_ids,'4');
							if($c3)
							{
								foreach($c3 AS $row1)
								{
									if(array_key_exists($row1['project_name'],$lst_def))
									{
									$po[$row1['project_name']][]=$row1['email'];
									}
								}
							}
							$c8=$this->alert_model->poc_list($u_ids,'5');
							if($c8)
							{
								foreach($c8 AS $row1)
								{
									if(array_key_exists($row1['project_name'],$lst_def))
									{
									$poc[$row1['project_name']][]=$row1['email'];
									}
								}
							}
							// $c4=$this->alert_model->sm_list($u_ids,'2');
							// if($c4)
							// {
								// foreach($c4 AS $row1)
								// {
									// $sm[$row1['full_name']]=$row1['email'];
									// $lst_sm[$row1['email']]=array();
									// foreach($lst_sm1 AS $key=>$row2)
									// {								
										// if($row1['dept_id']==0)
										// {
											// $lst_sm[$row1['email']] = array_merge($lst_sm[$row1['email']],$row2);
										// }
										// else 
										// {
											// $key1=explode(",",$key);
												// if(in_array($row1['dept_id'],$key1))
												// {	
												// $lst_sm[$row1['email']] = array_merge($lst_sm[$row1['email']],$row2);
												// }										
										// }
									// }
								// }
							// }
							if($proj)
							{
								foreach($proj AS $key=>$value)
								{
									
								if(array_key_exists($key,$po) || array_key_exists($key,$poc))
								{
									$mail = $this->phpmailer_library->load();
									$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
									
									if(array_key_exists($key,$poc))
									{
										foreach($poc[$key] AS $ma)
										{
										$mail->addAddress($ma);
										}	
									}									
									if(array_key_exists($key,$po))
									{
										foreach($po[$key] AS $ma)
										{
										$mail->addCC($ma);
										}
									}	
									$mail->addBCC('vivek.kumar1@byjus.com');
								//$mail->addBCC('noreply@byjusdwa.com');
							   //Content
								$mail->isHTML(true);        
								$mail->Subject = 'DWA: '.$key.'- Reviews Pending for previous week ('.$d3.' to '.$d4.') !!';								
								$message= 'Hi '.$key.' Team, <br> Following Reviewers have unreviewed logs for the previous week in DWA:<br>';	
								$message=$message."<table border='1'><thead><th>Reviewer</th><th>Total Logs</th><th>Reviews Pending</th></thead>";
								foreach($lst_def[$key] AS $f1=>$def_l)
								{
									$message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.$def_l['Total'].'</td>'.'<td>'.$def_l['Pending'].'</td></tr>';
								}
								$message=$message."</table>";
								//print_r($message);
								$mail->Body    = $message;
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}
								}	
								}
							}
							// if($man_rep)
							// {
								// foreach($man_rep AS $key=>$value)
								// {
								// $mail = $this->phpmailer_library->load();
								// $mail->setFrom('noreply@byjusdwa.com','Urgent!!!');								
								// $mail->addAddress($key);																
								// //$mail->addBCC('noreply@byjusdwa.com');
								
							   // //Content
								// $mail->isHTML(true);        
								// $mail->Subject = 'DWA: '.$key.'- Reviews Pending for previous week ('.$d3.' to '.$d4.') !!';								
								// $message= 'Hi '.$value.', <br> Following Reviewers have unreviewed logs for the previous week in DWA:<br>';	
								// $message=$message."<table border='1'><thead><th>Reviewer</th><th>Project</th><th>Total Logs</th><th>Reviews Pending</th></thead>";
								// foreach($lst_rep[$key] AS $f1=>$def_l)
								// {
									// foreach($def_l AS $f2=>$def_23)
									// {
										// $message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.$f2.'</td>'.'<td>'.$def_23['Total'].'</td>'.'<td>'.$def_23['Pending'].'</td></tr>';
									// }
										
								// }
								// $message=$message."</table>";
								// $mail->Body    = $message;
								// print_r($message);
								// // if(!$mail->Send())
								// // {
									// // $sendError = $mail->ErrorInfo;
								// // }else{
								   // // echo 'Message has been sent'; 
								// // }
									
								// }
							// }
			}    
	}
	
	public function alert_8_sm()
    {
			$u_ids='1,2,71,737';
			$day= date('Y-m-d');
			$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d', strtotime($day.' -'.($day_w+6).' days'));							
							$week_end = date('Y-m-d', strtotime($week_start.' +6 days'));
							$d3 = date('d-M', strtotime($week_start));							
							$d4 = date('d-M Y', strtotime($week_end));
		
						$c2=$this->alert_model->alert_8($week_start,$week_end,"Review Pending");
						$lst_def=array();
						$lst_sm=array();
						$lst_sm1=array();
						$sm=array();
						$po=array();
						$poc=array();
						$lst_rep=array();
						$proj=array();
						$man_rep=array();
						if($c2)
						{
							foreach($c2 AS $row1)
							{
								
								$proj[$row1['project_name']]=1;
								$lst_sm1[$row1['dept_id_fk']][$row1['full_name']][$row1['project_name']]['Total']=$row1['total_logs'];
								$lst_sm1[$row1['dept_id_fk']][$row1['full_name']][$row1['project_name']]['Pending']=$row1['audit_pend'];
							}
							
							
							$c4=$this->alert_model->sm_list($u_ids,'2');
							if($c4)
							{
								foreach($c4 AS $row1)
								{
									$sm[$row1['full_name']]=$row1['email'];
									$lst_sm[$row1['email']]=array();
									foreach($lst_sm1 AS $key=>$row2)
									{								
										if($row1['dept_id']==0)
										{
											$lst_sm[$row1['email']] = array_merge($lst_sm[$row1['email']],$row2);
										}
										else 
										{
											$key1=explode(",",$key);
												if(in_array($row1['dept_id'],$key1))
												{	
												$lst_sm[$row1['email']] = array_merge($lst_sm[$row1['email']],$row2);
												}										
										}
									}
								}
							}
							
							if($sm)
							{
								foreach($sm AS $key=>$value)
								{
									$mail = $this->phpmailer_library->load();
								$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
								$mail->addAddress($value);							
								//$mail->addBCC('noreply@byjusdwa.com');
								$mail->addBCC('vivek.kumar1@byjus.com');
							   //Content
							   //echo "Senioe Mna";
								$mail->isHTML(true); 
								if($lst_sm[$value])
								{
								$mail->Subject = 'DWA:- Reviews Pending for previous week ('.$d3.' to '.$d4.') !!';								
								$message= 'Hi '.$key.', <br> Following Reviewers have unreviewed logs for the previous week in DWA:<br>';	
								$message=$message."<table border='1'><thead><th>Reviewer</th><th>Project</th><th>Total Logs</th><th>Reviews Pending</th></thead>";
								foreach($lst_sm[$value] AS $f1=>$def_l)
								{
									foreach($def_l AS $f2=>$def_23)
									{
									$message=$message.'<tr><td>'.$f1.'</td>'.'<td>'.$f2.'</td>'.'<td>'.$def_23['Total'].'</td>'.'<td>'.$def_23['Pending'].'</td></tr>';
									}
								}
								$message=$message."</table>";
								//print_r($message);
								$mail->Body  = $message;
								
								if(!$mail->Send())
								{
									$sendError = $mail->ErrorInfo;
								}else{
								   echo 'Message has been sent'; 
								}
								}
								}
							}
			}    
	}
	
}
}
?>