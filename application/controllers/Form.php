<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form extends CI_Controller
{
	
    public function __construct()
    {       
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('form_model');	
		$this->load->model('user_model');	
        $this->load->library('session');		
    }
	
	public function ins_ques_response_mst()
    {
        $ans = $this->input->post("C");
        $resp = $this->form_model->ins_ques_response_mst($ans);
        echo $resp;
    }
	
	public function ins_form_response_mst()
    {
        $ans = $this->input->post("C");
        $resp = $this->form_model->ins_form_response_mst($ans);
        echo $resp;
    }
	public function fb_form_response_mst()
    {
        $ans = $this->input->post("C");
        $resp = $this->form_model->fb_form_response_mst($ans);
        echo $resp;
    }
	
	
	public function load_pros()
    {
        $file_nm            = $this->input->get('a');
		$u_id = $this->session->userdata('user_id');
		$emps            = $this->input->get('emp_id');
		$slot_chosen=$this->session->userdata('slot_id');
		if(!$u_id || !$slot_chosen){
			  redirect('user/login_view');
			}
		 $resp = $this->form_model->load_pros($file_nm,$u_id,$emps,$slot_chosen);
		 if( $resp)
		 {
			 foreach( $resp AS $v)
			 {
				 echo "<option value='".$v['val_id']."'>".$v['val_name']."</option>";
			 }
		 }
    }
	
	public function ins_sec_response_mst()
    {
        $ans = $this->input->post("C");
        $resp = $this->form_model->ins_sec_response_mst($ans);
        echo $resp;
    }
    
	public function fd_sec_response_mst()
    {
        $ans = $this->input->post("C");
        $resp = $this->form_model->fd_sec_response_mst($ans);
        echo $resp;
    }
	
	
	public function fd_ques_response_mst()
    {
        $ans = $this->input->post("C");
        $resp = $this->form_model->fd_ques_response_mst($ans);
        echo $resp;
    }
	
	public function del_ques_response_mst()
    {
        $ans = $this->input->post("C");
        $resp = $this->form_model->del_ques_response_mst($ans);
        echo $resp;
    }
	
	public function del_full_sec_ques_mst()
    {
        $ans = $this->input->post("C");
        $resp = $this->form_model->del_full_sec_ques_mst($ans);
        echo $resp;
    }
	
	public function same_mr_ans()
    {
        $ans = $this->input->post("C");
        $resp = $this->form_model->same_mr_ans($ans);
        echo $resp;
    }
	
	public function load_view_full_form()
    {
		$file_nm            = $this->input->get('a');
		$u_id = $this->session->userdata('user_id');
		$slot_chosen=$this->session->userdata('slot_id');
		if(!$u_id || !$slot_chosen){
			  redirect('user/login_view');
			}
		$fff = $this->session->userdata('forms_list');		
		$section =  $this->form_model->fetch_section_list_view($file_nm,$u_id,$slot_chosen);
		$ques =  $this->form_model->fetch_ques_list_view($file_nm,$u_id,$slot_chosen);
		$sec_resp =  $this->form_model->fetch_us_s_view_list($file_nm,$u_id,$slot_chosen);
		$opts =  $this->form_model->fetch_opt_val_list($file_nm,$u_id);
		$opt_vals=array();
		foreach($opts AS $opt_id)
		{
			$opt_vals[$opt_id['opt_code']][$opt_id['opt_values']]=$opt_id['opt_name'];
			
		}
                     echo '<div class="x_titl">';
				echo '<div id="wizard" class="form_wizard wizard_horizontal">';
                  echo '<ul class="wizard_steps anchor">'; 
						$kl=1;
						foreach($section AS $des_sec)
						{
						
									$cl='fi_col';	
						echo '<li class="s_click">
								<a>
								  <span class="step_no '.$cl.'" id="step-'.$des_sec['section_id'].'">'.$kl.'</span>
								  <span class="step_descr"><span>Step '.$kl.'<br/></span>
									<small class="dessec">'.$des_sec['sec_step_name'].'</small>
												</span>
								</a>
							</li>';
							$kl++;
							
						}
					 echo '</ul>';
					echo '</div>';
                echo '<div class="clearfix"></div>';
              echo '</div>';
            echo '<div class="x_content">';				
				$i=0;			
				//$a1=1;				
				foreach($section AS $sids)
				{
					//if($sids['section_id']==$section_id)
					{
					$num='null';
				if($sids['is_many']==1)
				{
					$num=1;
				}
					echo '<div class="f_section" sec_id="'.$sids['section_id'].'" id="fs_'.$sids['section_id'].'"> ';
					echo '<div class="panel-group">
								<div class="panel panel-primary">
								  <div class="panel-heading">'.$sids['sec_name'].'</div>
								  <div class="panel-body">';
						echo "<span>".$sids['sec_desc']."</span>";
					// if($sids['section_id']==1)
							// {
								
								// echo "<p>Dear Content/Design Guru,</p>
			// <p>Welcome to the new self-appraisal platform. This is a platform to highlight your contribution towards the company over the past year and help make your growth smoother in the company and contribution for the next year. This process is designed to provide time for the employee and supervisors to look back over the past and plan realistically for the future.     </p>
			// <p>We encourage you to be proud of your accomplishments and candid about your areas of improvement. The questions which follow are intended to help you organize your thoughts and share information with your supervisor prior to receiving your performance appraisal.  </p>
			// <p>An essential goal of the appraisal meeting is that both you and your supervisors know clearly what you expect of each other and feel strongly that you can achieve your objectives by working together.</p>
			// <p>The following text will be useful guidelines if confusions arise:</p>
			// <p>- Priority order is from 1 to 5, with 5 being your highest priority.</p>
			// <p>- Weigh each option against the others to come up with your most-suited priority order.</p>
			// <p>- Short Answer:</p>
			   // <p>-- Fill the right-spelled names of people according to the details in ESP, when required</p>
			   // <p>-- Give crisp responses</p>
			// <p>- Long Answer:</p>
			  // <p>-- Write your answers in detail.</p>
			  // <p>-- When suggestions are asked for, make them point-wise and specific.</p>
							
					  // <hr>";
							// }
			$pomr_inc=1;
              foreach($sec_resp AS $sr_tbl)
					{
						if($sr_tbl['section_id']==$sids['section_id'])
						{
						echo '<div class="f_resp_section" sec_r_cd="'.$sr_tbl['sec_cd'].'" sec_cd="'.$sr_tbl['sec_code'].'" num="'.$num.'" id="fs_resp_'.$num.'"> ';
						
						if($sids['is_many']==1)
						{
						echo '<div class="panel-group">
								<div class="panel panel-danger">
								  <div class="panel-heading">
									<span><a role="button" data-toggle="collapse" href="#collapse_'.$sids['sec_select'].'_'.$pomr_inc.'" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">  '.$sids['sec_step_name']." #".$pomr_inc.'</a>
									</span>';									
							  echo '</div>
									 <div id="collapse_'.$sids['sec_select'].'_'.$pomr_inc.'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									 <div class="panel-body">';
						}
						$ques_ll=explode(":",$sids['sec_name']);
						if($sids['sec_select']=='project')
						{
							echo '<div class="row sec_r_div" type="project_id_fk" sec_resp_id="'.$sr_tbl['sec_resp_id'].'">';
									echo '<div class="col-md-12">';
									echo "<label class='l_font_fix_4 required'>Choose ".strtolower($ques_ll[0])." for the review period.</label>";
									echo "</div>";
									echo '<div class="col-md-4">';					
									echo "<h6>".$sr_tbl['pro_nm']."</h6>";
								echo "</div>";
							echo "</div>";
							
						}
						if($sids['sec_select']=='owner')
						{
							//print_r($sr_tbl);
							echo '<div class="row sec_r_div" type="L_O_id_fk" sec_resp_id="'.$sr_tbl['sec_resp_id'].'">';
								echo '<div class="col-md-12">';
									echo "<label class='l_font_fix_4 required'>Who do you identify as your project owner for ".strtolower($ques_ll[0]).".</label>";
									echo "</div>";
									echo '<div class="col-md-4">';
									echo "<h6>".$sr_tbl['po_name']."</h6>";
								echo "</div>";
							echo "</div>";
							
						}
						if($sids['sec_select']=='manager')
						{
							echo '<div class="row sec_r_div" type="MR_id_fk" sec_resp_id="'.$sr_tbl['sec_resp_id'].'">';
								echo '<div class="col-md-12">';
									echo "<label class='l_font_fix_4 required'>Who do you identify as your project manager for ".strtolower($ques_ll[0]).".</label>";
									echo "</div>";
									echo '<div class="col-md-4">';
											echo "<h6>".$sr_tbl['m_name']."</h6>";
									
								echo "</div>";
							echo "</div>";
							
						}
					
					
				if($sids['section_id']==1)
				{
					$full_nm=$this->session->userdata('user_name');
					$tnl_id=$this->session->userdata('emp_id');
					
					echo '<div class="row">
									<div class="col-md-3 col-md-offset-2">
										<label class="l_font_fix_4 form-check-label">Employee Name : </label> 
										<h6 class="h6_sty">'.$full_nm.'</h6>
									</div>								 
									<div class="col-md-3 col-md-offset-2">
										<label class="l_font_fix_4 form-check-label">TNL ID : </label> 
										<h6 class="h6_sty">'.$tnl_id.'</h6>
									</div>
								 </div>';
			  }
			  
			  $df='';	
			  $a1=1;
		  foreach($ques AS $qids)
		  {
			  
			  if($sids['section_id']==$qids['section_id_fk'] && $sr_tbl['sec_cd']==$qids['sec_cd'])
			  {
				  
				  // if(!($sr_tbl['sec_resp_id']) && $sr_tbl['sec_select'])
				  // {
					  // $df='style="display:none;"';					  
				  // }
				
				echo '<div class="row cus_div" ques_id="'.$qids['question_id'].'" ques_resp_id="'.$qids['ques_resp_id'].'" '.$df.'>';
					echo '<div class="col-md-12">';
					$red="";
							if($qids['is_optional']!=1)
							{
								$red=" required";
							}
						echo "<label class='l_font_fix_4".$red."'>".$qids['ques_name']."</label>";
							echo "<div class='sub_qu'><span>".$qids['sub_ques_name']."</span></div>";
						if($qids['q_type_id_fk']==4)
						{
						if($qids['opt_code'])
						{
							foreach($opt_vals[$qids['opt_code']] AS $o_key=>$o_val)
							{
								$ck='ans_list';
								if($qids['decision']=='Y')
								{
									$ck='ans_list dec_chk';
								}
								if($qids['decision']=='O')
								{
									$ck='ans_list many_chk';
								}
								$sel='';
								if($qids['ques_typ_val']==$o_key)
								{
									$sel='checked';
									
								}
								echo ' <div class="custom-control custom-radio">
										<input class="'.$ck.' custom-control-input" '.$sel.' type="radio" t_val='.$o_key.' name="form_radio_'.$num."_".$sids['sec_code'].'_'.$qids['question_id'].'">';
								echo '<label class="custom-control-label">'.$o_val.'</label></div>';
							}						
						}
						}
						
						if($qids['q_type_id_fk']==3)
						{
							$a_key=array();
							echo "<table class='table table-responsive table-bordered'>";
						if($qids['opt_code'])
						{
							echo "<thead>";
							echo '<th></th>';
							foreach($opt_vals[$qids['opt_code']] AS $o_key=>$o_val)
							{
								//echo $o_key."|".$o_val;
								$a_key[]=$o_key;
								echo '<th>'.$o_val.'</th>';
							}
							echo "</thead>";				
						}
						echo "<tbody>";
						$tr=explode(",",$qids['ques_tbl_val']);
						if($qids['ques_typ_val'])
						{
						$ans_values=explode(",",$qids['ques_typ_val']);
						}else{
							$ans_values='';
						}
								//$a1=1;
								$c1=0;
								$b1=1;
								foreach($tr AS $tk1=>$tr_val)
								{								
									echo "<tr class='cc_row'>";
									echo "<td class='custom-control-label' style='text-align:left;'>".$tr_val."</td>";
									
									foreach($a_key AS $t_val)
									{
										$rg='';
										if(is_array($ans_values))
										{
											//print_r($ans_values);
										  if($t_val==$ans_values[$c1])
										  {
											$rg='checked';  
										  }
										}
										echo '<td><input class="ans_tbl_list custom-control-input" '.$rg.' type="radio" t_val='.$t_val.' name="form_radio_tbl_'.$sr_tbl['section_id']."_".$pomr_inc.'_'.$a1.$c1.'"></td>';
									}									
									echo "</tr>";
								
									$c1++;
								}
						echo "</tbody>";
						echo "</table>";
						}
						if($qids['q_type_id_fk']==2)
						{
							echo '<div class="">
										<textarea  rows="5" class="form-control ans_txt_list">'.$qids['ques_resp_desc'].'</textarea>
									</div>';
						}
					echo "</div>";
				echo "</div>";
				$a1++;
			   }
			  }
		  echo "</div>";
		  if($sids['is_many']==1)
						{
									echo "</div>";
								echo "</div>";
							echo "</div>";
						echo "</div>";
						$num++;
							$pomr_inc++;
						}
						// echo "</div>";
					}
			}			
		    echo "</div>";
			echo "</div>";
		}	
		 echo "</div>";
		 echo "</div>";
	  }
    }
	
	public function load_v_ins()
    {
		
	    $file_nm            = $this->input->get('a');
		$section_id            = $this->input->get('s_id');		
		if($file_nm =="self_form")
		{
		if(!$section_id)
		{
			$section_id=1;
		}		
		$this->session->set_userdata('sec_id', $section_id);
		}
		$u_id = $this->session->userdata('user_id');
		$slot_chosen=$this->session->userdata('slot_id');
		if(!$u_id || !$slot_chosen){
			  redirect('user/login_view');
			}
		$fff = $this->session->userdata('forms_list');		
		$forms =  $this->form_model->fetch_form_list($u_id,$fff);
		$section =  $this->form_model->fetch_section_list($file_nm,$u_id,$section_id,$slot_chosen);
		$ques =  $this->form_model->fetch_ques_list($file_nm,$u_id,$section_id,$slot_chosen);
		$sec_resp =  $this->form_model->fetch_us_sec_list($file_nm,$u_id,$section_id,$slot_chosen);
		
		$slcik_stop=$this->form_model->fetch_sec_stop($file_nm,$u_id,$slot_chosen);
		$sec_stop=0;
		if($slcik_stop)
		{
			$sec_stop=$slcik_stop[0]['stop_sec'];
		}
		
		if($section_id==1)
		{
			$prev_sec_id='';
		}else{
			//print_r($section_id);
			$prev_sec =  $this->form_model->prev_sec($file_nm,$u_id,$section_id,$slot_chosen);
			$prev_sec_id=$prev_sec[0]['prev_ss'];
		}
		$sel_label=$sec_resp[0]['sec_select'];
		$parent_sec=$sec_resp[0]['parent_sec'];
		$select_values=array();
		if($sel_label)
		{
		$select_values=  $this->form_model->fetch_sel_val($file_nm,$u_id,$section_id,$sel_label,$parent_sec,$slot_chosen);
		}
		$opts =  $this->form_model->fetch_opt_val_list($file_nm,$u_id);
		$opt_vals=array();
		foreach($opts AS $opt_id)
		{
			$opt_vals[$opt_id['opt_code']][$opt_id['opt_values']]=$opt_id['opt_name'];
			
		}
		//$data['opt_vals']=$opt_vals;
                     echo '<div class="x_titl">';
				echo '<div id="wizard" class="form_wizard wizard_horizontal">';
                  echo '<ul class="wizard_steps anchor">'; 
						$kl=1;
						foreach($section AS $des_sec)
						{
							//if($des_sec['section_id']<=8)
							{		
						$cl='';
								if($des_sec['section_id']==$section_id)
								{
									$cl='fi_col';
								}
								if($sec_stop && $des_sec['section_id']>$sec_stop)
								{
									echo '<li class="">';
								}else{
									echo '<li class="s_click">';
								}
									
						echo '<a>
								  <span class="step_no '.$cl.'" id="step-'.$des_sec['section_id'].'">'.$kl.'</span>
								  <span class="step_descr"><span>Step '.$kl.'<br/></span>
									<small class="dessec">'.$des_sec['sec_step_name'].'</small>
												</span>
								</a>
							</li>';
							$kl++;
							}
						}
					 echo '</ul>';
					echo '</div>';
                echo '<div class="clearfix"></div>';
              echo '</div>';
            echo '<div class="x_content">';				
				$i=0;				
				foreach($section AS $sids)
				{
					if($sids['section_id']==$section_id)
					{					
					$st='';
					if($i!=0)
					{
						$st='display:none;';
					}
					$i++;
					
					$num='null';
				if($sids['is_many']==1)
				{
					$num=1;
				}
					echo '<div class="f_section" sec_id="'.$sids['section_id'].'" id="fs_'.$sids['section_id'].'" style="'.$st.'"> ';
					
					echo '<div class="panel-group">
								<div class="panel panel-primary">';
								  if($sids['is_optional'] && !($sids['parent_sec']))
								  {
									  echo '<div class="panel-heading"><span>'.$sids['sec_name'].'</span>';
								  echo '<span class="pull-right">
									<h6 class="del_full_sec">Delete</h6>
									</span></div>';
								  }else{
									  echo '<div class="panel-heading">'.$sids['sec_name'].'</div>';
								  }
								 echo '<div class="panel-body">';
						echo "<span>".$sids['sec_desc']."</span>";
					// if($sids['section_id']==1)
							// {			
								// echo "<p>Dear Content/Design Guru,</p>
			// <p>Welcome to the new self-appraisal platform. This is a platform to highlight your contribution towards the company over the past year and help make your growth smoother in the company and contribution for the next year. This process is designed to provide time for the employee and supervisors to look back over the past and plan realistically for the future.     </p>
			// <p>We encourage you to be proud of your accomplishments and candid about your areas of improvement. The questions which follow are intended to help you organize your thoughts and share information with your supervisor prior to receiving your performance appraisal.  </p>
			// <p>An essential goal of the appraisal meeting is that both you and your supervisors know clearly what you expect of each other and feel strongly that you can achieve your objectives by working together.</p>
			// <p>The following text will be useful guidelines if confusions arise:</p>
			// <p>- Priority order is from 1 to 5, with 5 being your highest priority.</p>
			// <p>- Weigh each option against the others to come up with your most-suited priority order.</p>
			// <p>- Short Answer:</p>
			   // <p>-- Fill the right-spelled names of people according to the details in ESP, when required</p>
			   // <p>-- Give crisp responses</p>
			// <p>- Long Answer:</p>
			  // <p>-- Write your answers in detail.</p>
			  // <p>-- When suggestions are asked for, make them point-wise and specific.</p>
							
					  // <hr>";
							// }
					
				
			$pomr_inc=1;
              foreach($sec_resp AS $sr_tbl)
					{
						echo '<div class="f_resp_section" sec_r_cd="'.$sr_tbl['sec_cd'].'" sec_cd="'.$sr_tbl['sec_code'].'" num="'.$num.'" id="fs_resp_'.$num.'"> ';
						
						if($sids['is_many']==1)
						{
						echo '<div class="panel-group">
								<div class="panel panel-danger">
								  <div class="panel-heading">
									<span><a role="button" data-toggle="collapse" href="#collapse'.$pomr_inc.'" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">  '.$sids['sec_step_name']." #".$pomr_inc.'</a>
									</span>';
									if($pomr_inc>1)
									{
								echo'<span class="pull-right">
									<h6 class="delete_sec">Delete</h6>
									</span>';
									}
							  echo '</div>
									 <div id="collapse'.$pomr_inc.'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									 <div class="panel-body">';
						}
						$ques_ll=explode(":",$sids['sec_name']);
						if($sids['sec_select']=='project')
						{
							echo '<div class="row sec_r_div" type="project_id_fk" sec_resp_id="'.$sr_tbl['sec_resp_id'].'">';
									echo '<div class="col-md-12">';
									echo "<label class='l_font_fix_4 required'>Choose ".strtolower($ques_ll[0])." for the review period.</label>";
									echo "</div>";
									echo '<div class="col-md-4">';					
									echo "<select class='selectpicker dec_select form-control' title='Nothing Selected' data-live-search='true'>";
									foreach($select_values AS $v)
									{
										print_r($v['val_id']."|".$sr_tbl['project_id_fk']);
										$ab='';
										if($v['val_id']==$sr_tbl['project_id_fk'])
										{
											$ab='selected';
										}
										echo "<option ".$ab." value='".$v['val_id']."'>".$v['val_name']."</option>";
									}
									echo "</select>";
								echo "</div>";
							echo "</div>";
							
						}
						if($sids['sec_select']=='owner')
						{
							//print_r($sr_tbl);
							echo '<div class="row sec_r_div" type="L_O_id_fk" sec_resp_id="'.$sr_tbl['sec_resp_id'].'">';
								echo '<div class="col-md-10">';
									echo "<label class='l_font_fix_4 required'>Who do you identify as your project owner for ".strtolower($ques_ll[0]).".</label>";
									echo "</div>";
									if(!($select_values))
									{
										echo '<div class="col-md-2">';
										echo '<span class="">
									<a class="btn btn_click bt_skip" yes_nav="'.$sids['yn_id'].'" no_nav="'.$sids['nn_id'].'">Skip</a></span>';
									echo '</div>';
									}	
									echo '<div class="col-md-4">';
									
										$gh12=sizeof($select_values);
									echo "<select class='selectpicker dec_select po_looo form-control' siz='".$gh12."' title='Nothing Selected' data-live-search='true'>";
									if($select_values)
									{
									foreach($select_values AS $v)
									{
										$ab='';
										if($v['val_id']==$sr_tbl['L_O_id_fk'])
										{
											$ab='selected';
										}
										echo "<option ".$ab." value='".$v['val_id']."'>".$v['val_name']."</option>";
									}
									}
									echo "</select>";
									
								echo "</div>";
													
								
								echo '</div>';
						}
						if($sids['sec_select']=='manager')
						{
							echo '<div class="row sec_r_div" type="MR_id_fk" sec_resp_id="'.$sr_tbl['sec_resp_id'].'">';
								echo '<div class="col-md-8">';
									echo "<label class='l_font_fix_4 required'>Who do you identify as your project manager for ".strtolower($ques_ll[0]).".</label>";
									echo "</div>";
									if(!($select_values))
									{
										echo '<div class="col-md-1">';
										echo '<span class="">
									<a class="btn btn_click bt_skip" yes_nav="'.$sids['yn_id'].'" no_nav="'.$sids['nn_id'].'">Skip</a></span>';
											echo '</div>';
											if($sids['section_id']!=11)
											{
									echo '<div class="col-md-3">';
										echo '<span class="">
									<a class="btn btn_click bt_skip" no_nav="'.$sids['yn_id'].'" yes_nav="'.$sids['nn_id'].'">Choose Another Project</a></span>';
											echo '</div>';
											}
									}
									$gh12=sizeof($select_values);
									echo '<div class="col-md-4">';
									echo "<select class='selectpicker dec_select po_looo form-control' siz='".$gh12."' title='Nothing Selected' data-live-search='true'>";
									if($select_values)
									{
									foreach($select_values AS $v)
									{
										$ab='';
										if($v['val_id']==$sr_tbl['MR_id_fk'])
										{
											$ab='selected';
										}
										echo "<option ".$ab." value='".$v['val_id']."'>".$v['val_name']."</option>";
									}
									}
									echo "</select>";
								echo "</div>";
								if($sids['section_id']!=5)
								{
									$mr_f='';
									if(!($sr_tbl['sec_resp_id']) && $sr_tbl['sec_select'])
										  {
											  $mr_f='display:none;';					  
										  }
								echo '<div class="col-md-6 show_samemr" style="'.$mr_f.'">';
								echo '<div class="custom-control custom-radio">
									<input class="same_mr custom-control-input" type="radio" copy_section="5" copy_sec_cd="pmr1" name="form_radio_smmr">';
								echo '<label class="custom-control-label">Same Manager Feedback as Project I</label>';
								echo '</div>';
								echo '</div>';
								}
							echo "</div>";
						}
										
				if($sids['section_id']==1)
				{
					$full_nm=$this->session->userdata('user_name');
					$tnl_id=$this->session->userdata('emp_id');
					echo '<div class="row">
									<div class="col-md-3 col-md-offset-2">
										<label class="l_font_fix_4 form-check-label">Employee Name : </label> 
										<h6 class="h6_sty">'.$full_nm.'</h6>
									</div>
								 
									<div class="col-md-3 col-md-offset-2">
										<label class="l_font_fix_4 form-check-label">TNL ID : </label> 
										<h6 class="h6_sty">'.$tnl_id.'</h6>
									</div>
								 </div>';
				  
			  }
			  
			  $df='';	
		  foreach($ques AS $qids)
		  {
			  if($sids['section_id']==$qids['section_id_fk'] && $sr_tbl['sec_cd']==$qids['sec_cd'])
			  {
				  
				  if(!($sr_tbl['sec_resp_id']) && $sr_tbl['sec_select'])
				  {
					  $df='style="display:none;"';					  
				  }
				
				echo '<div class="row cus_div" ques_id="'.$qids['question_id'].'" ques_resp_id="'.$qids['ques_resp_id'].'" '.$df.'>';
					echo '<div class="col-md-12">';
					$red="";
							if($qids['is_optional']!=1)
							{
								$red=" required";
							}
						echo "<label class='l_font_fix_4".$red."'>".$qids['ques_name']."</label>";
							echo "<div class='sub_qu'><span>".$qids['sub_ques_name']."</span></div>";
						if($qids['q_type_id_fk']==4)
						{
						if($qids['opt_code'])
						{
							foreach($opt_vals[$qids['opt_code']] AS $o_key=>$o_val)
							{
								$ck='ans_list';
								if($qids['decision']=='Y')
								{
									$ck='ans_list dec_chk';
								}
								if($qids['decision']=='Y' && (($qids['opt_code']=='type_7')|| ($qids['opt_code']=='type_8')))
								{
									$ck='ans_list dec_chk loop_chk';
								}
								if($qids['decision']=='O')
								{
									$ck='ans_list many_chk';
								}
								$sel='';
								if($qids['ques_typ_val']==$o_key)
								{
									$sel='checked';
									
								}
								echo ' <div class="custom-control custom-radio">
										<input class="'.$ck.' custom-control-input" '.$sel.' type="radio" t_val='.$o_key.' name="form_radio_'.$num."_".$sids['sec_code'].'_'.$qids['question_id'].'">';
								echo '<label class="custom-control-label">'.$o_val.'</label></div>';
							}						
						}
						}
						
						if($qids['q_type_id_fk']==3)
						{
							$a_key=array();
							echo "<table class='table table-responsive table-bordered'>";
						if($qids['opt_code'])
						{
							echo "<thead>";
							echo '<th></th>';
							foreach($opt_vals[$qids['opt_code']] AS $o_key=>$o_val)
							{
								//echo $o_key."|".$o_val;
								$a_key[]=$o_key;
								echo '<th>'.$o_val.'</th>';
							}
							echo "</thead>";				
						}
						echo "<tbody>";
						$tr=explode(",",$qids['ques_tbl_val']);
						if($qids['ques_typ_val'])
						{
						$ans_values=explode(",",$qids['ques_typ_val']);
						}else{
							$ans_values='';
						}
								$a1='1';
								$c1=0;
								foreach($tr AS $tk1=>$tr_val)
								{								
									echo "<tr class='cc_row'>";
									echo "<td class='custom-control-label' style='text-align:left;'>".$tr_val."</td>";
									$b1=1;
									foreach($a_key AS $t_val)
									{
										$rg='';
										if(is_array($ans_values))
										{
											//print_r($ans_values);
										  if($t_val==$ans_values[$c1])
										  {
											$rg='checked';  
										  }
										}
										echo '<td><input class="ans_tbl_list custom-control-input" '.$rg.' type="radio" t_val='.$t_val.' name="form_radio_tbl_'.$pomr_inc.'_'.$a1.'"></td>';
									}									
									echo "</tr>";
									$a1++;
									$c1++;
								}
						echo "</tbody>";
						echo "</table>";
						}
						if($qids['q_type_id_fk']==2)
						{
							echo '<div class="">
										<textarea  rows="5" class="form-control ans_txt_list">'.$qids['ques_resp_desc'].'</textarea>
									</div>';
						}
					echo "</div>";
				echo "</div>";
			   }
			  }
		  echo "</div>";
		  if($sids['is_many']==1)
						{
									echo "</div>";
								echo "</div>";
							echo "</div>";
						echo "</div>";
						$num++;
							$pomr_inc++;
						}
			}		
			echo '<div class="actionBar text-center">';
					if($sids['section_id']==12 || $sids['section_id']==13 || $sids['section_id']==14)
					{
					echo '<a class="btn btn_click sub_forrrm">Submit Form</a>';					
					}
					if($prev_sec_id)
					{
					echo '<span class="">
								<a class="btn btn_click btprev" yes_nav="'.$prev_sec_id.'" no_nav="'.$prev_sec_id.'">Prev</a></span>';
					}
					if($sids['yn_id'] && $sids['nn_id'])
					{
					echo '<span class="">
								<a class="btn btn_click btnext" yes_nav="'.$sids['yn_id'].'" no_nav="'.$sids['nn_id'].'">Next</a>';
					}
				echo '</div>';				
		   echo "</div>";
		}		  
	  }
		echo '</div>
			</div>
		</div>';
	  echo "</div>";
    }
	
	
	public function load_feed_ins()
    {
		
	    $file_nm            = $this->input->get('a');
		$emp            = $this->input->get('emp_id');
		$pro            = $this->input->get('pro');
		$this->session->set_userdata('fb_emp_id', $emp);
		$this->session->set_userdata('fb_pro_id', $pro);
		if($file_nm=="feed_po")
		{
			$section_id=13;
		}else{
			$section_id=14;
		}
		$u_id = $this->session->userdata('user_id');
		$slot_chosen=$this->session->userdata('slot_id');
		if(!$u_id || !$slot_chosen){
			  redirect('user/login_view');
			}
		
		$fff = $this->session->userdata('forms_list');		
		$forms =  $this->form_model->fetch_form_list($u_id,$fff);
		$section =  $this->form_model->fetch_fd_section_list($file_nm,$u_id,$emp,$pro,$slot_chosen);
		$ques =  $this->form_model->fetch_fd_ques_list($file_nm,$u_id,$emp,$pro,$slot_chosen);
		$sec_resp =  $this->form_model->fetch_fd_us_sec_list($file_nm,$u_id,$emp,$pro,$slot_chosen);
		$fetch_f_sub_list =  $this->form_model->fetch_feed_submit($file_nm,$u_id,$emp,$pro,$slot_chosen);
		
		$opts =  $this->form_model->fetch_opt_val_list($file_nm,$u_id);
		$opt_vals=array();
		foreach($opts AS $opt_id)
		{
			$opt_vals[$opt_id['opt_code']][$opt_id['opt_values']]=$opt_id['opt_name'];
			
		}
		$addclass='';
		// print_r($fetch_f_sub_list);
		if($fetch_f_sub_list)
		{
			$addclass='only_view';
			echo '<div class="x_content">';				
				$i=0;				
				foreach($section AS $sids)
				{
					if($sids['section_id']==$section_id)
					{					
					$st='';
					if($i!=0)
					{
						$st='display:none;';
					}
					$i++;
					
					$num='null';
				if($sids['is_many']==1)
				{
					$num=1;
				}
					echo '<div class="f_section" sec_id="'.$sids['section_id'].'" id="fs_'.$sids['section_id'].'" style="'.$st.'"> ';
					echo '<div class="panel-group">
								<div class="panel panel-primary">';								  
									  echo '<div class="panel-heading">'.$sids['sec_name'].'</div>';								  
								 echo '<div class="panel-body">';
						echo "<span>".$sids['sec_desc']."</span>";
					//if($sids['section_id']==1)
							{
								
								echo "<p>Please take some time to fill this form as your inputs would play a pivotal role in determining your team member's appraisal.</p>
		<p>The following text will be useful guidelines if confusions arise:</p>

<p>- Priority order is from 1 to 5, with 5 being your highest priority</p>
<p>- Weigh each option against the others to come up with your most-suited priority order</p>
<p>- Short Answer:</p>
     <p>-- Fill the right-spelled names, IDs of people according to the details in RAMCO, when required</p>
     <p>-- Give crisp responses</p>
<p>- Long Answer:</p>
     <p>-- Write your answers in detail</p>
     <p>-- When suggestions are asked for, make them point-wise and specific</p>					
					  <hr>";
							}
				
			//$pomr_inc=1;
              foreach($sec_resp AS $sr_tbl)
					{
						echo '<div class="f_resp_section" sec_r_cd="'.$sr_tbl['sec_cd'].'" sec_cd="'.$sr_tbl['sec_code'].'" num="'.$num.'" id="fs_resp_'.$num.'"> ';
							echo '<div class="row sec_r_div" type="project_id_fk" sec_resp_id="'.$sr_tbl['sec_resp_id'].'">';
									
						  
			  $df='';	
		  foreach($ques AS $qids)
		  {
			  if($sids['section_id']==$qids['section_id_fk'] && $sr_tbl['sec_cd']==$qids['sec_cd'])
			  {				
				echo '<div class="row cus_div '.$addclass.'" ques_id="'.$qids['question_id'].'" ques_resp_id="'.$qids['ques_resp_id'].'" '.$df.'>';
					echo '<div class="col-md-12">';
					$red="";
							if($qids['is_optional']!=1)
							{
								$red=" required";
							}
						echo "<label class='l_font_fix_4".$red."'>".$qids['ques_name']."</label>";
							echo "<div class='sub_qu'><span>".$qids['sub_ques_name']."</span></div>";
						if($qids['q_type_id_fk']==4)
						{
						if($qids['opt_code'])
						{
							foreach($opt_vals[$qids['opt_code']] AS $o_key=>$o_val)
							{
								 $ck='';
								// if($qids['decision']=='Y')
								// {
									// $ck='ans_list dec_chk';
								// }
								// if($qids['decision']=='O')
								// {
									// $ck='ans_list many_chk';
								// }
								$sel='';
								if($qids['ques_typ_val']==$o_key)
								{
									$sel='checked';
									
								}
								echo ' <div class="custom-control custom-radio">
										<input class="'.$ck.' custom-control-input" '.$sel.' type="radio" t_val='.$o_key.' name="form_radio_'.$num."_".$sids['sec_code'].'_'.$qids['question_id'].'">';
								echo '<label class="custom-control-label">'.$o_val.'</label></div>';
							}						
						}
						}
						
						if($qids['q_type_id_fk']==3)
						{
							$a_key=array();
							echo "<table class='table table-responsive table-bordered'>";
						if($qids['opt_code'])
						{
							echo "<thead>";
							echo '<th></th>';
							foreach($opt_vals[$qids['opt_code']] AS $o_key=>$o_val)
							{
								//echo $o_key."|".$o_val;
								$a_key[]=$o_key;
								echo '<th>'.$o_val.'</th>';
							}
							echo "</thead>";				
						}
						echo "<tbody>";
						$tr=explode(",",$qids['ques_tbl_val']);
						if($qids['ques_typ_val'])
						{
						$ans_values=explode(",",$qids['ques_typ_val']);
						}else{
							$ans_values='';
						}
								$a1='1';
								$c1=0;
								foreach($tr AS $tk1=>$tr_val)
								{								
									echo "<tr class='cc_row'>";
									echo "<td class='custom-control-label' style='text-align:left;'>".$tr_val."</td>";
									$b1=1;
									foreach($a_key AS $t_val)
									{
										$rg='';
										if(is_array($ans_values))
										{
											//print_r($ans_values);
										  if($t_val==$ans_values[$c1])
										  {
											$rg='checked';  
										  }
										}
										echo '<td><input class="custom-control-input" '.$rg.' type="radio" t_val='.$t_val.' name="form_radio_tbl_'.$a1.'"></td>';
									}									
									echo "</tr>";
									$a1++;
									$c1++;
								}
						echo "</tbody>";
						echo "</table>";
						}
						if($qids['q_type_id_fk']==2)
						{
							echo '<div class="">
										<textarea  rows="5" class="form-control ">'.$qids['ques_resp_desc'].'</textarea>
									</div>';
						}
					echo "</div>";
				echo "</div>";
			   }
			  }
		  echo "</div>";
			}			
		   echo "</div>";
		}		  
	  }
		echo '</div>
			</div>
		</div>';
	  echo "</div>";
		}else{
            echo '<div class="x_content">';				
				$i=0;				
				foreach($section AS $sids)
				{
					if($sids['section_id']==$section_id)
					{					
					$st='';
					if($i!=0)
					{
						$st='display:none;';
					}
					$i++;
					
					$num='null';
				if($sids['is_many']==1)
				{
					$num=1;
				}
					echo '<div class="f_section" sec_id="'.$sids['section_id'].'" id="fs_'.$sids['section_id'].'" style="'.$st.'"> ';
					echo '<div class="panel-group">
								<div class="panel panel-primary">';								  
								  if($sids['is_optional'] && !($sids['parent_sec']))
								  {
									  echo '<div class="panel-heading"><span>'.$sids['sec_name'].'</span>';
								  echo '<span class="pull-right">
									<h6 class="del_full_sec">Delete</h6>
									</span></div>';
								  }else{
									  echo '<div class="panel-heading">'.$sids['sec_name'].'</div>';
								  }
								 echo '<div class="panel-body">';
						echo "<span>".$sids['sec_desc']."</span>";
					//if($sids['section_id']==1)
							{
								
								echo "<p>Please take some time to fill this form as your inputs would play a pivotal role in determining your team member's appraisal.</p>
		<p>The following text will be useful guidelines if confusions arise:</p>

<p>- Priority order is from 1 to 5, with 5 being your highest priority</p>
<p>- Weigh each option against the others to come up with your most-suited priority order</p>
<p>- Short Answer:</p>
     <p>-- Fill the right-spelled names, IDs of people according to the details in RAMCO, when required</p>
     <p>-- Give crisp responses</p>
<p>- Long Answer:</p>
     <p>-- Write your answers in detail</p>
     <p>-- When suggestions are asked for, make them point-wise and specific</p>					
					  <hr>";
							}
					
				
			$pomr_inc=1;
              foreach($sec_resp AS $sr_tbl)
					{
						echo '<div class="f_resp_section" sec_r_cd="'.$sr_tbl['sec_cd'].'" sec_cd="'.$sr_tbl['sec_code'].'" num="'.$num.'" id="fs_resp_'.$num.'"> ';
							echo '<div class="row sec_r_div" type="project_id_fk" sec_resp_id="'.$sr_tbl['sec_resp_id'].'">';
									
						  
			  $df='';	
		  foreach($ques AS $qids)
		  {
			  if($sids['section_id']==$qids['section_id_fk'] && $sr_tbl['sec_cd']==$qids['sec_cd'])
			  {				
				echo '<div class="row cus_div" ques_id="'.$qids['question_id'].'" ques_resp_id="'.$qids['ques_resp_id'].'" '.$df.'>';
					echo '<div class="col-md-12">';
					$red="";
							if($qids['is_optional']!=1)
							{
								$red=" required";
							}
						echo "<label class='l_font_fix_4".$red."'>".$qids['ques_name']."</label>";
							echo "<div class='sub_qu'><span>".$qids['sub_ques_name']."</span></div>";
						if($qids['q_type_id_fk']==4)
						{
						if($qids['opt_code'])
						{
							foreach($opt_vals[$qids['opt_code']] AS $o_key=>$o_val)
							{
								$ck='ans_list';
								if($qids['decision']=='Y')
								{
									$ck='ans_list dec_chk';
								}
								if($qids['decision']=='Y' && (($qids['opt_code']=='type_7')|| ($qids['opt_code']=='type_8')))
								{
									$ck='ans_list dec_chk loop_chk';
								}
								if($qids['decision']=='O')
								{
									$ck='ans_list many_chk';
								}
								$sel='';
								if($qids['ques_typ_val']==$o_key)
								{
									$sel='checked';
									
								}
								echo ' <div class="custom-control custom-radio">
										<input class="'.$ck.' custom-control-input" '.$sel.' type="radio" t_val='.$o_key.' name="form_radio_'.$num."_".$sids['sec_code'].'_'.$qids['question_id'].'">';
								echo '<label class="custom-control-label">'.$o_val.'</label></div>';
							}						
						}
						}
						
						if($qids['q_type_id_fk']==3)
						{
							$a_key=array();
							echo "<table class='table table-responsive table-bordered'>";
						if($qids['opt_code'])
						{
							echo "<thead>";
							echo '<th></th>';
							foreach($opt_vals[$qids['opt_code']] AS $o_key=>$o_val)
							{
								//echo $o_key."|".$o_val;
								$a_key[]=$o_key;
								echo '<th>'.$o_val.'</th>';
							}
							echo "</thead>";				
						}
						echo "<tbody>";
						$tr=explode(",",$qids['ques_tbl_val']);
						if($qids['ques_typ_val'])
						{
						$ans_values=explode(",",$qids['ques_typ_val']);
						}else{
							$ans_values='';
						}
								$a1='1';
								$c1=0;
								foreach($tr AS $tk1=>$tr_val)
								{								
									echo "<tr class='cc_row'>";
									echo "<td class='custom-control-label' style='text-align:left;'>".$tr_val."</td>";
									$b1=1;
									foreach($a_key AS $t_val)
									{
										$rg='';
										if(is_array($ans_values))
										{
											//print_r($ans_values);
										  if($t_val==$ans_values[$c1])
										  {
											$rg='checked';  
										  }
										}
										echo '<td><input class="ans_tbl_list custom-control-input" '.$rg.' type="radio" t_val='.$t_val.' name="form_radio_tbl_'.$pomr_inc.'_'.$a1.'"></td>';
									}									
									echo "</tr>";
									$a1++;
									$c1++;
								}
						echo "</tbody>";
						echo "</table>";
						}
						if($qids['q_type_id_fk']==2)
						{
							echo '<div class="">
										<textarea  rows="5" class="form-control ans_txt_list">'.$qids['ques_resp_desc'].'</textarea>
									</div>';
						}
					echo "</div>";
				echo "</div>";
			   }
			  }
		  echo "</div>";
			}	

				echo '<div class="actionBar text-center">';
					if($sids['section_id']==13 || $sids['section_id']==14)
					{
					echo '<a class="btn btn_click sub_forrrm">Submit Form</a>';					
					//echo '<a class="btn btn_click save_cont">Give another Feedback</a>';					
					}
					
				echo '</div>';				
		   echo "</div>";
		}		  
	  }
		echo '</div>
			</div>
		</div>';
	  echo "</div>";
		}
    }
	
	public function load_many_pos()
    {		
	    $file_nm            = $this->input->get('a');
		$section_id            = $this->input->get('s_id');
		$num            = $this->input->get('cd');
		$this->session->set_userdata('sec_id', $section_id);
		$u_id = $this->session->userdata('user_id');
		$slot_chosen=$this->session->userdata('slot_id');
		if(!$u_id || !$slot_chosen){
			  redirect('user/login_view');
			}
		$ques =  $this->form_model->fetch_qss_list($file_nm,$u_id,$section_id,$slot_chosen);
		$sec_resp =  $this->form_model->fetch_usssec_list($file_nm,$u_id,$section_id,$slot_chosen);
		$sel_label=$sec_resp[0]['sec_select'];
		$parent_sec=$sec_resp[0]['parent_sec'];
		$select_values=array();
		if($sel_label)
		{
		$select_values=  $this->form_model->fetch_sel_val($file_nm,$u_id,$section_id,$sel_label,$parent_sec,$slot_chosen);
		}
		$opts =  $this->form_model->fetch_opt_val_list($file_nm,$u_id);
		$opt_vals=array();
		foreach($opts AS $opt_id)
		{
			$opt_vals[$opt_id['opt_code']][$opt_id['opt_values']]=$opt_id['opt_name'];
			
		}
			$pomr_inc=$num;
			//print_r($sec_resp);
              foreach($sec_resp AS $sr_tbl)
					{
						echo '<div class="f_resp_section" sec_cd="'.$sr_tbl['sec_code'].'" num="'.$num.'" id="fs_resp_'.$num.'"> ';
						 
						echo '<div class="panel-group">
								<div class="panel panel-danger">
								  <div class="panel-heading">
									<span><a role="button" data-toggle="collapse" href="#collapse'.$pomr_inc.'" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">  '.$sr_tbl['sec_step_name']." #".$pomr_inc.'</a>
									</span>';
									if($pomr_inc>1)
									{
								echo'<span class="pull-right">
									<h6 class="delete_sec">Delete</h6>
									</span>';
									}
							  echo '</div>
									 <div id="collapse'.$pomr_inc.'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									 <div class="panel-body">';
						
						$ques_ll=explode(":",$sr_tbl['sec_name']);
						if($sr_tbl['sec_select']=='project')
						{
							echo '<div class="row sec_r_div" type="project_id_fk" sec_resp_id="">';
									echo '<div class="col-md-12">';
									echo "<label class='l_font_fix_4 required'>Choose ".strtolower($ques_ll[0])." for the review period.</label>";
									echo "</div>";
									echo '<div class="col-md-4">';					
									echo "<select class='selectpicker dec_select form-control' title='Nothing Selected' data-live-search='true'>";
									foreach($select_values AS $v)
									{
										$ab='';
										if($v['val_id']==$sr_tbl['project_id_fk'])
										{
											$ab='selected';
										}
										echo "<option ".$ab." value='".$v['val_id']."'>".$v['val_name']."</option>";
									}
									echo "</select>";
								echo "</div>";
							echo "</div>";
							
						}
						if($sr_tbl['sec_select']=='owner')
						{
							//print_r($sr_tbl);
							echo '<div class="row sec_r_div" type="L_O_id_fk" sec_resp_id="">';
								echo '<div class="col-md-12">';
									echo "<label class='l_font_fix_4 required'>Who do you identify as your project owner for ".strtolower($ques_ll[0]).".</label>";
									echo "</div>";
									if(!($select_values))
									{
										echo '<div class="col-md-2">';
										echo '<span class="">
									<a class="btn btn_click bt_skip" yes_nav="'.$sr_tbl['yn_id'].'" no_nav="'.$sr_tbl['nn_id'].'">Skip</a></span>';
									echo '</div>';
									}	
									echo '<div class="col-md-4">';
									
										$gh12=sizeof($select_values);
									echo "<select class='selectpicker dec_select po_looo form-control' siz='".$gh12."' title='Nothing Selected' data-live-search='true'>";
									
									if($select_values)
									{
									foreach($select_values AS $v)
									{
										$ab='';
										if($v['val_id']==$sr_tbl['L_O_id_fk'])
										{
											$ab='selected';
										}
										echo "<option ".$ab." value='".$v['val_id']."'>".$v['val_name']."</option>";
									}
									}
									echo "</select>";									
								echo "</div>";								
							echo "</div>";							
						}
						if($sr_tbl['sec_select']=='manager')
						{
							echo '<div class="row sec_r_div" type="MR_id_fk" sec_resp_id="">';
								echo '<div class="col-md-10">';
									echo "<label class='l_font_fix_4 required'>Who do you identify as your project manager for ".strtolower($ques_ll[0]).".</label>";
									echo "</div>";
									if(!($select_values))
									{
										echo '<div class="col-md-1">';
										echo '<span class="">
									<a class="btn btn_click bt_skip" yes_nav="'.$sr_tbl['yn_id'].'" no_nav="'.$sr_tbl['nn_id'].'">Skip</a></span>';
									echo '</div>';
									
									if($sr_tbl['section_id']!=11)
											{
									echo '<div class="col-md-3">';
										echo '<span class="">
									<a class="btn btn_click bt_skip" no_nav="'.$sr_tbl['yn_id'].'" yes_nav="'.$sr_tbl['nn_id'].'">Choose Another Project</a></span>';
											echo '</div>';
											}
									}	
									$gh12=sizeof($select_values);
									echo '<div class="col-md-4">';
									echo "<select class='selectpicker dec_select po_looo form-control' siz='".$gh12."'  title='Nothing Selected' data-live-search='true'>";
									
									if($select_values)
									{
									foreach($select_values AS $v)
									{
										$ab='';
										if($v['val_id']==$sr_tbl['MR_id_fk'])
										{
											$ab='selected';
										}
										echo "<option ".$ab." value='".$v['val_id']."'>".$v['val_name']."</option>";
									}
									}
									echo "</select>";
								echo "</div>";
								if($sr_tbl['section_id']!=5)
								{
									$mr_f='';
											  $mr_f='display:none;';					  
										  
								echo '<div class="col-md-6 show_samemr" style="'.$mr_f.'">';
								echo '<div class="custom-control custom-radio">
									<input class="same_mr custom-control-input" type="radio" copy_section="5" copy_sec_cd="pmr1" name="form_radio_smmr">';
								echo '<label class="custom-control-label">Same Manager Feedback as Project I</label>';
								echo '</div>';
								echo '</div>';
								}
							echo "</div>";
							
						}
					
		  foreach($ques AS $qids)
		  {
			 // if($sr_tbl['section_id']==$qids['section_id_fk'] && $sr_tbl['sec_cd']==$qids['sec_cd'])
			  {
				
				echo '<div class="row cus_div" ques_id="'.$qids['question_id'].'" ques_resp_id="" style="display:none;">';
					echo '<div class="col-md-12">';
					$red="";
							if($qids['is_optional']!=1)
							{
								$red=" required";
							}
						echo "<label class='l_font_fix_4".$red."'>".$qids['ques_name']."</label>";
							echo "<div class='sub_qu'><span>".$qids['sub_ques_name']."</span></div>";
						if($qids['q_type_id_fk']==4)
						{
						if($qids['opt_code'])
						{
							foreach($opt_vals[$qids['opt_code']] AS $o_key=>$o_val)
							{
								$ck='ans_list';
								if($qids['decision']=='Y')
								{
									$ck='ans_list dec_chk';
								}
								if($qids['decision']=='Y' && (($qids['opt_code']=='type_7')|| ($qids['opt_code']=='type_8')))
								{
									$ck='ans_list dec_chk loop_chk';
								}
								if($qids['decision']=='O')
								{
									$ck='ans_list many_chk';
								}
								$sel='';
								if($qids['ques_typ_val']==$o_key)
								{
									$sel='checked';
									
								}
								echo ' <div class="custom-control custom-radio">
										<input class="'.$ck.' custom-control-input" '.$sel.' type="radio" t_val='.$o_key.' name="form_radio_'.$num."_".$sr_tbl['sec_code'].'_'.$qids['question_id'].'">';
								echo '<label class="custom-control-label">'.$o_val.'</label></div>';
							}						
						}
						}
						
						if($qids['q_type_id_fk']==3)
						{
							$a_key=array();
							echo "<table class='table table-responsive table-bordered'>";
						if($qids['opt_code'])
						{
							echo "<thead>";
							echo '<th></th>';
							foreach($opt_vals[$qids['opt_code']] AS $o_key=>$o_val)
							{
								//echo $o_key."|".$o_val;
								$a_key[]=$o_key;
								echo '<th>'.$o_val.'</th>';
							}
							echo "</thead>";				
						}
						echo "<tbody>";
						$tr=explode(",",$qids['ques_tbl_val']);
						if($qids['ques_typ_val'])
						{
						$ans_values=explode(",",$qids['ques_typ_val']);
						}else{
							$ans_values='';
						}
								$a1='1';
								$c1=0;
								foreach($tr AS $tk1=>$tr_val)
								{								
									echo "<tr class='cc_row'>";
									echo "<td class='custom-control-label' style='text-align:left;'>".$tr_val."</td>";
									$b1=1;
									foreach($a_key AS $t_val)
									{
										$rg='';
										if(is_array($ans_values))
										{
											//print_r($ans_values);
										  if($t_val==$ans_values[$c1])
										  {
											$rg='checked';  
										  }
										}
										echo '<td><input class="ans_tbl_list custom-control-input" '.$rg.' type="radio" t_val='.$t_val.' name="form_radio_tbl_'.$num.'_'.$a1.'"></td>';
									}									
									echo "</tr>";
									$a1++;
									$c1++;
								}
						echo "</tbody>";
						echo "</table>";
						}
						if($qids['q_type_id_fk']==2)
						{
							echo '<div class="">
										<textarea  rows="5" class="form-control ans_txt_list"></textarea>
									</div>';
						}
					echo "</div>";
				echo "</div>";
			   }
			  }
		  echo "</div>";
		  //if($sids['is_many']==1)
						{
									echo "</div>";
								echo "</div>";
							echo "</div>";
						echo "</div>";
							$pomr_inc++;
							$num++;
						}
			}
    }
	
    public function load_view_f()
    {		
	    $file_nm            = $this->input->get('a');
		$u_id = $this->session->userdata('user_id');
		$emp_id = $this->session->userdata('fb_emp_id');
		$pro_id = $this->session->userdata('fb_pro_id');
		if(!$u_id){
			redirect('user/login_view');
		}
		
		
		
		
		$fff = $this->session->userdata('forms_list');			
		$sec_init=array('self_form'=>1,'feed_po'=>13,'feed_mr'=>14);
		
			$section_id=$this->session->userdata('sec_id');	
		if(!$section_id)
		{
			$section_id=$sec_init[$file_nm];
		}
		
		$this->session->set_userdata('sec_id', $section_id);
		
		$data['slots'] =  $this->form_model->fetch_slot_list($u_id,'Yearly');
		$slot_chosen =$this->input->get('slt_id');
		$ssid_li=$this->session->userdata('slot_id');
		if(!($slot_chosen) && $ssid_li)
		{
			$slot_chosen=$ssid_li;
		}
		if(!$slot_chosen && $data['slots'])
		{
			$slot_chosen=$data['slots'][0]['s_id'];
		}
		$this->session->set_userdata('slot_id', $slot_chosen);
		$data['slot_chosen']=$slot_chosen;
		$data['forms'] =  $this->form_model->fetch_form_list($u_id,$fff);
		$fetch_f_sub_list =  $this->form_model->fetch_f_sub_list($file_nm,$u_id,$slot_chosen);
		//$fform=0;
		if($file_nm=="feed_po" || $file_nm=="feed_mr" )
		{
			$data['fetch_f_cnt'] =array('tot'=>0,'comp'=>0);
			$data["user_feeds"]=$this->form_model->fetch_feed_users($file_nm,$u_id,$slot_chosen);
			if($emp_id)
			{
			$data["pro_feeds"]=$this->form_model->load_pros($file_nm,$u_id,$emp_id,$slot_chosen);
			}else{
				$data["pro_feeds"]=array();
			}			
			$data['fetch_f_cnt'] =  $this->form_model->fetch_feed_cnt($file_nm,$u_id,$slot_chosen);
		}
		$data['access_mst'] = $this->session->userdata('access_mst');
		if($fetch_f_sub_list && $file_nm=="self_form")
		{
			$this->load->view('forms/'.$file_nm . "_view.php", $data);
		}else{
        
        $this->load->view('forms/'.$file_nm . ".php", $data);
		}
    }
	
	// public function load_view_f()
    // {
	    // $file_nm            = $this->input->get('a');
		// $section_id            = $this->input->get('s_id');
		// if(!$section_id)
		// {
			// $section_id=1;
		// }
		// // $section_cd            = $this->input->get('s_cd');
		// // if(!$section_cd)
		// // {
			// // $section_cd='gs';
		// // }
		// $date_view          = $this->input->get('date_view');
		// $date_s          = $this->input->get('s_dt');
		// $date_e          = $this->input->get('e_dt');
		// $pro_sel_val          = $this->input->get('pro');	
        // $dept_opt = $this->input->get('dept');
		// $u_id = $this->session->userdata('user_id');
		// if(!$u_id){
			// redirect('user/login_view');
		// }

		// $dept_val = $this->user_model->fetchdept_data($u_id);
		// $data["dept_val"]=$dept_val;
			// if(!$dept_opt)
			// {
				// $dept_opt=$dept_val[0]['dept_id'];
			// }
		// $data['section_id']=$section_id;
		// $data['section_cd']=$section_cd;
		// $data['dept_opt']=$dept_opt;
		// $data['forms'] =  $this->form_model->fetch_form_list($u_id);
		// $data['section'] =  $this->form_model->fetch_section_list($file_nm,$u_id);
		// $data['ques'] =  $this->form_model->fetch_ques_list($file_nm,$u_id,$section_id);
		// $data['sec_resp'] =  $this->form_model->fetch_us_sec_list($file_nm,$u_id,$section_id);
		// $sel_label=$data['sec_resp'][0]['sec_select'];
		// //print_r($data['section']);
		// if($sel_label)
		// {
		// $data['select_values']=  $this->form_model->fetch_sel_val($file_nm,$u_id,$section_id,$sel_label);
		// }
		// $opts =  $this->form_model->fetch_opt_val_list($file_nm,$u_id);
		// $opt_vals=array();
		// foreach($opts AS $opt_id)
		// {
			// $opt_vals[$opt_id['opt_code']][$opt_id['opt_values']]=$opt_id['opt_name'];
			
		// }
		// $data['opt_vals']=$opt_vals;
        // $data['access_mst'] = $this->session->userdata('access_mst');
        // if($date_view)
		// {
			// $data['date_view']  = $date_view;
			// $day               = date('Y-m-d H:i:s', strtotime($date_view));
		// }else{
        // $data['date_view'] = '';
        // $day               = date('Y-m-d H:i:s');
		// }
        // $day_w             = date('w', strtotime($day));
        // if ($day_w == 0) {
            // $day_w = 7;
        // }
		// if($file_nm == 'Floor_View')
		// {
			// $date_s='';
			// $date_e='';
		// }
		// //$this->output->enable_profiler(TRUE);
        // if($date_s && $date_e)
		// {
			// $date_start=$date_s;
			// $date_end=$date_e;
			// $data['s_dt']=$date_start;
			// $data['e_dt']=$date_end;
			
		// }else{
        // $date_start = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
        // $date_end   = date('Y-m-d', strtotime($day . ' +' . (7 - $day_w) . ' days'));
			
			
			// if ($file_nm == 'Po_Leave_Confirm') {
				// $data['s_dt']='';
				// $data['e_dt']='';
			// }
		// }
        // $this->load->view('forms/'.$file_nm . ".php", $data);
    // }
	
	public function self_response(){
        $userId = $this->input->get('us');
		$file_nm = $this->input->get('a');
		$slot_chosen =$this->input->get('slt_id');
		$ssid_li=$this->session->userdata('slot_id');
		if(!($slot_chosen) && $ssid_li)
		{
			$slot_chosen=$ssid_li;
		}
		if(!$slot_chosen && $data['slots'])
		{
			$slot_chosen=$data['slots'][0]['s_id'];
		}
		$this->session->set_userdata('slot_id', $slot_chosen);
		$u_id = $this->session->userdata('user_id');
		$data['slots'] =  $this->form_model->fetch_slot_list($u_id,'Yearly');
		
		$emp_id = $this->session->userdata('fb_emp_id');
		$pro_id = $this->session->userdata('fb_pro_id');
		
       
		if(!$u_id || !$slot_chosen){
			  redirect('user/login_view');
			}

        $fff = $this->session->userdata('forms_list');
		$data['slot_chosen']=$slot_chosen;
if($userId && $slot_chosen)
{
		$data['forms'] =  $this->form_model->fetch_form_list($u_id,$fff);
        $data['projectsData'] = $this->getAppraisalProjectResponse($userId,$slot_chosen);
        $data['AppraisalUserList']=$this->form_model->getAppraisalUserList($slot_chosen);
        $data['AppraisalUser']=$this->form_model->getAppraisalUserDetails($userId,$slot_chosen);
        $data['academicsData'] = $this->getAppraisalTeachersOnlyResponse($userId,$slot_chosen);
        $data['moreInsightData'] = $this->getAppraisalMoreInsightResponse($userId,$slot_chosen);
        $data['feedbackForEmployee'] = $this->getAppraisalFeedbackForEmployee($userId,$slot_chosen);
		$data['feedbackFromEmp'] = $this->getAppraisalFeedbackFromEmp($userId,$slot_chosen);
		//$data['feedbackFromEmp'] = array();
        $data['feedbackForPoRm'] = $this->getAppraisalFeedbackForPoRm($userId,$slot_chosen);
		$data['feedbackgiven'] = $this->getAppraisalFeedbackgiven($userId,$slot_chosen);
			$qus_nms=array();
		$opval=array();
		$opt_list = $this->form_model->fetch_opt_val_list(1,1);		
		$ques_final_comb = $this->form_model->ques_final(1,1,$slot_chosen);
		//print_r($opt_list);
		 foreach ($opt_list as $ops) {
			$opval[$ops['opt_code']][$ops['opt_values']]=$ops['opt_name'];
		 }
		 $data['option_values']=$opval;
		 
		 foreach ($ques_final_comb as $qus) {
			$qus_nms[$qus['dec_sec_cd']]=$qus['ques_name'];
		 }
		$data['ques_final_comb']=$qus_nms;
	}else{
			$data['forms'] =  $this->form_model->fetch_form_list($u_id,$fff);
        $data['projectsData'] = array();
        $data['AppraisalUserList']=$this->form_model->getAppraisalUserList($slot_chosen);
        $data['AppraisalUser']=array();
        $data['academicsData'] = array();
        $data['moreInsightData'] = array();
        $data['feedbackForEmployee'] = array();
		$data['feedbackFromEmp'] = array();
        $data['feedbackForPoRm'] = array();
		$data['feedbackgiven'] = array();
	$data['option_values']=array();
	$data['ques_final_comb']=array();
		}
        $this->load->view('forms/self_response',$data);

	}
	
	
	public function Appraisal_response(){
        $userId = $this->input->get('us');
		$file_nm = $this->input->get('a');
		$u_id = $this->session->userdata('user_id');
		$emp_id = $this->session->userdata('fb_emp_id');
		$pro_id = $this->session->userdata('fb_pro_id');

		$slot_chosen =$this->input->get('slt_id');
		$ssid_li=$this->session->userdata('slot_id');
		if(!($slot_chosen) && $ssid_li)
		{
			$slot_chosen=$ssid_li;
		}
		if(!$slot_chosen && $data['slots'])
		{
			$slot_chosen=$data['slots'][0]['s_id'];
		}
		$this->session->set_userdata('slot_id', $slot_chosen);
		$data['slots'] =  $this->form_model->fetch_slot_list($u_id,'Yearly');
		
		
		if(!$u_id || !$slot_chosen){
			  redirect('user/login_view');
			}
$data['slot_chosen']=$slot_chosen;
        $fff = $this->session->userdata('forms_list');
		// $sec_init=array('self_form'=>1,'feed_po'=>13,'feed_mr'=>14);

	    // $section_id=$this->session->userdata('sec_id');
		// if (!$section_id) {
			// $section_id=$sec_init[$file_nm];
		// }
		// $this->session->set_userdata('sec_id', $section_id);
//$this->output->enable_profiler(TRUE);
if($userId && $slot_chosen)
{
		$data['forms'] =  $this->form_model->fetch_form_list($u_id,$fff);
        $data['projectsData'] = $this->getAppraisalProjectResponse($userId,$slot_chosen);
        $data['AppraisalUserList']=$this->form_model->getAppraisalUserList($slot_chosen);
        $data['AppraisalUser']=$this->form_model->getAppraisalUserDetails($userId,$slot_chosen);
        $data['academicsData'] = $this->getAppraisalTeachersOnlyResponse($userId,$slot_chosen);
        $data['moreInsightData'] = $this->getAppraisalMoreInsightResponse($userId,$slot_chosen);
        $data['feedbackForEmployee'] = $this->getAppraisalFeedbackForEmployee($userId,$slot_chosen);
		$data['feedbackFromEmp'] = $this->getAppraisalFeedbackFromEmp($userId,$slot_chosen);
		//$data['feedbackFromEmp'] = array();
        $data['feedbackForPoRm'] = $this->getAppraisalFeedbackForPoRm($userId,$slot_chosen);
		$data['feedbackgiven'] = $this->getAppraisalFeedbackgiven($userId,$slot_chosen);
			$qus_nms=array();
		$opval=array();
		$opt_list = $this->form_model->fetch_opt_val_list(1,1);		
		$ques_final_comb = $this->form_model->ques_final(1,1,$slot_chosen);
		//print_r($opt_list);
		 foreach ($opt_list as $ops) {
			$opval[$ops['opt_code']][$ops['opt_values']]=$ops['opt_name'];
		 }
		 $data['option_values']=$opval;
		 
		 foreach ($ques_final_comb as $qus) {
			$qus_nms[$qus['dec_sec_cd']]=$qus['ques_name'];
		 }
		$data['ques_final_comb']=$qus_nms;
	}else{
			$data['forms'] =  $this->form_model->fetch_form_list($u_id,$fff);
        $data['projectsData'] = array();
        $data['AppraisalUserList']=$this->form_model->getAppraisalUserList($slot_chosen);
        $data['AppraisalUser']=array();
        $data['academicsData'] = array();
        $data['moreInsightData'] = array();
        $data['feedbackForEmployee'] = array();
		$data['feedbackFromEmp'] = array();
        $data['feedbackForPoRm'] = array();
		$data['feedbackgiven'] = array();
	$data['option_values']=array();
	$data['ques_final_comb']=array();
		}
        $this->load->view('forms/appraisal_response',$data);

	}
	
    public function getAppraisalProjectResponse($userId)
    {
        $responseData =array();
		$slot_chosen=$this->session->userdata('slot_id');
		if(!$slot_chosen){
			  redirect('user/login_view');
			}
        $projectResponseData = $this->form_model->getAppraisalProjectResponse($userId,$slot_chosen);
			if($projectResponseData)
			{
        foreach ($projectResponseData as $key => $question) {
			$responseData[$question['dec_sec_cd']][$question['sec_resp_code']][$question['sec_step_name']][trim($question['project_name'], " \t.")][trim($question['owner_or_manager_name'], " \t.")] = array($question['opt_code'],$question['ans'],$question['ques_tbl_val']);
            // $answer = !empty($question['para_ans']) ? $question['para_ans'] : $question['radio_ans'];
            // $responseData[trim($question['ques_name'], " \t.")][$question['project_name']] = $answer;
			}
			}
        return $responseData;
    }

    public function getAppraisalTeachersOnlyResponse($userId)
    {
		$slot_chosen=$this->session->userdata('slot_id');
		if(!$slot_chosen){
			  redirect('user/login_view');
			}
        $responseData =array();
        $teachersOnlyResponseData = $this->form_model->getAppraisalTeachersOnlyResponse($userId,$slot_chosen);
			if($teachersOnlyResponseData)
			{
        foreach ($teachersOnlyResponseData as $key => $question) {
            $answer = !empty($question['para_ans']) ? $question['para_ans'] : $question['radio_ans'];
            $responseData[trim($question['ques_name'], " \t.")] = $answer;
        }
			}
        return $responseData;
    }

    public function getAppraisalMoreInsightResponse($userId)
    {
		$slot_chosen=$this->session->userdata('slot_id');
		if(!$slot_chosen){
			  redirect('user/login_view');
			}
        return $this->form_model->getAppraisalMoreInsightResponse($userId,$slot_chosen);
    }

    public function getAppraisalFeedbackForEmployee($userId)
    {
        $responseData =array();
		$slot_chosen=$this->session->userdata('slot_id');
		if(!$slot_chosen){
			  redirect('user/login_view');
			}
        $feedbackForEmployeeData = $this->form_model->getAppraisalFeedbackForEmployee($userId,$slot_chosen);
        // var_dump($feedbackForEmployeeData); exit;
		if($feedbackForEmployeeData)
			{
        foreach ($feedbackForEmployeeData as $key => $question) {
			 $responseData[$question['dec_sec_cd']][$question['sec_resp_code']][$question['sec_step_name']][trim($question['project_name'], " \t.")][trim($question['owner_or_manager_name'], " \t.")] = array($question['opt_code'],$question['ans'],$question['ques_tbl_val']);
        }
			}
        return $responseData;
    }
	
	public function getAppraisalFeedbackFromEmp($userId)
    {
        $responseData =array();
$slot_chosen=$this->session->userdata('slot_id');
		if(!$slot_chosen){
			  redirect('user/login_view');
			}
        $feedbackFromEmpData = $this->form_model->getAppraisalFeedbackFromEmp($userId,$slot_chosen);
        // var_dump($feedbackForEmployeeData); exit;
		if( $feedbackFromEmpData)
		{
        foreach ($feedbackFromEmpData as $key => $question) {
			 $responseData[$question['dec_sec_cd']][$question['sec_resp_code']][$question['sec_step_name']][trim($question['project_name'], " \t.")][trim($question['owner_or_manager_name'], " \t.")] = array($question['opt_code'],$question['ans'],$question['ques_tbl_val']);
        }
		}
        return $responseData;
    }
	
	public function getAppraisalFeedbackgiven($userId)
    {
		$slot_chosen=$this->session->userdata('slot_id');
		if(!$slot_chosen){
			  redirect('user/login_view');
			}
        $responseData =array();
        $feedbackgiven = $this->form_model->getAppraisalFeedbackgiven($userId,$slot_chosen);		
		if($feedbackgiven)
		{
			foreach ($feedbackgiven as $key => $question) {
				// $answer = !empty($question['para_ans']) ? $question['para_ans'] : $question['radio_ans'];
			  $responseData[$question['dec_sec_cd']][$question['sec_resp_code']][$question['sec_step_name']][trim($question['project_name'], " \t.")][trim($question['owner_or_manager_name'], " \t.")] = array($question['opt_code'],$question['ans'],$question['ques_tbl_val']);
			}
		}
        return $responseData;
    }

	public function getAppraisalFeedbackForPoRm($userId)
    {
        $responseData =array();
$slot_chosen=$this->session->userdata('slot_id');
		if(!$slot_chosen){
			  redirect('user/login_view');
			}
        $feedbackForPoRm = $this->form_model->getAppraisalFeedbackForPoRm($userId,$slot_chosen);
		
	if($feedbackForPoRm)
	{
        foreach ($feedbackForPoRm as $key => $question) {
            // $answer = !empty($question['para_ans']) ? $question['para_ans'] : $question['radio_ans'];
          $responseData[$question['dec_sec_cd']][$question['sec_resp_code']][$question['sec_step_name']][trim($question['project_name'], " \t.")][trim($question['owner_or_manager_name'], " \t.")] = array($question['opt_code'],$question['ans'],$question['ques_tbl_val']);
        }
	}
        return $responseData;
    }
	
}
?>

