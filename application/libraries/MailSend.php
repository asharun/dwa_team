<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MailSend
{
    protected $CI;
 
    function __construct()
    {
        $this->CI =&get_instance();
		$this->CI->load->helper('url');
        $this->CI->load->model('alert_model');
		$this->CI->load->model('user_model');
        $this->CI->load->library('session');
        $this->CI->load->library("PHPMailer_Library");
    }
 
    function sendOdRequestMail($requesterData,$projectId,$reqUserId,$deptId){
        $response = false;

        $smgr=$this->CI->alert_model->getSeniorManager($projectId,2,$deptId);
		$pow=$this->CI->alert_model->getRequesterMail($reqUserId);
		$po=$this->CI->alert_model->proj_own_list($projectId,'4');
		$mail = $this->CI->phpmailer_library->load();
		$mail->setFrom('noreply@byjusdwa.com','Urgent!!!');
		$mail->Subject = 'OD request intiated';
		$mail->isHTML(true);
		if($deptId == "1"){
			//Content head mail
			$mail->addAddress("sarosh.kb@byjus.com");
		}
		if($deptId == "2"){
			//Media head mail
			$mail->addAddress("gaurav.rai@byjus.com");
		}
		foreach($smgr as $sm){
			$mail->addAddress($sm['email']);
		}

		foreach($po as $p){
			$mail->addAddress($p['email']);
		}

		if($pow){
			$mail->addCC($pow[0]['email']);
		}
		$i=1;
		$message= "";
		$message.='Hi,<br>Please find the list of employees requested for od by -"'.$pow[0]['full_name'].'"<br><br>
				<table border="1" style=" border-collapse: collapse;width: 100%;">
					<thead>
						<th style="text-align:left;padding:8px;background-color: #813588;color:white;">NO</th>
						<th style="text-align:left;padding:8px;background-color: #813588;color:white;">Employee Name</th>
						<th style="text-align:left;padding:8px;background-color: #813588;color:white;">Project Requested</th>
						<th style="text-align:left;padding:8px;background-color: #813588;color:white;">Requested Date</th>
						<th style="text-align:left;padding:8px;background-color: #813588;color:white;">Punch In</th>
						<th style="text-align:left;padding:8px;background-color: #813588;color:white;">Punch Out</th>
						<th style="text-align:left;padding:8px;background-color: #813588;color:white;">Reason</th>
					</thead>';
					foreach($requesterData as $rd){
						$message.='<tr>
									<td style="text-align:left;padding:8px;">'.$i++.'</td>
									<td style="text-align:left;padding:8px;">'.$rd['requesterName'].'</td>
									<td style="text-align:left;padding:8px;">'.$rd['project_name'].'</td>
									<td style="text-align:left;padding:8px;">'.$rd['requester_requesting_date'].'</td>
									<td style="text-align:left;padding:8px;">'.$rd['requester_punch_in_time'] .'</td>
									<td style="text-align:left;padding:8px;">'.$rd['requester_punch_out_time'].'</td>
									<td style="text-align:left;padding:8px;">'.$rd['reason'].'</td>
								</tr>';
					}
			$message.='</table><br><br>';
			$mail->Body=$message;
			$mail->Send();
			$response = true;
		return $response;
    }
}
?>