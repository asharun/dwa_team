<?php
$user_id=$this->session->userdata('user_id');
   $file_nm='Team_Member_Info';
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
} 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			
				<div class='row hid1'>	
					<div class='col-md-12'>
                        <button class='stab_stages_2' ch="Team_Member_Info">Report Card</button>
						<button class='stab_stages_2' ch="Graph_Drilldown">Pies</button>
						<button class='stab_stages_2' ch="Team_Split_Graph">Swiss Rolls</button>
						<button class='stab_stages_2 stab_dis_selec' ch="Graph_Trends">Trends</button>
						<button class='stab_stages_2' ch="Graph_Sum_Sc">Stats</button>
						<button class='stab_stages_2' ch="Employee_Info">Employee Records</button>				
						<button class='stab_stages_2' ch='Status_Report'>Activity Records</button>						
						<button class='stab_stages_2' ch="Employee_Leave">Leave Tracker</button>
						<button class='stab_stages_2' ch="WPR_Feed">Scatter</button>
					</div>
				</div>
					<?php 	
						if(($s_dt) && ($e_dt))
						{
							$start_dt = date('d-M-Y', strtotime($s_dt));
							$end_dt = date('d-M-Y', strtotime($e_dt));
						}else
						{
							$day = date('Y-m-d H:i:s');
							$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}							
							$week_end = date('Y-m-d H:i:s', strtotime($day.' +'.(7-$day_w).' days'));
							
							$week_start = date('Y-m-d H:i:s', strtotime($week_end.' -1 month'));
							$week_st_w = date('w',strtotime($week_start));
							if($week_st_w==0)
							{
								$week_st_w=7;								
							}							
							$start_dt = date('d-M-Y', strtotime($week_start.' -'.($week_st_w-1).' days'));
							$end_dt = date('d-M-Y',strtotime($week_end));
						}				
					?>
					<div class='row hid'>	
						<div class='col-md-12'>	
							
			<div class='row row_style_1 text-center'>	
							<div class='col-md-12'>					
										<button class='stab_stages_2 in_stages' ch="Graph_Trends">Log Spread</button> || 
										<button class='stab_stages_2 in_stages' ch="Trend_Report">XP Spread</button> || 
										<button class='stab_stages_2 in_stages' ch="NonQ_Share">Share of Non-Quant XP</button> || 
										<button class='stab_stages_2 stab_dis_selec' ch="Top_Bottom">Top & Bottom Share</button>
							</div>														
					</div>
					
					<hr class="st_hr2">
			<div class='row third-row head' style='margin-top: 10px;'>	
												
												<?php
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Start Date: </label>	
															<input id='t_dtpicker' class='s_dt date_fm date-picker' value='".$start_dt."' />
														</span>
													</div>
													<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>End Date: </label>	
															<input id='t_dtpicker' class='e_dt date_fm date-picker' value='".$end_dt."' />
														</span>
													</div>
													
													</div>
													<div class='row row_style_1 third-row head'>
										<div class='col-md-12 cur-month text-center'><span>Spread (".$start_dt." to ".$end_dt.")</span>
												</div>	";
												$titl1='';
												$titl2='';
																								
												?>
							</div>
							
										
									<div class='row row_style_1  scroll-head'>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Dept:</label>
															<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php
															foreach ($dept_val as $row)
															{
																$sel='';
																	if($dept_opt==$row['dept_id'])
																	{
																		$sel='selected';																		
																	}
																echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
															}
															?>
															</select>
														</div>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Project:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															$pto='';
															$sel1='';
																	
															if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($pro_sel_val==$row2['sel_1_id'])
																	{
																		$pto=$row2['sel_1_name'];
																		$sel='selected';
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Level:</label>
															<select id='sel_2345' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
															<?php 
															echo '<option data-hidden="true"></option>';
															if(!isset($level_opt))
															{
																$level_opt=1;
															}
															$lel_val=array("Contributions","Members");
															foreach ($lel_val as $key=>$value)
															{
																$sel='';
															if($level_opt==$key)
															{
																$sel='selected';																		
															}
																echo "<option value='".$key."' ".$sel.">".$value."</option>";
															}
															
																?>
															</select>
														</div>			
										</div>
			<?php
            echo "<div class='t1'>";
            
			echo "<div class='row row_style_1'>
							<div class='col-md-6'>
							<input class='ch_dt hide' value='".$date_view."' />
								<label class='l_font_fix_3'>Weekly XP Spread:-</label>
								</div>
								<div class='col-md-2 no_dt1 hide'>
								<label>No Records!</label>
							</div>
                        </div>";			
						echo "<div class='data'>";
            
						if($avg)
						{
							
							$startDateUnix=strtotime($start_dt);
							$endDateUnix=strtotime($end_dt);
							
								$currentDateUnix = $startDateUnix;

								$weekNumbers = array();
								while ($currentDateUnix < $endDateUnix) {
									$weekNumbers[] = date('Y', (strtotime('+3 days', $currentDateUnix))).date('W', $currentDateUnix);
									$currentDateUnix = strtotime('+1 week', $currentDateUnix);	
								}
	
							$titl1=$pto." - Weekly XP Spread (".$start_dt." to ".$end_dt.")";
							$titl2=$pto." - Avg XP and Number of Team Members (".$start_dt." to ".$end_dt.")";																								
							
							foreach($weekNumbers AS $inde2)
										{
										$week=substr($inde2,4,2);
										$ye=substr($inde2,0,4);
										$inde1 = "W ".$week." (".date("d/m", strtotime($ye."-W".$week."-1"))." to ".date("d/m", strtotime($ye."-W".$week."-7")).")"; 
											if(array_key_exists($inde2,$avg))
											{
												$get_avg_final[]=array("act"=>$inde1,
																		"close"=>$avg[$inde2]['Q1'],
																		"high"=>$avg[$inde2]['Max'],
																		"low"=>$avg[$inde2]['Min'],
																		"open"=>$avg[$inde2]['Q3']
																		// "Avg XP Scored per member"=>$avg[$inde2]['Avg'],
																		// "Team Contributors"=>$avg[$inde2]['Cont'],
																		// "Team Members"=>$avg[$inde2]['Mem']
																		);
											}else{
												$get_avg_final[]=array("act"=>$inde1,
																		"close"=>0,
																		"high"=>0,
																		"low"=>0,
																		"open"=>0
																		);
																		// "Team Contributors"=>0,
																		// "Avg XP Scored per member"=>0,
																		// "Team Members"=>0
																		// );
											}
										}
						}else
						{
							$avg=null;
							$get_avg_final=null;
						}
								echo "</div>";
							echo "<div class='canvas_r1 text-center'>";
							echo '<div style="width: auto; height: 400px;!important;" id="chartdiv1"></div>
			</div>
			</div>';
            
			?>		
			</div>			        
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var title_head1 = '<?php echo $titl1 ?>';</script>
	<script>var tt_data1 = <?php echo  json_encode($get_avg_final) ?>;</script> 
	<script>var title_head2 = '<?php echo $titl2 ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/serial.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>
    
	$("body").on("focus", ".s_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1,
					daysOfWeekDisabled: "0,2,3,4,5,6"
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var sdt=moment(ele).format("YYYY-MM-DD");
						var ele1=Date.parse($('.e_dt').val()); 
						var edt=moment(ele1).format("YYYY-MM-DD");
						
						if(ele && ele1 && (ele<ele1))
						{
							var str='';
						var pro_v=$("body").find("#sel_1234").val();						
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						var lvl=$("body").find("#sel_2345").val();
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}						
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
													
						window.location = base_url+"index.php/User/load_view_f?a=Top_Bottom&s_dt="+sdt+"&e_dt="+edt+str;
						}
						}
					});	
	});
	
		$("body").on("focus", ".e_dt",function(){		
		$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1,
					daysOfWeekDisabled: "1,2,3,4,5,6"
			}).on('changeDate', function(e) {
				if($(this).val())
						{
						var ele=Date.parse($('.s_dt').val()); 
						var sdt=moment(ele).format("YYYY-MM-DD");
						var ele1=Date.parse($(this).val()); 
						var edt=moment(ele1).format("YYYY-MM-DD");	
						
						if(ele && ele1 && (ele<ele1))
						{
								var str='';
						var pro_v=$("body").find("#sel_1234").val();						
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}	
						var lvl=$("body").find("#sel_2345").val();
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}						
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						window.location = base_url+"index.php/User/load_view_f?a=FV_Spread&s_dt="+sdt+"&e_dt="+edt+str;
						}
						}
					});	
	});


	$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val()); 
						var sdt=moment(ele).format("YYYY-MM-DD");
						var ele1=Date.parse($('.e_dt').val()); 
						var edt=moment(ele1).format("YYYY-MM-DD");			
						var str='';
						var pro_v=$(this).val();		
							str=str+"&pro="+pro_v;						
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						var lvl=$("body").find("#sel_2345").val();
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
						if(ele && ele1 && (ele<ele1))
						{
													
						window.location = base_url+"index.php/User/load_view_f?a=Top_Bottom&s_dt="+sdt+"&e_dt="+edt+str;
						}else
						{
							window.location = base_url+"index.php/User/load_view_f?a=Top_Bottom"+str;
						}
		}
	});
    
	$("body").on("change","#sel_2345",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val()); 
						var sdt=moment(ele).format("YYYY-MM-DD");
						var ele1=Date.parse($('.e_dt').val()); 
						var edt=moment(ele1).format("YYYY-MM-DD");			
						var str='';
						
						var pro_v=$("body").find("#sel_1234").val();							
							str=str+"&pro="+pro_v;						
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						var lvl=$(this).val();
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
						if(ele && ele1 && (ele<ele1))
						{
													
						window.location = base_url+"index.php/User/load_view_f?a=Top_Bottom&s_dt="+sdt+"&e_dt="+edt+str;
						}else
						{
							window.location = base_url+"index.php/User/load_view_f?a=Top_Bottom"+str;
						}
		}
	});
	
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Spread",dept:$(this).val()},
				success: function(response){			
					$('#sel_1234').html(response).selectpicker('refresh');
				}
			});	
		}	
	});
	
    if(tt_data1)
  {
	var chartData1 = tt_data1;
	 var chart1;
            AmCharts.ready(function () {
                chart1 = new AmCharts.AmSerialChart();
                chart1.dataProvider = chartData1;
                chart1.categoryField = "act";
				
				chart1.addTitle(title_head1);
                chart1.autoMargins = true;
                var categoryAxis1 = chart1.categoryAxis;
				categoryAxis1.autoGridCount = true;
				categoryAxis1.autoWrap=true;				
				// categoryAxis1.labelFunction= function(valueText,categoryAxis1) {
						// return 'W '+valueText.substring(5,7);
				// }	
					var graph1 = new AmCharts.AmGraph();
					graph1.id = "g1";
					graph1.balloonText = "[[category]] <br>- Min:<b>[[low]]</b><br>25 percentile:<b>[[close]]</b><br>75 percentile:<b>[[open]]</b><br>Max:<b>[[high]]</b><br>";
					graph1.closeField="close";
					graph1.fillColors = '#FF6600';
					graph1.highField="high";
					graph1.lineColor = '#FF6600';
					graph1.lineAlpha = 1;
					graph1.lowField="low";
					graph1.fillAlphas = 0.9;
					graph1.negativeFillColors="#db4c3c";
					graph1.negativeLineColor="#db4c3c";
					graph1.openField="open";			
					
				graph1.type = "candlestick";				
				graph1.valueField="close"; 
                chart1.addGraph(graph1);
			
				   var valueAxis1 = new AmCharts.ValueAxis();
				valueAxis1.precision=2;
				valueAxis1.title='XP';
                chart1.addValueAxis(valueAxis1);
				
				
				chart1.responsive = {
					  "enabled": true
					};
					chart1.write("chartdiv1");
				chart1.export = {
					  "enabled": true,
					  "fileName":title_head1,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 1
						  },
					  "exportTitles" :true,
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart1.initHC = false;
					chart1.validateNow();
            });              
			}else{
					$("body").find(".no_dt1").removeClass('hide');
					$("body").find(".canvas_r1").addClass('hide');
				}
				
 	
$(window.document).scroll(function() {
        var windowtop = $(window).scrollTop();

        $("body").find(".scroll-head").each(function(i, e) {
            $(this).removeClass("divposition");
            if (windowtop > $(e).offset().top) {
                $(e).addClass("divposition");
                return;
            }
        })
    });

	</script>
  </body>
</html>
