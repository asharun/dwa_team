<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm="Team_Member_Info";

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
  	
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
					<div class='col-md-12'>
					<button class='stab_stages_2' ch="Team_Member_Info">Report Card</button>						
						<button class='stab_stages_2' ch="Graph_Drilldown">Pies</button>
						<button class='stab_stages_2' ch="Team_Split_Graph">Swiss Rolls</button>
						<button class='stab_stages_2' ch="Graph_Trends">Trends</button>					
						<button class='stab_stages_2' ch="Graph_Sum_Sc">Stats</button>
						<button class='stab_stages_2' ch="Employee_Info">Employee Records</button>				
						<button class='stab_stages_2' ch='Status_Report'>Activity Records</button>						
						<button class='stab_stages_2 stab_dis_selec' ch="Employee_Leave">Leave Tracker</button>
						<button class='stab_stages_2' ch="WPR_Feed">Scatter</button>
					</div>
				</div>
					<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));
							
							if($day_w==0)
							{
								$day_w=7;								
							}
							$day_fm=date('d-M-Y',strtotime($day));
							 $week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));							
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));							
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));					
					?>

					<div class='row hid'>	
						<div class='col-md-12'>	
						<div class='row row_style_1 text-center'>	
							<div class='col-md-12'>					
										<button class='stab_stages_2 stab_dis_selec' ch="Employee_Leave">Weekly Tracker</button> || 
										<button class='stab_stages_2 in_stages' ch="Po_Leave_Confirm">Leave Approval</button>
							</div>														
					</div>
					<div class='row row_style_1'>
			<div class="col-md-10">	
				<fieldset class="scheduler-border">
					<legend class="legli">
						Legend
					</legend>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop em_lv_4" style=""></i>
					<span class="fil_val" style="font-size: 12px;">Present</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop em_lv_3" style=""></i>
					<span class="fil_val" style="font-size: 12px;">Week off</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop em_lv_2" style=""></i>
					<span class="fil_val" style="font-size: 12px;">Absent</span>
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop em_lv_1" style=""></i>
					<span class="fil_val" style="font-size: 12px;">Half day</span>					
					</div>

			</fieldset>				
			
			</div>
			</div>
			
			
			<div class='row third-row head'>
												
												<?php

												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Choose Date: </label>	
															<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
														</span>
													</div>
										<div class='col-md-6 cur-month text-center'><span>Break-up (".$week_st." to ".$week_end.")</span>
												</div>	";
												$titl1='';
												$titl2='';
												$titl3='';
												$titl4='';												
										?>
							</div>
				
			<div class='row row_style_1' id='c_find'>	
					<div class='col-md-12'>					
								<a class='arr2 pull-left' ch='Employee_Leave' w_val="<?= $w_prev ?>">&laquo; Prev</a>						
								<a class='arr2 pull-right' ch='Employee_Leave' w_val="<?= $w_nxt ?>">Next&raquo;</a>					
					</div>														
			</div>
			<div class='row row_style_1 scroll-head'>
				<div class='col-md-3'>
					<label class='l_font_fix_3'>Choose Dept:</label>
					<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
					<?php
					foreach ($dept_val as $row)
					{
						$sel='';
							if($dept_opt==$row['dept_id'])
							{
								$sel='selected';																		
							}
						echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
					}
					?>
					</select>
				</div>																
				<div class='col-md-3'>
					<label class='l_font_fix_3'>Choose Project:</label>
					<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
					<?php 
					echo '<option data-hidden="true"></option>';
					if($pro_sel_dta)
					{
					foreach ($pro_sel_dta as $row2)
					{
						$sel='';
							if($pro_sel_val==$row2['sel_1_id'])
							{
								$sel='selected';																		
							}
						echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
					}
					}
						?>
					</select>
				</div>
				<div class='col-md-3 show_search'>
					<label class='l_font_fix_3'>Search By Present/Absent</label>
						<input type="text" onKeyUp="search_click(event)" class="form-control " id="my_cntry" data-live-search="true">	
				</div>	
				<div class='col-md-3  show_search'>
					<label class='l_font_fix_3'>Search By Employee</label>
						<input type="text" onKeyUp="search_click(event)" class="form-control " id="my_col" data-live-search="true">		
				</div>	
				
		</div>
		<div id="puttable"></div>
	</div>	
	</div>
	</div>
	</div>
</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/pie.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>

$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Project",dept:$(this).val()},
				success: function(response){				
					$('#sel_1234').html(response).selectpicker('refresh');
				}
			});
		}	
	});

  $('body').popover({
            html: true,
            trigger: "manual"

        })
        .on("mouseenter", '[data-toggle="popover"]', function() {
            var _this = this;

            var color = $(this).css("border-left-color");
			var c1=$(this).css("background-color");

            // if (!$(this).hasClass("badge")) {
                // $(this).css("background-color", color);
                // $(this).css("color", "white");
				// $(this).attr('b_c',c1);
            // }

            $(this).popover("show");
            var pop_id = $(this).attr("aria-describedby");

            $("body").find('#' + pop_id).find(".popover-title").css("background", color);

            $("body").on("mouseleave", '.popover', function() {
                $(_this).popover('hide');
            });
        }).on("mouseleave", '[data-toggle="popover"]', function() {
            var _this = this;
            setTimeout(function() {
                if (!$("body .popover:hover").length) {
                    $(_this).popover("hide");
                }
            }, 10);
            // if (!$(this).hasClass("badge")) {
                // var color1 = $(this).attr('b_c');
                // $(this).css("background-color", color1);
                // $(this).css("color", "black");
            // }
        });

    
	$("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						var pro_v=$("body").find("#sel_1234").val();
						var dept=$("body").find("#sel_dept_1").val();
						var str='';
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						if(dept)
						{
							str=str+"&dept="+dept;
						}
	
							window.location = base_url+"index.php/User/load_view_f?a=Employee_Leave&date_view="+date_v+str;
						}
					});	
	});
	

	
	var pro_v=$("body").find("#sel_1234").val();
		if(pro_v)
		{  
			var ele=Date.parse($(".ch_dt").val()); 
			var date_v=moment(ele).format("YYYY-MM-DD");
			
						var dept=$("body").find("#sel_dept_1").val();
						var str='';						
							str=str+"&pro="+pro_v;
						if(dept)
						{
							str=str+"&dept="+dept;
						}
			$.ajax({
				  url:base_url+"index.php/Report/emp_leave",
				   type: 'post',  // http method
				  data:{date_v:date_v,pro_v:pro_v,dept:dept},
				  success: function(response){
				    $("#puttable").html(response);
					$(".show_search").show();
				  }
			});
		}
	

$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{   $(".show_search").show();
			var ele=Date.parse($(".ch_dt").val()); 
			var date_v=moment(ele).format("YYYY-MM-DD");
			var pro_v=$(this).val();
						var dept=$("body").find("#sel_dept_1").val();
						var str='';						
							str=str+"&pro="+pro_v;
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						
			window.location = base_url+"index.php/User/load_view_f?a=Employee_Leave&date_view="+date_v+str;
			// $.ajax({
				  // url:base_url+"index.php/Report/emp_leave",
				   // type: 'post',  // http method
				  // data:{date_v:date_v,pro_v:pro_v,dept:dept},
				  // success: function(response){
				    // $("body").find("#puttable").html(response);
				  // }
			// });
		}
	});

  function search_click() {
       $("body").find(".tblData").find("tr").show();
      // $("body").find(".emp-cell").removeClass("hidden-xs hidden-sm");

       var filter, filter_loc;
       if ($("body").find('#my_col').val().length == 0 && $("body").find('#my_cntry').val().length == 0 ) {
           $(".event-lt").each(function() {
               $(this).show();
           });
       }

       if ($("body").find('#my_col').val().length != 0 || $("body").find('#my_cntry').val().length != 0 ) {

           filter = $("body").find('#my_col').val().toUpperCase();
           filter_loc = $("body").find('#my_cntry').val().toUpperCase();
           
           $(".event-lt").each(function() {
               var dom_str, loc_str;
               var subst = -1,
                   subst_loc = -1;

               if (filter.length != 0) {
                   dom_str = $(this).attr('col').toUpperCase();
                   subst = (dom_str.indexOf(filter));
               }
               if (filter_loc.length != 0) {
                   loc_str = $(this).attr('loc').toUpperCase();
                   subst_loc = (loc_str.indexOf(filter_loc));
               }
               
               if (filter.length == 0) {
                   subst = 1;
               }
               if (filter_loc.length == 0) {
                   subst_loc = 1;
               }
               
if (subst >= 0 && subst_loc >= 0) {
                   $(this).show();
               } else {
                   $(this).hide();
               }

           });
       }

   };
	</script>
  </body>
</html>