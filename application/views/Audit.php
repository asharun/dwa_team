<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
  //$file_nm="Confirmation_Process";
  $file_nm=str_replace('.php','',basename(__FILE__));

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
  </head>
  <body>
 <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']='Confirmation_Process';
				$this->load->view('common/sidebar',$data);
			?>	
				
				<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			<div class="col-md-10">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>					
					</div>
			</fieldset>				
			</div>			
			</div>
			<?php
			$file_nm=str_replace('.php','',basename(__FILE__));
			echo "<div class='row hid1'>	
				<div class='col-md-12'>";
				$check=array("Audit"=>"Audit","Allocation"=>"Allocation","Review"=>"Review","Confirm"=>"Confirm");
				foreach($check as $key=>$value)
				{
					if(in_array($value,$access_str))
					{
						$sel='';
						if($value==$file_nm)
						{
							$sel='stab_dis_selec';
						}					
						echo "<button class='stab_stages ".$sel."' ch='".$value."'><span>".$key."</span> <span class='badge badge-info'>".$cnf_cnt_dta[0][$value."_cnt"]."<span></button> ";
					}
				}
			echo "<button class='stab_stages' ch='Confirmation_Process'><span>All Activities</span></button>
					</div>
			</div>";
			?>
					<div class='row hid'>	
						<div class='col-md-12'>
						<div class='row row_style_1 text-center'>
						<div class='col-md-3'>
							<label class='l_font_fix_3'>Choose Dept:</label>
							<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
							<?php
							foreach ($dept_val as $row)
							{
								$sel='';
									if($dept_opt==$row['dept_id'])
									{
										$sel='selected';																		
									}
								echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
							}
							?>
							</select>
						</div>
						</div>
						<div class='row row_style_1 text-center'>
										<div class='col-md-5'>
											<div class="row text-left">
											<?php
					$rol=$this->session->userdata('role_id');
					$select_all='false';
					$hide_stg='';
					if($rol==1 || $rol==2)
					{
							$select_all='true';
							$hide_stg='';
					}
					?>											
											<div class='col-md-3 <?= $hide_stg ?>'>
											<label class='l_font_fix_3' style='display:block;'>Push to bucket:</label>	
											<label class='l_font_fix_2'>
											<input type='radio' name='option_32' val='Review Pending' checked class='status_ch' style='vertical-align: text-bottom;'/><span> Review</span></label>
											<label class='l_font_fix_2'>
											<input type='radio' name='option_32' val='Confirmation Pending' class='status_ch' style='vertical-align: text-bottom;'/><span> Confirm</span></label>
											</div>
											<div class='col-md-9'>
											<button class='btn add_but blue_but audit_app' type='button' style='padding:6px 6px'>Approve Filled Data</button>	
											<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>	
											<button class='btn add_but org_but audit_upd' type='button' style='padding:6px 6px'>Approve Updated Data</button>
											</div>
											</div>
										</div>
										<div class='col-md-7'>
										<div class="row">
											<div class="col-md-3">
											<label class='l_font_fix_3'>Audited Qty</label>	
											<input type='number' class='au_work form-control'/>	
											</div>
											<div class='col-md-5'>
												<label class='l_font_fix_3'>Audit Desc</label>	
												<input type='text' class='au_desc form-control'/>
											</div>														
											<div class='col-md-2'>
											<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>	
											<button class='btn add_but gre_but audit_ins' type='button'>Submit Audited Work</button>
											</div>
										</div>
									</div>
									</div>
							<hr class="str_hr" style="border-top:2px solid #ddd;">
						<div id="toolbar" > 
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="all">Export All</option>
									<option value="selected">Export Selected</option>
							</select>
					</div>
						<table class="display table table-bordered table-responsive" data-checkbox-header="<?=$select_all ?>" id="table3" data-toolbar="#toolbar" data-filter-control="true" data-show-export="true"  data-search-time-out=500 data-pagination="true" data-search="true">
								<thead>
									<tr>
										  <th data-class="l_font_fix_3" data-checkbox="true" data-field="state">#</th>
										  <th data-class='l_font_fix_3 hidden resp_id' data-field="response_id">ID</th>
										  <th data-class="l_font_fix_3 hidden act_id" data-field="activity_id_fk">Act Id</th>
										  <th data-sortable="true" data-class="l_font_fix_3 act_nm" data-filter-control="input"  data-field="day_val" data-formatter="dateSortFormate">Date</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="ins_dt" data-formatter="insDateSortFormate">Inserted On</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="full_name">User</th>
										  <th data-sortable="true" data-class="l_font_fix_3 act_nm" data-filter-control="input"  data-field="activity_name">Activity Name</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="wu_name">Work Units</th>										  
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="milestone_id">MID</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="std_tgt_val">Tgt</th>
										  <th data-sortable="true" data-class="l_font_fix_4" data-field="work_comp">Work</th>
										  <th data-sortable="true" data-class="l_font_fix_4" data-field="equiv_xp">Equiv Xp</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="task_desc">Task Desc</th>
										  <th data-class="l_font_fix_5 upd_work" data-field="upd_comp">Upd Work</th>
										  <th data-sortable="true" data-class="l_font_fix_5" data-field="upd_xp">Upd Xp</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="change_desc">Desc</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="iss_cur_stat">Curr Issue Status</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="issue_cat">Issue Category</th>
										  <th data-sortable="true" data-class="l_font_fix_3"  data-field="comp_xp">Comp XP</th>
										  <th data-sortable="true" data-align="center" data-halign="center" data-class="l_font_fix_3" data-filter-control="input" data-formatter="nameFormatter" data-field="tast_stat1">Status</th>
									</tr>
								</thead>
							</table>						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>
function runningFormatter(value, row, index) {
index++; {
return index;
}
}

$('#toolbar').find('select').change(function () {
            $('#table3').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });
//Date sorting formatter
function dateSortFormate(value, row, index){
	var date_cck = new Date(value*1000);
	if(value)
	{
		return moment(date_cck).format("DD/MM/YYYY");
	}
}

function insDateSortFormate(value, row, index){
	return moment.unix(value).format('Do MMMM, YYYY HH:mm:ss A')
}

function nameFormatter(value, row) {
	     var t=value.split('|');
        return "<i class='glyphicon glyphicon-stop "+t[1]+"'></i> "+t[0];
    }
	
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			var dep_v=$(this).val();					
			window.location = base_url+"index.php/User/load_view_f?a=Audit"+"&dept="+dep_v;
		}	
	});
	
$.ajax({
       url: base_url+"index.php/User/load_conf_boots?a=Audit"+"&dept="+dep_opt,
       dataType: 'json',
       success: function(response) {
       	console.log(response);
           $('#table3').bootstrapTable({
              data: response,
			  stickyHeader: true,
			  rememberOrder:true
           });
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });
	$(".but_filter").click(function(){
		$(this).toggleClass('badge-warning');
    var column = "table ." + $(this).attr("ch");
    $(column).toggleClass('hidden');
	});
</script>
  </body>
</html>