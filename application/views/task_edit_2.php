<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
if(!$user_id){
 redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<!--link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"-->	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
		
		<div class='container sty' id='load_info' url='".BASEURL."'>

			<div class='row row_style_2'>
			<div class="col-md-4">
					<h6 class='legli' style='visibility:hidden;'>
						kdfmd
					</h6>
					<button class='btn add_but home' type='button'><i class="glyphicon glyphicon-home"></i></button>
					<?php
						$day_chk1 = date('Y-m-d H:i:s');
						$day_chk2 = date('Y-m-d');
						$day_w23 = date('w',strtotime($day_chk1));
						$day_w24 = date('w',strtotime($day_chk2));
						if($day_w23==0)
							{
								$day_w23=7;								
							}
							if($day_w24==0)
							{
								$day_w24=7;								
							}

						$week_start23 = date('Y-m-d', strtotime($day_chk1.' -'.($day_w23-1).' days'));
						$week_end23 = date('Y-m-d', strtotime($day_chk1.' +'.(8-$day_w23).' days'));
						$dty=date('Y-m-d', strtotime($date_task));
						$week_start24 = date('Y-m-d', strtotime($day_chk2.' -'.($day_w24+6).' days'));
						$week_end24 = date('Y-m-d', strtotime($day_chk2.' +'.($day_w24-1).' days'));
						// echo $dty." | ".$week_start23." | ".$week_end23."<br>";
						 // echo $day_chk2." | ".$week_start24." | ".$week_end24;
						$show=0;						
						$date_chk = new DateTime(null, new DateTimeZone('Asia/Kolkata'));
						$ch_t=$date_chk->format('H');
						
						//echo  $day_chk2." | ".$week_start24." | ".$week_end24."| ".$ch_t."<br>";
						if(($dty>=$week_start23 && $dty<=$week_end23 )||($day_chk2==$week_end24 && $ch_t<='13' && ($dty>=$week_start24 && $dty<=$week_end24)))
						{
							$show=1;
							echo "<button class='btn add_but add_task' sh='".$show."' type='button'>Add Task</button>";
						}					
					?>
					<button class='btn add_but cal_1view' type='button'><i class="glyphicon glyphicon-calendar"></i></button>
				
			</div>
			<div class="col-md-8">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>					
					</div>	
			</fieldset>				
			</div>			
			</div>	
		
			<div class='row row_style_2'>			
				<div class='col-md-6 col-md-offset-3'>
					<?php
					echo "<h4 class='ev-c1 dat_val' user=".$user_id." date_val='".$date_task."'>Task for ".date('M d,Y', strtotime($date_task))."</h4>";
					?>
				</div>
				<?php
				$day_fm=date('d-M-Y',strtotime($date_task));
				echo "<div class='col-md-3'>
					<span>
						<label class='l_font_fix_3'>Choose Date: </label>	
						<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
					</span>
				</div>";
				?>
			</div>
				
									<div class='modal fade open_col' id='show_gl_col'>										
											<div class='modal-dialog' role='document'>
												<div class='modal-content'>
												  <div class='modal-body'>	
													<div id="load_ac">
													<div class='row row_style_1'>
														<div class='col-md-3'>
														<label class='l_font_fix_3'>Choose Dept:</label>
															<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php
															foreach ($dept_val as $row)
															{
																$sel='';
																	if($dept_opt==$row['dept_id'])
																	{
																		$sel='selected';																		
																	}
																echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
															}
															?>
														</select>
														</div>
														<div class='col-md-3'>
															<label class='l_font_fix_3'>Projects</label>
															<select id='sel_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															if($sel_1)
															{
															foreach ($sel_1 as $row2)
															{
																echo "<option value='" . $row2['sel_1_id'] .  "' >" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>
														<div class='col-md-6'>
															<label class='l_font_fix_3'>Category</label>
															<select id='sel_2' class='selectpicker form-control'  title="Nothing Selected" data-live-search="true">
															</select>
														</div>	
														</div>
														<div class='row row_style_1'>
														<div class='col-md-6'>
														<label class='l_font_fix_3'>Element</label>
															<select id='sel_3' class='selectpicker form-control'  title="Nothing Selected" data-live-search="true">
															</select>
														</div>	
														<div class='col-md-6'>
														<label class='l_font_fix_3'>Action</label>
															<select id='sel_4' class='selectpicker form-control'  title="Nothing Selected" data-live-search="true">
															</select>
														</div>	
														</div>
														<div class='row row_style'>
															<div class='col-md-2 col-md-offset-5'>
																<button class='btn add_but load_tsk' type='button'>Load Activity</button>
															</div>
														</div>
														</div>
														<div id='load_tsk_info'>
														</div>														
												 </div>								  
												</div>
											  </div>
											</div>
											
											<div class='modal fade open_col' id='alloc_cont_modal'>										
											<div class='modal-dialog' role='document'>
												<div class='modal-content'>
												  <div class='modal-body'>	
													<div id="loadkkk_ac">
													<div class='row row_style_1'>
														<div class='col-md-3'>
														<label class='l_font_fix_3'>Choose Dept:</label>
															<select id='c_allc_0' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php
															foreach ($dept_val as $row)
															{
																$sel='';
																	if($dept_opt==$row['dept_id'])
																	{
																		$sel='selected';
																	}
																echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
															}
															?>
														</select>
														</div>
														<div class='col-md-3'>
															<label class='l_font_fix_3'>Projects</label>
															<select id='sel_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															if($sel_1)
															{
															foreach ($sel_1 as $row2)
															{
																echo "<option value='" . $row2['sel_1_id'] .  "' >" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>
														<div class='col-md-6'>
															<label class='l_font_fix_3'>Category</label>
															<select id='sel_2' class='selectpicker form-control'  title="Nothing Selected" data-live-search="true">
															</select>
														</div>	
														</div>
														<div class='row row_style_1'>
														<div class='col-md-6'>
														<label class='l_font_fix_3'>Element</label>
															<select id='sel_3' class='selectpicker form-control'  title="Nothing Selected" data-live-search="true">
															</select>
														</div>	
														<div class='col-md-6'>
														<label class='l_font_fix_3'>Action</label>
															<select id='sel_4' class='selectpicker form-control'  title="Nothing Selected" data-live-search="true">
															</select>
														</div>	
														</div>
														<div class='row row_style'>
															<div class='col-md-2 col-md-offset-5'>
																<button class='btn add_but load_tsk' type='button'>Load Activity</button>
															</div>
														</div>
														</div>
														<div id='load_tsk_info'>
														</div>														
												 </div>								  
												</div>
											  </div>
											</div>
											
									<div class='modal fade open_col' id='show_gl_col1'>										
											<div class='modal-dialog' role='document'>
												<div class='modal-content'>
												  <div class='modal-body' id='modal_edit'>																										
												 </div>								  
												</div>
											  </div>
									</div>	
									<div class='modal fade open_col' id='show_gl_col2'>										
											<div class='modal-dialog' role='document'>
												<div class='modal-content'>
												  <div class='modal-body' id='modal_view'>																										
												 </div>								  
												</div>
											  </div>
									</div>										
					<div class="row" style="margin-bottom: 40px;">						
						<div class="col-md-12">					
						<div class='row row_style_2'>
									<div class='col-md-1'>
										<label class='hidden-xs hidden-sm'>#</label>
									</div>
									<div class='col-md-3'>
										<label class='hidden-xs hidden-sm'>Activity</label>
									</div>
									<div class='col-md-1'>
										<label class='hidden-xs hidden-sm'>Work Units</label>
									</div>
									<div class='col-md-1'>
										<label class='hidden-xs hidden-sm'>MID</label>
									</div>									
									<div class='col-md-2'>
									<label class='hidden-xs hidden-sm'>Quantity Done</label>
									</div>
									<div class='col-md-1'>
										<label class='hidden-xs hidden-sm'>Total XP</label>
									</div>
									
									<div class='col-md-1'>
										<label class='hidden-xs hidden-sm'>Target</label>
									</div>
									<div class='col-md-1'>
										<label class='hidden-xs hidden-sm'>Status</label>
									</div>
									<div class='col-md-1'>
											<label class='hidden-xs hidden-sm'>Options</label>
									</div>
								</div>

								<hr class="st_hr1">
								<?php
								if(!($response))
								{
									echo '<div class="row row_style_3 text-center">No Records!</div>';
									
									
								}
								else{
									$i=1;
								foreach ($response as $row4)
									{
							echo "<div class='row row_style_3'>	
									<div class='col-md-1'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>#</label>
										<h6><span class='badge'>".$i."</span></h6>
									</div>
									<div class='col-md-3'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Activity</label>
										<h6> ".$row4['activity_name']."</h6>
									</div>	
									<div class='col-md-1'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Work Units</label>
										<h6>".$row4['wu_name']."</h6>
									</div>	
									<div class='col-md-1'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>MID</label>
										<h6>".$row4['milestone_id']."</h6>
									</div>										
									<div class='col-md-2'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Quantity Done</label>
										<h6>".$row4['work_comp']."</h6>
									</div>									
									<div class='col-md-1'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Total XP</label>
										<h6>".$row4['equiv_xp']."</h6>
									</div>
									<div class='col-md-1'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Target</label>
										<h6>".$row4['std_tgt_val']."</h6>
									</div>
									<div class='col-md-1'>
										<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Status</label>
										<h6 class='task_stat'><i class='glyphicon glyphicon-stop ".$row4['tast_stat']."'></i></h6>
									</div>
									<div class='col-md-1'>
											<label class='l_font_fix_3 visible-xs-inline-block visible-sm-inline-block'>Options</label>
											<h6>
											<i class='glyphicon glyphicon-edit t_edit task_modi pull-left' resp_id='".$row4['response_id']."'></i>
											<i class='glyphicon glyphicon-info-sign pull-right task_view t_edit' resp_id='".$row4['response_id']."'></i>
											</h6>
											
									</div>
								</div>
								<hr class='st_hr2'>";
								
								$i++;
									}
								}
								?>
		</div>
		
	</div>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	
	<script>var base_url = '<?php echo base_url() ?>';</script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
  <script>
  
 jQuery(document).ready(function(){	 
	 $("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						window.location = base_url+"index.php/User/task_edit?t_date="+date_v;
						}
					});			
	});
	 
	  
	 	$(".selectpicker").selectpicker();
	$("body").on("click", ".add_task",function(){
		$("#show_gl_col").modal({
            keyboard: false
        });	
	});
	var dateID=$('.dat_val').attr('date_val');
	
	$("body").on("changed.bs.select", "#sel_dept_1",function(){
		if($(this).val())
		{
				$.ajax({					
				url: base_url+"index.php/Load_dt/load_data",
				type: 'post',
				data : {param : "sel_1",sel_0_ID:$(this).val(),sel_1_ID:'',sel_2_ID :'',sel_3_ID:'',sel_4_ID:'',dateID:dateID},
				success: function(response){					
					$('#sel_1').html(response).selectpicker('refresh');
					$('#sel_2').html('').selectpicker('refresh');
					$('#sel_3').html('').selectpicker('refresh');
					$('#sel_4').html('').selectpicker('refresh');
				}
			});
		}
	});
 
  	$("body").on("changed.bs.select", "#sel_1",function(){
		if($(this).val() && $('#sel_dept_1').val())
		{
				$.ajax({					
				url: base_url+"index.php/Load_dt/load_data",
				type: 'post',
				data : {param : "sel_2",sel_0_ID:$('#sel_dept_1').val(),sel_1_ID:$(this).val(),sel_2_ID :'',sel_3_ID:'',sel_4_ID:'',dateID:dateID},
				success: function(response){					
					$('#sel_2').html(response).selectpicker('refresh');
					$('#sel_3').html('').selectpicker('refresh');
					$('#sel_4').html('').selectpicker('refresh');
				}
			});
		}
	});
	
	
	$("body").on("changed.bs.select","#sel_2",function(){
		if($(this).val() && $('#sel_dept_1').val())
		{
				$.ajax({
				url: base_url+"index.php/Load_dt/load_data",
				type: 'post',
				data : {param : "sel_3",sel_0_ID:$('#sel_dept_1').val(),sel_1_ID:$('#sel_1').val(),sel_2_ID :$(this).val(),sel_3_ID:'',sel_4_ID:'',dateID:dateID},
				success: function(response){
					$('#sel_3').html(response).selectpicker('refresh');
					$('#sel_4').html('').selectpicker('refresh');
				}
			});
		}
	});
	
	
	$("body").on("changed.bs.select","#sel_3",function(){	
		if($(this).val() && $('#sel_dept_1').val())
		{
	$.ajax({	
				url: base_url+"index.php/Load_dt/load_data",
				type: 'post',
				data : {param : "sel_4",sel_0_ID:$('#sel_dept_1').val(),sel_1_ID:$('#sel_1').val(),sel_2_ID :$('#sel_2').val(),sel_3_ID:$(this).val(),sel_4_ID:'',dateID:dateID},
				success: function(response){
					$('#sel_4').html(response).selectpicker('refresh');
				}
			});
		}
	});
	
	$("body").on("click",".task_ref",function(){
		$('#load_tsk_info').html('');
		$('#load_ac').removeClass('frozen');
	});

	$("body").on("click",".load_tsk",function(){
		var sel_0_ID=$('#sel_dept_1').val();
		var sel_1_ID=$('#sel_1').val();
		var sel_2_ID=$('#sel_2').val();
		var sel_3_ID=$('#sel_3').val();
		var sel_4_ID=$('#sel_4').val();		
		if(sel_0_ID && (sel_1_ID||sel_2_ID||sel_3_ID||sel_4_ID) && dateID)
		{
			$.ajax({	
				url: base_url+"index.php/Load_dt/load_data",
				type: 'post',
				data : {param : "load_info",sel_0_ID:sel_0_ID,sel_1_ID:sel_1_ID,sel_2_ID :sel_2_ID,sel_3_ID:sel_3_ID,sel_4_ID:sel_4_ID,dateID:dateID},
				success: function(response){
					if(response=='Error')
					{
						alert("Please enter the tasks correctly.");
					}else
					{
					$('#load_tsk_info').html(response);
					$('#mile_id').selectpicker('refresh');
					$('#load_ac').addClass('frozen');
					}
				}
			});
		}
	});  
  });
  
  // $('.click_alloc').mouseover(function() {
  // $('.text').css("visibility","visible");
// });

// $('.click_alloc').mouseout(function() {
  // $('.text').css("visibility","hidden");
// });
 		
  </script>
  </body>
</html>