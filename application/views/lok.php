<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<style>
	.half_w {
    height: 66px;
}
.form_wrap {
    width: 80%;
    /* height: 50%; */
    /* position: relative; */
    /* top: 25%; */
    margin-top: 30px;
}
.ev-c1 {
    font-weight: bold;
    color: #813588;
    /* margin-bottom: 25px; */
}
.p_box {
    border: 1px solid #ccc;
    border-radius: 0px 0px 10px 10px;
}
.top_bar_2 {
    width: 100%;
    /* background-color: #5ad033; */
    background-color: #ffffff;
    border-bottom: 4px solid #813588;
}
.pan_head {
    color: #ffffff;
    background-color: #813588;
    border-color: #777;
    border-radius: 5px!important;
}
.panel-title{
	color:white !important;
}
	</style>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="https://www.dwastaging.xyz/dwa/assets/images/tnl132.png">	
	<link rel="stylesheet" href="https://www.dwastaging.xyz/dwa/assets/css/bootstrap.min.css">

  </head>
  
<body>
	<header id="header_2">
		<!--kode top bar start-->
		<div class="top_bar_2">
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<div class="edu2_ft_logo_wrap">
							<a href="https://www.byjusdwa.com/dwa/index.php/User/home_page">
								<img class="half_w" src="https://www.dwastaging.xyz/dwa/assets/images/byjus_new.png" alt="">
								</a>
							</div>
						</div>
						<div class="col-md-8">
							<h3 class='ev-c1'>
								<center>
									<span>Work Tracker </span>
								</center>
							</h3>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
			</div>
		</header>
		<div class="container form_wrap">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="login-panel panel">
						<div class="panel-heading pan_head">
							<a href="https://www.byjusdwa.com/dwa/"><h3 class="panel-title">Click here to login</h3></a>
						</div>
						
								</div>
							</div>
						</div>
		</div>
	</body>
</html>