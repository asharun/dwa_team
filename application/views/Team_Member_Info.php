<?php
$user_id=$this->session->userdata('user_id');
  $file_nm=str_replace('.php','',basename(__FILE__));
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	
$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);
if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">	
		<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>"  type="text/css">
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2 stab_dis_selec' ch="Team_Member_Info">Report Card</button>
						<button class='stab_stages_2' ch="Graph_Drilldown">Pies</button>
						<button class='stab_stages_2' ch="Team_Split_Graph">Swiss Rolls</button>
						<button class='stab_stages_2' ch="Graph_Trends">Trends</button>
						<button class='stab_stages_2' ch="Graph_Sum_Sc">Stats</button>
						<button class='stab_stages_2' ch="Employee_Info">Employee Records</button>				
						<button class='stab_stages_2' ch='Status_Report'>Activity Records</button>						
						<button class='stab_stages_2' ch="Employee_Leave">Leave Tracker</button>
						<button class='stab_stages_2' ch="WPR_Feed">Scatter</button>
					</div>
				</div>													
				<div class='row hid'>	
						<div class='col-md-12'>	
						
						<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
						$rol=$this->session->userdata('role_id');
							$day_w = date('w',strtotime($day));
							$day_fm=date('d-M-Y',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));
							
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));
							
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));
							
							$date_start = date('Y-m-d', strtotime($day.' -'.($day_w-1).' days'));
							$date_end=date('Y-m-d', strtotime($day.' +'.(7-$day_w).' days'));
							
							$show=0;
							$spanType = !empty($timeSpanType) ? ucwords($timeSpanType) : "Week";
							if($spanType=="Week")
							{
				 $date_chk = new DateTime(null, new DateTimeZone('Asia/Kolkata'));	
				 $ch_d=$date_chk->format('Y-m-d');
							
				$day_wch  = date('w', strtotime($ch_d));
				$ch_t=$date_chk->format('H');
				
				$s_dt = date('Y-m-d', strtotime($ch_d . ' -' . ($day_wch - 1) . ' days'));
				
				if($s_dt==$ch_d && $ch_t<='23')
					{
						$curr_st = date('Y-m-d', strtotime($ch_d . ' - ' . ($day_wch + 13) . ' days'));					
					}else{
						if ($day_wch == 0) {
							$day_wch = 7;
						}						
						$curr_st = date('Y-m-d', strtotime($ch_d . ' - ' . ($day_wch + 6) . ' days'));											
					}
				
				$curr_end=date('Y-m-d', strtotime($ch_d.' +'.(7-$day_wch).' days'));
						
					//echo $curr_st.$curr_end;
					
					$is_grading=$this->session->userdata('is_grading');
					if($is_grading=="Y")
					{
					if($dept_opt==1)
					{
						if($date_start>=$curr_st && $date_end<=$curr_end)
						{							
							//if($rol==1 || $rol==2 || $rol==3|| $rol==4)
							{
								$show=1;
							}
						}
					}else{
							{
								$show=1;
							}
					}
					}
				}			
									
				?>
				<?php include_once 'WprDateFilter.php'; ?>
								<div class='row row_style_1'>
												
												<div class='col-md-4'>
													<label class='l_font_fix_3'>Choose Level:</label>
														<select id='sel_2345' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
													<?php 
													echo '<option data-hidden="true"></option>';
													if(!isset($level_opt))
													{
														$level_opt=1;
													}
													$lel_val=array("Activity","Projects","Category","Element","Action","Trade","Date","Raw Data","MID");
													foreach ($lel_val as $key=>$value)
													{
														$sel='';
															if($level_opt==$key)
															{
																$sel='selected';																		
															}
														echo "<option value='".$key."' ".$sel.">".$value."</option>";
													}
													
														?>
													</select>
													</div>															
													<div class='col-md-4'>
														<label class='l_font_fix_3'>Choose Dept:</label>
														<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
														<?php
														foreach ($dept_val as $row)
														{
															$sel='';
																if($dept_opt==$row['dept_id'])
																{
																	$sel='selected';																		
																}
															echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
														}
														?>
														</select>
													</div>
												<div class='col-md-4'>
													<label class='l_font_fix_3'>Choose Project:</label>
													<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
													<?php 
													echo '<option data-hidden="true"></option>';
													if($pro_sel_dta)
													{
														if($rol==1)
														{
															$s_lop=!($pro_sel_val)?"selected":"";
														 echo "<option value='0' ".$s_lop." >All Projects</option>";
														}
													foreach ($pro_sel_dta as $row2)
													{
														$sel='';
															if($pro_sel_val==$row2['sel_1_id'])
															{
																$sel='selected';																		
															}
														echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
													}
													}
														?>
													</select>
												</div>
								</div>
								<hr class="st_hr2">
								<div id="toolbar"> 
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="all">Export All</option>
									<option value="selected">Export Selected</option>
							</select>
							<?php 
							$feedbackFormData['user_id'] = $user_id;  
							if($level_opt==1 && $show==1)
							{
							$this->load->view('partials/feedback-modal.php', $feedbackFormData);
							}
							
							?>
								
					</div>
							<table class="display table table-bordered table-responsive" data-show-footer="true" data-footer-style="footerStyle" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table" data-search-time-out=500 data-pagination="true" data-search="true">
										<thead>
											<tr>
											
											<?php
											
											if($level_opt==1 && $show==1)
											{
											echo '<th data-class="l_font_fix_3" class="chk_all" data-class="hidden" data-checkbox="true" data-field="state">
										  	</th>';
											}
											  echo '<th data-class="l_font_fix_3" data-formatter="runningFormatter">#</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="full_name">Member Name</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="val_name">'.$lel_val[$level_opt].'</th>';
											  if($level_opt==7)
											  {		 
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="activity_name">Activity</th>';
											  }	
											  if($level_opt==0 || $level_opt==7)
											  {											  
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="wu_name">Work Units</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="w_done">Qty Done</th>';
											  }											  										  
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="proj_xp" data-footer-formatter="ProjectedXp">Projected XPs</th>';					  
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="act_xp" data-footer-formatter="ConfirmedXp">Confirmed XPs</th>';
																	
											if($level_opt==1)
											{
								echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="tot_proj_xp" data-footer-formatter="totalProjectedXp">Total Projected XPs</th>';
							 echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="tot_act_xp" data-footer-formatter="totalConfirmedXp">Total Confirmed XPs</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="in_time">Median In Time</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="esp_wrk_hrs">Avg Office Work Hours</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="sapience_on">Avg Sapience On</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="sapience_off">Avg Sapience Off</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="attendance_cnt">Attendance</th>';
											   echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="cu">Projected %CU</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="ccu">Confirmed %CU</th>';
											}
											
											if($level_opt==6)
											{
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="in_time">In Time</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="esp_wrk_hrs">Office Work Hours</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="sapience_on">Sapience On</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="sapience_off">Sapience Off</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="attendance_cnt">Attendance</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="cu">% CU</th>';
											}
											if($level_opt==1)
											{
											 echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="grade">Grade</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="grade_remarks">Remarks</th>';
											   echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="f_full_name">Feedback By</th>';
											}
											  ?>
											</tr>
										</thead>
									</table>								
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/feedback.js?v=<?= v_num() ?>"></script>
	
	<script>
	
	window.useDateFilters = '<?php echo $useDateFilters ?>';
window.teamMembersBootstrapTable = '';


	function runningFormatter(value, row, index) {
index++; {
return index;
}
}

function ConfirmedXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "<strong>Conf XP:<br> " +total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+ "</strong>";
            }
			
function totalConfirmedXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "<strong>Total Conf XP:<br> " +total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+ "</strong>";
            }
            function ProjectedXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "<strong>Proj XP:<br> " +total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+ "</strong>";
            }
			
			function redirectUser(pro_v,dept,lvl,start_date,end_date){
				var str = "";
				if(pro_v)
				{
					str=str+"&pro="+pro_v;
				}
				if(dept)
				{
					str=str+"&dept="+dept;
				}
				if(lvl)
				{
					str=str+"&lvl="+lvl;
				}
				if(start_date){
					str+='&start_date='+start_date;
				}
				if(end_date){
					str+='&end_date='+end_date;
				}	
				var redirectUrl = base_url+"index.php/User/load_view_f?a=Team_Member_Info"+str;
				window.location = redirectUrl;
			}
			
			function totalProjectedXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "<strong>Total Proj XP:<br> " +total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+ "</strong>";
            }
			
			function footerStyle(value, row, index) {
          return {
            css: { "font-weight": "bold" }
          };
        }


	$('#toolbar').find('select').change(function () {
            $('#table').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });
	
	$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var ele=Date.parse($(".ch_dt").val()); 
			var date_v=moment(ele).format("YYYY-MM-DD");
			var lvl=$("body").find("#sel_2345").val();
			var pro_v = $(this).val();
			var dept=$("body").find("#sel_dept_1").val();
				
			var start_date = moment($("#st_dt").val()).format("YYYY-MM-DD");
			var end_date =moment($("#e_dt").val()).format("YYYY-MM-DD");
			redirectUser(pro_v,dept,lvl,start_date,end_date);					
		}
	});
	
	$("body").on("change","#sel_2345",function(){
		if($(this).val())
		{
			var ele=Date.parse($(".ch_dt").val()); 
			var date_v=moment(ele).format("YYYY-MM-DD");
			var pro_v=$("body").find("#sel_1234").val();
			var dept=$("body").find("#sel_dept_1").val();
			var lvl=$(this).val();
			var start_date = moment($("#st_dt").val()).format("YYYY-MM-DD");
			var end_date =moment($("#e_dt").val()).format("YYYY-MM-DD");
			redirectUser(pro_v,dept,lvl,start_date,end_date);											
		}
	});
		var ele=Date.parse($(".ch_dt").val()); 
	var date_v=moment(ele).format("YYYY-MM-DD");
	var pr='';
	var lev=0;
	if($("#sel_1234").find("option:selected").val())
	{
		pr=$("#sel_1234").find("option:selected").val();
	}
	
	if($("#sel_2345").find("option:selected").val())
	{
		lev=$("#sel_2345").find("option:selected").val();
	}

	$(".ch_dt").datepicker({
		format: 'dd-M-yyyy',							
		yearRange: "-1:+1",
		weekStart:1
	});
	$(document).on('click', '.btn_dt_filter', function(event) {
		event.preventDefault();
		/* Act on the event */
		var pro_v=$("body").find("#sel_1234").val();
		var lvl=$("body").find("#sel_2345").val();
		var dept=$("body").find("#sel_dept_1").val();
		var start_date = moment($("#st_dt").val()).format("YYYY-MM-DD");
		var end_date =moment($("#e_dt").val()).format("YYYY-MM-DD");

		redirectUser(pro_v,dept,lvl,start_date,end_date);
		
	});

	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"ProjectTM",dept:$(this).val()},
				success: function(response){				
					$('#sel_1234').html(response).selectpicker('refresh');
				}
			});
		}	
	});
	var ajaxUrl = base_url+"index.php/User/load_table_boots?a=Team_Member_Info&date_view="+date_v+"&pro="+pr+"&lvl="+lev+"&dept="+dep_opt;
	ajaxUrl = ajaxUrl +'&start_date=' + '<?php echo $start_date; ?>' + '&end_date=' + '<?php echo $end_date; ?>' + '&time_span=' + '<?php echo $timeSpanType; ?>';
	
	
	$.ajax({
       url: ajaxUrl,
       dataType: 'json',
       success: function(response) {
		  window.weeklyBootstrapTable = $('#table').bootstrapTable({
              data: response,
			  stickyHeader: true,
			  onPostBody:function() {
			 $("body").find('.fixed-table-body').scroll(function(){
				//$("body").find('#table-sticky-header-sticky-header-container').find('thead').parent().scrollLeft($(this).scrollLeft());
				$("body").find('.fix-sticky').find('thead').parent().scrollLeft($(this).scrollLeft());
				});
			  }
        });
		
       },
       error: function(e) {
           //console.log(e.responseText);
       }
    });
	
	// JAVASCRIPT FOR FEEDBACK
	$(document).ready(function() {
		window.usersToGiveFeedback = [];
		
		
		BYJU.userFeedback.getActiveFeedbackGradesData();

		var canGiveFeedback = '<?php echo $this->user_model->canGiveFeedback(); ?>';
		
		$('#table').on('check.bs.table', function (e, row) {
			window.usersToGiveFeedback.push(row);
			BYJU.userFeedback.showFeedbackModal(canGiveFeedback);
		})
		
		$('#table').on('uncheck.bs.table', function (e, row) {
			window.usersToGiveFeedback.forEach(function(value, index, object) {
				if (value.user_id_fk == row.user_id_fk) {
					//console.log(value.user_id_fk, row.user_id_fk, index)
					object.splice(index,1);
				}
			});
			if (getBootstrapTableCheckedBoxCount('table') <= 0) {
				BYJU.userFeedback.hideFeedbackModal();
			}

		});

		$('#table').on('check-all.bs.table', function (e, row) {
			window.usersToGiveFeedback.length = 0;
       		$.each(row,function(index,value){
				window.usersToGiveFeedback.push(value)
			});
			BYJU.userFeedback.showFeedbackModal(canGiveFeedback);
			   
		});
		
		$('#table').on('uncheck-all.bs.table', function (e, name) {
			window.usersToGiveFeedback.length = 0;
			BYJU.userFeedback.hideFeedbackModal();
		})

		$('#feedbackModal').on('shown.bs.modal', function() {
			BYJU.userFeedback.createFeedbackForm();
		});

	});
	</script>
  </body>
</html>