<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;

if(!$user_id){ 
 redirect('user/login_view');
 }
 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<!--link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"-->	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
  </head>
  <body>
 <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont1">
		<div class="row ma_row">
			<div class="col-md-12 c_row">
			<?php 
			$a_right=$this->session->userdata('access'); 
			$access_str=explode("|",$a_right);
			//print_r($access_str);
			echo "<div class='row mp_row1'></div>";
			$i=3;
			$ac_arr=array("Allocations","Yearly_Form","Quarterly_Form");
			foreach($access_mst AS $a_r)
			{
				$dg[$a_r->access_name]=$a_r->icon;
				$ac_arr[$i++]=$a_r->access_name;
				$color[$a_r->access_name]=$a_r->tab_color;
				$name[$a_r->access_name]=$a_r->tab_name;
			}
			$i=0;
			echo "<div class='row mp_row'>";
		foreach($access_str AS $rw)
		{
			if(in_array($rw,$ac_arr))
			{
				if($i%4==0 && $i!=0)
				{
					echo "</div>";
					echo "<div class='row mp_row'>";
					
				}
				if($rw=="Allocations")
					{
						echo '<div class="col-md-3">
						<a href="'.base_url().'index.php/allocation/LoadAllocationOverview/syllabus/'.$allocation_dept_val.'/1">						
						<div class="mp_col" style="background-color:#813588;">
							<i class="glyphicon glyphicon-tags img_hi"></i>
							<h5>Allocation</h5>	
						</div>	
						</a>						
					</div>';						
					}else if($rw=="Yearly_Form")
					{
						echo '<div class="col-md-3">
						<a href="'.base_url().'index.php/Form/load_view_f?a=Yearly_Form">
						<div class="mp_col" style="background-color:#813588;">
							<i class="glyphicon glyphicon-paste img_hi"></i>
							<h5>Self Appraisals</h5>	
						</div>	
						</a>						
					</div>';						
					}else if($rw=="Quarterly_Form")
					{
						echo '<div class="col-md-3">
						<a href="'.base_url().'index.php/FormQ/load_view_f?a=Quarterly_Form">
						<div class="mp_col" style="background-color:#813588;">
							<i class="glyphicon glyphicon-paste img_hi"></i>
							<h5>Feedbacks</h5>	
						</div>	
						</a>						
					</div>';						
					}
					else{				
				echo "<div class='col-md-3'>";
					echo "<div class='mp_col' data_id='".str_replace(' ', '_', $rw)."' style='background-color:".$color[$rw].";'>
							<i class='glyphicon ".$dg[$rw]." img_hi'></i>
							<h5>".$name[$rw]."</h5>	
						</div>";
					echo "</div>";				
				
				}
				$i++;
			}
			}
			echo "</div>";
			
			?>
			</div>
		</div>
	</div>
</div>
  </body>
</html>