<!-- Modal -->
<div class="modal fade" id="AllocationModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <?php
            $isChapter = $level_mst[count($level_mst)-1]['level_name'] == 'Chapter' ? true : false;
            $isVideoClip = $level_mst[count($level_mst)-1]['level_name'] == 'Video Clip' ? true : false;
            $haveAccesToDept = in_array('Media', array_column($dept_val, 'dept_name'));
        ?>
        <div class="modal-body">
                <div class="container-fluid">
             	<div class="row">
                        <form id="alloctionmodal">
             		<div class="row">
             			<div class="col-sm-6">
                            <div class="row">
                     			 <div class="col-sm-5">
                                    <label class='l_font_fix_3 form-check-label' >CODE: </label>
                                    <input class='form-control mid_fix_code' readonly  name='mid_fix_code' value='<?php echo $Code; ?>'>
                                </div>
                                <div class="col-sm-7">
                                    <label class='l_font_fix_3 form-check-label levelcodename' ><?php echo $level_mst[count($level_mst)-1]['level_name'].':'; ?>   <a class="popuppattern"  title="Field Pattern" onmouseover="" style="cursor: pointer;"data-content="<?php echo 'Must have '.strlen($level_mst[count($level_mst)-1]['level_code_example']).' character-Example:'.$level_mst[count($level_mst)-1]['level_code_example'] ?>"><i class="glyphicon glyphicon-info-sign"></i></a></label>
                                    <input class='form-control' id="mid_main_code" name='mid_main_code'>
                                </div>
                 			</div>
                        </div>
             			<div class="col-sm-3">
             				<label class='l_font_fix_3 form-check-label'>NAME:</label>
             				<input class='form-control mid_name'name='mid_name' value=''>
             			</div>

                        <div class="col-md-3">
             				<label class='l_font_fix_3 form-check-label' >OWNER: </label>
    						<!-- <select id='owner_mid' class='selectpicker form-control' title='Nothing Selected' name='owner_mid' data-live-search='true'>
    							<option>Dummpy1</option>
    							<option>Dummpy2</option>
    						</select> -->
                            <select id='owner_mid' class='selectpicker form-control'  multiple='multiple'  name='owner_mid[]'  data-live-search="true">
                            <?php
                            foreach ($deptusers as $row)
                            {
                                $sel='';

                                echo "<option value='".$row['user_id']."' ".$sel.">".$row['full_name']."</option>";
                            }
                            ?>
                            </select>
             			</div>
             		</div>
             		<div class="row">
             			<div class="col-md-3">
             				<label class='l_font_fix_3 form-check-label ' >START DATE: </label>
           					<input  type='text' data-date-format='dd-mm-yyyy' id='start_dt' name='start_dt' class='form-control toary initalizedt' value='<?php echo date("d-m-Y") ?>' />
             			</div>
             			<div class="col-md-3">
             				<label class='l_font_fix_3 form-check-label' >END DATE: </label>
             				<input  type='text' data-date-format='dd-mm-yyyy' id='end_dt' name='end_dt' class='form-control toary initalizedt' value='<?php echo date("d-m-Y") ?>' />
             			</div>
                        <div class="col-md-3">
                            <label class='l_font_fix_3 form-check-label' >ESTIMATED END DATE: </label>
                            <input  type='text' data-date-format='dd-mm-yyyy' id='est_end_dt' name='est_end_dt' class='form-control toary initalizedt' value='<?php echo date("d-m-Y") ?>' />
                        </div>
             			<div class="col-md-3 invisible">
             				<label class='l_font_fix_3 form-check-label' >STATUS: </label>
             				<select id='status_nm' class='selectpicker form-control'  data-live-search='true' name='status_nm'>
    							<option value='Active'>Active</option>
    							<option value='InActive'>InActive</option>
    						</select>
             			</div>
             		</div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label class='l_font_fix_3 form-check-label' >DESCRIPTION: </label>
                           <textarea id='hier_desc'name='hier_desc' class="form-control hier_desc"></textarea>
                        </div>
                    </div>

                    <!-- CHAPTER LEVEL ADDITIOANL IFORMATION -->
                    <?php if ($isChapter && $haveAccesToDept): ?>
                        <h4><strong>Additional Info:</strong></h4>
                        <div class="row">
                            <div class="col-sm-4">
                                <label class='l_font_fix_3 form-check-label' >PRESENTER: </label>
                                <select id='presenter' class='selectpicker form-control'  multiple='multiple'  name='presenter[]'  data-live-search="true">
                                <?php
                                foreach ($deptusers as $row)
                                {
                                    $sel='';

                                    echo "<option value='".$row['user_id']."' ".$sel.">".$row['full_name']."</option>";
                                }
                                ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label class='l_font_fix_3 form-check-label' >STORYBOARD INCHARGE: </label>
                                <select id='storyboard_incharge' class='selectpicker form-control'  multiple='multiple'  name='storyboard_incharge[]'  data-live-search="true">
                                <?php
                                foreach ($deptusers as $row)
                                {
                                    $sel='';

                                    echo "<option value='".$row['user_id']."' ".$sel.">".$row['full_name']."</option>";
                                }
                                ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label class='l_font_fix_3 form-check-label' >SIGN OFF: </label>
                                <select id='sign_off' class='selectpicker form-control'  multiple='multiple'  name='sign_off[]'  data-live-search="true">
                                <?php
                                foreach ($deptusers as $row)
                                {
                                    $sel='';

                                    echo "<option value='".$row['user_id']."' ".$sel.">".$row['full_name']."</option>";
                                }
                                ?>
                                </select>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                 				<label class='l_font_fix_3 form-check-label'>MANDAYS:</label>
                 				<input class='form-control' name='mandays' id="mandays" value=''>
                 			</div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12 chapter-duration-grid-box text-center">

                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($isVideoClip && $haveAccesToDept): ?>
                        <h4><strong>Additional Info:</strong></h4>
                        <div class="row">
                            <div class="col-sm-4">
                                <label class='l_font_fix_3 form-check-label' >VIDEO CLIP TYPE: </label>
                                <select id='video_clip_type' class='form-control' name='video_clip_type'  data-live-search="true">
                                    <option value="IA">IA</option>
                                    <option value="WC">WC</option>
                                    <option value="OD">OD</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label class='l_font_fix_3 form-check-label' >VIDEO CLIP DURATION: (UNIT IN MINUTE)</label>
                                <input type="number" name="duration" id="duration" class="form-control" value="">
                            </div>
                            <div class="col-sm-4">
                                <label class='l_font_fix_3 form-check-label' >STORYBOARDED BY: </label>
                                <select id='storyboarded_by' class='selectpicker form-control'  multiple='multiple'  name='storyboarded_by[]'  data-live-search="true">
                                <?php
                                foreach ($deptusers as $row)
                                {
                                    $sel='';

                                    echo "<option value='".$row['user_id']."' ".$sel.">".$row['full_name']."</option>";
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>
                 </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" value="<?php echo $this->uri->segment(5); ?>" name="level_id_fk" id="level_id_fk">
             <input type="hidden" value="<?php echo $this->uri->segment(6); ?>" name="p_hierarchy_id" id="p_hierarchy_id">
             <input type="hidden" value="" id="hierarchyid" name="hierarchyid">
          <button type="button" class="btn btn-default add_but alloctionsave" data-dismiss="modal">Submit</button>
          <button type="button" class="btn btn-default add_but alloctionupdate" data-dismiss="modal">Update</button>

        </div>
    </form>
      </div>
    </div>
  </div>
</div>

<!-- Update level attribute csv modal -->
<div class='modal fade' id='csv_upload_allocation'>
    <div class='modal-dialog' role='document'>
        <div class='modal-content'>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Attributes Update</h4>
             </div>
            <div class='modal-body' id='modal_edit'>
                <form class='form-horizontal'  id='frm_allocation' enctype="multipart/form-data" method="post">
                    <div class='row row_style_1'>
                        <div class='col-md-4'>
                            <label class='l_font_fix_3'>Choose Dept:</label>
                            <select id='department' class='selectpicker form-control' title="Nothing Selected" data-live-search="true" required="true" name="department">
                                <?php
                                foreach ($dept_val as $row)
                                {
                                    $sel='';
                                        if($dept_opt==$row['dept_id'])
                                        {
                                            $sel='selected';
                                        }
                                    echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class='col-md-8'>
                            <label class='l_font_fix_3'>Select a file to upload</label>
                            <input type = "file" id="level_file" name ="level_file" />
                        </div>
                    </div>
                    <div class='row row_style_1'>
                        <!--input class='btn add_but act_csv_sub' id='submit' type='button' value="Upload"-->
                        <div class="col-md-2">
                            <input class='btn add_but btn_lvl_csv' id='btn_lvl_csv' type='submit' value="Upload">
                        </div>
                        <div class="col-md-3">
                            <a href="<?php echo base_url('uploads/allocation/levelMaster.csv') ?>" title="Download csv template" class="btn add_but" download>Download csv template</a>
                        </div>
                    </div>
                </form>
                <div class='row row_style_1'>
                <div class='col-md-12 file_errors'>

                </div>
                </div>
            </div>
        </div>
     </div>
</div>

<!-- Allocate activities to user -->
<div class='modal fade' id='mdl_activity_allocate'>
    <div class='modal-dialog' role='document'>
        <div class='modal-content'>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Activity Allocation</h4>
             </div>
            <div class='modal-body' id='modal_edit'>
                <form class='form-horizontal'  id='frm_act_allocate' enctype="multipart/form-data" method="post">
                    <div class='row row_style_1'>
                        <div class='col-md-4'>
                            <label class='l_font_fix_3'>Choose Dept:</label>
                            <select id='dept_allocation' class='selectpicker form-control' title="Nothing Selected" data-live-search="true" required="true" name="department">
                                <?php
                                foreach ($dept_val as $row)
                                {
                                    $sel='';
                                        if($dept_opt==$row['dept_id'])
                                        {
                                            $sel='selected';
                                        }
                                    echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class='col-md-8'>
                            <label class='l_font_fix_3'>Select a file to upload</label>
                            <input type = "file" id="activity_file" name ="activity_file" />
                        </div>
                    </div>
                    <div class='row row_style_1'>
                        <!--input class='btn add_but act_csv_sub' id='submit' type='button' value="Upload"-->
                        <div class="col-md-2">
                            <input class='btn add_but btn_act_upload' id='btn_act_upload' type='submit' value="Upload">
                        </div>

                        <div class="col-md-2">
                          <a href="<?php echo base_url('uploads/allocation/activityUpdateMaster.csv') ?>" title="Download activity csv template" class="btn add_but" download>
                            Download csv template
                          </a>
                        </div>
                    </div>
                </form>
                <div class='row row_style_1'>
                <div class='col-md-12 file_errors'>

                </div>
                </div>
            </div>
        </div>
     </div>
</div>

<input type="hidden" id="levelcode" value=''>
<!-- Modal -->
    <script>var base_url = '<?php echo base_url() ?>';</script>
    <script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/loadingoverlay.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
    <script type="text/javascript" src="<?= getAssestsUrl() ?>js/custom_allocation.js?v=<?= v_num() ?>"></script>
     <!-- <script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery-ui.js" ></script> -->


	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>

	 <script src="<?= getAssestsUrl() ?>js/select2.min.js"></script>
   <script type="text/javascript" src="<?= getAssestsUrl() ?>dx.custom.js"></script>
    <!-- <script type="text/javascript" src="<?= getAssestsUrl() ?>js/dx.js?v=<?= v_num() ?>"></script> -->
    <script type="text/javascript" src="<?= getAssestsUrl() ?>js/allocations/allocation-details.js?v=<?= v_num() ?>"></script>
    <script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
    <!-- <script type="text/javascript" src="<?= getAssestsUrl() ?>js/dhtmlxgantt.js?v=<?= v_num() ?>"></script> -->

    <script type="text/javascript" src="<?= getAssestsUrl() ?>dist/dhtmlxgantt.js"></script>
    <script type="text/javascript" src="<?= getAssestsUrl() ?>js/dhtmlxgantt_tooltip.js?v=<?= v_num() ?>"></script>

    <!-- <script type="text/javascript" src="<?= getAssestsUrl() ?>dist/app.min.js"></script> -->
    <script type="text/javascript" src="<?= getAssestsUrl() ?>dist/vue.js"></script>
    <script type="text/javascript" src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
     <script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.cookie.min.js"></script>
<!--script>
$(document).ready(function() {
	
	$("body").on("focus", ".s_dt",function(){		
	$(this).datepicker({
					format: 'dd-mm-yyyy',
					yearRange: "-1:+1",
					weekStart:1
			});
	});
	$("body").on("focus", ".e_dt",function(){		
	$(this).datepicker({
					format: 'dd-mm-yyyy',
					yearRange: "-1:+1",
					weekStart:1
			});
	});
});
	</script-->
 </body>
</html>