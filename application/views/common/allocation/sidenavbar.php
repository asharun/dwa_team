<nav class="navbar navbar-inverse sidebar" role="navigation">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<span><a class="navbar-brand" href="<?= base_url().'index.php/User/home_page' ?>"><i class="glyphicon glyphicon-home h_imh"></i> Home</a><span>
			<span id="togglenavopen" class="glyphicon glyphicon-arrow-left togglenav" style="font-size:30px;margin-top: 7px;margin-left: 26px;;color:#dccdcd"></span><span
			id="togglenavclose"class="glyphicon glyphicon-arrow-right togglenav hide" style="font-size:30px;margin-top: 7px;margin-left: 26px;;color:#dccdcd"></span>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
			<ul class="nav navbar-nav">
				 <?php
				$dept_id=$this->uri->segment(4);
				$parent_id=$this->uri->segment(6);
				$levelid=$this->uri->segment(5);
				 $act='';
				  foreach($level_mst as $lvlmst){
				 			$class='selectednav';
							if($lvlmst['level_id']<$this->uri->segment(5))
							{
								$class='drillup';

							}else{
								if($lvlmst['level_id']==$this->uri->segment(5)){
				 				$act='active';
								}
							}

				 	?>
				<li class="<?php echo $class." ".$act; ?>" style="width: 197px"   data-levelname="<?php echo $lvlmst['level_name'];?>"
						 	data-levelid="<?php echo $lvlmst['level_id'];?>"
						 	data-parentid="<?php echo $parent_id;?>"
						 	><a href="javascript:void(0)"><?php echo $lvlmst['level_name']; ?><span style="font-size:16px" class="pull-right hidden-xs showopacity glyphicon <?php echo $lvlmst['icon_name']; ?>"></span></a></li>

				<?php  }  ?>

			</ul>
		</div>
	</div>
</nav>