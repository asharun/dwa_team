<div class="container-fluid">
	    <nav class="navbar navbar" style="margin-top: -9px;margin-bottom: 1px;">
	        <!-- Brand and toggle get grouped for better mobile display -->
	        <div class="navbar-header">
	            <button type="button"style="background-color:black" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
	                <span class="sr-only" >Toggle navigation</span>
	                <span class="icon-bar"style="background-color:white"></span>
	                <span class="icon-bar"style="background-color:white"></span>
	                <span class="icon-bar"style="background-color:white"></span>
	            </button>

	        </div>
	        <!-- Collection of nav links, forms, and other content for toggling -->
	        <?php
	        	$tabName = $this->uri->segment(2);
				$levelCode = $this->uri->segment(3);
				$deptId = $this->uri->segment(4);
				$levelId = $this->uri->segment(5);
				$hierarchyId = $this->uri->segment(6);
				$search_val = $this->input->get('mid', TRUE);
				$search_q_str = $search_val ? "?mid=$search_val" : '';
	        ?>
	        <div id="navbarCollapse" class="collapse navbar-collapse">
	            <ul class="nav navbar-nav ">
	                <li class="">
	                <a class="overview-tab <?php if($tabName == 'LoadAllocationOverview' || $tabName == 'LoadByNavigation'){
	                	echo 'activeclass'; } ?>" href="<?php echo base_url().'index.php/allocation/LoadAllocationOverview/'.$levelCode.'/'.$deptId.'/'.$levelId.'/'.$hierarchyId.$search_q_str ?>">OVERVIEW</a></li>
						<li><a class="details-tab <?php echo ($tabName == 'getDetailsPage') ? 'activeclass' : '' ?>" href="<?php echo base_url().'index.php/allocation/getDetailsPage/'.$levelCode.'/'.$deptId.'/'.$levelId.'/'.$hierarchyId.$search_q_str ?>">DETAILS</a></li>
						<!-- <li><a class="details-mid-tab <?php echo ($tabName == 'getDetailsMidPage') ? 'activeclass' : '' ?>" href="<?php echo base_url().'index.php/allocation/getDetailsMidPage/'.$levelCode.'/'.$deptId.'/'.$levelId.'/'.$hierarchyId.$search_q_str ?>">MID OVERVIEW</a></li> -->
						<li><a class="summary-resource-tab <?php echo ($tabName == 'getActivityLogData') ? 'activeclass' : '' ?>" href="<?php echo base_url().'index.php/allocation/getActivityLogData/'.$levelCode.'/'.$deptId.'/'.$levelId.'/'.$hierarchyId.$search_q_str ?>">ACTUALS</a></li>
						<li><a class="summary-tab <?php echo ($tabName == 'getSummaryPage') ? 'activeclass' : '' ?>" href="<?php echo base_url().'index.php/allocation/getSummaryPage/'.$levelCode.'/'.$deptId.'/'.$levelId.'/'.$hierarchyId.$search_q_str ?>">SCHEDULE</a></li>
						<li><a class="summary-resource-tab <?php echo ($tabName == 'getSummaryResourcePage') ? 'activeclass' : '' ?>" href="<?php echo base_url().'index.php/allocation/getSummaryResourcePage/'.$levelCode.'/'.$deptId.'/'.$levelId.'/'.$hierarchyId.$search_q_str ?>">RESOURCE
						</a></li>
						<!-- <li><a class="summary-resource-tab <?php echo ($tabName == 'getActivityLogData') ? 'activeclass' : '' ?>" href="<?php echo base_url().'index.php/allocation/getActivityLogData/'.$levelCode.'/'.$deptId.'/'.$levelId.'/'.$hierarchyId.$search_q_str ?>">Actuals</a></li> -->





	                <!--li><a href="#">TASK</a></li>
	                <li class="dropdown">
	                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">SCHEDULE <b class="caret"></b></a>
	                    <ul class="dropdown-menu">
	                        <li><a href="#">Inbox</a></li>
	                        <li><a href="#">Drafts</a></li>
	                        <li><a href="#">Sent Items</a></li>
	                        <li class="divider"></li>
	                        <li><a href="#">Trash</a></li>
	                    </ul>
	                </li-->
	            </ul>
	            <div class="pull-right col-md-3">
	            	<label class='l_font_fix_3'>Go to MID :</label>
					<form id="frm_mid_overview" action="">
						<div class="input-group">
							<input type="search" class="form-control" value="<?php echo $search_val ?>" id="mid_code_search" name="mid_code_search">
							<span class="input-group-btn">
								<button class="btn add_but" type="submit" id="search_overview">
									<span class="glyphicon glyphicon-search" aria-hidden="true">
									</span>
								</button>
							</span>
						</div>
					</form>
				</div>

	            <!--ul class="nav navbar-nav navbar-right">

	            	 <li><a href="#"><i class="glyphicon glyphicon-th-list"></i></a></li>
	                <li><a href="#"><i class="glyphicon glyphicon-filter"></i></a></li>
	            </ul-->
	        </div>
	    </nav>
	</div>
	<div class="col-md-12 c_row">
		<div class='row hid1'>
			<hr class="str_hr" style="border-top:2px solid #ddd;">
			<div class="row" id="app">
				<?php if ($tabName == 'getDetailsMidPage'): ?>
					<a href="<?php echo current_url(); ?>?exportSelector=true" class="btn btn-default pull-right allocation-export-btn"><i class="glyphicon glyphicon-export"></i></a>
				<?php endif; ?>


     			<div class='col-md-2' style="border: ">
							<label class='l_font_fix_3'>Choose Dept:</label>

							<select id='sel_dept_1' class='selectpicker form-control sel_dept_1' data-live-search="true" data-tab-name="<?php echo $tabName; ?>">
							<?php

							foreach ($dept_val as $row)
							{

								$sel='';
									if($row['dept_id']==$this->uri->segment(4))
									{
										$sel='selected';
									}
								echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
							}
							?>
							</select>
					</div>

					<?php if ($tabName == 'getSummaryResourcePage'): ?>

					<div class="col-md-3">
						<label class='l_font_fix_3'>Choose Member:</label>

						<select id='syllabus_owner' class='selectpicker form-control' name="allocated_to[]" multiple="multiple" title='Nothing Selected' data-live-search='true'>
                            <?php foreach ($usersList as $user): ?>
                              <option value="<?php echo $user['user_id'] ?>"><?php echo $user['full_name'] ?></option>
                            <?php endforeach; ?>
          				</select>
					</div>
					<div class='col-md-2'>
						<span>
						<?php
						$startDate1 = !empty($data['startDate']) ? date("d-m-Y",strtotime($data['startDate'])) : date("d-m-Y",strtotime('-1 week'));
						$endDate1 = !empty($data['endDate']) ? date("d-m-Y",strtotime($data['endDate'])) : date("d-m-Y",strtotime('+1 week', strtotime(date('Y-m-d'))));
						?>
							<label class='l_font_fix_3'>Start Date: </label>

								<input id='t_dtpicker1' class='s_dt date_fm date-picker' data-date-format="dd-mm-yyyy" value='<?php echo $startDate1 ?>' />

						</span>
					</div>
					<div class='col-md-2'>
						<span>
							<label class='l_font_fix_3'>End Date: </label>
								<input id='t_dtpicker2' class='e_dt date-picker date_fm' data-date-format="dd-mm-yyyy" value='<?php echo $endDate1 ?>' />

						</span>
					</div>
					<div class='col-md-2' style='padding:20px;'>
						<span>
							<button class='btn add_but gre_but' id="sch-res-filter-btn" type='button' >Submit</button>
						</span>
					</div>
				<?php endif; ?>


					<?php if ($tabName == 'LoadAllocationOverview'): ?>
						<div class='col-md-10'>
							<div class="col-md-4" style="margin-top:23px;">
								<button class="btn add_but blue_but btn_up_allocation" type="button">Upload Attributes Update CSV</button>
							</div>
							<div class="col-md-4" style="margin-top:23px;">
								<button class="btn add_but blue_but btn_alct_act" type="button">Upload Activity Allocation CSV</button>
							</div>

						</div>

					<?php endif; ?>
					<?php if ($tabName == 'getDetailsPage'): ?>

						<!-- <div class="col-md-2" style="padding-top: 26px">
							<button type="button" name="button" class="btn btn-default pull-right bulk-upload-btn">Bulk Allocate</button>

						</div> -->
						<div class="col-md-1" style="padding-top: 26px">
							<button type="button" name="button" class="btn btn-default pull-right bulk-alloc-import-btn">Export</button>
						</div>
						<div class="col-md-7" style="padding-top:12px">
							<fieldset class="scheduler-border">
								<legend class='legli'>
									Legend
								</legend>
								<?php foreach ($allLevelData as $key => $level): ?>
									<div class="col-md-3 col_sp">
										<i class="glyphicon glyphicon-stop level-legend" style="--color: <?php echo $level['color_code'] ?>; --background: <?php echo $level['color_code'] ?>"></i>
										<span class="fil_val" style="font-size: 12px;"><?php echo $level['level_name']; ?></span>
									</div>
								<?php endforeach; ?>
							</fieldset>
						</div>
				<?php endif; ?>
				<?php if ($tabName == 'getSummaryPage'):  ?>
					<div class='col-md-2'>
						<span>
							<label class='l_font_fix_3'>Start Date: </label>
							<input id='t_dtpicker1' class='s_dt date_fm date-picker' data-date-format="dd-mm-yyyy" value='<?php echo date("d-m-Y") ?>' />
						</span>
					</div>
					<div class='col-md-2'>
						<span>
							<label class='l_font_fix_3'>End Date: </label>
							<input id='t_dtpicker2' class='e_dt date-picker date_fm' data-date-format="dd-mm-yyyy" value='<?php echo date("d-m-Y",strtotime("+10 days")) ?>' />
						</span>
					</div>
					<div class="col-md-2">
						<label class='l_font_fix_3'>Choose Time Scale:</label>
						<?php echo form_dropdown('time_filter', ['week' => 'Weekly','month' => 'Monthly','quarter' => 'Quarterly'], "week",['class' => 'selectpicker form-control time_filter','id' => 'time_filter']); ?>
					</div>
				<?php endif; ?>
				<?php
					$legendList = [
						['color' => 'rgba(128, 139, 150)', 'name' => 'Not Started'],
						['color' => 'rgba(52, 152, 219)', 'name' => 'In Progress'],
						['color' => 'rgba(217,83,79)', 'name' => 'On Hold'],
						['color' => 'rgba(212, 172, 13)', 'name' => 'Completed']
					];
				?>
				<?php if ($tabName == 'getSummaryPage' || $tabName == 'getSummaryResourcePage'): ?>
					<legend-list legend-list='<?php echo json_encode($legendList); ?>'></legend-list>
				<?php endif; ?>

      </div>

	  <script type="text/javascript">
	  	// STORE CURRENT TAB GLOBALLY
	  	window.ALLOCATION_CURRENT_TAB = '<?php echo $tabName; ?>';
		window.CURRENT_DEPT_ID = '<?php echo $deptId; ?>';


$(document).ready(function() {

	$(".s_dt").datepicker({
        todayBtn:  1,
        autoclose: true,
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.e_dt').datepicker('setStartDate', minDate);
    });

    $(".e_dt").datepicker()
    .on('changeDate', function (selected) {
        var maxDate = new Date(selected.date.valueOf());
        $('.s_dt').datepicker('setEndDate', maxDate);
    });
	// $(".s_dt").datepicker("option", "dateFormat", "dd-mm-yy");

	$(document).on('click', '.allocation-export-btn', function(e) {
			e.preventDefault();
			if (window.isExport == true) {
				if (window.exportResource != '') {
					$(this).attr('disabled', true);
					$(this).after('<p class="well well-sm pull-right waiting-notif-alloc-export"><strong>Wait, System is working..</strong></p>');
					BYJU.allocationDetails.allocationMidData();
				} else {
					alert("Please select level to export data");
				}
			} else {
				window.location.href = $(this).attr('href');
			}
	});
});
</script>