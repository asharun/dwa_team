<?php
$user_id=$this->session->userdata('user_id');
$file_nm=str_replace('.php','',basename(__FILE__));
$a_right1=str_replace(' ','_',$this->session->userdata('access'));
$access_str1=explode("|",$a_right1);

$a_right=$this->session->userdata('access');

$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
<!DOCTYPE html>
<html>
<head>
    <style>
  		html, body {
  			padding: 0px;
  			margin: 0px;
  		}

    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <meta charset="UTF-8">

    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">

  	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/custom_allocation.css?v=<?= v_num() ?>">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	 <link rel="stylesheet" type="text/css" href="<?= getAssestsUrl() ?>css/dx.common.css" />
     <link rel="dx-theme" data-theme="generic.light" href="<?= getAssestsUrl() ?>css/dx.light.css" />
     <link href="<?= getAssestsUrl() ?>css/select2.min.css" rel="stylesheet" />
     <link href="<?= getAssestsUrl() ?>css/select2.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
    <!-- <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/jquery-ui.css" rel="Stylesheet"></link> -->
    <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/dhtmlxgantt.css?v=<?= v_num() ?>">

	<!-- <link rel="stylesheet" href="<?= getAssestsUrl() ?>dist/app.css">
  <link rel="dx-theme" data-theme="generic.light" href="<?= getAssestsUrl() ?>css/dx.light.css" />
  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script> -->
</head>
<body>
<?php
  $this->load->view("Header.php");
?>