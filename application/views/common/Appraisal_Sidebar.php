<div class="col-md-3 left_col">
  <div class="left_col scroll-view">

    <div class="navbar nav_title" style="border: 0;">
      <a class="site_title"><i class="glyphicon glyphicon-th-list"></i> <span>Work Tracker</span></a>
    </div>
    <div class="clearfix"></div>

    <!-- menu prile quick info -->
    <div class="profile">
     <a href="<?= base_url() ?>index.php/User/home_page" style="padding: 20%;">
                      <img class="half_wjk" src="<?= getAssestsUrl() ?>images/byjulogo.png" alt=""></a>
    </div>
    <!-- /menu prile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

      <div class="menu_section">
        <h3>Tabs</h3>
        <ul class="nav side-menu">
          <li><a class="md_t_h"><i class="glyphicon glyphicon-home"></i> Home </a>
          </li>
		  <?php 
		  $a_right=$this->session->userdata('access');
		$access_str=explode("|",$a_right);
		if(in_array("appraisal_response",$access_str))
		  {
         echo '<li>
		  <a class="form_select" hrf="Appraisal_response"><i class="glyphicon glyphicon-pencil"></i> Appraisal Analysis </a>
          </li>';
		  }
		  
		  if(in_array("self_response",$access_str))
		  {
         echo '<li>
		  <a class="form_select" hrf="self_response"><i class="glyphicon glyphicon-pencil"></i>Form Responses CSV </a>
          </li>';
		  }
			?>
          <li ><a><i class="glyphicon glyphicon-edit" ></i>
              Forms
                  <span class="glyphicon glyphicon-chevron-down pull-right"></span>
              </a>
            <ul class="nav child_menu">
            <?php
            $f_name='';
			if($forms)
			{
                  foreach($forms AS $f_id)
                  {
                      if($f_id['form_code']==$file_nm)
                      {
                          $f_name=$f_id['form_name'];
                      }
                      echo ' <li><a class="form_select" hrf="'.$f_id['form_code'].'">'.$f_id['form_name'].'</a>
                              </li>';
                  }
			}
                  ?>
            </ul>
          </li>

        </ul>
      </div>

    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <!--div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Logout">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div-->
    <!-- /menu footer buttons -->
  </div>
</div>