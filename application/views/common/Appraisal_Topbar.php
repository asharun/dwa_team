<!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
		   <div class="row row_style_1">
            <div class="col-md-3 nav toggle">
              <a id="menu_toggle"><i class="glyphicon glyphicon-resize-horizontal" style="cursor: pointer;"></i></a>
            </div>
			 <div class='col-md-4'>
					<label class='l_font_fix_3' style='width:100%;'>Choose Slot:</label>
					<select id='sel_slot_1' class='selectpicker' title="Nothing Selected" data-live-search="true" style='width:100%;'>																		
					<?php
					foreach ($slots as $row)
					{
						$sel='';
							if($slot_chosen==$row['s_id'])
							{
								$sel='selected';																		
							}
						echo "<option value='".$row['s_id']."' ".$sel.">".$row['s_name']."</option>";
					}
					?>
					</select>
				</div>
			 <div class='col-md-5'>
            <ul class="nav navbar-nav navbar-right">
              <li class="">
                 <a class='dropdown-toggle user_p' data-toggle="dropdown" style='margin:2px;padding:2px'>
									<img class="prof_img" title="Logout" style="margin:0px;" src="<?= getAssestsUrl() ?>images/profile.png">
									<?php echo "<span class='u_in' style='color: black;text-align: center;' user=".$this->session->userdata('user_id').">Welcome ".$this->session->userdata('user_name')." !</span>"; ?>
				</a>
				 <ul class="dropdown-menu dropdown-user">
						<li><a href="#"><i class="glyphicon glyphicon-cog img_l"></i> Account Info</a>
						</li>
						<li class="divider"></li>
						<li><a href="<?php echo base_url('index.php/user/user_logout');?>">
						<i class="glyphicon glyphicon-log-out img_l"></i> Logout</a>
						</li>
					</ul>
              </li>
            </ul>
			</div>
		</div>
		</nav>
        </div>

      </div>
      <!-- /top navigation -->