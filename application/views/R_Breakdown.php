<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm="Reportees";

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
} 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Reportees">Data</button>
						<button class='stab_stages_2' ch="R_Team_Graphs">Activity-wise Graph</button>
						<button class='stab_stages_2 stab_dis_selec' ch="R_Breakdown">Breakdown</button>
						<button class='stab_stages_2' ch="R_Leave_Confirm"><span>Leave Approval</span> <?php echo '<span class="badge badge-info">'.($la_cnt_val[0]["i_cnt"]+$la_wee_cnt_val[0]["i_cnt"]).'</span>';?></button>
					</div>
				</div>
					<?php 	
						if(($s_dt) && ($e_dt))
						{
							$start_dt = date('d-M-Y', strtotime($s_dt));
							$end_dt = date('d-M-Y', strtotime($e_dt));
						}else
						{
							$day = date('Y-m-d H:i:s');
							$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}							
							$week_end = date('Y-m-d H:i:s', strtotime($day.' +'.(7-$day_w).' days'));
							$week_start = date('Y-m-d H:i:s', strtotime($day.'+'.(1-$day_w).' days'));
										
							$start_dt = date('d-M-Y', strtotime($week_start));
							$end_dt = date('d-M-Y',strtotime($week_end));
						}				
					?>
					<div class='row hid'>	
						<div class='col-md-12'>	
			
			<div class='row third-row head'>
												
												<?php
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Start Date: </label>	
															<input id='t_dtpicker' class='s_dt date_fm date-picker' value='".$start_dt."' />
														</span>
													</div>
													<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>End Date: </label>	
															<input id='t_dtpicker' class='e_dt date_fm date-picker' value='".$end_dt."' />
														</span>
													</div>
													<div class='col-md-2'>
																<button class='btn add_but gre_but sub_repo' type='button'>Submit</button>
																</div>
													</div>
									<div class='row row_style_1 third-row head'>
				<div class='col-md-12 cur-month text-center'><span>Break-up (".$start_dt." to ".$end_dt.")</span>
												</div>	";
												$titl1='';
												$titl2='';
												$titl3='';
												$titl4='';												
												?>
							</div>
									<div class='row row_style_1 scroll-head'>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Employee:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option data-hidden="true"></option>';
															if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($pro_sel_val==$row2['sel_1_id'])
																	{
																		$sel='selected';																		
																		$titl1=$row2['sel_1_name']." - Category-wise Break-up (".$start_dt." to ".$end_dt.")";
																		$titl2=$row2['sel_1_name']." - Element-wise Break-up (".$start_dt." to ".$end_dt.")";
																		$titl3=$row2['sel_1_name']." - Action-wise Break-up (".$start_dt." to ".$end_dt.")";
																		$titl4=$row2['sel_1_name']." - Trade-wise Break-up (".$start_dt." to ".$end_dt.")";
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>										
										</div>
			<?php
			echo "<div class='t1'>";
			echo "<div class='row row_style_1'>
							<div class='col-md-4'>
								<label class='l_font_fix_3'>Category-wise Break Up:-</label>
								</div>
								<div class='col-md-2 no_dt1 hide'>
								<label>No Records!</label>
							</div>
						</div>";			echo "<div class='data'>";
						//print_r($proj_xp);
						if($tm_data1)
						{
							foreach($tm_data1 AS $row1)
										{									
									// $tsdff=explode('_', $row1['tab_name'], 2);
									// $f_repl=$tsdff[1];							
									echo "<h5 class='hide params1' act_id='".$row1['tab_name']."'>".$row1['equiv_xp']."</h5>";
								
										}
						}else
						{
							$tm_data1=null;
						}
								echo "</div>";
							echo "<div class='canvas_r1 text-center'>";
							echo '<div style="width: auto; height: 400px;!important;" id="chartdiv1"></div>';
							echo '<div id="legenddiv1" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>';
			echo "<div class='t4'>";
			echo "<div class='row row_style_1'>
							<div class='col-md-4'>
								<label class='l_font_fix_3'>Trade-wise Break Up:-</label>
								</div>
								<div class='col-md-2 no_dt4 hide'>
								<label>No Records!</label>
							</div>
						</div>";
						echo "<div class='data'>";
						if($tm_data4)
						{
							foreach($tm_data4 AS $row1)
										{									
									echo "<h5 class='hide params4' act_id='".$row1['tab_name']."'>".$row1['equiv_xp']."</h5>";
									}
						}else
						{
							$tm_data4=null;
						}
								echo "</div>";
							echo "<div class='canvas_r4 text-center'>";
							echo '<div style="width: auto; height: 400px;!important;" id="chartdiv4"></div>';
							echo '<div id="legenddiv4" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>';
			echo "<div class='t2'>";
			echo "<div class='row row_style_1'>
							<div class='col-md-4'>
								<label class='l_font_fix_3'>Element-wise Break Up:-</label>
								</div>
								<div class='col-md-2 no_dt2 hide'>
								<label>No Records!</label>
							</div>
						</div>";
						echo "<div class='data'>";
						if($tm_data2)
						{
							foreach($tm_data2 AS $row1)
										{									
									echo "<h5 class='hide params2' act_id='".$row1['tab_name']."'>".$row1['equiv_xp']."</h5>";
									}
						}else
						{
							$tm_data2=null;
						}
								echo "</div>";
							echo "<div class='canvas_r2 text-center'>";
							echo '<div style="width: auto;  height: 400px;!important;" id="chartdiv2"></div>';
							echo '<div id="legenddiv2" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>';
		
			echo "<div class='t3'>";
			echo "<div class='row row_style_1'>
							<div class='col-md-4'>
								<label class='l_font_fix_3'>Action-wise Break Up:-</label>
								</div>
								<div class='col-md-2 no_dt3 hide'>
								<label>No Records!</label>
							</div>
						</div>";
						echo "<div class='data'>";
						if($tm_data3)
						{
							foreach($tm_data3 AS $row1)
										{									
									echo "<h5 class='hide params3' act_id='".$row1['tab_name']."'>".$row1['equiv_xp']."</h5>";
									}
						}else
						{
							$tm_data3=null;
						}
								echo "</div>";
							echo "<div class='canvas_r3 text-center'>";
							echo '<div style="width: auto; height: 400px;!important;" id="chartdiv3"></div>';
							echo '<div id="legenddiv3" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>';
			
			?>
			</div>			        
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var title_head1 = '<?php echo $titl1 ?>';</script>
	<script>var tt_data1 = '<?php echo sizeof($tm_data1) ?>';</script>
	<script>var title_head2 = '<?php echo $titl2 ?>';</script>
	<script>var tt_data2 = '<?php echo sizeof($tm_data2) ?>';</script>
	<script>var title_head3 = '<?php echo $titl3 ?>';</script>
	<script>var tt_data3 = '<?php echo sizeof($tm_data3) ?>';</script>
	<script>var title_head4 = '<?php echo $titl4 ?>';</script>
	<script>var tt_data4 = '<?php echo sizeof($tm_data4) ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/pie.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>
	$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");
						var str='';
						str=str+"&pro="+$(this).val();					
			window.location = base_url+"index.php/User/load_view_f?a=R_Breakdown&s_dt="+sdt+"&e_dt="+edt+str;
		}
	});
	
	$("body").on("click",".sub_repo",function(){
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");	
			var pro_v=$("body").find("#sel_1234").val();
						var str='';
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						
			window.location = base_url+"index.php/User/load_view_f?a=R_Breakdown&s_dt="+sdt+"&e_dt="+edt+str;
	});
	
	$("body").on("focus", ".s_dt",function(){
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1"
			});
	});
	
	$("body").on("focus", ".e_dt",function(){		
		$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1"
			});
	});
	
     var myArray1 =[];
	 var myArray2 = [];
	 var myArray3 = [];
	 var myArray4 = [];
   
    $(".params1").each(function(index) {
		item = {};
		item ["act"] =  $(this).attr('act_id');
        item ["work"] = $(this).text();
         myArray1.push(item);
    });

	 $(".params2").each(function(index) {
		item = {};
		item ["act"] =  $(this).attr('act_id');
        item ["work"] = $(this).text();
         myArray2.push(item);
    });
	
	 $(".params3").each(function(index) {
		item = {};
		item ["act"] =  $(this).attr('act_id');
        item ["work"] = $(this).text();
         myArray3.push(item);
    });
	
	$(".params4").each(function(index) {
		item = {};
		item ["act"] =  $(this).attr('act_id');
        item ["work"] = $(this).text();
         myArray4.push(item);
    });

	
    if(tt_data1!=0)
  {
	var chartData1 = myArray1;
	 var chart1;
            AmCharts.ready(function () {
                // SERIAL CHART
                chart1 = new AmCharts.AmPieChart();
                chart1.dataProvider = chartData1;
                 chart1.titleField = "act";
				 chart1.valueField = "work";				
				chart1.addTitle(title_head1);
				chart1.hideLabelsPercent=3;
                chart1.autoMargins = true;
				var legend1 = new AmCharts.AmLegend();
                legend1.borderAlpha = 0.2;
                legend1.horizontalGap = 10;
                legend1.autoMargins = true;				
				legend1.valueWidth=70;
                legend1.marginLeft = 20;
                legend1.valueText="[[value]] XP";
				legend1.marginRight = 20;
				
                chart1.addLegend(legend1, "legenddiv1");
				chart1.responsive = {
					  "enabled": true
					};
					chart1.write("chartdiv1");
				chart1.export = {
					  "enabled": true,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 1
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv1')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart1.initHC = false;
					chart1.validateNow();
            });              
			}else{
					$("body").find(".no_dt1").removeClass('hide');
					$("body").find(".canvas_r1").addClass('hide');
				}
				
    if(tt_data2!=0)
  {
	var chartData2 = myArray2;
	 var chart2;
            AmCharts.ready(function () {
                // SERIAL CHART
                chart2 = new AmCharts.AmPieChart();
                chart2.dataProvider = chartData2;
                 chart2.titleField = "act";
				 chart2.valueField = "work";				
				chart2.addTitle(title_head2);
                chart2.autoMargins = true;
				chart2.hideLabelsPercent=3;
				var legend2 = new AmCharts.AmLegend();
                legend2.borderAlpha = 0.2;
                legend2.horizontalGap = 10;
                legend2.autoMargins = true;
                legend2.marginLeft = 20;
				legend2.valueText="[[value]] XP";
				legend2.valueWidth=70;
                legend2.marginRight = 20;
                chart2.addLegend(legend2, "legenddiv2");
				chart2.responsive = {
					  "enabled": true
					};
					chart2.write("chartdiv2");
				chart2.export = {
					  "enabled": true,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 2
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv2')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart2.initHC = false;
					chart2.validateNow();
            });              
			}else{
					$("body").find(".no_dt2").removeClass('hide');
					$("body").find(".canvas_r2").addClass('hide');
				}
				
    if(tt_data3!=0)
  {
	var chartData3 = myArray3;
	 var chart3;
            AmCharts.ready(function () {
                // SERIAL CHART
                chart3 = new AmCharts.AmPieChart();
                chart3.dataProvider = chartData3;
                 chart3.titleField = "act";
				 chart3.valueField = "work";				
				chart3.addTitle(title_head3);
				chart3.hideLabelsPercent=3;
                chart3.autoMargins = true;
				var legend3 = new AmCharts.AmLegend();
                legend3.borderAlpha = 0.2;
                legend3.horizontalGap = 10;
                legend3.autoMargins = true;
				legend3.valueText="[[value]] XP";
				legend3.valueWidth=70;                
                legend3.marginLeft = 20;
                legend3.marginRight = 20;
                chart3.addLegend(legend3, "legenddiv3");
				chart3.responsive = {
					  "enabled": true
					};
					chart3.write("chartdiv3");
				chart3.export = {
					  "enabled": true,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 3
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv3')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart3.initHC = false;
					chart3.validateNow();
            });              
			}else{
					$("body").find(".no_dt3").removeClass('hide');
					$("body").find(".canvas_r3").addClass('hide');
				}
				
				if(tt_data4!=0)
  {
	var chartData4 = myArray4;
	 var chart4;
            AmCharts.ready(function () {
                // SERIAL CHART
                chart4 = new AmCharts.AmPieChart();
                chart4.dataProvider = chartData4;
                 chart4.titleField = "act";
				 chart4.valueField = "work";				
				chart4.addTitle(title_head4);
				chart4.hideLabelsPercent=3;
				
                chart4.autoMargins = true;
				var legend4 = new AmCharts.AmLegend();
                legend4.borderAlpha = 0.2;
                legend4.horizontalGap = 10;
                legend4.autoMargins = true;				
				legend4.valueWidth=70;
                legend4.marginLeft = 20;
                legend4.valueText="[[value]] XP";
				legend4.marginRight = 20;
				
				
                chart4.addLegend(legend4, "legenddiv4");
				chart4.responsive = {
					  "enabled": true
					};
					chart4.write("chartdiv4");
				chart4.export = {
					  "enabled": true,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1   // number which controls the opacity from 0 - 4
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv4')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart4.initHC = false;
					chart4.validateNow();
            });              
			}else{
					$("body").find(".no_dt4").removeClass('hide');
					$("body").find(".canvas_r4").addClass('hide');
				}
				
$(window.document).scroll(function() {
        var windowtop = $(window).scrollTop();

        $("body").find(".scroll-head").each(function(i, e) {
            $(this).removeClass("divposition");
            if (windowtop > $(e).offset().top) {
                $(e).addClass("divposition");
                return;
            }
        })
    });

	</script>
  </body>
</html>