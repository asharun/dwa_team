<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm='Team_Member_Info';

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">						
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Team_Member_Info">Report Card</button>
						<button class='stab_stages_2' ch="Graph_Drilldown">Pies</button>
						<button class='stab_stages_2' ch="Team_Split_Graph">Swiss Rolls</button>
						<button class='stab_stages_2 stab_dis_selec' ch="Graph_Trends">Trends</button>
						<button class='stab_stages_2' ch="Graph_Sum_Sc">Stats</button>
						<button class='stab_stages_2' ch="Employee_Info">Employee Records</button>				
						<button class='stab_stages_2' ch='Status_Report'>Activity Records</button>						
						<button class='stab_stages_2' ch="Employee_Leave">Leave Tracker</button>
						<button class='stab_stages_2' ch="WPR_Feed">Scatter</button>
					</div>
				</div>
					<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));
							$day_fm=date('d-M-Y',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));
							
							
							
							$date_start = date('Y-m-d', strtotime($day.' -'.($day_w-1).' days'));
							$date_end=date('Y-m-d', strtotime($day.' +'.(7-$day_w).' days'));
							
							$week_st = date('d-M', strtotime($date_start));
							
							$week_end = date('d-M', strtotime($date_end));
										
					?>
					<div class='row hid'>	
						<div class='col-md-12'>							
					<div class='row row_style_1 text-center'>	
							<div class='col-md-12'>					
										<button class='stab_stages_2 stab_dis_selec' ch="Graph_Trends">Log Spread</button> || 
										<button class='stab_stages_2 in_stages' ch="Trend_Report">XP Spread</button> || 
										<button class='stab_stages_2 in_stages' ch="NonQ_Share">Share of Non-Quant XP</button> || 
										<button class='stab_stages_2 in_stages' ch="Top_Bottom">Top & Bottom Share</button>
							</div>
					</div>					
					<hr class="st_hr2">
			<div class='row third-row head' style='margin-top: 10px;'>						
												<?php
												echo "
														<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Choose Date: </label>		
													<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
													</span>
												</div>
												
										<div class='col-md-6 cur-month text-center'><span>Week (".$week_st." to ".$week_end.")</span>
												
												</div>	";
													
												$titl2='';																								
												?>
							</div>
							<div class='row row_style_1' id='c_find'>	
											<div class='col-md-12'>					
														<a class='arr2 pull-left' ch='Graph_Trends' w_val="<?= $weekPrv ?>">&laquo; Prev</a>						
														<a class='arr2 pull-right' ch='Graph_Trends' w_val="<?= $weekNxt ?>">Next&raquo;</a>					
											</div>														
									</div>
									
									<div class='row row_style_1  scroll-head'>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Dept:</label>
															<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php
															foreach ($dept_val as $row)
															{
																$sel='';
																	if($dept_opt==$row['dept_id'])
																	{
																		$sel='selected';																		
																	}
																echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
															}
															?>
															</select>
														</div>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Project:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															$pto='';
															$sel1='';
																	if($pro_sel_val==0)
																	{
																		$sel1='selected';
																		$pto='OverAll';
																	}
															//echo '<option value="0" '.$sel1.'>OverAll</option>';
															if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($pro_sel_val==$row2['sel_1_id'])
																	{
																		$pto=$row2['sel_1_name'];
																		$sel='selected';
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>										
										</div>
			<?php
			echo "<div class='t1'>";
			echo "<div class='row row_style_1'>
							<div class='col-md-6'>
							<input class='ch_dt hide' value='".$date_view."' />
								<label class='l_font_fix_3'>Weekly Log Spread:-</label>
								</div>
								<div class='col-md-2 no_dt2 hide'>
								<label>No Records!</label>
							</div>
						</div>";		
					
						if($avg)
						{
							$titl2=$pto." - Weekly Log Spread (".$week_st." to ".$week_end.")";
							//print_r(($avg));
							$entries=sizeof($avg);
							$tot_cnt=$avg[$entries-1]['log_cnt_rt'];
						}else
						{
							$avg=null;
							$tot_cnt=0;
						}
								echo "</div>";
							echo "<div class='canvas_r2 text-center'>";
							echo '<div style="width: auto; height: 400px;!important;" id="chartdiv2"></div>
							<div id="legenddiv2" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>';
			
			?>		
			
			</div>			        
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var title_head2 = '<?php echo $titl2 ?>';</script>
	<script>var tt_data2 = <?php echo json_encode($avg); ?>;</script>
	<script>var total_cn = <?php echo ($tot_cnt); ?>;</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/serial.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	 <!--script src="https://www.amcharts.com/lib/4/themes/animated.js"></script-->
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>

window.useDateFilters = '<?php echo $useDateFilters ?>';
    
	$("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						var pro_v=$("body").find("#sel_1234").val();
						var str='';
						var dept=$("body").find("#sel_dept_1").val();
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						//window.location = base_url+"index.php/User/load_view_f?a=Graph_Trends&date_view="+date_v+str;
						var redirectUrl = base_url+"index.php/User/load_view_f?a=Graph_Trends&date_view="+date_v+str;
						if (window.useDateFilters == '1') {
							redirectUrl = getTimeSpanRedirectUrl(date_v, redirectUrl);
						}					
						window.location = redirectUrl;
						
						}
					});	
	});

    
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Project",dept:$(this).val()},
				success: function(response){			
					$('#sel_1234').html(response).selectpicker('refresh');
				}
			});	
		}	
	});
	
	
	$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var ele=Date.parse($(".ch_dt").val()); 
			var date_v=moment(ele).format("YYYY-MM-DD");
						
						var str='';
						str=str+"&pro="+$(this).val();						
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}						
			//window.location = base_url+"index.php/User/load_view_f?a=Graph_Trends&date_view="+date_v+str;
			
			var redirectUrl = base_url+"index.php/User/load_view_f?a=Graph_Trends&date_view="+date_v+str;
						if (window.useDateFilters == '1') {
							redirectUrl = getTimeSpanRedirectUrl(date_v, redirectUrl);
						}					
						window.location = redirectUrl;
		}
	});
	
    
	 var myArray2 = [];
  
	if(tt_data2)
  {			

			var ele;
			$.each(tt_data2, function(key,value) {
			  item = {};			  
			 ele=Date.parse(value['dates_v']); 
			 item ["act"]=moment(ele).format("DD-MMM (ddd)");
			  item ["Total logs for day"]=(total_cn==0?0:((value['log_cnt_rt']/total_cn) * 100).toFixed(2));
			  item ["Total logs on same day"]=(total_cn==0?0:((value['log_del_cnt_rt']/total_cn) * 100).toFixed(2));
			  item ["Audit logs"]=(total_cn==0?0:((value['audit_cnt_rt']/total_cn) * 100).toFixed(2));
			  item ["Review Logs"]=(total_cn==0?0:((value['rev_cnt_rt']/total_cn) * 100).toFixed(2));
			  item ["Confirmed Logs"]=(total_cn==0?0:((value['conf_cnt_rt']/total_cn) * 100).toFixed(2));
			  myArray2.push(item);			  
			}); 				
	ke2=[];
	ke2[0] ="Total logs for day";
	ke2[1] ="Total logs on same day";
	ke2[2] ="Audit logs";
	ke2[3] ="Review Logs";
	ke2[4] ="Confirmed Logs";
    
	var chartData2 = myArray2;
	 var chart2;
            AmCharts.ready(function () {
                chart2 = new AmCharts.AmSerialChart();
                chart2.dataProvider = chartData2;
                chart2.categoryField = "act";
				chart2.synchronizeGrid=true;
				chart2.addTitle(title_head2);
                chart2.autoMargins = true;
				
                var categoryAxis2 = chart2.categoryAxis;
                categoryAxis2.axisAlpha = 0;
                categoryAxis2.gridPosition = "start";
				categoryAxis2.autoGridCount = true;
				categoryAxis2.autoWrap=true;
				categoryAxis2.minorGridEnabled=true;
				
				var color=["#9c27b0","#FF0000","#FF9800","#03A9F4","#CDDC39"];
				var shape=["square","square","round","triangleUp","triangleDown"];
				
				for(i=0;i<=4;i++)
				{
					var graph2 = new AmCharts.AmGraph();
					graph2.valueAxis='v1';
					graph2.title = ke2[i];
					graph2.labelText = "[[value]]";
					graph2.valueField = ke2[i];
					graph2.bullet = shape[i];
					graph2.hideBulletsCount = 30;
					graph2.balloonText = "[[category]]<br>[[title]] - <b>[[value]] %</b>";
					graph2.lineColor = color[i];
					graph2.color = 'black';
					graph2.labelFunction = function(item,label){
					  return label == "0" ? "" : label+"%";
					}
					if(i!=1)
					{
				 graph2.fillAlphas=0.6;
					}
                chart2.addGraph(graph2);
				}
					
					var	valueAxis2 = new AmCharts.ValueAxis();               
						valueAxis2.id='v1';
						valueAxis2.precision=2;					
						valueAxis2.min=0.00;
						valueAxis2.max=100.00;
						valueAxis2.strictMinMax=true;
						valueAxis2.title='%';
						chart2.addValueAxis(valueAxis2);
				
				var legend2= new AmCharts.AmLegend();
                legend2.borderAlpha = 0.2;
                legend2.horizontalGap = 10;
                legend2.autoMargins = true;
                legend2.marginLeft = 20;
                legend2.marginRight = 20;
				legend2.useGraphSettings=true;
                chart2.addLegend(legend2, "legenddiv2");
				
				chart2.responsive = {
					  "enabled": true
					};
					chart2.write("chartdiv2");
					// chart2.chartCursor = {
					  // "cursorPosition": "mouse"
					// };
				chart2.export = {
					  "enabled": true,
					  "fileName":title_head2,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 2
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv2')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart2.initHC = false;
					chart2.validateNow();
            });              
			}else{
					$("body").find(".no_dt2").removeClass('hide');
					$("body").find(".canvas_r2").addClass('hide');
				}
				
	
// $(window.document).scroll(function() {
        // var windowtop = $(window).scrollTop();

        // $("body").find(".scroll-head").each(function(i, e) {
            // $(this).removeClass("divposition");
            // if (windowtop > $(e).offset().top) {
                // $(e).addClass("divposition");
                // return;
            // }
        // })
    // });

	</script>
  </body>
</html>
