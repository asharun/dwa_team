<header id="header_2">
    		<!--kode top bar start-->
    		<div class="top_bar_2">
	    		<div class="container">
	    			<div class="row">	    				
						<div class="col-md-3">						
							<div class="edu2_ft_logo_wrap">
							<a href="<?= base_url() ?>index.php/User/home_page">
							<img class="half_w" src="<?= getAssestsUrl() ?>images/byjus_new.png" alt=""></a>
							</div>
						</div>
						<div class="col-md-6">    					
								<h3 class='ev-c1'>
									<center>									
										<span>Work Tracker </span>
										<i class="glyphicon glyphicon-th-list" style="font-size: 21px;"></i>
									</center>
								</h3>	
	    					</div>
						<div class="col-md-2">	
						<center>
								<div class='ev-c2'>
									<a class='dropdown-toggle user_p' data-toggle="dropdown">
									<img class="prof_img" title="Logout" src="<?= getAssestsUrl() ?>images/profile.png">
									</a>
									 <ul class="dropdown-menu dropdown-user">
											<li><a href="#"><i class="glyphicon glyphicon-cog img_l"></i> Account Info</a>
											</li>
											<li class="divider"></li>
											<li><a href="<?php echo base_url('index.php/user/user_logout');?>">
											<i class="glyphicon glyphicon-log-out img_l"></i> Logout</a>
											</li>
										</ul>
									<?php echo "<div class='u_in' user=".$this->session->userdata('user_id').">Welcome ".$this->session->userdata('user_name')." !</div>"; ?>
								</div>
							</center>
	    				</div>
					<div class="col-md-1">	
					 <a href="http://content-wiki.byjus.com" target="_blank">
						<img class="" src="<?= getAssestsUrl() ?>images/wiki.jpg" style="height: 64px;">
					</a>
 					</div>	
					</div>
	    		</div>
	    	</div>    		      	
	   </header>
	   
	    	   <div class="edu2_counter_wrap">
				<div class="container">					
					<div class="edu2_counter_des">						
						<span><img class="img_lo" src="<?= getAssestsUrl() ?>images/meter.png" alt=""></span>
						<h4 class="counter"><?php echo $metrics[0]->today_xp;?></h4>
						<h6>YESTERDAY's XP</h6>
					</div>
					<div class="edu2_counter_des">						
						<span><img class="img_lo" src="<?= getAssestsUrl() ?>images/sum.png" alt=""></span>
						<h4 class="counter"><?php echo $metrics[0]->week_xp_val;?></h4>
						<h6>WEEK's XP</h6>
					</div>
					<div class="edu2_counter_des">						
						<span><img class="img_lo" src="<?= getAssestsUrl() ?>images/rank.png" alt=""></span>
						<h4 class="counter"><?php echo $this->session->userdata('streak');?></h4>
						<h6>STREAK</h6>
					</div>
				
				</div>
			</div>