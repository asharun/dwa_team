<?php
$user_id=$this->session->userdata('user_id');
   $file_nm='Floor_View';
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				
				<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			<div class="col-md-10">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>					
					</div>
			</fieldset>				
			</div>			
			</div>
			
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Floor_View">Weekly Summary</button>
						<button class='stab_stages_2' ch="FV_Wow">Productivity WoW</button>	
						<button class='stab_stages_2 ' ch="FV_Spread">Trends</button>
						<button class='stab_stages_2 stab_dis_selec' ch="FV_Feed">Scatter</button>	
						<button class='stab_stages_2' ch="FV_Summary">Descriptives</button>
						<button class='stab_stages_2' ch="FV_Drilldown">Pies</button>					
						<button class='stab_stages_2' ch="FV_Member_Info">Weekly Report</button>
						<button class='stab_stages_2' ch="FV_Contributor">Projects & Members</button>
						
					</div>
				</div>
					<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));
							
							if($day_w==0)
							{
								$day_w=7;								
							}
							$day_fm=date('d-M-Y',strtotime($day));
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));							
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));							
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));					
					?>
					<div class='row hid'>	
						<div class='col-md-12'>	
			
			<div class='row third-row head'>
												
												<?php
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Choose Date: </label>	
															<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
														</span>
													</div>
										<div class='col-md-6 cur-month text-center'><span>Break-up (".$week_st." to ".$week_end.")</span>
												</div>	";
												$titl1='';
												$titl2='';
																								
												?>
							</div>
							
									<div class='row row_style_1' id='c_find'>	
											<div class='col-md-12'>					
														<a class='arr pull-left' ch='FV_Feed' w_val="<?= $w_prev ?>">&laquo; Prev</a>						
														<a class='arr pull-right' ch='FV_Feed' w_val="<?= $w_nxt ?>">Next&raquo;</a>					
											</div>														
									</div>
									<div class='row row_style_1'>	
							<div class='col-md-2'>
								<label class='l_font_fix_3'>Choose Dept:</label>
								<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
								<?php
								foreach ($dept_val as $row)
								{
									$sel='';
										if($dept_opt==$row['dept_id'])
										{
											$sel='selected';																		
										}
									echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
								}
								?>
								</select>
							</div>
							<div class='col-md-3'>
															<label class='l_font_fix_3'>Choose Project:</label>
															<?php  $selected=explode(',',$pro_sel_val);?>
															<select id='sel_1234' class='selectpicker form-control'multiple title="Nothing Selected" data-live-search="true">										
															<?php 
															$pto=array();
															$sel1='';
																	// if($pro_sel_val==0)
																	// {
																		// $sel1='selected';
																		// $pto='OverAll';
																	// }
															
															if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if(in_array($row2['sel_1_id'],$selected))
																	{
																		$pto[]=$row2['sel_1_name'];
																		$sel='selected';
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>	
														<div class='col-md-2'>
															<label class='l_font_fix_3'>Input:</label>
															<?php 
																$selected=$this->input->get('input_val');
															?>
															<select id='input_drp' class='selectpicker form-control' data-live-search="false">			
															
																<option value="esp_wrk_hrs"<?php if($selected=="esp_wrk_hrs"){echo "Selected";} ?>>Esp work hours</option>';
																<option value="sapience_on"<?php if($selected=="sapience_on"){echo "Selected";} ?>>Sapience On</option>
															
															</select>
														</div>	
														<div class='col-md-2'>
															<label class='l_font_fix_3'>Output:</label>
															<?php 
																$selected=$this->input->get('output_val');
															?>
															<select id='output_drp' class='selectpicker form-control' data-live-search="false">			
															
																<option value="act"<?php if($selected=="act"){echo "Selected";} ?>>Confirmed XP</option>
																<option value="proj_xp"<?php if($selected=="proj_xp"){echo "Selected";} ?>>Projected XP</option>
															
															</select>
														</div>	
														<div class='col-md-2'>
										<label class='l_font_fix_3'></label>
								
								<input type="button" id="selectproject" class="form-control btn-primary" value="submit">
							</div>	
						</div>
									
			<?php
			
			echo "<div class='t3'>";
			echo "<div class='row row_style_1'>
							<div class='col-md-6'>
								<label class='l_font_fix_3'>"
								.implode(',',$pto)."  - Scatter Plot:-</label>
								</div>
								<div class='col-md-2 no_dt1 hide'>
								<label>No Records!</label>
							</div>
						</div>";
						echo "<div class='data'>";
						$titl3='';
						if($avg)
						{
							$titl3="Avg Work Hours Vs Weekly Output (".$week_st." to ".$week_end.")";
						}else
						{
							$avg=null;
						}
								echo "</div>";
							echo "<div class='canvas_r2 text-center'>";
							echo '<div style="width: auto; height: 400px;!important;" id="chartdiv2"></div>';
							echo '<div id="legenddiv2" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>';	
			
			// echo "<div class='t2'>";
			// echo "<div class='row row_style_1'>
							// <div class='col-md-6'>
								// <label class='l_font_fix_3'>Top and Bottom Quartile Share:-</label>
								// </div>
								// <div class='col-md-2 no_dt2 hide'>
								// <label>No Records!</label>
							// </div>
						// </div>";
						// echo "<div class='data'>";
						// if($Q13per)
						// {
							// $titl2="Top and Bottom Quartile Share (".$week_st." to ".$week_end.")";																	
							// foreach($Q13per AS $row1=>$value1)
									// {									
									// echo "<h5 class='hide params2' v1='".$value1['Bottom']."' v2='".$value1['Top']."'>".$row1."</h5>";
									// }
						// }else
						// {
							// $Q13per=null;
						// }
								// echo "</div>";
							// echo "<div class='canvas_r2 text-center'>";
							// echo '<div style="width: auto;  height: 400px;!important;" id="chartdiv2"></div>';
							// echo '<div id="legenddiv2" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			// </div>
			// </div>';		
				// echo "<div class='t1'>";
			// echo "<div class='row row_style_1'>
							// <div class='col-md-6'>
								// <label class='l_font_fix_3'>Highest and Lowest XP:-</label>
								// </div>
								// <div class='col-md-2 no_dt1 hide'>
								// <label>No Records!</label>
							// </div>
						// </div>";			echo "<div class='data'>";
						// if($tm_data1)
						// {
							// $titl1="Highest and Lowest XP (".$week_st." to ".$week_end.")";																	
							// foreach($tm_data1 AS $row1)
										// {											
									// echo "<h5 class='hide params1' v1='".round($row1['v1'],2)."' v2='".round($row1['v2'],2)."'>".$row1['project_name']."</h5>";
								
										// }
						// }else
						// {
							// $tm_data1=null;
						// }
								// echo "</div>";
							// echo "<div class='canvas_r1 text-center'>";
							// echo '<div style="width: auto; height: 400px;!important;" id="chartdiv1"></div>';
							// echo '<div id="legenddiv1" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			// </div>
			// </div>';
			?>		
			</div>			        
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var tt_data2 = <?php echo json_encode($avg); ?>;</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/xy.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>

	$("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");	
						var str='';
						var dept=$("body").find("#sel_dept_1").val();
						var pro_v=$("body").find("#sel_1234").val();
						var input=$("#input_drp").val();
						var output=$("#output_drp").val();
							str=str+"&pro="+pro_v+"&input_val="+input+"&output_val="+output;					
						if(dept)
						{
							str=str+"&dept="+dept;
						}						
						window.location = base_url+"index.php/User/load_view_f?a=FV_Feed&date_view="+date_v+str;
						
						}
					});	
	});
	
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
						// var ele=Date.parse($("body").find(".ch_dt").val()); 
						// var date_v=moment(ele).format("YYYY-MM-DD");
						// var str='';
						// var dept=$(this).val();
						// if(dept)
						// {
							// str=str+"&dept="+dept;
						// }
						// window.location = base_url+"index.php/User/load_view_f?a=FV_Feed&date_view="+date_v+str;		
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Feed",dept:$(this).val()},
				success: function(response){			
					$('#sel_1234').html(response).selectpicker('refresh');
				}
			});							
		}	
	});
	
$("body").on("click","#selectproject",function(){
			//console.log($("#sel_1234").val());return;
		if($("#sel_1234").val())
		{	
			var ele=Date.parse($("body").find(".ch_dt").val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						var str='';
						var input=$("#input_drp").val();
						var output=$("#output_drp").val();
						var pro_v=$("#sel_1234").val();		
							str=str+"&pro="+pro_v+"&input_val="+input+"&output_val="+output;						
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
													
						window.location = base_url+"index.php/User/load_view_f?a=FV_Feed&date_view="+date_v+str;
						
		}
	});
    
	myArray2=[];
	var x_max=17;
	var y_max=16;
	if(tt_data2)
	{
$.each(tt_data2, function(key,value) {
			  item = {};			  
			// ele=Date.parse(value['dates_v']); 
			 item ["x"+key]=parseFloat(value['x']);
			 item ["y"+key]=parseFloat(value['y']);
			 if(x_max<parseFloat(value['x']))
			 {
			  x_max=parseFloat(value['x']);
			 }
			 if(y_max<parseFloat(value['y']))
			 {
			  y_max=parseFloat(value['y']);
			 }
			 item ["value"]=3;
			  item ["title"]=value['format'];		 
			  item ["form"]=value['format'];
		      item['popovertextxp']=value['popovertextxp'];
		      item['popovertexttype']=value['popovertexttype'];
			  myArray2.push(item);			  
			}); 
	}
			//console.log(myArray2);
			// console.log(x_max);
			// var x_max = Math.max.apply(this,$.map($.parseJSON(myArray2), function(o){ return o.x; }));
			// var y_max = Math.max.apply(this,$.map($.parseJSON(myArray2), function(o){ return o.y; }));
var count=(myArray2.length);
	 if(tt_data2!=0)
  { 
            AmCharts.ready(function () {
var chart= new AmCharts.AmXYChart();        
chart.dataProvider = myArray2;
//chart.marginLeft = 35;
//chart.startDuration = 1.5;
//chart.bandValueScope=20;

var xAxis = new AmCharts.ValueAxis();
xAxis.position = "left";
xAxis.axisAlpha=0;
xAxis.id="xa";
xAxis.minimum =0;
xAxis.maximum =x_max;

xAxis.title="Weekly o/p (XP)";
//xAxis.autoGridCount = true;
chart.addValueAxis(xAxis);

var yAxis = new AmCharts.ValueAxis();
yAxis.position = "bottom";
yAxis.id="ya";
yAxis.minimum =0;
yAxis.maximum =y_max;
//yAxis.autoGridCount = true;
//yAxis.title="AVG "+item['popovertexttype'];

yAxis.axisAlpha=0;
chart.addValueAxis(yAxis);                



				
var trendLine = new AmCharts.TrendLine();
// trendLine.initialDate = new Date(2012, 0, 2, 12); // 12 is hour - to start trend line in the middle of the day
// trendLine.finalDate = new Date(2012, 0, 11, 12);
trendLine.initialValue = 0;
trendLine.finalValue = 30;
trendLine.initialXValue = 8;
trendLine.finalXValue = 8;
trendLine.thickness=2;
//trendLine.dashLength=3;
trendLine.lineColor = "#000000";
chart.addTrendLine(trendLine);


var trendLine = new AmCharts.TrendLine();
// trendLine.initialDate = new Date(2012, 0, 2, 12); // 12 is hour - to start trend line in the middle of the day
// trendLine.finalDate = new Date(2012, 0, 11, 12);
trendLine.initialValue = 10;
trendLine.finalValue = 10;
 trendLine.initialXValue = 0;
 trendLine.finalXValue = 18;
//trendLine.valueAxis="ya";
trendLine.lineColor = "#CC0000";
chart.addTrendLine(trendLine);



				for(i=0;i<count;i++)
				{
				var graph2 = new AmCharts.AmGraph();
					graph2.balloonText="[[title]]:[[popovertexttype]]:<b>[[x]]</b> [[popovertextxp]]:<b>[[y]]</b>";
					graph2.bullet= "circle";
					graph2.bulletBorderAlpha= 0.2;
					graph2.bulletAlpha= 0.8;
					graph2.lineAlpha= 0;
					graph2.fillAlphas= 0;
					graph2.title= myArray2[i]["form"];
					graph2.valueField= "value";
					graph2.xField = "x"+i;
					graph2.yField = "y"+i;
					graph2.maxBulletSize= 10;
					
					chart.addGraph(graph2);
					// if(i!=0)
					// {
					// chart.hideGraph(graph2);
					// }
				}
				
				
					
				var legend2= new AmCharts.AmLegend();
                legend2.borderAlpha = 0.2;
                legend2.horizontalGap = 10;
                legend2.autoMargins = true;
                legend2.marginLeft = 20;
                legend2.marginRight = 20;
                chart.addLegend(legend2, "legenddiv2");
				
                         chart.responsive = {
					  "enabled": true
					};     
chart.write("chartdiv2");
				chart.export = {
					  "enabled": true,
					 // "fileName":title_head2,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 2
						  },
					  "exportTitles" :true,					  
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart.initHC = false;
					chart.validateNow();
            });              
			}else{
					$("body").find(".no_dt1").removeClass('hide');
					$("body").find(".canvas_r2").addClass('hide');

				}
				
				
$(window.document).scroll(function() {
        var windowtop = $(window).scrollTop();

        $("body").find(".scroll-head").each(function(i, e) {
            $(this).removeClass("divposition");
            if (windowtop > $(e).offset().top) {
                $(e).addClass("divposition");
                return;
            }
        })
    });

	</script>
  </body>
</html>