<?php
	$user_id=$this->session->userdata('user_id');
  $file_nm='Reports';
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	
$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);
if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

 ?> 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">	
  </head>
	<body>
	<?php 
	$this->load->view("Header.php");  
	
	?>
	<div class="desc">
		<div class="ic_cont">
			<div class="row ma_row">
				<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
						<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Reports">Overview</button>
						<button class='stab_stages_2' ch="PT_Report_Card">Report Card</button>
						<button class='stab_stages_2' ch="PT_Pies">Pies</button> 						
						<button class='stab_stages_2 stab_dis_selec' ch="PT_Trends">Trends</button> 						
						<button class='stab_stages_2' ch="PT_Stats">Stats</button> 						
						<button class='stab_stages_2' ch="PT_Feedback">Feedback</button>						
					</div>
				</div>	
					<?php
						// $s_dt=''; 
						// $e_dt='';

						if(($s_dt) && ($e_dt))
						{
							$start_dt = date('d-M-Y', strtotime($s_dt));
							$end_dt = date('d-M-Y', strtotime($e_dt));
						}else
						{
							$day = date('Y-m-d H:i:s');
							$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}							
							$week_end = date('Y-m-d H:i:s', strtotime($day.' +'.(7-$day_w).' days'));
							
							$week_start = date('Y-m-d H:i:s', strtotime($week_end.' -1 month'));
							$week_st_w = date('w',strtotime($week_start));
							if($week_st_w==0)
							{
								$week_st_w=7;								
							}							
							$start_dt = date('d-M-Y', strtotime($week_start.' -'.($week_st_w-1).' days'));
							$end_dt = date('d-M-Y',strtotime($week_end));
							
							$s_dt = date('Y-m-d', strtotime($start_dt));
							$e_dt = date('Y-m-d', strtotime($end_dt));
						}						
					?>
					<div class='row hid'>
						<div class="col-md-12">	
							<div class='row row_style_1 text-center'>	
							<div class='col-md-12'>					
										<button class='stab_stages_2 in_stages' ch="PT_Trends">Log Spread</button> || 
										<!--button class='stab_stages_2 in_stages' ch="PT_Trend_Report">XP Spread</button--> 
										<button class='stab_stages_2 stab_dis_selec' ch="PT_NonQ_Share">Share of Non-Quant XP</button>
							</div>														
					</div>
					
					<hr class="st_hr2">
			<div class='row third-row head' style='margin-top: 10px;'>	
												<?php
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Start Date: </label>	
															<input id='t_dtpicker' class='s_dt date_fm date-picker' dt='".$s_dt."' value='".$start_dt."' />
														</span>
													</div>
													<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>End Date: </label>	
															<input id='t_dtpicker' class='e_dt date_fm date-picker' dt='".$e_dt."' value='".$end_dt."' />
														</span>
													</div>
													</div>
													<div class='row row_style_1 third-row head'>
										<div class='col-md-12 cur-month text-center'><span>Week (".$start_dt." to ".$end_dt.")</span>
												</div>	";
												$titl1='';
												$titl2='';
																								
												?>
							</div>	
					   	</div>			
						<div class='row row_style_1'>
							<div class='row row_style_1 scroll-head'>
							<?php 
								$pto='';
										echo "<div class='col-md-4'>
												<label class='l_font_fix_3' style='width:100%'>Choose Employee</label>	";
												echo "<select id='sel_emp' class='selectpicker form-control' title='Nothing Selected'  data-live-search=true'>";
												if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($row2['sel_1_id']==$pro_sel_val)
																	{
																		$sel='selected';	
																		$pto=$row2['sel_1_name'];
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
															
												echo "</select>";											
										echo "</div>";
								?>
							</div>
						</div>
						<div class='row row_style_1'>
							<div class='row row_style_1 scroll-head'>
								<?php		
								echo "<div class='data'>";	
										$startDateUnix=strtotime($start_dt);
										$endDateUnix=strtotime($end_dt);
										
											$currentDateUnix = $startDateUnix;
											$weekNumbers = array();
											while ($currentDateUnix < $endDateUnix) {
												//$y=date('d-Y', ($currentDateUnix));
												$weekNumbers[] = date('Y', (strtotime('+3 days', $currentDateUnix))).date('W', $currentDateUnix);
												$currentDateUnix = strtotime('+1 week', $currentDateUnix);
												
											}
											//print_r($avg);
										//$titl2="Share of Non Quant XPs over weeks (".$start_dt." to ".$end_dt.")";																			
								$avg_final=array();
					foreach($weekNumbers AS $inde2)
								{
								$week=substr($inde2,4,2);
								$ye=substr($inde2,0,4);
								 $inde1 = "W ".$week." (".date("d/m", strtotime($ye."-W".$week."-1"))." to ".date("d/m", strtotime($ye."-W".$week."-7")).")"; 			
								 
								 	if(!empty($avg)){
								 		if(array_key_exists($inde2,$avg))
										{
											$avg_final[]=array("dt"=>$inde1,"xp"=>$avg[$inde2]);
										}else{
										$avg_final[]=array("dt"=>$inde1,"xp"=>0);
									}

								 }
									
								}	
									
										if(!empty($avg_final)){


											echo "</div>";
											 echo "<div class='t2'>";
											echo "<div class='row row_style_1'>
															<div class='col-md-6'>
															<input class='ch_dt hide' value='".$date_view."' />
																<label class='l_font_fix_3'>Share of Non-Quantifiable XPs:-</label>
																</div>
																<div class='col-md-2 no_dt1 hide'>
																<label>No Records!</label>
															</div>
														</div>";
										echo "<div class='canvas_r2 text-center'>";
										echo '<div style="width: auto;  height: 400px;!important;" id="chartdiv2"></div>';
										echo '<div id="legenddiv2" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
											 </div>
											 </div>';
										}
										?>
																	
								
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>


	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url ='<?php echo base_url() ?>'</script> 
	 <script>var title_head2 = '<?php echo $titl2 ?>';</script>
	<script>var avg_data = <?php  echo json_encode($avg_final)  ?>;</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/serial.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript">
	
	
	
	$("body").on("change","#sel_emp",function(){
		if($(this).val())
		{
			$.ajax({				
				url: base_url+"index.php/User/load_emp_string",
				type: 'post',
				data : {param:"Personal_Trends",pt_emp:$(this).val().toString()},
				success: function(response) {				
						var sdt=$("body").find(".s_dt").attr('dt');
						var edt=$("body").find(".e_dt").attr('dt');
						if(sdt && edt && sdt<=edt)
						{
						window.location = base_url+"index.php/User/load_view_f?a=PT_NonQ_Share&s_dt="+sdt+"&e_dt="+edt;
						}
				}
			});
		}
	});

		$("body").on("focus", ".s_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1,
					daysOfWeekDisabled: "0,2,3,4,5,6"
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var sdt=moment(ele).format("YYYY-MM-DD");
						$(this).attr('dt',sdt);
						var ele1=Date.parse($('.e_dt').val()); 
						var edt=moment(ele1).format("YYYY-MM-DD");
						
						if(ele && ele1 && (ele<ele1))
						{													
						window.location = base_url+"index.php/User/load_view_f?a=PT_NonQ_Share&s_dt="+sdt+"&e_dt="+edt;
						}
						}
					});	
	});
	
		$("body").on("focus", ".e_dt",function(){		
		$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1,
					daysOfWeekDisabled: "1,2,3,4,5,6"
			}).on('changeDate', function(e) {
				if($(this).val())
						{
						var ele=Date.parse($('.s_dt').val()); 
						var sdt=moment(ele).format("YYYY-MM-DD");
						var ele1=Date.parse($(this).val()); 
						var edt=moment(ele1).format("YYYY-MM-DD");	
						$(this).attr('dt',edt);
						if(ele && ele1 && (ele<ele1))
						{
						window.location = base_url+"index.php/User/load_view_f?a=PT_NonQ_Share&s_dt="+sdt+"&e_dt="+edt;
						}
						}
					});	
			});
		

  	 var myArray2 = [];
 if(avg_data)
  {  
  	
$.each(avg_data, function(key,value) {

 item = {}; 
 item ["dt"]=value["dt"];
 item ["xp"]=value["xp"];
 myArray2.push(item);  
});     

//console.log(myArray2);
var chartData2 = myArray2;
	 var chart2;
            AmCharts.ready(function () {
                chart2 = new AmCharts.AmSerialChart();
                chart2.dataProvider = chartData2;
                chart2.categoryField = 'dt';
				//chart2.synchronizeGrid=true;
				chart2.addTitle(title_head2);
                chart2.autoMargins = true;
				
                var categoryAxis2 = chart2.categoryAxis;
                categoryAxis2.gridAlpha = 0;
                categoryAxis2.axisAlpha = 0;
                categoryAxis2.gridPosition = "start";
				categoryAxis2.autoGridCount = true;
				categoryAxis2.autoWrap=true;
				categoryAxis2.title = "Weeks";
				
				
					var graph2 = new AmCharts.AmGraph();
					
					graph2.title = "Share of Non-Quantifiable XP";
					graph2.valueField = "xp";					
					graph2.labelText = "[[value]]%";                
					graph2.type = "column";
					graph2.lineAlpha = 0;
					graph2.fillColors = '#FF6600';
					graph2.balloonText = "[[category]]<br>[[title]] - <b>[[value]] %</b>";
					graph2.color = 'black';
					graph2.lineColor = '#FF6600';
					graph2.lineColor = 'black';
					graph2.fillAlphas = 1; 				 					
					graph2.hideBulletsCount = 30;
					graph2.labelFunction = function(item,label){
						  return label == "0 XP" ? "" : label;
						}						
					chart2.addGraph(graph2);
					
					
					  var valueAxis2 = new AmCharts.ValueAxis();
                 valueAxis2.stackType = "regular";
                valueAxis2.maximum=100;
				valueAxis2.minimum=0;
				valueAxis2.precision=2;
                chart2.addValueAxis(valueAxis2);
				
					
				var legend2= new AmCharts.AmLegend();
                legend2.borderAlpha = 0.2;
                legend2.horizontalGap = 10;
                legend2.autoMargins = true;
                legend2.marginLeft = 20;
                legend2.marginRight = 20;
                chart2.addLegend(legend2, "legenddiv2");
				
				
				chart2.responsive = {
					  "enabled": true
					};
					chart2.write("chartdiv2");
				chart2.export = {
					  "enabled": true,
					  "fileName":title_head2,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 2
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv2')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart2.initHC = false;
					chart2.validateNow();
            });   
}else{
					$("body").find(".no_dt1").removeClass('hide');
					$("body").find(".canvas_r2").addClass('hide');
				}

	</script>
	</body>
  </html>