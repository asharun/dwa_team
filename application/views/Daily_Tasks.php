<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
  $file_nm=str_replace('.php','',basename(__FILE__));

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	
  </head>
  <body>
 <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
						<?php
						//echo "Hi".$date_view;
						
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
						
						$day_fm=date('d-M-Y',strtotime($day));
							$day_w = date('w',strtotime($day));
							//echo $day_w;
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));
							
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));
							
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));
							
							//echo $week_st;
							//echo $week_end;
							
							?>
			<div class="col-md-10 c_row">
							<div class='row row_style_1'>
			<div class="col-md-10">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>					
					</div>
			</fieldset>				
			</div>			
			</div>	
							<div class='row third-row head row_style_3'>
												
												<?php
												echo "
												<div class='col-md-3'>
												<span>
												<label class='l_font_fix_3'>Choose Date: </label>	
													<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
													</span>
												</div>
												<div class='col-md-6 cur-month1 text-center'>
													<span>Records from ".$week_st." to ".$week_end."</span>
												<i class='glyphicon glyphicon-calendar curr_dt_view'></i></div>";
												?>
							</div>
							
									<div class='row third-row row_style_3' id='c_find'>	
											<div class='col-md-12'>					
														<a ch='Daily_Tasks' class='arr3 pull-left' w_val="<?= $w_prev ?>">&laquo; Prev</a>
														<a ch='Daily_Tasks' class='arr3 pull-right' w_val="<?= $w_nxt ?>">Next&raquo;</a>					
											</div>														
									</div>
														
									<div class='row'>				
								<div class='col-md-12 week seven-cols hidden-xs hidden-sm'>
									
									<div class='col-md-1 week_cell'>MONDAY</div>
									<div class='col-md-1 week_cell'>TUESDAY</div>
									<div class='col-md-1 week_cell'>WEDNESDAY</div>
									<div class='col-md-1 week_cell'>THURSDAY</div>
									<div class='col-md-1 week_cell'>FRIDAY</div>
									<div class='col-md-1 week_cell'>SATURDAY</div>	
									<div class='col-md-1 week_cell'>SUNDAY</div>
								</div>
								</div>
											
									<?php
								echo "<div class='row'> 
										<div class='col-md-12 week seven-cols'>";

								
								$curr_day=date('Y-m-d');
								
										for($i=0;$i<=6;$i++)
										{											
											$date_wek = date('Y-m-d H:i:s', strtotime($week_start . ' +'.$i.' day'));	
											//echo $date_wek;
											
											$date_chk = date('Y-m-d', strtotime($date_wek));
											
											$day1=date("d",strtotime($date_wek));											
											$week_name=date("D", strtotime($date_wek));
											$highl='';
											$high2='';
												//echo $date_chk." ".$curr_day;
											if($date_chk==$curr_day)
											{
												$highl='highl';
												$high2='highl_mob';
											}
											
											//echo $date_chk;
											echo "<div class='col-md-1 tab-cell'>";
											echo "<div class='day-num-sm visible-xs-inline-block visible-sm-inline-block'>
											 <span class='pull-left ".$high2."'>".$day1." | ".$week_name."</span>
											 <span class='pull-right'>
											 <i class='glyphicon glyphicon-edit t_edit edit_task' t_date='".$date_chk."'></i>
											</span>
											 <span class='count_list' id='cnt_r'></span></div>";											
												
											echo "<div class='day-num hidden-xs hidden-sm'><span class='".$highl."'>".$day1 . "</span>
											<span class='pull-right'>
											<i class='glyphicon glyphicon-edit t_edit edit_task' t_date='".$date_chk."'></i>
											</span>
											</div>";
											
											echo "<div class='month_tab'>";
			//echo "<div class='count_list align-text' id='cnt_r'></div>";
			if($cal_data)
			{
				foreach($cal_data AS $rewo)
				{
					if($rewo['day_val']==$date_chk)
					{
				echo "<table class='tbldata' id='tblData'>";
					echo "<tbody>";
					echo "<tr><td class='td_al'>";
					echo "<button type='button' class='btn event-lt ".$rewo['tast_stat']."' b_c='' data-container = '.ic_cont' data-placement='auto bottom' data-toggle='popover' data-html='true'  date_val='".date("M d,Y", strtotime($date_wek))."' data-content=\"";
							
											echo "<b><center class='act_nm_break'>"  . str_replace("\xA0", " ", $rewo['activity_name']) . "</center></b><br>";										
										echo "<center><span class='badge pull-right ".$rewo['badge_col']."' style='color:white;'>" .str_replace("\xA0", " ", $rewo['stat_nm'])."</span><span class='pull-left'> ". date("M d,Y", strtotime($date_wek)). "</span></center>";
											echo"<hr class='style1_pop'>";
											echo "<b>Quantity Done: </b>" . str_replace("\xA0", " ", $rewo['work_comp']) . "<br>";
											echo "<b>Standard Target: </b>" . str_replace("\xA0", " ", $rewo['std_tgt_val']) . "<br>";
											echo "<b>Filled XP: </b>" . str_replace("\xA0", " ", $rewo['equiv_xp']) . "<br>";
											echo "<b>Comp XP: </b>" . str_replace("\xA0", " ", $rewo['comp_xp']) . "<br>";
											echo "<b>Audit Desc: </b>" . str_replace("\xA0", " ", $rewo['audit_desc']) . "<br>";
											echo "<b>Total XP: </b>" . str_replace("\xA0", " ", $rewo['tot_xp']) . "<br>";
							echo "\">";																				
					echo $rewo['activity_name']."</button>";
					echo "</td></tr>";
				echo "</tbody>";
				echo "</table>";
					}
				}
			}
				echo"</div>";			
			echo "</div>";
			}
		echo "</div>
		</div>";	
	?>													
			</div>
		</div>
	</div>
</div>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script>
	
	  $('body').popover({
            html: true,
            trigger: "manual"

        })
        .on("mouseenter", '[data-toggle="popover"]', function() {
            var _this = this;

            var color = $(this).css("border-left-color");
			var c1=$(this).css("background-color");

            if (!$(this).hasClass("badge")) {
                $(this).css("background-color", color);
                $(this).css("color", "white");
				$(this).attr('b_c',c1);
            }

            $(this).popover("show");
            var pop_id = $(this).attr("aria-describedby");

            $("body").find('#' + pop_id).find(".popover-title").css("background", color);

            $("body").on("mouseleave", '.popover', function() {
                $(_this).popover('hide');
            });
        }).on("mouseleave", '[data-toggle="popover"]', function() {
            var _this = this;
            setTimeout(function() {
                if (!$("body .popover:hover").length) {
                    $(_this).popover("hide");
                }
            }, 10);
            if (!$(this).hasClass("badge")) {
                var color1 = $(this).attr('b_c');
                $(this).css("background-color", color1);
                $(this).css("color", "black");
            }
        });

		
		$("body").on("click", ".arr3",function(){
		var w_val = $(this).attr("w_val");
		var ch = $(this).attr("ch");
		if($("body").find("#sel_1234").val())
		{
			var pro_v=$("body").find("#sel_1234").val();
			window.location = base_url+"index.php/User/load_view_f?a="+ch+"&date_view="+w_val+"&pro="+pro_v;
		}else{
			window.location = base_url+"index.php/User/load_view_f?a="+ch+"&date_view="+w_val;
		}
	});
	
	$("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						window.location = base_url+"index.php/User/load_view_f?a=Daily_Tasks&date_view="+date_v;
						}
					});			
	});
	</script>
  </body>
</html>