<?php
$user_id=$this->session->userdata('user_id');
$file_nm='Reports';
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 	
$access_str1=explode("|",$a_right1);	
$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);
if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">	
</head>
<body>
	<?php 
	$this->load->view("Header.php");  
	?>
	<div class="desc">
		<div class="ic_cont">
			<div class="row ma_row">
				<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
					<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Reports">Overview</button>
						<button class='stab_stages_2' ch="PT_Report_Card">Report Card</button>
						<button class='stab_stages_2' ch="PT_Pies">Pies</button> 						
						<button class='stab_stages_2 stab_dis_selec' ch="PT_Trends">Trends</button> 						
						<button class='stab_stages_2' ch="PT_Stats">Stats</button> 						
						<button class='stab_stages_2' ch="PT_Feedback">Feedback</button>						
					</div>
				</div>	
					<div class='row hid'>
						<div class="col-md-12">	
							<div class='row row_style_1 text-center'>	
							<div class='col-md-12'>					
										<button class='stab_stages_2 in_stages' ch="PT_Trends">Log Spread</button> || 
										<button class='stab_stages_2 stab_dis_selec' ch="PT_Trend_Report">XP Spread</button> || 
										<button class='stab_stages_2 in_stages' ch="PT_NonQ_Share">Share of Non-Quant XP</button>
							</div>														
					</div>					
					<hr class="st_hr2">
			<div class='row third-row head' style='margin-top: 10px;'>	
								<?php          
												if(($s_dt) && ($e_dt))
												{
													$start_dt1 = date('d-M-Y', strtotime($s_dt));
													$end_dt1 = date('d-M-Y', strtotime($e_dt));
												}else
												{
													$day = date('Y-m-d H:i:s');
													$day_w = date('w',strtotime($day));
													if($day_w==0)
													{
														$day_w=7;								
													}							
													$week_end = date('Y-m-d H:i:s', strtotime($day.' +'.(7-$day_w).' days'));
													
													$week_start = date('Y-m-d H:i:s', strtotime($week_end.' -1 month'));
													$week_st_w = date('w',strtotime($week_start));
													if($week_st_w==0)
													{
														$week_st_w=7;								
													}							
													$start_dt1 = date('d-M-Y', strtotime($week_start.' -'.($week_st_w-1).' days'));
													$end_dt1 = date('d-M-Y',strtotime($week_end));
													
													$s_dt = date('Y-m-d', strtotime($start_dt1));
													$e_dt = date('Y-m-d', strtotime($end_dt1));
												}		
												
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Start Date: </label>	
															<input id='t_dtpicker' class='s_dt date_fm date-picker' dt='".$s_dt."' value='".$start_dt1."' />
														</span>
													</div>
													<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>End Date: </label>	
															<input id='t_dtpicker' class='e_dt date_fm date-picker' dt='".$e_dt."' value='".$end_dt1."' />
														</span>
													</div>
													</div>
													<div class='row row_style_1 third-row head'>
										<div class='col-md-12 cur-month text-center'>
										<span>	Week (".$start_dt1." to ".$end_dt1.")</span>
												</div>	";
												$titl1='';
												$titl2='';
																								
												?>
							</div>
						</div>
						<div class='row row_style_1'>
							<div class='row row_style_1 scroll-head'>
																	
								<?php
								$pto='';
										echo "<div class='col-md-4'>
												<label class='l_font_fix_3' style='width:100%'>Choose Employee</label>	";
												echo "<select id='sel_emp' class='selectpicker form-control' title='Nothing Selected'  data-live-search=true'>";
												if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($row2['sel_1_id']==$pro_sel_val)
																	{
																		$sel='selected';	
																		$pto=$row2['sel_1_name'];
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
															
												echo "</select>";											
										echo "</div>";
								
								?>
							</div>
						</div>
	
							<div class='row row_style_1'>
								<?php		
								echo "<div class='data'>";	
										$startDateUnix=strtotime($start_dt1);
										$endDateUnix=strtotime($end_dt1);
										
											$currentDateUnix = $startDateUnix;
											$weekNumbers = array();
											while ($currentDateUnix < $endDateUnix) {
												//$y=date('d-Y', ($currentDateUnix));
												$weekNumbers[] = date('Y', (strtotime('+3 days', $currentDateUnix))).date('W', $currentDateUnix);
												$currentDateUnix = strtotime('+1 week', $currentDateUnix);
											}
											//print_r($weekNumbers);
										
								$get_avg_final=array();
								$i=0;
					foreach($weekNumbers AS $inde2)
					{					

								// $inde1="Week ".substr($inde2,4,2)." '".substr($inde2,2,2);	
								$week=substr($inde2,4,2);
								$ye=substr($inde2,0,4);
								 $inde1 = "W ".$week." (".date("d/m", strtotime($ye."-W".$week."-1"))." to ".date("d/m", strtotime($ye."-W".$week."-7")).")"; 			
								
								 	if(!empty($get_avg_exp)){
								 		if(array_key_exists($inde2,$get_avg_exp))
										{	
											$get_avg_final[]=array("week_val"=>$inde1,"# Team Member"=>$get_avg_exp[$inde2]['tem_cnt'],"Avg XP per TM"=>$get_avg_exp[$inde2]['Avg_val'],"Max XP"=>$get_avg_exp[$inde2]['v1'],"Min XP"=>$get_avg_exp[$inde2]['v2'],"Avg XP per TM per day"=>$get_avg_exp[$inde2]['Avg_per_day']);
										}else{
										$get_avg_final[]=array("week_val"=>$inde1,"# Team Member"=>0,"Avg XP per TM"=>0,"Max XP"=>0,"Min XP"=>0,"Avg XP per TM per day"=>0);
									}

								 }
								
								}
											echo "</div>";
											 echo "<div class='t2'>";
											echo "<div class='row row_style_1'>
															<div class='col-md-6'>
															<input class='ch_dt hide' value='".$date_view."' />
																<label class='l_font_fix_3'></label>
																</div>
																<div class='col-md-2 no_dt1 hide'>
																<label>No Records!</label>
															</div>
														</div>";
															echo "<div class='canvas_r2 text-center'>";
															echo '<div style="width: auto;  height: 400px;!important;" id="chartdiv2"></div>';
															echo '<div id="legenddiv2" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:100px;max-height:150px!important;"></div>
											 </div>
											 </div>';
										//}
										?>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	 <script>var base_url ='<?php echo base_url() ?>'</script> 
	 <script>var title_head2 = '<?php echo $titl2 ?>';</script>
	  <script>var get_avg = <?php echo  json_encode($get_avg_final) ?>;</script> 
	   <script>var get_avg_cnt = <?php echo  count($get_avg_final) ?>;</script> 
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/serial.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script type="text/javascript">

	
	
	$("body").on("change","#sel_emp",function(){
		if($(this).val())
		{
			$.ajax({				
				url: base_url+"index.php/User/load_emp_string",
				type: 'post',
				data : {param:"Personal_Trends",pt_emp:$(this).val().toString()},
				success: function(response) {				
						var sdt=$("body").find(".s_dt").attr('dt');
						var edt=$("body").find(".e_dt").attr('dt');
						if(sdt && edt && sdt<=edt)
						{
						window.location = base_url+"index.php/User/load_view_f?a=PT_Trend_Report&s_dt="+sdt+"&e_dt="+edt;
						}
				}
			});
		}
	});

	
	$("body").on("focus", ".s_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1,
					daysOfWeekDisabled: "0,2,3,4,5,6"
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var sdt=moment(ele).format("YYYY-MM-DD");
						$(this).attr('dt',sdt);
						var ele1=Date.parse($('.e_dt').val()); 
						var edt=moment(ele1).format("YYYY-MM-DD");
						
						if(ele && ele1 && (ele<ele1))
						{													
						window.location = base_url+"index.php/User/load_view_f?a=PT_Trend_Report&s_dt="+sdt+"&e_dt="+edt;
						}
						}
					});	
	});
	
		$("body").on("focus", ".e_dt",function(){		
		$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1,
					daysOfWeekDisabled: "1,2,3,4,5,6"
			}).on('changeDate', function(e) {
				if($(this).val())
						{
						var ele=Date.parse($('.s_dt').val()); 
						var sdt=moment(ele).format("YYYY-MM-DD");
						
						var ele1=Date.parse($(this).val()); 
						var edt=moment(ele1).format("YYYY-MM-DD");	
						$(this).attr('dt',edt);						
						if(ele && ele1 && (ele<ele1))
						{								
						window.location = base_url+"index.php/User/load_view_f?a=PT_Trend_Report&s_dt="+sdt+"&e_dt="+edt;
						}
						}
					});	
			});

if(get_avg_cnt!=0)
  { ke2=[];
	ke2[0] ="Max XP";
	ke2[1] ="Min XP";
	ke2[2] ="Avg XP per TM";
	ke2[3] ="Avg XP per TM per day"; 
	ke2[4] ="# Team Member"; 
  	 var chartData2 = get_avg;
	 var chart2;
            AmCharts.ready(function () {
                chart2 = new AmCharts.AmSerialChart();
                chart2.dataProvider = chartData2;
                chart2.categoryField = "week_val";
				chart2.synchronizeGrid=true;
				chart2.addTitle(title_head2);
                chart2.autoMargins = true;
				
                var categoryAxis2 = chart2.categoryAxis;
                categoryAxis2.gridAlpha = 0;
                categoryAxis2.axisAlpha = 0;
                categoryAxis2.gridPosition = "start";
				categoryAxis2.autoGridCount = true;
				categoryAxis2.autoWrap=true;

				var color=["#FF6600","#4CAF50","#9C27B0","#2196F3"];
				var shape=["square","round","triangleUp","triangleDown","round"];
				
				for(i=0;i<=3;i++)
				{
					var graph2 = new AmCharts.AmGraph();
					graph2.valueAxis='v1';
					graph2.title = ke2[i];
					graph2.labelText = "[[value]] XP";
					graph2.valueField = ke2[i];
					graph2.bullet = shape[i];
					graph2.hideBulletsCount = 30;
					graph2.balloonText = "[[category]]<br>[[title]] - <b>[[value]] XP</b>";
					graph2.lineColor = color[i];
					graph2.color = 'black';
					graph2.labelFunction = function(item,label){
					  return label == "0 XP" ? "" : label;
					}
				 graph2.fillAlphas=0;
                chart2.addGraph(graph2);
				if(i==1)
				{
				chart2.hideGraph(graph2);
				}
				}
				
				var graph2 = new AmCharts.AmGraph();
					graph2.valueAxis='v1';
					graph2.title = ke2[i];
					graph2.valueField = ke2[i];
					graph2.labelText = "[[value]] #";
					
					graph2.bullet = "round";
					graph2.hideBulletsCount = 30;
					graph2.balloonText = "[[category]]<br>[[title]] - <b>[[value]] #</b>";
					//graph2.lineColor = color[i];
					graph2.color = 'black';
					graph2.labelFunction = function(item,label){
					  return label == "0" ? "" : label;
					}
				 graph2.fillAlphas=0;
                chart2.addGraph(graph2);
				
					var	valueAxis2 = new AmCharts.ValueAxis();               
						valueAxis2.id='v2';
						valueAxis2.precision=2;
						valueAxis2.position="left";
						valueAxis2.title="XP";
						chart2.addValueAxis(valueAxis2);	
				
				var legend2= new AmCharts.AmLegend();
                legend2.borderAlpha = 0.2;
                legend2.horizontalGap = 10;
                legend2.autoMargins = true;
                legend2.marginLeft = 20;
                legend2.marginRight = 20;
				legend2.useGraphSettings=true;
                chart2.addLegend(legend2, "legenddiv2");
				
				
				chart2.responsive = {
					  "enabled": true
					};
					chart2.write("chartdiv2");
				chart2.export = {
					  "enabled": true,
					  "fileName":title_head2,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 2
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv2')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart2.initHC = false;
					chart2.validateNow();
            });              
			}else{

					$("body").find(".no_dt1").removeClass('hide');
					$("body").find(".canvas_r2").addClass('hide');

				}
				
	
$(window.document).scroll(function() {
        var windowtop = $(window).scrollTop();

        $("body").find(".scroll-head").each(function(i, e) {
            $(this).removeClass("divposition");
            if (windowtop > $(e).offset().top) {
                $(e).addClass("divposition");
                return;
            }
        })
    });

</script>
<body>
</html>