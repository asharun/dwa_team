<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm="Reports";

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
} 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Reports">Overview</button>
						<button class='stab_stages_2 ' ch="PT_Report_Card">Report Card</button>
						<button class='stab_stages_2' ch="PT_Pies">Pies</button> 						
						<button class='stab_stages_2' ch="PT_Trends">Trends</button> 						
						<button class='stab_stages_2 ' ch="PT_Stats">Stats</button> 						
						<button class='stab_stages_2 stab_dis_selec' ch="PT_Feedback">Feedback</button>	
					</div>
				</div>
					<?php 	
						if(($s_dt) && ($e_dt))
												{
													$start_dt1 = date('d-M-Y', strtotime($s_dt));
													$end_dt1 = date('d-M-Y', strtotime($e_dt));
												}else
												{
													$day = date('Y-m-d H:i:s');
													$day_w = date('w',strtotime($day));
													if($day_w==0)
													{
														$day_w=7;								
													}							
													$week_end = date('Y-m-d H:i:s', strtotime($day.' +'.(7-$day_w).' days'));
													
													$week_start = date('Y-m-d H:i:s', strtotime($week_end.' -1 month'));
													$week_st_w = date('w',strtotime($week_start));
													if($week_st_w==0)
													{
														$week_st_w=7;								
													}							
													$start_dt1 = date('d-M-Y', strtotime($week_start.' -'.($week_st_w-1).' days'));
													$end_dt1 = date('d-M-Y',strtotime($week_end));
													
													$s_dt = date('Y-m-d', strtotime($start_dt1));
													$e_dt = date('Y-m-d', strtotime($end_dt1));
												}		
							
							
					?>
					<div class='row hid'>	
						<div class='col-md-12'>						
			<div class='row third-row head' style='margin-top: 10px;'>
												
												<?php
												echo "
										<div class='col-md-12 cur-month text-center'><span>Scatter Plot (".$start_dt1." to ".$end_dt1.")</span>
												</div>	";
												$titl='';
												$titl1='';
												?>
							</div>
							
									
									<div class='row row_style_1  scroll-head'>
									<?php
														$pto='';
														echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3' style='width:100%'>Start Date: </label>	
															<input id='t_dtpicker' class='s_dt date_fm date-picker' dt='".$s_dt."' value='".$start_dt1."' />
														</span>
													</div>
													<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3' style='width:100%'>End Date: </label>	
															<input id='t_dtpicker' class='e_dt date_fm date-picker' dt='".$e_dt."' value='".$end_dt1."' />
														</span>
													</div>
													";
														echo "<div class='col-md-4'>
												<label class='l_font_fix_3' style='width:100%'>Choose Employee</label>	";
												echo "<select id='sel_emp' class='selectpicker form-control' title='Nothing Selected'  data-live-search=true'>";
												if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($row2['sel_1_id']==$pro_sel_val)
																	{
																		$sel='selected';	
																		$pto=$row2['sel_1_name'];
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
															
												echo "</select>";											
										echo "</div>";							
										echo "</div>";
								
				echo '<hr class="st_hr2">';
			
			
			 echo "<div class='t2'>";
											echo "<div class='row row_style_1'>
															<div class='col-md-6'>
															<input class='ch_dt hide' value='".$date_view."' />
																<label class='l_font_fix_3'></label>
																</div>
																<div class='col-md-2 no_dt1 hide'>
																<label>No Records!</label>
															</div>
														</div>";
															echo "<div class='canvas_r2 text-center'>";
															echo '<div style="width: auto;  height: 400px;!important;" id="chartdiv2"></div>';
															//echo '<div id="legenddiv2" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:100px;max-height:150px!important;"></div>
											 echo '</div>
											 </div>';
		
			?>
			</div>
			</div>
			        
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var tt_data2 = <?php echo json_encode($user_plot); ?>;</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/xy.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>

	

	$("body").on("focus", ".s_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var sdt=moment(ele).format("YYYY-MM-DD");
						$(this).attr('dt',sdt);
						var ele1=Date.parse($('.e_dt').val()); 
						var edt=moment(ele1).format("YYYY-MM-DD");
						
						if(ele && ele1 && (ele<ele1))
						{													
						window.location = base_url+"index.php/User/load_view_f?a=PT_Feedback&s_dt="+sdt+"&e_dt="+edt;
						}
						}
					});	
	});
	
		$("body").on("focus", ".e_dt",function(){		
		$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
				if($(this).val())
						{
						var ele=Date.parse($('.s_dt').val()); 
						var sdt=moment(ele).format("YYYY-MM-DD");
						
						var ele1=Date.parse($(this).val()); 
						var edt=moment(ele1).format("YYYY-MM-DD");	
						$(this).attr('dt',edt);						
						if(ele && ele1 && (ele<ele1))
						{								
						window.location = base_url+"index.php/User/load_view_f?a=PT_Feedback&s_dt="+sdt+"&e_dt="+edt;
						}
						}
					});	
			});

			
			
	$("body").on("change","#sel_emp",function(){
		if($(this).val())
		{
			var sdt=$("body").find(".s_dt").attr('dt');
						var edt=$("body").find(".e_dt").attr('dt');
						if(sdt && edt && sdt<=edt)
						{
				$.ajax({				
				url: base_url+"index.php/User/load_emp_string",
				type: 'post',
				data : {param:"Personal_Trends",pt_emp:$(this).val().toString()},
				success: function(response) {						
						window.location = base_url+"index.php/User/load_view_f?a=PT_Feedback&s_dt="+sdt+"&e_dt="+edt;
						
					}
				});		
			}
		}
	});
     
	 
	
myArray2=[];
$.each(tt_data2, function(key,value) {
			  item = {};			  
			// ele=Date.parse(value['dates_v']); 
			 item ["x"]=parseFloat(value['x']);
			 item ["y"]=parseFloat(value['y']);
			 item ["value"]=value['value'];
			  item ["format"]=value['format'];
			 
			  myArray2.push(item);			  
			}); 

	 if(tt_data2!=0)
  { 
            AmCharts.ready(function () {
var chart= new AmCharts.AmXYChart();        
chart.dataProvider = myArray2;
//chart.marginLeft = 35;
chart.startDuration = 1.5;

var xAxis = new AmCharts.ValueAxis();
xAxis.position = "left";
//xAxis.autoGridCount = true;
chart.addValueAxis(xAxis);

var yAxis = new AmCharts.ValueAxis();
yAxis.position = "bottom";
//yAxis.autoGridCount = true;
chart.addValueAxis(yAxis);                

var trendLine = new AmCharts.TrendLine();
// trendLine.initialDate = new Date(2012, 0, 2, 12); // 12 is hour - to start trend line in the middle of the day
// trendLine.finalDate = new Date(2012, 0, 11, 12);
trendLine.initialValue = 0;
trendLine.finalValue = 30;
trendLine.initialXValue = 9;
trendLine.finalXValue = 9;
trendLine.dashLength=3;
//trendLine.lineColor = "#CC0000";
chart.addTrendLine(trendLine);


var trendLine = new AmCharts.TrendLine();

trendLine.initialValue = 0;
trendLine.finalValue = 30;
trendLine.initialCategory = 10;
trendLine.finalCategory = 10;
trendLine.dashLength=3;
//trendLine.lineColor = "#CC0000";
chart.addTrendLine(trendLine);

	var graph2 = new AmCharts.AmGraph();
					graph2.balloonText="Work Hours:<b>[[x]]</b> Confirmed XPs:<b>[[y]]</b><br>Day:<b>[[title]]</b>";
					graph2.bullet= "circle";
					graph2.bulletBorderAlpha= 0.2;
					graph2.bulletAlpha= 0.8;
					graph2.lineAlpha= 0;
					graph2.fillAlphas= 0;
					graph2.valueField= "value";
					graph2.xField = "x";
					graph2.yField = "y";
					graph2.maxBulletSize= 10;
					chart.addGraph(graph2);

                         chart.responsive = {
					  "enabled": true
					};     
chart.write("chartdiv2");
				chart.export = {
					  "enabled": true,
					 // "fileName":title_head2,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 2
						  },
					  "exportTitles" :true,					  
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart.initHC = false;
					chart.validateNow();
            });              
			}else{
					$("body").find(".no_dt1").removeClass('hide');
					$("body").find(".canvas_r2").addClass('hide');

				}
				
				
				
  $(window.document).scroll(function() {
        var windowtop = $(window).scrollTop();

        $("body").find(".scroll-head").each(function(i, e) {
            $(this).removeClass("divposition");
            if (windowtop > $(e).offset().top) {
                $(e).addClass("divposition");
                return;
            }
        })
    });
	</script>
  </body>
</html>