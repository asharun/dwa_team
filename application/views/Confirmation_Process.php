<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
  $file_nm=str_replace('.php','',basename(__FILE__));

$a_right1=str_replace(' ','_',$this->session->userdata('access'));
$access_str1=explode("|",$a_right1);

$a_right=$this->session->userdata('access');
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">

	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>"  type="text/css">

	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/select2.min.css?v=<?= v_num() ?>"  type="text/css">

  </head>
  <body>
 <?php
  $this->load->view("Header.php");
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>

				<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			<div class="col-md-10">
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>
					</div>
			</fieldset>
			</div>
			</div>
			<?php

			echo "<div class='row hid1'>
				<div class='col-md-12'>";

				$check=array("Audit"=>"Audit","Allocation"=>"Allocation","Review"=>"Review","Confirm"=>"Confirm");
				foreach($check as $key=>$value)
				{
					if(in_array($value,$access_str))
					{
						echo "<button class='stab_stages' ch='".$value."'><span>".$key."</span> <span class='badge badge-info'>".$cnf_cnt_dta[0][$value."_cnt"]."<span></button> ";
					}
				}
			echo "<button class='stab_stages stab_dis_selec' ch='Confirmation_Process'><span>All Activities</span></button>
					</div>
			</div>";
			?>
					<div class='row hid'>
						<div class='col-md-12'>
						<?php
						if(($st_dt)&&($en_dt))
						{
							$date_start=date('d-M-Y',strtotime($st_dt));
							$date_end=date('d-M-Y',strtotime($en_dt));
						}else
						{
							$day = date('Y-m-d');
							$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;
							}
							$date_start = date('d-M-Y', strtotime($day.' -'.($day_w-1).' days'));
							$date_end=date('d-M-Y', strtotime($day.' +'.(7-$day_w).' days'));
						}
						echo "<div class='row row_style_3'>";
						echo "<div class='col-md-3'>";
								echo "<label class='l_font_fix_3'>Choose Dept:</label>";
								echo "<select id='sel_dept_1' class='selectpicker form-control' title='Nothing Selected' data-live-search='true'>";

								foreach ($dept_val as $row)
								{
									$sel='';
										if($dept_opt==$row['dept_id'])
										{
											$sel='selected';
										}
									echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
								}
								echo "</select>";
							echo "</div>";
							echo "<div class='col-md-2'>";
								echo "<label class='l_font_fix_3'>Start Date: </label>";
								echo "<input id='t_dtpicker' class='st_dt date_fm date-picker' value='".$date_start."' />";
							echo "</div>";
							echo "<div class='col-md-2'>";
								echo "<label class='l_font_fix_3'>End Date: </label>";
								echo "<input id='t_dtpicker2' class='en_dt date_fm date-picker' value='".$date_end."' />";
							echo "</div>";
							echo "<div class='col-md-2'>";
								echo "<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>";
								echo "<button class='btn add_but gre_but check_CP' type='button'>Submit</button>";
								echo "</div>";
								echo "<div class='col-md-2'>";
								echo "<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>";
								echo "<button class='btn add_but blue_but exp_all' type='button'>Export All Data</button>";
							echo "</div>";
						echo "</div>";
					?>
						<div class='row'>
								<div class='col-md-12 text-center'>

								<?php
								$check1=array("Audit"=>"Audit","Review"=>"Review","Confirm"=>"Confirm");
								foreach($check1 as $key=>$value)
								{
									if(in_array($value,$access_str))
									{
										$ch_val=strtolower(substr($value,0,3));
										echo "<span class='badge but_filter' ch='".$ch_val."_c'>".$key." Details</span>";
									}
								}
								?>

								<div class="pull-right" id="freez-col-option-box"></div>
								</div>
						</div>
						<div  id="toolbar">
							<select class="form-control pull-left">
									<option value="">Export Excel</option>
							</select>
							</div>

						<div id="fixed-col-option-box" class="form-group">
						</div>

						<table class="display table table-bordered table-responsive"  data-show-footer="true" data-footer-style="footerStyle" data-checkbox-header="false" data-toolbar="#toolbar" data-filter-control="true" data-show-export="true"  id="table3" data-search-time-out=500 data-pagination="true" data-search="true" data-show-columns="true" data-show-multi-sort="true">
								<thead>
									<tr>
										  <th data-class="l_font_fix_3" data-formatter="runningFormatter">#</th>
										  <th data-class='l_font_fix_3 hidden resp_id' data-field="response_id">ID</th>
										  <th data-class="l_font_fix_3 hidden act_id" data-field="activity_id_fk">Act Id</th>
										  <th data-sortable="true" data-class="l_font_fix_3 act_n" data-field="day_val" data-formatter="todate_ch" >Date</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="ins_dt" data-formatter="todatetime_ch">Inserted On</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="full_name">User</th>
										  <th data-sortable="true" data-class="l_font_fix_3 act_nm" data-filter-control="input" data-field="activity_name">Activity Name</th>
											<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="wu_name">Work Units</th>
											<th data-sortable="true" data-class="l_font_fix_3" data-field="work_comp">Work Done</th>
											<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="milestone_id">MID</th>
										  <th data-sortable="true" data-class="l_font_fix_3"   data-field="equiv_xp" data-footer-formatter="totalFilledXp">Filled Xp</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="task_desc">Task Desc</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="iss_stat">Issues</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="issue_cat">Issue Category</th>
										  <th data-sortable="true" data-class="l_font_fix_3"   data-field="comp_xp"  data-footer-formatter="totalCompXp">Comp XP</th>
										  <th data-sortable="true" data-class="l_font_fix_3"  data-field="issue_desc">Issue Desc</th>
										  <th data-sortable="true" data-align="center" data-halign="center" data-filter-control="input" data-class="l_font_fix_3" data-formatter="nameFormatter" data-field="tast_stat1">Status</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="audit_yn">Audit Done</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="alloc_yn">Alloc Done</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="review_yn">Review Done</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="conf_yn">Conf Done</th>

										  <th data-sortable="true" data-class="l_font_fix_3 aud_c" data-field="audit_comp">Audit Work Done</th>
										  <th data-sortable="true" data-class="l_font_fix_3 aud_c"  data-field="audit_xp" data-footer-formatter="totalAuditXp">Audit XP</th>
										  <th data-sortable="true" data-class="l_font_fix_3 aud_c" data-filter-control="input" data-field="audit_user">Audit By</th>
										  <th data-sortable="true" data-class="l_font_fix_3 aud_c" data-field="audit_upd_dt" data-formatter="todatetime_ch">Audit On</th>
										  <th data-sortable="true" data-class="l_font_fix_3 aud_c" data-field="audit_desc">Audit Desc</th>
										  <th data-sortable="true" data-class="l_font_fix_3 aud_c" data-field="assignee_desc">Alloc Desc</th>
										  <th data-sortable="true" data-class="l_font_fix_3 aud_c" data-field="rev_assignee">Review Assigned To</th>
										  <th data-sortable="true" data-class="l_font_fix_3 rev_c" data-field="review_xp">Review Feedback</th>
										  <th data-sortable="true" data-class="l_font_fix_3 rev_c" data-filter-control="input" data-field="review_user">Review By</th>
										  <th data-sortable="true" data-class="l_font_fix_3 rev_c" data-field="review_upd_dt" data-formatter="todatetime_ch">Review On</th>
										  <th data-sortable="true" data-class="l_font_fix_3 rev_c" data-field="review_desc">Review Desc</th>
										  <th data-sortable="true" data-class="l_font_fix_3  con_c" data-field="total_xp"  data-footer-formatter="totalConfirmedXp">Confirmed XP</th>
										  <th data-sortable="true" data-class="l_font_fix_3  con_c" data-filter-control="input" data-field="conf_user">Confirmed By</th>
										  <th data-sortable="true" data-class="l_font_fix_3  con_c" data-field="conf_upd_dt" data-formatter="todatetime_ch">Confirmed On</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>

	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-multiple-sort.js?v=<?= v_num() ?>"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/xlsx.core.min.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/FileSaver.min.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/byjus-bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/select2.min.js"></script>
<script>

// To sort date according to date data type
// function dateSorter(a, b) {
	// a = Date.parse(a);
	// b = Date.parse(a);

	// var d1 = new Date(a);
	// var d2 = new Date(b);

	// if (d1 < d2) return -1;
	// if (d1 > d2) return 1;
	// return 0;
// }

function addArray(a, b) {
	if (a == null) {
		a = 0;
	}
	if (b == null) {
		b = 0;
	}
    return Math.floor((parseFloat(a) + parseFloat(b)) * 100) / 100;
}

// Calculate XP's based on prop
function calculateXp(data, prop)
{
	var d=[];
	for (var key in data) {
		if (data.hasOwnProperty(key)) {
			d.push(data[key][prop]);
		}
	}
	return d.reduce(addArray, 0);
}

function totalFilledXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "<strong>Filled XP:<br> " +total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+ "</strong>";
            }

function totalCompXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "<strong>Comp XP:<br> " +total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+ "</strong>";
            }

function totalConfirmedXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "<strong>Confirmed XP:<br> " +total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+ "</strong>";
            }

			function totalAuditXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "<strong>Audit XP:<br> " +total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+ "</strong>";
            }

// function totalConfirmedXp(data, d=[]) {
	// var prop = 'total_xp';
	// return "<strong>Confirmed XP: " + calculateXp(data, prop) + "</strong>";
// }

// function totalCompXp(data, d=[]) {
	// var prop = 'comp_xp';
	// return "<strong>Comp XP: " + calculateXp(data, prop) + "</strong>";
// }

// function totalAuditXp(data, d=[]) {
	// var prop = 'audit_xp';
	// return "<strong>Audit XP: " + calculateXp(data, prop) + "</strong>";
// }

function runningFormatter(value, row, index) {
index++; {
return index;
}
}

function todate_ch(value, row, index) {
	var date_cck = new Date(value*1000);
	{
	return moment(date_cck).format("DD/MM/YYYY");
	}
}

function todatetime_ch(value, row, index) {
	if(value)
	{
	var date_cck = new Date(value*1000);
	{
		//console.log(value,date_cck);
	return moment(date_cck).format("DD-MMM-YYYY HH:mm:ss");
	}
	}else{
		return "";
	}
}
//console.log(moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"));


$('#toolbar').find('select').change(function () {
            $('#table3').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val(),
                toolbarAlign:"right"

            });
        });

	function nameFormatter(value, row) {
	     var t=value.split('|');
        return "<i class='glyphicon glyphicon-stop "+t[1]+"'></i> "+t[0];
    }


	$("body").on("focus", ".st_dt",function(){
	$(this).datepicker({
					format: 'dd-M-yyyy',
					yearRange: "-1:+1",
					weekStart:1
			});
	});
	$("body").on("focus", ".en_dt",function(){
	$(this).datepicker({
					format: 'dd-M-yyyy',
					yearRange: "-1:+1",
					weekStart:1
			});
	});
	var ele=Date.parse($(".st_dt").val());
	var st_dt=moment(ele).format("YYYY-MM-DD");
	var ele1=Date.parse($(".en_dt").val());
	var en_dt=moment(ele1).format("YYYY-MM-DD");


	$("body").on("change","#sel_dept_1",function(){
			var ele2=Date.parse($(".st_dt").val());
			var ele12=Date.parse($(".en_dt").val());
			var a_diff  = new Date(ele12 - ele2);
			var a_days  = a_diff/1000/60/60/24;
		if(ele2 && ele12 && (ele2<ele12))
		{
			if(a_days<=30)
			{
			var st_dt1=moment(ele2).format("YYYY-MM-DD");
			var en_dt1=moment(ele12).format("YYYY-MM-DD");
			var dept=$(this).val();
			var str='';
							str=str+"&dept="+dept;
			window.location = base_url+"index.php/User/load_view_f?a=Confirmation_Process&s_dt="+st_dt1+"&e_dt="+en_dt1+str;
			}else{
				alert('Pls choose 1 month interval!');
			}
		}else{
			alert('Check the start and end dates!');
		}
	});

	$("body").on("click", ".check_CP",function(){
			var ele2=Date.parse($(".st_dt").val());
			var ele12=Date.parse($(".en_dt").val());
		if(ele2 && ele12 && (ele2<ele12))
		{
			var st_dt1=moment(ele2).format("YYYY-MM-DD");
			var en_dt1=moment(ele12).format("YYYY-MM-DD");
			var dept=$("body").find("#sel_dept_1").val();
			var str='';
						if(dept)
						{
							str=str+"&dept="+dept;
						}
			window.location = base_url+"index.php/User/load_view_f?a=Confirmation_Process&s_dt="+st_dt1+"&e_dt="+en_dt1+str;
		}else{
			alert('Check the start and end dates!');
		}
	});

	function ConvertToCSV(objArray) {
            var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
            var str = '';

			var hide=["id","response_id","activity_id_fk"];

			var headin=["MID","Activity Name,Work Units,User,Date,Inserted On,Work Done,Filled XP,Task Desc,Issues,Issue Category,Comp XP,Issue Desc,Status,Audit Done,Alloc Done,Review Done,Conf Done,Audit Work Done,Audit XP,Audit By,Audit On,Audit Desc,Alloc Desc,Review Assigned To,Review Feedback,Review By,Review On,Review Desc,Confirmed XP,Confirmed By,Confirmed On"];

			 str += headin + '\r\n';
            for (var i = 0; i < array.length; i++) {
                var line = '';
            for (var index in array[i]) {
				if(hide.indexOf(index)<0)
				{
					var ch_rec=(array[i][index])?array[i][index].replace(/"/g, '\''):array[i][index];
					//console.log(('jn lsd').replace(/"/g, '\''));
					if (line != '')
					{
						line += ',';
					}
					if(index=="tast_stat1")
					{
						line += ch_rec.substring(0, ch_rec.indexOf("|"));
					}
					else if(index=="day_val")
					{
						line += ("'"+ch_rec+"'"=="'null'")?"":'"'+moment(new Date(ch_rec*1000)).format("DD/MM/YYYY")+'"';
					}
					else if(index=="audit_upd_dt" || index=="review_upd_dt" || index=="conf_upd_dt" ||index=="ins_dt"  )
					{
							line += ("'"+ch_rec+"'"=="'null'")?"":'"'+moment(new Date(ch_rec*1000)).format("DD-MMM-YYYY HH:mm:ss")+'"';
					}
					else{
					line += ("'"+ch_rec+"'"=="'null'")?"":'"'+ch_rec+'"';
					}
				}
            }

            str += line + '\r\n';
        }
        return str;
    }

	var dataere='';
	

$.ajax({
       url: base_url+"index.php/User/load_conf_boots?a=Confirmation_Process&st="+st_dt+"&en="+en_dt+"&dept="+dep_opt,
       dataType: 'json',
       success: function(response) {
		    //var response =JSON.parse(JSON.stringify(response).replace(/null/g, '""'));
		   window.reponsedata=response;
           $('#table3').bootstrapTable({
              data: response,
			  stickyHeader: true,
			  pageSize: getRowsNumberForTable(),
			  toolbarAlign:"left",
			  onPostBody:function() {
				$("body").find('.fixed-table-body').scroll(function(){					
				//$("body").find('#table-sticky-header-sticky-header-container').find('thead').parent().scrollLeft($(this).scrollLeft());
				$("body").find('.fix-sticky').find('thead').parent().scrollLeft($(this).scrollLeft());
				});

				   $('.but_filter').each(function(index){
				 	  var column = "table ." + $(this).attr("ch");
					 //  console.log(column);
				 	  if($(this).hasClass('badge-warning'))
				 	  {
				 		$(column).removeClass('hidden');
				 	  }else
				 	  {
				 		  $(column).addClass('hidden');
				 	  }
				   });
				   //BYJU.bootstrapTable.renderFooter(response);
			  },
			  onSearch: function() {
				BYJU.bootstrapTable.freezCleanUp();
			  }
           });

	$('body').find(".aud_c").addClass('hidden');
	$('body').find(".rev_c").addClass('hidden');
	$('body').find(".con_c").addClass('hidden');
	dataere=response;
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });

	$(".exp_all").click(function(){
		da_re=ConvertToCSV(dataere);
		saveAs(new Blob([da_re]),"export.csv");
	});

	$(".but_filter").click(function(){
		$(this).toggleClass('badge-warning');
    var column = "table ." + $(this).attr("ch");
    $(column).toggleClass('hidden');
	});

	$(document).ready(function() {
		  var $table = $('#table3');
        	$('#export').click(function () {
          	$table.tableExport({
              type: 'csv',
              escape: false
            });
          });
		  
		setTimeout(function() {
			//console.log("dfds");
			BYJU.bootstrapTable.initFreez();
		}, 2000);
	});

</script>
  </body>
</html>