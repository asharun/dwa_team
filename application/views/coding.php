<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm="Team_Member_Info";
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css">
		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<div class="col-md-2 c_row">			
				<div class='row mfkdsf text-center'>
					<div class='col-md-12'>
					<div class="md_t col_men">
					<i class="glyphicon glyphicon-th h_imh"></i> 
					</div>
					<div class="md_t md_t_h">
					<i class="glyphicon glyphicon-home h_imh"></i> Home</div>
					</div>
				</div>
			
			<?php 
			$i=0;
			$ac_arr=array();
			foreach($access_mst AS $a_r)
			{
				$dg[$a_r->access_name]=$a_r->icon;
				$ac_arr[$i++]=$a_r->access_name;$color[$a_r->access_name]=$a_r->tab_color;$name[$a_r->access_name]=$a_r->tab_name;
			}			
			foreach($access_str AS $rw)
			{
				if(in_array($rw,$ac_arr))
			{
				$cl='';
				$d_id=str_replace(' ', '_', $rw);
				if($d_id==$file_nm)
				{
					$cl=' m_col_act';					
				}
				echo "<div class='row m_row'>
						<div class='col-md-12 m_col ".$cl."' data_id='".$d_id."' style='background-color:".$color[$rw].";'>						
							<i class='glyphicon ".$dg[$rw]." img_hi'></i>
							<h5>".$name[$rw]."</h5>	
						</div>			
					</div>";	
			}					
			}					
			?>
				</div>
				
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Team_Member_Info">Data</button>
						<button class='stab_stages_2' ch="Employee_Info">Employee</button>
						<button class='stab_stages_2 stab_dis_selec' ch="Team_Graphs">Activity-wise Graph</button>
						<button class='stab_stages_2' ch="Team_Split_Graph">Member-wise Graph</button> 
						<button class='stab_stages_2' ch='Status_Report'>Achieved Tgt</button>
						<button class='stab_stages_2' ch="Graph_Breakdown">Breakdown</button>
						<button class='stab_stages_2' ch="Graph_Summary">Summary</button>
						<button class='stab_stages_2' ch="Graph_Sum_Sc">Summary Score</button>
					</div>
				</div>
		
				<!--div class='row row_style_1'>
			<div class="col-md-4">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-6 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_0" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Projected XP</span>					
					</div>
					<div class="col-md-6 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Actual XP</span>
					
				</div>	
			</fieldset>				
			</div>			
			</div-->
					<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));
							
							if($day_w==0)
							{
								$day_w=7;								
							}
							$day_fm=date('d-M-Y',strtotime($day));
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));							
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));							
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));
							
							
					?>
					<div class='row hid'>	
						<div class='col-md-12'>	
			
			<div class='row third-row head'>
												
												<?php
												if(!$level_opt)
															{
																$level_opt=0;
															}
															$lel_val=array("Activity","Projects","Category","Element","Action","Trade");
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Choose Date: </label>	
															<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
														</span>
													</div>
										<div class='col-md-6 cur-month text-center'><span>".$lel_val[$level_opt]."-wise Break-up (".$week_st." to ".$week_end.")</span>
												</div>	";
												$titl='';
												?>
							</div>
							
									<div class='row row_style_1' id='c_find'>	
											<div class='col-md-12'>					
														<a class='arr2 pull-left' ch='Team_Graphs' w_val="<?= $w_prev ?>">&laquo; Prev</a>						
														<a class='arr2 pull-right' ch='Team_Graphs' w_val="<?= $w_nxt ?>">Next&raquo;</a>					
											</div>														
									</div>
									<div class='row row_style_1'>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Level:</label>
																<select id='sel_2345' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option data-hidden="true"></option>';
															
															foreach ($lel_val as $key=>$value)
															{
																$sel='';
																	if($level_opt==$key)
																	{
																		$sel='selected';																		
																	}
																echo "<option value='".$key."' ".$sel.">".$value."</option>";
															}
															
																?>
															</select>
															</div>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Project:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															
															echo '<option data-hidden="true"></option>';
															if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($pro_sel_val==$row2['sel_1_id'])
																	{
																		$sel='selected';	
																	$titl=$row2['sel_1_name']." - ".$lel_val[$level_opt]."-wise Break-up (".$week_st." to ".$week_end.")";																		
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>										
										</div>
			<?php
			echo "<div class='row row_style_2'><div class='col-md-12'><div class='no_dt hide'>No Records!</div></div></div>";
						echo "<div class='data'>";
						//print_r($proj_xp);
						if($tm_data)
						{
							$cate_fil=array();
							foreach($tm_data AS $row1)
							{
								if($level_opt==0)
								{
									$tsdff=explode('_', $row1['activity_name'], 2);
									$f_repl=$tsdff[1];
								}else{
									$f_repl=$row1['activity_name'];
								}	
								$cate_fil[]=$f_repl;
									
									echo "<h5 class='hide params1' user_id='".$row1['full_name']."' act_id='".$f_repl."'>".$row1['equiv_xp']."</h5>";
									//echo "<h5 class='hide params1' user_id='".$row1['full_name']."' act_id='".$row1['activity_name']."'>".$row1['equiv_xp']."</h5>";
								}
								$result = array_unique($cate_fil);
								
							foreach($result AS $row2)
							{
								echo '<label class="checkbox-inline filter-name"><input class="filter-position" type="checkbox" checked value="">'.$row2.'</label>';
							}
						}else
						{
							$tm_data=null;
						}
								echo "</div>";
							echo "<div class='canvas_r text-center'>";
							echo '<div style="width: auto; height: 400px;" id="chartdiv"></div>';
		
			?>
			<div id="legenddiv" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>
			        
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var title_head = '<?php echo $titl ?>';</script>
	<script>var tt_data = '<?php echo sizeof($tm_data) ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/serial.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>

		
$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var ele=Date.parse($(".ch_dt").val()); 
			var date_v=moment(ele).format("YYYY-MM-DD");
						var lvl=$("body").find("#sel_2345").val();
						var str='';
						str=str+"&pro="+$(this).val();
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}						
			window.location = base_url+"index.php/User/load_view_f?a=Team_Graphs&date_view="+date_v+str;
		}
	});
    
	$("body").on("change","#sel_2345",function(){
		if($(this).val())
		{
			var ele=Date.parse($(".ch_dt").val()); 
			var date_v=moment(ele).format("YYYY-MM-DD");
			var pro_v=$("body").find("#sel_1234").val();
						var str='';
						str=str+"&lvl="+$(this).val();
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}						
			window.location = base_url+"index.php/User/load_view_f?a=Team_Graphs&date_view="+date_v+str;
		}
	});
	
$("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1"
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						var pro_v=$("body").find("#sel_1234").val();
						var lvl=$("body").find("#sel_2345").val();
						var str='';
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}						
							window.location = base_url+"index.php/User/load_view_f?a=Team_Graphs&date_view="+date_v+str;
						}
					});	
	});
	
	
	
     var myArray =[];
	 var a_id = [];
   
var p=[];
    $(".params1").each(function(index) {
		item = {};
		item ["act"] =  $(this).attr('act_id');
        item ["work"] = $(this).text();
		item ["user"] = $(this).attr('user_id');
         myArray.push(item);
		  p[index]=$(this).attr('user_id');
    });

var group_to_values = myArray.reduce(function (obj, item) {
    obj[item.act] = obj[item.act] || [];
	item2 = {};
	item2 ["wh"] =item.work;
		item2 ["user"] = item.user;
    obj[item.act].push(item2);
    return obj;
}, {});

var groups = Object.keys(group_to_values).map(function (key) {
    return {act: key, work: group_to_values[key]};
});
  
  var k_array=[];

  $.each( groups, function( key, value ) {
  item23 = {};
		item23 ["act"] = value.act.toString();
 
	$.each( value.work, function( key1, value1 ) {
		item23 [value1.user.toString()] = value1.wh.toString();
   
	});
	k_array.push(item23);
});
   
  //console.log(k_array);
  
  ke = jQuery.unique(p);
    if(tt_data!=0)
  {
	 var chart;
	 function getFilteredData(chartData) {
  var filters = [];  
   $("input[type=checkbox]:checked").each(function(index) {
	   filters[index] = $(this).closest(".filter-name").text();	   
   });
  
  var newData = [];  
  chartData.forEach(function(element){
	  if(filters.indexOf(element['act'])>-1)
	  {
		newData.push(element);		  
	  }
  });
  var t=[];
  for(i in newData)
  {
	  for(j in newData[i])
	  {
		  if(j!='act')
		  {
			t.push(j);
		  }
	  }
  }
  ke = jQuery.unique(t);
    return newData;
}

var chartData = k_array;

$("body").on("change",".filter-position",function(){
 var Provider = getFilteredData(chartData); 
chart.clear();
 prepare(Provider);
 // for(i in chart.graphs)
// {
 // chart.removeGraph(chart.graphs[i]);
// }

// chart.removeLegend();

 // $.each( ke, function( key2, value2 ) {
                // var graph = new AmCharts.AmGraph();
                // graph.title = value2;
                // graph.labelText = "[[percents]]%";
                // graph.balloonText = "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> XP ([[percents]]%)</span>";
                // graph.valueField = value2;
                // graph.type = "column";
                // graph.lineAlpha = 0;
				// //graph.labelRotation= 90;
                // graph.fillAlphas = 0.8;
                // //graph.lineColor = "#C72C95";
                // chart.addGraph(graph);
				// });
// chart.validateData();
});
prepare(k_array);
function prepare(k_array)
{
            //AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = k_array;
                chart.categoryField = "act";				
				chart.addTitle(title_head);
                chart.autoMargins = true;
                
                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridAlpha = 0;
				
                categoryAxis.axisAlpha = 0;
				categoryAxis.stackByValue = true;
				//"stackByValue": true,
                categoryAxis.gridPosition = "start";
				categoryAxis.autoGridCount = false;
				categoryAxis.gridCount = k_array.length;
				//categoryAxis.labelRotation = "0";
				categoryAxis.labelFunction= function(valueText, serialDataItem, categoryAxis) {
      if (valueText.length > 20)
	  {
		return valueText.substring(0, 20) + '...';
	  }
      else
	  {
        return valueText;
	  }
    }
				categoryAxis.autoWrap=true;
				
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.stackType = "100%"; // this line makes the chart 100% stacked
                
                valueAxis.labelsEnabled = true;
                chart.addValueAxis(valueAxis);
				ke.sort();
				$.each( ke, function( key2, value2 ) {
                var graph = new AmCharts.AmGraph();
                graph.title = value2;
                graph.labelText = "[[percents]]%";
                graph.balloonText = "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> XP ([[percents]]%)</span>";
                graph.valueField = value2;
                graph.type = "column";
                graph.lineAlpha = 0;
				//graph.labelRotation= 90;
                graph.fillAlphas = 0.8;
                chart.addGraph(graph);
				});
					
                // LEGEND
                var legend = new AmCharts.AmLegend();
                legend.borderAlpha = 0.2;
                legend.horizontalGap = 10;
                legend.autoMargins = true;
                legend.marginLeft = 20;
                legend.marginRight = 20;
                chart.addLegend(legend, "legenddiv");

				chart.responsive = {
					  "enabled": true
					};
					
					chart.write("chartdiv");
				chart.export = {
					  "enabled": true,
					  "fileName": title_head,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 1
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart.initHC = false;
					chart.validateNow();
            //});
}
			}else{
	  $("body").find(".no_dt").removeClass('hide');
  } 
	</script>
  </body>
</html>