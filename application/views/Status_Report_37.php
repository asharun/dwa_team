<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
  $file_nm='Targets';

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
		<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>"  type="text/css">
	
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
					<div class='col-md-12'>					
					<button class='stab_stages' ch='Targets'>Set Targets</button> 
					<button class='stab_stages stab_dis_selec' ch='Status_Report_37'>Suggested Targets: Past 3 & 7 day Status</button>
					<?php 
					$role_id            = $this->session->userdata('role_id');
					if($role_id==1)
					{
					echo "<button class='stab_stages' ch='Week_Targets'>Tgt Changes</button>
					<button class='stab_stages' ch='All_Targets'>All Targets</button>";
					}?>
					</div>
				</div>													
					<div class='row hid'>	
						<div class='col-md-12'>	
						
						<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}						
						
									$day_w  = date('w', strtotime($day));
								if ($day_w == 0) {
									$day_w = 7;
								}				
								$st_dt = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
								$e_dt   = date('Y-m-d', strtotime($day . ' +' . (7 - $day_w) . ' days'));				
							
							$day_fm=date('d-M-Y',strtotime($day));							
							$week_st = date('d-M', strtotime($st_dt));
							$week_end = date('d-M', strtotime($e_dt));													
					?>
			
			<div class='row third-row head'>
							</div>
							
									
											<div class='row row_style_1'>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Dept:</label>
															<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php
															foreach ($dept_val as $row)
															{
																$sel='';
																	if($dept_opt==$row['dept_id'])
																	{
																		$sel='selected';																		
																	}
																echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
															}
															?>
															</select>
														</div>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Project:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 	
															echo '<option data-hidden="true"></option>';
															if($pro_sel_dta)
															{
																foreach ($pro_sel_dta as $row2)
																{
																	$sel='';
																		if($pro_sel_val==$row2['sel_1_id'])
																		{
																			$sel='selected';																		
																		}
																	echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
																}
															}
																?>
															</select>
														</div>
														
										</div>
								<hr class="st_hr2">
								<div id="toolbar" > 
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="all">Export All</option>
									<option value="selected">Export Selected</option>
							</select>
					</div>
							<table class="display table table-bordered table-responsive" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table" data-search-time-out=500 data-pagination="true" data-search="true">
										<thead>
											<tr>
											  <th data-class="l_font_fix_3" data-formatter="runningFormatter">#</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="activity_name">Activity Name</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="wu_name">Work Units</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="quantity_done7">Qty</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="final_xp7">XP</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="std_tgt">Set Tgt</th>											  
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="date_3">3 day Range</th>											  
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="achieved_tgt3">3 day Tgt</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="date_7">7 day Range</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="achieved_tgt7">7 day Tgt</th>
											  
											</tr>
										</thead>
									</table>						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script>
	var ele=Date.parse($(".ch_dt").val()); 
	var date_v=moment(ele).format("YYYY-MM-DD");	
	
	function runningFormatter(value, row, index) {
index++; {
return index;
}
}

$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Project",dept:$(this).val()},
				success: function(response){				
					$('#sel_1234').html(response).selectpicker('refresh');
				}
			});
		}	
	});

$('#toolbar').find('select').change(function () {
            $('#table').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });
		
$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var str='';
						var pro_v=$(this).val();						
						str=str+"&pro="+pro_v;
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
			window.location = base_url+"index.php/User/load_view_f?a=Status_Report_37"+str;
		}
	});
	
	var pr='';
	if($("#sel_1234").find("option:selected").val())
	{
		pr=$("#sel_1234").find("option:selected").val();
	}
	
	var deptid='';
	var deptid=$("body").find("#sel_dept_1").val();
	
	
	$.ajax({
       url: base_url+"index.php/User/load_table_boots?a=Status_Report_37&pro="+pr+"&dept="+deptid,
       dataType: 'json',
       success: function(response) {
           $('#table').bootstrapTable({
              data: response,
			  stickyHeader: true
           });
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });

	
	</script>
  </body>
</html>