<?php
$user_id=$this->session->userdata('user_id');
   $file_nm='Floor_View';
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				
				<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			<div class="col-md-10">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>					
					</div>
			</fieldset>				
			</div>			
			</div>
			
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Floor_View">Weekly Summary</button>
						<button class='stab_stages_2' ch="FV_Wow">Productivity WoW</button>	
						<button class='stab_stages_2 ' ch="FV_Spread">Trends</button>
						<button class='stab_stages_2' ch="FV_Feed">Scatter</button>	
						<button class='stab_stages_2 stab_dis_selec' ch="FV_Summary">Descriptives</button>
						<button class='stab_stages_2' ch="FV_Drilldown">Pies</button>					
						<button class='stab_stages_2' ch="FV_Member_Info">Weekly Report</button>
						<button class='stab_stages_2' ch="FV_Contributor">Projects & Members</button>
						
					</div>
				</div>
					<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));
							
							if($day_w==0)
							{
								$day_w=7;								
							}
							$day_fm=date('d-M-Y',strtotime($day));
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));							
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));							
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));					
					?>
					<div class='row hid'>	
						<div class='col-md-12'>	
			
			<div class='row third-row head'>
												
												<?php
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Choose Date: </label>	
															<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
														</span>
													</div>
										<div class='col-md-6 cur-month text-center'><span>Break-up (".$week_st." to ".$week_end.")</span>
												</div>	";
												$titl1='';
												$titl2='';
																								
												?>
							</div>
							
									<div class='row row_style_1' id='c_find'>	
											<div class='col-md-12'>					
														<a class='arr pull-left' ch='FV_Summary' w_val="<?= $w_prev ?>">&laquo; Prev</a>						
														<a class='arr pull-right' ch='FV_Summary' w_val="<?= $w_nxt ?>">Next&raquo;</a>					
											</div>														
									</div>
									<div class='row row_style_1'>	
							<div class='col-md-4'>
								<label class='l_font_fix_3'>Choose Dept:</label>
								<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
								<?php
								foreach ($dept_val as $row)
								{
									$sel='';
										if($dept_opt==$row['dept_id'])
										{
											$sel='selected';																		
										}
									echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
								}
								?>
								</select>
							</div>
						</div>
									
			<?php
			
			echo "<div class='t3'>";
			echo "<div class='row row_style_1'>
							<div class='col-md-6'>
								<label class='l_font_fix_3'>Weekly Output/TM and Number of Team Members:-</label>
								</div>
								<div class='col-md-2 no_dt3 hide'>
								<label>No Records!</label>
							</div>
						</div>";
						echo "<div class='data'>";
						$titl3='';
						if($avg)
						{
							$titl3="Weekly Output/TM and Number of Team Members (".$week_st." to ".$week_end.")";																	
							foreach($avg AS $row3=>$key3)
									{
									echo "<h5 class='hide params3' v1='".$key3['Avg']."' v2='".$key3['Mem']."'>".$row3."</h5>";
									}
						}
								echo "</div>";
							echo "<div class='canvas_r3 text-center'>";
							echo '<div style="width: auto; height: 400px;!important;" id="chartdiv3"></div>';
							echo '<div id="legenddiv3" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>';	
			
			echo "<div class='t2'>";
			echo "<div class='row row_style_1'>
							<div class='col-md-6'>
								<label class='l_font_fix_3'>Top and Bottom Quartile Share:-</label>
								</div>
								<div class='col-md-2 no_dt2 hide'>
								<label>No Records!</label>
							</div>
						</div>";
						echo "<div class='data'>";
						if($Q13per)
						{
							$titl2="Top and Bottom Quartile Share (".$week_st." to ".$week_end.")";																	
							foreach($Q13per AS $row1=>$value1)
									{									
									echo "<h5 class='hide params2' v1='".$value1['Bottom']."' v2='".$value1['Top']."'>".$row1."</h5>";
									}
						}else
						{
							$Q13per=null;
						}
								echo "</div>";
							echo "<div class='canvas_r2 text-center'>";
							echo '<div style="width: auto;  height: 400px;!important;" id="chartdiv2"></div>';
							echo '<div id="legenddiv2" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>';	
echo "<div class='t5'>";
			echo "<div class='row row_style_1'>
							<div class='col-md-6'>
								<label class='l_font_fix_3'>Efficiency & Non Quantifiable Share:-</label>
								</div>
								<div class='col-md-2 no_dt5 hide'>
								<label>No Records!</label>
							</div>
						</div>";
						echo "<div class='data'>";
						if($avg)
						{
							$titl5="Efficiency (".$week_st." to ".$week_end.")";																	
							foreach($avg AS $row3=>$key3)
									{
									echo "<h5 class='hide params5' v1='".$key3['Avg_per_day']."' v2='".$key3['nq_su']."'>".$row3."</h5>";
									}
						}
								echo "</div>";
							echo "<div class='canvas_r5 text-center'>";
							echo '<div style="width: auto;  height: 400px;!important;" id="chartdiv5"></div>';
							echo '<div id="legenddiv5" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>';			
				echo "<div class='t1'>";
			echo "<div class='row row_style_1'>
							<div class='col-md-6'>
								<label class='l_font_fix_3'>Highest and Lowest XP:-</label>
								</div>
								<div class='col-md-2 no_dt1 hide'>
								<label>No Records!</label>
							</div>
						</div>";			echo "<div class='data'>";
						if($tm_data1)
						{
							$titl1="Highest and Lowest XP (".$week_st." to ".$week_end.")";																	
							foreach($tm_data1 AS $key=>$row1)
										{											
									echo "<h5 class='hide params1' v1='".round($row1['v1'],2)."' v2='".round($row1['v2'],2)."'>".$key."</h5>";
								
										}
						}else
						{
							$tm_data1=null;
						}
								echo "</div>";
							echo "<div class='canvas_r1 text-center'>";
							echo '<div style="width: auto; height: 400px;!important;" id="chartdiv1"></div>';
							echo '<div id="legenddiv1" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>';
			?>		
			</div>			        
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var title_head1 = '<?php echo $titl1 ?>';</script>
	<script>var tt_data1 = '<?php echo sizeof($tm_data1) ?>';</script>
	<script>var title_head2 = '<?php echo $titl2 ?>';</script>
	<script>var tt_data2 = '<?php echo sizeof($Q13per) ?>';</script>
	<script>var title_head3 = '<?php echo $titl3 ?>';</script>
	<script>var tt_data3 = '<?php echo sizeof($avg) ?>';</script>
	<script>var title_head5 = '<?php echo $titl5 ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/serial.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>
    
	$("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");	
						var str='';
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}						
						window.location = base_url+"index.php/User/load_view_f?a=FV_Summary&date_view="+date_v+str;
						
						}
					});	
	});
	
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
						var ele=Date.parse($("body").find(".ch_dt").val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						var str='';
						var dept=$(this).val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						window.location = base_url+"index.php/User/load_view_f?a=FV_Summary&date_view="+date_v+str;			
		}	
	});
	

     var myArray1 =[];
	 var myArray2 = [];
	 var myArray3 = [];
	 var myArray5 = [];
   
    $(".params1").each(function(index) {
		item = {};
		item ["act"] =  $(this).text();
        item ["Highest"] = $(this).attr('v2');
		item ["Lowest"] = $(this).attr('v1');
         myArray1.push(item);
    });

	 $(".params2").each(function(index) {
		item = {};
		item ["act"] =  $(this).text();
        item ["Bottom Share"] = $(this).attr('v1');
		item ["Top Share"] = $(this).attr('v2');
         myArray2.push(item);
    });
	
	 $(".params3").each(function(index) {
		item = {};
		item ["act"] =  $(this).text();
        item ["Weekly Output/TM"] = $(this).attr('v1');
		item ["Team Members"] = $(this).attr('v2');
         myArray3.push(item);
    });
	
	$(".params5").each(function(index) {
		item = {};
		item ["act"] =  $(this).text();
        item ["Efficiency"] = $(this).attr('v1');
		item ["Non Quantifiable Share"] = $(this).attr('v2');
         myArray5.push(item);
    });
	
	ke1=[];
	ke1['0'] ="Highest";
	ke1['1'] ="Lowest";
	//console.log(ke1);
    if(tt_data1!=0)
  {
	var chartData1 = myArray1;
	 var chart1;
            AmCharts.ready(function () {
                chart1 = new AmCharts.AmSerialChart();
                chart1.dataProvider = chartData1;
                chart1.categoryField = "act";
				//chart1.columnWidth=0.1;
				//chart1.columnWidth=0.3;
				chart1.addTitle(title_head1);
                chart1.autoMargins = true;
				
                var categoryAxis1 = chart1.categoryAxis;
                categoryAxis1.gridAlpha = 0;
                categoryAxis1.axisAlpha = 0;
                categoryAxis1.gridPosition = "start";
				categoryAxis1.autoGridCount = true;
				//categoryAxis1.gridCount = 10;
				categoryAxis1.autoWrap=true;				
				
					var graph1 = new AmCharts.AmGraph();
                graph1.title = ke1[0];
                graph1.labelText = "[[value]]";
                graph1.valueField = ke1[0];
                graph1.type = "column";
                graph1.lineAlpha = 0;
				graph1.fillColors = '#FF6600';
				graph1.balloonText = "[[category]]<br>[[title]] - <b>[[value]] XP</b>";
				graph1.color = 'black';
				graph1.lineColor = '#FF6600';
				//graph1.lineColor = 'black';
		         graph1.fillAlphas = 1;
                chart1.addGraph(graph1);
				
					var graph1 = new AmCharts.AmGraph();
                graph1.title = ke1[1];
                graph1.labelText = "[[value]]";
                graph1.valueField = ke1[1];
                graph1.type = "column";
				graph1.balloonText = "[[category]]<br>[[title]] XP - <b>[[value]] XP</b>";
                graph1.lineAlpha = 0;
				graph1.fillColors = '#B0DE09';
				graph1.color = 'black';
				graph1.lineColor = '#B0DE09';
		         graph1.fillAlphas = 1;
                chart1.addGraph(graph1);
				
				// $.each( ke1, function( key2, value2 ) {
                // var graph = new AmCharts.AmGraph();
                // graph.title = value2;
				// graph.id=value2;
                // graph.labelText = "[[value]] XP";
                // graph.balloonText = "[[category]]<br><span style='font-size:14px;'>[[title]] XP - <b>[[value]]</b></span>";
                // graph.valueField = value2;
                // graph.type = "column";
                // graph.lineAlpha = 1;
                // graph.fillAlphas = 0.8;
                // chart1.addGraph(graph);
				// });
				
				   var valueAxis1 = new AmCharts.ValueAxis();
                // valueAxis1.stackType = "regular";
                // //valueAxis1.maximum=100;
				valueAxis1.precision=2;
                //valueAxis1.labelsEnabled = true;
                chart1.addValueAxis(valueAxis1);
				
				var legend1= new AmCharts.AmLegend();
                legend1.borderAlpha = 0.2;
                legend1.horizontalGap = 10;
                legend1.autoMargins = true;
                legend1.marginLeft = 20;
                legend1.marginRight = 20;
                chart1.addLegend(legend1, "legenddiv1");
				
				
				chart1.responsive = {
					  "enabled": true
					};
					chart1.write("chartdiv1");
				chart1.export = {
					  "enabled": true,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 1
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv1')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart1.initHC = false;
					chart1.validateNow();
            });              
			}else{
					$("body").find(".no_dt1").removeClass('hide');
					$("body").find(".canvas_r1").addClass('hide');
				}
				
  var ke2=[];
	ke2['0'] ="Bottom Share";
	ke2['1'] ="Top Share";
    if(tt_data2!=0)
  {
	var chartData2 = myArray2;
	 var chart2;
            AmCharts.ready(function () {
                chart2 = new AmCharts.AmSerialChart();
                chart2.dataProvider = chartData2;
                chart2.categoryField = "act";
				
				chart2.addTitle(title_head2);
                chart2.autoMargins = true;
                var categoryAxis2 = chart2.categoryAxis;
                categoryAxis2.gridAlpha = 0;
                categoryAxis2.axisAlpha = 0;
                categoryAxis2.gridPosition = "start";
				categoryAxis2.autoGridCount = true;
				categoryAxis2.autoWrap=true;				
				
				var graph2 = new AmCharts.AmGraph();
                graph2.title = ke2[0];
                graph2.labelText = "[[value]]%";
                graph2.valueField = ke2[0];
                graph2.type = "column";
                graph2.lineAlpha = 0;
				graph2.fillColors = '#FF6600';
				graph2.balloonText = "[[category]]<br>[[title]] - <b>[[value]] %</b>";
				graph2.color = 'black';
				graph2.lineColor = '#FF6600';
				graph2.lineColor = 'black';
		         graph2.fillAlphas = 1;
                chart2.addGraph(graph2);
				
					var graph2 = new AmCharts.AmGraph();
                graph2.title = ke2[1];
                graph2.labelText = "[[value]]%";
                graph2.valueField = ke2[1];
                graph2.type = "column";
				graph2.balloonText = "[[category]]<br>[[title]] - <b>[[value]] %</b>";
                graph2.lineAlpha = 0;
				graph2.fillColors = '#B0DE09';
				graph2.color = 'black';
				graph2.lineColor = '#B0DE09';
		         graph2.fillAlphas = 1;
                chart2.addGraph(graph2);
			
				
				   var valueAxis2 = new AmCharts.ValueAxis();
                 valueAxis2.stackType = "regular";
                valueAxis2.maximum=100;
				
				valueAxis2.minimum=0;
				valueAxis2.precision=2;
                chart2.addValueAxis(valueAxis2);
				
				var legend2= new AmCharts.AmLegend();
                legend2.borderAlpha = 0.2;
                legend2.horizontalGap = 10;
                legend2.autoMargins = true;
                legend2.marginLeft = 20;
                legend2.marginRight = 20;
                chart2.addLegend(legend2, "legenddiv2");
				
				
				chart2.responsive = {
					  "enabled": true
					};
					chart2.write("chartdiv2");
				chart2.export = {
					  "enabled": true,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 2
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv2')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart2.initHC = false;
					chart2.validateNow();
            });              
			}else{
					$("body").find(".no_dt2").removeClass('hide');
					$("body").find(".canvas_r2").addClass('hide');
				}
								
	ke3=[];
	ke3['0'] ="Weekly Output/TM";
	ke3['1'] ="Team Members";
    if(tt_data3!=0)
  {
	var chartData3 = myArray3;
	 var chart3;
            AmCharts.ready(function () {
                chart3 = new AmCharts.AmSerialChart();
                chart3.dataProvider = chartData3;
                chart3.categoryField = "act";
				chart3.synchronizeGrid=true;
				chart3.addTitle(title_head3);
                chart3.autoMargins = true;
				
                var categoryAxis3 = chart3.categoryAxis;
                categoryAxis3.gridAlpha = 0;
                categoryAxis3.axisAlpha = 0;
                categoryAxis3.gridPosition = "start";
				categoryAxis3.autoGridCount = true;
				categoryAxis3.autoWrap=true;				
				
				var graph3 = new AmCharts.AmGraph();
				graph3.valueAxis='v1';
                graph3.title = ke3[0];
                graph3.labelText = "[[value]] XP";
                graph3.valueField = ke3[0];
				graph3.bullet = "round";
                graph3.hideBulletsCount = 30;
				graph3.lineColor = '#FF6600';
				graph3.balloonText = "[[category]]<br>[[title]] - <b>[[value]] XP</b>";
				graph3.color = 'black';
		         graph3.fillAlphas = 0;
                chart3.addGraph(graph3);
				
					var graph3 = new AmCharts.AmGraph();
					graph3.valueAxis='v1';
					graph3.title = ke3[1];
					graph3.labelText = "[[value]]";
					graph3.valueField = ke3[1];
					graph3.bullet = "square";
					graph3.hideBulletsCount = 30;
					graph3.balloonText = "[[category]]<br>[[title]] - <b># [[value]]</b>";
					graph3.lineColor = '#B0DE09';
					graph3.color = 'black';
		         graph3.fillAlphas = 0;
                chart3.addGraph(graph3);
				
					var	valueAxis3 = new AmCharts.ValueAxis();               
						valueAxis3.id='v1';
						valueAxis3.precision=2;
						valueAxis3.axisColor="#FF6600";
						valueAxis3.axisThickness=2;
						valueAxis3.position="left";
						chart3.addValueAxis(valueAxis3);
					var	valueAxis3 = new AmCharts.ValueAxis();
						valueAxis3.id='v2';
						valueAxis3.precision=2;
						valueAxis3.axisColor="#B0DE09";
						valueAxis3.axisThickness=2;
						valueAxis3.position="right";
						chart3.addValueAxis(valueAxis3);
				
				
				var legend3= new AmCharts.AmLegend();
                legend3.borderAlpha = 0.2;
                legend3.horizontalGap = 10;
                legend3.autoMargins = true;
                legend3.marginLeft = 20;
                legend3.marginRight = 20;
                chart3.addLegend(legend3, "legenddiv3");
				
				
				chart3.responsive = {
					  "enabled": true
					};
					chart3.write("chartdiv3");
				chart3.export = {
					  "enabled": true,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 3
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv3')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart3.initHC = false;
					chart3.validateNow();
            });              
			}else{
					$("body").find(".no_dt3").removeClass('hide');
					$("body").find(".canvas_r3").addClass('hide');
				}
				
				
				ke5=[];
	ke5['0'] ="Efficiency";
	ke5['1'] ="Non Quantifiable Share";
    if(tt_data3!=0)
  {
	var chartData5 = myArray5;
	 var chart3;
            AmCharts.ready(function () {
                chart3 = new AmCharts.AmSerialChart();
                chart3.dataProvider = chartData5;
                chart3.categoryField = "act";
				chart3.synchronizeGrid=true;
				chart3.addTitle(title_head5);
                chart3.autoMargins = true;
				
                var categoryAxis3 = chart3.categoryAxis;
                categoryAxis3.gridAlpha = 0;
                categoryAxis3.axisAlpha = 0;
                categoryAxis3.gridPosition = "start";
				categoryAxis3.autoGridCount = true;
				categoryAxis3.autoWrap=true;				
				
				var graph3 = new AmCharts.AmGraph();
				graph3.valueAxis='v1';
                graph3.title = ke5[0];
                graph3.labelText = "[[value]] XP";
                graph3.valueField = ke5[0];
				graph3.bullet = "round";
                graph3.hideBulletsCount = 30;
				graph3.lineColor = '#FF6600';
				graph3.balloonText = "[[category]]<br>[[title]] - <b>[[value]] XP</b>";
				graph3.color = 'black';
		         graph3.fillAlphas = 0;
                chart3.addGraph(graph3);
				
					var graph3 = new AmCharts.AmGraph();
					graph3.valueAxis='v1';
					graph3.title = ke5[1];
					graph3.labelText = "[[value]]%";
					graph3.valueField = ke5[1];
					graph3.bullet = "square";
					graph3.hideBulletsCount = 30;
					graph3.balloonText = "[[category]]<br>[[title]] - <b>[[value]]%</b>";
					graph3.lineColor = '#B0DE09';
					graph3.color = 'black';
		         graph3.fillAlphas = 0;
                chart3.addGraph(graph3);
				
					var	valueAxis3 = new AmCharts.ValueAxis();               
						valueAxis3.id='v1';
						valueAxis3.precision=2;
						valueAxis3.axisColor="#FF6600";
						valueAxis3.axisThickness=2;
						valueAxis3.position="left";
						chart3.addValueAxis(valueAxis3);
					var	valueAxis3 = new AmCharts.ValueAxis();
						valueAxis3.id='v2';
						valueAxis3.precision=2;
						valueAxis3.axisColor="#B0DE09";
						valueAxis3.axisThickness=2;
						valueAxis3.position="right";
						chart3.addValueAxis(valueAxis3);
				
				
				var legend3= new AmCharts.AmLegend();
                legend3.borderAlpha = 0.2;
                legend3.horizontalGap = 10;
                legend3.autoMargins = true;
                legend3.marginLeft = 20;
                legend3.marginRight = 20;
                chart3.addLegend(legend3, "legenddiv5");
				
				
				chart3.responsive = {
					  "enabled": true
					};
					chart3.write("chartdiv5");
				chart3.export = {
					  "enabled": true,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 3
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv5')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart3.initHC = false;
					chart3.validateNow();
            });              
			}else{
					$("body").find(".no_dt5").removeClass('hide');
					$("body").find(".canvas_r5").addClass('hide');
				}
$(window.document).scroll(function() {
        var windowtop = $(window).scrollTop();

        $("body").find(".scroll-head").each(function(i, e) {
            $(this).removeClass("divposition");
            if (windowtop > $(e).offset().top) {
                $(e).addClass("divposition");
                return;
            }
        })
    });

	</script>
  </body>
</html>