<div id="gradesModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:30%">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Weekly Feedback Grades</b></h4>
      </div>
      <div class="modal-body">
          <form action="" class="form store-grade-form" data-user-id="<?php echo $this->session->userdata('user_id') ?>">
            <div class="form-group">
              <label for="">Grade</label>
              <input type="number" class="form-control" name="grade" required>
            </div>
            <div class="form-group">
              <label for="">Value</label>
              <input type="text" class="form-control" name="value" required>
            </div> 
            <!--div class="form-group">
              <label for="">Active</label>
              <input type="checkbox" class="form-control" name="active" required>
            </div--> 
          </form> 
          <button class="grade-btn" onclick="BYJU.userFeedback.storeGrade('store-grade-form')">Add New</button>

          <hr>
          <div class="weekly-feedback-panel"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>