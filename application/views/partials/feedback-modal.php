<button type="submit" class="btn add_but gre_but feedback-button" data-toggle="modal" data-target="#feedbackModal"><strong>Give Feedback</strong></button>

<?php
$rol=$this->session->userdata('role_id');
if($rol==1)
{
echo '<small><a href="" class="btn add_but blue_but feedback-button" data-toggle="modal"  data-target="#gradesModal">Grades List</a></small>';
}
?>

<!-- Modal -->
<div id="feedbackModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:40%">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b style="color:#813588">Give Feedback</b></h4>
      </div>
        <div class="modal-body">
           <div class="row">
                <div class="col-sm-12 feedback-panel text-center">
                  
                </div>
           </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default f_disb" onclick="BYJU.userFeedback.storeFeedback('store-feedback-form')">Save</button>

      </div>
    </div>

  </div>
</div>

<?php $this->load->view('partials/feedback-grades-modal.php'); ?>