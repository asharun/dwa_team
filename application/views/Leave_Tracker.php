<?php
$user_id=$this->session->userdata('user_id');
  $file_nm=str_replace('.php','',basename(__FILE__));

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"> 
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	
  </head>
  <body>
 <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));
							$day_fm=date('d-M-Y',strtotime($day));
							$mm_day=date('Y-m-d',strtotime($day));
							$date=date('01-m-Y',strtotime($day));
							$mnth_prev=date('Y-m-d', strtotime($date.' -1 month'));
							$mnth_nxt =date('Y-m-d', strtotime($date.' +1 month'));
							
							$curmnth=strtoupper(date("F Y", strtotime($date)));
							$prev_mon=date("M Y", strtotime($mnth_prev));
							$next_mon=date("M Y", strtotime($mnth_nxt));
							$daysInMonth = date("t", strtotime($date));
									
							$rows = 1;
							$offset = date("w", strtotime($date))-1;
							if($offset==-1)
							{
								$offset=6;								
							}
						?>
			<div class="col-md-10 c_row">
			
					<div class="row row_style_1">
						<div class="col-md-10">	
							<fieldset class="scheduler-border">
								<legend class="legli">
									Legend
								</legend>
								<?php 
								if($lev_type)
								{
									foreach($lev_type as $rw)
									{
									echo "<div class='col-md-3 cl_lege' va='l_c_".$rw['l_type_id']."'>										
									<i class='glyphicon glyphicon-stop' style='color:".$rw['color_code'].";background:".$rw['b_code'].";border:1px solid #ccc;'></i>
									<span class='fil_val' style='font-size: 12px;'>".$rw['leave_type_name']."</span>					
									</div>";
									}
								}
								?>
							</fieldset>				
						</div>			
					</div>
					<div class='modal fade open_col' id='show_gl_col1'>										
											<div class='modal-dialog' role='document'>
												<div class='modal-content'>
												  <div class='modal-body' id='modal_edit'>																										
												 </div>								  
												</div>
											  </div>
									</div>	
							
												
								<?php
								echo "<div class='row third-row row_style_3'>";
									echo "<div class='col-md-4'>
								<button class='btn add_but red_but leave_ins' t_date='".$mm_day."' type='button'>Add Leave</button>
								<button class='btn add_but blue_but leave_week_off' t_date='".$mm_day."' type='button'>Adjust WeekOffs</button>
								</div></div>";
								echo "<div class='row third-row row_style_3 hid'>";
									echo "<div class='col-md-3'>
									<span>
									<label class='l_font_fix_3'>Choose Date: </label>	
										<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
										</span>
									</div>";									
									echo "<div class='col-md-6 cur-month1 text-center'>
										<span>".$curmnth."</span>
									<i class='glyphicon glyphicon-calendar curr_leave_view'></i></div></div>";
								?>
							<div class='row third-row row_style_3' id='c_find'>	
									<div class='col-md-12'>					
												<a ch='Leave_Tracker' class='arr3 pull-left' w_val="<?= $mnth_prev ?>">&laquo; <?= $prev_mon ?></a>
												<a ch='Leave_Tracker' class='arr3 pull-right' w_val="<?= $mnth_nxt ?>"><?= $next_mon ?> &raquo;</a>					
									</div>														
							</div>
							<div class='cal' style="border:1px solid black;">
							<div class='row'>				
								<div class='col-md-12 week seven-cols hidden-xs hidden-sm'>
									<div class='col-md-1 week_cell'>MONDAY</div>
									<div class='col-md-1 week_cell'>TUESDAY</div>
									<div class='col-md-1 week_cell'>WEDNESDAY</div>
									<div class='col-md-1 week_cell'>THURSDAY</div>
									<div class='col-md-1 week_cell'>FRIDAY</div>
									<div class='col-md-1 week_cell'>SATURDAY</div>	
									<div class='col-md-1 week_cell'>SUNDAY</div>
								</div>
							</div>
									<?php
								echo "<div class='row'> 
										<div class='col-md-12 day seven-cols'>";							
								$curr_day=date('Y-m-d');
								
								 for($i = 1; $i <= $offset; $i++)
									{
										echo "				<div class='col-md-1 tab-cell  hidden-xs hidden-sm'>
																<div class='day-off'></div>
															</div>";
									}
									
								for($day = 1; $day <= $daysInMonth; $day++)
								{
									
									if( ($day + $offset - 1) % 7 == 0 && $day != 1)
									{
										echo "</div></div><div class='row'> 
																	<div class='col-md-12 day seven-cols'>";
										$rows++;
									}

																	$day1=str_pad($day, 2, '0', STR_PAD_LEFT);
																	$date_wek=date($day1.'-m-Y',strtotime($date));
																	$week_name=date("D", strtotime($date_wek));
																	
																	$date_chk = date('Y-m-d', strtotime($date_wek));
																		
																		$highl='';
																		$high2='';
																		
																		if($date_chk==$curr_day)
																		{
																			$highl='highl';
																			$high2='highl_mob';
																		}
																		echo "<div class='col-md-1 tab-cell'>";
																		echo "<div class='day-num-sm visible-xs-inline-block visible-sm-inline-block'>
																		 <span class='pull-left ".$high2."'>".$day1." | ".$week_name."</span>
																		 <span class='pull-right'>
																		 <i class='glyphicon glyphicon-edit t_edit edit_le' t_date='".$date_chk."'></i>
																		</span>
																		 <span class='count_list' id='cnt_r'></span></div>";												
																		echo "<div class='day-num hidden-xs hidden-sm'><span class='".$highl."'>".$day1 . "</span>
																		<span class='pull-right'>
																		<i class='glyphicon glyphicon-edit t_edit edit_le' t_date='".$date_chk."'></i>
																		</span>
																		</div>";
																		
																		echo "<div class='month_tab'>";
										if($l_data)
										{
											//print_r($l_data);
											foreach($l_data AS $key=>$re)
											{
											if($key==$date_chk)
												{
											echo "<table class='tbldata' id='tblData'>";
												echo "<tbody>";
												echo "<tr><td class='td_al'>";
											foreach($re AS $rewo)
												{
												echo "<button type='button' class='btn event-lt l_c_".$rewo['w_id']."' style='background-color:".$rewo['w_b_code'].";color:white;border-color:".$rewo['w_color_code'].";' data-container = '.ic_cont' data-placement='auto bottom' data-toggle='popover' data-html='true'  date_val='".date("M d,Y", strtotime($date_wek))."' data-content=\"";
														
																		// echo "<b><center class='act_nm_break'>"  . str_replace("\xA0", " ", $rewo['activity_name']) . "</center></b><br>";										
																	echo "<span class='pull-left'> ". date("M d,Y", strtotime($date_wek)). "</span></center>";
																		// echo"<hr class='style1_pop'>";
																		// echo "<b>Quantity Done: </b>" . str_replace("\xA0", " ", $rewo['work_comp']) . "<br>";
																		// echo "<b>Standard Target: </b>" . str_replace("\xA0", " ", $rewo['std_tgt_val']) . "<br>";
																		// echo "<b>Filled XP: </b>" . str_replace("\xA0", " ", $rewo['equiv_xp']) . "<br>";
																		// echo "<b>Comp XP: </b>" . str_replace("\xA0", " ", $rewo['comp_xp']) . "<br>";
																		// echo "<b>Audit Desc: </b>" . str_replace("\xA0", " ", $rewo['audit_desc']) . "<br>";
																		// echo "<b>Total XP: </b>" . str_replace("\xA0", " ", $rewo['tot_xp']) . "<br>";
														echo "\">";																				
												echo $rewo['w_val']."</button>";
												}
												echo "</td></tr>";
											echo "</tbody>";
											echo "</table>";
												}
											}
										}
											echo"</div>";			
										echo "</div>";
										}
							while( ($day + $offset) <= $rows * 7)
							{
								
								echo "<div class='col-md-1 tab-cell  hidden-xs hidden-sm'>
								<div class='day-off'></div>
								</div>";
								$day++;		
							}
			?>													
			</div>
		</div>
		</div>
	</div>
</div>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-multiselect.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script>
	
	
	
	  $('body').popover({
            html: true,
            trigger: "manual"

        })
        .on("mouseenter", '[data-toggle="popover"]', function() {
            var _this = this;

            var color = $(this).css("border-left-color");
			var c1=$(this).css("background-color");

            if (!$(this).hasClass("badge")) {
                $(this).css("background-color", color);
                $(this).css("color", "black");
				$(this).attr('b_c',c1);
            }

            $(this).popover("show");
            var pop_id = $(this).attr("aria-describedby");

            $("body").find('#' + pop_id).find(".popover-title").css("background", color);

            $("body").on("mouseleave", '.popover', function() {
                $(_this).popover('hide');
            });
        }).on("mouseleave", '[data-toggle="popover"]', function() {
            var _this = this;
            setTimeout(function() {
                if (!$("body .popover:hover").length) {
                    $(_this).popover("hide");
                }
            }, 10);
            if (!$(this).hasClass("badge")) {
                var color1 = $(this).attr('b_c');
                $(this).css("background-color", color1);
                $(this).css("color", "white");
            }
        });

		
		$("body").on("click", ".arr3",function(){
		var w_val = $(this).attr("w_val");
		var ch = $(this).attr("ch");
		if($("body").find("#sel_1234").val())
		{
			var pro_v=$("body").find("#sel_1234").val();
			window.location = base_url+"index.php/User/load_view_f?a="+ch+"&date_view="+w_val+"&pro="+pro_v;
		}else{
			window.location = base_url+"index.php/User/load_view_f?a="+ch+"&date_view="+w_val;
		}
	});
	
	$("body").on("click", ".edit_le",function(){
		var t_date=$(this).attr('t_date');
		 $.ajax({
				url: base_url+"index.php/User/leave_add?t_date="+t_date,
				success: function(data){
					$('#modal_edit').html(data);
					$("body").find('.selectpicker').selectpicker('refresh');
					$("body").find('.date-picker').datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1"
			});
			$("#show_gl_col1").modal({
            keyboard: false
			});	
		}});	
	});

		$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Project_overall",dept:$(this).val()},
				success: function(response){				
					$("body").find('.pro_po_sel').html(response).multiselect("destroy").multiselect({enableFiltering: true,
		includeSelectAllOption: true,
		selectAllJustVisible: false,
					numberDisplayed: 1});
				}
			});
		}	
	});
	
	$("body").on("click", ".leave_ins",function(){
		var t_date=$(this).attr('t_date');
		 $.ajax({
				url: base_url+"index.php/User/leave_new?t_date="+t_date,
				success: function(data){
					$('#modal_edit').html(data);
					$("body").find('.selectpicker').selectpicker('refresh');
					$("body").find('.date-picker').datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1"
			});
			$("#show_gl_col1").modal({
            keyboard: false
				});	
				$(".pro_po_sel").multiselect({
		enableFiltering: true,
		includeSelectAllOption: true,
		selectAllJustVisible: false,
		numberDisplayed: 1
	});
			}
		});	
	});
	
	$('body').on('change','.ll_typ', function() {
		if($(this).val())
		{
			if($(this).find("option:selected").text()=="Comp Off")
			{				
				var elesss=$(this).closest(".l_sty");
				$(elesss).find('.comp_s').text("Comp off On");
				$(elesss).find('.comp_e').text("Worked On");
				var half_bl=$(elesss).find('.half_d_bl');
				$(half_bl).find("input[name='option_38'][val='0']").prop('checked',true);
				$(half_bl).find("input[name='option_38'][val='1']").prop('checked',false);
				// $(half_bl).find("input[name='option_38'][val='0']").prop('checked');
				// $(half_bl).find("input[name='option_38'][val='1']").removeAttr('checked');
				// $(half_bl).find("input[name='option_38'][val='1']").removeProp('checked');
				$(half_bl).addClass('hide');				
			}else{
				var elesss=$(this).closest(".l_sty");
				$(elesss).find('.comp_s').text("Start Date");
				$(elesss).find('.comp_e').text("End Date");
				$(elesss).find('.half_d_bl').removeClass('hide');
			}
			
		}
	});

	$("body").on("click", ".leave_week_off",function(){
		//var t_date=$(this).attr('t_date');
		 $.ajax({
				url: base_url+"index.php/User/week_off",
				success: function(data){
					$('#modal_edit').html(data);
					$("body").find('.selectpicker').selectpicker('refresh');
					$("body").find('.date-picker').datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1"
			});
			$("#show_gl_col1").modal({
            keyboard: false
			});	
		}});	
	});
	
	
	$("body").on("click", ".week_ins",function(){
		 $.ajax({
				url: base_url+"index.php/User/week_off_new",
				success: function(data){
					$(data).insertBefore('#app_marker');
					$("body").find('.selectpicker').selectpicker('refresh');
					$("body").find('.date-picker').datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1"
					});
				}
		});
	});
	
	$("body").on("click", ".week_off_sub",function(){	
				$(this).attr("disabled", true);					
				var obj = [];
				//var l_id='';						
				$("body").find('.l_sty').each(function(index){
					if($(this).find('.week_chn').prop('checked'))
					{
						var C=[];
					C[0]=$(this).attr('tab_id');
					C[1]=($(this).attr('tab_id'))?"Update":"Insert";
					C[2]=($(this).find('.day_typ').find("option:selected").val());
					if($(this).find('.l_s_dt').val())
					{
					var ele=Date.parse($(this).find('.l_s_dt').val()); 
					C[3]=moment(ele).format("YYYY-MM-DD");
					}else{
						var ele=Date.parse($(this).find('.l_s_dt').text()); 
					C[3]=moment(ele).format("YYYY-MM-DD");
					}
					if($(this).find('.l_e_dt').val())
					{
					var ele1=Date.parse($(this).find('.l_e_dt').val()); 
					C[4]='"'+moment(ele1).format("YYYY-MM-DD")+'"';
					}else{
					C[4]='null';	
					}
					C[6]="Pending";
					C[7]=$(".u_in").attr('user');
					obj.push(C);
					}
				});	
				//console.log(obj);
				if(obj.length > 0)
				{
					$.post(base_url+"index.php/User/week_off_adju",{C : obj}, function(data, textStatus) {
							if (textStatus == 'success') {								
								if(data)
								{
						   $.post(base_url+"index.php/Notify/leave_mail",{C : data,param:"W",po:""}, function(data, textStatus) {
									if (textStatus == 'success') {	
										//alert(data);									
										window.location.reload();
										}
								});
								}
							}else{
								alert(data);
							}
					});
				}else{
					alert("Please select atleast one checkbox!");
					$(this).attr("disabled", false);
				}
	});	
	
	$("body").on("click", ".leave_new_log",function(){	
				$(this).attr("disabled", true);					
				var obj = [];		
				var p_id='';
				$("body").find('.l_sty').each(function(index){
						var C=[];
					C[1]=$(this).find('.ll_typ').find("option:selected").val();
					C[2]=(($(this).find('.ll_typ').find("option:selected").text()=="Comp Off")?1:null);
					var ele=Date.parse($(this).find('.l_s_dt').val()); 
					C[3]=moment(ele).format("YYYY-MM-DD");
					var ele1=Date.parse($(this).find('.l_e_dt').val()); 
					C[4]=moment(ele1).format("YYYY-MM-DD");
					C[5]=$(this).find(".reason").val();
					C[6]="Pending";
					C[7]=$(".u_in").attr('user');
					p_id=$(this).find(".pro_po_sel").val();
					C[8]=$("body").find("input[name='option_38']:checked").attr('val');
					C[9]=p_id;
					obj.push(C);
				});				
				if(obj)
				{
					$.post(base_url+"index.php/User/ins_leave",{C : obj}, function(data, textStatus) {
							if (data) {
								//alert(p_id);
								$.post(base_url+"index.php/Notify/leave_mail",{C : data,param:"C",po:p_id}, function(data, textStatus) {
									if (textStatus == 'success') {	
										//alert(data);									
										window.location.reload();
										}
								});
							}else{
								alert(data);
							}
					});
				}else{
					$(this).attr("disabled", false);
				}
	});	
	
	$("body").on("click", ".leave_sub_log",function(){	
				$(this).attr("disabled", true);					
				var obj = [];
					var l_id='';
				$("body").find('.l_sty').each(function(index){
						var C=[];
					C[0]=$(this).attr('tab_id');	
					l_id=l_id+","+$(this).attr('tab_id');
					C[1]=$(this).find('.ll_typ').find("option:selected").val();
					C[2]=(($(this).find('.ll_typ').find("option:selected").text()=="Comp Off")?1:null);
					var ele=Date.parse($(this).find('.l_s_dt').val()); 
					C[3]=moment(ele).format("YYYY-MM-DD");
					var ele1=Date.parse($(this).find('.l_e_dt').val()); 
					C[4]=moment(ele1).format("YYYY-MM-DD");
					C[5]=$(this).find(".reason").val();
					C[6]="Pending";
					C[7]=$(".u_in").attr('user');
					C[8]=$(this).find("input[class='status_ch']:checked").attr('val');
					C[9]=$(this).find("input[class='kl_stl']:checked").attr('val');
					obj.push(C);
				});	
			
			l_id=l_id.substring(1);				
				if(obj)
				{
					$.post(base_url+"index.php/User/update_leave",{C : obj}, function(data, textStatus) {
							if (textStatus == 'success') {
						   $.post(base_url+"index.php/Notify/leave_mail",{C : l_id,param:"C",po:""}, function(data, textStatus) {
									if (textStatus == 'success') {	
										//alert(data);							
									   window.location.reload();
										}
								});
							}else{
								alert(data);
							}
					});
				}else{
					$(this).attr("disabled", false);
				}
	});	
	
	$("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						window.location = base_url+"index.php/User/load_view_f?a=Leave_Tracker&date_view="+date_v;
						}
					});			
	});
	</script>
  </body>
</html>