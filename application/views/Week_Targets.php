<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
     $file_nm='Targets';

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

$role_id            = $this->session->userdata('role_id');
if($role_id==1)
{
if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
}else{
	 redirect('user/login_view');
}

 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
		<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">	
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>
			
				<div class="col-md-10 c_row">
						
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages' ch='Targets'>Set Targets</button> 
					<button class='stab_stages' ch='Status_Report_37'>Suggested Targets: Past 3 & 7 day Status</button>
					<?php 
					
					if($role_id==1)
					{
					echo "<button class='stab_stages stab_dis_selec' ch='Week_Targets'>Tgt Changes</button>
					<button class='stab_stages' ch='All_Targets'>All Targets</button>";
					}?>
					</div>
				</div>
					<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));				
							if($day_w==0)
							{
								$day_w=7;								
							}
							$day_fm=date('d-M-Y',strtotime($day));
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));							
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));							
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));					
					?>
					<div class='row hid'>	
						<div class='col-md-12'>	
							<div class='row third-row head'>												
												<?php
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Choose Date: </label>	
															<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
														</span>
													</div>
													<div class='col-md-6 cur-month text-center'><span>Week (".$week_st." to ".$week_end.")</span>
													</div>";
												$titl1='';
												$titl2='';
												$titl3='';												
												?>
							</div>							
							<div class='row row_style_1' id='c_find'>	
									<div class='col-md-12'>					
												<a class='arr pull-left' ch='Week_Targets' w_val="<?= $w_prev ?>">&laquo; Prev</a>						
												<a class='arr pull-right' ch='Week_Targets' w_val="<?= $w_nxt ?>">Next&raquo;</a>					
									</div>														
							</div>
							
								<div id="toolbar" > 
							<select class="form-control">
									<option value="all">Export All</option>
									<option value="">Export Page</option>
							</select>
					</div>
			
					<table class="display table table-bordered table-responsive" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table" data-search-time-out=500 data-pagination="true" data-search="true">
										<thead>
											<tr>
											  <th data-class="l_font_fix_3" data-formatter="runningFormatter">#</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="dept_name">Department Name</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="project_name">Project Name</th>
											  <th data-sortable="true" data-class="l_font_fix_3 act_nm" data-filter-control="input" data-field="activity_name">Activity Name</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="wu_name">Work Units</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="prev_change_desc">Change Desc</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="tgt_val">Target</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="ins_user">Inserted By</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="ins_dt">Inserted On</th>											  
											  <th data-sortable="true" data-class="l_font_fix_3"  data-filter-control="input" data-field="start_date">Start Date</th>
											  <th data-sortable="true" data-class="l_font_fix_3"  data-filter-control="input" data-field="end_date">End Date</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="upd_user">Updated By</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="upd_dt">Updated On</th>
											</tr>
										</thead>
									</table>
					
					</div>			        
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script>    
	
		function runningFormatter(value, row, index) {
index++; {
return index;
}
}
$('#toolbar').find('select').change(function () {
            $('#table').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });

	$("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");						
						
						window.location = base_url+"index.php/User/load_view_f?a=Week_Targets&date_view="+date_v;
						}
					});	
	});
	
	var ele=Date.parse($("body").find(".ch_dt").val()); 
	var date_v=moment(ele).format("YYYY-MM-DD");
	
	$.ajax({
       url: base_url+"index.php/User/load_data_pull?a=Week_Targets&date_view="+date_v,
       dataType: 'json',
       success: function(response) {
           $('#table').bootstrapTable({
              data: response,
			  stickyHeader: true,
			  exportDataType : 'all'
           });
		 // $('#table').bootstrapTable('checkInvert');
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });
	
	// var ele=Date.parse($("body").find(".ch_dt").val()); 
						// var date_v=moment(ele).format("YYYY-MM-DD");
						//console.log(date_v);
	
	// $("body").on("change","#sel_dept_1",function(){
		// if($(this).val())
		// {
						// var ele=Date.parse($("body").find(".ch_dt").val()); 
						// var date_v=moment(ele).format("YYYY-MM-DD");
						// var str='';
						// var dept=$(this).val();
						// if(dept)
						// {
							// str=str+"&dept="+dept;
						// }
						// window.location = base_url+"index.php/User/load_view_f?a=Floor_View&date_view="+date_v+str;			
		// }	
	// });
	
	// $('#table').bootstrapTable({
			// pageSize:20,
			// stickyHeader:true,
			// exportDataType:'all'
	// });
	</script>
  </body>
</html>