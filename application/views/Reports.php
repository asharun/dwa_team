<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
  $file_nm=str_replace('.php','',basename(__FILE__));
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"> 
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	<style>
	
	
	.multiselect-container {
        width: 100% !important;
		max-height: 400px;
    overflow: auto;
    }
	
	.multiselect.dropdown-toggle,.bt_cont_pt{
		width:100%;
	}
	</style>
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				
				<div class="col-md-10 c_row">
				
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2 stab_dis_selec' ch="Reports">Overview</button>
						<button class='stab_stages_2' ch="PT_Report_Card">Report Card</button> 						
						<button class='stab_stages_2' ch="PT_Pies">Pies</button> 						
						<!--button class='stab_stages_2' ch="PT_Trends">Trends</button> 						
						<button class='stab_stages_2' ch="PT_Stats">Stats</button--> 						
						<button class='stab_stages_2' ch="PT_Feedback">Feedback</button>
					</div>
				</div>	
					<?php 	
												
							$st_dt= date('d-M-y', strtotime($start_dt));
							$en_dt= date('d-M-y', strtotime($end_dt));	
							$titl3= "(".$st_dt." to ".$en_dt.")";
				$start_dt = !empty($startDateFilter) ? $startDateFilter : $start_dt;
                $end_dt = !empty($endDateFilter) ? $endDateFilter : $end_dt;							
					?>			
								
				
			<?php
			$day_fm1=date('d-M-Y',strtotime($start_dt));
			$day_fm2=date('d-M-Y',strtotime($end_dt));
				echo "<div class='row hid'>	
						<div class='col-md-12'>	
						<div class='row third-row head row_style_1 text-center'>";
								echo "<div class='col-md-4'>
												<label class='l_font_fix_3' style='width:100%'>Choose Employee</label>	";
												echo "<select id='sel_emp' class='form-control' title='Nothing Selected'  multiple='multiple' data-live-search=true'>";
												if($pro_sel_dta)
															{
															//	echo "<option value='0' >All Projects</option>";
															$rt_val=explode(",",$pro_sel_val);
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if(in_array($row2['sel_1_id'],$rt_val))
																	{
																		$sel='selected';																	
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
															
												echo "</select>";											
										echo "</div>";
							echo "<div class='col-md-2'>
                                            <label class='l_font_fix_3' style='width:100%;'>Choose Time Filter: </label>
                                            <select name='time-span-filter' id='' class='form-control' onchange='timeSpanFilter(this.value)'>
                                                <option value='weekly'>Weekly</option>
                                                <option value='monthly'>Monthly</option>
                                                <option value='quarterly'>Quarterly</option>
                                                <option value='half-yearly'>Half Yearly</option>
                                                <option value='all-time'>All Time</option>
                                            </select>
                                            <span class='help-block hide reset-reports-filter' style='cursor:pointer'><small>Remove timespan filter</small></span>
                                        </div>";
								echo "<div class='col-md-2'>
											<span>
												<label class='l_font_fix_3'>Start Date</label>	
												<input id='t_dtpicker' class='s_dt form-control date-picker' dt='".$start_dt."' value='".$day_fm1."' />
											</span>
										</div>
										<div class='col-md-2'>
											<span>
												<label class='l_font_fix_3'>End Date</label>	
												<input id='t_dtpicker2' class='e_dt form-control date-picker' dt='".$end_dt."' value='".$day_fm2."' />
											</span>
										</div>
										<div class='col-md-2'>
										<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>	
										<button class='btn add_but gre_but change_rep' type='button'>Submit</button>
										</div>
						</div>";
						 echo "<div class='row row_style_1' id='c_find'>
                                <div class='col-md-12'>
                                            <a class='arr2 pull-left' ch='Reports' w_val='".$weekPrv. "'>&laquo; Prev</a>
                                            <a class='arr2 pull-right' ch='Reports' w_val='".$weekNxt. "'>Next&raquo;</a>
                                </div>
                        </div>";
						echo "<div class='data'>";
						$final_arry=array();						
						$grade_arry=array();
						$startDateUnix=strtotime($start_dt);
							$endDateUnix=strtotime(date('Y-m-d'));
							
								$currentDateUnix = $startDateUnix;

								$weekNumbers = array();
								while ($currentDateUnix < $endDateUnix) {
									//$y=date('d-Y', ($currentDateUnix));
									$weekNumbers[] = date('Y', (strtotime('+3 days', $currentDateUnix))).date('W', $currentDateUnix);
									$currentDateUnix = strtotime('+1 week', $currentDateUnix);
									
								}
								
					
						foreach($weekNumbers AS $inde2)
								{		
										//$inde1="Week ".." '".substr($inde2,2,2);
										$week=substr($inde2,4,2);
										$ye=substr($inde2,0,4);
										$inde1 = "W ".$week." (".date("d/m", strtotime($ye."-W".$week."-1"))." to ".date("d/m", strtotime($ye."-W".$week."-7")).")";
										foreach($final_user AS $key=>$value)	
										{	
											if(array_key_exists($value,$grades_dt))
											{
											if(array_key_exists($inde2,$grades_dt[$value]))
											{
												$grade_arry[]=array("act"=>$inde1,"work"=>$grades_dt[$value][$inde2],"user"=>$value);	
											}
											else{
											$grade_arry[]=array("act"=>$inde1,"work"=>"0","user"=>$value);
											}
											}else{
											$grade_arry[]=array("act"=>$inde1,"work"=>"0","user"=>$value);
											}
										}
										
								}
							
					$j_ar=0;
						 $i=$start_dt;
						
						while($i<=date('Y-m-d'))		
						{
							$date_chk_re = date('Y-m-d', strtotime($i));
							$final_arry[$j_ar]=array("label"=>$date_chk_re);
							foreach($final_user AS $id=>$name)
							{
								if(array_key_exists($date_chk_re,$proj_xp[$id]))
								{
							$date_chk = date('d/m/y', strtotime($date_chk_re));
							$ab=$proj_xp[$id][$date_chk_re];
							$ab2=$actual_xp[$id][$date_chk_re];
							$ab3=str_replace(":",".",$in_time[$id][$date_chk_re]);
							$ab4=str_replace(":",".",$wrk_hrs[$id][$date_chk_re]);
							$ab5=str_replace(":",".",$sapience_on[$id][$date_chk_re]);
			$final_arry[$j_ar]+=array($id."value1"=>$ab2,$id."value2"=>$ab,$id."value3"=>$ab3,$id."value4"=>$ab4,$id."value5"=>$ab5);
								}
						}
						//$final_arry=array("label"=>$date_chk_re,$id."value1"=>$ab2,$id."value2"=>$ab,$id."value3"=>$ab3,$id."value4"=>$ab4,$id."value5"=>$ab5);
						$j_ar++;
					$i = date('Y-m-d', strtotime($i . ' +1 day'));
					 }
					//print_r($final_arry);
					echo "</div>";	
echo "<div class='row row_style_1'>
								<div class='col-md-4'>
									<label class='l_font_fix_3'>Output ".$titl3.":-</label>
								</div>
								<div class='col-md-2 no_dt1 hide'>
									<label>No Records!</label>
								</div>
						</div>";									
								echo "<div class='canvas_r text-center'>";
							echo '<div style="width: auto; height: 400px;" id="chartdiv"></div>
							</div>';
							echo "<div class='row row_style_1'>
								<div class='col-md-4'>
									<label class='l_font_fix_3'>Input ".$titl3.":-</label>
								</div>
								<div class='col-md-2 no_dt1 hide'>
									<label>No Records!</label>
								</div>
						</div>";		
							echo "<div class='canvas_r2 text-center'>";
							echo '<div style="width: auto; height: 400px;" id="chartdiv2"></div>
							</div>';
							
							echo "<div class='row row_style_1'>
								<div class='col-md-4'>
									<label class='l_font_fix_3'>Grading ".$titl3.":-</label>
								</div>
								<div class='col-md-2 no_dt4 hide'>
									<label>No Records!</label>
								</div>
						</div>";		
						echo "<div class='row row_style_1'>
								<div class='col-md-12'>";
								foreach($f_grades AS $key=>$value)
								{
									echo "<label class='l_font_fix_3'>".$key."-".$value."</label>\t\t";
								}
								echo "</div>
						</div>";		
							// echo "<div class='canvas_r3 text-center'>";
							// echo '<div style="width: auto; height: 400px;" id="chartdiv3"></div>
							// </div>';
							echo "<div class='canvas_r4 text-center'>";
							echo '<div style="width: auto; height: 400px;" id="chartdiv4"></div>
							 </div>';
							 echo '<div id="legenddiv4" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>';
							 
							 echo "<div class='row row_style_1'>
								<div class='col-md-4'>
									<label class='l_font_fix_3'>Scatter Plot ".$titl3.":-</label>
								</div>
								<div class='col-md-2 no_dt6 hide'>
									<label>No Records!</label>
								</div>
							</div>";	
							 echo "<div class='canvas_r6 text-center'>";
							echo '<div style="width: auto; height: 400px;" id="chartdiv6"></div>
							 </div>';
							 echo '<div id="legenddiv6" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>';
			?>
			
			</div>
		</div>
				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>	
	<script>var title_head3 = '<?php echo $titl3 ?>';</script>	
	<script>var arrayFromPHP = <?php echo json_encode($final_arry); ?>;</script>
	<script>var final_user_array = <?php echo json_encode($final_user); ?>;</script>	
	<script>var grade_arry = <?php echo json_encode($grade_arry); ?>;</script>
	<script>var f_grades = <?php echo json_encode($f_grades); ?>;</script>	
	<script>var tt_data2 = <?php echo json_encode($sctter); ?>;</script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/serial.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/xy.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-multiselect.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>
		window.useDateFilters = '<?php echo $useDateFilters ?>';


    $(document).ready(function() {
        if (window.useDateFilters == '1') {
            $('.reset-reports-filter').removeClass('hide');
            $('.reset-reports-filter').on('click', function() {
                window.location = base_url+"index.php/User/load_view_f?a=Reports";
            })
        }
    });
		
		
	$("#sel_emp").multiselect({
		enableCaseInsensitiveFiltering: true,
		includeSelectAllOption: false,
		selectAllJustVisible: true,
		numberDisplayed: 1
		
	});
	
	
	$("body").on("focus", ".s_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					autoclose:true,
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						$(this).attr('dt',date_v);
						}
					});
	});
	
	
	$("body").on("change","#sel_emp",function(){
		if($(this).val())
		{
			$.ajax({				
				url: base_url+"index.php/User/load_emp_string",
				type: 'post',
				data : {param:"Personal_Trends",pt_emp:$(this).val().toString()}
			});
		}
	});

	
	$("body").on("focus", ".e_dt",function(){
		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					autoclose:true,
					startDate:$(".s_dt").val(),
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{							
							var ele=Date.parse($(this).val()); 
							var date_v=moment(ele).format("YYYY-MM-DD");
							$(this).attr('dt',date_v);
						}
					});	
	});
	
	$("body").on("click", ".change_rep",function(){
		var sdt=$(".s_dt").attr('dt');
		var edt=$(".e_dt").attr('dt');
		if(sdt && edt && sdt<=edt)
		{
		window.location = base_url+"index.php/User/load_view_f?a=Reports&s_dt="+sdt+"&e_dt="+edt;
		}
	});
	
		var myArray =[];
		myArray=arrayFromPHP;		
	// $(".params1").each(function(index) {
		// item = {};
		// item ["value1"] =  $(this).attr('act_xp');
        // item ["value2"] = $(this).attr('proj_xp');
		// item ["value3"] = $(this).attr('in_time');
		// item ["value4"] = $(this).attr('wrk_hrs');
		// item ["value5"] = $(this).attr('sapience_on');
		// item ["label"] = $(this).text(); 
         // myArray.push(item);
    // });
	
	if(myArray.length)
	{
		$("body").find(".no_dt1").addClass('hide');
					$("body").find(".canvas_r").removeClass('hide');
					$("body").find(".canvas_r2").removeClass('hide');
	}else{
					$("body").find(".no_dt1").removeClass('hide');
					$("body").find(".canvas_r").addClass('hide');
					$("body").find(".canvas_r2").addClass('hide');
		}
	//console.log(arrayFromPHP);
	 var chart3;
            AmCharts.ready(function () {
                chart3 = new AmCharts.AmSerialChart();
                chart3.dataProvider = myArray;
                chart3.categoryField = "label";
				
				chart3.synchronizeGrid=true;
				chart3.addTitle("Output "+title_head3);
                chart3.autoMargins = true;
				chart3.dataDateFormat = "YYYY-MM-DD";			
				
                var categoryAxis3 = chart3.categoryAxis;
                categoryAxis3.gridAlpha = 0;
               categoryAxis3.axisAlpha = 0;
                categoryAxis3.gridPosition = "end";
				categoryAxis3.autoGridCount = true;
				//categoryAxis3.dateFormats="DD-MM";
				categoryAxis3.equalSpacing = true;
				categoryAxis3.minPeriod="DD";
				//categoryAxis3.centerLabels = true;
				categoryAxis3.autoWrap=true;
				categoryAxis3.parseDates=true;
				//categoryAxis3.minorGridEnabled=true;
			
		total_use=final_user_array.length;
		$.each(final_user_array, function(index, item) {
				var graph4 = new AmCharts.AmGraph();				
                graph4.title =item+"- Confirmed XP";
                graph4.labelText = "[[value]]";
                graph4.valueField = index+"value1";
				graph4.valueAxis = "v1";
				graph4.bullet = "round";
				graph4.bulletSize = 10;
                //graph4.hideBulletsCount = 30*total_use;
				graph4.lineColor = '#B0DE09';
				graph4.balloonText = "[[title]]: <b>[[value]] XP</b>";
				graph4.color = 'black';
		         graph4.fillAlphas = 0;
                chart3.addGraph(graph4);
				graph4.labelFunction= function(item, label) {
					 return label == "0" ? "" : label;
				}
				
					var graph4 = new AmCharts.AmGraph();
					graph4.title = item+"- Projected XP";
					graph4.labelText = "[[value]]";
					graph4.valueField = index+"value2";
					graph4.valueAxis = "v1";
					graph4.bullet = "round";
					//graph4.hideBulletsCount = 30*total_use;
					graph4.balloonText = "[[title]]: <b>[[value]] XP</b>";
					graph4.lineColor = '#FF6600';
					graph4.color = 'black';
		         graph4.fillAlphas = 0;
                chart3.addGraph(graph4);
				
				graph4.labelFunction= function(item, label) {
					 return label == "0" ? "" : label;
				}
				
		});
					var	valueAxis3 = new AmCharts.ValueAxis();               
						valueAxis3.id='v1';
						valueAxis3.precision=2;
						valueAxis3.position="left";
						chart3.addValueAxis(valueAxis3);	
						
				var legend3= new AmCharts.AmLegend();
                legend3.borderAlpha = 0.2;
                legend3.horizontalGap = 10;
                legend3.autoMargins = true;
                legend3.marginLeft = 20;
                legend3.marginRight = 20;
                chart3.addLegend(legend3);
				
				chart3.responsive = {
					  "enabled": true
					};
					
				chart3.export = {
					  "enabled": true,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 3
						  },
					  "exportTitles" :true,
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart3.write("chartdiv");
            }); 
	
	var chart3;
            AmCharts.ready(function () {
                chart3 = new AmCharts.AmSerialChart();
                chart3.dataProvider = myArray;
                chart3.categoryField = "label";
				//chart3.synchronizeGrid=true;
				//chart3.connect=false;
				chart3.addTitle("Input "+title_head3);
                chart3.autoMargins = true;
				//chart3.startDuration=1.5;
				chart3.dataDateFormat = "YYYY-MM-DD";
				
                var categoryAxis3 = chart3.categoryAxis;
                categoryAxis3.gridAlpha = 0;
                categoryAxis3.axisAlpha = 0;
                //categoryAxis3.gridPosition = "start";
				categoryAxis3.autoGridCount = true;
				//categoryAxis3.type = "date";
				categoryAxis3.autoWrap=true;
				//categoryAxis3.gridPosition="middle";
				categoryAxis3.parseDates=true;
				categoryAxis3.equalSpacing =true;
				categoryAxis3.minPeriod="DD";
				//categoryAxis3.startOnAxis = true;
				
			
				
					
				$.each(final_user_array, function(index, item) {
				var graph3 = new AmCharts.AmGraph();
					graph3.title = item+"- In Time";
					graph3.labelText = "[[value]]";
					graph3.valueField = index+"value3";
					graph3.valueAxis = "v3";
					graph3.bullet = "round"; 
					//graph3.connect = false;
					//graph3.dateFormat="HH:NN";
					//graph3.hideBulletsCount = 30*total_use;
					graph3.balloonText = "[[title]] [[category]]: <b>[[value]]</b>";
					graph3.lineColor = '#9C27B0';
					graph3.color = 'black';
		         graph3.fillAlphas = 0;
				 graph3.balloonFunction= function( item, graph ) {
				  // init variables
				  var chart = graph.chart;
				  var key = graph.valueField;
				  var data = chart.dataProvider;
				  var text = graph.title + ":- <b>" + data[ item.index ][ key ].replace(".",":");
				  return text;
				}
                chart3.addGraph(graph3);
				
				graph3.labelFunction= function(item, label) {
					 return label == "0" ? "" : label.replace(".",":");
				}
				
				
				var graph3 = new AmCharts.AmGraph();
					graph3.title = item+"- Work Hours";
					graph3.labelText = "[[value]]";
					graph3.valueField = index+"value4";
					graph3.valueAxis = "v3";
					graph3.bullet = "round"; 
					//graph3.connect = false;
					//graph3.dateFormat="HH:NN";
					//graph3.hideBulletsCount = 30*total_use;
					graph3.balloonText = "[[title]] [[category]]: <b>[[value]]</b>";
					graph3.lineColor = '#E91E63';
					graph3.color = 'black';
		         graph3.fillAlphas = 0;
				 graph3.balloonFunction= function( item, graph ) {
				  // init variables
				  var chart = graph.chart;
				  var key = graph.valueField;
				  var data = chart.dataProvider;
				  var text = graph.title + ":- <b>" + data[ item.index ][ key ].replace(".",":");
				  return text;
				}
                chart3.addGraph(graph3);
				
				graph3.labelFunction= function(item, label) {					
					 return label == "0" ? "" : label.replace(".",":");
				}
				
				var graph3 = new AmCharts.AmGraph();
					graph3.title = item+"- Sapience On";
					graph3.labelText = "[[value]]";
					graph3.valueField = index+"value5";
					graph3.valueAxis = "v3";
					//graph3.connect = false;
					graph3.bullet = "round"; 
					//graph3.dateFormat="HH:NN";
					//graph3.hideBulletsCount = 30*total_use;
					graph3.balloonText = "[[title]] [[category]]: <b>[[value]]</b>";
					graph3.lineColor = '#b0de09';
					graph3.color = 'black';
		         graph3.fillAlphas = 0;
				 graph3.balloonFunction= function( item, graph ) {
				  // init variables
				  var chart = graph.chart;
				  var key = graph.valueField;
				  var data = chart.dataProvider;
				  var text = graph.title + ":- <b>" + data[ item.index ][ key ].replace(".",":");
				  return text;
				}
                chart3.addGraph(graph3);
				
				 graph3.labelFunction= function(item, label) {
					// return label == "0:00" ? "" : label.replace(".",":");
					 return label == "0" ? "" : label.replace(".",":");
				}
				});
					// var	valueAxis3 = new AmCharts.ValueAxis();               
						// valueAxis3.id='v1';
						// valueAxis3.precision=2;
						// valueAxis3.position="left";
						// chart3.addValueAxis(valueAxis3);	

						
						var	valueAxis3 = new AmCharts.ValueAxis();               
						valueAxis3.id='v3';
						
						valueAxis3.position="left";
						//valueAxis3.autoGridCount = true;
						valueAxis3.gridCount=3;
					valueAxis3.maximum=24;
					valueAxis3.strictMinMax=0;
						valueAxis3.axisColor="#000000";
						chart3.addValueAxis(valueAxis3);	
				
				valueAxis3.labelFunction= function(valueText, serialDataItem, ValueAxis) {
								
						return valueText+":00";					  			  
				}	
	
				
				var legend3= new AmCharts.AmLegend();
                legend3.borderAlpha = 0.2;
                legend3.horizontalGap = 10;
                legend3.autoMargins = true;
                legend3.marginLeft = 20;
                legend3.marginRight = 20;
                chart3.addLegend(legend3);
				
				chart3.responsive = {
					  "enabled": true
					};
					
				chart3.export = {
					  "enabled": true,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 3
						  },
					  "exportTitles" :true,
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart3.write("chartdiv2");
            }); 
	
	
	var group_to_values = grade_arry.reduce(function (obj, item) {
    obj[item.act] = obj[item.act] || [];
	item2 = {};
	item2 ["wh"] =item.work;
		item2 ["user"] = item.user;
    obj[item.act].push(item2);
    return obj;
}, {});

var groups = Object.keys(group_to_values).map(function (key) {
    return {act: key, work: group_to_values[key]};
});
  
  var k_array=[];

  $.each( groups, function( key, value ) {
  item23 = {};
		item23 ["act"] = value.act.toString();
 
	$.each( value.work, function( key1, value1 ) {
		item23 [value1.user.toString()] = value1.wh.toString();
   
	});
	k_array.push(item23);
});

//console.log(k_array);
	// var chart3;
            // AmCharts.ready(function () {
                // chart3 = new AmCharts.AmSerialChart();
                // chart3.dataProvider = k_array;
                // chart3.categoryField = "act";
				// //chart3.synchronizeGrid=true;
				// chart3.addTitle("Grading "+title_head3);
                // chart3.autoMargins = true;
				
                // var categoryAxis3 = chart3.categoryAxis;
                // categoryAxis3.gridAlpha = 0;
                // categoryAxis3.axisAlpha = 0;
               // // categoryAxis3.gridPosition = "start";
				// categoryAxis3.autoGridCount = true;
				// categoryAxis3.autoWrap=true;
			
				
				// //$.each(f_grades, function(in_dec, i_emm) {
				// $.each(final_user_array, function(index, item) {
					// var graph4 = new AmCharts.AmGraph();				
					// graph4.title =item;
					// graph4.labelText = "[[value]]";
					// graph4.valueField = item;
					// graph4.valueAxis = "v1";
					// graph4.type = "column";
					// // graph4.bullet = "round";
					// // graph4.bulletSize = 10;
					// // //graph4.hideBulletsCount = 30*total_use;
					// //graph4.lineColor = '#B0DE09';
					// graph4.balloonText = "[[title]] [[category]]<br>: <b>[[value]]</b>";
					// graph4.color = 'black';
					 // graph4.fillAlphas = 1;
					 // graph4.lineAlpha = 0;
					// chart3.addGraph(graph4);
				// });			
			
				
				// var	valueAxis3 = new AmCharts.ValueAxis();               
						// valueAxis3.id='v1';
						// valueAxis3.precision=2;
						// valueAxis3.position="left";
						// chart3.addValueAxis(valueAxis3);	
		
				// var legend3= new AmCharts.AmLegend();
                // legend3.borderAlpha = 0.2;
                // legend3.horizontalGap = 10;
                // legend3.autoMargins = true;
                // legend3.marginLeft = 20;
                // legend3.marginRight = 20;
                // chart3.addLegend(legend3);
				
				// chart3.responsive = {
					  // "enabled": true
					// };
					
				// chart3.export = {
					  // "enabled": true,
					  // "border": {
							// "stroke": "#000000",  // HEX-CODE to define the border color
							// "strokeWidth": 1,     // number which represents the width in pixel
							// "strokeOpacity": 1    // number which controls the opacity from 0 - 3
						  // },
					  // "exportTitles" :true,
					   // "menu": [ {
						  // "class": "export-main",
						  // "menu": [ "PNG", "JPG" ]
					   // }]
					// };
					// chart3.write("chartdiv3");
            // }); 
if(k_array)
  {
	var chartData2 = k_array;
	 var chart4;
            AmCharts.ready(function () {
                chart4 = new AmCharts.AmSerialChart();
                chart4.dataProvider = chartData2;
                chart4.categoryField = "act";
				chart4.synchronizeGrid=true;
				//chart4.addTitle("Grading");
				chart4.addTitle("Grading "+title_head3);
                chart4.autoMargins = true;
				
                var categoryAxis2 = chart4.categoryAxis;
                categoryAxis2.gridAlpha = 0;
                categoryAxis2.axisAlpha = 0;
                categoryAxis2.gridPosition = "start";
				categoryAxis2.autoGridCount = true;
				categoryAxis2.autoWrap=true;
				// categoryAxis2.labelFunction= function(valueText,categoryAxis2) {
						// return 'W '+valueText.substring(5,7);
				// }				
				
				$.each(final_user_array, function(index, item) {
				var graph2 = new AmCharts.AmGraph();
				graph2.valueAxis='v1';
                graph2.title = item;
                graph2.labelText = "[[value]]";
                graph2.valueField = item;
				graph2.bullet = "round";
                graph2.hideBulletsCount = 30;
				graph2.balloonText = "[[category]]<br>[[title]] - <b>[[value]]</b>";
				graph2.color = 'black';
		         graph2.fillAlphas = 0;
				chart4.addGraph(graph2);
				});
					var	valueAxis2 = new AmCharts.ValueAxis();               
						valueAxis2.id='v1';
						//valueAxis2.precision=2;
						valueAxis2.position="left";
						valueAxis2.min=0;
						valueAxis2.max=5;
						valueAxis2.autoGridCount = false;
						valueAxis2.labelFrequency = 1;
						valueAxis2.gridCount = 5;
						valueAxis2.strictMinMax=true;
						chart4.addValueAxis(valueAxis2);
					
				
				var legend2= new AmCharts.AmLegend();
                legend2.borderAlpha = 0.2;
                legend2.horizontalGap = 10;
                legend2.autoMargins = true;
                legend2.marginLeft = 20;
                legend2.marginRight = 20;
                chart4.addLegend(legend2, "legenddiv4");
				
				
				chart4.responsive = {
					  "enabled": true
					};
					chart4.write("chartdiv4");
				chart4.export = {
					  "enabled": true,
					  "fileName":"Grading "+title_head3,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 2
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv4')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart4.initHC = false;
					chart4.validateNow();
            });              
			}else{
					$("body").find(".no_dt4").removeClass('hide');
					$("body").find(".canvas_r4").addClass('hide');
				}
			
myArray6=[];
	var x_max=17;
	var y_max=16;
	if(tt_data2)
	{
		//myArray6=tt_data2;
$.each(tt_data2, function(key,value) {
			  item = {};			  
			// ele=Date.parse(value['dates_v']); 
			 item ["x"+value['format']]=parseFloat(value['x']);
			 item ["y"+value['format']]=parseFloat(value['y']);
			 // item ["x"+key]=parseFloat(value['x']);
			 // item ["y"+key]=parseFloat(value['y']);
			 if(x_max<parseFloat(value['x']))
			 {
			  x_max=parseFloat(value['x']);
			 }
			 if(y_max<parseFloat(value['y']))
			 {
			  y_max=parseFloat(value['y']);
			 }
			 item ["value"+value['format']]=3;
			  item ['bullet']=value['dt'];		 
			  
			  myArray6.push(item);			  
			}); 
	}
	console.log(y_max);
			
var count=(final_user_array.length);
	 if(tt_data2!=0)
  { 
            AmCharts.ready(function () {
var chart= new AmCharts.AmXYChart();        
chart.dataProvider = myArray6;
chart.addTitle("Scatter Plot "+title_head3);
//chart.marginLeft = 35;
//chart.startDuration = 1.5;
//chart.bandValueScope=20;

var xAxis = new AmCharts.ValueAxis();
xAxis.position = "left";
xAxis.axisAlpha=0;
xAxis.id="xa";
xAxis.minimum =0;
xAxis.maximum =x_max;

xAxis.title="Daily o/p (XP)";
//xAxis.autoGridCount = true;
chart.addValueAxis(xAxis);

var yAxis = new AmCharts.ValueAxis();
yAxis.position = "bottom";
yAxis.id="ya";
yAxis.minimum =0;
yAxis.maximum =y_max;
//yAxis.autoGridCount = true;
yAxis.title="Daily Work hrs";

yAxis.axisAlpha=0;
chart.addValueAxis(yAxis);                



				
var trendLine = new AmCharts.TrendLine();
// trendLine.initialDate = new Date(2012, 0, 2, 12); // 12 is hour - to start trend line in the middle of the day
// trendLine.finalDate = new Date(2012, 0, 11, 12);
trendLine.initialValue = 0;
trendLine.finalValue = 30;
trendLine.initialXValue = 8;
trendLine.finalXValue = 8;
trendLine.thickness=2;
//trendLine.dashLength=3;
trendLine.lineColor = "#000000";
chart.addTrendLine(trendLine);


var trendLine = new AmCharts.TrendLine();
// trendLine.initialDate = new Date(2012, 0, 2, 12); // 12 is hour - to start trend line in the middle of the day
// trendLine.finalDate = new Date(2012, 0, 11, 12);
trendLine.initialValue = 10;
trendLine.finalValue = 10;
 trendLine.initialXValue = 0;
 trendLine.finalXValue = 18;
//trendLine.valueAxis="ya";
trendLine.lineColor = "#CC0000";
chart.addTrendLine(trendLine);

//console.log(final_user_array);
				$.each( final_user_array, function( key2, value2 ) {
				var graph6 = new AmCharts.AmGraph();
					graph6.balloonText="<b>[[title]] </b> ( [[bullet]] ): Work Hours:<b>[[x]]</b> Confirmed XPs:<b>[[y]]</b>";
					graph6.bullet= "circle";
					graph6.bulletBorderAlpha= 0.2;
					graph6.bulletAlpha= 0.8;
					graph6.lineAlpha= 0;
					graph6.fillAlphas= 0;
					graph6.title= value2;
					graph6.valueField= "value"+key2;
					graph6.xField = "x"+key2;
					graph6.yField = "y"+key2;
					graph6.maxBulletSize= 10;
					chart.addGraph(graph6);
				});
				
				
					
				var legend2= new AmCharts.AmLegend();
                legend2.borderAlpha = 0.2;
                legend2.horizontalGap = 10;
                legend2.autoMargins = true;
                legend2.marginLeft = 20;
                legend2.marginRight = 20;
                chart.addLegend(legend2, "legenddiv6");
				
                         chart.responsive = {
					  "enabled": true
					};     
				chart.write("chartdiv6");
				chart.export = {
					  "enabled": true,
					 // "fileName":title_head6,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 2
						  },
					  "exportTitles" :true,					  
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart.initHC = false;
					chart.validateNow();
            });              
			}else{
					$("body").find(".no_dt6").removeClass('hide');
					$("body").find(".canvas_r6").addClass('hide');

				}
							
			
</script>
  </body>
</html>