<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm="Team_Member_Info";

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<!--link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"-->	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script src="<?= getAssestsUrl() ?>js/jquery.bootpag.min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/RGraph.svg.common.core.js"></script>
	<script src="<?= getAssestsUrl() ?>js/RGraph.svg.common.key.js"></script>
	<script src="<?= getAssestsUrl() ?>js/RGraph.svg.bar.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
  </head>
  <body>
 <header id="header_2">
    		<!--kode top bar start-->
    		<div class="top_bar_2">
	    		<div class="container">
	    			<div class="row">	    				
						<div class="col-md-2">
							<div class="edu2_ft_logo_wrap">
							<a href="<?= base_url() ?>index.php/User/home_page">
							<img class="half_w" src="<?= getAssestsUrl() ?>images/byjus_new.png" alt=""></a>
							</div>
						</div>
						<div class="col-md-8">	    					
								<h3 class='ev-c1'>
									<center>
										<span>Work Tracker </span>
										<i class="glyphicon glyphicon-th-list" style="font-size: 21px;"></i>
									</center>
								</h3>	
	    					</div>
						<div class="col-md-2">	
						<center>
										<div class='ev-c2'>
							<a class='dropdown-toggle user_p' data-toggle="dropdown">
							<img class="prof_img" title="Logout" src="<?= getAssestsUrl() ?>images/profile.png">
							</a>
							 <ul class="dropdown-menu dropdown-user">
									<li><a href="#"><i class="glyphicon glyphicon-cog img_l"></i> Account Info</a>
									</li>
									<li class="divider"></li>
									<li><a href="<?php echo base_url('index.php/user/user_logout');?>">
									<i class="glyphicon glyphicon-log-out img_l"></i> Logout</a>
									</li>
								</ul>
					
							<?php echo "<div class='u_in' user=".$user_id.">Welcome ".$this->session->userdata('user_name')." !</div>"; ?>
						</div>
							</center>
	    				</div> 						
					</div>
	    		</div>
	    	</div>    		      	
	   </header> 
	   <div class="edu2_counter_wrap">
				<div class="container">					
					<div class="edu2_counter_des">						
						<span><img class="img_lo" src="<?= getAssestsUrl() ?>images/meter.png" alt=""></span>
						<h4 class="counter"><?php echo $metrics[0]->today_xp;?></h4>
						<h6>TODAY's XP</h6>
					</div>
					<div class="edu2_counter_des">						
						<span><img class="img_lo" src="<?= getAssestsUrl() ?>images/sum.png" alt=""></span>
						<h4 class="counter"><?php echo $metrics[0]->week_xp_val;?></h4>
						<h6>WEEK's XP</h6>
					</div>
					<div class="edu2_counter_des">						
						<span><img class="img_lo" src="<?= getAssestsUrl() ?>images/rank.png" alt=""></span>
						<h4 class="counter"><?php echo $this->session->userdata('streak');?></h4>
						<h6>STREAK</h6>
					</div>
				
				</div>
			</div>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<div class="col-md-2 c_row">			
				<div class='row mfkdsf text-center'>
					<div class='col-md-12'>
					<div class="md_t col_men">
					<i class="glyphicon glyphicon-th h_imh"></i> 
					</div>
					<div class="md_t md_t_h">
					<i class="glyphicon glyphicon-home h_imh"></i> Home</div>
					</div>
				</div>
			
			<?php 
			foreach($access_mst AS $a_r)
			{
				$dg[$a_r->access_name]=$a_r->icon;
			}			
			foreach($access_str AS $rw)
			{
				$cl='';
				$d_id=str_replace(' ', '_', $rw);
				if($d_id==$file_nm)
				{
					$cl=' m_col_act';					
				}
				echo "<div class='row m_row'>
						<div class='col-md-12 m_col ".$cl."' data_id='".$d_id."' style='background-color:".$color[$rw].";'>						
							<i class='glyphicon ".$dg[$rw]." img_hi'></i>
							<h5>".$name[$rw]."</h5>	
						</div>			
					</div>";				
			}			
			?>
				</div>
				
				<div class="col-md-10 c_row">
				<div class='row'>	
					<div class='col-md-12'>
						<button class='stab_stages' ch="Team_Member_Info">Members</button>
						<button class='stab_stages stab_dis_selec' ch="Team_Graphs">Graphs</button>
					</div>
				</div>
		
				<!--div class='row row_style_1'>
			<div class="col-md-4">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-6 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_0" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Projected XP</span>					
					</div>
					<div class="col-md-6 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Actual XP</span>
					
				</div>	
			</fieldset>				
			</div>			
			</div-->
					<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.$day_w.' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w+1).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(7-$day_w).' days'));
							$week_st = date('d-M', strtotime($day.' -'.$day_w.' days'));
							$week_end = date('d-M', strtotime($day.' +'.(6-$day_w).' days'));
							
					?>
			
			<div class='row third-row head'>
												
												<?php
												echo "<div class='col-md-12 cur-month text-center'><span>Records from ".$week_st." to ".$week_end."</span>
												</div>	";
												?>
							</div>
							
									<div class='row row_style_1' id='c_find'>	
											<div class='col-md-12'>					
														<a class='arr pull-left' ch='Team_Graphs' w_val="<?= $w_prev ?>">&laquo; Prev</a>						
														<a class='arr pull-right' ch='Team_Graphs' w_val="<?= $w_nxt ?>">Next&raquo;</a>					
											</div>														
									</div>
			<?php
			
						echo "<div class='data'>";
						//print_r($proj_xp);
						if($tm_data)
						{
							foreach($tm_data AS $row1)
										{									
									
									echo "<h5 class='hide params1' user_id='".$row1['full_name']."' act_id='".$row1['activity_name']."'>".$row1['work_done']."</h5>";
										}
						}
								echo "</div>";
							echo "<div class='row canvas_r text-center'>";
							echo '<div style="width: 750px; height: 300px" id="chart-container"></div>';
		
			?>
			</div>
				
			</div>
		</div>
	</div>
</div>
<script>
    window.onload = function ()
    {
		
		 // var parArray1 = [];
     var myArray =[];
	 var a_id = [];
    // var label = [];
    // var outarr = [];

    $(".params1").each(function(index) {
				//a_id[index]=$(this).attr('act_id');
		item = {};
		item ["act"] =  $(this).attr('act_id');
        item ["work"] = $(this).text();
		item ["user"] = $(this).attr('user_id');
         myArray.push(item);
			
    });
	

//console.log(myArray);

var group_to_values = myArray.reduce(function (obj, item) {
    obj[item.act] = obj[item.act] || [];
	item2 = {};
	item2 ["wh"] =item.work;
		item2 ["user"] = item.user;
    obj[item.act].push(item2);
    return obj;
}, {});

var groups = Object.keys(group_to_values).map(function (key) {
    return {act: key, work: group_to_values[key]};
});
	
console.log(groups);
	
   //console.log(groups[1].work);
  data_set='';
  xlabel='';
  data_set_lab='';
  
  $.each( groups, function( key, value ) {
  //alert( key + ": " + value );
    xlabel=xlabel+',"'+(value.act.toString())+'"';
	d1='';
	d3='';
	$.each( value.work, function( key, value ) {
    d1=d1+','+(value.wh.toString());
	d3=d3+','+(value.user.toString());
	});
	 data_set=data_set+',['+(d1.substring(1))+']';
	 data_set_lab=data_set_lab+',['+(d3.substring(1))+']';
});
   
  // console.log(data_set);
  // console.log(data_set_lab);
   
   res=data_set.substring(1);
   res2=data_set_lab.substring(1);
   d2=xlabel.substring(1);
   //eval('([[' + parArray1 + '],[' + parArray2 + ']])')
    // Define the data
   //data  = [[4,9,9,5,6,7,7],[8,6],[1,2,5],[5,6,2],[4,8,8],[3,2,5],[4,6,5]];

   data = eval('(['+res+'])');
   ddkit = ('(['+res2+'])');
    // Calculate the percentages that the data represents
    data.forEach(function (v, k, arr)
    {
        var total = RGraph.SVG.arraySum(v);
			console.log(v);
        for (var i=0; i<v.length; ++i) {
            v[i] = (v[i] / total) * 100;
        }
    });
	
	console.log(res2);
	
	$(myLine.canvas).on('mousemove', function (e)
{
    var key = RGraph.Registry.get('key-element');

    if (key) {
        // alert() the details of the object
        $p(key);
    }
});

    new RGraph.SVG.Bar({
        id: 'chart-container',
        data: data,
        options: {
            gutterLeft: 50,
           // colors: ['#c66','#6c6','#66c'],
            yaxisUnitsPost: '%',
            xaxisLabels: eval('(['+d2+'])'),
            grouping: 'stacked',
            hmargin: 10,
			key:ddkit,
			gutterTop: 50,
            labelsAbove: true,
            labelsAboveUnitsPost: 'kg',
            labelsAboveSize: 8,
            labelsAboveColor: '#333',
            linewidth:1,
            yaxisMax: 100,
			 yaxis: false,
            title: 'Activity-wise Break Up'
        }
    }).draw();
	}
</script>
</script>
  </body>
</html>