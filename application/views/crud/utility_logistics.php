<?php
$user_id=$this->session->userdata('user_id');
//$this->session->set_userdata('pagename',basename(__FILE__));
 //echo $user_id;
$file_nm='Utility';
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
  </head>
  <body>
  	
 <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']="Utility";
				$this->load->view('common/sidebar',$data);
			?>			
			<div class="col-md-10 c_row">
					
				<div class='row hid1'>	
						<div class='col-md-12'>	
				<?php
				$file_nm=str_replace('.php','',basename(__FILE__));
					// echo "<button class='stab_stages' ch='Utility'>Tables</button> ";
					foreach($check_tab as $key=>$value)
					{
							$sel='';
							if($value==$file_nm)
							{
								$sel='stab_dis_selec';
							}					
							echo "<button class='stab_stages_1 ".$sel."' ch='".$value."'><span>".$key."</span></button> ";
					}
				echo "</div></div>";
				?>		<input type="hidden" class="stab_dis_ut" ch="activity_mst">
						<div class='row hid'>	
							<div class='col-md-12'>	
								<div class="row row_style_1 text-center">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-4 text-left">
												<button class="btn add_but gre_but logistic_ins" type="button">+Add Record</button>	
												<button class="btn add_but" type="button" id="btn_import_logistics">Import Logistics</button>
											</div>
											<!-- <div class="col-md-3 col-md-offset-5">
												<label class='l_font_fix_3'>Choose Current Status:</label>
												<?php $cur_status=$this->input->get('cur_status') ? $this->input->get('cur_status') : 'Y'?>
												<?php echo form_dropdown('current_status', ['A'=>'All','Y'=>'Active','N'=>'InActive'],$cur_status ? $cur_status : 'Y', ['class'=>'selectpicker form-control','id'=>'current_status','data-live-search'=>'true']); ?>
											</div> -->
										</div>
									</div>
								</div>

								<div id="toolbar"> 
									<select class="form-control">
											<option value="">Export Page</option>
											<option value="selected">Export Selected</option>
									</select>
								</div>
								
								<table class="display table table-bordered table-responsive" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table3"   data-search-time-out=500 data-pagination="true" data-search="true">
									<thead>
										<tr>
										  <th data-sortable="true" data-class='l_font_fix_3' data-formatter="buttonFormatter" data-field="logistics_id">Action</th>
										  <th data-sortable="true" data-class='l_font_fix_3 u_id' data-filter-control="input" data-field="emp_id">Employee ID</th>										  
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="email_id">Email ID</th>		  
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="deputed_location">Deputed Location</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="office">Office</th>		  
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="role_center">Role Center</th>		  
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="sapience_status">Sapience Status</th>		  
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="f_track">F-Track</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="adobe_id">Adobe ID</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="domain_login">Domain Login</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="workstation">Workstation</th>
										  <th data-sortable="true" data-class="l_font_fix_3"  data-filter-control="input"  data-field="employment_type">Employment Type</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="employment_status">Employment Status</th>
										  <th data-sortable="true" data-class="l_font_fix_3"  data-field="image" data-formatter="imageFormat">Image</th>		  
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="datepicker" data-field="start_date" data-formatter="dateSortFormate">Start Date</th>		  
										  <th data-sortable="true" data-class="l_font_fix_3"  data-filter-control="datepicker" data-field="end_date" data-formatter="dateSortFormate">End Date</th>		  
										  <th data-sortable="true" data-class="l_font_fix_3"  data-filter-control="input" data-field="status_nm">Status</th>	
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="ins_user">Ins By</th>
										  <th data-sortable="true" data-class="l_font_fix_3"  data-field="ins_dt" data-formatter="dateInsSortFormate">Ins At</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="upd_user">Upd By</th>
										  <th data-sortable="true" data-class="l_font_fix_3"  data-field="upd_dt" data-formatter="dateInsSortFormate">Upd At</th>	  
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="is_current">Currently Active</th>	  
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<input type="hidden" value="" id="pagecount">
	<!-- Logistics Modal -->
	<div class='modal fade open_col' id='mdl_logistics'>										
		<div class='modal-dialog' role='document'>
			<div class='modal-content'>
			 	<div class='modal-body' id='mdl_body_logistics'>		

			 	</div>								  
			</div>
		</div>
	</div>

	<!-- Import Logistics Modal -->
	<div class='modal fade open_col' id='importLogisticModal'>
    	<div class='modal-dialog' role='document'>
    		<div class='modal-content'>
    			<div class='modal-body' id='modal_edit'>
    				<form class='form-horizontal'  id='upload_file'>
    					<div class='row row_style_1'>
    						<div class='col-md-8'>
    							<label class='l_font_fix_3'>Select a file to upload</label>
    							<input type="file" id="logisticFile" name ="file" />
    						 </div>
    					</div>
    					<div class='row row_style_1'>
    						<div class='col-md-12'>
    							<!--input class='btn add_but up_csv_utlity' id='submit' type='button' value="upload"-->

    							<div class='col-md-2'>
    							<input class='btn add_but up_csv_utlity' data-upload-type="import-logistic" id='submit' type='button' value="upload">
    							</div>
    							<div class='col-md-3'>
    							<input type ="button" id="downloadLogisticTemplate" class="btn add_but" name ="downloadTem" value="Download csv template"/>
    						 </div>
    						</div>
    					</div>

                        <div class="panel panel-default error-panel hide">
                            <div class="panel-body error-import">

                            </div>
                        </div>
    				</form>
    				<div class='row row_style_1'>
    				<div class='col-md-12'>
    					</div>
    					<div class='col-md-12'>
    						<div id='files' style="color:green"></div>
    					</div>
    				</div>
    			</div>
    		</div>
    	 </div>
    </div>


<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var f_name = '<?php echo $file_nm ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/FileSaver.min.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
<script>
	//Date sorting formatter
	function dateSortFormate(value, row, index){
		return value ? moment(value).format("DD/MM/YYYY") : '-';
	}

	function dateInsSortFormate(value, row, index){
		return value ? moment(value).format("DD/MM/YYYY HH:mm:ss") : '-';
	}

	function imageFormat(value){
		return value ? `<img src="<?php echo base_url('uploads/logistics/') ?>${value}" alt="Logistics Image" height="10%" weight="10%">` : '-';
	}
	function buttonFormatter(value, row) {
       	return "<span><i class='glyphicon glyphicon-edit t_edit user_logistic_edit' data-logistic_id='"+value+"'></i></span>";
	}

	//Upload file
	function uploadUtility(url, formData) {
        $.ajax({
            url:url,
            type:"POST",
            data:formData,
            processData:false,
            contentType:false,
            cache:false,
            async:false,
            success: function(data){
                if (data=="Successfully") {
                    swal('Done', 'Sucessfully Updated', 'success').then(function() {
                       location.reload();
                    });
                } else if (data.success) {
                    swal('Done', data.message, 'success').then(function() {
                       location.reload();
                    });
                }  else if (!data.success && data.errors == undefined) {
                    swal('Oops', data.message, 'error');
                } else if (!data.success && data.errors.length) {
                    var errorMsg = '';
                    data.errors.forEach(function(error) {
                        errorMsg += '<strong>Row ' + error.key + ':</strong> Error: ' + error.error + '<br>';
                    });
                    $("#importLogisticModal").find('.error-panel').removeClass('hide');
                    $("#importLogisticModal").find('.error-panel').find('.panel-body').append(errorMsg);
                    swal('Done with errors', data.message, 'error');
                } else {
                    swal('Oops', 'Please select file or may be you\'ve choase wrrong file', 'error');
                }

                $(".up_csv_utlity").prop("disabled", false);
                $(".up_csv_utlity").val('Upload');
            }
        });
    }

    function loadData(query){
    	//Load logistics data
		
    }

	$(document).ready(function(){
		var curStatus=$("#current_status").val();
		$.ajax({
			url: base_url+"index.php/Crud/load_tab_data",
			data:{a:'user_logistic','cur_status':curStatus},
	       	dataType: 'json',
	       	success: function(response) {
	       	
	       	$('#table3').bootstrapTable({
	            data: response
				,stickyHeader: true
			});
	       },
	       error: function(e) {
	        console.log(e.responseText);
	       }
	    });

		$(document).on('change','#current_status',function(e){
	    	e.preventDefault();
	    	var type=$(this).val();
			window.location = base_url+"index.php/User/load_view_f?a=utility_logistics&loc=crud&cur_status="+type;
	    });

	    $(document).on('click', '#btn_import_logistics', function(event) {
	    	event.preventDefault();
	    	/* Act on the event */
	    	$("#upload_file").trigger('reset');
	    	$(".error-import").empty();
	    	$(".error-panel").addClass('hide');
	    	$("#importLogisticModal").modal("show");
	    });

		//Open logistics modal
		$(document).on("click",".logistic_ins",function(){
    		$.get(base_url+"index.php/Crud/loadLogisticData",{type:'insert'},function(data, status){
    			if(status){
    				$("#mdl_body_logistics").html(data);
    				$(".selectpicker").selectpicker();

    				$("#start_date").datepicker({
				        todayBtn:  1,
				        autoclose: true,
				        format : 'dd-mm-yyyy'
				        
				    }).on('changeDate', function (selected) {
				        var minDate = new Date(selected.date.valueOf());
				        $('#end_date').datepicker('setStartDate', minDate);
				    });

				    $("#end_date").datepicker({format : 'dd-mm-yyyy'})
				        .on('changeDate', function (selected) {
				            var maxDate = new Date(selected.date.valueOf());
				            $('#start_date').datepicker('setEndDate', maxDate);
				        });
				    $("#mdl_logistics").modal({keyboard: false});
    			}
			});
    	});

    	//Add logistics record
    	$(document).on('submit','#frm_logistics',function(e){
    		e.preventDefault();
    		var frm=$(this),btn=$("#btn_add_logistic");
    		btn.attr('disabled',true);

    		$.ajax({
			   	enctype: 'multipart/form-data',
	            url: base_url+"index.php/Crud/insLogistics",
			    data:new FormData(this),
			    dataType:'json',
	            type:"post",
	            processData: false,
	            contentType: false,
	            cache: false
			})
			.done (function(data) {
				var message = data.message ? data.message :'Something went wrrong.';
				if(typeof data != "undefined" && data.status){
					swal('Done', message, 'success').then(function() {
                       location.reload();
                    });
                    $("#mdl_logistics").modal('hide');
				}else{
					swal('Oops', message, 'error');
				}
			})
			.fail (function(jqXHR) {
				swal('Oops', "Something went wrong", 'error');
			})
			.always (function(jqXHROrData) {
				btn.attr('disabled',false);
			});
    	});

    	//Edit logistics
    	$(document).on('click','.user_logistic_edit',function(){
    		var l_id=$(this).data('logistic_id');
    		$.get(base_url+"index.php/Crud/loadLogisticData",{type:'edit',l_id:l_id},function(data, status){
    			if(status){
    				$("#mdl_body_logistics").html(data);
    				$(".selectpicker").selectpicker();

    				$("#start_date").datepicker({
				        todayBtn:  1,
				        autoclose: true,
				        format : 'dd-mm-yyyy'
				        
				    }).on('changeDate', function (selected) {
				        var minDate = new Date(selected.date.valueOf());
				        $('#end_date').datepicker('setStartDate', minDate);
				    });

				    $("#end_date").datepicker({format : 'dd-mm-yyyy'})
				        .on('changeDate', function (selected) {
				            var maxDate = new Date(selected.date.valueOf());
				            $('#start_date').datepicker('setEndDate', maxDate);
				        });
				    $("#mdl_logistics").modal({keyboard: false});
    			}
			});

    	});	

		
		$(".exp_all").click(function(){
			saveAs(new Blob([dataere]),"export.csv");
		});

		// Upload logistics using csv
		$(".up_csv_utlity").click(function(e){
	        e.preventDefault();
	        var formData=new FormData();
	        formData.append('file', $('#logisticFile')[0].files[0]);
	        url = base_url+'index.php/upload/importLogistic';
	        
	        if (formData.get('file') != 'undefined'){
	            $(this).val('Please wait...');
	            $(this).prop("disabled", true);
	            uploadUtility(
	                url,
	                formData
	            );
	        }else {
	            swal('Oops', 'Please select file', 'error');
	        }
		});
		
		//Download sample file of a logistics
		$("#downloadLogisticTemplate").click(function(e){
	       	document.location.href=base_url+'index.php/upload/DownloadUerStatusTem?tab=import_logistic_csv';
	   	});

		//utiltiy act
		
		$('#toolbar').find('select').change(function () {
	        $('#table3').bootstrapTable('refreshOptions', {
	            exportDataType: $(this).val()
	        });
	    });
	});
	

</script>

</body>
</html>