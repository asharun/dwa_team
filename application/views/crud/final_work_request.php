<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
 $file_nm='Work_Request';
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
 $access_str1=explode("|",$a_right1);	
$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

$role_id            = $this->session->userdata('role_id');
if($role_id==1 || $role_id==2)
{
if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
}else{
	 redirect('user/login_view');
}

 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>
				<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			<div class="col-md-10">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<!--<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop" style='background-color:#8BC34A;color:#8BC34A'></i>
					<span class="fil_val" style="font-size: 12px;">Approved</span>					
					</div> -->
					<!-- <div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop" style='background-color:#d2463b;color:#d2463b'></i>
					<span class="fil_val" style="font-size: 12px;">Rejected</span>
					</div> -->
					<!-- <div class="col-md-3 col_sp">									
					<i class="glyphicon glyphicon-stop " style='background-color:#f0ad4e;color:#f0ad4e'></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>					
					</div>-->
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop" style='background-color:#FF9800;color:#FF9800'></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>					
					</div>
					<!--div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop" style='background-color:#d2463b;color:#d2463b'></i>
					<span class="fil_val" style="font-size: 12px;">Rejected</span>
					</div-->
			</fieldset>				
			</div>			
			</div>
			<?php include('ot_header.php'); ?>
			<div class='row hid1'>	
					<div class='col-md-12'>	
			<?php
			$file_nm=str_replace('.php','',basename(__FILE__));
				//echo "<button class='stab_stages' ch='Work_Request'>Tables</button> ";
				if($checkaccess==false)
				{
					$po_n_access=array('Confirm Work Request','Approve Work Request');
					$check_work_tab =array_diff_key($check_work_tab, array_flip($po_n_access));
				}				
				//if($checkaccess==false){$check_work_tab =array_diff_key($check_work_tab, array_flip((array) 'Confirm Work request'));}
				foreach($check_work_tab as $key=>$value)
				{
					// if(in_array($value,$access_str))
					// {
						$sel='';
						if($value==$file_nm)
						{
							$sel='stab_dis_selec';
						}					
						echo "<button class='stab_stages_1 ".$sel."' ch='".$value."'><span>".$key."</span></button> ";
					//}
				}
			echo "</div></div>";
			?>
					<div class='row hid'>	
						<div class='col-md-12'>
						<div class='row row_style_1 text-center'>
							<div class='col-md-8'>
								<div class="row">	
						<div class='col-md-4'>
							<label class='l_font_fix_3'>Choose Dept:</label>
							<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
							<?php
							foreach ($dept_val as $row)
							{
								$sel='';
									if($dept_opt==$row['dept_id'])
									{
										$sel='selected';																		
									}
								echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
							}
							?>
							</select>
						</div>
									<div class='col-md-4'>
										<label class='l_font_fix_3'>Choose Status:</label>
										<select id='sel_status' class='selectpicker form-control' title="Nothing Selected" data-live-search="false">
													<option value="Approved">Approve</option>
													<option value="UnApproved">Reject</option>										
										</select>
									</div>
									<div class='col-md-4'>
										<label class='l_font_fix_3'>Remarks:</label>
										<input  type='text'  class='form-control toary' id="remarks" />
									</div>
									</div>
									</div>
									<div class='col-md-4 text-left'>
									<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>									
										<button class='btn add_but gre_but' id='update_work' type='button'>Update</button>
									</div>
									
									
								</div>
							</div>
						</div>
						<div class='row row_style_1 text-center'>
						<div id="toolbar">
										<select class="form-control">
												<option value="">Export Page</option>
												<option value="all">Export All</option>
												<option value="selected">Export Selected</option>
										</select>
								</div>
							<table class="display table table-bordered table-responsive" data-filter-control="true"  data-show-export="true" data-checkbox-header="true" data-toolbar="#toolbar" id="table3"   data-search-time-out=500 data-pagination="true" data-search="true">
								<thead>
									<tr>
										 <th data-class="l_font_fix_3" class="chk_all" data-checkbox="true" data-field="state">
										   <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="requester_Name">Requester Name</th>
										   <th data-sortable="true" data-class='l_font_fix_3 u_id' data-filter-control="input" data-field="full_name">Employee Called</th>											  							  
										  <th data-sortable="true" data-class='l_font_fix_3' data-filter-control="input" data-field="project_name">Project Requested</th>	
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-formatter="dateformat"data-field="called_dt">Called Date</th>
										 	<th data-sortable="true" data-class='l_font_fix_3' data-filter-control="input" data-field="combos_type">Approval Type</th>	   
										   <th data-sortable="true" data-class="l_font_fix_3" data-formatter="statusFormat" data-filter-control="input" data-field="conf_status">Status</th>
										   <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="reason">Reason</th>
										   <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="equiv_xps">Projected XPs</th>
										   <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="act_xp">Confirmed XPs</th>
										   <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="in_time">In Time</th>
										   <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="esp_wrk_hrs">Work Hours</th>
										   <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="break_hrs">Break Hours</th>
										   <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="sapience_on">Sapience On</th>
										   <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="conf_users">Confirmed By</th>
										   <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="conf_dts">Confirmed On</th>
										</tr>
								</thead>
							</table>			
						</div>
									
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>


	function buttonFormatter(value, row) {
        return "<span><i class='glyphicon glyphicon-edit t_edit crud_edit_util' tab_id='"+value+"'></i></span>";
    }
    function dateformat(value, row) {
       return moment(value).format('DD-MM-YYYY');
       			
    }
  
	
   	  function statusFormat(value, row){
    		 if(row.conf_status=='Approved'){
 				return "<i class='glyphicon glyphicon-stop' style='background-color:#8BC34A;color:#8BC34A'></i> "+row.conf_status+""
   			 }else if(row.conf_status=='UnApproved'){
 				return "<i class='glyphicon glyphicon-stop' style='background-color:#3f51b5;color:#3f51b5'></i> "+row.conf_status+""
   			 }else if(row.conf_status=='Confirmed'){
 				return "<i class='glyphicon glyphicon-stop' style='background-color:#FF9800;color:#FF9800'></i> "+row.conf_status+""
   			 }
   			 else if(row.conf_status=='Rejected'){
    		return "<i class='glyphicon glyphicon-stop' style='background-color:#d2463b;color:#d2463b'></i> "+row.conf_status+""
    		}else{
    			return "<i class='glyphicon glyphicon-stop' style='background-color:grey;color:grey'></i> "+row.conf_status+""
   			 }
    }
	 $(document).ready(function(){
	
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			var dep_v=$(this).val();					
			window.location = base_url+"index.php/User/load_view_f?a=final_work_request&loc=crud"+"&dept="+dep_v;
		}	
	});
	$('#toolbar').find('select').change(function () {
            $('#table3').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });
	
$.ajax({

	   url: base_url+"index.php/Crud/load_final_work_request?dept="+dep_opt,
       dataType: 'json',
       success: function(response) {
       		//console.log(response)
           $('#table3').bootstrapTable({
              data: response
			  ,stickyHeader: true
           });

       },
       error: function(e) {
           console.log(e.responseText);
       }
    });

var checkedRows=[];
 $('#table3').on('check.bs.table', function (e, row) {
       checkedRows.push({work_request_id:row.work_request_id})
     
  });
  
  $('#table3').on('uncheck.bs.table', function (e, row) {
   		checkedRows.forEach(function(value, index, object) {
   				//console.log(value)
			if(value.work_request_id==row.work_request_id){
       			object.splice(index,1);
       		}	  

   		});
   });
   
    $('#table3').on('check-all.bs.table', function (e, name) {
       		$.each(name,function(index,value){
       			checkedRows.push({work_request_id:value.work_request_id})
       		})


    });
	
     $('#table3').on('uncheck-all.bs.table', function (e, name) {
     			checkedRows.length = 0;
      		 
    });
    
$("#update_work").click(function(e){
   	e.preventDefault(); 
   	$err='';
	$(this).attr("disabled", true);
   		var sel_status=$("#sel_status").val();
		var rem=$("#remarks").val();
   		if(sel_status==''){   			
   			$err+="Select status\n";
   		}if(checkedRows.length==0){
   			$err+="Select users from table by checkbox";
   		}
		// if(rem.length==0){
   			// $err+="Enter Remarks";
   		// }
   		if($err==''){			
		        $.ajax({
		       		 url:base_url+'index.php/crud/upate_work_request_status',
		       		  type:"post",
		             data:{sel_status:sel_status,checkedRows:checkedRows,rem:rem},
		                success: function(data){
		                	if(data) {
								 //  $.post(base_url+"index.php/Notify/work_confirm_mail",{checkedRows:checkedRows,sel_status:sel_status},function(data){			
									//	console.log(data)
										swal({
											    text:"Sucessfully updated",
											    type: "success",
										    icon: "success",
											})
										.then(function() {
											   location.reload();
											});
									//});
							}		                					
		          	    }
		         });
	    }else{
	    	$err1="Please correct the following errors:-\n";
	    	$err1+=$err;
	    	alert($err1);
			$(this).attr("disabled", false);
	    }
   });



	$('#table3').on('click', '.approve_st', function(){
   				var user_row_id=$(this).attr('data-rowid')
   				var btntype=$(this).text();
   				var combotype=$(this).attr('data-combo-type');
				var rem=$(this).val();
   				$(this).attr("disabled", true);
		        $.ajax({
		       		 url:base_url+'index.php/crud/approve_with_combotype',
		       		  type:"post",
		             data:{user_row_id:user_row_id,combotype:combotype},
		                success: function(data){
		                	//alert(data)
		              		swal({
							    text:"Sucessfully updated",
							    type: "success",
						    icon: "success",
							}).then(function() {
							   location.reload();
							});				
		          	    }
		         });

	});
})


</script>
  </body>
</html>