<?php
$user_id=$this->session->userdata('user_id');
//$this->session->set_userdata('pagename',basename(__FILE__));
 //echo $user_id;
  $file_nm='Utility';
$a_right1=str_replace(' ','_',$this->session->userdata('access'));
$access_str1=explode("|",$a_right1);

$a_right=$this->session->userdata('access');
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
  </head>
  <body>

 <?php
  $this->load->view("Header.php");
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php
				$data['file_nm']="Utility";
				$this->load->view('common/sidebar',$data);
			?>
		<div class="col-md-10 c_row">
				<div align="right"><a href="" id="scrollToBottom"><img src="<?= getAssestsUrl() ?>images/down.png" style="height: 30px;"></a></div>
			<div class='row hid1'>
					<div class='col-md-12'>
			<?php
			$file_nm=str_replace('.php','',basename(__FILE__));
				// echo "<button class='stab_stages' ch='Utility'>Tables</button> ";
				foreach($check_tab as $key=>$value)
				{
						$sel='';
						if($value==$file_nm)
						{
							$sel='stab_dis_selec';
						}
						echo "<button class='stab_stages_1 ".$sel."' ch='".$value."'><span>".$key."</span></button> ";
				}
			echo "</div></div>";
			?>		<input type="hidden" class="stab_dis_ut" ch="activity_mst">
					<div class='row hid'>
						<div class='col-md-12'>
						<div class='row row_style_1 text-center'>
										<div class='col-md-6'>
											<div class="row">
											<div class='col-md-4'>
												<label class='l_font_fix_3'>Choose Dept:</label>
												<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
												<?php
												foreach ($dept_val as $row)
												{
													$sel='';
														if($dept_opt==$row['dept_id'])
														{
															$sel='selected';
														}
													echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
												}
												?>
												</select>
											</div>
											<div class='col-md-8 text-left'>
											<label class='l_font_fix_3' style='width:100%;'>New Activity</label>
										 <!--button class='btn add_but gre_but crud_ins_utility' type='button'>+Add Record</button-->
											<button class='btn add_but gre_but act_up_csv' type='button'>Upload CSV</button>
											</div>
											</div>
										</div>
										<div class="col-md-2">
											<label class='l_font_fix_3' style='width:100%;'>Activity Closure</label>
											<button class='btn add_but blue_but close_act' type='button'>Upload CSV</button>
										</div>
										<div class="col-md-4">
											<label class='l_font_fix_3 invisible' style='width:100%;'>Activity</label>
											<button class='btn add_but red_but exp_all' type='button'>Export All Data</button>
										</div>
									</div>

						<div class='modal fade open_col' id='show_gl_crud'>
											<div class='modal-dialog' role='document'>
												<div class='modal-content'>
												  <div class='modal-body' id='modal_edit'>
												 </div>
												</div>
											  </div>
									</div>


									<div class='modal fade open_col' id='show_up_csv'>
											<div class='modal-dialog' role='document'>
												<div class='modal-content'>
												  <div class='modal-body' id='modal_csv'>
												 </div>
												</div>
											  </div>
									</div>
									<div class='modal fade open_col' id='show__act'>
										<div class='modal-dialog' role='document'>
											<div class='modal-content'>
												<div class='modal-body' id='modal_edit'>
													<form class='form-horizontal'  id='upload_fil'>
														<div class='row row_style_1'>
														<div class='col-md-4'>
																	<label class='l_font_fix_3'>Choose Dept:</label>
																	<select id='sel_dept' class='selectpicker form-control' title="Nothing Selected" data-live-search="true"  name="sel_dept">
																	<?php
																	foreach ($dept_val as $row)
																	{
																		$sel='';
																			if($dept_opt==$row['dept_id'])
																			{
																				$sel='selected';
																			}
																		echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
																	}
																	?>
																	</select>
																</div>
															<div class='col-md-8'>
																<label class='l_font_fix_3'>Select a file to upload</label>
																<input type = "file" id="file" name ="file" />
															 </div>
														</div>
														<div class='row row_style_1'>
															<div class='col-md-12'>
																<!--input class='btn add_but act_csv_sub' id='submit' type='button' value="Upload"-->
																]<div class="col-md-2">
																<input class='btn add_but act_csv_sub' id='act_csv_sub' type='submit' value="Upload">
																</div>
																<div class="col-md-2">
																<input type ='button' id='actclosingTem' class='btn add_but' value='Download csv template'/>
																</div>
															</div>
														</div>
													</form>
													<div class='row row_style_1'>
													<div class='col-md-12'>
														</div>
														<div class='col-md-12'>
															<div id='files' style="color:green"></div>
														</div>
													</div>
												</div>
											</div>
										 </div>
									</div>
						<div id="toolbar" style="margin-left: 4px;">
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="selected">Export Selected</option>
							</select>
						</div>
						<table class="display table table-bordered table-responsive" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table3"   data-search-time-out=500 data-pagination="true" data-search="true">
								<thead>
									<tr>
										  <th data-sortable="true" data-class='l_font_fix_3' data-formatter="buttonFormatter" data-field="tab_id">Action</th>
										  <th data-sortable="true" data-class='l_font_fix_3 u_id' data-filter-control="input" data-field="activity_id">ID</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="activity_name">Activity Name</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="project_name">Projects</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="task_name">Task</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="sub_name">Sub Task</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="job_name">Job</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="wu_name">Work Unit</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="is_closed">Closed</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="status_nm">Status</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="ins_user">Ins By</th>
										  <th data-sortable="true" data-class="l_font_fix_3"  data-field="ins_dt" data-formatter="dateSortFormate">Ins At</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="upd_user">Upd By</th>
										  <th data-sortable="true" data-class="l_font_fix_3"  data-field="upd_dt" data-formatter="dateSortFormate">Upd At</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<div align="right"><a href="" id="scrollToTop"><img src="<?= getAssestsUrl() ?>images/up.png" style="height: 30px;"></a></div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" value="" id="pagecount">
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var f_name = '<?php echo $file_nm ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/FileSaver.min.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script  src="<?= getAssestsUrl() ?>js/sweetalert.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
<script>
function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
    var CSV = '';    
    

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";
        
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {
            
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);
        
        //append Label row with line break
        CSV += row + '\r\n';
    }
    
    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";
        
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);
        
        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
    //Generate a file name
    //this will remove the blank-spaces from the title and replace it with an underscore
    var fileName = ReportTitle.replace(/ /g,"_");   
    
    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    
    
    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");    
    link.href = uri;
    
    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}


	//Date sorting formatter
	function dateSortFormate(value, row, index){
		var date_cck = new Date(value*1000);
		if(value){
			return moment(date_cck).format("DD/MM/YYYY HH:mm:ss");
		}
	}

	function buttonFormatter(value, row) {
        return "<span><i class='glyphicon glyphicon-edit t_edit crud_edit_util' tab_id='"+value+"'></i></span>";


// return "<span class='pull-left'><i class='glyphicon glyphicon-edit t_edit crud_edit' tab_id='"+value+"'></i></span><span class='pull-right'><i class='glyphicon glyphicon-info-sign crud_view t_edit' tab_id='"+value+"'></i></span>";
    }

	function ConvertToCSV(objArray) {
            var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
            var str = '';
			var hide=["id","tab_id"];
			var headin=["#,Activity Name,Projects,Tasks,Sub Tasks,Job,Work Units,Closed,Status,Inserted By,Inserted On,Updated By,Updated On"];
			 str += headin + '\r\n';
            for (var i = 0; i < array.length; i++) {
                var line = '';
            for (var index in array[i]) {
				if(hide.indexOf(index)<0)
				{

					var ch_rec=(array[i][index])?array[i][index].replace(/"/g, '\''):array[i][index];
					if (line != '')
					{
						line += ',';
					}

					line += ("'"+ch_rec+"'"=="'null'")?"":'"'+ch_rec+'"';
				}
            }

            str += line + '\r\n';
        }
        return str;
    }

$.ajax({

	   url: base_url+"index.php/Crud/load_tab_data?a="+'activity_mst'+"&dept="+dep_opt,
       dataType: 'json',
       success: function(response) {

           $('#table3').bootstrapTable({
              data: response
			  ,stickyHeader: true,
			  toolbarAlign:"right"

           });
		   dataere=ConvertToCSV(response);
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });

	$(".exp_all").click(function(){
		saveAs(new Blob([dataere]),"export.csv");
	});
	//do_utility_act  bulk
	$("body").on("submit", "#upload_file",function(e){
		$(this).find(".up_csv_sub").attr("disabled", true);
		$(this).find(".up_csv_sub").text('Please wait..');
		var d_id=$("body").find("#sel_dept_imp").val();
		 e.preventDefault();
		//console.log(d_id);
		 if(d_id)
		 {
			// console.log(d_id);
		         $.ajax({
		             url:base_url+'index.php/upload/do_utility_act?d='+d_id,
		             type:"post",
		             data:new FormData(this),
		             processData:false,
		             contentType:false,
		             cache:false,
		             async:false,
		              success: function(data, status, jqXHR){
						
						var data1=JSON.parse(data);
						  var message = (typeof data1.message !== "undefined") ? data1.message : "Something went wrong.";
			if(typeof data1 != "undefined" && data1.status){
				
				var csvErrors = (typeof data1.errors != "undefined") ?  data1.errors : [];
				var alertType = (csvErrors.length > 0) ? "info" : "success";
				if(csvErrors.length > 0){
					JSONToCSVConvertor(csvErrors,'Errors_Utility_Act',true);
				}
				swal("", message, alertType)
				.then((value) => {
				  window.location.reload();
				});
			}else{
				swal("Error", message, "error");
				var btn=$("body").find(".up_csv_sub");
				 btn.attr('disabled',false);
            btn.text('Upload');
			}			
		              	//console.log(data);return;
		              	$(this).find(".up_csv_sub").attr("disabled", false);
		           }
		         });
		 }else{
				$(this).find(".up_csv_sub").attr("disabled", false);
		 }
	});

		//do_utility_activity for closing
	$("body").on("submit", "#upload_fil",function(e){
		$(this).find(".act_csv_sub").attr("disabled", true);
		$(this).find(".act_csv_sub").text('Please wait..');
		var d_id=$("body").find("#sel_dept").val();
		 e.preventDefault();
		//console.log(d_id);
		 if(d_id)
		 {
			// console.log(d_id);
		         $.ajax({
		             url:base_url+'index.php/upload/do_utility_activity?d='+d_id,
		             type:"post",
		             data:new FormData(this),
		             processData:false,
		             contentType:false,
		             cache:false,
		             async:false,
		              success: function(data, status, jqXHR){
						
						var data1=JSON.parse(data);
						  var message = (typeof data1.message !== "undefined") ? data1.message : "Something went wrong.";
			if(typeof data1 != "undefined" && data1.status){				
				var alertType = "success";				
				swal("", message, alertType)
				.then((value) => {
				  window.location.reload();
				});
			}else{
				swal("Error", message, "error");
				var btn=$("body").find(".act_csv_sub");
				 btn.attr('disabled',false);
            btn.text('Upload');
			}			
		              	//console.log(data);return;
		              	$(this).find(".act_csv_sub").attr("disabled", false);
		           }
		         });
		 }else{
				$(this).find(".act_csv_sub").attr("disabled", false);
		 }
	});

	//utiltiy act


$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
			var dep_v=$(this).val();
			window.location = base_url+"index.php/User/load_view_f?loc=crud&a="+f_name+"&dept="+dep_v;
		}
	});

	$('#toolbar').find('select').change(function () {
            $('#table3').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val(),
                toolbarAlign:"right"
            });
        });

	$("body").on("click", ".act_up_csv",function(){
		$.ajax({
				url: base_url+"index.php/Crud/upload_form",
				success: function(data){
					$('#modal_csv').html(data);
			$("#show_up_csv").modal({
            keyboard: false
			});
		}});
	});
	$("body").on("click", ".close_act",function(){
		$("#upload_fil").trigger("reset");
			$("#sel_dept").val(1);
			$("#sel_dept").selectpicker("refresh");
			$("#show__act").modal({
            keyboard: false
				});
		});
	$("body").on("click", ".in_crud_activity",function(){
		$(this).attr("disabled", true);
		var p=($("#us_2 option:selected" ).text()).replace("Nothing Selected", "");;
	var t=($("#us_3 option:selected" ).text()).replace("Nothing Selected", "");;
	var s=($("#us_4 option:selected" ).text()).replace("Nothing Selected", "");;
	var j=($("#us_5 option:selected" ).text()).replace("Nothing Selected", "");;
	var c_text = $.grep([p, t, s, j], Boolean).join("_");

				var C=[];
				C[0]=$(this).attr('tab_id');
				C[1]=c_text;
				C[2]=$("#us_2").val();
				C[3]=$("#us_3").val();
				C[4]=$("#us_4").val();
				C[5]=$("#us_5").val();
				C[6]=$("#us_6").val();
				C[7]=$(".us_7").val();
				C[8]=$(".u_in").attr('user');
				C[10]=$("#us_8").val();
				C[12]='activity_insert';
				C[13]='activity_mst';
				C[15]=$(".dep_1").val();
				if(C[2] && C[3] && C[4] && C[5] && C[6])
				{
				$.post(base_url+"index.php/Crud/insert",{C : C}, function(data, textStatus) {
            if (textStatus == 'success') {
               window.location.reload();
				}
			});
			}else{
				$(this).attr("disabled", false);
					alert("Enter Project,Task,Subtask,Job,Work Unit!");
				}

	});


	$("body").on("change",".dep_1",function(){
		if($(this).val())
		{
	$.ajax({
				url: base_url+"index.php/Crud/load_data_c",
				type: 'post',
				data : {param : "project_mst",sel_0_ID:$(this).val()},
				success: function(response){
					$("body").find('#us_2').html(response).selectpicker('refresh');
				}
			});

		$.ajax({
				url: base_url+"index.php/Crud/load_data_c",
				type: 'post',
				data : {param : "task_mst",sel_0_ID:$(this).val()},
				success: function(response){
					$("body").find('#us_3').html(response).selectpicker('refresh');
				}
			});

		$.ajax({
				url: base_url+"index.php/Crud/load_data_c",
				type: 'post',
				data : {param : "sub_mst",sel_0_ID:$(this).val()},
				success: function(response){
					$("body").find('#us_4').html(response).selectpicker('refresh');
				}
			});

		$.ajax({
				url: base_url+"index.php/Crud/load_data_c",
				type: 'post',
				data : {param : "job_mst",sel_0_ID:$(this).val()},
				success: function(response){
					$("body").find('#us_5').html(response).selectpicker('refresh');
				}
			});

			$.ajax({
				url: base_url+"index.php/Crud/load_data_c",
				type: 'post',
				data : {param : "work_unit_mst",sel_0_ID:$(this).val()},
				success: function(response){
					$("body").find('#us_6').html(response).selectpicker('refresh');
				}
			});
		}
	});

	$("body").on("click", ".up_crud_activity",function(){
$(this).attr("disabled", true);
	var p=($("#us_2 option:selected" ).text()).replace("Nothing Selected", "");
	var t=($("#us_3 option:selected" ).text()).replace("Nothing Selected", "");
	var s=($("#us_4 option:selected" ).text()).replace("Nothing Selected", "");
	var j=($("#us_5 option:selected" ).text()).replace("Nothing Selected", "");
	var c_text = $.grep([p, t, s, j], Boolean).join("_");
				var C=[];
				C[0]=$(this).attr('tab_id');
				C[1]=c_text;
				C[2]=$("#us_2").val();
				C[3]=$("#us_3").val();
				C[4]=$("#us_4").val();
				C[5]=$("#us_5").val();
				C[6]=$("#us_6").val();
				C[7]=$("#us_62").val();
				C[8]=$(".u_in").attr('user');
				C[10]=$("#us_8").val();
				C[12]='activity_update';
				C[13]='activity_mst';
				C[15]=$(".dep_1").val();
				if(C[0])
				{
				$.post(base_url+"index.php/Crud/update",{C : C}, function(data, textStatus) {
            if (textStatus == 'success') {
               window.location.reload();
				}else{
					alert(data);
				}
			});
				}else{
					$(this).attr("disabled", false);
				}
	});

	$(window).on("scroll", function() {
	var scrollHeight = $(document).height();
	var scrollPosition = $(window).height() + $(window).scrollTop();
	if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
	   // alert()
	}
});

        $(function () {
            $('#scrollToBottom').bind("click", function () {
                $('html, body').animate({ scrollTop: $(document).height() }, 1200);
                return false;
            });
            $('#scrollToTop').bind("click", function () {
                $('html, body').animate({ scrollTop: 0 }, 1200);
                return false;
            });
        });

// 	$('body').on('click', '#table3 tr td', function(){

// });

$("body").on("click", "#downloadactTem",function(){
   				document.location.href=base_url+'index.php/upload/DownloadUerStatusTem?tab=utiltiy_activity'

   });
$("body").on("click", "#actclosingTem",function(){
   				document.location.href=base_url+'index.php/upload/DownloadUerStatusTem?tab=utility_closing_activity'

   });


</script>

  </body>
</html>