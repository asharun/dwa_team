<?php
$user_id=$this->session->userdata('user_id');
   $file_nm="Utility";
$a_right1=str_replace(' ','_',$this->session->userdata('access'));
$access_str1=explode("|",$a_right1);

$a_right=$this->session->userdata('access');
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">

	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>"  type="text/css">

  </head>
  <body>
 <?php
  $this->load->view("Header.php");
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>

	<div class="col-md-10 c_row">
		<div class='row hid1'>
            <div class='col-md-12'>
            <?php
            $file_nm=str_replace('.php','',basename(__FILE__));
            // echo "<button class='stab_stages' ch='Utility'>Tables</button> ";
            foreach($check_tab as $key=>$value)
            {
            // if(in_array($value,$access_str))
            // {
                $sel='';
                if($value==$file_nm)
                {
                    $sel='stab_dis_selec';
                }
                echo "<button class='stab_stages_1 ".$sel."' ch='".$value."'><span>".$key."</span></button> ";
            //}
            }
            echo "</div>";
            ?>
		</div>
		<div class='row hid'>
					<div class='col-md-12'>

						<?php
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));
							$day_fm=date('d-M-Y',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;
							}
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));

							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));

							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));

							$date_start = date('Y-m-d', strtotime($day.' -'.($day_w-1).' days'));
							$date_end=date('Y-m-d', strtotime($day.' +'.(7-$day_w).' days'));

					?>



					<hr class="st_hr2">

					<div class='modal fade open_col' id='show_up_csv'>
						<div class='modal-dialog' role='document'>
							<div class='modal-content'>
							  <div class='modal-body' id='modal_csv'>
							 </div>
							</div>
						  </div>
					</div>

			<div class='row third-row head' style='margin-top: 10px;'>

												<?php
												echo "
												<div class='col-md-2'>
												<span>
												<label class='l_font_fix_3' style='width:100%'>Choose Date: </label>
													<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
													</span>
												</div>
												<div class='col-md-2'>
												<label class='l_font_fix_3' style='width:100%'>Choose Time Filter: </label>
													<select name='time-span-filter' id='' class='form-control' onchange='timeSpanFilter(this.value)'>
														<option value='weekly'>Weekly</option>
														<option value='monthly'>Monthly</option>
														<option value='quarterly'>Quarterly</option>
														<option value='half-yearly'>Half Yearly</option>
														<option value='all-time'>All Time</option>
													</select>
												</div>";
												$spanType = !empty($timeSpanType) ? ucwords($timeSpanType) : "Week";
												echo "<div class='col-md-4 cur-month text-center'><span>".$spanType." (".$startDate." to ".$endDate.")</span>
												</div>";

												?>
							</div>

									<div class='row row_style_1' id='c_find'>
											<div class='col-md-12'>
														<a class='arr2_pt arr_loc_crud pull-left' ch='Emp_io_upload_utility' w_val="<?= $weekPrv ?>">&laquo; Prev</a>
														<a class='arr2_pt arr_loc_crud pull-right' ch='Emp_io_upload_utility' w_val="<?= $weekNxt ?>">Next&raquo;</a>
											</div>
									</div>

											<div class='row row_style_1'>


															<div class='col-md-2'>
																<label class='l_font_fix_3'>Choose Dept:</label>
																<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
																<?php
																foreach ($dept_val as $row)
																{
																	$sel='';
																		if($dept_opt==$row['dept_id'])
																		{
																			$sel='selected';
																		}
																	echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
																}
																?>
																</select>
															</div>

															<div class='col-md-4'>
															<label class='l_font_fix_3'>Filter By:</label>
																<select id='sel_2345' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
															<?php
															echo '<option data-hidden="true"></option>';
															if(!isset($level_opt))
															{
																$level_opt=1;
															}
															$lel_val=array("Employees","Projects");
															foreach ($lel_val as $key=>$value)
															{
																$sel='';
																	if($level_opt==$key)
																	{
																		$sel='selected';
																	}
																echo "<option value='".$key."' ".$sel.">".$value."</option>";
															}

																?>
															</select>
															</div>

														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Project/Emp:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
															<?php
															echo '<option data-hidden="true"></option>';
															if($pro_sel_dta)
															{
															//	echo "<option value='0' >All Projects</option>";
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($pro_sel_val==$row2['sel_1_id'])
																	{
																		$sel='selected';
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>
														<?php
														$role_id  = $this->session->userdata('role_id');
														//echo $role_id;
														if($role_id==1)
														{
														echo "<div class='col-md-2 text-right'>";
																echo "<label class='l_font_fix_3 invisible'>Choose Dept:</label>";
																echo "<button class='btn add_but blue_but act_up_csv' type='button'>Upload CSV</button>";
														echo "</div>";
														}
													?>
										</div>
								<hr class="st_hr2">
								<div id="toolbar" >
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="all">Export All</option>
									<option value="selected">Export Selected</option>
							</select>
					</div>
							<table class="display table table-bordered table-responsive" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table" data-search-time-out=500 data-pagination="true" data-search="true">
										<thead>
											<tr>
											<?php
											echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="full_name">Member Name</th>';
											echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="date_val" data-formatter="dateSortFormate">Date</th>';
											echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="in_time">In Time</th>';
											echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="out_time">Out Time</th>';
											echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="esp_wrk_hrs">Esp Work Hrs</th>';
											echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="esp_status">Esp Status</th>';
											echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="sapience_on">Sapience On</th>';
											echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="sapience_off">Sapience Off</th>';
											echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="total_days">Total Days</th>';
											echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="attendance_cnt">Attendance Count</th>';
											  ?>
											</tr>
										</thead>
									</table>

								<div class='modal fade open_col' id='show_gl_col26'>
											<div class='modal-dialog' role='document'>
												<div class='modal-content'>
												  <div class='modal-body' id='modal_edit'>

												 </div>
												</div>
											  </div>
									</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.min.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script  src="<?= getAssestsUrl() ?>js/sweetalert.min.js"></script>
	<script>
		function dateSortFormate(value, row, index){
		var date_cck = new Date(value*1000);
		if(value){
			return moment(date_cck).format("DD/MM/YYYY");
		}
	}

	window.useDateFilters = '<?php echo $useDateFilters ?>';

	$("body").on("click", ".act_up_csv",function(){

		$.ajax({
				url: base_url+"index.php/Crud/upload_form_io",
				success: function(data){
                    //console.log(data);
					$('#modal_csv').html(data);
			$("#show_up_csv").modal({
            keyboard: false
			});
		}});
	});


	$('#toolbar').find('select').change(function () {
            $('#table').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });


$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var ele=Date.parse($(".ch_dt").val());
			var date_v=moment(ele).format("YYYY-MM-DD");
						var lvl=$("body").find("#sel_2345").val();
						var str='';
						str=str+"&pro="+$(this).val();
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
			//window.location = base_url+"index.php/User/load_view_f?loc=crud&a=Emp_io_upload_utility&date_view="+date_v+str;
			var redirectUrl = base_url+"index.php/User/load_view_f?loc=crud&a=Emp_io_upload_utility&date_view="+date_v+str;

						if (window.useDateFilters == '1') {
							redirectUrl = getTimeSpanRedirectUrl(date_v, redirectUrl);
						}
						window.location = redirectUrl;
		}
	});

	function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
    var CSV = '';    
    

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";
        
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {
            
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);
        
        //append Label row with line break
        CSV += row + '\r\n';
    }
    
    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";
        
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);
        
        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
    //Generate a file name
    //this will remove the blank-spaces from the title and replace it with an underscore
    var fileName = ReportTitle.replace(/ /g,"_");   
    
    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    
    
    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");    
    link.href = uri;
    
    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

	$("body").on("change","#sel_2345",function(){
		if($(this).val())
		{
			var ele=Date.parse($(".ch_dt").val());
			var date_v=moment(ele).format("YYYY-MM-DD");
			//var pro_v=$("body").find("#sel_1234").val();
						var str='';
						str=str+"&lvl="+$(this).val();
						// if(pro_v)
						// {
							// str=str+"&pro="+pro_v;
						// }
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
			//window.location = base_url+"index.php/User/load_view_f?loc=crud&a=Emp_io_upload_utility&date_view="+date_v+str;
			var redirectUrl = base_url+"index.php/User/load_view_f?loc=crud&a=Emp_io_upload_utility&date_view="+date_v+str;
						if (window.useDateFilters == '1') {
							redirectUrl = getTimeSpanRedirectUrl(date_v, redirectUrl);
						}
						window.location = redirectUrl;
		}
	});
		var ele=Date.parse($(".ch_dt").val());
	var date_v=moment(ele).format("YYYY-MM-DD");
	var pr='';
	var lev=0;
	if($("#sel_1234").find("option:selected").val())
	{
		pr=$("#sel_1234").find("option:selected").val();
	}

	if($("#sel_2345").find("option:selected").val())
	{
		lev=$("#sel_2345").find("option:selected").val();
	}

	$("body").on("submit", "#upload_file",function(e){
		$(this).find(".up_csv_sub").attr("disabled", true);
		var d_id='';
		 e.preventDefault();
		 //console.log(d_id);
			 //console.log(d_id);
			 // if(d_id)
			 // {
	         $.ajax({
	             url:base_url+'index.php/upload/do_upload_io_feed?d='+d_id,
	             type:"post",
	             data:new FormData(this),
	             processData:false,
	             contentType:false,
	             cache:false,
	             async:false,
	              success: function(data, status, jqXHR){
						
						var data1=JSON.parse(data);
						  var message = (typeof data1.message !== "undefined") ? data1.message : "Something went wrong.";
			if(typeof data1 != "undefined" && data1.status){
				
				var csvErrors = (typeof data1.errors != "undefined") ?  data1.errors : [];
				var alertType = (csvErrors.length > 0) ? "info" : "success";
				if(csvErrors.length > 0){
					JSONToCSVConvertor(csvErrors,'Errors_IO_Upload',true);
				}
				swal("", message, alertType)
				.then((value) => {
				  window.location.reload();
				});
			}else{
				swal("Error", message, "error");
				var btn=$("body").find(".up_csv_sub");
				 btn.attr('disabled',false);
            btn.text('Upload');
			}			
		              	//console.log(data);return;
		              	$(this).find(".up_csv_sub").attr("disabled", false);
		           }
	         });
			 // }else{
				// $(this).find(".up_csv_sub").attr("disabled", false);
		 // }
	});


	$("body").on("focus", ".ch_dt",function(){
	$(this).datepicker({
					format: 'dd-M-yyyy',
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val());
						var date_v=moment(ele).format("YYYY-MM-DD");
						var pro_v=$("body").find("#sel_1234").val();
						var lvl=$("body").find("#sel_2345").val();
						var str='';
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
					var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						//	window.location = base_url+"index.php/User/load_view_f?loc=crud&a=Emp_io_upload_utility&date_view="+date_v+str;

							var redirectUrl = base_url+"index.php/User/load_view_f?loc=crud&a=Emp_io_upload_utility&date_view="+date_v+str;
						if (window.useDateFilters == '1') {
							redirectUrl = getTimeSpanRedirectUrl(date_v, redirectUrl);
						}
						window.location = redirectUrl;
						}
					});
	});



	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
			var p_parm='Project';
			if($("body").find("#sel_2345").val()=='1')
			{
				p_parm='Emp';

			}
			var ele=Date.parse($(".ch_dt").val());
			var date_v=moment(ele).format("YYYY-MM-DD");
			$.ajax({
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:p_parm,file_name:"Employee_Info",dept:$(this).val(),date_view:date_v},
				success: function(response){
					$('#sel_1234').html(response).selectpicker('refresh');
				}
			});
		}
	});

	var ajaxUrl = base_url+"index.php/User/load_table_boots?a=Emp_io_upload&date_view="+date_v+"&pro="+pr+"&lvl="+lev+"&dept="+dep_opt;
	if ('<?php echo $useDateFilters ?>') {
		ajaxUrl = ajaxUrl +'&start_date=' + '<?php echo $startDateFilter; ?>' + '&end_date=' + '<?php echo $endDateFilter; ?>' + '&time_span=' + '<?php echo $timeSpanType; ?>';
	}

	$.ajax({
       url: ajaxUrl,
       dataType: 'json',
       success: function(response) {
           $('#table').bootstrapTable({
              data: response,
			  stickyHeader: true
           });
       },
       error: function(e) {
         //  console.log(e.responseText);
       }
    });
	
	
	$("body").on("click", ".arr_loc_crud",function(){
		var w_val = $(this).attr("w_val");
		var ch = $(this).attr("ch");
		var pro_v=$("body").find("#sel_1234").val();
		var emp_c=$("body").find("#sel_emp").val();
		var dept=$("body").find("#sel_dept_1").val();
						var lvl=$("body").find("#sel_2345").val();
						//console.log(lvl);
						var str='';
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						if(emp_c)
						{
							str=str+"&emp="+emp_c;
						}
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}						
					//window.location = base_url+"index.php/User/load_view_f?a="+ch+"&date_view="+w_val+str;
					
					var redirectUrl = base_url+"index.php/User/load_view_f?loc=crud&a="+ch+"&date_view="+w_val+str;
						
						// Manipulate url if time span fitlers are there in url	
						if (window.useDateFilters == '1') {
							redirectUrl = getTimeSpanRedirectUrl(w_val, redirectUrl);
						}		
						window.location = redirectUrl;
	});
	</script>
  </body>
</html>