<?php
    $user_id=$this->session->userdata('user_id');
     //echo $user_id;
      $file_nm='Operations';
    $a_right1=str_replace(' ','_',$this->session->userdata('access'));
    $access_str1=explode("|",$a_right1);
    
    $a_right=$this->session->userdata('access');
    $access_str=explode("|",$a_right);
    
    if(!$user_id || !in_array($file_nm,$access_str1)){
      redirect('user/login_view');
    }
    $status = $this->input->get('status');
     ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content=" ">
        <meta name="robot" content="index,follow">
        <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
        <meta name="revisit-after" content="30">
        <title>Work Tracker</title>
        <link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
        <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
        <link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
        <link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
        <link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
        <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
        <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
        <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
    </head>
    <body>
        <?php
            $this->load->view("Header.php");
            ?>
        <div class="desc">
        <div class="ic_cont">
        <div class="row ma_row">
            <?php
                $data['file_nm']=$file_nm;
                $this->load->view('common/sidebar',$data);
                ?>
            <div class="col-md-10 c_row">
                <div class='row hid1'>
                    <div class='col-md-12'>
                        <?php
                            $file_nm=str_replace('.php','',basename(__FILE__));
                            	// echo "<button class='stab_stages' ch='Operations'>Tables</button> ";
                            	foreach($check as $key=>$value)
                            	{
                            			$sel='';
                            			if($value==$file_nm)
                            			{
                            				$sel='stab_dis_selec';
                            			}
                            			echo "<button class='stab_stages_1 ".$sel."' ch='".$value."'><span>".$key."</span></button> ";
                            	}
                            echo "</div></div>";
                            ?>
                        <div class='row hid'>
                            <div class='col-md-12'>
                                <div class='row row_style_1 text-center'>
                                    <!-- <div class='col-md-4'> -->
                                        <div class="row">
                                            <div class='col-md-4'>
                                                <label class='l_font_fix_3'>Choose Dept:</label>
                                                <select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
                                                <?php
                                                    foreach ($dept_val as $row)
                                                    {
                                                    	$sel='';
                                                    		if($dept_opt==$row['dept_id'])
                                                    		{
                                                    			$sel='selected';
                                                    		}
                                                    	echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class='col-md-2 text-left'>
                                                <label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>
                                                <button class='btn add_but gre_but crud_ins' type='button' data-type="user_mst">+Add Record</button>
                                            </div>
	                                            <div class="col-md-4">
	                                            	<label class='l_font_fix_3'>Select Status:</label>
	                                            	<?php echo form_dropdown('status',['Active' => 'Active','InActive' => 'InActive'],$status,['class' => 'selectpicker form-control','title'=>'Nothing Selected','id' => 'user_status']); ?>
	                                            </div>
                                        </div>
                                    <!-- </div> -->
                                </div>
                                <div class='modal fade open_col' id='show_gl_crud'>
                                    <div class='modal-dialog' role='document'>
                                        <div class='modal-content'>
                                            <div class='modal-body' id='modal_edit'>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="toolbar" style="margin-left: 4px;">
                                    <select class="form-control">
                                        <option value="">Export Page</option>
                                        <option value="all">Export All</option>
                                        <option value="selected">Export Selected</option>
                                    </select>
                                </div>
                                <table class="display table table-bordered table-responsive" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table3" data-search-time-out=500 data-pagination="true" data-search="true">
                                    <thead>
                                        <tr>
                                            <!--th data-class="l_font_fix_3" data-checkbox="true" data-field="state"></th-->
                                            <th data-sortable="true" data-class='l_font_fix_3' data-formatter="buttonFormatter" data-field="tab_id">Action</th>
                                            <th data-sortable="true" data-class='l_font_fix_3 u_id' data-filter-control="input" data-field="emp_id">ID</th>
                                            <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="user_login_name">Email</th>
                                            <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="full_name">User</th>
                                            <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="role_name">Role</th>
                                            <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="manager" data-undefined-text="N/A">Manager</th>
                                            <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="access_name">IsReview</th>
                                            <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="is_grad">Grading Access</th>
                                            <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="project_id_fk">Projects</th>
                                            <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="status_nm">Status</th>
                                            <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="ins_user">Ins By</th>
                                            <th data-sortable="true" data-class="l_font_fix_3"  data-field="ins_dt" data-formatter="dateSortFormate">Ins At</th>
                                            <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="upd_user">Upd By</th>
                                            <th data-sortable="true" data-class="l_font_fix_3"  data-field="upd_dt" data-formatter="dateSortFormate">Upd At</th>
                                            <th data-sortable="true" data-class='l_font_fix_3' data-formatter="dateJoinSortFormate" data-field="joining_date_intern">Joining Date Intern</th>
                                            <th data-sortable="true" data-class='l_font_fix_3' data-formatter="dateJoinSortFormate" data-field="joining_date_permanant">Joining Date Permanent</th>
                                            <th data-sortable="true" data-class='l_font_fix_3'  data-field="slot">Slot</th>
                                            <th data-sortable="true" data-class='l_font_fix_3'  data-field="sub_department">Sub Department</th>
                                            <th data-sortable="true" data-class='l_font_fix_3'  data-field="unit_name">Unit Name</th>
                                            <th data-sortable="true" data-class='l_font_fix_3'  data-field="vertical">Vertical</th>
                                            <th data-sortable="true" data-class='l_font_fix_3'  data-field="designation">Designation</th>
                                            <th data-sortable="true" data-class='l_font_fix_3'  data-field="p_manager">Manager</th>
                                            <th data-sortable="true" data-class='l_font_fix_3'  data-field="project_owner">Project Owner</th>
                                            <th data-sortable="true" data-class='l_font_fix_3'  data-field="poc">POC</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
        <script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
        <script>var base_url = '<?php echo base_url() ?>';</script>
        <script>var f_name = '<?php echo $file_nm ?>';</script>
        <script>var dep_opt = '<?php echo $dept_opt ?>';</script>
        <script>var status = '<?php echo $status ?>';</script>
        <script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
        <script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>
        <script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>
        <script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>
        <script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>
        <script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
        <script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
        <script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js?v=<?= v_num() ?>"></script>
        <script>
            //Date sorting formatter
            function dateSortFormate(value, row, index){
            	var date_cck = new Date(value*1000);
            	if(value){
            		return moment(date_cck).format("DD/MM/YYYY HH:mm:ss");
            	}
            }
            function dateJoinSortFormate(value, row, index){
            	return value ? moment(value).format("DD/MM/YYYY") : '-';
            }
            
            function buttonFormatter(value, row) {
                   return "<span><i class='glyphicon glyphicon-edit t_edit crud_edit' tab_id='"+value+"' data-type='user_mst'></i></span>";
               }
            $.ajax({
                  url: base_url+"index.php/Crud/load_tab_data?a="+f_name+"&dept="+dep_opt+"&status="+status,
                  dataType: 'json',
                  success: function(response) {
                  	//console.log(response);
				var response =JSON.parse(JSON.stringify(response).replace(/null/g, '""'));
                      $('#table3').bootstrapTable({
                         data: response
            		  ,stickyHeader: true,
            		  toolbarAlign:"right"
                      });
                  },
                  error: function(e) {
                      console.log(e.responseText);
                  }
               });
            
            $("body").on("change","#sel_dept_1",function(){
            	if($(this).val())
            	{
            		var dep_v=$(this).val();
            		window.location = base_url+"index.php/User/load_view_f?loc=crud&a="+f_name+"&dept="+dep_v;
            	}
            });
            
            $("body").on("click",".dept_ch1",function(){
            
            	if($(this).is(':checked'))
            	{
            		$(".dept_ch").removeProp('checked');
            		$("input[name='ppp']").closest(".pro_user").removeClass('hide');
            	}else{
            		$(".dept_ch").each(function(index) {
            				var t=$(this).val();
            				if($(this).prop('checked'))
            						{
            							$("input[dep='"+t+"']").closest(".pro_user").removeClass('hide');
            						}else{
            							$("input[dep='"+t+"']").removeAttr("checked");
            							$("input[dep='"+t+"']").closest(".pro_user").addClass('hide');
            						}
            		});
            	}
            });
            $("body").on("click",".dept_ch",function(){
            			var check=$('.dept_ch1').is(':checked');
            			$(".dept_ch").each(function(index) {
            				var t=$(this).val();
            				if(check)
            				{
            					//$(this).removeProp('checked');
            					$("input[name='ppp']").closest(".pro_user").removeClass('hide');
            				}
            				else{
            						if($(this).prop('checked'))
            						{
            							$("input[dep='"+t+"']").closest(".pro_user").removeClass('hide');
            						}else{
            
            							$("input[dep='"+t+"']").removeAttr("checked");
            							$("input[dep='"+t+"']").closest(".pro_user").addClass('hide');
            						}
            				}
            			});
            });
            
            $("body").on("click", ".in_crud_user",function(){
            	$(this).attr("disabled", true);
            	var tc='',pc='',dc='';
            
            	 $("input[name='jjj']:checked").each(function(index) {
            			if ($(this).attr('at'))
            			{
            			tc = tc+'|'+$(this).attr('at');
            			}
            		});
            	$("input[name='dept_dd']:checked").each(function(index) {
            			if ($(this).val())
            			{
            			dc = dc+','+$(this).val();
            			}
            		});
            
            		 $("input[name='ppp']:checked").each(function(index) {
            			if ($(this).val())
            			{
            			pc = pc+','+$(this).val();
            			}
            		});
            			var C=[];
            			C[0]=$(this).attr('tab_id');
            			C[1]=$(".us_1").val();
            			C[2]=$(".us_2").val();
            			//C[3]=$(".us_3").val();
            			C[4]=$(".us_4").val();
            			C[5]=$("#us_5").val();
            			C[6]=$(".us_6").val();
            			C[7] = tc.substring(1);
            			C[8]=$(".u_in").attr('user');
            			C[9]=$("#us_7").val();
            			C[10]=$("#us_8").val();
            			C[11]=pc.substring(1);
            			C[12]='user_insert';
            			C[13]='user_mst';
            			C[15] = dc.substring(1);
            			C[16] = $("#joining_date_intern").val();
            			C[17] = $("#joining_date_permanant").val();
            			C[18] = $("#slot").val();
            			C[19] = $("#sub_department").val();
            			C[20] = $("#unit_name").val();
            			C[21] = $("#vertical").val();
            			C[22] = $("#designation").val();
            			C[23] = $("#manager").val();
            			C[24] = $("#project_owner").val();
            			C[25] = $("#poc").val();
            			C[26] = $("#is_greading").val();
            			// console.log(C); return false;
            			if(C[1])
            			{
            			$.post(base_url+"index.php/Crud/insert",{C : C}, function(data, textStatus) {
            				if (textStatus == 'success') {
            				 window.location.reload();
            				 // window.location = base_url+"index.php/User/load_view_f?loc=crud&a="+f_name+"&dept="+dep_opt;
            				}
            			});
            			}else{
            				$(this).attr("disabled", false);
            				alert("Enter Mail ID!");
            			}
            
            });

            $(document).on('change', '#user_status', function(event) {
            	event.preventDefault();
            	/* Act on the event */
            	var redirectUrl = base_url + 'index.php/User/load_view_f?a=user_mst&loc=crud&status='+ $(this).val();
            	if(typeof redirectUrl !== "undefine"){
            		window.location.href = redirectUrl;
            	}
            });
            
            $('#toolbar').find('select').change(function () {
                       $('#table3').bootstrapTable('refreshOptions', {
                           exportDataType: $(this).val(),
                           toolbarAlign:"right"
                       });
                   });
            
            
            $("body").on("click", ".up_crud_user",function(){
            	var tc='',pc='',dc='';
            $(this).attr("disabled", true);
            	 $("input[name='jjj']:checked").each(function(index) {
            			if ($(this).attr('at'))
            			{
            			tc = tc+'|'+$(this).attr('at');
            			}
            		});
            		$("input[name='dept_dd']:checked").each(function(index) {
            			if ($(this).val())
            			{
            			dc = dc+','+$(this).val();
            			}
            		});
            
            		 $("input[name='ppp']:checked").each(function(index) {
            			if ($(this).val())
            			{
            			pc = pc+','+$(this).val();
            			}
            		});
            
            			var C=[];
            			C[0]=$(this).attr('tab_id');
            			C[1]=$(".us_1").val();
            			C[2]=$(".us_2").val();
            			//C[3]=$(".us_3").val();
            			C[4]=$(".us_4").val();
            			C[5]=$("#us_5").val();
            			C[6]=$(".us_6").val();
            			C[7] = tc.substring(1);
            			C[8]=$(".u_in").attr('user');
            			C[9]=$("#us_7").val();
            			C[10]=$("#us_8").val();
            			C[11]=pc.substring(1);
            			C[12]='user_update';
            			C[13]='user_mst';
            			C[14]=$("#us_8").attr('old');
            			C[15] = dc.substring(1);
            			C[16] = $("#joining_date_intern").val();
            			C[17] = $("#joining_date_permanant").val();
            			C[18] = $("#slot").val();
            			C[19] = $("#sub_department").val();
            			C[20] = $("#unit_name").val();
            			C[21] = $("#vertical").val();
            			C[22] = $("#designation").val();
            			C[23] = $("#manager").val();
            			C[24] = $("#project_owner").val();
            			C[25] = $("#poc").val();
            			C[26] = $("#is_greading").val();
            			if(C[0])
            			{
            			$.post(base_url+"index.php/Crud/update",{C : C}, function(data, textStatus) {
                       if (textStatus == 'success') {
                          window.location.reload();
            		   //window.location = base_url+"index.php/User/load_view_f?loc=crud&a="+f_name+"&dept="+dep_opt;
            			}
            		});
            			}else{
            				$(this).attr("disabled", false);
            			}
            
            });
        </script>
    </body>
</html>