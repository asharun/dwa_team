<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
  $file_nm='Operations';
$a_right1=str_replace(' ','_',$this->session->userdata('access'));
$access_str1=explode("|",$a_right1);

$a_right=$this->session->userdata('access');
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">

  </head>
  <body>
  <?php
  $this->load->view("Header.php");
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
						<?php
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>
		<div class="col-md-10 c_row">
			<div class='row hid1'>
					<div class='col-md-12'>
			<?php
			$file_nm=str_replace('.php','',basename(__FILE__));
				// echo "<button class='stab_stages' ch='Operations'>Tables</button> ";
				foreach($check as $key=>$value)
				{
						$sel='';
						if($value==$file_nm)
						{
							$sel='stab_dis_selec';
						}
						echo "<button class='stab_stages_1 ".$sel."' ch='".$value."'><span>".$key."</span></button> ";
				}
			echo "</div></div>";
			?>
					<div class='row hid'>
						<div class='col-md-12'>
						<div class='row row_style_1 text-center'>
										<div class='col-md-4'>
											<div class="row">
											<div class='col-md-8'>
												<label class='l_font_fix_3'>Choose Dept:</label>
												<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
												<?php
												foreach ($dept_val as $row)
												{
													$sel='';
														if($dept_opt==$row['dept_id'])
														{
															$sel='selected';
														}
													echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
												}
												?>
												</select>
											</div>
											<div class='col-md-4 text-left'>
											<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>
											<button class='btn add_but gre_but crud_ins' type='button'>+Add Record</button>
											</div>
											</div>
										</div>
									</div>

						<div class='modal fade open_col' id='show_gl_crud'>
											<div class='modal-dialog' role='document'>
												<div class='modal-content'>
												  <div class='modal-body' id='modal_edit'>
												 </div>
												</div>
											  </div>
									</div>
						<div id="toolbar" style="margin-left: 4px;">
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="all">Export All</option>
									<option value="selected">Export Selected</option>
							</select>
					</div>
						<table class="display table table-bordered table-responsive" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table3" data-search-time-out=500 data-pagination="true" data-search="true">
								<thead>
									<tr>
										  <th data-sortable="true" data-class='l_font_fix_3' data-formatter="buttonFormatter" data-field="tab_id">Action</th>
										  <th data-sortable="true" data-class='l_font_fix_3 u_id' data-filter-control="input" data-field="task_id">ID</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="task_name">Tasks</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="status_nm">Status</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="ins_user">Ins By</th>
										  <th data-sortable="true" data-class="l_font_fix_3"  data-field="ins_dt" data-formatter="dateSortFormate">Ins At</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="upd_user">Upd By</th>
										  <th data-sortable="true" data-class="l_font_fix_3"  data-field="upd_dt" data-formatter="dateSortFormate">Upd At</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>

	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var f_name = '<?php echo $file_nm ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js?v=654"></script>

<script>
	//Date sorting formatter
	function dateSortFormate(value, row, index){
		var date_cck = new Date(value*1000);
		if(value){
			return moment(date_cck).format("DD/MM/YYYY HH:mm:ss");
		}
	}

	function buttonFormatter(value, row) {
        return "<span><i class='glyphicon glyphicon-edit t_edit crud_edit' tab_id='"+value+"'></i></span>";

// return "<span class='pull-left'><i class='glyphicon glyphicon-edit t_edit crud_edit' tab_id='"+value+"'></i></span><span class='pull-right'><i class='glyphicon glyphicon-info-sign crud_view t_edit' tab_id='"+value+"'></i></span>";
    }

	$('#toolbar').find('select').change(function () {
            $('#table3').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val(),
                toolbarAlign:"right"
            });
        });
$.ajax({
       url: base_url+"index.php/Crud/load_tab_data?a="+f_name+"&dept="+dep_opt,
       dataType: 'json',
       success: function(response) {
           $('#table3').bootstrapTable({
              data: response
			  ,stickyHeader: true,
			  toolbarAlign:"right"
           });
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
			var dep_v=$(this).val();
			window.location = base_url+"index.php/User/load_view_f?loc=crud&a="+f_name+"&dept="+dep_v;
		}
	});

	$("body").on("click", ".in_crud_task",function(){
		$(this).attr("disabled", true);
				var C=[];
				C[0]=$(this).attr('tab_id');
				C[1]=$(".us_1").val();
				C[8]=$(".u_in").attr('user');
				C[10]=$("#us_8").val();
				C[12]='task_insert';
				C[13]='task_mst';
				C[15]=$(".dep_1").val();
				if(C[1])
				{
				$.post(base_url+"index.php/Crud/insert",{C : C}, function(data, textStatus) {
            if (textStatus == 'success') {
               window.location.reload();
				}
			});
			}else{
				$(this).attr("disabled", false);
					alert("Enter task Name!");
				}

	});

	$("body").on("click", ".up_crud_task",function(){
				$(this).attr("disabled", true);
				var C=[];
				C[0]=$(this).attr('tab_id');
				C[1]=$(".us_1").val();
				C[8]=$(".u_in").attr('user');
				C[10]=$("#us_8").val();
				C[12]='task_update';
				C[13]='task_mst';
				C[14]=$("#us_8").attr('old');
				C[15]=$(".dep_1").val();
				if(C[0])
				{
				$.post(base_url+"index.php/Crud/update",{C : C}, function(data, textStatus) {
				if (textStatus == 'success') {
				   window.location.reload();
					}
				});
				}else{
					$(this).attr("disabled", false);
				}

	});
</script>
  </body>
</html>