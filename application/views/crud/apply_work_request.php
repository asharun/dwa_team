<?php
$user_id=$this->session->userdata('user_id');
 
 //$file_nm=str_replace('.php','',basename(__FILE__));
 $file_nm='Work_Request';
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
 $access_str1=explode("|",$a_right1);	
 //print_r($access_str1);
$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">

	<style>
	.label-large {
  vertical-align: super;
  font-size: large;
}
	</style>
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>
				<div class="col-md-10 c_row">
			<?php include('ot_header.php'); ?>
			<div class='row hid1'>	
					<div class='col-md-12'>	
			<?php
			$file_nm=str_replace('.php','',basename(__FILE__));
				//echo "<button class='stab_stages' ch='Work_Request'>Tables</button> ";
				if($checkaccess==false)
				{
					$po_n_access=array('Confirm Work Request','Approve Work Request');
					$check_work_tab =array_diff_key($check_work_tab, array_flip($po_n_access));
				}
				foreach($check_work_tab as $key=>$value)
				{
					// if(in_array($value,$access_str))
					// {
						$sel='';
						if($value==$file_nm)
						{
							$sel='stab_dis_selec';
						}					
						echo "<button class='stab_stages_1 ".$sel."' ch='".$value."'><span>".$key."</span></button> ";
					//}
				}
			echo "</div></div>";
			?>
					<div class='row hid'>	
						<div class='col-md-12'>	
							<span class="label label-primary label-large center-block"">Requester Details</span>
						<div class='row row_style_1 text-center'>
							<div class='col-md-2'>
								<label class='l_font_fix_3'>Requester Name</label>
								<input type="text" name="RequesterName" id="RequesterName" class="form-control" disabled="disabled" value="<?php echo $this->session->userdata('user_name') ?>">
								<input type="hidden" name="RequesterId" id="RequesterId" class="form-control" disabled="disabled" value="<?php echo $this->session->userdata('user_id') ?>">	
							</div>
							<div class='col-md-2'>
								<label class='l_font_fix_3'>Requester TNL ID</label>
								<input type="text" name="RequesterTlId" id="RequesterTlId" class="form-control" disabled="disabled" value="<?php echo $this->session->userdata('emp_id') ?>">	
							</div>		
							<div class='col-md-2'>
								<label class='l_font_fix_3' >Date</label>
								<?php
								$s_dt=date("d-m-Y");
								echo "<input  type='text' id='Requesterdt'  class='form-control' disabled='disabled' class='s_dt' value='".$s_dt."' />";
								?>
								
							</div>	
							<div class='col-md-2'>
									<label class='l_font_fix_3'>Choose Dept:</label>
									<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
									<?php
									foreach ($dept_val as $row)
									{
										$sel='';
											if($dept_opt==$row['dept_id'])
											{
												$sel='selected';																		
											}
										echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
									}
									?>
									</select>
							</div>	
							<div class='col-md-2'>
								<label class='l_font_fix_3'>Project</label>
								<select id='pro_id' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">							<?php 
													echo '<option data-hidden="true"></option>';
													if($pro_sel_dta)
													{
														//	echo "<option value='0' >All Projects</option>";
														foreach ($pro_sel_dta as $row2)
														{
															$sel='';
																if($pro_sel_va==$row2['sel_1_id'])
																{
																	$sel='selected';																		
																}
															echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
														}
													}
												?>
								</select>
							</div>
											
														
							<div class='col-md-2'>	
										<button class='btn  blue_but' type='button' onclick='show_model()'> +Add Users</button>
							</div>		
						</div>
							<!-- <hr class="str_hr" style="border-top:2px solid #ddd;"> -->
							<span class="label label-warning label-large  center-block">People called</span>	
								<div class="row">								
									<div class="container-fluid">
										  <table class="table" id="mytable">
										    <thead id="hide_thead">
										      <tr>
										        <th>Name</th>
										        <th>Date</th>
										        <th>Combo type</th>
										        <th>Reason</th>
										        <th>Action</th>
										      </tr>
										    </thead>
										    <tbody>
										      
										    </tbody>
										  </table>
									</div>
									<div class='col-md-7'>
											<button class='btn pull-right' style='background-color:#8BC34A;color:white' type='button' id="work_request">Raise Work Request</button>
									</div>
								</div>
												
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class='modal fade open_col' id='show_gl_crud'>	
						<div class='modal-dialog' role='document'>
							<div class='modal-content'>
								<div class='modal-body' id='modal_edit'>		
									<form class='form-horizontal'  id='tbl_upload'>
										<div class='row row_style_1'>
												<div class='col-md-2'>
															<label class='l_font_fix_3'>Choose Member:</label>
															<select  id="emp_name"  name="emp_name"  class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option data-hidden="true"></option>';
															if($pro_sel)
															{
																foreach ($pro_sel as $row2)
																{
																	$sel='';
																		// if($pro_sel_val==$row2['sel_1_id'])
																		// {
																			// $sel='selected';																		
																		// }
																	echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
																}
															}
																?>
															</select>
														
												</div>	
												<div class='col-md-2'>
													<label class='l_font_fix_3'>Calling Date</label>
													<?php
													$s_dt=date("d-m-Y");
													echo "<input  type='text' data-date-format='dd-mm-yyyy' id='calling_dt' name='s_dt' class='s_dt form-control toary' value='".$s_dt."' />";
													?>	
												</div>
												<div class='col-md-1'>
													<label class='l_font_fix_3'>OT</label><br>
														<input  type='radio' class="toary" id="1" name="group1"  value='Ot comp' isChecked="false"/>	
												</div>
												<div class='col-md-2'>
													<label class='l_font_fix_3'>Comp Off</label><br>
													<input  type='radio' class="toary" id="2" name="group1" value='Comp off' isChecked="false" />
												</div>
												<div class='col-md-1'>
													<label class='l_font_fix_3'>None</label><br>
													<input  type='radio' class="toary" id="3" name="group1" value='With No Comp. Off' isChecked="false" />
												</div>
												<div class='col-md-4'>
													<label class='l_font_fix_3'>Reason</label>
													<input  type='text'  class='form-control toary'name="reason" id="reason" />
													
												</div>	
											
										</div>
										<div class='row row_style_1'>
											<div class='col-md-6'></div>
											<button class='btn add_but' type='button' id="addrows" >Add</button>
										</div>  
										</form>
										<div class='row row_style_1'>
										
										</div>							
								</div>								  
							</div>
						 </div>
					</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var f_name = '<?php echo $file_nm ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
// utitlity code
function show_model(){
	
	$("#show_gl_crud").modal({
            keyboard: false
			});	
}
$('.s_dt').datepicker().datepicker("setDate", new Date());
$(document).ready(function() {
	$("#work_request").hide();
	$("#hide_thead").hide();
	var list = [];
	var dulicaterow=false;
	var slnno=1;
	//	
   $("#addrows").click(function () {
   	var validateData = {};
   	var err='';
   		if($("#emp_name").val()==''){
   			err+="Enter Name\n";
   		}
   		if($("#emp_tnl_id").val()==''){
   			err+="Enter Tnl Id\n";
   		}
   		if($('input[name="group1"]:checked').length == 0){
   			err+="Select Combo Type\n";
   		}
   		if($("#reason").val()==''){
   			err+="Enter Reason\n";
   		}
   		if(err==''){
			var tblary=$("#tbl_upload").serializeArray()
				tblary.unshift({ 
			  	name: 'SelectedMemText',
			  	value:$("#emp_name option:selected").text()
			});
			  	tblary.push({ 
			  	name: 'radioselectid',
			  	value:$("input[type='radio']:checked").attr("id")
			});

			$("#hide_thead").show();
     		$("#mytable").each(function () {
		        var tds = '<tr class="warning">';
		        var empId = tblary[1]['value'];
		        var empDate = tblary[2]['value'];
		        var foundUser = $.grep(list, function(v) {
				    return v.emp_name == empId && v.s_dt == empDate;
				});

		        if(foundUser.length > 0){
		        	alert("You are trying to add same user with same date");
		        	return false;
		        }
				jQuery.each(tblary, function (key,value) {
	         		// console.log($.inArray($("#emp_name option:selected").text(), list));
	         		var memberKey = value.name;
	         		if(memberKey === "emp_name" || memberKey === "s_dt"){
						validateData[memberKey] = value.value;
	         		}
		         	if(key==1){
		         		tds += "<td style='display:none' class='um_id' name='um_id' id='um_id'><input type='hidden' value="+value.value+"></td>";
		         	}else if(key==5){
		         		tds += "<td style='display:none' class='um_id' name='um_id' id='um_id'><input type='hidden' value="+value.value+"></td>";
		         	}else{
		         		tds += '<td>'+value.value+'</td>';
		         	}
		        });
	           	list.push(validateData);
	           
		        if(dulicaterow==false){
		        	tds += "<td><button class='btn  remove_row'style='background-color:#FF5722;color:white' type='button' id='remove_row' >Remove from List</button></td>";
		        } 
		        tds += '</tr>';
		        if ($('tbody', this).length > 0) {
		            $('tbody', this).append(tds);
		             //list=[];
		        } else {
		            $(this).append(tds);
		        }
		        slnno++;
	     	});
	     	$("#work_request").show();
		}else{
   			var err1="Please correct the following errors:-\n";
	    	err1+=err;
   			swal(err1);
   		}
	});

   $("#mytable").on('click', '.remove_row', function () {
  	  $(this).closest('tr').remove();
	});
   
var TableData=new Array();
   $("#work_request").click(function (){
	   
   		var RequesterId=$("#RequesterId").val();
   		var RequesterTlId=$("#RequesterTlId").val();
   		var Requesterdt=$("#Requesterdt").val();
   		var pro_id=$("#pro_id").val();
   		var dept_id=$("#sel_dept_1").val();
   		if(pro_id==''){
   			swal("Select project");
   			return;
   		}
   			//console.log(mytable)
   			$("#mytable tr").each(function(row,tr){
   				TableData[row]={ 'Name':$(this).find('td:eq(0)').text(),
   								'Employee_user_id':$(this).find('td:eq(1) input').val(), 
								'Project':$("body").find('#pro_id option:selected').text(),
   								'Date':$(this).find('td:eq(2)').text(),
   								'Combo_type':$(this).find('td:eq(3)').text(),
   								'Reason':$(this).find('td:eq(4)').text(),
   								'radioselectid':$(this).find('td:eq(5) input').val(),
   			}

   		});
   		TableData.shift();
   		//console.log(TableData);
		//return;
		var btn = $(this);
		btn.attr("disabled", true);
        btn.text("Requesting..");
        
		$.ajax({
			url: base_url+'index.php/Crud/work_request', 
			type: 'post',
			 data:{TableDa:JSON.stringify(TableData),RequesterId:RequesterId,RequesterTlId:RequesterTlId,
             	Requesterdt:Requesterdt,pro_id:pro_id,dept_id:dept_id},
		})
		.done(function(data) {
			if(data) {
			   $.post(base_url+"index.php/Notify/work_request_mail",{TableDa:JSON.stringify(TableData),RequesterId:RequesterId,
			   	Requesterdt:Requesterdt,pro_id:pro_id,dept_id:dept_id},function(data){			
					//console.log(data)
					 swal({
						    text:"Sucessfully updated",
						    type: "success",
					    icon: "success",
						}).then(function() {
						  window.location.href=base_url+"index.php/User/load_view_f?a=view_work_request&loc=crud";
						});
				});
			} 
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			btn.attr("disabled", false);
        	btn.text("Raise Work Request");
		});
		
   		$.ajax({
             url:base_url+'index.php/Crud/work_request', 
             type:"post",
             data:{TableDa:JSON.stringify(TableData),RequesterId:RequesterId,RequesterTlId:RequesterTlId,
             	Requesterdt:Requesterdt,pro_id:pro_id,dept_id:dept_id},
              success: function(data){
					if(data) {
							   $.post(base_url+"index.php/Notify/work_request_mail",{TableDa:JSON.stringify(TableData),RequesterId:RequesterId,
							   	Requesterdt:Requesterdt,pro_id:pro_id,dept_id:dept_id},function(data){			
									//console.log(data)
									 swal({
										    text:"Sucessfully updated",
										    type: "success",
									    icon: "success",
										}).then(function() {
										  window.location.href=base_url+"index.php/User/load_view_f?a=view_work_request&loc=crud";
										});
								});
							} 						
		       }
	    });

	});

	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Project",dept:$(this).val()},
				success: function(response){				
					$('#pro_id').html(response).selectpicker('refresh');
					//$("body").find('#emp_name').html(response).selectpicker('refresh');
				}
			});
			
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Emp",dept:$(this).val(),file_name:'apply_work_request'},
				success: function(response){				
					$("body").find('#emp_name').html(response).selectpicker('refresh');
				}
			});
		}	
	});
});
</script>
  </body>
</html>