<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
  $file_nm='Operations';
$a_right1=str_replace(' ','_',$this->session->userdata('access'));
$access_str1=explode("|",$a_right1);

$a_right=$this->session->userdata('access');
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	 <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/jquery-ui.css" rel="Stylesheet"></link>
  </head>
  <body>
 <?php
  $this->load->view("Header.php");
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
						<?php
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>
		<div class="col-md-10 c_row">
			<div class='row hid1'>
					<div class='col-md-12'>
			<?php
			$file_nm=str_replace('.php','',basename(__FILE__));
				// echo "<button class='stab_stages' ch='Operations'>Tables</button> ";
				foreach($check as $key=>$value)
				{
						$sel='';
						if($value==$file_nm)
						{
							$sel='stab_dis_selec';
						}
						echo "<button class='stab_stages_1 ".$sel."' ch='".$value."'><span>".$key."</span></button> ";
				}
			echo "</div></div>";
			?>
					<div class='row hid'>
						<div class='col-md-12'>
						<div class='row row_style_1 text-center'>
										<div class='col-md-10'>
											<div class="row">
											<div class='col-md-4'>
												<label class='l_font_fix_3'>Choose Dept:</label>
												<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
												<?php
												foreach ($dept_val as $row)
												{
													$sel='';
														if($dept_opt==$row['dept_id'])
														{
															$sel='selected';
														}
													echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
												}
												?>
												</select>
											</div>
											<div class='col-md-8 text-left'>
											<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>
											<?php 
											
											if($dept_opt!=1)
											{
											echo "<button class='btn add_but gre_but crud_ins_mids ' type='button'>+Add Record</button>";
											}else{
												echo "<button class='btn add_but gre_but crud_ins_mids' style='display:none;' type='button'>+Add Record</button>";
											}
											?>
											<button class='btn add_but blue_but up_csv' type='button'>Upload CSV</button>
											<!-- <button class='btn add_but red_but exp_all' type='button'>Export All Data</button> -->
											</div>
											</div>
										</div>
									</div>

						<div class='modal fade open_col' id='show_gl_crud'>
											<div class='modal-dialog' role='document'>
												<div class='modal-content'>
												  <div class='modal-body' id='modal_edit'>
												 </div>
												</div>
											  </div>
									</div>


									<div class='modal fade open_col' id='show_up_csv'>
											<div class='modal-dialog' role='document'>
												<div class='modal-content'>
												  <div class='modal-body' id='modal_csv'>
												 </div>
												</div>
											  </div>
									</div>
						<!-- <div id="toolbar" >
							<select class="form-control tbl3">
									<option value="">Export Page</option>
									<option value="selected">Export Selected</option>
							</select>
						</div> -->
						<div id=tbln3>
						<table class="display table table-bordered table-responsive tbl3" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table3" data-search-time-out=500 data-pagination="true" data-search="true">
								<thead>
									<tr>
										  <!--th data-sortable="true" data-class='l_font_fix_3' data-formatter="contentbuttonFormatter" data-field="gen_id">Action</th>
										   <th data-sortable="true" data-class='l_font_fix_3 u_id' data-filter-control="input" data-field="gen_id">ID</th> -->
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="activity_name">Activity Name</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="year">Year</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="syllabus">Syllabus</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="grade">grade</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="subject">Subject</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="chapter">Chapter</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="sub_div">Sub  Div</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="div_num">Div num</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="sec_sub_div">Sec sub div</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="sec_div_num">Sec div num</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="third_sub_div">Third sub div</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="third_div_num">Third div num</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="status_nm">Status</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="inst_user">Ins By</th>
										  <th data-sortable="true" data-class="l_font_fix_3"  data-field="ins_dt" data-formatter="dateSortFormate">Ins At</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="updt_user">Upd By</th>
										  <th data-sortable="true" data-class="l_font_fix_3"  data-field="upd_dt" data-formatter="dateSortFormate">Upd At</th>
									</tr>
								</thead>
							</table>
						</div>
							<div id="tbln4">
							<!-- <div id="toolbar" >
							<select class="form-control tbl4">
									<option value="">Export Page</option>
									<option value="selected">Export Selected</option>
							</select>
							</div> -->
							<div>
							<div id="toolbar" >
								<input type="text" id="mid_code_search"  placeholder="Search for exact Mid" class="form-control">
							</div>
							<table class="display table table-bordered table-responsive tbl4" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table4" data-search-time-out=500 data-pagination="true" data-search="true">
								<thead>
									<tr>
										  <th  data-class='l_font_fix_3' data-formatter="mediabuttonFormatter" data-field="mile_act_id">Action</th>
										  <!-- <th data-sortable="true" data-class='l_font_fix_3 u_id' data-filter-control="input" data-field="mile_act_id">ID</th> -->
                                          <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="project_name">Project Name</th>
                                          <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="milestone_id">Milestone Id</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="status_nm">Status</th>
										  <!-- <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="dept_name">Dept Name</th> -->


									</tr>
								</thead>
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class='modal fade open_col' id='show__act'>
										<div class='modal-dialog' role='document'>
											<div class='modal-content'>
												<div class='modal-body' id='modal_edit'>
													<form class='form-horizontal'  id='upload_fil'>
														<div class='row row_style_1'>
															<div class='col-md-4'>
																	<label class='l_font_fix_3'>Choose Dept:</label>
																	<select id='sel_dept' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
																	<?php
																	foreach ($dept_val as $row)
																	{
																		$sel='';
																			if($dept_opt==$row['dept_id'])
																			{
																				$sel='selected';
																			}
																		echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
																	}
																	?>
																	</select>
																</div>
															<div class='col-md-8'>
																<label class='l_font_fix_3'>Select a file to upload</label>
																<input type = "file" id="file" name ="file" />
															 </div>
														</div>
														<div class='row row_style_1'>
															<div class='col-md-12'>
																<!--input class='btn add_but act_csv_sub' id='submit' type='button' value="Upload"-->
																<div class="col-md-2">
																<input class='btn add_but add_csv_sub' id='submit_csv' type='button' value="Upload">
																</div>
																<div class="col-md-4">
																<input type ='button' id='contentmidtem' class='btn add_but' value='Download content csv template'/>
																</div>
																<div class="col-md-4">
																<input type ='button' id='mediamidtem' class='btn add_but' value='Download media csv template'/>																</div>

															</div>
														</div>
													</form>
													<div class='row row_style_1'>
													<div class='col-md-12'>
														</div>
														<div class='col-md-12'>
															<div id='files' style="color:green"></div>
														</div>
													</div>
												</div>
											</div>
										 </div>
									</div>

<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	 <script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery-ui.js" ></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>

function dateSortFormate(value, row, index){
		var date_cck = new Date(value*1000);
		if(value){
			return moment(date_cck).format("DD/MM/YYYY HH:mm:ss");
		}
	}

	function contentbuttonFormatter(value, row) {

        return "<span><i class='glyphicon glyphicon-edit t_edit crud_edit' tab_id='"+value+"'></i></span>";
    }
    function mediabuttonFormatter(value, row) {

        return "<span><i class='glyphicon glyphicon-edit t_edit crud_media_mid' tab_id='"+value+"'></i></span>";
    }

	// function ConvertToCSV(objArray) {
  //            var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
 //            var str = '';
	// 		var hide=["id","tab_id"];
	// 		var headin=["#,Activity Name,Projects,Tasks,Sub Tasks,Job,Work Units,Closed,Status,Inserted By,Inserted On,Updated By,Updated On"];
	// 		 str += headin + '\r\n';
 //            for (var i = 0; i < array.length; i++) {
 //                var line = '';
 //            for (var index in array[i]) {
	// 			if(hide.indexOf(index)<0)
	// 			{

	// 				var ch_rec=(array[i][index])?array[i][index].replace(/"/g, '\''):array[i][index];
	// 				if (line != '')
	// 				{
	// 					line += ',';
	// 				}

	// 				line += ("'"+ch_rec+"'"=="'null'")?"":'"'+ch_rec+'"';
	// 			}
 //            }

 //            str += line + '\r\n';
 //        }
 //        return str;
 //    }

	// var dataere='';

$.ajax({
	   url: base_url+"index.php/Crud/load_tab_data?a="+'mid_gen'+"&dept="+dep_opt,
      dataType: 'json',
       success: function(response) {
		          	var response =JSON.parse(JSON.stringify(response).replace(/null/g, '""'));
       		if(dep_opt==1){
			       		$('#tbln3').css("display", "block");
			       		$('#tbln4').css("display", "none");
			       		$('#mid_code_search').css("display", "none");
			       		 $('#table3').bootstrapTable({
				              data: response
							  ,stickyHeader: true
			         	  });
			       		}else{
			       			$('.tbl4').show();
			       			$('#tbln4').css("display", "block");
			       			$('#mid_code_search').css("display", "block");
			       			$('#tbln3').css("display", "none");
			       			 $('#table4').bootstrapTable({
				              data: response
							  ,stickyHeader: true
			         	  });
			       		}
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });

// 	$(".exp_all").click(function(){
// 		saveAs(new Blob([dataere]),"export.csv");
// 	});

	 $("#submit_csv").click(function(e){
			$(this).val('Please wait...');
			var sel_dept=$("body").find("#sel_dept").val();
		      e.preventDefault();
				var formData=new FormData();
		 		formData.append('file', $('input[type=file]')[0].files[0]);
		         $.ajax({
		             url:base_url+'index.php/upload/do_mid_gen?d='+sel_dept,
		             type:"post",
		             data:formData,
		             processData:false,
		             contentType:false,
		             cache:false,
		             async:false,
		              success: function(data){
		              	$("#submit").val('Upload');
		              		swal({
								    text:data,
								    type:"success",
								}).then(function() {
								   location.reload();
								});
		           }
		         });

	});

$('.tbl4').hide();
$('#mid_code_search').css("display", "none");
$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
			var dep_v=$(this).val();
			$.ajax({
				   url: base_url+"index.php/Crud/load_tab_data?a="+'mid_gen'+"&dept="+dep_v,
			      dataType: 'json',
			       success: function(response) {
			       		//console.log(response)
			       		window.reponsedata=response;
			       		if(dep_v==1){
			       		$('#tbln3').css("display", "block");
						$('.crud_ins_mids').css("display", "none");
			       		$('#tbln4').css("display", "none");
			       		$('#mid_code_search').css("display", "none");
			       		 $('#table3').bootstrapTable({
				              data: response
							  ,stickyHeader: true
			         	  });
			       		}else{
			       			$('.tbl4').show();
			       			$('#tbln4').css("display", "block");
							
			       			$('#mid_code_search').css("display", "block");
			       			$('.crud_ins_mids').css("display", "inline");
							$('#tbln3').css("display", "none");
			       			 $('#table4').bootstrapTable({
				              data: response
							  ,stickyHeader: true
			         	  });
			       		}


					  // dataere=ConvertToCSV(response);
			       },
				       error: function(e) {
				          // console.log(e.responseText);
				      		 }
			  		  });
					}
		});

	$('#toolbar').find('select').change(function () {
            $('#table3').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });

	$("body").on("click", ".act_up_csv",function(){
		$.ajax({
				url: base_url+"index.php/Crud/upload_form",
				success: function(data){
					$('#modal_csv').html(data);
				$("#show_up_csv").modal({
	            keyboard: false
				});
		}});
	});
	$("body").on("click", ".up_csv",function(){
			$("#show__act").modal({
            keyboard: false
				});
		})

	$("body").on("click", ".in_crud_content_mid",function(){
		$(this).attr("disabled", true);
		var p=($("#project option:selected" ).text()).replace("Nothing Selected", "");;
	    var t=($("#task option:selected" ).text()).replace("Nothing Selected", "");;
	    var s=($("#subtask option:selected" ).text()).replace("Nothing Selected", "");;
	    var j=($("#job option:selected" ).text()).replace("Nothing Selected", "");;
	    var c_text = $.grep([p, t, s, j], Boolean).join("_");
		//console.log(c_text);
			 	var C=[];
			 	C[0]=$(this).attr('tab_id');
				C[1]=c_text;
				C[2]=$("#sel_dept_1").val();
				C[3]=$("#status").val();
				C[4]=$("#syllabus").val();
				C[5]=$("#subject").val();
				C[6]=$("#chapter").val();
				C[7]=$("#subdiv").val();
				C[8]=$("#divnum").val();
				C[9]=$("#grade").val();
				C[10]=$(".u_in").attr('user');
				C[11]='create_insert';
				C[12]='create_mid';
				C[13]=$("#project").val();
				C[14]=$("#task").val();
				C[15]=$("#subtask").val();
				C[16]=$("#job").val();
				C[17]=$("#year").val();
				C[18]=$("#secsubdiv").val();
				C[19]=$("#secdivnum").val();
				C[20]=$("#thirdsubdiv").val();
				C[21]=$("#thirddivnum").val();

				//if( (C[3] && C[4] && C[5] && C[6] && C[7] && C[8] && C[9] && C[11] && C[12] && C[13] && C[14] && C[15] && C[16]))
				//if((C[12] && C[13] && C[14] && C[15] && C[16]))
			if((C[17] && C[14] && C[15] && C[16]))
				{
				$.post(base_url+"index.php/Crud/contentmidinsert",{C : C}, function(data) {
					console.log(data);
	            if (data.trim()=='done') {
	               swal({
						    text:'Sucessfully inserted',
						    type: "success",
						    icon: "success",
						})
	               .then(function() {
						   location.reload();
						});
					}else{
						swal(data);
						$(".in_crud_content_mid").attr("disabled", false);
					}
				});
			}else{
				$(this).attr("disabled", false);
					swal("Fields cannot be empty!");
				}

	});


						var dateID=moment().format("YYYY-MM-DD");

	$("body").on("changed.bs.select", "#project",function(){
		if($(this).val() && $('#sel_dept_1').val())
		{
				$.ajax({
				url: base_url+"index.php/Load_dt/load_data",
				type: 'post',
				data : {param : "sel_2",sel_0_ID:$('#sel_dept_1').val(),sel_1_ID:$(this).val(),sel_2_ID :'',sel_3_ID:'',sel_4_ID:'',dateID:dateID},
				success: function(response){
					$('#task').html(response).selectpicker('refresh');
					$('#subtask').html('').selectpicker('refresh');
					$('#job').html('').selectpicker('refresh');
				}
			});
		}
	});



	$("body").on("changed.bs.select","#task",function(){
		if($(this).val() && $('#sel_dept_1').val())
		{
				$.ajax({
				url: base_url+"index.php/Load_dt/load_data",
				type: 'post',
				data : {param : "sel_3",sel_0_ID:$('#sel_dept_1').val(),sel_1_ID:$('#project').val(),sel_2_ID :$(this).val(),sel_3_ID:'',sel_4_ID:'',dateID:dateID},
				success: function(response){
					$('#subtask').html(response).selectpicker('refresh');
					$('#job').html('').selectpicker('refresh');
				}
			});
		}
	});


	$("body").on("changed.bs.select","#subtask",function(){
		if($(this).val() && $('#sel_dept_1').val())
		{
	$.ajax({
				url: base_url+"index.php/Load_dt/load_data",
				type: 'post',
				data : {param : "sel_4",sel_0_ID:$('#sel_dept_1').val(),sel_1_ID:$('#project').val(),sel_2_ID :$('#task').val(),sel_3_ID:$(this).val(),sel_4_ID:'',dateID:dateID},
				success: function(response){
					$('#job').html(response).selectpicker('refresh');
				}
			});
		}
	});
// 	$("body").on("change",".dep_1",function(){
// 		if($(this).val())
// 		{
// 	$.ajax({
// 				url: base_url+"index.php/Crud/load_data_c",
// 				type: 'post',
// 				data : {param : "project_mst",sel_0_ID:$(this).val()},
// 				success: function(response){
// 					$("body").find('#us_2').html(response).selectpicker('refresh');
// 				}
// 			});

// 		$.ajax({
// 				url: base_url+"index.php/Crud/load_data_c",
// 				type: 'post',
// 				data : {param : "task_mst",sel_0_ID:$(this).val()},
// 				success: function(response){
// 					$("body").find('#us_3').html(response).selectpicker('refresh');
// 				}
// 			});

// 		$.ajax({
// 				url: base_url+"index.php/Crud/load_data_c",
// 				type: 'post',
// 				data : {param : "sub_mst",sel_0_ID:$(this).val()},
// 				success: function(response){
// 					$("body").find('#us_4').html(response).selectpicker('refresh');
// 				}
// 			});

// 		$.ajax({
// 				url: base_url+"index.php/Crud/load_data_c",
// 				type: 'post',
// 				data : {param : "job_mst",sel_0_ID:$(this).val()},
// 				success: function(response){
// 					$("body").find('#us_5').html(response).selectpicker('refresh');
// 				}
// 			});

// 			$.ajax({
// 				url: base_url+"index.php/Crud/load_data_c",
// 				type: 'post',
// 				data : {param : "work_unit_mst",sel_0_ID:$(this).val()},
// 				success: function(response){
// 					$("body").find('#us_6').html(response).selectpicker('refresh');
// 				}
// 			});
// 		}
// 	});

	$("body").on("click", ".up_crud_activity",function(){
$(this).attr("disabled", true);
	var p=($("#project option:selected" ).text()).replace("Nothing Selected", "");
	var t=($("#task option:selected" ).text()).replace("Nothing Selected", "");
	var s=($("#subtask option:selected" ).text()).replace("Nothing Selected", "");
	var j=($("#job option:selected" ).text()).replace("Nothing Selected", "");
	var c_text = $.grep([p, t, s, j], Boolean).join("_");
				var C=[];
				C[0]=$(this).attr('tab_id');
				C[1]=$("#sel_dept_1").val();
				C[2]=$("#project").val();
				C[3]=$("#task").val();
				C[4]=$("#subtask").val();
				C[5]=$("#job").val();
				C[6]=$(".subdiv").val();
				C[7]=$(".divnum").val();
				C[8]=$(".syllabus").val();
				C[9]=$(".chapter").val();
				C[10]=$(".grade").val();
				C[11]='contentmid_update';
				C[12]='mid_gen';
				C[13]=$(".u_in").attr('user');
				C[14]=$("#status").val();
				C[15]=$(".subject").val();
				C[16]=$(".year").val();
				C[17]=$(".SecSubDiv").val();
				C[18]=$(".SecDivNum").val();
				C[19]=$(".ThirdSubDiv").val();
				C[20]=$(".ThirdDivNum").val();
				if(C[0])
				{
				$.post(base_url+"index.php/Crud/contentmidupdate",{C : C}, function(data, textStatus) {
					//console.log(textStatus);
					if(textStatus=='success'){
						swal(data).then(function() {
								   location.reload();
								});
					}else{
						swal("something went wrong!");
						$(".up_crud_activity").attr("disabled", false);

					}
			});
				}else{
					$(this).attr("disabled", false);
				}
	});

//for all mdeia mids
//edit
$("body").on("click", ".crud_media_mid",function(){
       tab_id=$(this).attr('tab_id');
	   // sh=$('body').find('.stab_dis_selec').attr('ch');
	   	 sh="MediaMids";
	    var dept_id=$('#sel_dept_1 option:selected').val();
	   $.ajax({
				url: base_url+"index.php/Crud/edit_tab_data?a="+sh+"&tab_id="+tab_id+"&mid_dept="+dept_id,
				success: function(data){
					$('#modal_edit').html(data);
					$(".selectpicker").selectpicker();
						$("#show_gl_crud").modal({
						keyboard: false
					});
		}});
		});

//add mid
$("body").on("click", ".crud_ins_mids",function(){
	   var dept=$('#sel_dept_1').val();
	    sh=$('body').find('.stab_dis_selec').attr('ch');
		if(dept!=1)
		{
	   $.ajax({
				url: base_url+"index.php/Crud/ins_tab_data?a="+sh+"&dept="+dept,
				success: function(data){
					//alert(data)
					$('#modal_edit').html(data);
					$(".selectpicker").selectpicker();
			$("#show_gl_crud").modal({
            keyboard: false
			});
		}});}

	});
//insert_media_mid

$("body").on("click", ".insert_media_mid",function(){
	   	var dept=$('#sel_dept_1').val();
	    sh=$('body').find('.stab_dis_selec').attr('ch');
	   $.ajax({
				url: base_url+"index.php/Crud/ins_tab_data?a="+sh+"&dept="+dept,
				success: function(data){
					//alert(data)
					$('#modal_edit').html(data);
					$(".selectpicker").selectpicker();
			$("#show_gl_crud").modal({
            keyboard: false
			});
		}});

	});


$("body").on("click", ".inst_media_mid_indus",function(){
		$(this).attr("disabled", true);
		var p=($("#project option:selected" ).text()).replace("Nothing Selected", "");;
			 	var C=[];

				C[0]=$("#media_mid").val();
				C[1]=$("#midproject option:selected" ).val()
				C[2]=$("#sel_dept_1").val();
				C[3]=$(".u_in").attr('user');
				C[4]=$("#sta_tus").val();

				if(C[0] && C[1] && C[4])
				{
				$.post(base_url+"index.php/Crud/contentmidinsert",{C : C}, function(data) {
					//alert(data);
	            if (data.trim()=='done') {
	               swal({
						    text:"Sucessfully Inserted",
						    type: "success",
						    icon: "success",
						})
	               .then(function() {
						   location.reload();
						});
					}else{
						swal("something went wrong!");
						$(".in_crud_content_mid").attr("disabled", false);
					}
				});
			}else{
				$(this).attr("disabled", false);
					alert("Fields cannot be empty!");
				}

	});

$("body").on("click", ".up_crud_mediamids",function(){
$(this).attr("disabled", true);
	var p=($("#project_mid_id option:selected" ).text()).replace("Nothing Selected", "");
				var C=[];
				C[0]=$(this).attr('tab_id');
				C[1]=$("#sel_dept_1").val();
				C[2]=$(".milestone_id").val();
				C[3]=$("#project_mid_id option:selected" ).val()
				C[4]=$(".u_in").attr('user');
				C[5]=$(".status_nm").val();
				if(C[0])
				{
				$.post(base_url+"index.php/Crud/contentmidupdate",{C : C}, function(data, textStatus) {
					//console.log(textStatus);
					if(textStatus=='success'){
						 swal({
						    text:data,
						    type: "success",
						    icon: "success",
						}).then(function() {
								   location.reload();
								});
					}else{
						swal("something went wrong!");
						$(".up_crud_activity").attr("disabled", false);

					}
			});
				}else{
					$(this).attr("disabled", false);
				}
	});
//$("#mediamidtem").hide();
$("body").on("click", "#contentmidtem",function(){
   		document.location.href=base_url+'index.php/upload/DownloadUerStatusTem?tab=contentmid'
   });
$("body").on("click", "#mediamidtem",function(){
   		document.location.href=base_url+'index.php/upload/DownloadUerStatusTem?tab=mediamid'
   });

resultck=[];
	// $("#table4").on('column-search.bs.table',function(evnt, text,value){
	// var res=window.reponsedata;
	// console.log(value);
	// var table = $(this).data('bootstrap.table'); // The table instance
	// 	if(value.length!=0 && value!=''){
	// 		 if(text=='milestone_id'){
	// 	    	var val=value;
	// 			//console.log(val)
	// 			resultdata = jQuery.grep(res, function(key,value) {
	// 			  return key.milestone_id === val;
	// 			});
	// 			//console.log(resultdata)
	// 			if(resultdata.length!=0){
	// 					table.load(resultdata);
	// 			}else{
	// 					//console.log(value.length);
	// 					table.load(resultck);
	// 			}
	//    		}
	// 	}else{
	// 		table.load(res);
	// 	}
	// });

	//AUTO SEARCH MID
	$('#mid_code_search').focus(function() {
		var res=window.reponsedata;
	        $(this).autocomplete({
	      source: function( request, response ){
	    var table = $("#table4").data('bootstrap.table');
		  	var val=request.term;
			if(val.length!=0 && val!=''){


				//console.log(val)
				resultdata = jQuery.grep(res, function(key,value) {
				  return key.milestone_id === val;
				});
				//console.log(resultdata)
				if(resultdata.length!=0){
						table.load(resultdata);
				}else{
						//console.log(value.length);
						table.load(resultck);
				}

		}else{
			table.load(res);
		}


	      },
	       minLength: 0,

	  });
    })


   $("body").on('mouseover', '.cck', function(e) {
   			 $(this).popover('show');
	});
   $("body").on('mouseleave', '.cck', function(e) {
   			 $(this).popover('hide');
	});

</script>
  </body>
</html>