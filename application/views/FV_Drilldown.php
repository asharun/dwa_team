<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm='Floor_View';

$a_right1=str_replace(' ','_',$this->session->userdata('access'));
$access_str1=explode("|",$a_right1);

$a_right=$this->session->userdata('access');
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}


 ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">

	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/dc.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">

  </head>
  <body>
  <?php
  $this->load->view("Header.php");
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>
				<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			<div class="col-md-10">
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>
					</div>
			</fieldset>
			</div>
			</div>

				<div class='row hid1'>
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Floor_View">Weekly Summary</button>
						<button class='stab_stages_2' ch="FV_Wow">Productivity WoW</button>
						<button class='stab_stages_2' ch="FV_Spread">Trends</button>
						<button class='stab_stages_2' ch="FV_Feed">Scatter</button>
						<button class='stab_stages_2' ch="FV_Summary">Descriptives</button>
						<button class='stab_stages_2 stab_dis_selec' ch="FV_Drilldown">Pies</button>
						<button class='stab_stages_2' ch="FV_Member_Info">Weekly Report</button>
						<button class='stab_stages_2' ch="FV_Contributor">Projects & Members</button>
					</div>
				</div>
					<?php
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));

							if($day_w==0)
							{
								$day_w=7;
							}
							$day_fm=date('d-M-Y',strtotime($day));
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));
					?>
					<div class='row hid'>
						<div class='col-md-12'>
			<div class='row row_style_1 text-center'>
							<div class='col-md-12'>
										<button class='stab_stages_2 in_stages' ch="FV_Breakdown">Break Down</button> ||
										<button class='stab_stages_2 stab_dis_selec ' ch="FV_Drilldown">Drill Down</button>
							</div>
					</div>
					<hr class="st_hr2">
			<div class='row third-row head'>

												<?php
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Choose Date: </label>
															<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
														</span>
													</div>
										<div class='col-md-6 cur-month text-center'><span>Break-up (".$week_st." to ".$week_end.")</span>
												</div>	";
												$titl1='';
												$titl2='';
												$titl3='';
												$titl4='';

												?>
							</div>

									<div class='row row_style_1' id='c_find'>
											<div class='col-md-12'>
														<a class='arr pull-left' ch='FV_Drilldown' w_val="<?= $w_prev ?>">&laquo; Prev</a>
														<a class='arr pull-right' ch='FV_Drilldown' w_val="<?= $w_nxt ?>">Next&raquo;</a>
											</div>
									</div>
									<div class='row row_style_1'>
							<div class='col-md-4'>
								<label class='l_font_fix_3'>Choose Dept:</label>
								<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
								<?php
								foreach ($dept_val as $row)
								{
									$sel='';
										if($dept_opt==$row['dept_id'])
										{
											$sel='selected';
										}
									echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
								}
								?>
								</select>
							</div>
							<div class="col-md-2">
									<label class="l_font_fix_3 invisible" style="width:100%;">Update</label>
									<button class="btn add_but blue_but" id='sel_bu' type="button">Filter: Reset All</button>
							</div>
						</div>

			<?php

			echo "<div class='t1'>";
			echo "<div class='row row_style_1'>";
				echo "<div class='col-md-6'>";
					echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Project Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";
						echo "<div class='col-md-12'>";
					echo "<div class='canvas_r1 text-center row'>";
						echo '<div id="chartdiv1" class="col-md-7 col-sm-6 col-xs-12"></div>';
						echo '<div id="legenddiv1" class="col-md-5"col-sm-6 col-xs-12></div>';
					echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "<div class='col-md-6'>";
					echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Element Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";
						echo "<div class='col-md-12'>";
					echo "<div class='canvas_r3 text-center row'>";
						echo '<div id="chartdiv3" class="col-md-7 col-sm-6 col-xs-12"></div>';
						echo '<div id="legenddiv3" class="col-md-5"col-sm-6 col-xs-12></div>';
					echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "</div>";
			echo "</div>";
			echo "<div class='row row_style_1'>";
				echo "<div class='col-md-6'>";
				echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Trade Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";
						echo "<div class='col-md-12'>";
					echo "<div class='canvas_r2 text-center row'>";
						echo '<div id="chartdiv2" class="col-md-7 col-sm-6 col-xs-12"></div>';
						echo '<div id="legenddiv2" class="col-md-5"col-sm-6 col-xs-12></div>';
					echo "</div>";

				echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "<div class='col-md-6'>";
					echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Action Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";
						echo "<div class='col-md-12'>";
					echo "<div class='canvas_r4 text-center row'>";
						echo '<div id="chartdiv4" class="col-md-7 col-sm-6 col-xs-12"></div>';
						echo '<div id="legenddiv4" class="col-md-5 col-sm-6 col-xs-12"></div>';
					echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "</div>";
			echo "</div>";

				?>

			</div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var arrayFromPHP = <?php echo json_encode($tm_data_all); ?>;</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
<script src="<?= getAssestsUrl() ?>js/d3.js" type="text/javascript"></script>
<script src="<?= getAssestsUrl() ?>js/crossfilter.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/dc.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/d3-scale-chromatic.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>

	$("body").on("focus", ".ch_dt",function(){
	$(this).datepicker({
					format: 'dd-M-yyyy',
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val());
						var date_v=moment(ele).format("YYYY-MM-DD");
						var str='';
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						window.location = base_url+"index.php/User/load_view_f?a=FV_Drilldown&date_view="+date_v+str;

						}
					});
	});

		$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
						var ele=Date.parse($("body").find(".ch_dt").val());
						var date_v=moment(ele).format("YYYY-MM-DD");
						var str='';
						var dept=$(this).val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						window.location = base_url+"index.php/User/load_view_f?a=FV_Drilldown&date_view="+date_v+str;
		}
	});

	var data = arrayFromPHP;

	if(arrayFromPHP)
	{
	var pieChart1 = dc.pieChart("#chartdiv1"),
		pieChart2 = dc.pieChart("#chartdiv2"),
		pieChart3 = dc.pieChart("#chartdiv3"),
		pieChart4 = dc.pieChart("#chartdiv4");


var ndx = crossfilter(data),
    projectDimension = ndx.dimension(function (d) {
        return d.project_name;
    }),
    projectGroup = projectDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
    taskDimension = ndx.dimension(function (d) {
        return d.task_name;
    }),
    taskGroup = taskDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
    subDimension = ndx.dimension(function (d) {
        return d.sub_name;
    }),
    subGroup = subDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
    jobDimension = ndx.dimension(function (d) {
        return d.job_name;
    }),
    jobGroup = jobDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    });

	function precise_round(num,decimals) {
   return Math.round(num*Math.pow(10, decimals)) / Math.pow(10, decimals);
}


	// pieChart1.dimension(projectDimension).group(projectGroup).height(300).radius(100).cy(125).externalLabels(5).on('pretransition', function(chart) {
        // chart.selectAll('text.pie-slice').text(function(d) {
            // return d.data.key + ' ' + dc.utils.printSingleValue((d.endAngle - d.startAngle) / (2*Math.PI) * 100) + '%';
        // })
	// });

	pieChart1.dimension(projectDimension).group(projectGroup).height(300).radius(100).cy(125).externalLabels(1);

	pieChart2.dimension(taskDimension).group(taskGroup).height(300).radius(100).cy(125).externalLabels(1);

	pieChart3.dimension(subDimension).group(subGroup).height(300).radius(100).cy(125).externalLabels(1);

	pieChart4.dimension(jobDimension).group(jobGroup).height(300).radius(100).cy(125).externalLabels(1);

	pieChart1.legend(dc.htmlLegend().legendText(function(d)
				{
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)';
				}
			).container('#legenddiv1').horizontal(true).highlightSelected(true));

	pieChart2.legend(dc.htmlLegend().legendText(function(d)
				{
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)';
				}
			).container('#legenddiv2').horizontal(true).highlightSelected(true));

		pieChart3.legend(dc.htmlLegend().legendText(function(d)
				{
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)';
				}
			).container('#legenddiv3').horizontal(true).highlightSelected(true));

		pieChart4.legend(dc.htmlLegend().legendText(function(d)
				{
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)';
				}
			).container('#legenddiv4').horizontal(true).highlightSelected(true));

	// pieChart1.colors(d3.scaleOrdinal(d3.schemePaired));
	// pieChart4.colors(d3.scaleOrdinal(d3.schemeCategory20b));
	$("body").find("#sel_bu").removeClass('hide');
 	dc.renderAll();

	}else{
		$("body").find(".no_dt").removeClass('hide');
		$("body").find("#sel_bu").addClass('hide');
		$("body").find(".canvas_r1").addClass('hide');
		$("body").find(".canvas_r2").addClass('hide');
		$("body").find(".canvas_r3").addClass('hide');
		$("body").find(".canvas_r4").addClass('hide');

	}
	$("body").on("click","#sel_bu",function(){
			pieChart1.filter(null);
			pieChart2.filter(null);
			pieChart3.filter(null);
			pieChart4.filter(null);
			dc.redrawAll();
	});

$(window.document).scroll(function() {
        var windowtop = $(window).scrollTop();

        $("body").find(".scroll-head").each(function(i, e) {
            $(this).removeClass("divposition");
            if (windowtop > $(e).offset().top) {
                $(e).addClass("divposition");
                return;
            }
        })
    });

	</script>
  </body>
</html>