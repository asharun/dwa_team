<?php
$user_id=$this->session->userdata('user_id');
  $file_nm=str_replace('.php','',basename(__FILE__));
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	
$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);
if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>"  type="text/css">
	
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
		<div class='row hid1'>	
			<div class='col-md-12'>
				<button class='stab_stages_2 stab_dis_selec' ch="Reportees">Data</button>
				<button class='stab_stages_2' ch="R_Team_Graphs">Activity-wise Graph</button>
				<button class='stab_stages_2' ch="R_Breakdown">Breakdown</button>
				<button class='stab_stages_2' ch="R_Leave_Confirm"><span>Leave Approval</span> <?php echo '<span class="badge badge-info">'.($la_cnt_val[0]["i_cnt"]+$la_wee_cnt_val[0]["i_cnt"]).'</span>';?></button>
			</div>
		</div>
			<div class='row hid'>	
						<div class='col-md-12'>		
						<?php 	
						if(($s_dt) && ($e_dt))
						{
							$start_dt = date('d-M-Y', strtotime($s_dt));
							$end_dt = date('d-M-Y', strtotime($e_dt));
						}else
						{
							$day = date('Y-m-d H:i:s');
							$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}							
							$week_end = date('Y-m-d H:i:s', strtotime($day.' +'.(7-$day_w).' days'));
							$week_start = date('Y-m-d H:i:s', strtotime($day.'+'.(1-$day_w).' days'));
										
							$start_dt = date('d-M-Y', strtotime($week_start));
							$end_dt = date('d-M-Y',strtotime($week_end));
						}
					?>
			
			<div class='row third-row head'>
						<?php
						echo "<div class='col-md-3'>
								<span>
									<label class='l_font_fix_3'>Start Date: </label>	
									<input id='t_dtpicker' class='s_dt date_fm date-picker' value='".$start_dt."' />
								</span>
							</div>
							<div class='col-md-3'>
								<span>
									<label class='l_font_fix_3'>End Date: </label>	
									<input id='t_dtpicker' class='e_dt date_fm date-picker' value='".$end_dt."' />
								</span>
							</div>
							<div class='col-md-2'>
										<button class='btn add_but gre_but sub_repo' type='button'>Submit</button>
										</div>
							</div>
							<div class='row row_style_1 third-row head'>
				<div class='col-md-12 cur-month text-center'><span>Reportees (".$start_dt." to ".$end_dt.")</span>
						</div>	";
						?>
			</div>				
								
						
											<div class='row row_style_1'>
														
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Level:</label>
																<select id='sel_2345' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option data-hidden="true"></option>';
															if(!$level_opt)
															{
																$level_opt=0;
															}
															$lel_val=array("Activity","Projects","Category","Element","Action","Trade","Date");
															foreach ($lel_val as $key=>$value)
															{
																$sel='';
																	if($level_opt==$key)
																	{
																		$sel='selected';																		
																	}
															echo "<option value='".$key."' ".$sel.">".$value."</option>";
															}
																?>
															</select>
															</div>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Member:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option data-hidden="true"></option>';
															if($pro_sel_dta)
															{
																foreach ($pro_sel_dta as $row2)
																{
																	$sel='';
																		if($pro_sel_val==$row2['sel_1_id'])
																		{
																			$sel='selected';																		
																		}
																	echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
																}
															}
																?>
															</select>
														</div>
										</div>
								<hr class="st_hr2">
								<div id="toolbar" > 
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="all">Export All</option>
									<option value="selected">Export Selected</option>
							</select>
					</div>
							<table class="display table table-bordered table-responsive" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table" data-search-time-out=500 data-pagination="true" data-search="true">
										<thead>
											<tr>
											<?php
											  echo '<th data-class="l_font_fix_3" data-formatter="runningFormatter">#</th>';											  
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="dept_name">Dept</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="val_name">'.$lel_val[$level_opt].'</th>';
											  
											  
											  if($level_opt==6)
											  {											 
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="activity_name">Activity</th>';
											  }	
											  if($level_opt==0 || $level_opt==6)
											  {											  
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="wu_name">Work Units</th>';
											  }											  										  
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="proj_xp">Projected XPs</th>';					  
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="act_xp">Confirmed XPs</th>';
											if($level_opt==0 || $level_opt==6)
											  {											  
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="w_done">Qty Done</th>';
											  }
											  ?>
											</tr>
										</thead>
									</table>								
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script>
	function runningFormatter(value, row, index) {
			index++; 
		{
		return index;
		}	
	}

	$('#toolbar').find('select').change(function () {
            $('#table').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });
		
$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");
						var lvl=$("body").find("#sel_2345").val();
						var str='';
						str=str+"&pro="+$(this).val();
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}						
			window.location = base_url+"index.php/User/load_view_f?a=Reportees&s_dt="+sdt+"&e_dt="+edt+str;
		}
	});
	
$("body").on("change","#sel_2345",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");
	
			var pro_v=$("body").find("#sel_1234").val();
						var str='';
						str=str+"&lvl="+$(this).val();
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}		
			window.location = base_url+"index.php/User/load_view_f?a=Reportees&s_dt="+sdt+"&e_dt="+edt+str;
		}
	});
	
	var ele=Date.parse($('.s_dt').val()); 
	var s_dt=moment(ele).format("YYYY-MM-DD");
	var ele1=Date.parse($('.e_dt').val()); 
	var e_dt=moment(ele1).format("YYYY-MM-DD");
	var pr='';
	var lev=0;
	
	if($("#sel_1234").find("option:selected").val())
	{
		pr=$("#sel_1234").find("option:selected").val();
	}
	
	if($("#sel_2345").find("option:selected").val())
	{
		lev=$("#sel_2345").find("option:selected").val();
	}
	
	$("body").on("click",".sub_repo",function(){
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");	
			var pro_v=$("body").find("#sel_1234").val();
						var str='';
						var lvl=$("body").find("#sel_2345").val();
						
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
						
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						
			window.location = base_url+"index.php/User/load_view_f?a=Reportees&s_dt="+sdt+"&e_dt="+edt+str;
	});
	
	$("body").on("focus", ".s_dt",function(){
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1"
			});
	});
	
	$("body").on("focus", ".e_dt",function(){		
		$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1"
			});
	});

	$.ajax({
       url: base_url+"index.php/User/load_table_boots?a=Reportees&s_dt="+s_dt+"&e_dt="+e_dt+"&pro="+pr+"&lvl="+lev,
       dataType: 'json',
       success: function(response) {
           $('#table').bootstrapTable({
              data: response,
			  stickyHeader: true
           });
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });
	</script>
  </body>
</html>