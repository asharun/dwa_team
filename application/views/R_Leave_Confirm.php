<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm="Reportees";

$a_right1=str_replace(' ','_',$this->session->userdata('access'));
$access_str1=explode("|",$a_right1);

$a_right=$this->session->userdata('access');
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
  </head>
  <body>
 <?php
  $this->load->view("Header.php");
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>
				<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			<div class="col-md-10">
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>
					</div>
			</fieldset>
			</div>
			</div>
				<div class='row hid1'>
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Reportees">Data</button>
						<button class='stab_stages_2' ch="R_Team_Graphs">Activity-wise Graph</button>
						<button class='stab_stages_2' ch="R_Breakdown">Breakdown</button>
						<button class='stab_stages_2 stab_dis_selec' ch="R_Leave_Confirm"><span>Leave Approval</span> <?php echo '<span class="badge badge-info">'.($la_cnt_val[0]["i_cnt"]+$la_wee_cnt_val[0]["i_cnt"]).'</span>';?></button>
					</div>
				</div>
					<?php
						if(($s_dt) && ($e_dt))
						{
							$start_dt = date('d-M-Y', strtotime($s_dt));
							$end_dt = date('d-M-Y', strtotime($e_dt));
						}else
						{
							$day = date('Y-m-d H:i:s');
							$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;
							}
							$week_end = date('Y-m-d H:i:s', strtotime($day.' +'.(7-$day_w).' days'));
							$week_start = date('Y-m-d H:i:s', strtotime($day.'+'.(1-$day_w).' days'));

							$start_dt = date('d-M-Y', strtotime($week_start));
							$end_dt = date('d-M-Y',strtotime($week_end));
						}
					?>
					<div class='row hid'>
						<div class='col-md-12'>
						<div class='row third-row head'>

												<?php
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Start Date: </label>
															<input id='t_dtpicker' class='s_dt date_fm date-picker' value='".$start_dt."' />
														</span>
													</div>
													<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>End Date: </label>
															<input id='t_dtpicker' class='e_dt date_fm date-picker' value='".$end_dt."' />
														</span>
													</div>
													<div class='col-md-2'>
																<button class='btn add_but blue_but sub_repo' type='button'>Submit</button>
																</div>
													</div>
									<div class='row row_style_1 third-row head'>
				<div class='col-md-12 cur-month text-center'><span>Leave Approvals (".$start_dt." to ".$end_dt.")</span>
												</div>	";
												?>
							</div>
									<div class='row row_style_1 scroll-head'>
									<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Status:</label>
																<select id='sel_2345' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
															<?php
															echo '<option data-hidden="true"></option>';
															if(!$level_opt)
															{
																$level_opt=0;
															}
															$lel_val=array("Pending","Approved","Rejected");
															foreach ($lel_val as $key=>$value)
															{
																$sel='';
																	if($level_opt==$key)
																	{
																		$sel='selected';
																	}
															echo "<option value='".$key."' ".$sel.">".$value."</option>";
															}
																?>
															</select>
															</div>

														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Employee:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
															<?php
															echo '<option value="0">All logs</option>';
															if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($pro_sel_val==$row2['sel_1_id'])
																	{
																		$sel='selected';
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>
											<div class='col-md-4'>
											<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>
											<button class='btn add_but gre_but leava_app' type='button'>Approve</button>
											<button class='btn add_but red_but leava_rej' type='button'>Reject</button>
											</div>
										</div>
							<hr class="str_hr" style="border-top:2px solid #ddd;">
						<div id="toolbar" style="margin-left: 4px;">
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="all">Export All</option>
									<?php
											if($level_opt==0)
											{
										  echo '<option value="selected">Export Selected</option>';
											}

										?>

							</select>
					</div>
						<table class="display table table-bordered table-responsive" data-checkbox-header="true" id="table3" data-toolbar="#toolbar" data-filter-control="true" data-show-export="true"  data-search-time-out=500 data-pagination="true" data-search="true">
								<thead>
									<tr>
											<?php
											if($level_opt==0)
											{
										  echo '<th data-class="l_font_fix_3" data-checkbox="true" data-field="state">#</th>';
											}else{
												echo '<th data-class="l_font_fix_3" data-field="state">#</th>';
											}

										  ?>
										  <th data-class='l_font_fix_3 hidden resp_id' data-field="comp_track_id">ID</th>
										  <th data-class='l_font_fix_3 hidden tbl_nm' data-field="tbl_nm">Tb</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="full_name">User</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="leave_type_name">Leave Category</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="is_half_day">Half Day</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="start_date" data-formatter="dateSortFormate">Start Date</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="end_date" data-formatter="dateSortFormate">End Date</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="comp_dt">Comp Off Date</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="work_dt">Worked On</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="reason">Reason</th>
										 <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="d_week">Day of Week</th>
										  <th data-sortable="true" data-align="center" data-halign="center" data-class="l_font_fix_3" data-filter-control="input" data-field="conf_status">Status</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="in_user">Inserted By</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="in_dt" data-formatter="dateInsSortFormate">Inserted On</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="up_user">Updated By</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="up_dt" data-formatter="dateInsSortFormate">Updated On</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>
	//Date sorting formatter
	function dateSortFormate(value, row, index){
		var date_cck = new Date(value*1000);
		if(value){
			return moment(date_cck).format("DD-MMM-YYYY");
		}
	}

	function dateInsSortFormate(value, row, index){
		var date_cck = new Date(value*1000);
		{
			return moment(date_cck).format("DD/MM/YYYY HH:mm:ss");
		}
	}

	$('#toolbar').find('select').change(function () {
            $('#table3').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val(),
                toolbarAlign:"right"
            });
        });

$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val());
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val());
			var edt=moment(ele1).format("YYYY-MM-DD");
						var lvl=$("body").find("#sel_2345").val();
						var str='';
						str=str+"&pro="+$(this).val();
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
			window.location = base_url+"index.php/User/load_view_f?a=R_Leave_Confirm&s_dt="+sdt+"&e_dt="+edt+str;
		}
	});

$("body").on("change","#sel_2345",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val());
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val());
			var edt=moment(ele1).format("YYYY-MM-DD");

			var pro_v=$("body").find("#sel_1234").val();
						var str='';
						str=str+"&lvl="+$(this).val();
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
			window.location = base_url+"index.php/User/load_view_f?a=R_Leave_Confirm&s_dt="+sdt+"&e_dt="+edt+str;
		}
	});

	var ele=Date.parse($('.s_dt').val());
	var s_dt=moment(ele).format("YYYY-MM-DD");
	var ele1=Date.parse($('.e_dt').val());
	var e_dt=moment(ele1).format("YYYY-MM-DD");
	var pr='';
	var lev=0;

	if($("#sel_1234").find("option:selected").val())
	{
		pr=$("#sel_1234").find("option:selected").val();
	}

	if($("#sel_2345").find("option:selected").val())
	{
		lev=$("#sel_2345").find("option:selected").val();
	}

	$("body").on("click",".sub_repo",function(){
			var ele=Date.parse($('.s_dt').val());
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val());
			var edt=moment(ele1).format("YYYY-MM-DD");
			var pro_v=$("body").find("#sel_1234").val();
						var str='';
						var lvl=$("body").find("#sel_2345").val();

						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}

						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}

			window.location = base_url+"index.php/User/load_view_f?a=R_Leave_Confirm&s_dt="+sdt+"&e_dt="+edt+str;
	});

	$("body").on("focus", ".s_dt",function(){
	$(this).datepicker({
					format: 'dd-M-yyyy',
					yearRange: "-1:+1"
			});
	});

	$("body").on("focus", ".e_dt",function(){
		$(this).datepicker({
					format: 'dd-M-yyyy',
					yearRange: "-1:+1"
			});
	});

	$.ajax({
       url: base_url+"index.php/User/load_table_boots?a=R_Leave_Confirm&s_dt="+s_dt+"&e_dt="+e_dt+"&pro="+pr+"&lvl="+lev,
       dataType: 'json',
       success: function(response) {
           $('#table3').bootstrapTable({
              data: response,
			  stickyHeader: true,
			  toolbarAlign:"right"
           });
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });

	$("body").on("click", ".leava_app",function(){
		var tc='',ac='';
		$(this).attr("disabled", true);
		 $("#table3 tr.selected").each(function(index) {
				if ($(this).find(".resp_id").text())
				{
					if($(this).find(".tbl_nm").text()=="comp_track")
					{
						tc = tc+','+$(this).find(".resp_id").text();
					}
					if($(this).find(".tbl_nm").text()=="emp_week_off")
					{
						ac = ac+','+$(this).find(".resp_id").text();
					}
				}
			});
			if(tc || ac)
			{
				var C=[];
				C[1] = tc.substring(1);
				C[2]=$(".u_in").attr('user');
				C[3]="Approved";
				C[4] = ac.substring(1);
				$.post(base_url+"index.php/User/leav_upd_rep",{C : C}, function(data, textStatus) {
					if (textStatus == 'success') {

					if(tc)
					{
							   $.post(base_url+"index.php/Notify/leave_mail_status",{C : tc.substring(1),param:"C",po:""}, function(data, textStatus) {

								});
					}
				if(ac)
				{
			    $.post(base_url+"index.php/Notify/leave_mail_status",{C : ac.substring(1),param:"W",po:""}, function(data, textStatus) {

								});
				}
					 window.location.reload().delay(5000);
				}else{
					$(this).attr("disabled", false);
				}
			});
			}else{
				$(this).attr("disabled", false);
			}
	});

	$("body").on("click", ".leava_rej",function(){
		var tc='',ac='';
		$(this).attr("disabled", true);
		 $("#table3 tr.selected").each(function(index) {
				if ($(this).find(".resp_id").text())
				{
					if($(this).find(".tbl_nm").text()=="comp_track")
					{
						tc = tc+','+$(this).find(".resp_id").text();
					}
					if($(this).find(".tbl_nm").text()=="emp_week_off")
					{
						ac = ac+','+$(this).find(".resp_id").text();
					}
				}
			});
			if(tc || ac)
			{
				var C=[];
				C[1] = tc.substring(1);
				C[2]=$(".u_in").attr('user');
				C[3]="Rejected";
				C[4] = ac.substring(1);
				$.post(base_url+"index.php/User/leav_upd_rep",{C : C}, function(data, textStatus) {
            if (textStatus == 'success') {
               // window.location.reload();
			   if(tc)
			   {
			    $.post(base_url+"index.php/Notify/leave_mail_status",{C : tc.substring(1),param:"C",po:""}, function(data, textStatus) {

								});
			   }
			   if(ac)
			   {
			    $.post(base_url+"index.php/Notify/leave_mail_status",{C : ac.substring(1),param:"W",po:""}, function(data, textStatus) {

								});
			   }
			    window.location.reload().delay(5000);
				}else{
					$(this).attr("disabled", false);
				}
			});
			}else{
				$(this).attr("disabled", false);
			}
	});

</script>
  </body>
</html>