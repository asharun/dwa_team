<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm='Team_Member_Info';

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/dc.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				
				<div class="col-md-10 c_row">
			
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Team_Member_Info">Report Card</button>
						<button class='stab_stages_2 stab_dis_selec' ch="Graph_Drilldown">Pies</button>
						<button class='stab_stages_2' ch="Team_Split_Graph">Swiss Rolls</button>
						<button class='stab_stages_2' ch="Graph_Trends">Trends</button>					
						<button class='stab_stages_2 ' ch="Graph_Sum_Sc">Stats</button>
						<button class='stab_stages_2' ch="Employee_Info">Employee Records</button>				
						<button class='stab_stages_2' ch='Status_Report'>Activity Records</button>						
						<button class='stab_stages_2' ch="Employee_Leave">Leave Tracker</button>
						<button class='stab_stages_2' ch="WPR_Feed">Scatter</button>
					</div>
				</div>
					<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							// $day_w = date('w',strtotime($day));
							
							// if($day_w==0)
							// {
								// $day_w=7;								
							// }
							$day_fm=date('d-M-Y',strtotime($day));
							// $week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							// $w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							// $w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));							
							// $week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));							
							// $week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));					
					?>
					<div class='row hid'>	
						<div class='col-md-12'>			
						<div class='row row_style_1 text-center'>	
							<div class='col-md-12'>					
										<button class='stab_stages_2 in_stages' ch="Graph_Breakdown">Break Down</button> || 
										<button class='stab_stages_2 stab_dis_selec ' ch="Graph_Drilldown">Drill Down</button>
							</div>														
					</div>				
					<hr class="st_hr2">
					<?php include_once 'WprDateFilter.php'; ?>
							
									<div class='row row_style_1' id='c_find'>	
											<div class='col-md-12'>					
														<a class='arr pull-left' ch='Graph_Drilldown' w_val="<?= $weekPrv  ?>">&laquo; Prev</a>						
														<a class='arr pull-right' ch='Graph_Drilldown' w_val="<?= $weekNxt  ?>">Next&raquo;</a>					
											</div>														
									</div>
									<div class='row row_style_1'>	
							<div class='col-md-4'>
								<label class='l_font_fix_3'>Choose Dept:</label>
								<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
								<?php
								foreach ($dept_val as $row)
								{
									$sel='';
										if($dept_opt==$row['dept_id'])
										{
											$sel='selected';																		
										}
									echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
								}
								?>
								</select>
							</div>
							<div class='col-md-4'>
									<label class='l_font_fix_3'>Choose Project:</label>
									
									<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
									<?php 
									echo '<option data-hidden="true"></option>';
									if($pro_sel_dta)
									{
									foreach ($pro_sel_dta as $row2)
									{
										$sel='';
											if($pro_sel_val==$row2['sel_1_id'])
											{
												$sel='selected';																		
												// $titl1=$row2['sel_1_name']." - Category-wise Break-up (".$week_st." to ".$week_end.")";
												// $titl2=$row2['sel_1_name']." - Element-wise Break-up (".$week_st." to ".$week_end.")";
												// $titl3=$row2['sel_1_name']." - Action-wise Break-up (".$week_st." to ".$week_end.")";
												// $titl4=$row2['sel_1_name']." - Trade-wise Break-up (".$week_st." to ".$week_end.")";
											}
										echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
									}
									}
										?>
									</select>
								</div>
								<div class="col-md-2">
															<label for="" class="1_font_fix_3">Status</label>
															<div class="form-group">
																<select name="status-filter" id="status_filter" class="form-control">
																	<option value="projected">Projected</option>
																	<option value="confirmed">Confirmed</option>
																</select>
															</div>
								</div>
								<div class="col-md-2">
									<label class="l_font_fix_3 invisible" style="width:100%;">Update</label>
									<button class="btn add_but blue_but" id='sel_bu' type="button">Filter: Reset All</button>
								</div>								
						</div>									
			<?php
			
			echo "<div class='t1'>";
			echo "<div class='row row_style_1'>";
				echo "<div class='col-md-6'>";
					echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>MID-wise Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";			
						echo "<div class='col-md-12'>";
							echo "<div class='canvas_r5 text-center row'>";								
								echo '<div id="chartdiv5" class="col-md-7 col-sm-6 col-xs-12"></div>';							
								echo '<div id="legenddiv5" class="col-md-5 col-sm-6 col-xs-12"></div>';
							echo "</div>";
						echo "</div>";				
					echo "</div>";
				echo "</div>";
				echo "<div class='col-md-6'>";
					echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Category-wise Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";			
						echo "<div class='col-md-12'>";
						echo "<div class='canvas_r1 text-center row'>";								
							echo '<div id="chartdiv1" class="col-md-7 col-sm-6 col-xs-12"></div>';							
							echo '<div id="legenddiv1" class="col-md-5 col-sm-6 col-xs-12"></div>';
					echo "</div>";
				echo "</div>";				
				echo "</div>";
				echo "</div>";
			echo "</div>";
			echo "<div class='row row_style_1'>";
				echo "<div class='col-md-6'>"; 
				
					echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Element-wise Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";			
						echo "<div class='col-md-12'>";
						echo "<div class='canvas_r3 text-center row'>";								
							echo '<div id="chartdiv3" class="col-md-7 col-sm-6 col-xs-12"></div>';							
							echo '<div id="legenddiv3" class="col-md-5 col-sm-6 col-xs-12"></div>';					
					echo "</div>";
				echo "</div>";
				
				echo "</div>";
				echo "</div>";
				echo "<div class='col-md-6'>";
				echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Trade-wise Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";			
						echo "<div class='col-md-12'>";
						echo "<div class='canvas_r2 text-center row'>";								
							echo '<div id="chartdiv2" class="col-md-7 col-sm-6 col-xs-12"></div>';							
							echo '<div id="legenddiv2" class="col-md-5 col-sm-6 col-xs-12"></div>';					
					echo "</div>";					
				echo "</div>";
				
					
				echo "</div>";
				echo "</div>";
			echo "</div>";
			
				echo "<div class='row row_style_1'>";
				echo "<div class='col-md-6'>";
					echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Action-wise Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";			
						echo "<div class='col-md-12'>";
						echo "<div class='canvas_r4 text-center row'>";								
							echo '<div id="chartdiv4" class="col-md-7 col-sm-6 col-xs-12"></div>';							
							echo '<div id="legenddiv4" class="col-md-5 col-sm-6 col-xs-12"></div>';
							
					echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "</div>";
			echo "</div>";
			
				?>		
			
			</div>			        
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var arrayFromPHP = <?php echo json_encode($tm_data_all); ?>;</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
<script src="<?= getAssestsUrl() ?>js/d3.js" type="text/javascript"></script>
<script src="<?= getAssestsUrl() ?>js/crossfilter.js" type="text/javascript"></script>	 
	 <script src="<?= getAssestsUrl() ?>js/dc.js" type="text/javascript"></script>	 
	 <script src="<?= getAssestsUrl() ?>js/d3-scale-chromatic.min.js" type="text/javascript"></script>	 
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>
	function redirectUser(pro_v,dept,start_date,end_date,filter_val = ""){
		var str = "";
		if(pro_v)
		{
			str=str+"&pro="+pro_v;
		}
		if(dept)
		{
			str=str+"&dept="+dept;
		}
		if(start_date){
			str+='&start_date='+start_date;
		}
		if(end_date){
			str+='&end_date='+end_date;
		}
		if(filter_val && filter_val==="confirmed"){
			str+="&confirmed=true";
		}
		var redirectUrl = base_url+"index.php/User/load_view_f?a=Graph_Drilldown"+str;
		window.location = redirectUrl;
	}
window.useDateFilters = '<?php echo $useDateFilters ?>';
    
	$(".ch_dt").datepicker({
		format: 'dd-M-yyyy',							
		yearRange: "-1:+1",
		weekStart:1
	});

	$(document).on('click', '.btn_dt_filter', function(event) {
		event.preventDefault();
		/* Act on the event */
		var pro_v=$("body").find("#sel_1234").val();
		var dept=$("body").find("#sel_dept_1").val();
		var start_date = moment($("#st_dt").val()).format("YYYY-MM-DD");
		var end_date =moment($("#e_dt").val()).format("YYYY-MM-DD");

		redirectUser(pro_v,dept,start_date,end_date);
		
	});

	$(document).on('change', '#status_filter', function(event) {
		event.preventDefault();
		/* Act on the event */
		var filterVal = $(this).val();
		var pro_v=$("body").find("#sel_1234").val();
		var dept=$("body").find("#sel_dept_1").val();
		var start_date = moment($("#st_dt").val()).format("YYYY-MM-DD");
		var end_date =moment($("#e_dt").val()).format("YYYY-MM-DD");

		redirectUser(pro_v,dept,start_date,end_date,filterVal);
	});
	
		$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Project",dept:$(this).val()},
				success: function(response){				
					$('#sel_1234').html(response).selectpicker('refresh');
				}
			});
		}	
	});

	
	
$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var ele=Date.parse($(".ch_dt").val()); 
			var date_v=moment(ele).format("YYYY-MM-DD");
			var pro_v=$(this).val();
			var dept=$("body").find("#sel_dept_1").val();
			var start_date = moment($("#st_dt").val()).format("YYYY-MM-DD");
			var end_date =moment($("#e_dt").val()).format("YYYY-MM-DD");
			redirectUser(pro_v,dept,start_date,end_date);
		}
	});
	
	var data = arrayFromPHP;	

	if(arrayFromPHP)
	{
	var pieChart1 = dc.pieChart("#chartdiv1"),
		pieChart2 = dc.pieChart("#chartdiv2"),
		pieChart3 = dc.pieChart("#chartdiv3"),
		pieChart4 = dc.pieChart("#chartdiv4");
		pieChart5 = dc.pieChart("#chartdiv5");
	

var ndx = crossfilter(data),
    catDimension = ndx.dimension(function (d) {
        return d.cat_name;
    }),
    catGroup = catDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
    taskDimension = ndx.dimension(function (d) {
        return d.task_name;
    }),
    taskGroup = taskDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
    subDimension = ndx.dimension(function (d) {
        return d.sub_name;
    }),
    subGroup = subDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
    jobDimension = ndx.dimension(function (d) {
        return d.job_name;
    }),
    jobGroup = jobDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
	midDimension = ndx.dimension(function (d) {
        return d.milestone_id;
    }),
    midGroup = midDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    });
	
	function precise_round(num,decimals) {
   return Math.round(num*Math.pow(10, decimals)) / Math.pow(10, decimals);
}

	
	pieChart1.dimension(catDimension).group(catGroup).height(300).radius(100).cy(125).externalLabels(1);
	
	pieChart2.dimension(taskDimension).group(taskGroup).height(300).radius(100).cy(125).externalLabels(1);
	
	pieChart3.dimension(subDimension).group(subGroup).height(300).radius(100).cy(125).externalLabels(1);
	
	pieChart4.dimension(jobDimension).group(jobGroup).height(300).radius(100).cy(125).externalLabels(1);
	pieChart5.dimension(midDimension).group(midGroup).height(300).radius(100).cy(125).externalLabels(1);
	
	pieChart1.legend(dc.htmlLegend().legendText(function(d) 
				{ 
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)'; 
				}
			).container('#legenddiv1').horizontal(true).highlightSelected(true));
		
	pieChart2.legend(dc.htmlLegend().legendText(function(d) 
				{ 
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)'; 
				}
			).container('#legenddiv2').horizontal(true).highlightSelected(true));
		
		pieChart3.legend(dc.htmlLegend().legendText(function(d) 
				{ 
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)'; 
				}
			).container('#legenddiv3').horizontal(true).highlightSelected(true));
		
		pieChart4.legend(dc.htmlLegend().legendText(function(d) 
				{ 
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)'; 
				}
			).container('#legenddiv4').horizontal(true).highlightSelected(true));
			
			pieChart5.legend(dc.htmlLegend().legendText(function(d) 
				{ 
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)'; 
				}
			).container('#legenddiv5').horizontal(true).highlightSelected(true));
	
	$("body").find("#sel_bu").removeClass('hide');
 	dc.renderAll();
	
	}else{
		$("body").find(".no_dt").removeClass('hide');
		$("body").find("#sel_bu").addClass('hide');
		$("body").find(".canvas_r1").addClass('hide');
		$("body").find(".canvas_r2").addClass('hide');
		$("body").find(".canvas_r3").addClass('hide');
		$("body").find(".canvas_r4").addClass('hide');
		$("body").find(".canvas_r5").addClass('hide');
	}
	
	$("body").on("click","#sel_bu",function(){
			pieChart1.filter(null);
			pieChart2.filter(null);
			pieChart3.filter(null);
			pieChart4.filter(null);
			pieChart5.filter(null);
			dc.redrawAll();
	});
	
// $(window.document).scroll(function() {
        // var windowtop = $(window).scrollTop();
        // $("body").find(".scroll-head").each(function(i, e) {
            // $(this).removeClass("divposition");
            // if (windowtop > $(e).offset().top) {
                // $(e).addClass("divposition");
                // return;
            // }
        // })
    // });

	</script>
  </body>
</html>