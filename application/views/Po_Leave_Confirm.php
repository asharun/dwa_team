<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm="Team_Member_Info";

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
} 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
  </head>
  <body>
 <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>					
				<div class="col-md-10 c_row">
                <div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Team_Member_Info">Report Card</button>						
						<button class='stab_stages_2' ch="Graph_Drilldown">Pies</button>
						<button class='stab_stages_2' ch="Team_Split_Graph">Swiss Rolls</button>
						<button class='stab_stages_2' ch="Graph_Trends">Trends</button>					
						<button class='stab_stages_2' ch="Graph_Sum_Sc">Stats</button>
						<button class='stab_stages_2' ch="Employee_Info">Employee Records</button>				
						<button class='stab_stages_2' ch='Status_Report'>Activity Records</button>						
						<button class='stab_stages_2 stab_dis_selec' ch="Employee_Leave">Leave Tracker</button>
						<button class='stab_stages_2' ch="WPR_Feed">Scatter</button>
					</div>
				</div>
				
					<?php 	
						if(($s_dt) && ($e_dt))
						{
							$start_dt = date('d-M-Y', strtotime($s_dt));
							$end_dt = date('d-M-Y', strtotime($e_dt));
						}else
						{
							$day = date('Y-m-d H:i:s');
							//$day = date('Y-m-d H:i:s',strtotime('2019-01-27'));
							//$day_w = date('w',strtotime($day));							
							// if($day_w==0)
							// {
								// $day_w=7;								
							// }	
							// $week_end = date('Y-m-d H:i:s', strtotime($day.' +'.(7-$day_w).' days'));
							// $week_start = date('Y-m-d H:i:s', strtotime($day.'+'.(1-$day_w).' days'));			
							// $start_dt = date('d-M-Y', strtotime($week_start));
							// $end_dt = date('d-M-Y',strtotime($week_end));
							$day_w=date('d',strtotime($day));	
								if($day_w<=25)
								{
									$m_start = date('Y-m-26', strtotime($day . " - 1 month"));									
									$m_end=date("Y-m-25", strtotime($day));	
								}else{
									$m_start=date("Y-m-26", strtotime($day));	
									$m_end=date('Y-m-26', strtotime($day . " + 1 month"));
								}
							$start_dt = date('d-M-Y', strtotime($m_start));
							$end_dt = date('d-M-Y',strtotime($m_end));
							
						}				
					?>
                    
					<div class='row hid'>	
						<div class='col-md-12'>
                        <div class='row row_style_1 text-center'>	
							<div class='col-md-12'>					
										<button class='stab_stages_2 in_stages' ch="Employee_Leave">Weekly Tracker</button> || 
										<button class='stab_stages_2  stab_dis_selec' ch="Po_Leave_Confirm">Leave Approval</button>
							</div>														
					</div>
                    
						<div class='row third-row head'>
												
												<?php
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Start Date: </label>	
															<input id='t_dtpicker' class='s_dt date_fm date-picker' value='".$start_dt."' />
														</span>
													</div>
													<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>End Date: </label>	
															<input id='t_dtpicker' class='e_dt date_fm date-picker' value='".$end_dt."' />
														</span>
													</div>
													<div class='col-md-2'>
																<button class='btn add_but blue_but sub_repo' type='button'>Submit</button>
																</div>
													</div>
									<div class='row row_style_1 third-row head'>
				<div class='col-md-12 cur-month text-center'><span>Leave Approvals (".$start_dt." to ".$end_dt.")</span>
												</div>	";											
												?>
							</div>
									<div class='row row_style_1 scroll-head'>
									<div class='col-md-2'>
                                            <label class='l_font_fix_3'>Choose Dept:</label>
                                            <select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
                                            <?php
                                            foreach ($dept_val as $row)
                                            {
                                                $sel='';
                                                    if($dept_opt==$row['dept_id'])
                                                    {
                                                        $sel='selected';
                                                    }
                                                echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
                                            }
                                            ?>
                                            </select>
                                        </div>
									<div class='col-md-2'>
															<label class='l_font_fix_3'>Choose Status:</label>
																<select id='sel_2345' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option data-hidden="true"></option>';
															if(!$level_opt)
															{
																$level_opt=0;
															}
															$lel_val=array("Pending","Approved","Rejected");
															foreach ($lel_val as $key=>$value)
															{
																$sel='';
																	if($level_opt==$key)
																	{
																		$sel='selected';																		
																	}
															echo "<option value='".$key."' ".$sel.">".$value."</option>";
															}
																?>
															</select>
															</div>
													
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Projects:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option value="0">All logs</option>';
															if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($pro_sel_val==$row2['sel_1_id'])
																	{
																		$sel='selected';																		
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>	
											<div class='col-md-4'>
											<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>	
											
                                        
                                            <?php

											$role_ids = $this->session->userdata('role_id');
											if ($role_ids <= 4) 
											{
											echo "<button class='btn add_but gre_but leava_app' data-status='Approved' type='button' data-is-final-change='false'>Approve</button>											
											<button class='btn add_but red_but leava_rej'  data-status='Rejected' type='button' data-is-final-change='false'>Reject</button>";
											}
											if ($role_ids == 1) 
											{
                                               // echo ' <div class="btn-group" role="group" aria-label="Basic example">
                                                    // <button type="button" class="btn btn-default admin-leave-update-btn" data-status="Approved" data-is-final-change="true">Direct Approve</button>
                                                    // <button type="button" class="btn btn-default admin-leave-update-btn" data-status="Rejected" data-is-final-change="true">Direct Reject</button>
                                                // </div>';
                                            } ?>
											</div>											
										</div>
							<hr class="str_hr" style="border-top:2px solid #ddd;">
						<div id="toolbar" > 
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="all">Export All</option>
									<?php
											if($level_opt==0 && $role_ids<=4)
											{
										  echo '<option value="selected">Export Selected</option>';
											}
										  
										?>
									
							</select>
					</div>
						<table class="display table table-bordered table-responsive" data-checkbox-header="true" id="table3" data-toolbar="#toolbar" data-filter-control="true" data-show-export="true"  data-search-time-out=500 data-pagination="true" data-search="true">
								<thead>
									<tr>
											<?php
											if($level_opt==0 && $role_ids<=4)
											{
										  echo '<th data-class="l_font_fix_3" data-checkbox="true" data-field="state">#</th>';
											}else{
												echo '<th data-class="l_font_fix_3" data-formatter="runningFormatter">#</th>';
											}
											
											$pids= $this->session->userdata('proj_fk');
										  
										  ?>
										  <th data-class='l_font_fix_3 hidden resp_id' data-field="comp_track_id">ID</th>
										  <th data-class='l_font_fix_3 hidden pro_id' data-field="project_ids_fks">ID</th>
										  <th data-class='l_font_fix_3 hidden tbl_nm' data-field="tbl_nm">Tb</th>
										  <?php $project = $this->input->get('pro'); 
										//  print_r($project);
										  if (($project == '' OR $project == 0 OR !($project))): ?>
										  	<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="project_name">Project</th>
											<?php endif; ?>
										  	<th data-sortable="true" data-class="l_font_fix_3 hidden single_project_id" data-filter-control="input"  data-field="project_id">Project Id</th>
										  
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="full_name">User</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="leave_type_name">Leave Category</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="is_half_day">Half Day</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="start_date">Start Date</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="end_date">End Date</th>									  
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="comp_dt">Comp Off Date</th>										  
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input"  data-field="work_dt">Worked On</th>										  
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="reason">Reason</th>
										 <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="d_week">Day of Week</th>																				  
										  <th data-sortable="true" data-align="center" data-halign="center" data-class="l_font_fix_3" data-filter-control="input" data-field="conf_status">Status</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="pending_in" data-formatter="PendingFormatter" >Pending In</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="approved_in">Approved In</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="rejected_in">Rejected In</th>										  
                                          <th data-sortable="true" data-class="l_font_fix_3" data-field="in_user">Inserted By</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="in_dt">Inserted On</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="up_user">Updated By</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="up_dt">Updated On</th>								  
									</tr>
								</thead>
							</table>						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>

function runningFormatter(value, row, index) {
index++; {
return index;
}
}

    window.proectsThatYouOwned = '<?php echo $pids; ?>';

	function PendingFormatter(value, row) {
	     var t=value.split('|');
		 var all_id =t[0].split(',');
		 var compl_id =t[1].split(',');
		var difference = $(all_id).not(compl_id).get();
        return difference;
    }
	
	$('#toolbar').find('select').change(function () {
            $('#table3').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });
		
$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");
						var lvl=$("body").find("#sel_2345").val();
						var str='';
						str=str+"&pro="+$(this).val();
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}					
				var dep_v=$("body").find("#sel_dept_1").val();
						if(dep_v)
						{
							str=str+"&dept="+dep_v;
						}						
			window.location = base_url+"index.php/User/load_view_f?a=Po_Leave_Confirm&s_dt="+sdt+"&e_dt="+edt+str;
		}
	});
	
$("body").on("change","#sel_2345",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");
	
			var pro_v=$("body").find("#sel_1234").val();
						var str='';
						str=str+"&lvl="+$(this).val();
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}		
						var dep_v=$("body").find("#sel_dept_1").val();
						if(dep_v)
						{
							str=str+"&dept="+dep_v;
						}	
			window.location = base_url+"index.php/User/load_view_f?a=Po_Leave_Confirm&s_dt="+sdt+"&e_dt="+edt+str;
		}
	});
	
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
            var ele=Date.parse($('.s_dt').val());
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val());
			var edt=moment(ele1).format("YYYY-MM-DD");

			var pro_v=$("body").find("#sel_1234").val();
			
						var str='';
						str=str+"&lvl="+$("body").find("#sel_2345").val();
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						var dep_v=$(this).val();
						if(dep_v)
						{
							str=str+"&dept="+dep_v;
						}
			window.location = base_url+"index.php/User/load_view_f?a=Po_Leave_Confirm&s_dt="+sdt+"&e_dt="+edt+str;
		}
	});
	
	var ele=Date.parse($('.s_dt').val()); 
	var s_dt=moment(ele).format("YYYY-MM-DD");
	var ele1=Date.parse($('.e_dt').val()); 
	var e_dt=moment(ele1).format("YYYY-MM-DD");
	var pr='';
	var lev=0;
	var dept_j=0;
	
	if($("#sel_1234").find("option:selected").val())
	{
		pr=$("#sel_1234").find("option:selected").val();
	}
	
	if($("#sel_2345").find("option:selected").val())
	{
		lev=$("#sel_2345").find("option:selected").val();
	}
	if($("#sel_dept_1").find("option:selected").val())
	{
		dept_j=$("#sel_dept_1").find("option:selected").val();
	}
	
	
	
	$("body").on("click",".sub_repo",function(){
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");	
			var pro_v=$("body").find("#sel_1234").val();
						var str='';
						var lvl=$("body").find("#sel_2345").val();
						
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
						
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						var dep_v=$("body").find("#sel_dept_1").val();
						if(dep_v)
						{
							str=str+"&dept="+dep_v;
						}	
						
			window.location = base_url+"index.php/User/load_view_f?a=Po_Leave_Confirm&s_dt="+sdt+"&e_dt="+edt+str;
	});
	
	
	
	$("body").on("focus", ".s_dt",function(){
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			});
	});
	
	$("body").on("focus", ".e_dt",function(){		
		$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			});
	});

	$.ajax({
       url: base_url+"index.php/User/load_table_boots?a=Po_Leave_Confirm&s_dt="+s_dt+"&e_dt="+e_dt+"&pro="+pr+"&lvl="+lev+"&dept="+dept_j,
       dataType: 'json',
       success: function(response) {
           $('#table3').bootstrapTable({
              data: response,
			  stickyHeader: true
           });
       },
       error: function(e) {
        //   console.log(e.responseText);
       }
    });
    
    

    $("body").on("click", ".admin-leave-update-btn, .leava_app, .leava_rej", function() {
        // Get Status
        var status = $(this).data('status');
        var isFinalChange = $(this).data('is-final-change');
        // Get List of selected records from the table
        var listOfLeave = getSelectedLeavesFromTable();
        
        if (!listOfLeave.length) {
            alert('Please select records from table');
            return false;
        }

        $(this).attr("disabled", true);
        $(this).text('Working..');
        
        // Update ststus and send email if final status
        updateLeaveStatus(listOfLeave, status, isFinalChange);
    });

    // function getSelectedLeavesFromTable() {
        // var listOfLeaveToBeApprove = [];

        // // We are getting the list of projectIds and cmptackId here to insert projectWise Approval
        // $("#table3 tr.selected").each(function(index) {
            // var approveForProjectList = getCommonElementsInArray(
                // window.proectsThatYouOwned.split(','),
                // $(this).find('.pro_id').text().split(',')
            // );
            // var compTrackId = $(this).find(".resp_id").text();

            // listOfLeaveToBeApprove.push({
                // 'aprroveForProject': approveForProjectList,
                // 'compTrackId': compTrackId
            // });			
        // });
        // return listOfLeaveToBeApprove;
    // }

    // function updateLeaveStatus(listOfLeave, status, isFinalChange) {
        // $.ajax({
            // url: base_url+"index.php/User/upsertPoLeaveStatus",
            // type: 'POST',
            // data: {'listOfLeave' : listOfLeave, 'status': status, 'isFinalChange': isFinalChange},
            // success: function(response) {
                // console.log(response);
                // // Here we are going to handle the email this om suucess ok and only send email to the list which recived in response
                // if (response.approvedLeaveList.length > 0) {
                    // var compTrackIdList = '';
                    // if (status == "Approved") {
                        // compTrackIdList = response.approvedLeaveList.toString();
                    // }
                    // if (status == "Rejected") {
                        // compTrackIdList = response.rejectedLeaveList.toString();
                    // }
                    // $.post(base_url+"index.php/Notify/leave_mail_status",{C : compTrackIdList, param:"C", po:""}, function(data, textStatus) {
                        // if (textStatus == 'success') {
                            // window.location.reload();
                        // }
                    // });
                // } else {
                    // window.location.reload();
                // }
            // }
        // })
    // }
	
	function getSelectedLeavesFromTable() {
        var listOfLeaveToBeUpdated = [];
        var selectedProject = (getQueryStringValue('pro') != '' && getQueryStringValue('pro') != 0) 
        ? getQueryStringValue('pro') 
        : '';

        // We are getting the list of projectIds and cmptackId here to insert projectWise Approval
        $("#table3 tr.selected").each(function(index) {
            // Filter down the "leavesProjectList" array to only change status for selectedProject 
            // if (selectedProject != '') {
            	// var leavesProjectList = getCommonElementsInArray(
	                // window.proectsThatYouOwned.split(','),
	                // $(this).find('.pro_id').text().split(',')
	            // );
	            // leavesProjectList = getCommonElementsInArray(
	                // leavesProjectList,
	                // [selectedProject]
	            // );	
            // }
            // else {
				var leavesProjectList = $(this).find('.single_project_id').text();
          //  }

            var compTrackId = $(this).find(".resp_id").text();

            listOfLeaveToBeUpdated.push({
                'leavesProjectList': leavesProjectList,
                'compTrackId': compTrackId
            });	
			
        });
	        return listOfLeaveToBeUpdated;
    }

    function updateLeaveStatus(listOfLeave, status, isFinalChange) {
        $.ajax({
            url: base_url+"index.php/User/upsertPoLeaveStatus",
            type: 'POST',
            data: {'listOfLeave' : listOfLeave, 'status': status, 'isFinalChange': isFinalChange},
            success: function(response) {
               // console.log(response);
                // Here we are going to handle the email this om suucess ok and only send email to the list which recived in response
                if (response.approvedLeaveList.length > 0 || response.rejectedLeaveList.length > 0) {
                    var compTrackIdList = '';
                    if (status == "Approved") {
                        compTrackIdList = response.approvedLeaveList.toString();
                    }
                    if (status == "Rejected") {
                        compTrackIdList = response.rejectedLeaveList.toString();
                    }
                    $.post(base_url+"index.php/Notify/leave_mail_status",{C : compTrackIdList, param:"C", po:""}, function(data, textStatus) {
                        if (textStatus == 'success') {
                            window.location.reload();
                        }
                    });
                } else {
                	if (response.message != '') {
                		alert(response.message);
                	}
                    window.location.reload();
                }
            }
        })
    }
	
</script>
  </body>
</html>