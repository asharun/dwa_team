<?php
$user_id=$this->session->userdata('user_id');
  $file_nm='Floor_View';
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	
$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);
if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>"  type="text/css">
	
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>
			<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			<div class="col-md-10">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>					
					</div>
			</fieldset>				
			</div>			
			</div>
			
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Floor_View">Weekly Summary</button>
						<button class='stab_stages_2' ch="FV_Wow">Productivity WoW</button>	
						<button class='stab_stages_2' ch="FV_Spread">Trends</button>
						<button class='stab_stages_2' ch="FV_Feed">Scatter</button>							
						<button class='stab_stages_2' ch="FV_Summary">Descriptives</button>
						<button class='stab_stages_2 ' ch="FV_Drilldown">Pies</button>					
						<button class='stab_stages_2 stab_dis_selec' ch="FV_Member_Info">Weekly Report</button>
						<button class='stab_stages_2' ch="FV_Contributor">Projects & Members</button>						
					</div>
				</div>															
					<div class='row hid'>	
						<div class='col-md-12'>	
						<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));
							$day_fm=date('d-M-Y',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));
							
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));
							
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));
							
							$date_start = date('Y-m-d', strtotime($day.' -'.($day_w-1).' days'));
							$date_end=date('Y-m-d', strtotime($day.' +'.(7-$day_w).' days'));
							
					?>
			
			<div class='row third-row head'>
												
								<?php
								echo "
								<div class='col-md-3'>
								<span>
								<label class='l_font_fix_3'>Choose Date: </label>	
									<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
									</span>
								</div>
								<div class='col-md-6 cur-month text-center'><span>Week (".$week_st." - ".$week_end.")</span>
								</div>	";
								?>
							</div>
							
									<div class='row row_style_1' id='c_find'>	
											<div class='col-md-12'>					
														<a class='arr2 pull-left' ch='FV_Member_Info' w_val="<?= $w_prev ?>">&laquo; Prev</a>						
														<a class='arr2 pull-right' ch='FV_Member_Info' w_val="<?= $w_nxt ?>">Next&raquo;</a>					
											</div>														
									</div>
						
											<div class='row row_style_1'>
														
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Level:</label>
																<select id='sel_2345' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option data-hidden="true"></option>';
															if(!$level_opt || $level_opt>=3)
															{
																$level_opt=0;
															}
															
															$lel_val=array("Activity","Projects","Employee");
															
															foreach ($lel_val as $key=>$value)
															{
																$sel='';
																	if($level_opt==$key)
																	{
																		$sel='selected';																		
																	}
																echo "<option value='".$key."' ".$sel.">".$value."</option>";
															}
															
																?>
															</select>
															</div>															
															<div class='col-md-4'>
																<label class='l_font_fix_3'>Choose Dept:</label>
																<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
																<?php
																foreach ($dept_val as $row)
																{
																	$sel='';
																		if($dept_opt==$row['dept_id'])
																		{
																			$sel='selected';																		
																		}
																	echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
																}
																?>
																</select>
															</div>		
															<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Project:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option data-hidden="true"></option>';
															
															if($pro_sel_val)
															{
																echo "<option value='0' >All Projects</option>";
															}else{
																echo "<option value='0' selected >All Projects</option>";
															}
															if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($pro_sel_val==$row2['sel_1_id'])
																	{
																		$sel='selected';
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>
										</div>
								<hr class="st_hr2">
								<div id="toolbar" > 
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="all">Export All</option>
									<option value="selected">Export Selected</option>
							</select>
					</div>
							<table class="display table table-bordered table-responsive" data-show-footer="true" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table" data-search-time-out=500 data-pagination="true" data-search="true">
							<thead>
								<tr>
								<?php
								  echo '<th data-class="l_font_fix_3" data-formatter="runningFormatter">#</th>';
								  
								   //if($level_opt==1 || $level_opt==0)
								  {
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="project_name">Project</th>';
								  }
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="full_name">Member Name</th>';
								  if($level_opt==0)
								  {
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="val_name">'.$lel_val[$level_opt].'</th>';												  
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="wu_name">Work Units</th>';
								  }											  										  
								 							  
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="proj_xp" data-footer-formatter="totalProjectedXp">Projected XPs</th>';
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="act_xp" data-footer-formatter="totalConfirmedXp">Confirmed XPs</th>';
								 if($level_opt==1)
								  { 
							echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="tot_proj_xp" data-footer-formatter="totalProjectedXp">Total Projected XPs</th>';
							 echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="tot_act_xp" data-footer-formatter="totalConfirmedXp">Total Confirmed XPs</th>';
								  }
								if($level_opt==0)
								  {
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="w_done">Qty Done</th>';
								  }
								
								if($level_opt==1||$level_opt==2)
								{
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="in_time">Median In Time</th>';
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="esp_wrk_hrs">Avg Office Work Hours</th>';
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="sapience_on">Avg Sapience On</th>';
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="sapience_off">Avg Sapience Off</th>';
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="attendance_cnt">Attendance</th>';
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="cu">Projected % CU</th>';
								  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="ccu">Confirmed % CU</th>';
								  
								}	
						if($level_opt==1)
							{
							 echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="grade">Grade</th>';
							  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="grade_remarks">Remarks</th>';
							  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="f_full_name">Feedback By</th>';
							}								
								
								  ?>
								</tr>
							</thead>
						</table>								
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script>

	function runningFormatter(value, row, index) {
index++; {
return index;
}
}

	$('#toolbar').find('select').change(function () {
            $('#table').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });
	
	
	$("body").on("change","#sel_2345",function(){
		if($(this).val())
		{
			var ele=Date.parse($(".ch_dt").val()); 
			var date_v=moment(ele).format("YYYY-MM-DD");
			var dept=$("body").find("#sel_dept_1").val();
			
						var str='';
						str=str+"&lvl="+$(this).val();
						
						if(dept)			
						{
							str=str+"&dept="+dept;
						}	
						var pro=$("body").find("#sel_1234").val();
					if(pro)			
						{
							str=str+"&pro="+pro;
						}						
			window.location = base_url+"index.php/User/load_view_f?a=FV_Member_Info&date_view="+date_v+str;
		}
	});
	
	$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var ele=Date.parse($(".ch_dt").val()); 
			var date_v=moment(ele).format("YYYY-MM-DD");
			var dept=$("body").find("#sel_dept_1").val();
						var str='';
						var lvl=$("body").find("#sel_2345").val();
						if(lvl)
						{
						str=str+"&lvl="+lvl;
						}
						var dept=$("body").find("#sel_dept_1").val();
						
						str=str+"&pro="+$(this).val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}						
						window.location = base_url+"index.php/User/load_view_f?a=FV_Member_Info&date_view="+date_v+str;	
		}
	});
	
	function totalProjectedXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
            }

 function totalConfirmedXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
            }
		var ele=Date.parse($(".ch_dt").val()); 
	var date_v=moment(ele).format("YYYY-MM-DD");
	var pr='';
	var lev=0;
	var pro=0;
	
	if($("#sel_2345").find("option:selected").val())
	{
		lev=$("#sel_2345").find("option:selected").val();
	}
	if($("#sel_1234").find("option:selected").val())
	{
		pro=$("#sel_1234").find("option:selected").val();
	}
	$("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						var lvl=$("body").find("#sel_2345").val();
						var dept=$("body").find("#sel_dept_1").val();
						var str='';
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
					var pro=$("body").find("#sel_1234").val();
					if(pro)			
						{
							str=str+"&pro="+pro;
						}						
							window.location = base_url+"index.php/User/load_view_f?a=FV_Member_Info&date_view="+date_v+str;
						}
					});	
	});
	
	
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
						var ele=Date.parse($("body").find(".ch_dt").val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						var str='';
						var lvl=$("body").find("#sel_2345").val();
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
						var dept=$(this).val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						str=str+"&pro=0";
						window.location = base_url+"index.php/User/load_view_f?a=FV_Member_Info&date_view="+date_v+str;			
		}	
	});
	

	
	$.ajax({
       url: base_url+"index.php/User/load_table_boots?a=FV_Member_Info&date_view="+date_v+"&lvl="+lev+"&dept="+dep_opt+"&pro="+pro,
       dataType: 'json',
       success: function(response) {
           $('#table').bootstrapTable({
              data: response,
			  stickyHeader: true
           });
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });
	
	</script>
  </body>
</html>