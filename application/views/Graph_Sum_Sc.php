<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm="Team_Member_Info";

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
} 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Team_Member_Info">Report Card</button>						
						<button class='stab_stages_2' ch="Graph_Drilldown">Pies</button>
						<button class='stab_stages_2' ch="Team_Split_Graph">Swiss Rolls</button>
						<button class='stab_stages_2' ch="Graph_Trends">Trends</button>					
						<button class='stab_stages_2 stab_dis_selec' ch="Graph_Sum_Sc">Stats</button>
						<button class='stab_stages_2' ch="Employee_Info">Employee Records</button>				
						<button class='stab_stages_2' ch='Status_Report'>Activity Records</button>						
						<button class='stab_stages_2' ch="Employee_Leave">Leave Tracker</button>
						<button class='stab_stages_2' ch="WPR_Feed">Scatter</button>
					</div>
				</div>
					<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));
							
							if($day_w==0)
							{
								$day_w=7;								
							}
							$day_fm=date('d-M-Y',strtotime($day));
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));							
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));							
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));
							
							
					?>
					<div class='row hid'>	
						<div class='col-md-12'>	
			<div class='row row_style_1 text-center'>	
							<div class='col-md-12'>					
										<button class='stab_stages_2 in_stages' ch="Graph_Summary">Contributors</button> || 
										<button class='stab_stages_2 stab_dis_selec' ch="Graph_Sum_Sc">Members</button>
							</div>														
					</div>
					<hr class="st_hr2">
			
							<?php include_once 'WprDateFilter.php'; ?>
									<!-- <div class='row row_style_1' id='c_find'>	
											<div class='col-md-12'>					
														<a class='arr2 pull-left' ch='Graph_Sum_Sc' w_val="<?= $weekPrv ?>">&laquo; Prev</a>						
														<a class='arr2 pull-right' ch='Graph_Sum_Sc' w_val="<?= $weekNxt ?>">Next&raquo;</a>					
											</div>														
									</div> -->
									<div class='row row_style_1  scroll-head'>
														<div class='col-md-4'>
																<label class='l_font_fix_3'>Choose Dept:</label>
																<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
																<?php
																foreach ($dept_val as $row)
																{
																	$sel='';
																		if($dept_opt==$row['dept_id'])
																		{
																			$sel='selected';																		
																		}
																	echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
																}
																?>
																</select>
															</div>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Project:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															
															echo '<option data-hidden="true"></option>';
															if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($pro_sel_val==$row2['sel_1_id'])
																	{
																		$sel='selected';
																	$titl1=$row2['sel_1_name'];															
																	$titl=$row2['sel_1_name']." - Share of Top and Bottom Quartiles (".$week_st." to ".$week_end.")";																		
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>										
										</div>
			<?php
			if($avg)
				{
					echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Avg XP per member:- </label> ";
							echo "<span>".round($avg,2)."</span>";
						echo "</div>";
					echo "</div>";
					
				}
			
					echo "<div class='row row_style_1'>";
							if($tm_data_max)
							{
							echo "<div class='col-md-6'>
								<table class='table table-bordered' style='table-layout:fixed;'>
								<th class='l_font_fix_3'>Highest Scorers</th>
								<th class='l_font_fix_3' style='width:25%;'>Highest Score</th>
								<th class='l_font_fix_3' style='width:25%;'>Attendance</th>";
								
							foreach($tm_data_max AS $key1=>$row1)
							{ 
							  	echo "<tr>";
										echo "<td>".$key1."</td>";
										echo "<td>".$row1['e_xp']." XP</td>";
										echo "<td>".$row1['a_c']." </td>";
								echo "</tr>";
							}
							echo "</table>
								</div>";
							}
							if($tm_data_min)
							{
								echo "<div class='col-md-6'>
								<table class='table table-bordered' style='table-layout:fixed;'>
								<th class='l_font_fix_3'>Lowest Scorers</th>
								<th  class='l_font_fix_3' style='width:25%;'>Lowest Score</th>
								<th class='l_font_fix_3' style='width:25%;'>Attendance</th>";
							foreach($tm_data_min AS $key1=>$row1)
							{ 
							  	echo "<tr>";
										echo "<td>".$key1."</td>";
										echo "<td>".$row1['e_xp']." XP</td>";
										echo "<td>".$row1['a_c']." </td>";
								echo "</tr>";
							}
							echo "</table>
								</div>";
							}
						echo "</div>";	
						
			if($Quart1 || $Quart3)
				{
					echo "<div class='row row_style_1'>
							<div class='col-md-6'>
								<table class='table table-bordered' style='table-layout:fixed;'>
								<th class='l_font_fix_3'>Bottom Quartile Scorers</th>
								<th  class='l_font_fix_3' style='width:25%;'>XP</th>
								<th  class='l_font_fix_3' style='width:25%;'>Attendance</th>";
							foreach($Quart1 AS $key1=>$val1)
							{ 
									echo "<tr>";
										echo "<td>".$key1."</td>";
										echo "<td>".$val1['e_xp']." XP</td>";
										echo "<td>".$val1['a_c']."</td>";
									echo "</tr>";
							}
							echo "</table>
								</div>
								<div class='col-md-6'>
								<table class='table table-bordered' style='table-layout:fixed;'>
								<th class='l_font_fix_3'>Top Quartile Scorers</th>
								<th  class='l_font_fix_3' style='width:25%;'>XP</th>
								<th  class='l_font_fix_3' style='width:25%;'>Attendance</th>";
							foreach($Quart3 AS $key3=>$val3)
							{ 
							 echo "<tr>";
										echo "<td>".$key3."</td>";
										echo "<td>".$val3['e_xp']." XP</td>";
										echo "<td>".$val3['a_c']."</td>";
							 echo "</tr>";
							}
							echo "</table>
								</div>
						</div>";
						}
						echo "<hr class='st_hr3'>";
			echo "<div class='row row_style_2'><div class='col-md-12'><div class='no_dt hide'>No Records!</div></div></div>";
						echo "<div class='data'>";
						//print_r($proj_xp);
						if($Q1per || $Q3per)
							{
							$tm_data1=1;
									echo "<h5 class='hide params1' titl='".$titl."' act_id='".$titl1."' user_id='Share of Bottom Quartile' >".$Q1per."</h5>";
									echo "<h5 class='hide params2' user_id='Share of Top Quartile' >".$Q3per."</h5>";
									
							}else
						{
							$tm_data1=0;
							$tm_data=null;
						}
								echo "</div>";
							echo "<div class='canvas_r text-center'>";
							echo '<div style="width: 100%; height: 400px;" id="chartdiv"></div>';
		
			?>
			</div>
			</div>
			        
			</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var tt_data = '<?php echo ($tm_data1) ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/serial.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>
window.useDateFilters = '<?php echo $useDateFilters ?>';
	function redirectUser(pro_v,dept,start_date,end_date){
		var str = "";
		if(pro_v)
		{
			str=str+"&pro="+pro_v;
		}
		if(dept)
		{
			str=str+"&dept="+dept;
		}
		if(start_date){
			str+='&start_date='+start_date;
		}
		if(end_date){
			str+='&end_date='+end_date;
		}	
		var redirectUrl = base_url+"index.php/User/load_view_f?a=Graph_Sum_Sc"+str;
		window.location = redirectUrl;
	}
	
	$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var pro_v = $(this).val();
			var dept=$("body").find("#sel_dept_1").val();
			var start_date = moment($("#st_dt").val()).format("YYYY-MM-DD");
			var end_date =moment($("#e_dt").val()).format("YYYY-MM-DD");					
			redirectUser(pro_v,dept,start_date,end_date);
		}
	});
	
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Project",dept:$(this).val()},
				success: function(response){				
					$('#sel_1234').html(response).selectpicker('refresh');
				}
			});
		}	
	});
	
    
	$(".ch_dt").datepicker({
		format: 'dd-M-yyyy',							
		yearRange: "-1:+1",
		weekStart:1
	});

	$(document).on('click', '.btn_dt_filter', function(event) {
		event.preventDefault();
		/* Act on the event */
		var pro_v=$("body").find("#sel_1234").val();
		var dept=$("body").find("#sel_dept_1").val();
		var start_date = moment($("#st_dt").val()).format("YYYY-MM-DD");
		var end_date =moment($("#e_dt").val()).format("YYYY-MM-DD");
		redirectUser(pro_v,dept,start_date,end_date);
	});
	
	
	
     var myArray =[];
	  var p =[];
		item = {};
		item ["act"] = $(".params1").attr('act_id');
		item [$(".params1").attr('user_id')] =$(".params1").text();
		item [$(".params2").attr('user_id')] =$(".params2").text();
myArray.push(item);

var title_head1=$(".params1").attr('titl');
	ke=[];
	ke[0]='Share of Bottom Quartile';
	ke[1]='Share of Top Quartile';
	//console.log(myArray);
    if(tt_data!=0)
  {
	 var chart;
      var chartData = myArray;
            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData;
                chart.categoryField = "act";
					chart.columnWidth=0.1;
					chart.addTitle(title_head1);
                chart.autoMargins = true;
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridAlpha = 0;
                categoryAxis.axisAlpha = 0;
				categoryAxis.stackByValue = true;
                categoryAxis.gridPosition = "start";
				categoryAxis.autoGridCount = true;
				categoryAxis.gridCount = 10;
				categoryAxis.autoWrap=true;
				
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.stackType = "regular";
                valueAxis.maximum=100;
				
				valueAxis.minimum=0;
				valueAxis.axisThickness=0;
				valueAxis.precision=2;
                valueAxis.labelsEnabled = true;
				valueAxis.showFirstLabel = true;
				
                chart.addValueAxis(valueAxis);

              var graph = new AmCharts.AmGraph();
                graph.title = ke[0];
                graph.labelText = "[[value]]%";
                graph.valueField = ke[0];
                graph.type = "column";
                graph.lineAlpha = 0;
				graph.fillColors = '#F10101';
				graph.balloonText = "[[category]]<br>[[title]] - <b>[[value]] %</b>";
				graph.color = 'white';
		         graph.fillAlphas = 1;
                chart.addGraph(graph);
				
					var graph = new AmCharts.AmGraph();
                graph.title = ke[1];
                graph.labelText = "[[value]]%";
                graph.valueField = ke[1];
                graph.type = "column";
				graph.balloonText = "[[category]]<br>[[title]] - <b>[[value]] %</b>";
                graph.lineAlpha = 0;
				graph.fillColors = '#03A9F4';
				graph.color = 'white';
		         graph.fillAlphas = 1;
                chart.addGraph(graph);
				
		        var legend = new AmCharts.AmLegend();
                legend.borderAlpha = 0.5;
                legend.horizontalGap = 10;
                legend.autoMargins = true;
                legend.marginLeft = 20;
                legend.marginRight = 20;
				legend.position='right';
                chart.addLegend(legend);
				chart.responsive = {
					  "enabled": true,
					  "rules": [
                                {
                                    "maxWidth": 400,
                                    "overrides": {
                                        "legend": {
                                            "enabled": true,
											"position":'bottom'
                                        }
                                    }
                                }]
					};
					
				chart.export = {
					  "enabled": true,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 1
						  },
					  "exportTitles" :true,
					   "legend": {
									 "position": "right"
									 // "height": $('#legenddiv')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart.write("chartdiv");
            });
			}else{
	  $("body").find(".no_dt").removeClass('hide');
  } 
  
  $(window.document).scroll(function() {
        var windowtop = $(window).scrollTop();

        $("body").find(".scroll-head").each(function(i, e) {
            $(this).removeClass("divposition");
            if (windowtop > $(e).offset().top) {
                $(e).addClass("divposition");
                return;
            }
        })
    });
	</script>
  </body>
</html>