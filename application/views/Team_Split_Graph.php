<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm="Team_Member_Info";
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
} 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Team_Member_Info">Report Card</button>						
						<button class='stab_stages_2' ch="Graph_Drilldown">Pies</button>
						<button class='stab_stages_2 stab_dis_selec' ch="Team_Split_Graph">Swiss Rolls</button>
						<button class='stab_stages_2' ch="Graph_Trends">Trends</button>					
						<button class='stab_stages_2' ch="Graph_Sum_Sc">Stats</button>
						<button class='stab_stages_2' ch="Employee_Info">Employee Records</button>				
						<button class='stab_stages_2' ch='Status_Report'>Activity Records</button>						
						<button class='stab_stages_2' ch="Employee_Leave">Leave Tracker</button>
						<button class='stab_stages_2' ch="WPR_Feed">Scatter</button>
					</div>
				</div>
		
					<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));
							$day_fm=date('d-M-Y',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));							
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));							
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));
							
							
					?>
					<div class='row hid'>	
						<div class='col-md-12'>	
			
			<div class='row row_style_1 text-center'>	
							<div class='col-md-12'>					
										<button class='stab_stages_2 in_stages' ch="Team_Graphs">Activity-wise</button> || 
										<button class='stab_stages_2 stab_dis_selec ' ch="Team_Split_Graph">Member-wise</button>
							</div>														
					</div>
					
					<hr class="st_hr2">
											
									<?php include_once 'WprDateFilter.php'; ?>
									<div class='row row_style_1'>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Level:</label>
																<select id='sel_2345' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option data-hidden="true"></option>';
															if(!isset($level_opt))
															{
																$level_opt=1;
															}
															$lel_val=array("Activity","Projects","Category","Element","Action","Trade");
															foreach ($lel_val as $key=>$value)
															{
																$sel='';
																	if($level_opt==$key)
																	{
																		$sel='selected';																		
																	}
																echo "<option value='".$key."' ".$sel.">".$value."</option>";
															}
															
																?>
															</select>
															</div>
															<div class='col-md-4'>
																<label class='l_font_fix_3'>Choose Dept:</label>
																<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
																<?php
																foreach ($dept_val as $row)
																{
																	$sel='';
																		if($dept_opt==$row['dept_id'])
																		{
																			$sel='selected';																		
																		}
																	echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
																}
																?>
																</select>
															</div>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Project:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option data-hidden="true"></option>';
															if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($pro_sel_val==$row2['sel_1_id'])
																	{
																		$sel='selected';
																		$titl=$row2['sel_1_name']." - Member-wise Break-up (".$week_st." to ".$week_end.")";
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>										
										</div>
										
										<?php
											if($tm_data) { 
											$C=array_column($tm_data,'full_name');
											}				 
														
											?>
											<div class='row row_style_1'>
												<div class='col-md-4'>
													<?php if($tm_data) {  ?>
													<label class='l_font_fix_3' style="width:100%">Choose Members:</label>
													<select id='member-selector' class='form-control member-selector'
													 multiple="multiple">
													<?php
													foreach (array_unique($C) as $tm)
														{
														  
														echo "<option value='" .$tm.  "' ".$sel.">"
														.$tm. "</option>";
														}
													
													?>
													</select>
												<?php 	}  ?>
												</div>	
											</div>
			<?php
			echo "<div class='row row_style_2'><div class='col-md-12'><div class='no_dt hide'>No Records!</div></div></div>";
			
						echo "<div class='data'>";
						//print_r($proj_xp);
						if($tm_data)
						{
							foreach($tm_data AS $row1)
										{									
									if($level_opt==0)
								{
									$tsdff=explode('_', $row1['activity_name'], 2);
									$f_repl=$tsdff[1];
								}else{
									$f_repl=$row1['activity_name'];
								}	
									echo "<h5 class='hide params1' user_id='".$row1['full_name']."' act_id='".$f_repl."'>".$row1['equiv_xp']."</h5>";
										}
						}else{
							$tm_data=null;
						}
								echo "</div>";
								
							echo "<div class='canvas_r text-center'>";
							echo '<div style="width: 100%; height: 400px;" id="chartdiv"></div>';
		
			?>
			<div id="legenddiv" style="overflow: scroll; position: relative;text-align: left;min-height:70px;width: auto;max-height:150px!important;"></div>
			
			</div>
			</div>
			        
			</div>
			</div>
		</div>
	</div>

</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-multiselect.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>	
	<script>var title_head = '<?php echo $titl ?>';</script>
	<script>var tt_data = '<?php echo $tm_data ? sizeof($tm_data) : 0 ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/serial.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>
    window.useDateFilters = '<?php echo $useDateFilters ?>';
    function redirectUser(pro_v,dept,lvl,start_date,end_date){
		var str = "";
		if(pro_v)
		{
			str=str+"&pro="+pro_v;
		}
		if(dept)
		{
			str=str+"&dept="+dept;
		}
		if(lvl)
		{
			str=str+"&lvl="+lvl;
		}
		if(start_date){
			str+='&start_date='+start_date;
		}
		if(end_date){
			str+='&end_date='+end_date;
		}	
		var redirectUrl = base_url+"index.php/User/load_view_f?a=Team_Split_Graph"+str;
		window.location = redirectUrl;
	}

	$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var pro_v = $(this).val();
			var lvl=$("body").find("#sel_2345").val();
			var dept=$("body").find("#sel_dept_1").val();
			var start_date = moment($("#st_dt").val()).format("YYYY-MM-DD");
			var end_date =moment($("#e_dt").val()).format("YYYY-MM-DD");					
			redirectUser(pro_v,dept,lvl,start_date,end_date);
		}
	});
    
	$("body").on("change","#sel_2345",function(){
		if($(this).val())
		{
			var pro_v=$("body").find("#sel_1234").val();
			var dept=$("body").find("#sel_dept_1").val();
			var lvl = $(this).val();
			var start_date = moment($("#st_dt").val()).format("YYYY-MM-DD");
			var end_date =moment($("#e_dt").val()).format("YYYY-MM-DD");
			redirectUser(pro_v,dept,lvl,start_date,end_date);
		}
	});
	
	$(".ch_dt").datepicker({
		format: 'dd-M-yyyy',							
		yearRange: "-1:+1",
		weekStart:1
	});

	$(document).on('click', '.btn_dt_filter', function(event) {
		event.preventDefault();
		/* Act on the event */
		var pro_v=$("body").find("#sel_1234").val();
		var lvl=$("body").find("#sel_2345").val();
		var dept=$("body").find("#sel_dept_1").val();
		var start_date = moment($("#st_dt").val()).format("YYYY-MM-DD");
		var end_date =moment($("#e_dt").val()).format("YYYY-MM-DD");
		redirectUser(pro_v,dept,lvl,start_date,end_date);
	});
	
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Project",dept:$(this).val()},
				success: function(response){				
					$('#sel_1234').html(response).selectpicker('refresh');
				}
			});
		}	
	});
	
     var myArray =[];
	 var a_id = [];
   
var p=[];
    $(".params1").each(function(index) {
		item = {};
		item ["act"] =  $(this).attr('act_id');
        item ["work"] = $(this).text();
		item ["user"] = $(this).attr('user_id');
         myArray.push(item);
		 p[index]=$(this).attr('act_id');
    });

var group_to_values = myArray.reduce(function (obj, item) {
    obj[item.user] = obj[item.user] || [];
	item2 = {};
	item2 ["wh"] =item.work;
		item2 ["act"] = item.act;
    obj[item.user].push(item2);
    return obj;
}, {});

var groups = Object.keys(group_to_values).map(function (key) {
    return {user: key, work: group_to_values[key]};
});
  
  var k_array=[];

  $.each( groups, function( key, value ) {
  item23 = {};
		item23 ["user"] = value.user.toString();
 
	$.each( value.work, function( key1, value1 ) {
		item23 [value1.act.toString()] = value1.wh.toString();
   
	});
	k_array.push(item23);
});
   
   
function removeDups(arr) {
    var result = [], map = {};
    for (var i = 0; i < arr.length; i++) {
        if (!map[arr[i]]) {
            result.push(arr[i]);
            map[arr[i]] = true;
        }
    }
    return(result);
}

ke = removeDups(p);
  //console.log(k_array);
  //console.log(p);
  
//  ke = jQuery.unique(p);


  
  if(tt_data!=0)
  {
	/// console.log(ke);
	 var chart;

            var chartData = k_array;

            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData;
                chart.categoryField = "user";

                // sometimes we need to set margins manually
                // autoMargins should be set to false in order chart to use custom margin values
                chart.autoMargins = true;
                // chart.marginLeft = 0;
                // chart.marginRight = 0;
                // chart.marginTop = 30;
                // chart.marginBottom = 40;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridAlpha = 0;
				
                categoryAxis.axisAlpha = 0;
                categoryAxis.gridPosition = "start";
				categoryAxis.autoGridCount = false;
				categoryAxis.gridCount = chartData.length;
				//categoryAxis.labelRotation = "0";
				//categoryAxis.ignoreAxisWidth =true;
				categoryAxis.autoWrap=true;
				categoryAxis.labelFunction= function(valueText, serialDataItem, categoryAxis) {
      if (valueText.length > 20)
        return valueText.substring(0, 20) + '...';
      else
        return valueText;
    }

                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.stackType = "100%"; // this line makes the chart 100% stacked
                 valueAxis.title = "%";
                // valueAxis.axisAlpha = 0;
				
                valueAxis.labelsEnabled = true;
                chart.addValueAxis(valueAxis);

                // GRAPHS
                // first graph
				
				$.each( ke, function( key2, value2 ) {
                var graph = new AmCharts.AmGraph();
                graph.title = value2;
                graph.labelText = "[[percents]]%";
                graph.balloonText = "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> XP ([[percents]]%)</span>";
                graph.valueField = value2;
                graph.type = "column";
                graph.lineAlpha = 0;
				//graph.labelRotation= 90;
                graph.fillAlphas = 0.8;
                //graph.lineColor = "#C72C95";
                chart.addGraph(graph);
				});
				
				
                // LEGEND
                var legend = new AmCharts.AmLegend();
                legend.borderAlpha = 0.2;
                legend.horizontalGap = 10;
                legend.autoMargins = true;
                legend.marginLeft = 50;
                legend.marginRight = 50;
                //chart.addLegend(legend);
				legend.labelText="[[title]]";
				
				legend.periodValueText ="[[value.sum]] XP";
				
				chart.addLegend(legend, "legenddiv");
			chart.addTitle(title_head);
				chart.responsive = {
					  "enabled": true
					};
					
					chart.write("chartdiv");
					
					//console.log($('#legenddiv')[0].scrollHeight);
					
					//chart.write("chartdiv");
					chart.export = {
					  "enabled": true,
					  "fileName": title_head,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 1
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart.initHC = false;
					chart.validateNow();
					
                // WRITE
                
				// chart.validateNow();
            });

           
  }else{
	  $("body").find(".no_dt").removeClass('hide');
  }       
           
		   var arrayOfObjects = k_array // all that should remain   
    $("body").on("change","#member-selector",function(){
		if($(this).val())
		{ var mem=$(this).val();
		const toDelete = new Set(mem);
		const newArray = arrayOfObjects.filter(obj => toDelete.has(obj.user));
		chart.dataProvider=newArray;
		    chart.validateData();
		}
	});   
	
	 $('.member-selector').multiselect({
                enableFiltering: true,
                includeSelectAllOption: true,
                selectAllJustVisible: false,
                numberDisplayed: 1
            });
	
	</script>
  </body>
</html>