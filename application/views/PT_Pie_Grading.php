<?php
$user_id=$this->session->userdata('user_id');
   $file_nm='Reports';
$a_right1=str_replace(' ','_',$this->session->userdata('access'));
$access_str1=explode("|",$a_right1);

$a_right=$this->session->userdata('access');
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

 ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-multiselect.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/dc.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	<style>
	.multiselect-container {
        width: 100% !important;
		max-height: 400px;
    overflow: auto;
    }

	.multiselect.dropdown-toggle,.bt_cont_pt{
		width:100%;
	}


	</style>
  </head>
  <body>
  <?php
  $this->load->view("Header.php");
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>
				<div class="col-md-10 c_row">

				<div class='row hid1'>
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Reports">Overview</button>
						<button class='stab_stages_2' ch="PT_Report_Card">Report Card</button>
						<button class='stab_stages_2 stab_dis_selec' ch="PT_Pies">Pies</button>
						<!--button class='stab_stages_2' ch="PT_Trends">Trends</button>
						<button class='stab_stages_2' ch="PT_Stats">Stats</button-->
						<button class='stab_stages_2' ch="PT_Feedback">Feedback</button>
					</div>
				</div>
					<?php
							$st_dt= date('d-M-y', strtotime($s_dt));
							$en_dt= date('d-M-y', strtotime($e_dt));
							$titl3= "(".$st_dt." to ".$en_dt.")";
							$day_fm1=date('d-M-Y',strtotime($s_dt));
							$day_fm2=date('d-M-Y',strtotime($e_dt));
						?>
					<div class='row hid'>
						<div class='col-md-12'>
								<div class='row row_style_1 text-center'>
										<div class='col-md-12'>
										<button class='stab_stages_2 in_stages' ch="PT_Pies">MiD Pies</button> ||
										<button class='stab_stages_2 stab_dis_selec' ch="PT_Pie_Grading">Grading Pies</button> ||
										<button class='stab_stages_2 in_stages' ch="PT_Pie_Reviews">Review Pies</button>
										</div>
								</div>
								<hr class="st_hr2">
						<!--div class='row row_style_1 text-center'>
							<div class='col-md-12'>
										<button class='stab_stages_2 in_stages' ch="Graph_Breakdown">Break Down</button> ||
										<button class='stab_stages_2 stab_dis_selec ' ch="Graph_Drilldown">Drill Down</button>
							</div>
					</.dc->
			<!--div class='row third-row head' style='margin-top: 10px;'>
												<?php
												// echo "<div class='col-md-3'>
														// <span>
															// <label class='l_font_fix_3'>Choose Date: </label>
															// <input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
														// </span>
													// </div>
										// <div class='col-md-6 cur-month text-center'><span>Break-up (".$week_st." to ".$week_end.")</span>
												// </div>	";
												// $titl1='';
												// $titl2='';
												// $titl3='';
												// $titl4='';

												?>
							</div-->

									<div class='row row_style_1'>
									<?php
							echo "<div class='col-md-4'>
												<label class='l_font_fix_3' style='width:100%'>Choose Employee</label>	";
												echo "<select id='sel_emp' class='form-control' title='Nothing Selected'  multiple='multiple' data-live-search=true'>";
												if($pro_sel_dta)
															{

															$rt_val=explode(",",$pro_sel_val);
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if(in_array($row2['sel_1_id'],$rt_val))
																	{
																		$sel='selected';
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}

												echo "</select>";
										echo "</div>";
								echo "<div class='col-md-2'>
											<span>
												<label class='l_font_fix_3'>Start Date</label>
												<input id='t_dtpicker' class='s_dt form-control date-picker' dt='".$s_dt."' value='".$day_fm1."' />
											</span>
										</div>
										<div class='col-md-2'>
											<span>
												<label class='l_font_fix_3'>End Date</label>
												<input id='t_dtpicker2' class='e_dt form-control date-picker' dt='".$e_dt."' value='".$day_fm2."' />
											</span>
										</div>";
										?>

										<div class='col-md-2'>
										<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>
										<button class='btn add_but gre_but change_rep' type='button'>Submit</button>
										</div>
								<div class="col-md-2">
															<label for="" class="1_font_fix_3">Status</label>
															<div class="form-group">
																<select name="status-filter" id="" class="form-control" onchange="statusFilter(this.value)">
																	<option value="projected">Projected</option>
																	<option value="confirmed">Confirmed</option>
																</select>
															</div>
								</div>
								<div class="col-md-2">
									<label class="l_font_fix_3 invisible" style="width:100%;">Update</label>
									<button class="btn add_but blue_but" id='sel_bu' type="button">Filter: Reset All</button>
								</div>
						</div>
			<?php

			echo "<div class='t1'>";
			echo "<div class='row row_style_1'>";
				echo "<div class='col-md-6'>";
					echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Grading-wise Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";
						echo "<div class='col-md-12'>";
					echo "<div class='canvas_r7 text-center row'>";
						//echo '<span class="display-qux"></span>';
						echo '<div id="chartdiv7" class="col-md-7 col-sm-6 col-xs-12"></div>';
						echo '<div id="legenddiv7" class="col-md-5 col-sm-6 col-xs-12"></div>';
						//echo '<div id="legenddiv1" style="overflow: scroll; position: relative;text-align: left;width: 100%;min-height:70px;max-height:70px!important;"></div>';
					echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "<div class='col-md-6'>";
					echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Project-wise Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";
						echo "<div class='col-md-12'>";
					echo "<div class='canvas_r6 text-center row'>";
						echo '<div id="chartdiv6" class="col-md-7 col-sm-6 col-xs-12"></div>';
						echo '<div id="legenddiv6" class="col-md-5 col-sm-6 col-xs-12"></div>';
					echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "</div>";
			echo "</div>";
			echo "<div class='row row_style_1'>";
				echo "<div class='col-md-6'>";
				echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Category-wise Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";
						echo "<div class='col-md-12'>";
					echo "<div class='canvas_r1 text-center row'>";
						echo '<div id="chartdiv1" class="col-md-7 col-sm-6 col-xs-12"></div>';
						echo '<div id="legenddiv1" class="col-md-5 col-sm-6 col-xs-12 col-sm-6 col-xs-12"></div>';
					echo "</div>";

				echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "<div class='col-md-6'>";
					echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Element-wise Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";
						echo "<div class='col-md-12'>";
					echo "<div class='canvas_r3 text-center row'>";
						echo '<div id="chartdiv3" class="col-md-6 col-sm-6 col-xs-12"></div>';
						echo '<div id="legenddiv3" class="col-md-6 col-sm-6 col-xs-12"></div>';
					echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "</div>";
			echo "</div>";


				echo "<div class='row row_style_1'>";
				echo "<div class='col-md-6'>";
				echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Trade-wise Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";
						echo "<div class='col-md-12'>";
					echo "<div class='canvas_r2 text-center row'>";
						echo '<div id="chartdiv2" class="col-md-7 col-sm-6 col-xs-12"></div>';
						echo '<div id="legenddiv2" class="col-md-5 col-sm-6 col-xs-12"></div>';
					echo "</div>";

				echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "<div class='col-md-6'>";
				echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Action-wise Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";
						echo "<div class='col-md-12'>";
					echo "<div class='canvas_r4 text-center row'>";
						echo '<div id="chartdiv4" class="col-md-7 col-sm-6 col-xs-12"></div>';
						echo '<div id="legenddiv4" class="col-md-5 col-sm-6 col-xs-12"></div>';
					echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "<div class='row row_style_1'>";
				echo "<div class='col-md-6'>";
				echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>MID-wise Break Up:-</label>";
						echo "</div>";
						echo "<div class='col-md-6 no_dt hide'>";
							echo "<label>No Records!</label>";
						echo "</div>";
						echo "<div class='col-md-12'>";
					echo "<div class='canvas_r5 text-center row'>";
						echo '<div id="chartdiv5" class="col-md-7 col-sm-6 col-xs-12"></div>';
						echo '<div id="legenddiv5" class="col-md-5 col-sm-6 col-xs-12"></div>';
					echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "</div>";
				echo "</div>";
				?>

			</div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-multiselect.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var arrayFromPHP = <?php echo json_encode($tm_data_all); ?>;</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
<script src="<?= getAssestsUrl() ?>js/d3.js" type="text/javascript"></script>
<script src="<?= getAssestsUrl() ?>js/crossfilter.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/dc.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/d3-scale-chromatic.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>

	$("body").on("change","#sel_emp",function(){
		if($(this).val())
		{
			$.ajax({
				url: base_url+"index.php/User/load_emp_string",
				type: 'post',
				data : {param:"Personal_Trends",pt_emp:$(this).val().toString()}
			});
		}
	});
	$("body").on("focus", ".s_dt",function(){
	$(this).datepicker({
					format: 'dd-M-yyyy',
					yearRange: "-1:+1",
					autoclose:true,
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val());
						var date_v=moment(ele).format("YYYY-MM-DD");
						$(this).attr('dt',date_v);
						}
					});
	});

	$("body").on("focus", ".e_dt",function(){

	$(this).datepicker({
					format: 'dd-M-yyyy',
					yearRange: "-1:+1",
					autoclose:true,
					startDate:$(".s_dt").val(),
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
							var ele=Date.parse($(this).val());
							var date_v=moment(ele).format("YYYY-MM-DD");
							$(this).attr('dt',date_v);
						}
					});
	});

	$("body").on("click", ".change_rep",function(){
		var sdt=$("body").find(".s_dt").attr('dt');
		var edt=$("body").find(".e_dt").attr('dt');
	//	var lvl=$("body").find("#sel_2345").val();
		if(sdt && edt && sdt<=edt)
		{
		window.location = base_url+"index.php/User/load_view_f?a=PT_Pie_Grading&s_dt="+sdt+"&e_dt="+edt;
		}
	});

	$("#sel_emp").multiselect({
		enableCaseInsensitiveFiltering: true,
		includeSelectAllOption: false,
		selectAllJustVisible: true,
		numberDisplayed: 1,
		includeSelectAllOption: true
	});

	var data = arrayFromPHP;

	if(arrayFromPHP)
	{
	var pieChart1 = dc.pieChart("#chartdiv1"),
		pieChart2 = dc.pieChart("#chartdiv2"),
		pieChart3 = dc.pieChart("#chartdiv3"),
		pieChart4 = dc.pieChart("#chartdiv4");
		pieChart5 = dc.pieChart("#chartdiv5");
		pieChart6 = dc.pieChart("#chartdiv6");
		pieChart7 = dc.pieChart("#chartdiv7");


var ndx = crossfilter(data),
    catDimension = ndx.dimension(function (d) {
        return d.cat_name;
    }),
    catGroup = catDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
	proDimension = ndx.dimension(function (d) {
        return d.project_name;
    }),
    proGroup = proDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
    taskDimension = ndx.dimension(function (d) {
        return d.task_name;
    }),
    taskGroup = taskDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
    subDimension = ndx.dimension(function (d) {
        return d.sub_name;
    }),
    subGroup = subDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
    jobDimension = ndx.dimension(function (d) {
        return d.job_name;
    }),
    jobGroup = jobDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
	graDimension = ndx.dimension(function (d) {
        return d.gra_name;
    }),
    graGroup = graDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    }),
	midDimension = ndx.dimension(function (d) {
        return d.milestone_id;
    }),
    midGroup = midDimension.group().reduceSum(function (d) {
        return d.equiv_xp;
    });

	function precise_round(num,decimals) {
   return Math.round(num*Math.pow(10, decimals)) / Math.pow(10, decimals);
}


	pieChart1.dimension(catDimension).group(catGroup).height(300).radius(100).cy(125).externalLabels(1);

	pieChart2.dimension(taskDimension).group(taskGroup).height(300).radius(100).cy(125).externalLabels(1);

	pieChart3.dimension(subDimension).group(subGroup).height(300).radius(100).cy(125).externalLabels(1);

	pieChart4.dimension(jobDimension).group(jobGroup).height(300).radius(100).cy(125).externalLabels(1);
	pieChart5.dimension(midDimension).group(midGroup).height(300).radius(100).cy(125).externalLabels(1);
	pieChart6.dimension(proDimension).group(proGroup).height(300).radius(100).cy(125).externalLabels(1);
	pieChart7.dimension(graDimension).group(graGroup).height(300).radius(100).cy(125).externalLabels(1);

	pieChart1.legend(dc.htmlLegend().legendText(function(d)
				{
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)';
				}
			).container('#legenddiv1').horizontal(true).highlightSelected(true));

	pieChart2.legend(dc.htmlLegend().legendText(function(d)
				{
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)';
				}
			).container('#legenddiv2').horizontal(true).highlightSelected(true));

		pieChart3.legend(dc.htmlLegend().legendText(function(d)
				{
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)';
				}
			).container('#legenddiv3').horizontal(true).highlightSelected(true));

		pieChart4.legend(dc.htmlLegend().legendText(function(d)
				{
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)';
				}
			).container('#legenddiv4').horizontal(true).highlightSelected(true));

			pieChart5.legend(dc.htmlLegend().legendText(function(d)
				{
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)';
				}
			).container('#legenddiv5').horizontal(true).highlightSelected(true));

			pieChart6.legend(dc.htmlLegend().legendText(function(d)
				{
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)';
				}
			).container('#legenddiv6').horizontal(true).highlightSelected(true));

			pieChart7.legend(dc.htmlLegend().legendText(function(d)
				{
				var groupTotal = d.chart.group().all().reduce(function(a, v){ return a + v.value; }, 0);
				return d.name + ': '+ precise_round(d.data,2)+" XP (" + precise_round((d.data/groupTotal)*100,2)+ '%)';
				}
			).container('#legenddiv7').horizontal(true).highlightSelected(true));

	$("body").find("#sel_bu").removeClass('hide');
 	dc.renderAll();

	}else{
		$("body").find(".no_dt").removeClass('hide');
		$("body").find("#sel_bu").addClass('hide');
		$("body").find(".canvas_r1").addClass('hide');
		$("body").find(".canvas_r2").addClass('hide');
		$("body").find(".canvas_r3").addClass('hide');
		$("body").find(".canvas_r4").addClass('hide');
		$("body").find(".canvas_r5").addClass('hide');
		$("body").find(".canvas_r6").addClass('hide');
		$("body").find(".canvas_r7").addClass('hide');
	}

	$("body").on("click","#sel_bu",function(){
			pieChart1.filter(null);
			pieChart2.filter(null);
			pieChart3.filter(null);
			pieChart4.filter(null);
			pieChart5.filter(null);
			pieChart6.filter(null);
			pieChart7.filter(null);
			dc.redrawAll();
	});
	</script>
  </body>
</html>