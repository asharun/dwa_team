<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm='Floor_View';

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			<div class="col-md-10">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>					
					</div>
			</fieldset>				
			</div>			
			</div>
			
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Floor_View">Weekly Summary</button>
						<button class='stab_stages_2 stab_dis_selec' ch="FV_Wow">Productivity WoW</button>	
						<button class='stab_stages_2 ' ch="FV_Spread">Trends</button>	
						<button class='stab_stages_2' ch="FV_Feed">Scatter</button>	
						<button class='stab_stages_2 ' ch="FV_Summary">Descriptives</button>
						<button class='stab_stages_2' ch="FV_Drilldown">Pies</button>					
						<button class='stab_stages_2' ch="FV_Member_Info">Weekly Report</button>
						<button class='stab_stages_2' ch="FV_Contributor">Projects & Members</button>
						
					</div>
				</div>
					<?php 	
						if(($s_dt) && ($e_dt))
						{
							$start_dt = date('d-M-Y', strtotime($s_dt));
							$end_dt = date('d-M-Y', strtotime($e_dt));
						}else
						{
							$day = date('Y-m-d H:i:s');
							$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}							
							$week_end = date('Y-m-d H:i:s', strtotime($day.' +'.(7-$day_w).' days'));
							
							$week_start = date('Y-m-d H:i:s', strtotime($week_end.' -1 month'));
							$week_st_w = date('w',strtotime($week_start));
							if($week_st_w==0)
							{
								$week_st_w=7;								
							}							
							$start_dt = date('d-M-Y', strtotime($week_start.' -'.($week_st_w-1).' days'));
							$end_dt = date('d-M-Y',strtotime($week_end));
							
						}	
					$cn_dt1 = date('d-M-Y',strtotime($cn_dt));						
					?>
					<div class='row hid'>	
						<div class='col-md-12'>	
							
			<div class='row third-row head'>
												
												<?php
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Start Date: </label>	
															<input id='t_dtpicker' class='s_dt date_fm date-picker' value='".$start_dt."' />
														</span>
													</div>
													<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>End Date: </label>	
															<input id='t_dtpicker' class='e_dt date_fm date-picker' value='".$end_dt."' />
														</span>
													</div>
													<div class='col-md-4'>
														<span>
															<label class='l_font_fix_3'>Target Fix Date: </label>	
															<input id='t_dtpicker' class='cn_dt date_fm date-picker' value='".$cn_dt1."' />
														</span>
													</div>
													<div class='col-md-2'>
														<span>
															<button class='btn add_but gre_but sub_wow' type='button' style='padding:3px 6px;'>Submit</button>
														</span>
													</div>
													</div>
													<div class='row row_style_1 third-row head'>
										<div class='col-md-12 cur-month text-center'><span>WoW Trendline (".$start_dt." to ".$end_dt.")</span>
												</div>	";
												$titl1='';
												$titl2='';
																								
												?>
							</div>								
									<div class='row row_style_1  scroll-head'>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Dept:</label>
															<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php
															foreach ($dept_val as $row)
															{
																$sel='';
																	if($dept_opt==$row['dept_id'])
																	{
																		$sel='selected';																		
																	}
																echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
															}
															?>
															</select>
														</div>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Show Quantifiable only:</label>
															<select id='sel_34' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															if($nq==0)
															{
																echo "<option selected value=0>No</option>";
																echo "<option value=1>Yes</option>";
															}
															else{
																echo "<option value=0>No</option>";
																echo "<option selected  value=1>Yes</option>";	
															}
															
															?>
															</select>
														</div>	
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose:</label>
															<select id='sel_345' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															if($o_n_c==0)
															{
																echo "<option selected value=0>Daily Output</option>";
																echo "<option value=1>Daily Efficiency</option>";
															}
															else{
																echo "<option value=0>Daily Output</option>";
																echo "<option selected value=1>Daily Efficiency</option>";	
															}
															
															?>
															</select>
														</div>	
										</div>
			<?php		
			echo "<div class='data'>";
						if($avg)
						{
							$startDateUnix=strtotime($start_dt);
							$endDateUnix=strtotime($end_dt);
							
								$currentDateUnix = $startDateUnix;

								$weekNumbers = array();
								while ($currentDateUnix < $endDateUnix) {
									//$y=date('d-Y', ($currentDateUnix));
									$weekNumbers[] = date('Y', (strtotime('+3 days', $currentDateUnix))).date('W', $currentDateUnix);
									$currentDateUnix = strtotime('+1 week', $currentDateUnix);
									
								}
							$titl2="WoW Trendline (".$start_dt." to ".$end_dt.")";																								
							foreach($weekNumbers AS $inde2)
										{											
										// $inde1="Week ".substr($inde2,4,2)." '".substr($inde2,2,2);	
										$week=substr($inde2,4,2);
										$ye=substr($inde2,0,4);
										$inde1 = "W ".$week." (".date("d/m", strtotime($ye."-W".$week."-1"))." to ".date("d/m", strtotime($ye."-W".$week."-7")).")"; 			

											if(array_key_exists($inde2,$avg))
											{
												foreach($avg[$inde2] AS $r_key=>$r_val)
												{
												echo "<h5 class='hide params2' v1='".$r_val."' v2='".$r_key."'>".$inde1."</h5>";
												}
											}
										}
						}else
						{
							$avg=null;
						}
								echo "</div>";
			echo "<div class='t2'>";
			echo "<div class='row row_style_1'>
							<div class='col-md-6'>
							<input class='ch_dt hide' value='".$date_view."' />
								<label class='l_font_fix_3'>Avg RP scored per Member:-</label>
								</div>
								<div class='col-md-2 no_dt1 hide'>
								<label>No Records!</label>
							</div>
						</div>";
							echo "<div class='canvas_r2 text-center'>";
							echo '<div style="width: auto;  height: 400px;!important;" id="chartdiv2"></div>';
							echo '<div id="legenddiv2" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>';	
			?>		
			</div>			        
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var title_head2 = '<?php echo $titl2 ?>';</script>
	<script>var tt_data2 = '<?php echo sizeof($avg) ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/serial.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>
 
 $("body").on("focus", ".s_dt",function(){
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1,
					daysOfWeekDisabled: "0,2,3,4,5,6"
			});
	});
	
	$("body").on("focus", ".cn_dt",function(){
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-2:+2",
					weekStart:1,
					daysOfWeekDisabled: "0,2,3,4,5,6"
			});
	});
	
	$("body").on("focus", ".e_dt",function(){		
		$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1,
					daysOfWeekDisabled: "1,2,3,4,5,6"
			});
	});
	
	$("body").on("click",".sub_wow",function(){
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");	
			var pro_v=$("body").find("#sel_34").val();
			
			var ele2=Date.parse($('.cn_dt').val()); 
			var cndt=moment(ele2).format("YYYY-MM-DD");
						var str='';
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						var pro_c=$("body").find("#sel_345").val();
						if(pro_c)
						{
							str=str+"&const="+pro_c;
						}
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
				if(ele && ele1 && (ele<ele1))
				{
					window.location = base_url+"index.php/User/load_view_f?a=FV_Wow&s_dt="+sdt+"&cn_dt="+cndt+"&e_dt="+edt+str;
				}else{
					alert("Check the start & end Dates !");
				}
	});
	   
   $("body").on("change","#sel_34",function(){
	if($(this).val())
	{
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");			
			var ele2=Date.parse($('.cn_dt').val()); 
			var cndt=moment(ele2).format("YYYY-MM-DD");
						var str='';
						var pro_v=$(this).val();			
						str=str+"&pro="+pro_v;
					if(ele && ele1 && (ele<ele1))
					{
						var pro_c=$("body").find("#sel_345").val();
						if(pro_c)
						{
							str=str+"&const="+pro_c;
						}
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						window.location = base_url+"index.php/User/load_view_f?a=FV_Wow&s_dt="+sdt+"&cn_dt="+cndt+"&e_dt="+edt+str;
					}else
					{
						alert("Check the start & end Dates !");
					}
	}
});


$("body").on("change","#sel_345",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");			
			var ele2=Date.parse($('.cn_dt').val()); 
			var cndt=moment(ele2).format("YYYY-MM-DD");
						var str='';
						var dept=$("body").find("#sel_dept_1").val();	
					str=str+"&dept="+dept;	
					if(ele && ele1 && (ele<ele1))
					{
						var pro_v=$("body").find("#sel_34").val();
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						var pro_c=$(this).val();
						if(pro_c)
						{
							str=str+"&const="+pro_c;
						}
						window.location = base_url+"index.php/User/load_view_f?a=FV_Wow&s_dt="+sdt+"&cn_dt="+cndt+"&e_dt="+edt+str;
					}else
					{
						alert("Check the start & end Dates !");
					}
		}	
	});

$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");			
			var ele2=Date.parse($('.cn_dt').val()); 
			var cndt=moment(ele2).format("YYYY-MM-DD");
						var str='';
						var dept=$(this).val();	
					str=str+"&dept="+dept;	
					if(ele && ele1 && (ele<ele1))
					{
						var pro_v=$("body").find("#sel_34").val();
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						var pro_c=$("body").find("#sel_345").val();
						if(pro_c)
						{
							str=str+"&const="+pro_c;
						}
						window.location = base_url+"index.php/User/load_view_f?a=FV_Wow&s_dt="+sdt+"&cn_dt="+cndt+"&e_dt="+edt+str;
					}else
					{
						alert("Check the start & end Dates !");
					}
		}	
	});
	
     var ke =[],p=[];
	 var myArray2 = [];
   
 	 $(".params2").each(function(index) {
		item = {};
		item ["user"] =  $(this).text();
        item ["work"] = $(this).attr('v1');
		item ["act"] = $(this).attr('v2');
		p[index]=$(this).attr('v2');
         myArray2.push(item);
    });
	
	function removeDups(arr) {
    var result = [], map = {};
    for (var i = 0; i < arr.length; i++) {
        if (!map[arr[i]]) {
            result.push(arr[i]);
            map[arr[i]] = true;
        }
    }
    return(result);
}
 //		ke = jQuery.unique(p);
 ke = removeDups(p);
	ke.sort();	
var group_to_values = myArray2.reduce(function (obj, item) {
    obj[item.user] = obj[item.user] || [];
	item2 = {};
	item2 ["wh"] =item.work;
		item2 ["act"] = item.act;
    obj[item.user].push(item2);
    return obj;
}, {});

var groups = Object.keys(group_to_values).map(function (key) {
    return {user: key, work: group_to_values[key]};
});
  
  var k_array=[];

  $.each( groups, function( key, value ) {
  item23 = {};
		item23 ["user"] = value.user.toString();
	$.each( value.work, function( key1, value1 ) {
		item23 [value1.act.toString()] = value1.wh.toString();
	});
	k_array.push(item23);
});
   
    if(tt_data2!=0)
  {
	var chartData2 = k_array;
	 var chart2;
            AmCharts.ready(function () {
                chart2 = new AmCharts.AmSerialChart();
                chart2.dataProvider = chartData2;
                chart2.categoryField = 'user';
				chart2.synchronizeGrid=true;
				chart2.addTitle(title_head2);
                chart2.autoMargins = true;
				
                var categoryAxis2 = chart2.categoryAxis;
                categoryAxis2.gridAlpha = 0;
                categoryAxis2.axisAlpha = 0;
                categoryAxis2.gridPosition = "start";
				categoryAxis2.autoGridCount = true;
				categoryAxis2.autoWrap=true;
				categoryAxis2.title = "Weeks";
				
				// categoryAxis2.labelFunction= function(valueText,categoryAxis2) {
						// return 'W '+valueText.substring(5,7);
				// }				
				
				
				var graph2 = new AmCharts.AmGraph();
				graph2.valueAxis="OverAll";
                graph2.title = "OverAll";
                graph2.labelText = "[[value]]";
                graph2.valueField = "OverAll";
				graph2.bullet = "round";
                graph2.hideBulletsCount = 30;
				//graph2.lineColor = '#FF66	00';
				//graph2.balloonText = "[[category]]<br>[[title]] - <b>[[value]]</b>";
				graph2.balloonText = "<b>[[title]] ([[category]])</b><br>[[value]]";
				//graph2.color = 'black';
		         graph2.fillAlphas = 0;
                
				graph2.labelFunction = function(item,label){
					  return label == "0" ? "" : label;
					}
				chart2.addGraph(graph2);
				
				$.each( ke, function( key2, value2 ) {
					if(value2!="OverAll")
					{
					var graph2 = new AmCharts.AmGraph();
					graph2.valueAxis=value2;
					graph2.title = value2;
					graph2.labelText = "[[value]]";
					graph2.valueField = value2;
					graph2.bullet = "round";
					graph2.hideBulletsCount = 30;
					graph2.balloonText = "<b>[[title]] ([[category]])</b><br>[[value]]";
					 graph2.fillAlphas = 0;                
					graph2.labelFunction = function(item,label){
						  return label == "0" ? "" : label;
						}						
					chart2.addGraph(graph2);
					chart2.hideGraph(graph2);
					}
					chart2.numberFormatter = {
					  precision:2,decimalSeparator:".",thousandsSeparator:""
					};
				});	
				
					var	valueAxis2 = new AmCharts.ValueAxis();               
						valueAxis2.id='v1';
						valueAxis2.precision=2;
						valueAxis2.axisThickness=2;
						valueAxis2.position="left";
						chart2.addValueAxis(valueAxis2);
						valueAxis2.title="Avg RP per member per day";
				var legend2= new AmCharts.AmLegend();
                legend2.borderAlpha = 0.2;
                legend2.horizontalGap = 10;
                legend2.autoMargins = true;
                legend2.marginLeft = 20;
                legend2.marginRight = 20;
                chart2.addLegend(legend2, "legenddiv2");
				
				
				chart2.responsive = {
					  "enabled": true
					};
					chart2.write("chartdiv2");
				chart2.export = {
					  "enabled": true,
					  "fileName":title_head2,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 2
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv2')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart2.initHC = false;
					chart2.validateNow();
            });              
			}else{
					$("body").find(".no_dt1").removeClass('hide');
					$("body").find(".canvas_r2").addClass('hide');
				}
				
				   // BYJ-186
      $(document).ready(function() {
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });
        if (readCookie('week-pwow-hide-col-list') != null) {
          window.hidePWowCol = readCookie('week-pwow-hide-col-list').split(',');
          setTimeout(function() {
            readCookie('week-pwow-hide-col-list').split(',').forEach(function (item, index) {
              $(document).find('g[aria-label="'+item+'"]').find('g.amcharts-legend-switch').click();
            });
          }, 2000);
          // if (readCookie('week-pwow-hide-col-list') != '') {
          //   $('label.pwow-reset-filter').remove();
          //   $('div#legenddiv2').before('<label class="label label-warning pwow-reset-filter" href="#" data-toggle="tooltip" title="'+ readCookie('week-pwow-hide-col-list') +'" style="margin-left:23px;display:block;cursor:pointer;width:100px">Reset Team Filter</label>');
          // }
        } else {
          window.hidePWowCol = [];
        }

        $(document).on('click', 'g[aria-label]', function() {
          var labelName = $(this).attr('aria-label');
          if ($(this).find('g.amcharts-legend-switch').attr('visibility') == undefined) {
            if (window.hidePWowCol.indexOf(labelName.trim()) <= -1) {
              window.hidePWowCol.push(labelName.trim());
            }
          } else {
            window.hidePWowCol.popArray(labelName.trim());
          }
          createCookie('week-pwow-hide-col-list', window.hidePWowCol.filter(onlyUnique), 1);
          $('label.pwow-reset-filter').remove();
          if (readCookie('week-pwow-hide-col-list') != '') {
            $('div#legenddiv2').before('<label class="label label-warning pwow-reset-filter" href="#" data-toggle="tooltip" title="'+ readCookie('week-pwow-hide-col-list') +'" style="margin-left:23px;display:block;cursor:pointer;width:100px">Reset Team Filter</label>');
          }
        });

        $(document).on('click', 'label.pwow-reset-filter', function() {
          eraseCookie('week-pwow-hide-col-list');
          window.location = window.location.href;
        });
      });
	  
	</script>
  </body>
</html>
