<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm=str_replace('.php','',basename(__FILE__));

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
} 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
		<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	<style>
		li[role="menuitem"]:first-child{
			display:none;
		}
	</style>
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">	
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>
			
				<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			<div class="col-md-10">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>					
					</div>
			</fieldset>				
			</div>			
			</div>
			
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2 stab_dis_selec' ch="Floor_View">Weekly Summary</button>
						<button class='stab_stages_2 ' ch="FV_Wow">Productivity WoW</button>	
						<button class='stab_stages_2 ' ch="FV_Spread">Trends</button>	
						<button class='stab_stages_2' ch="FV_Feed">Scatter</button>						
						<button class='stab_stages_2 ' ch="FV_Summary">Descriptives</button>
						<button class='stab_stages_2' ch="FV_Drilldown">Pies</button>					
						<button class='stab_stages_2' ch="FV_Member_Info">Weekly Report</button>
						<button class='stab_stages_2 ' ch="FV_Contributor">Projects & Members</button>
					</div>
				</div>
					<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));				
							if($day_w==0)
							{
								$day_w=7;								
							}
							$day_fm=date('d-M-Y',strtotime($day));
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));							
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));							
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));					
					?>
					<div class='row hid'>	
						<div class='col-md-12'>	
							<div class='row third-row head'>												
												<?php
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Choose Date: </label>	
															<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
														</span>
													</div>
													<div class='col-md-6 cur-month text-center'><span>Week (".$week_st." to ".$week_end.")</span>
													</div>";
												$titl1='';
												$titl2='';
												$titl3='';												
												?>
							</div>							
							<div class='row row_style_1' id='c_find'>	
									<div class='col-md-12'>					
												<a class='arr pull-left' ch='Floor_View' w_val="<?= $w_prev ?>">&laquo; Prev</a>						
												<a class='arr pull-right' ch='Floor_View' w_val="<?= $w_nxt ?>">Next&raquo;</a>					
									</div>														
							</div>
							<div class='row row_style_1'>	
							<div class='col-md-4'>
								<label class='l_font_fix_3'>Choose Dept:</label>
								<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
								<?php
								foreach ($dept_val as $row)
								{
									$sel='';
										if($dept_opt==$row['dept_id'])
										{
											$sel='selected';																		
										}
									echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
								}
								?>
								</select>
							</div>
						</div>
				<?php 
					if($proj)
					{
					echo "					
								<table class='display table table-bordered table-responsive' data-sortable='true' data-align='center' data-valign='middle' data-show-columns='true' data-escape='false' data-show-export='true' data-toolbar='#toolbar' id='table3' data-search-time-out=500 data-search='true'>";
								
										echo "<thead><tr><th class='l_font_fix_3' data-valign='middle'>Metrics</th>";
								foreach($proj AS $key1)
								{
								echo "<th class='l_font_fix_3' data-sortable='true' data-valign='middle' data-align='center'>".$key1."</th>";
								}
								echo "</tr></thead>";
							
									echo "<tbody>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'><i class='glyphicon glyphicon-stop t_stat_1'></i> Audit Pending</td>";
										foreach($proj AS $key1)
											{
												echo "<td>".$avg[$key1]['A_stat']."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'><i class='glyphicon glyphicon-stop t_stat_2'></i> Review Pending</td>";
										foreach($proj AS $key1)
											{
												echo "<td>".$avg[$key1]['R_stat']."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'><i class='glyphicon glyphicon-stop t_stat_3'></i> Confirmation Pending</td>";
										foreach($proj AS $key1)
											{
												echo "<td>".$avg[$key1]['D_stat']."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'><i class='glyphicon glyphicon-stop t_stat_4'></i> Confirmed</td>";
										foreach($proj AS $key1)
											{
												echo "<td>".$avg[$key1]['C_stat']."</td>";
											}
									echo "</tr>";
									
									// echo "<tr>";
										// echo "<td class='l_font_fix_1'>Total Contributors</td>";
										// foreach($proj AS $key1)
											// {
														// echo "<td>".$avg[$key1]['Mem_c']."</td>";
											// }
									// echo "</tr>";
										echo "<td class='l_font_fix_1'>Grading Pending</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".($avg[$key1]['Mem_c']- $avg[$key1]['G_done'])."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Grading Done</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".$avg[$key1]['G_done']."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Highest Team Contribution</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".round($avg[$key1]['Max_c'],2)."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Lowest Team Contribution</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".round($avg[$key1]['Min_c'],2)."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Total XP by Contributors</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".round($avg[$key1]['Sum_c'],2)."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Highest XP Scorer</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".str_replace(',','<br>',$avg[$key1]['High_sc'])."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Highest XP</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".round($avg[$key1]['Max'],2)."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Lowest XP Scorer</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".str_replace(",","<br>",$avg[$key1]['Low_sc'])."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Lowest XP</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".round($avg[$key1]['Min'],2)."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Total XP by Members</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".$avg[$key1]['Sum']."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Team Members</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".$avg[$key1]['Mem']."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Weekly Output per TM</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".round($avg[$key1]['Avg'],2)."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Efficiency</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".round($avg[$key1]['Efficiency'],2)."</td>";
											}
									echo "</tr>";
									$f_grades=array("1"=>"Needs Awareness","2"=>"Needs Improvement","3"=>"Meets Expectation",
			"4"=>"Exceeds Expectation","5"=>"Strongly Exceeds Expectation");
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Avg Grading</td>";										
										foreach($proj AS $key1)
											{
												if($avg[$key1]['A_grade'])
												{
													echo "<td>".$f_grades[$avg[$key1]['A_grade']]."</td>";
												}else{
													echo "<td>-</td>";
												}
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Top Quartile Share</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".$avg[$key1]['Top']."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Bottom Quartile Share</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".$avg[$key1]['Bottom']."</td>";
											}
									echo "</tr>";
									// echo "<tr>";
										// echo "<td class='l_font_fix_1'>Grading Pending</td>";
										// foreach($proj AS $key1)
											// {
														// echo "<td>".$avg[$key1]['G_pending']."</td>";
											// }
									// echo "</tr>";
									
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Non Quant Share</td>";
										foreach($proj AS $key1)
											{
														echo "<td>".$avg[$key1]['NQShare']."</td>";
											}
									echo "</tr>";
										echo "<tr>";
										echo "<td class='l_font_fix_1'>Avg Work Hours</td>";
										foreach($proj AS $key1)
											{
												echo "<td>".$avg[$key1]['InOutAvg']."</td>";
											}
									echo "</tr>";
									echo "<tr>";
										echo "<td class='l_font_fix_1'>Median In time</td>";
										foreach($proj AS $key1)
											{
												echo "<td>".$avg[$key1]['mediantime']."</td>";
											}
									echo "</tr></tbody>";
							echo "</table>";	
					}else{
						echo "<div class='col-md-12 text-center'>No Records</div>";
					}
						?>
					</div>			        
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script>    
	$("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						var str='';
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						window.location = base_url+"index.php/User/load_view_f?a=Floor_View&date_view="+date_v+str;
						}
					});	
	});
	
	var ele=Date.parse($("body").find(".ch_dt").val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						//console.log(date_v);
	
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
						var ele=Date.parse($("body").find(".ch_dt").val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						var str='';
						var dept=$(this).val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						window.location = base_url+"index.php/User/load_view_f?a=Floor_View&date_view="+date_v+str;			
		}	
	});
	
	$('#table3').bootstrapTable({
			pageSize:20,
			stickyHeader:true,
			exportDataType:'all'
	});
	
	  $(document).ready(function() {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
    if (readCookie('week-summary-hide-col-list') != null) {
      window.hideCol = readCookie('week-summary-hide-col-list').split(',');
      readCookie('week-summary-hide-col-list').split(',').forEach(function (item, index) {
        var itemIndex = $('div.keep-open > ul.dropdown-menu[role="menu"] > li > label:contains("'+item+'") > input[type="checkbox"]').val();
        if (itemIndex != undefined) {
          $('#table3').bootstrapTable('hideColumn', itemIndex);
        }
      });
      if (readCookie('week-summary-hide-col-list') != '') {
        $(document).find('div.keep-open').after('<label class="label label-warning reset-filter" href="#" data-toggle="tooltip" title="'+ readCookie('week-summary-hide-col-list') +'" style="margin-left:10px;display:inline;cursor:pointer">Reset Team Filter</label>');
      }
    } else {
      window.hideCol = [];
    }

    $('div.keep-open > ul.dropdown-menu[role="menu"] > li > label > input[type="checkbox"]').on('click', function(e) {
      if (!$(this).is(':checked')) {
        window.hideCol.push($(this).parent().text().trim());
      } else {
        window.hideCol.popArray($(this).parent().text().trim());
      }

      createCookie('week-summary-hide-col-list', window.hideCol, 1);
      $('label.reset-filter').remove();
      if (readCookie('week-summary-hide-col-list') != '') {
        $(document).find('div.keep-open').after('<label class="label label-warning reset-filter" href="#" data-toggle="tooltip" title="'+ readCookie('week-summary-hide-col-list') +'" style="margin-left:10px;display:inline;cursor:pointer">Reset Team Filter</label>');
      }
    });

    $('label.reset-filter').on('click', function() {
      eraseCookie('week-summary-hide-col-list');
      window.location = window.location.href;
    });
  });
	</script>
  </body>
</html>