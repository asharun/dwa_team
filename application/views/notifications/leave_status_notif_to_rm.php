<?php
if (!empty($pendingLeave)) {
	$managerName = $pendingLeave[0]['manager_name'];
} else if (!empty($approvedLeave)) {
	$managerName = $approvedLeave[0]['manager_name'];
}else if (!empty($rejectedLeave)) {
	$managerName = $rejectedLeave[0]['manager_name'];
}
if (empty($managerName)) {
	return null;
}
?>

Hi, <?php echo $managerName; ?> .<br>
Here is the previous month leave status from date <?php echo $startDate; ?> to <?php echo $endDate; ?>, please check it out


<h4>Pending Leave(s)</h4>
<table class="table">
	<th>Name</th>
	<th>Start Date</th>
	<th>End Date</th>	
	<th>Reason</th>
	<th>Status</th>

	<?php foreach ($pendingLeave as $key => $leave): ?>
		<tr>
			<td><?php echo $leave['reportee']; ?></td>
			<td><?php echo $leave['start_date']; ?></td>
			<td><?php echo $leave['end_date']; ?></td>
			<td><?php echo $leave['reason']; ?></td>
			<td><?php echo $leave['conf_status']; ?></td>
		</tr>
	<?php endforeach; ?>
</table>

<h4>Approved Leave(s)</h4>
<table class="table">
	<th>Name</th>
	<th>Start Date</th>
	<th>End Date</th>	
	<th>Reason</th>
	<th>Status</th>

	<?php foreach ($approvedLeave as $key => $leave): ?>
		<tr>
			<td><?php echo $leave['reportee']; ?></td>
			<td><?php echo $leave['start_date']; ?></td>
			<td><?php echo $leave['end_date']; ?></td>
			<td><?php echo $leave['reason']; ?></td>
			<td><?php echo $leave['conf_status']; ?></td>
		</tr>
	<?php endforeach; ?>
</table>

<h4>Rejected Leave(s)</h4>
<table class="table">
	<th>Name</th>
	<th>Start Date</th>
	<th>End Date</th>	
	<th>Reason</th>
	<th>Status</th>

	<?php foreach ($rejectedLeave as $key => $leave): ?>
		<tr>
			<td><?php echo $leave['reportee']; ?></td>
			<td><?php echo $leave['start_date']; ?></td>
			<td><?php echo $leave['end_date']; ?></td>
			<td><?php echo $leave['reason']; ?></td>
			<td><?php echo $leave['conf_status']; ?></td>
		</tr>
	<?php endforeach; ?>
</table>