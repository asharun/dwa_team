<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm="Reportees";
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Reportees">Data</button>
						<button class='stab_stages_2 stab_dis_selec' ch="R_Team_Graphs">Activity-wise Graph</button>
						<button class='stab_stages_2' ch="R_Breakdown">Breakdown</button>
						<button class='stab_stages_2' ch="R_Leave_Confirm"><span>Leave Approval</span> <?php echo '<span class="badge badge-info">'.($la_cnt_val[0]["i_cnt"]+$la_wee_cnt_val[0]["i_cnt"]).'</span>';?></button>
					</div>
				</div>
		
					<?php 	
						if(($s_dt) && ($e_dt))
						{
							$start_dt = date('d-M-Y', strtotime($s_dt));
							$end_dt = date('d-M-Y', strtotime($e_dt));
						}else
						{
							$day = date('Y-m-d H:i:s');
							$day_w = date('w',strtotime($day));
							if($day_w==0)
							{
								$day_w=7;								
							}							
							$week_end = date('Y-m-d H:i:s', strtotime($day.' +'.(7-$day_w).' days'));
							$week_start = date('Y-m-d H:i:s', strtotime($day.'+'.(1-$day_w).' days'));
										
							$start_dt = date('d-M-Y', strtotime($week_start));
							$end_dt = date('d-M-Y',strtotime($week_end));
						}				
					?>
					
					<div class='row hid'>	
						<div class='col-md-12'>				
						<div class='row third-row head'>
												<?php
												if(!$level_opt)
															{
																$level_opt=0;
															}
															$lel_val=array("Activity","Projects","Category","Element","Action","Trade");
						echo "<div class='col-md-3'>
								<span>
									<label class='l_font_fix_3'>Start Date: </label>	
									<input id='t_dtpicker' class='s_dt date_fm date-picker' value='".$start_dt."' />
								</span>
							</div>
							<div class='col-md-3'>
								<span>
									<label class='l_font_fix_3'>End Date: </label>	
									<input id='t_dtpicker' class='e_dt date_fm date-picker' value='".$end_dt."' />
								</span>
							</div>
							<div class='col-md-2'>
										<button class='btn add_but gre_but sub_repo' type='button'>Submit</button>
										</div>
							</div>
							<div class='row row_style_1 third-row head'>
				<div class='col-md-12 cur-month text-center'><span>".$lel_val[$level_opt]."-wise Break-up (".$start_dt." to ".$end_dt.")</span>
												</div>	";
												$titl='';
												?>
							</div>
									<div class='row row_style_1'>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Level:</label>
																<select id='sel_2345' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option data-hidden="true"></option>';
															
															foreach ($lel_val as $key=>$value)
															{
																$sel='';
																	if($level_opt==$key)
																	{
																		$sel='selected';																		
																	}
																echo "<option value='".$key."' ".$sel.">".$value."</option>";
															}
															
																?>
															</select>
															</div>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Employee:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															
															echo '<option data-hidden="true"></option>';
															if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($pro_sel_val==$row2['sel_1_id'])
																	{
																		$sel='selected';	
																	$titl=$row2['sel_1_name']." - ".$lel_val[$level_opt]."-wise Break-up (".$start_dt." to ".$end_dt.")";																		
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>										
										</div>
			<?php
			echo "<div class='row row_style_2'><div class='col-md-12'><div class='no_dt hide'>No Records!</div></div></div>";
						echo "<div class='data'>";
						if($tm_data)
						{
							foreach($tm_data AS $row1)
							{
								if($level_opt==0)
								{
									$tsdff=explode('_', $row1['activity_name'], 2);
									$f_repl=$tsdff[1];
								}else{
									$f_repl=$row1['activity_name'];
								}									
									echo "<h5 class='hide params1' user_id='".$row1['full_name']."' act_id='".$f_repl."'>".$row1['equiv_xp']."</h5>";
							}
						}else
						{
							$tm_data=null;
						}
								echo "</div>";
							echo "<div class='canvas_r text-center'>";
							echo '<div style="width: auto; height: 400px;" id="chartdiv"></div>';
		
			?>
			<div id="legenddiv" style="overflow: scroll; position: relative;text-align: left;width: auto;min-height:70px;max-height:150px!important;"></div>
			</div>
			</div>
			        
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var title_head = '<?php echo $titl ?>';</script>
	<script>var tt_data = '<?php echo sizeof($tm_data) ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/serial.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>

		$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");
						var lvl=$("body").find("#sel_2345").val();
						var str='';
						str=str+"&pro="+$(this).val();
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}						
			window.location = base_url+"index.php/User/load_view_f?a=R_Team_Graphs&s_dt="+sdt+"&e_dt="+edt+str;
		}
	});
	
$("body").on("change","#sel_2345",function(){
		if($(this).val())
		{
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");
	
			var pro_v=$("body").find("#sel_1234").val();
						var str='';
						str=str+"&lvl="+$(this).val();
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}		
			window.location = base_url+"index.php/User/load_view_f?a=R_Team_Graphs&s_dt="+sdt+"&e_dt="+edt+str;
		}
	});
	
	var ele=Date.parse($('.s_dt').val()); 
	var s_dt=moment(ele).format("YYYY-MM-DD");
	var ele1=Date.parse($('.e_dt').val()); 
	var e_dt=moment(ele1).format("YYYY-MM-DD");
	var pr='';
	var lev=0;
	
	if($("#sel_1234").find("option:selected").val())
	{
		pr=$("#sel_1234").find("option:selected").val();
	}
	
	if($("#sel_2345").find("option:selected").val())
	{
		lev=$("#sel_2345").find("option:selected").val();
	}
	
	$("body").on("click",".sub_repo",function(){
			var ele=Date.parse($('.s_dt').val()); 
			var sdt=moment(ele).format("YYYY-MM-DD");
			var ele1=Date.parse($('.e_dt').val()); 
			var edt=moment(ele1).format("YYYY-MM-DD");	
			var pro_v=$("body").find("#sel_1234").val();
						var str='';
						var lvl=$("body").find("#sel_2345").val();
						
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
						
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						
			window.location = base_url+"index.php/User/load_view_f?a=R_Team_Graphs&s_dt="+sdt+"&e_dt="+edt+str;
	});
	
	$("body").on("focus", ".s_dt",function(){
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1"
			});
	});
	
	$("body").on("focus", ".e_dt",function(){		
		$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1"
			});
	});

     var myArray =[];
	 var a_id = [];
   
var p=[];
    $(".params1").each(function(index) {
		item = {};
		item ["act"] =  $(this).attr('act_id');
        item ["work"] = $(this).text();
		item ["user"] = $(this).attr('user_id');
         myArray.push(item);
		  p[index]=$(this).attr('user_id');
    });

var group_to_values = myArray.reduce(function (obj, item) {
    obj[item.act] = obj[item.act] || [];
	item2 = {};
	item2 ["wh"] =item.work;
		item2 ["user"] = item.user;
    obj[item.act].push(item2);
    return obj;
}, {});

var groups = Object.keys(group_to_values).map(function (key) {
    return {act: key, work: group_to_values[key]};
});
  
  var k_array=[];

  $.each( groups, function( key, value ) {
  item23 = {};
		item23 ["act"] = value.act.toString();
 
	$.each( value.work, function( key1, value1 ) {
		item23 [value1.user.toString()] = value1.wh.toString();
   
	});
	k_array.push(item23);
});
   
  //console.log(k_array);
  //console.log(p);
  
  //ke = jQuery.unique(p);
  
  
	function removeDups(arr) {
    var result = [], map = {};
    for (var i = 0; i < arr.length; i++) {
        if (!map[arr[i]]) {
            result.push(arr[i]);
            map[arr[i]] = true;
        }
    }
    return(result);
}
 //		ke = jQuery.unique(p);
 ke = removeDups(p);
 
    if(tt_data!=0)
  {
	 var chart;

            var chartData = k_array;
			

            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData;
                chart.categoryField = "act";				
				chart.addTitle(title_head);
                chart.autoMargins = true;
                
                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridAlpha = 0;
				
                categoryAxis.axisAlpha = 0;
				categoryAxis.stackByValue = true;
				//"stackByValue": true,
                categoryAxis.gridPosition = "start";
				categoryAxis.autoGridCount = false;
				categoryAxis.gridCount = chartData.length;
				//categoryAxis.labelRotation = "0";
				categoryAxis.labelFunction= function(valueText, serialDataItem, categoryAxis) {
      if (valueText.length > 20)
	  {
		return valueText.substring(0, 20) + '...';
	  }
      else
	  {
        return valueText;
	  }
    }
				categoryAxis.autoWrap=true;
				
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.stackType = "100%"; // this line makes the chart 100% stacked
                
                valueAxis.labelsEnabled = true;
                chart.addValueAxis(valueAxis);

				$.each( ke, function( key2, value2 ) {
                var graph = new AmCharts.AmGraph();
                graph.title = value2;
                graph.labelText = "[[percents]]%";
                graph.balloonText = "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> XP ([[percents]]%)</span>";
                graph.valueField = value2;
                graph.type = "column";
                graph.lineAlpha = 0;
				//graph.labelRotation= 90;
                graph.fillAlphas = 0.8;
                //graph.lineColor = "#C72C95";
                chart.addGraph(graph);
				});
					
                // LEGEND
                var legend = new AmCharts.AmLegend();
                legend.borderAlpha = 0.2;
                legend.horizontalGap = 10;
                legend.autoMargins = true;
                legend.marginLeft = 20;
                legend.marginRight = 20;
                chart.addLegend(legend, "legenddiv");

				var legend = new AmCharts.AmLegend();
                legend.borderAlpha = 0.2;
                legend.horizontalGap = 10;
                legend.autoMargins = true;
                legend.marginLeft = 20;
                legend.marginRight = 20;
				
                chart.addLegend(legend, "legenddiv");

				chart.responsive = {
					  "enabled": true
					};
					chart.write("chartdiv");
				chart.export = {
					  "enabled": true,
					  "border": {
							"stroke": "#000000",  // HEX-CODE to define the border color
							"strokeWidth": 1,     // number which represents the width in pixel
							"strokeOpacity": 1    // number which controls the opacity from 0 - 1
						  },
					  "exportTitles" :true,
					  "legend": {
									"position": "bottom",
									 "height": $('#legenddiv')[0].scrollHeight
								  },
					   "menu": [ {
						  "class": "export-main",
						  "menu": [ "PNG", "JPG" ]
					   }]
					};
					chart.initHC = false;
					chart.validateNow();
					
                // WRITE
                
				// chart.validateNow();
            });
			}else{
	  $("body").find(".no_dt").removeClass('hide');
  } 
	</script>
  </body>
</html>