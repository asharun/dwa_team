<div class='row hid1'>	
			<div class='col-md-12'>
			<?php 
			
				$tab_array=array("Team_Member_Info","Employee_Info","Team_Graphs","Team_Split_Graph","Status_Report","Graph_Breakdown","Graph_Summary","Employee_Leave");
				$tab_name=array("Team_Member_Info"=>"Report Card",
								"Employee_Info"=>"Employee Records",
								"Graph_Breakdown"=>"Pies"
								"Team_Graphs"=>"Swiss Rolls");
				
				<button class='stab_stages_2 stab_dis_selec' ch="Team_Member_Info">Report Card</button>
				<button class='stab_stages_2' ch="Employee_Info">Employee Records</button>
				<button class='stab_stages_2' ch="Team_Graphs">Activity-wise</button> 
				<button class='stab_stages_2' ch="Team_Split_Graph">Member-wise</button> 
				<button class='stab_stages_2' ch='Status_Report'>Activity Records</button>
				<button class='stab_stages_2' ch="Graph_Breakdown">Pie Charts</button>
				<button class='stab_stages_2' ch="Graph_Sum_Sc">Stats</button>
				<button class='stab_stages_2' ch="Employee_Leave">Leave Tracker</button>
				
				?>
			</div>
		</div>

		<button class='stab_stages_2' ch="Team_Split_Graph">Member-wise</button> 
		
		<button class='stab_stages_2' ch="Graph_Drilldown">Drilldown</button>