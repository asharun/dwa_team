<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
  $file_nm=str_replace('.php','',basename(__FILE__));

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				
				<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			<div class="col-md-10">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>					
					</div>
			</fieldset>				
			</div>			
			</div>
				<div class='row third-row head'>												
					<div class='col-md-12 cur-month text-center'>Issues</div>
				</div>
				<div class='row hid1'>	
			<div class='col-md-12'>
				<button class='stab_stages stab_dis_selec' ch="Issues"><span>Pending</span> <?php echo '<span class="badge badge-info">'.$iss_cnt_val[0]["i_cnt"].'<span>';?></button>
				<button class='stab_stages' ch="Issues_comp">Modify Issue</button>
				<!-- <button class='stab_stages' ch="Issues_reject">Rejected</button> -->
			</div>
		</div>
		
					<div class='row hid'>	
						<div class='col-md-12'>	
								<div class='row row_style_1 text-center'>
										<div class='col-md-10'>
											<div class="row">
											<div class='col-md-4'>
												<label class='l_font_fix_3'>Choose Dept:</label>
												<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
												<?php
												foreach ($dept_val as $row)
												{
													$sel='';
														if($dept_opt==$row['dept_id'])
														{
															$sel='selected';																		
														}
													echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
												}
												?>
												</select>
											</div>
											</div>
										</div>
									</div>
						<div id="toolbar" >
										<select class="form-control">
												<option value="">Export Page</option>
												<option value="all">Export All</option>
												<option value="selected">Export Selected</option>
										</select>
								</div>
						<table class="display table table-bordered table-responsive" data-show-footer="true" data-footer-style="footerStyle"  data-toolbar="#toolbar" data-filter-control="true" data-checkbox-header="false" data-show-export="true" id="table3" data-search-time-out=500 data-pagination="true" data-search="true">
								<thead>
									<tr>
										  <th data-class="l_font_fix_3" data-checkbox="true" data-field="state">#</th>
										  <th data-class='l_font_fix_3 hidden resp_id'  data-field="response_id">ID</th>
										  <th data-class="l_font_fix_3 hidden act_id" data-field="activity_id">Act Id</th>
										  <th data-sortable="true" data-class="l_font_fix_3 act_nm" data-filter-control="input" data-field="day_val"  data-formatter="dateSortFormate">Date</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="full_name">User Name</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="issue_cat">Issue Category</th>
										  <th data-sortable="true" data-class="l_font_fix_3 act_nm" data-filter-control="input" data-field="activity_name">Activity Name</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="wu_name">Work Units</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="work_comp">Work Done</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="equiv_xp" data-footer-formatter="totalFilledXp">Filled Xp</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="audit_xp" data-footer-formatter="totalAuditXp">Audit Xp</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="audit_desc">Audit Desc</th>
										  <th data-sortable="true" data-align="center" data-halign="center" data-class="l_font_fix_3" data-filter-control="input" data-formatter="nameFormatter" data-field="tast_stat1">Status</th>
										  <th data-sortable="true" data-class="l_font_fix_3" data-field="issue_desc">Issue Desc</th>
									</tr>
								</thead>
							</table>							
							<div class='row row_style_1'>
								<div class='col-md-12 text-center'>
									<button class='btn iss_but is_approve' type='button'>Award XPs</button>
									<button class='btn iss_but red_but is_reject' type='button'>Reject Issues</button>
								</div>
							</div>
							
							<div class='modal fade open_col' id='show_gl_col23'>										
											<div class='modal-dialog asdklk_qw' role='document'>
												<div class='modal-content'>
												  <div class='modal-body' id='modal_edit'>
												    <div class='row row_style_1 text-center'>
														<div class='col-md-3 col-md-offset-3'>
															<label class='l_font_fix_3'>Comp XP</label>	
															<input type='number' class='iss_comp_xp form-control'/>	
														</div>
														<div class='col-md-2'>
														<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>	
														<button class='btn add_but is_app_sub' type='button'>Submit</button>
														</div>
													</div>
												 </div>								  
												</div>
											  </div>
									</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>

$('#toolbar').find('select').change(function () {
            $('#table3').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });

		function totalFilledXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "<strong>Filled XP:<br> " +total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+ "</strong>";
            }
            function totalAuditXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "<strong>Audit XP:<br> " +total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+ "</strong>";
            }
		
		function dateSortFormate(value, row, index){
	var date_cck = new Date(value*1000);
	if(value){
		return moment(date_cck).format("DD/MM/YYYY");
	}
}
function nameFormatter(value, row) {
	     var t=value.split('|');
        return "<i class='glyphicon glyphicon-stop "+t[1]+"'></i> "+t[0];
    }
	
	
	
$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			var dep_v=$(this).val();					
			window.location = base_url+"index.php/User/load_view_f?a=Issues"+"&dept="+dep_v;
		}	
	});
	
$.ajax({
       url: base_url+"index.php/User/load_table_boots?a=Issues"+"&dept="+dep_opt,
       dataType: 'json',
       success: function(response) {
           $('#table3').bootstrapTable({
              data: response   ,
			stickyHeader: true			  
			});			
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });
</script>
  </body>
</html>