<?php 
	$start_date = $startDateFilter ? $startDateFilter : date("Y-m-d");
	$end_date = $endDateFilter ? $endDateFilter : date("Y-m-d",strtotime("+7 days"));
?>


<div class='row third-row head'>
	<?php
	echo "
	<div class='col-md-12 cur-month text-center row_style_1'><span>(".$startDate." to ".$endDate.")</span>
	</div>
	<div class='col-md-3'>
		<span>
		<label class='l_font_fix_3'>Start Date: </label>	
		<input id='st_dt' class='ch_dt date_fm date-picker' value='".date("d-M-Y", strtotime($start_date))."' />
		</span>
	</div>
	<div class='col-md-3'>
		<span>
			<label class='l_font_fix_3'>End Date: </label>	
			<input id='e_dt' class='ch_dt date_fm date-picker' value='".date("d-M-Y", strtotime($end_date))."' />
		</span>
	</div>
	<div class='col-md-3'>
		<button type='button' class='btn btn_dt_filter gre_but'><strong>Submit</strong></button>
	</div>
		";
	?>
</div>