<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
$file_nm='Team_Member_Info';
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>"  type="text/css">
	
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
					<div class='col-md-12'>
					<button class='stab_stages_2' ch="Team_Member_Info">Report Card</button>						
						<button class='stab_stages_2' ch="Graph_Drilldown">Pies</button>
						<button class='stab_stages_2' ch="Team_Split_Graph">Swiss Rolls</button>
						<button class='stab_stages_2' ch="Graph_Trends">Trends</button>					
						<button class='stab_stages_2' ch="Graph_Sum_Sc">Stats</button>
						<button class='stab_stages_2' ch="Employee_Info">Employee Records</button>				
						<button class='stab_stages_2 stab_dis_selec' ch='Status_Report'>Activity Records</button>						
						<button class='stab_stages_2' ch="Employee_Leave">Leave Tracker</button>
						<button class='stab_stages_2' ch="WPR_Feed">Scatter</button>
					</div>
				</div>													
					<div class='row hid'>	
						<div class='col-md-12'>	
						
						<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}						
						if($f==0)
							{
									$day_w  = date('w', strtotime($day));
								if ($day_w == 0) {
									$day_w = 7;
								}				
								$st_dt = date('Y-m-d', strtotime($day . ' -' . ($day_w - 1) . ' days'));
								$e_dt   = date('Y-m-d', strtotime($day . ' +' . (7 - $day_w) . ' days'));				
							}
							// else if($f==1)
							// {
								// $st_dt = date('Y-m-d', strtotime($day . ' -' . (4) . ' days'));
								// $e_dt   = date('Y-m-d', strtotime($day . '-' . (1) . ' days'));	
							// }else if($f==2)
							// {
								// $st_dt = date('Y-m-d', strtotime($day . ' -' . (8) . ' days'));
								// $e_dt   = date('Y-m-d', strtotime($day . '-' . (1) . ' days'));	
							// }
							
							$day_fm=date('d-M-Y',strtotime($day));							
							$week_st = date('d-M', strtotime($st_dt));
							$week_end = date('d-M', strtotime($e_dt));													
					?>
			
			<div class='row third-row head'>
												
												<?php
												//echo "sdf ".$pro_sel_val;
												echo "<div class='col-md-3'>
												<span>
												<label class='l_font_fix_3'>Choose Date: </label>	
													<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
													</span>
												</div>
												<div class='col-md-6 cur-month text-center'><span>".$week_st." - ".$week_end."</span>
												</div>	";
												?>
							</div>
							
									
											<div class='row row_style_1'>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Dept:</label>
															<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php
															foreach ($dept_val as $row)
															{
																$sel='';
																	if($dept_opt==$row['dept_id'])
																	{
																		$sel='selected';																		
																	}
																echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
															}
															?>
															</select>
														</div>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Project:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 	
															echo '<option data-hidden="true"></option>';
															if($pro_sel_dta)
															{
																foreach ($pro_sel_dta as $row2)
																{
																	$sel='';
																		if($pro_sel_val==$row2['sel_1_id'])
																		{
																			$sel='selected';																		
																		}
																	echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
																}
															}
																?>
															</select>
														</div>
														
										</div>
								<hr class="st_hr2">
								<div id="toolbar" > 
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="all">Export All</option>
									<option value="selected">Export Selected</option>
							</select>
								</div>
							<table class="display table table-bordered table-responsive" data-filter-control="true" data-show-footer="true" data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table" data-search-time-out=500 data-pagination="true" data-search="true">
										<thead>
											<tr>
											  <th data-class="l_font_fix_3" data-formatter="runningFormatter">#</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="activity_name">Activity Name</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="wu_name">Work Units</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="quantity_done">Quantity Done</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="final_xp"  data-footer-formatter="totalEquivXp">Equiv XP</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="achieved_tgt">Suggested Target</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="std_tgt">Set Target</th>											  
											</tr>
										</thead>
									</table>						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>

	<script>
	var ele=Date.parse($(".ch_dt").val()); 
	var date_v=moment(ele).format("YYYY-MM-DD");	
	
	$('#toolbar').find('select').change(function () {
            $('#table').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });
	
	 function totalEquivXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "<strong>Equiv XP:<br> " +total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]+ "</strong>";
            }


	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Project",dept:$(this).val()},
				success: function(response){				
					$('#sel_1234').html(response).selectpicker('refresh');
				}
			});
		}	
	});
	
$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{
			var pro_v=$("body").find("#sel_1234").val();
						var dept=$("body").find("#sel_dept_1").val();
						var str='';
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						if(dept)
						{
							str=str+"&dept="+dept;
						}
			window.location = base_url+"index.php/User/load_view_f?a=Status_Report&date_view="+date_v+str;
		}
	});
	
	function runningFormatter(value, row, index) {
index++; {
return index;
}
}

	var pr='';
	if($("#sel_1234").find("option:selected").val())
	{
		pr=$("#sel_1234").find("option:selected").val();
	}
	
	
	$.ajax({
       url: base_url+"index.php/User/load_table_boots?a=Status_Report&date_view="+date_v+"&pro="+pr+"&dept="+dep_opt,
       dataType: 'json',
       success: function(response) {
           $('#table').bootstrapTable({
              data: response,
			  stickyHeader: true
           });
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });
	

	$("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						var pro_v=$("body").find("#sel_1234").val();
						var str='';
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}	
						
						window.location = base_url+"index.php/User/load_view_f?a=Status_Report&date_view="+date_v+str;
						
						}
					});	
	});
	</script>
  </body>
</html>