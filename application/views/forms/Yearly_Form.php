<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
$file_nm=str_replace('.php','',basename(__FILE__));
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);


if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

$file_nm=str_replace('.php','',basename(__FILE__));
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	  <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/forms/custom.css" rel="stylesheet">
  <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/forms/purple.css" rel="stylesheet">
	
  </head>
  <body>
<body class="nav-md">

  <div class="container body">

    <div class="main_container">
		<?php
        $data['file_nm']=$file_nm;
        $this->load->view("common/Appraisal_Sidebar.php", $data);
		$this->load->view("common/Appraisal_Topbar.php", $data);
        ?>
      

      <!-- page content -->
	    <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3><?php echo "Self-Appraisal Platform" ?></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel" id="cont_replace">
                  <p>Dear Content/Design Guru,</p>
			<p> Welcome to the new self-appraisal platform. This is a platform to highlight your contribution towards the company over the past year and help make your growth smoother in the company and contribution for the next year. This process is designed to provide time for the employee and supervisors to look back over the past and plan realistically for the future.     </p>
			<p> We encourage you to be proud of your accomplishments and candid about your areas of improvement. The questions which follow are intended to help you organize your thoughts and share information with your supervisor prior to receiving your performance appraisal.  </p>
			<p>An essential goal of the appraisal meeting is that both you and your supervisors know clearly what you expect of each other and feel strongly that you can achieve your objectives by working together.</p>
			<ul style="list-style-type:square;">The following text will be useful guidelines if confusions arise:
			<li> Priority order is from 1 to 5, with 5 being your highest priority.</li>
			<li> Weigh each option against the others to come up with your most-suited priority order.</li>
			<li> Short Answer:
				<ul>
			   <li> Fill the right-spelled names of people according to the details in ESP, when required</li>
			   <li> Give crisp responses</li>
			   </ul>
			   </li>
			<li>Long Answer:
				<ul>
			  <li> Write your answers in detail.</li>
			  <li> When suggestions are asked for, make them point-wise and specific.</li>			
			  </ul>
			  </li>
			  <li>Be comprehensive and courteous!</li>
			  </ul>					  <hr>
                </div>
              </div>
            </div>

          </div>
        </div>
      
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var ss_section_id = '<?php echo $this->session->userdata('sec_id') ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/bootstrap-progressbar.min.js"></script>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/jquery.nicescroll.min.js"></script>
		  <!-- icheck -->
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/icheck.min.js"></script>
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/custom.js"></script>
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/appraisal.js?v=<?= v_num() ?>"></script>
	 <script type="text/javascript">	 
    $(document).ready(function() {
		// $("body").on("click", ".md_t_h",function(){
		// window.location = base_url+"index.php/User/home_page";
	// });
		
    //var url = window.location;
	// var urlParams = new URLSearchParams(window.location.search);
	// var file=urlParams.get('a')
    // $('#sidebar-menu a[hrf="' + file + '"]').parent('li').addClass('current-page');
    // $('#sidebar-menu a[hrf="' + file + '"]').parent('li').parent('ul').slideDown().parent().addClass('active');

	// $("body").on("click", ".form_select",function(){
		// var t=$(this).attr('hrf');
	// window.location = base_url+"index.php/Form/load_view_f?a="+t;
	// });

    });
  </script>
  </body>
</html>