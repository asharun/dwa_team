<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
$file_nm=str_replace('.php','',basename(__FILE__));
$a_right1=str_replace(' ','_',$this->session->userdata('access'));
$access_str1=explode("|",$a_right1);

$a_right=$this->session->userdata('access');
$access_str=explode("|",$a_right);
// print_r($access_str1);
// print_r($file_nm);
if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

$file_nm=str_replace('.php','',basename(__FILE__));
//echo '<pre>';
//print_r($moreInsightData);
 ?>

<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
	<meta name="google" content="notranslate">
   
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	  <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/forms/custom.css" rel="stylesheet">
	  <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/forms/purple.css" rel="stylesheet">

  </head>
  <body>
<body class="nav-md">
  <div class="container body">
    <div class="main_container">
        <?php
        $data['file_nm']=$file_nm;

        $this->load->view("common/Appraisal_Sidebar_Q.php", $data);
		$this->load->view("common/Appraisal_Topbar.php", $data);
        ?>


      <!-- page content -->
      <?php
         
       // print_r($desc);
      //   $desc=['q1'=>array('p1'=>'ans','p2'=>'ans'),
      //           'q2'=>array('p1'=>'ans2','p2'=>'null')
      //          ];
              $cnt=0;
      ?>
	    <div class="right_col" role="main">
        <div class="x_panel">
          <div class="page-title">
            <div class="col-md-9"><h3><?php echo "Feedback Analysis Platform" ?></h3></div>
              <div class="col-md-3"><?php
              if($AppraisalUserList) {
                     $userid=($this->input->get('us'));
                echo "<select class='selectpicker dec_select form-control' id='Userappraisal_id' title='Nothing Selected' data-live-search='true'>";
                  foreach($AppraisalUserList AS $v)
                  {
                          $sel='';
                        if($userid==$v['user_id'])
                        {
                          $sel='selected';
                        }
                    echo "<option  value='".$v['user_id']."' ".$sel.">".$v['full_name']."</option>";
                  }
                  echo "</select>";
                  }

               ?>
              </div>
            </div>
            <div class="title_left">

          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="panel panel-primary">
                  <div class="panel-heading">User Detail</div>
                  <div class="panel-body"><span></span>
                    <div class="f_resp_section" sec_r_cd="" sec_cd="" num="null" id="fs_resp_null">
                        <div class="row">
                          <div class="col-md-3">
                            <label class="l_font_fix_4 form-check-label">Employee Name : </label>
                            <h6 class="h6_sty"><?php $name=($AppraisalUser)?  $AppraisalUser[0]['full_name']:'';echo $name;   ?></h6>
                          </div>
                        <div class="col-md-3">
                          <label class="l_font_fix_4 form-check-label">TNL ID : </label>
                          <h6 class="h6_sty"><?php $empid=($AppraisalUser)?  $AppraisalUser[0]['emp_id']:'';echo $empid;   ?></h6>
                       </div>
                        <div class="col-md-3">
                          <label class="l_font_fix_4 form-check-label">Email id : </label>
                          <h6 class="h6_sty"><?php $useremail=($AppraisalUser)?  $AppraisalUser[0]['user_login_name']:'';echo $useremail;   ?></h6>
                       </div>
                       <div class="col-md-3">
                          <label class="l_font_fix_4 form-check-label">Reporting Manager : </label>
                          <h6 class="h6_sty"><?php $managername=($AppraisalUser)?  $AppraisalUser[0]['managername']:'';echo $managername;   ?></h6>
                       </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
		  <!--div class='row'>
			<div class='col-md-3'>
				<button class='btn add_but blue_but ecpo_opt' type='button'>Export All Data</button>
			</div>
		  </div-->
          <div class="" id="cont_replace">
		  
              <ul class="nav nav-tabs">
			  <?php if($projectsData)
					  {
						 echo '<li class="nav"><a data-toggle="tab" class="kl_choo" href="#sectionA"><strong>Projects</strong></a></li>';
					  }
					  if($academicsData)
					  {
						echo '<li class="nav"><a data-toggle="tab" class="kl_choo" href="#sectionB"><strong>Academics</strong></a></li>';
					  }
					  if($moreInsightData)
					  {
						echo '<li class="nav"><a data-toggle="tab" class="kl_choo" href="#sectionC"><strong>Insight</strong></a></li>';
					  }
					  if($feedbackForEmployee)
					  {
                 echo '<li class="nav"><a data-toggle="tab" class="kl_choo" href="#sectionD"><strong>Feedback For Employee</strong></a></li>';
					  }
					   if($feedbackForPoRm)
					  {
                 echo '<li class="nav"><a data-toggle="tab" class="kl_choo" href="#sectionE"><strong>Feedback for PO/RM</strong></a></li>';
					  } 
					  if($feedbackFromEmp)
					  {
                 echo '<li class="nav"><a data-toggle="tab" class="kl_choo" href="#sectionF"><strong>Feedback from Members</strong></a></li>';
					  }
					  if($feedbackgiven)
					  {
                 echo '<li class="nav"><a data-toggle="tab" class="kl_choo" href="#sectionG"><strong>Feedback given to Members</strong></a></li>';
					  }
			  
				 ?>
              </ul>
             <div class="tab-content">			  
			  <div id="sectionA" class="tab-pane fade">
                  <section class="accordion-section clearfix mt-3" aria-label="Question Accordions">
                    <div class="container">
                       <h2></h2>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						
						 <table class="display table table-bordered table-responsive fancy" id='table9' data-show-export="true">
                                <thead> 
									<tr>
                                        <th class="name_ques">Questions</th>
                                        <?php foreach($projectsData as $key => $sec_resp_cd):  ?>
										 <?php foreach($sec_resp_cd as $ssnm => $project_nm):  ?>
										
										 <?php foreach($project_nm as $pr_nm => $project):  ?>
                                            <?php foreach($project as $projectKey => $owner):  ?>
                                                <?php foreach($owner as $ownerkey => $answer):  ?>
                                                   <th> <label class="l_font_fix_3"><?php echo $projectKey; ?></label><br>
                                                    </th>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
											<?php endforeach; ?>
											<?php endforeach; ?>
                                        <?php break; endforeach; ?>
									</tr>
                                </thead>
                                <tbody>
                                    <?php 
									//print_r($feedbackForPoRm);
									foreach($projectsData as $key => $sec_resp_cd):  ?>
                                    <tr>
                                    <td><?php echo $ques_final_comb[$key]; ?></td>
									<?php foreach($sec_resp_cd as $ssnm => $project_nm):  ?>
									 <?php foreach($project_nm as $pr_nm => $project):  ?>
                                    <?php foreach($project as $projectKey => $owner):  ?>
                                        <?php foreach($owner as $ownerkey => $answer):  ?>
                                            <td>
                                            <?php 
											if($answer[0])
											{
													if($answer[0]=="type_3" || $answer[0]=="type_4")
													{
														$ch_tr=explode(",",$answer[1]);
														asort($ch_tr);
													//	print_r($ch_tr);
														$sub_Q=explode(",",$answer[2]);
														foreach($ch_tr AS $kk=>$nsd)
														{
														 echo "<label class='label label-success'>".$option_values[$value['opt_code']][$nsd]."</label> - ".$sub_Q[$kk]."<br>";
														}
													}else{
														echo $option_values[$answer[0]][$answer[1]];
													}
											}else{
												echo $answer[1];
											}
											?></td>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
									<?php endforeach; ?>
									<?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table> 
                        </div>
                   </div>
                </section>
              </div>
			  
              <div id="sectionB" class="tab-pane fade">
                  <section class="accordion-section clearfix mt-3" aria-label="Question Accordions">
                    <div class="container">
                       <h2></h2>
                   <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php
                      if($academicsData){
						  //print_r($academicsData);
                     foreach ( $academicsData  as  $key=>$val){?>
                         <div class="panel panel-default">
                        <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
                        <h3 class="panel-title">
                          <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $cnt;?>" aria-expanded="true" aria-controls="collapse0">
                          <?php  echo $key; ?>
                          </a>
                        </h3>
                        </div>
                        <div id="collapse<?php echo $cnt;?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0">
                            <div class="panel-body px-3 mb-4">

                              <!-- <h2 class="tab-section-heading"><?php  echo $k; ?></h2> -->
                              <p> <?php  echo $val; ?></p>


                          </div>
                        </div>
                      </div>
                        <?php $cnt++;  }
                     }else{  ?>
                        <div class="panel panel-default">
                        <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
                        <h3 class="panel-title">
                          <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $cnt;?>" aria-expanded="true" aria-controls="collapse0">
                          <?php  echo 'Have you taken up teaching responsibilities in the academic year 2017-18?'; ?>
                          </a>
                        </h3>
                        </div>
                        <div id="collapse<?php echo $cnt;?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0">
                            <div class="panel-body px-3 mb-4">

                              
                              <p> <?php  echo "No"; ?></p>


                          </div>
                        </div>
                      </div>
                   <?php  $cnt++; } ?>
                </section>
              </div>
              <div id="sectionC" class="tab-pane fade">
                  <section class="accordion-section clearfix mt-3" aria-label="Question Accordions">
                    <div class="container">
                       <h2></h2>
                   <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php
                    $i=0;
                    foreach ( $moreInsightData as $key => $value){?>
                         <div class="panel panel-default">
                        <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
                        <h3 class="panel-title">
                          <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $cnt;?>" aria-expanded="true" aria-controls="collapse0">
                          <?php  echo $value['ques_name']; ?>
                          </a>
                        </h3>
                        </div>
                        <div id="collapse<?php echo $cnt;?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0">
                            <div class="panel-body px-3 mb-4">
                              <?php  //foreach($value as $k=>$val) {?>
                              <h2 class="tab-section-heading"></h2>
                              <p> <?php 
											if($value['opt_code'])
											{
													if($value['opt_code']=="type_3" || $value['opt_code']=="type_4")
													{
														$ch_tr=explode(",",$value['ans']);
														asort($ch_tr);
													//	print_r($ch_tr);
														$sub_Q=explode(",",$value['ques_tbl_val']);
														if($answer[0]=="type_3")
														{
															echo "<label class='label label-danger'>Least</label> to <label class='label label-success'>Most</label><br>";
														}
														foreach($ch_tr AS $kk=>$nsd)
														{
														echo $sub_Q[$kk]."<br>";
														//echo "<span><label class='label label-success'>".$option_values[$answer[0]][$nsd]."</label> - ".$sub_Q[$kk]."</span><br>";
														}
													}else{
														echo $option_values[$value['opt_code']][$value['ans']];
													}
											}else{
												echo $value['ans'];
											} ?></p>
                          </div>
                        </div>
                      </div>
           
                       <?php $cnt++;  }?>
                </section>
              </div>

              <div id="sectionD" class="tab-pane fade">
                  <section class="accordion-section clearfix mt-3" aria-label="Question Accordions">
                    <div class="container">
                       <h2></h2>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						
						 <table class="display table table-bordered table-responsive fancy" id='table4' data-show-export="true">
                                <thead> 
									<tr>
                                        <th class="name_ques">Questions</th>
                                        <?php foreach($feedbackForEmployee as $key => $sec_resp_cd):  ?>
										 <?php foreach($sec_resp_cd as $ssnm => $project_nm):  ?>
										
										 <?php foreach($project_nm as $pr_nm => $project):  ?>
                                            <?php foreach($project as $projectKey => $owner):  ?>
                                                <?php foreach($owner as $ownerkey => $answer):  ?>
                                                   <th> <label class="l_font_fix_3"><?php echo $projectKey. '</label> <span>( <label class="label label-success">'.strtoupper(substr($ssnm,0,2))."</label> )</span>"; ?><br>
                                                        <label class="l_font_fix_4"><?php echo $ownerkey; ?></label><br>
                                                    </th>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
											<?php endforeach; ?>
											<?php endforeach; ?>
                                        <?php break; endforeach; ?>
									</tr>
                                </thead>
                                <tbody>
                                    <?php 
									//print_r($feedbackForPoRm);
									foreach($feedbackForEmployee as $key => $sec_resp_cd):  ?>
                                    <tr>
                                    <td><?php echo $ques_final_comb[$key]; ?></td>
									<?php foreach($sec_resp_cd as $ssnm => $project_nm):  ?>
									 <?php foreach($project_nm as $pr_nm => $project):  ?>
                                    <?php foreach($project as $projectKey => $owner):  ?>
                                        <?php foreach($owner as $ownerkey => $answer):  ?>
                                            <td>
                                            <?php 
											if($answer[0])
											{
													if($answer[0]=="type_3" || $answer[0]=="type_4")
													{
														$ch_tr=explode(",",$answer[1]);
														asort($ch_tr);
													//	print_r($ch_tr);
														$sub_Q=explode(",",$answer[2]);
														if($answer[0]=="type_3")
														{
															echo "<label class='label label-danger'>Least</label> to <label class='label label-success'>Most</label><br>";
														}
														foreach($ch_tr AS $kk=>$nsd)
														{
														echo $sub_Q[$kk]."<br>";
														//echo "<span><label class='label label-success'>".$option_values[$answer[0]][$nsd]."</label> - ".$sub_Q[$kk]."</span><br>";
														}
													}else{
														echo $option_values[$answer[0]][$answer[1]];
													}
											}else{
												echo $answer[1];
											}
											?></td>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
									<?php endforeach; ?>
									<?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table> 
                        </div>
                   </div>
                </section>
              </div>

              <div id="sectionE" class="tab-pane fade">
                  <section class="accordion-section clearfix mt-3" aria-label="Question Accordions">
                    <div class="container">
                       <h2></h2>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					
                            <table class="display table table-bordered table-responsive fancy" id='table3' data-show-export="true">
                                <thead> 
									<tr>
                                        <th class="name_ques">Questions</th>
                                        <?php foreach($feedbackForPoRm as $key => $sec_resp_cd):  ?>
										 <?php foreach($sec_resp_cd as $ssnm => $project_nm):  ?>
										
										 <?php foreach($project_nm as $pr_nm => $project):  ?>
                                            <?php foreach($project as $projectKey => $owner):  ?>
                                                <?php foreach($owner as $ownerkey => $answer):  ?>
                                                   <th> <label class="l_font_fix_3"><?php echo $projectKey. '</label> <span>( <label class="label label-success">'.strtoupper(substr($ssnm,1))."</label> )</span>"; ?><br>
                                                        <label class="l_font_fix_4"><?php echo $ownerkey; ?></label><br>
                                                    </th>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
											<?php endforeach; ?>
											<?php endforeach; ?>
                                        <?php break; endforeach; ?>
									</tr>
                                </thead>
                                <tbody>
                                    <?php 
									//print_r($feedbackForPoRm);
									foreach($feedbackForPoRm as $key => $sec_resp_cd):  ?>
                                    <tr>
                                    <td><?php echo $ques_final_comb[$key]; ?></td>
									<?php foreach($sec_resp_cd as $ssnm => $project_nm):  ?>
									 <?php foreach($project_nm as $pr_nm => $project):  ?>
                                    <?php foreach($project as $projectKey => $owner):  ?>
                                        <?php foreach($owner as $ownerkey => $answer):  ?>
                                            <td>
                                            <?php 
											if($answer[0])
											{
													if($answer[0]=="type_3" || $answer[0]=="type_4")
													{
														$ch_tr=explode(",",$answer[1]);
														asort($ch_tr);
													//	print_r($ch_tr);
														$sub_Q=explode(",",$answer[2]);
														if($answer[0]=="type_3")
														{
															echo "<label class='label label-danger'>Least</label> to <label class='label label-success'>Most</label><br>";
														}
														foreach($ch_tr AS $kk=>$nsd)
														{
														echo $sub_Q[$kk]."<br>";
														//echo "<span><label class='label label-success'>".$option_values[$answer[0]][$nsd]."</label> - ".$sub_Q[$kk]."</span><br>";
														}
													}else{
														echo $option_values[$answer[0]][$answer[1]];
													}
											}else{
												echo $answer[1];
											}
											?></td>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
									<?php endforeach; ?>
									<?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                   </div>
                </section>
              </div>

			  <div id="sectionF" class="tab-pane fade">
                  <section class="accordion-section clearfix mt-3" aria-label="Question Accordions">
                    <div class="container">
                       <h2></h2>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					
                            <table class="display table table-bordered table-responsive fancy" id='table5' data-show-export="true">
                                <thead> 
									<tr>
                                        <th class="name_ques">Questions</th>
                                        <?php foreach($feedbackFromEmp as $key => $sec_resp_cd):  ?>
										 <?php foreach($sec_resp_cd as $ssnm => $project_nm):  ?>
										
										 <?php foreach($project_nm as $pr_nm => $project):  ?>
                                            <?php foreach($project as $projectKey => $owner):  ?>
                                                <?php foreach($owner as $ownerkey => $answer):  ?>
                                                   <th> <label class="l_font_fix_3"><?php echo $projectKey. '</label> <span>( <label class="label label-success">'.strtoupper(substr($ssnm,1))."</label> )</span>"; ?><br>
                                                        <label class="l_font_fix_4"><?php echo $ownerkey; ?></label><br>
                                                    </th>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
											<?php endforeach; ?>
											<?php endforeach; ?>
                                        <?php break; endforeach; ?>
									</tr>
                                </thead>
                                <tbody>
                                    <?php 
									//print_r($feedbackForPoRm);
									foreach($feedbackFromEmp as $key => $sec_resp_cd):  ?>
                                    <tr>
                                    <td><?php echo $ques_final_comb[$key]; ?></td>
									<?php foreach($sec_resp_cd as $ssnm => $project_nm):  ?>
									 <?php foreach($project_nm as $pr_nm => $project):  ?>
                                    <?php foreach($project as $projectKey => $owner):  ?>
                                        <?php foreach($owner as $ownerkey => $answer):  ?>
                                            <td>
                                            <?php 
											if($answer[0])
											{
													if($answer[0]=="type_3" || $answer[0]=="type_4")
													{
														$ch_tr=explode(",",$answer[1]);
														asort($ch_tr);
													//	print_r($ch_tr);
														$sub_Q=explode(",",$answer[2]);
														if($answer[0]=="type_3")
														{
															echo "<label class='label label-danger'>Least</label> to <label class='label label-success'>Most</label><br>";
														}
														foreach($ch_tr AS $kk=>$nsd)
														{
														echo $sub_Q[$kk]."<br>";
														//echo "<span><label class='label label-success'>".$option_values[$answer[0]][$nsd]."</label> - ".$sub_Q[$kk]."</span><br>";
														}
													}else{
														echo $option_values[$answer[0]][$answer[1]];
													}
											}else{
												echo $answer[1];
											}
											?></td>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
									<?php endforeach; ?>
									<?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                   </div>
                </section>
				</div>
				<div id="sectionG" class="tab-pane fade">
                  <section class="accordion-section clearfix mt-3" aria-label="Question Accordions">
                    <div class="container">
                       <h2></h2>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						
						 <table class="display table table-bordered table-responsive fancy" id='table42' data-show-export="true">
                                <thead> 
									<tr>
                                        <th class="name_ques">Questions</th>
                                        <?php foreach($feedbackgiven as $key => $sec_resp_cd):  ?>
										 <?php foreach($sec_resp_cd as $ssnm => $project_nm):  ?>
										
										 <?php foreach($project_nm as $pr_nm => $project):  ?>
                                            <?php foreach($project as $projectKey => $owner):  ?>
                                                <?php foreach($owner as $ownerkey => $answer):  ?>
                                                   <th> <label class="l_font_fix_3"><?php echo "Given for ".$projectKey. ' ( as '.strtoupper(substr($ssnm,0,2))." ) to </label>"; ?><br>
                                                        <label class="l_font_fix_4"><?php echo $ownerkey; ?></label><br>
                                                    </th>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
											<?php endforeach; ?>
											<?php endforeach; ?>
                                        <?php break; endforeach; ?>
									</tr>
                                </thead>
                                <tbody>
                                    <?php 
									//print_r($feedbackForPoRm);
									foreach($feedbackgiven as $key => $sec_resp_cd):  ?>
                                    <tr>
                                    <td><?php echo $ques_final_comb[$key]; ?></td>
									<?php foreach($sec_resp_cd as $ssnm => $project_nm):  ?>
									 <?php foreach($project_nm as $pr_nm => $project):  ?>
                                    <?php foreach($project as $projectKey => $owner):  ?>
                                        <?php foreach($owner as $ownerkey => $answer):  ?>
                                            <td>
                                            <?php 
											if($answer[0])
											{
													if($answer[0]=="type_3" || $answer[0]=="type_4")
													{
														$ch_tr=explode(",",$answer[1]);
														asort($ch_tr);
													//	print_r($ch_tr);
														$sub_Q=explode(",",$answer[2]);
														if($answer[0]=="type_3")
														{
															echo "<label class='label label-danger'>Least</label> to <label class='label label-success'>Most</label><br>";
														}
														foreach($ch_tr AS $kk=>$nsd)
														{
														echo $sub_Q[$kk]."<br>";
														//echo "<span><label class='label label-success'>".$option_values[$answer[0]][$nsd]."</label> - ".$sub_Q[$kk]."</span><br>";
														}
													}else{
														echo $option_values[$answer[0]][$answer[1]];
													}
											}else{
												echo $answer[1];
											}
											?></td>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
									<?php endforeach; ?>
									<?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table> 
                        </div>
                   </div>
                </section>
              </div>
			  
              
        </div>
         <hr style="border-top: 1px solid #813589;">
    </div>
  </div>
</div>

</div>
</div>

	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var arrayFromPHP1 = <?php echo json_encode($projectsData); ?>;</script>
	<script>var ss_section_id = '<?php echo $this->session->userdata('sec_id') ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/bootstrap-progressbar.min.js"></script>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/jquery.nicescroll.min.js"></script>
		  <!-- icheck -->
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/FileSaver.min.js?v=<?= v_num() ?>"></script>	
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/icheck.min.js"></script>
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/custom.js"></script>
          <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/appraisal_q.js?v=<?= v_num() ?>"></script>

	 <script type="text/javascript">
    $(document).ready(function() {
		$('body').find('.kl_choo:first').click();	
	
	function exportTableToCSV(filename) {
		var str='\n';
		$('body').find('.f_resp_section .col-md-3').each(function (index) {
			var la1=$(this).find("label").html();
			var la2=$(this).find(".h6_sty").html();
			if(index==0)
			{
			filename=la2+".csv";
			}
			str+=la1+'-'+la2 +',';
		});
		
		str+='\n\nAcademics\r';
	
	$('body').find('#sectionB .panel-default').each(function () {
			var la1=$(this).find(".collapsed").html().replace(/\s+/g, ' ');
			var la2=$(this).find(".panel-body>p").html().replace(/\s+/g, ' ');
			str+=',"'+la1+'","'+la2 +'"\r\n';
		});
		str+='\n\n';
		
		str+='\n\n Feedback for Projects \n';
    var rows = document.querySelectorAll("#table9 tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        var line = '';
        for (var j = 0; j < cols.length; j++) 
		{			
			line += ',';
			var mystr = cols[j].textContent.replace(/\s+/g, ' ');
			line += '"'+mystr+'"';
		}
        
		str += line + '\r\n';
    }
	
	
	str += '\r\n\n\n Feedback for PO/MR \n';
	
	var rows = document.querySelectorAll("#table3 tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        var line = '';
        for (var j = 0; j < cols.length; j++) 
		{			
			line += ',';
			var mystr = cols[j].textContent.replace(/\s+/g, ' ');
			line += '"'+mystr+'"';
		}
        
		str += line + '\r\n';
    }
	str+='\n\nMore Insights\r';
	
	$('body').find('#sectionC .panel-default').each(function () {
			var la1=$(this).find(".collapsed").html().replace(/\s+/g, ' ');
			var la2=$(this).find(".panel-body>p").html().replace(/\s+/g, ' ');
			str+=',"'+la1+'","'+la2 +'"\r\n';
		});
		str+='\n\n';
		
	// Download CSV file
	saveAs(new Blob([str]),filename);		
}

	 $('body').on('click','.ecpo_opt', function() {
	//da_re=ConvertToCSV(arrayFromPHP1);	
	exportTableToCSV("export.csv");
		
	 });

  $('body').on('change','#Userappraisal_id', function() {
        var user_id=$(this).val();
          window.location.href=base_url+'index.php/FormQ/Appraisal_response_q?r=1&us='+user_id
    });

	function fancyTable(table) {
    
    $(table).bootstrapTable({
        pagination: false,
		stickyHeader: true,
        escape: false
    });
}

	 $("table.fancy").each(function () {
        fancyTable(this);
    });
	
    });
  </script>
  </body>
</html>