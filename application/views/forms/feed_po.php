<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
  
$file_nm=str_replace('.php','',basename(__FILE__));
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

$file_nm=str_replace('.php','',basename(__FILE__));
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	  <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/forms/custom.css" rel="stylesheet">
  <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/forms/purple.css" rel="stylesheet">
	
  </head>
  <body>
<body class="nav-md">

  <div class="container body">


    <div class="main_container">
<?php
        $data['file_nm']=$file_nm;
        $this->load->view("common/Appraisal_Sidebar.php", $data);
		$this->load->view("common/Appraisal_Topbar.php", $data);
        ?>
      
      

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3><?php echo "Feedback Platform" ?></h3>
            </div>
          </div>
          <div class="clearfix"></div>
			<div class="row">
            <div class="col-xl-3 col-md-3">
								<div class="card bg-c-lite-green update-card">
                                                    <div class="card-block">
                                                        <div class="row align-items-end">
                                                            <div class="col-md-12">
                                                                <h4 class="text-white"><?php echo $fetch_f_cnt[0]['tot'];?></h4>
                                                                <h6 class="text-white m-b-0">Total Feedback Assigned</h6>
                                                            </div>
															
                                                            
											</div>
										</div>
										
									</div>
						</div>
						<div class="col-xl-3 col-md-3">
								<div class="card bg-c-green update-card">
                                                    <div class="card-block">
                                                        <div class="row align-items-end">
                                                            <div class="col-md-12">
                                                                <h4 class="text-white"><?php echo $fetch_f_cnt[0]['comp'];?></h4>
                                                                <h6 class="text-white m-b-0">#Feedbacks Submitted</h6>
                                                            </div>
															
                                                            
											</div>
										</div>
										
									</div>
						</div>
					</div>
												
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
			 <div class="x_panel">
			<?php
			
			echo '<div class="row" id="freez_top">';
				echo '<div class="col-md-4">';
				echo "<label class='l_font_fix_4'>Choose Employee:</label>";
					echo "<select class='selectpicker po_looo form-control' title='Nothing Selected' data-live-search='true'>";
									
										$emp_id = $this->session->userdata('fb_emp_id');
										$pro_id = $this->session->userdata('fb_pro_id');
									foreach($user_feeds AS $v)
									{
										$ab='';
										if($v['val_id']==$emp_id)
										{
											$ab='selected';
										}
										echo "<option ".$ab." value='".$v['val_id']."'>".$v['val_name']."</option>";
									}
									echo "</select>";
				echo "</div>";
				echo '<div class="col-md-4">';
				echo "<label class='l_font_fix_4'>Choose Project:</label>";
					echo "<select class='selectpicker polo form-control' id='pro_looo' title='Nothing Selected' data-live-search='true'>";
									
									foreach($pro_feeds AS $v)
									{
										$ab='';
										//print_r($pro_id);
										if($v['val_id']==$pro_id)
										{
											$ab='selected';
										}
										echo "<option ".$ab." value='".$v['val_id']."'>".$v['val_name']."</option>";
									}
									echo "</select>";
				echo "</div>";
				echo '<div class="col-md-2">';
				echo "<label class='l_font_fix_4 invisible'>Choose:</label>";
					 echo '<a class="btn btn_click sub_emp_po" style="width:100;">Give Feedback</a>';
				echo "</div>";				
			echo "</div>";
								
echo '<div class="row text-center">';
				echo '<div class="col-md-2 col-md-offset-5">';
				echo "<label class='l_font_fix_4 invisible'>Choose:</label>";
					 echo '<a class="btn btn_click save_cont" style="width:100;">Give AnotherFeedback</a>';
				echo "</div>";	
					echo "</div>";			
				?>
				
               <div class="row" id="cont_replace">
			   </div>
                  <!-- SmartWizard Content -->
                </div>
              </div>
            </div>

          </div>
        </div>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/bootstrap-progressbar.min.js"></script>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/jquery.nicescroll.min.js"></script>
		  <!-- icheck -->
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/icheck.min.js"></script>
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/custom.js"></script>
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/appraisal.js?v=<?= v_num() ?>"></script>
	 <script type="text/javascript">
	 
    $(document).ready(function() {
		
	// $.ajax({
       // url: base_url+"index.php/Form/load_v_ins?a=feed_po&s_id="+13,
       // success: function(data){
		   // $('#cont_replace').html(data);
		   // $(".selectpicker").selectpicker();
       // },
       // error: function(e) {
           // console.log(e.responseText);
       // }
    // });
	
	// $("body").on("click", ".md_t_h",function(){
		// window.location = base_url+"index.php/User/home_page";
	// });
		
		var wordLen = 1500,
		w_len; // Maximum word length
$('body').on('keydown','.ans_txt_list',function(event) {	
	w_len = $(this).val().split(/[\s]+/);
	//alert(w_len);
	if (w_len.length > wordLen) { 
		if ( event.keyCode == 46 || event.keyCode == 8 ) {// Allow backspace and delete buttons
    } else if (event.keyCode < 48 || event.keyCode > 57 ) {//all other buttons
    	event.preventDefault();
    }
	}
});

		
		var em_pre=$("body").find(".selectpicker.po_looo").val();
		var pro_pre=$("body").find("#pro_looo").val();
		//alert(pro_pre);
		if(em_pre && (pro_pre==0 || pro_pre!='')?0:pro_pre)
		{
			$("body").find("#freez_top").addClass('disabled_sec');
			var C=[];
			C[0] = $(this).closest('.sec_r_div').attr('sec_resp_id');
			C[1]=em_pre;
			C[2]=pro_pre;
			C[3] = $(".u_in").attr('user');
			C[4]=13;
			C[12] = $("body").find("#sel_slot_1").val();
			$.post(base_url + "index.php/Form/fd_sec_response_mst", {
            C: C
			}, function(data, textStatus) {
            if (textStatus == 'success') {
					$.ajax({
			   url: base_url+"index.php/Form/load_feed_ins?a=feed_po&emp_id="+em_pre+"&pro="+pro_pre,
			   success: function(data){
				   $('#cont_replace').html(data);				   
			   },
			   error: function(e) {
				   console.log(e.responseText);
			   }
			});
            }
              
        });
			 
		}
		
$("body").on("change",".po_looo", function() {
	if($(this).val())
	{
		var em=$(this).val();
//		var pro=$("body").find(".pro_looo").val();
		 $.ajax({
		   url: base_url+"index.php/Form/load_pros?a=feed_po&emp_id="+em,
		   success: function(data){
			   $('#pro_looo').html(data).selectpicker('refresh');  
		   },
		   error: function(e) {
			   console.log(e.responseText);
		   }
		});
	}
});

$('body').on('click', '.sub_emp_po', function() {
	$("body").find("#freez_top").addClass('disabled_sec');
		var em=$("body").find(".selectpicker.po_looo").val();
		var pro=$("body").find("#pro_looo").val();
		//alert(pro);
		if(em && (pro==0 && pro!='')?1:pro)
		{
			var C=[];
			C[0] = $(this).closest('.sec_r_div').attr('sec_resp_id');
			C[1]=em;
			C[2]=pro;
			C[3] = $(".u_in").attr('user');
			C[4]=13;
			C[12] = $("body").find("#sel_slot_1").val();
			$.post(base_url + "index.php/Form/fd_sec_response_mst", {
            C: C
			}, function(data, textStatus) {
            if (textStatus == 'success') {
					$.ajax({
			   url: base_url+"index.php/Form/load_feed_ins?a=feed_po&emp_id="+em+"&pro="+pro,
			   success: function(data){
				   $('#cont_replace').html(data);				   
			   },
			   error: function(e) {
				   console.log(e.responseText);
			   }
			});
            }
              
        });
			 
		}else{
			alert("Choose the correct options!");
		}
});
	
	
		var timeoutId;
		
		$('body').on('click', '.sub_forrrm', function() {
			var dis_ele=$(this);
	$(dis_ele).attr("disabled", true);
			var req=$('body').find('.required').length;		
		var done=0;
		$('body').find('.required').each(function(index) {
			q_id=$(this).closest(".cus_div").attr('ques_resp_id');				
				if(q_id)
				{
					var ele = $(this).closest(".cus_div").find('table');	
					if(ele)
					{						
					var tc=0;
						$(ele).find('.cc_row').each(function(index){
						var th=$(this).find('.ans_tbl_list:checked').attr('t_val');
						if(!(th)) {
							 tc++;
							}
						});	
						if(tc==0)
						{
							done++;
						}
					}else{						
					done++;
					}
				}
			});
		if(done==req)
		{
			if (confirm('Are you sure you want to submit?')) {
			var em=$("body").find(".selectpicker.po_looo").val();
			var pro=$("body").find("#pro_looo").val();
			var C = [];
			C[1]="feed_po";
			C[2] = $(".u_in").attr('user');
			C[3]=em;
			C[4]=pro;
			C[12] = $("body").find("#sel_slot_1").val();
			$.post(base_url + "index.php/Form/fb_form_response_mst", {
				C: C
			}, function(data, textStatus) {
				if (textStatus == 'success') {
					window.location = base_url+"index.php/Form/load_view_f?a=feed_po";
				} else {
				  location.reload();
				}
			});
			}
		}else{
			alert("Please fill the required questions!");
			$(dis_ele).attr("disabled", false);
		}
			
		
		});
		
		
$('body').on('change', '.ans_list', function() {
    $(this).attr("disabled", true);
	var em=$("body").find(".selectpicker.po_looo").val();
		var pro=$("body").find("#pro_looo").val();
		//alert(pro);
		
    var ele = $(this);
    var C = [];
    C[0] = $(this).closest('.cus_div').attr('ques_resp_id');
    C[1] = $(this).closest('.f_section').attr('sec_id');
    C[2] = $(this).closest('.cus_div').attr('ques_id');
    C[3] = $(".u_in").attr('user');
    C[4] = $(this).attr('t_val');
	C[6]='ques_typ_val';
	C[7] = $(this).closest('.cus_div').closest('.f_resp_section').attr('sec_cd');
	C[8]=$(this).closest('.cus_div').closest('.f_resp_section').attr('num');
	C[9]=em;
	C[10]=pro;
	C[12] = $("body").find("#sel_slot_1").val();
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function() {
        $.post(base_url + "index.php/Form/fd_ques_response_mst", {
            C: C
        }, function(data, textStatus) {
            if (textStatus == 'success') {
                if (!C[0]) {
                    $hj = $.trim(data);
                    $(ele).closest('.cus_div').attr('ques_resp_id', $hj);
                }
                $(ele).attr("disabled", false);

            } else {
              location.reload();
            }
        });
    }, 0);
});
	

$('body').on('change', '.ans_txt_list', function() {
	var content=$(this).val();
	var rt=$(this).closest('.cus_div').find('.required').length;
	//console.log(rt);
	if(content.length<1 && rt)
   {
        window.alert("This field cant be left empty!");
        event.stopImmediatePropagation();
   }
   else
   {
	 $(this).attr("disabled", true);
	var em=$("body").find(".selectpicker.po_looo").val();
		var pro=$("body").find("#pro_looo").val();
    var ele = $(this);
    var C = [];
    C[0] = $(this).closest('.cus_div').attr('ques_resp_id');
    C[1] = $(this).closest('.f_section').attr('sec_id');
    C[2] = $(this).closest('.cus_div').attr('ques_id');
    C[3] = $(".u_in").attr('user');
    C[4] = $(this).val();
	C[6]='ques_resp_desc';
	C[7] = $(this).closest('.cus_div').closest('.f_resp_section').attr('sec_cd');
	C[8]=$(this).closest('.cus_div').closest('.f_resp_section').attr('num');
	C[9]=em;
	C[10]=pro;
	C[12] = $("body").find("#sel_slot_1").val();
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function() {
        $.post(base_url + "index.php/Form/fd_ques_response_mst", {
            C: C
        }, function(data, textStatus) {
            if (textStatus == 'success') {
                if (!C[0]) {
                    $hj = $.trim(data);
                    $(ele).closest('.cus_div').attr('ques_resp_id', $hj);
                }
                $(ele).attr("disabled", false);

            } else {
              location.reload();
            }
        });
    }, 0);
   }
});
		
$('body').on('change', '.ans_tbl_list', function() {
	var el = $(this);
   var col = el.attr("t_val");   
   
     $(this).closest('table').addClass('disabled_sec');
	var em=$("body").find(".selectpicker.po_looo").val();
		var pro=$("body").find("#pro_looo").val();
    var ele = $(this).closest('table');
	$(ele).find("input[t_val=" + col + "]").prop("checked", false);
   el.prop("checked", true);  
   
	var tc='';
	$(ele).find('.cc_row').each(function(index){
		var th=$(this).find('.ans_tbl_list:checked').attr('t_val');
		if(!(th)) {
			 tc=tc+',';
			}else{
				tc=tc+','+th;
			}
	});
	//console.log(tc);
    var C = [];
    C[0] = $(this).closest('.cus_div').attr('ques_resp_id');
    C[1] = $(this).closest('.f_section').attr('sec_id');
    C[2] = $(this).closest('.cus_div').attr('ques_id');
    C[3] = $(".u_in").attr('user');
    C[4] = tc.substring(1);
	C[6]='ques_typ_val';
	C[7] = $(this).closest('.cus_div').closest('.f_resp_section').attr('sec_cd');
	C[8]=$(this).closest('.cus_div').closest('.f_resp_section').attr('num');
	C[9]=em;
	C[10]=pro;
	C[12] = $("body").find("#sel_slot_1").val();
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function() {

        $.post(base_url + "index.php/Form/fd_ques_response_mst", {
            C: C
        }, function(data, textStatus) {
            if (textStatus == 'success') {
                if (!C[0]) {
                    $hj = $.trim(data);
                    $(ele).closest('.cus_div').attr('ques_resp_id', $hj);
                }
                $(ele).closest('table').removeClass('disabled_sec');

            } else {
               location.reload();
            }
        });
    }, 0);

});

$("body").on("click", ".save_cont",function(){
	$("body").find("#freez_top").removeClass('disabled_sec');
	$("#cont_replace").html('');
});		
		
/* Sidebar Menu active class */

    //var url = window.location;
	var urlParams = new URLSearchParams(window.location.search);
	var file=urlParams.get('a')
    $('#sidebar-menu a[hrf="' + file + '"]').parent('li').addClass('current-page');
    $('#sidebar-menu a[hrf="' + file + '"]').parent('li').parent('ul').slideDown().parent().addClass('active');

	// $("body").on("click", ".form_select",function(){
		// var t=$(this).attr('hrf');
	// window.location = base_url+"index.php/Form/load_view_f?a="+t;
	// });
	
	
     $(".selectpicker").selectpicker();
    });
  </script>
  </body>
</html>