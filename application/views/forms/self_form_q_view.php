<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
  
$file_nm='self_form';
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

$file_nm=str_replace('.php','',basename(__FILE__));
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	  <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/forms/custom.css" rel="stylesheet">
  <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/forms/purple.css" rel="stylesheet">	
  <style> input[type=checkbox], input[type=radio]{
	  pointer-events:none;
	  }
	.form-control
	{
		pointer-events:none;
	}
</style>
  </head>
  <body>
<body class="nav-md">
  <div class="container body">
    <div class="main_container">
	<?php
        $data['file_nm']=$file_nm;
        $this->load->view("common/Appraisal_Sidebar_Q.php", $data);
		$this->load->view("common/Appraisal_Topbar.php", $data);
        ?>
     
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3><?php echo "Feedback Platform" ?></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel" id="cont_replace">
                  <!-- SmartWizard Content -->
                </div>
              </div>
            </div>

          </div>
        </div>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var ss_section_id = '<?php echo $this->session->userdata('sec_id') ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/bootstrap-progressbar.min.js"></script>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/jquery.nicescroll.min.js"></script>
		  <!-- icheck -->
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/icheck.min.js"></script>
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/custom.js"></script>
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/appraisal_q.js?v=<?= v_num() ?>"></script>
	 <script type="text/javascript">
	 
    $(document).ready(function() {
		
	$.ajax({
       url: base_url+"index.php/FormQ/load_view_full_form?a=self_form_q",
       success: function(data){
		   $('#cont_replace').html(data);
		   $(".selectpicker").selectpicker();
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });
	
		// $("body").on("click", ".md_t_h",function(){
		// window.location = base_url+"index.php/User/home_page";
	// });
		

    //var url = window.location;
	var urlParams = new URLSearchParams(window.location.search);
	var file="self_form"
    $('#sidebar-menu a[hrf="' + file + '"]').parent('li').addClass('current-page');
    $('#sidebar-menu a[hrf="' + file + '"]').parent('li').parent('ul').slideDown().parent().addClass('active');

	// $("body").on("click", ".form_select",function(){
		// var t=$(this).attr('hrf');
	// window.location = base_url+"index.php/FormQ/load_view_f?a="+t;
	// });
	
	$.fn.gotoAnchor = function(anchor) {
    location.href = this.selector;
}
	$("body").on("click", ".s_click",function(){		
			var t=$(this).find('.step_no').attr('id');		
			t = t.replace("step-", "");
			$('#fs_'+t).gotoAnchor();
	});
	
	
    });
  </script>
  </body>
</html>