<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
  
$file_nm=str_replace('.php','',basename(__FILE__));
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}

$file_nm=str_replace('.php','',basename(__FILE__));
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	  <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/forms/custom.css" rel="stylesheet">
  <link rel="stylesheet" href="<?= getAssestsUrl() ?>css/forms/purple.css" rel="stylesheet">
	
  </head>
  <body>
<body class="nav-md">

  <div class="container body">


    <div class="main_container">
<?php
        $data['file_nm']=$file_nm;
        $this->load->view("common/Appraisal_Sidebar_Q.php", $data);
		
		$this->load->view("common/Appraisal_Topbar.php", $data);
        ?>
    

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3><?php echo "Feedback Platform" ?></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel" id="cont_replace">
                  <!-- SmartWizard Content -->
                </div>
              </div>
            </div>

          </div>
        </div>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var ss_section_id = '<?php echo $this->session->userdata('sec_id') ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/bootstrap-progressbar.min.js"></script>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/jquery.nicescroll.min.js"></script>
		  <!-- icheck -->
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/icheck.min.js"></script>
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/custom.js"></script>
		  <script type="text/javascript" src="<?= getAssestsUrl() ?>js/forms/appraisal_q.js?v=<?= v_num() ?>"></script>
	 <script type="text/javascript">
	 
	 ss_section_id=((ss_section_id>12)?1:ss_section_id);
    $(document).ready(function() {
	$.ajax({
       url: base_url+"index.php/FormQ/load_v_ins?a=self_form&s_id="+ss_section_id,
       success: function(data){
		   $('#cont_replace').html(data);
		   $(".selectpicker").selectpicker();
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });
	
		// $("body").on("click", ".md_t_h",function(){
		// window.location = base_url+"index.php/User/home_page";
	// });
		
		$("body").on("click", ".delete_sec",function(){
			
			if (confirm('Are you sure you want to delete this?')) {   
			 var ele=$(this).closest('.f_resp_section');
			 var tc='';
			$(ele).find('.cus_div').each(function(index){
				var th=$(this).attr('ques_resp_id');
				if(th) {
						tc=tc+','+th;
					}
			});
			 var ac='';
			$(ele).find('.sec_r_div').each(function(index){
				var th=$(this).attr('sec_resp_id');
				if(th) {
						ac=ac+','+th;
					}
			});
			var C = [];
			C[0]=tc.substr(1);
			C[1]=ac.substr(1);
			C[3] = $(".u_in").attr('user');
			C[12] = $("body").find("#sel_slot_1").val();
			$.post(base_url + "index.php/FormQ/del_ques_response_mst", {
				C: C
			}, function(data, textStatus) {
				if (textStatus == 'success') {
					$(ele).remove();
				} else {
				   location.reload();
				}
			});
		} else {
			// Do nothing!
		}
		//window.location = base_url+"index.php/User/home_page";
	});
				
		$("body").on("click", ".del_full_sec",function(){			
			if (confirm('Are you sure you want to delete this?')) {   
			 var ss_curr=parseInt($("body").find('.f_section').attr('sec_id'));
			 
			 if(ss_curr==6)
			 {
				ss_curr='6,7,8'; 
			 }
			 if(ss_curr==9)
			 {
				ss_curr='9,10,11'; 
			 }
			var C = [];
			C[0]=ss_curr;
			C[1] = $(".u_in").attr('user');
			C[12] = $("body").find("#sel_slot_1").val();
			$.post(base_url + "index.php/FormQ/del_full_sec_ques_mst", {
				C: C
			}, function(data, textStatus) {
				if (textStatus == 'success') {
					location.reload();
				} else {
				   location.reload();
				}
			});
		} else {
			// Do nothing!
		}
		//window.location = base_url+"index.php/User/home_page";
	});
		
		$("body").on("click", ".bt_skip",function(event){
			var t=$(this).attr('no_nav');
				$.ajax({
			   url: base_url+"index.php/FormQ/load_v_ins?a=self_form&s_id="+t,
			   success: function(data){
				   $('#cont_replace').html(data);
				   $(".selectpicker").selectpicker();
			   },
			   error: function(e) {
				   console.log(e.responseText);
			   }
			});			
		});
				
			$("body").on("click", ".same_mr",function(event){	
			var sme_elem=$(this);
			if (confirm('Are you sure you want to delete the answers and keep answers from Project I?')) {   
			 var ss_curr=parseInt($("body").find('.f_section').attr('sec_id'));
			var C = [];
			C[0]=ss_curr;
			C[1] = $(".u_in").attr('user');
			C[2]=$(sme_elem).attr('copy_section');
			C[3]=$(sme_elem).attr('copy_sec_cd');
			C[7] = $(sme_elem).closest('.f_resp_section').attr('sec_cd');
			C[8]=$(sme_elem).closest('.f_resp_section').attr('num');
			C[12] = $("body").find("#sel_slot_1").val();
			$.post(base_url + "index.php/FormQ/same_mr_ans", {
				C: C
			}, function(data, textStatus) {
				if (textStatus == 'success') {
					location.reload();
				} else {
				   location.reload();
				}
			});
		} else {
				event.preventDefault();
		}
		//window.location = base_url+"index.php/User/home_page";
	});
	

$("body").on("change",".selectpicker.po_looo", function(e) {
	//alert("sdfmks");
	if($(this).val())
	{
		var ele=$(this);
		var selected=$(this).val();
		var num=0;
	$("body").find(".selectpicker.po_looo").each(function(index){
		
		if(selected==$(this).val())
		{
			num=num+1;
		}
	});
	
	if(num>1)
	{
		alert("Same owner can't be chosen!");
		//e.stopImmediatePropagation();
		$(ele).selectpicker('val','');
	}
	}
});

var wordLen = 1500,
		w_len; // Maximum word length
$('body').on('keydown','.ans_txt_list',function(event) {	
	w_len = $(this).val().split(/[\s]+/);
	//alert(w_len);
	if (w_len.length > wordLen) { 
		if ( event.keyCode == 46 || event.keyCode == 8 ) {// Allow backspace and delete buttons
    } else if (event.keyCode < 48 || event.keyCode > 57 ) {//all other buttons
    	event.preventDefault();
    }
	}
});

	$("body").on("click", ".many_chk",function(event){	
				if($(this).val() && $(this).attr('t_val')==1)
				{
					var tpo=$("body").find(".selectpicker.po_looo").length;
			var tottt=0;
			var ele_sel=$(this).closest('.cus_div');
		$("body").find(".selectpicker.po_looo").each(function() { 
		 tottt= $(this).attr("siz");	
		});
			if(tpo<tottt)
				{
			var ele=$(this).closest('.f_resp_section');
			var ids = [];
				$("body").find(".f_resp_section").each(function () {
				  ids.push(parseInt($(this).attr('num'))); // ids.push(this.id) would work as well.
				});	
            // var num=Math.max(ids);	
			  var num=Math.max.apply(null, ids);
				//console.log(ids);
			var  ss_sec=$("body").find(".f_section").attr("sec_id");
			$.ajax({
			   url: base_url+"index.php/FormQ/load_many_pos?a=self_form&s_id="+ss_sec+"&cd="+(num+1),
			   success: function(data){
				   $(ele).after(data);
				   $(".selectpicker").selectpicker();
			   },
			   error: function(e) {
				   console.log(e.responseText);
			   }
			});
			}else{
				//console.log("jdn");
			alert("No more Project Owner exists!");
			$(ele_sel).find("input[t_val=2]").prop("checked", true);
			event.stopImmediatePropagation();
		}
		}
		
	});
	
		var timeoutId;
		
		$('body').on('click', '.sub_forrrm', function() {
			
			var dis_ele=$(this);
			$(dis_ele).attr("disabled", true);
			var req=$('body').find('.required').length;		
		var done=0;
		$('body').find('.required').each(function(index) {
			q_id=$(this).closest(".cus_div").attr('ques_resp_id');
			sd_id=$(this).closest(".sec_r_div").attr('sec_resp_id');
				if(q_id || sd_id)
				{
				 if(q_id)
					{
					var ele = $(this).closest(".cus_div").find('table');	
					if(ele)
					{						
					var tc=0;
						$(ele).find('.cc_row').each(function(index){
						var th=$(this).find('.ans_tbl_list:checked').attr('t_val');
						if(!(th)) {
							 tc++;
							}
						});	
						if(tc==0)
						{
							done++;
						}
					}else{						
					done++;
					}
					}
					if(sd_id)
					{
						done++;
					}
				}			
		});
		if(done==req)
		{
			if (confirm('Are you sure you want to submit?')) {		
			var C = [];
			C[1]="self_form";
			C[3] = $(".u_in").attr('user');
			C[12] = $("body").find("#sel_slot_1").val();
			$.post(base_url + "index.php/FormQ/ins_form_response_mst", {
				C: C
			}, function(data, textStatus) {
				if (textStatus == 'success') {
					window.location = base_url+"index.php/FormQ/load_view_f?a=self_form_q";
				} else {
				   location.reload();
				}
			});
			}
		}else{
			alert("Please fill the required questions!");
			$(dis_ele).attr("disabled", false);
		}
		});
		
		
$('body').on('change', '.ans_list', function() {
    $(this).attr("disabled", true);
	//console.log("asn_kist");
    var ele = $(this);
    var C = [];
    C[0] = $(this).closest('.cus_div').attr('ques_resp_id');
    C[1] = $(this).closest('.f_section').attr('sec_id');
    C[2] = $(this).closest('.cus_div').attr('ques_id');
    C[3] = $(".u_in").attr('user');
    C[4] = $(this).attr('t_val');
	C[6]='ques_typ_val';
	C[7] = $(this).closest('.cus_div').closest('.f_resp_section').attr('sec_cd');
	C[8]=$(this).closest('.cus_div').closest('.f_resp_section').attr('num');
	C[12] = $("body").find("#sel_slot_1").val();
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function() {

        $.post(base_url + "index.php/FormQ/ins_ques_response_mst", {
            C: C
        }, function(data, textStatus) {
            if (textStatus == 'success') {
                if (!C[0]) {
                    $hj = $.trim(data);
                    $(ele).closest('.cus_div').attr('ques_resp_id', $hj);
                }
                $(ele).attr("disabled", false);

            } else {
               location.reload();
            }
        });
    }, 0);
});	

$('body').on('change', '.ans_txt_list', function(event) {
	var content=$(this).val();
	var rt=$(this).closest('.cus_div').find('.required').length;
	if(content.length<1 && rt)
   {
        window.alert("This field cant be left empty!");
        event.stopImmediatePropagation();
   }
   else
   {
    $(this).attr("disabled", true);
    var ele = $(this);
    var C = [];
    C[0] = $(this).closest('.cus_div').attr('ques_resp_id');
    C[1] = $(this).closest('.f_section').attr('sec_id');
    C[2] = $(this).closest('.cus_div').attr('ques_id');
    C[3] = $(".u_in").attr('user');
    C[4] = $(this).val();
	C[6]='ques_resp_desc';
	C[7] = $(this).closest('.cus_div').closest('.f_resp_section').attr('sec_cd');
	C[8]=$(this).closest('.cus_div').closest('.f_resp_section').attr('num');
	C[12] = $("body").find("#sel_slot_1").val();
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function() {

        $.post(base_url + "index.php/FormQ/ins_ques_response_mst", {
            C: C
        }, function(data, textStatus) {
            if (textStatus == 'success') {
                if (!C[0]) {
                    $hj = $.trim(data);
                    $(ele).closest('.cus_div').attr('ques_resp_id', $hj);
                }
                $(ele).attr("disabled", false);

            } else {
               location.reload();
            }
        });
    }, 0);
   }
});
		
$('body').on('change', '.ans_tbl_list', function() {	
	var el = $(this);
   var col = el.attr("t_val");   
   
    $(this).closest('table').addClass('disabled_sec');
    var ele = $(this).closest('table');
	
	var cct=($(ele).find("input[t_val=" + col + "]:checked").length);
	if(cct>1)
	{
		
	}
	$(ele).find("input[t_val=" + col + "]").prop("checked", false);
   el.prop("checked", true);
	var tc='';
	$(ele).find('.cc_row').each(function(index){
		var th=$(this).find('.ans_tbl_list:checked').attr('t_val');
		if(!(th)) {
			 tc=tc+',';
			}else{
				tc=tc+','+th;
			}
	});
	//console.log(tc);
    var C = [];
    C[0] = $(this).closest('.cus_div').attr('ques_resp_id');
    C[1] = $(this).closest('.f_section').attr('sec_id');
    C[2] = $(this).closest('.cus_div').attr('ques_id');
    C[3] = $(".u_in").attr('user');
    C[4] = tc.substring(1);
	C[6]='ques_typ_val';
	C[7] = $(this).closest('.cus_div').closest('.f_resp_section').attr('sec_cd');
	C[8]=$(this).closest('.cus_div').closest('.f_resp_section').attr('num');
	C[12] = $("body").find("#sel_slot_1").val();
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function() {

        $.post(base_url + "index.php/FormQ/ins_ques_response_mst", {
            C: C
        }, function(data, textStatus) {
            if (textStatus == 'success') {
                if (!C[0]) {
                    $hj = $.trim(data);
                    $(ele).closest('.cus_div').attr('ques_resp_id', $hj);
                }
                $(ele).closest('table').removeClass('disabled_sec');

            } else {
               location.reload();
            }
        });
    }, 0);

});

		
		$('body').on('change', '.dec_select', function() {
			if($(this).val())
			{
    $(this).attr("disabled", true);
	
    var ele = $(this);
    var C = [];
    C[0] = $(this).closest('.sec_r_div').attr('sec_resp_id');
    C[1] = $(this).closest('.f_section').attr('sec_id');
    C[2] = $(this).closest('.f_resp_section').attr('sec_cd');
	C[5] = $(this).closest('.f_resp_section').attr('num');
    C[3] = $(".u_in").attr('user');
    C[4] = $(this).val();
	C[6]=$(this).closest('.sec_r_div').attr('type');
	C[12] = $("body").find("#sel_slot_1").val();
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function() {
        $.post(base_url + "index.php/FormQ/ins_sec_response_mst", {
            C: C
        }, function(data, textStatus) {
            if (textStatus == 'success') {
                if (!C[0]) {
                    $hj = $.trim(data);
                    $(ele).closest('.sec_r_div').attr('sec_resp_id', $hj);
						$(ele).closest('.f_resp_section').find('.cus_div').show();
					$(ele).closest('.sec_r_div').find('.show_samemr').show();
                }
                $(ele).attr("disabled", false);

            } else {
               location.reload();
            }
        });
    }, 0);
			}

});
		

		
$("body").on("click", ".loop_chk",function(event){	
				if($(this).val() && $(this).attr('t_val')==3)
				{
					var tpo=$("body").find(".selectpicker.po_looo").length;
			var tottt=0;
			var ele_sel=$(this).closest('.cus_div');
		$("body").find(".selectpicker.po_looo").each(function() { 
		 tottt= $(this).attr("siz");	
		});
			if(tpo<tottt)
				{
			var ele=$(this).closest('.f_resp_section');
			var ids = [];
				$("body").find(".f_resp_section").each(function () {
				  ids.push(parseInt($(this).attr('num'))); // ids.push(this.id) would work as well.
				});	
            // var num=Math.max(ids);	
			  var num=Math.max.apply(null, ids);
				//console.log(ids);
			var  ss_sec=$("body").find(".f_section").attr("sec_id");
			$.ajax({
			   url: base_url+"index.php/FormQ/load_many_pos?a=self_form&s_id="+ss_sec+"&cd="+(num+1),
			   success: function(data){
				   $(ele).after(data);
				   $(".selectpicker").selectpicker();
			   },
			   error: function(e) {
				   console.log(e.responseText);
			   }
			});
			}else{
			alert("No more Manager exists!");
			$(ele_sel).find("input[t_val=2]").prop("checked", true);
		}
		}
		
	});
	
	$("body").on("click", ".btnext",function(){		
		var req=$('body').find('.required').length;		
		var done=0;
		$('body').find('.required').each(function(index) {
			q_id=$(this).closest(".cus_div").attr('ques_resp_id');
			sd_id=$(this).closest(".sec_r_div").attr('sec_resp_id');
				if(q_id || sd_id)
				{
					if(q_id)
					{
					var ele = $(this).closest(".cus_div").find('table');	
					if(ele)
					{						
					var tc=0;
						$(ele).find('.cc_row').each(function(index){
						var th=$(this).find('.ans_tbl_list:checked').attr('t_val');
						if(!(th)) {
							 tc++;
							}
						});	
						if(tc==0)
						{
							done++;
						}
					}else{						
					done++;
					}
					}
					if(sd_id)
					{
						done++;
					}
				}			
		});
		if(done==req)
		{
			 var ss_curr=parseInt($("body").find('.f_section').attr('sec_id'));
			 
		var pp=$(this).closest('.f_section').find('.dec_chk:checked').last().attr('t_val');
			if(pp=="1")
			{
			var t=$(this).attr('yes_nav');
			}else if(pp=="3"){
				var t=ss_curr;
			}else{
				var t=$(this).attr('no_nav');
			}
			//alert(pp);
				$.ajax({
			   url: base_url+"index.php/FormQ/load_v_ins?a=self_form&s_id="+t,
			   success: function(data){
				   $('#cont_replace').html(data);
				   $(".selectpicker").selectpicker();
			   },
			   error: function(e) {
				   console.log(e.responseText);
			   }
			});
		}else{
			alert("Please fill the required questions!");
		}
	});
	
	$("body").on("click", ".btprev",function(){		
			var t=$(this).attr('yes_nav');				
			$.ajax({
			   url: base_url+"index.php/FormQ/load_v_ins?a=self_form&s_id="+t,
			   success: function(data){
				   $('#cont_replace').html(data);
				   $(".selectpicker").selectpicker();
			   },
			   error: function(e) {
				   console.log(e.responseText);
			   }
			});
	});
	
	$("body").on("click", ".s_click",function(){
		var eleme=$(this);
		var req=$('body').find('.required').length;		
		var done=0;
		$('body').find('.required').each(function(index) {
			q_id=$(this).closest(".cus_div").attr('ques_resp_id');
			sd_id=$(this).closest(".sec_r_div").attr('sec_resp_id');
				if(q_id || sd_id)
				{
				 if(q_id)
					{
					var ele = $(this).closest(".cus_div").find('table');	
					if(ele)
					{						
					var tc=0;
						$(ele).find('.cc_row').each(function(index){
						var th=$(this).find('.ans_tbl_list:checked').attr('t_val');
						if(!(th)) {
							 tc++;
							}
						});	
						if(tc==0)
						{
							done++;
						}
					}else{						
					done++;
					}
					}
					if(sd_id)
					{
						done++;
					}
				}			
		});
		var kj=$(eleme).find('.step_no').attr('id');		
			kj = parseInt(kj.replace("step-", ""));
			var ss_curr=parseInt($("body").find('.f_section').attr('sec_id'));
			//alert(kj);
		if(done==req || kj<=ss_curr)
		{	
				
			var t=$(eleme).find('.step_no').attr('id');		
			t = t.replace("step-", "");
			$.ajax({
			   url: base_url+"index.php/FormQ/load_v_ins?a=self_form&s_id="+t,
			   success: function(data){
				   $('#cont_replace').html(data);
				   $(".selectpicker").selectpicker();
			   },
			   error: function(e) {
				   console.log(e.responseText);
			   }
			});
		}
	});
	
	var urlParams = new URLSearchParams(window.location.search);
	var file=urlParams.get('a');
	//console.log( $('#sidebar-menu a[hrf="' + file + '"]').parent('li'));
    $("body").find('#sidebar-menu a[hrf="' + file + '"]').parent('li').addClass('current-page');
    $("body").find('#sidebar-menu a[hrf="' + file + '"]').parent('li').parent('ul').slideDown().parent().addClass('active');
	
     $(".selectpicker").selectpicker();
	
    });
  </script>
  </body>
</html>