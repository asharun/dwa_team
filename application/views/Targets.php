<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
  $file_nm=str_replace('.php','',basename(__FILE__));
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?> 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>"  type="text/css">
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
			<div class='col-md-12'>
					<button class='stab_stages stab_dis_selec' ch='Targets'>Set Targets</button> 
					<button class='stab_stages' ch='Status_Report_37'>Suggested Targets: Past 3 & 7 day Status</button>
					<?php 
					$role_id            = $this->session->userdata('role_id');
					if($role_id==1)
					{
					echo "<button class='stab_stages' ch='Week_Targets'>Tgt Changes</button>
					<button class='stab_stages' ch='All_Targets'>All Targets</button>";
					}?>
			</div>
		</div>
					<div class='row hid'>	
						<div class='col-md-12'>							
						<div class='row row_style_1 text-center'>
														<div class='col-md-2'>
															<label class='l_font_fix_3'>New Target</label>	
															<input type='number' class='new_tgt form-control'/>	
														</div>
														<div class='col-md-2'>
															<label class='l_font_fix_3'>Choose Start Date</label>	
															<input id='t_dtpicker' class='choose_dt date-picker form-control'/>	
														</div>
														<div class='col-md-2'>
															<label class='l_font_fix_3'>Start Date</label>	
															<h6 class='start_dt' dt=''></h6>
														</div>
														<div class='col-md-2'>
															<label class='l_font_fix_3'>Prev End Date</label>	
															<h6 class='end_dt' dt=''></h6>	
														</div>
														<div class='col-md-3'>
															<label class='l_font_fix_3'>Change Desc</label>	
															<input id='change_desc' type='text' class='form-control'/>	
														</div>
														<div class='col-md-1'>
														<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>	
														<button class='btn add_but gre_but std_upd' type='button'>Submit</button>
												</div>
													</div>						
							<hr class="str_hr" style="border-top:2px solid #ddd;">
									<div class='modal fade open_col' id='show_gl_col24'>										
											<div class='modal-dialog asdklk_qw' role='document'>
												<div class='modal-content'>
												  <div class='modal-body' id='modal_edit'>
												    
												 </div>								  
												</div>
											  </div>
									</div>
									
									<div class='row row_style_1'>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Dept:</label>
															<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php
															foreach ($dept_val as $row)
															{
																$sel='';
																	if($dept_opt==$row['dept_id'])
																	{
																		$sel='selected';																		
																	}
																echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
															}
															?>
															</select>
														</div>
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Project:</label>
															<select id='sel_1234' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															echo '<option data-hidden="true"></option>';
															if($pro_sel_dta)
															{
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($pro_sel_val==$row2['sel_1_id'])
																	{
																		$sel='selected';																		
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
																?>
															</select>
														</div>	
														<div class='col-md-4'>
															<label class='l_font_fix_3'>Choose Activity Status:</label>
															<select id='sel_456' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
															<?php 
															//echo '<option data-hidden="true"></option>';
															//echo $t_status;
															if($t_status)
															{
																$sttat=array("","Active","Closed","All");
																foreach ($sttat as $key=>$row2)
																{
																	if($key)
																	{
																	$sel='';
																		if($t_status==$key)
																		{
																			$sel='selected';																		
																		}
																	echo "<option value='" . $key.  "' ".$sel.">" . $row2 . "</option>";
																	}
																}
															}
																?>
															</select>
														</div>														
										</div>
										<div id="toolbar" > 
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="all">Export All</option>
									<option value="selected">Export Selected</option>
							</select>
					</div>
									<table class="display table table-bordered table-responsive" data-toolbar="#toolbar" data-filter-control="true" data-show-export="true"  data-checkbox-header="false" id="table" data-search-time-out=500 data-pagination="true" data-search="true">
										<thead>
											<tr>
											  <th data-formatter="chk_formatter" data-events="chkselected" data-field="state"></th>
											  <th data-class='l_font_fix_3 hidden std_id' data-field="std_tgt_id">ID</th>
											  <th data-class="l_font_fix_3 hidden resp_id" data-field="activity_id">Act Id</th>
											  <th data-sortable="true" data-class="l_font_fix_3 act_nm" data-filter-control="input" data-field="activity_name">Activity Name</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="wu_name">Work Units</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="pre_tgt_val">Prev Target</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="tgt_val">Set Target</th>
											  <th data-sortable="true" data-class="l_font_fix_3"  data-field="achieved_tgt">Suggested Target</th>
											  <th data-sortable="true" data-class="l_font_fix_3"  data-filter-control="input" data-field="day_val">Start Date</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="change_desc">Prev Change Desc</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="full_name">Inserted By</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-field="ins_dt">Inserted On</th>
											  <th data-sortable="true" data-class="l_font_fix_3" data-align="center" data-halign="center" data-formatter="nameFormatter">Options</th>
											</tr>
										</thead>
									</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>

	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>

var chkselected = {
    'click :checkbox': function (e, value, row, index,ele) {
		//console.log($(ele));
		$(ele).toggleClass("selected");

    }
};

$('#toolbar').find('select').change(function () {
            $('#table').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });
		
	$('body').on('focus',".choose_dt",  function(){	
		$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1,
					daysOfWeekDisabled: "0,2,3,4,5,6"
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var endOfWeek = moment(ele).startOf('week').toDate();
						var d1 = moment(ele).startOf('isoWeek').toDate();						
						var date_lor =moment(d1).format("DD-MM-YYYY 00:00:00");
						var date_lor1 =moment(d1).format("YYYY-MM-DD 00:00:00");
						var date_end =moment(endOfWeek).format("DD-MM-YYYY 23:59:59");
						var date_end1 =moment(endOfWeek).format("YYYY-MM-DD 23:59:59");
						$("body").find(".end_dt").text(date_end);
						$("body").find(".end_dt").attr('dt',date_end1);
						$("body").find(".start_dt").text(date_lor);	
						$("body").find(".start_dt").attr('dt',date_lor1);
						}
					});
		});
		
		function nameFormatter(value, row) {
        return "<i class='glyphicon glyphicon-edit t_edit tgt_modi'></i>";
    }
	
	
	

	
	function chk_formatter(value, row, index) {
    return '<input class="" type="checkbox" id="d_' + row["id"] +'"' + ((value == 0) ? ' disabled ' : '') + '>';
}

	// function chk_formatter(value, row, index, field) {
		// console.log(value);
        // if (value == 0) {
            // return {
                // disabled: true
            // };
    // }else{
		// return {
                // disabled: false
            // };
	// }
	// }
	
	$("body").on("change","#sel_1234",function(){
		if($(this).val())
		{				var str='';
						var pro_v=$(this).val();						
						str=str+"&pro="+pro_v;
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						var stat=$("body").find("#sel_456").val();
						if(stat)
						{
							str=str+"&d="+stat;
						}
						
			window.location = base_url+"index.php/User/load_view_f?a=Targets"+str;
		}
	});
	
	$("body").on("change","#sel_456",function(){
		if($(this).val())
		{				var str='';
						var stat=$(this).val();						
						str=str+"&d="+stat;
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						
						var pro_v=$("body").find("#sel_1234").val();
						if(pro_v)
						{
							str=str+"&pro="+pro_v;
						}
						
						
			window.location = base_url+"index.php/User/load_view_f?a=Targets"+str;
		}
	});
	
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{			
			$.ajax({				
				url: base_url+"index.php/User/load_proj_data",
				type: 'post',
				data : {param:"Project",dept:$(this).val()},
				success: function(response){				
					$('#sel_1234').html(response).selectpicker('refresh');
				}
			});
		}	
	});
	
$.ajax({
       url: base_url+"index.php/User/load_table_boots?a=Targets&pro="+$('#sel_1234').val()+"&dept="+dep_opt+"&stat="+$('#sel_456').val(),
       dataType: 'json',
       success: function(response) {
           $('#table').bootstrapTable({
              data: response,
			  stickyHeader: true
           });
		 // $('#table').bootstrapTable('checkInvert');
       },
       error: function(e) {
           console.log(e.responseText);
       }
    });
	

</script>
  </body>
</html>