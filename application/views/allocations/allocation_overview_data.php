<?php
 	if(!empty($detailsbox)){
	 	foreach($detailsbox as $val){
	 		$levelid=$val['level_id_fk']+1;
	 		?>
 			<div class="col-sm-3 <?php echo $val['mid_code']; ?> card_overview">
				<div class="panel panel-primary shadowonbox">
				    <div class="panel-heading">
				        <h3 class="panel-title midsearchcode" data-midcode=<?php echo $val['mid_code']; ?>>
				        	<?php echo $val['mid_code']; ?>
				        	<?php if($val['show_p']!=0){
							echo "<span class='pull-right addallocationtab' data-btn='edit' data-edit_id=".$val['hierarchy_id']."><i class='glyphicon glyphicon-pencil'></i></span>";
				        	} ?>
				        </h3>
				    </div>
				    <div class="panel-body loadboxurl"style="height:199px" data-level_name=<?php echo $val['level_name'];?>
							 data-level_id=<?php echo $levelid;?>  data-hierarchy_id=<?php echo $val['hierarchy_id'];?> >
							 <p><?php echo $val['mid_name']; ?></p>
							 <p><?php echo $val['hier_desc']; ?></p>
				    <p>Start- <?php  isset($val['start_dt'])? $stdt=date('d-m-Y',STRTOTIME($val['start_dt'])):$stdt='';
					echo $stdt; ?></p>
					<p>End date-<?php isset($val['end_dt'])?$enddt=date('d-m-Y',STRTOTIME($val['end_dt'])):$enddt='';
					echo $enddt; ?></p>

					<!-- CHILD LEVEL COUNT -->
					<p><strong><small>Number of <?=$val['level_name']?>: <?=$val['child_count']?></small></strong></p>
				    </div>
			    </div>
			</div>

		<?php
	 	}
	}
 ?>

<?php
	 $depttype=$this->uri->segment(4);
	 $pagination = $this->input->get('page');
	 if(isset($depttype) && $depttype==2 && !$pagination)
	 {
	 ?>
	 <div class="col-sm-3">
			<div class="panel panel-primary addallocationtab" data-btn='save'>
		<div class="panel-body shadowonbox" style="height:238px	;text-align: center;background-color: gainsboro;">
	 <span style="font-size: 96px"><h1 class="glyphicon glyphicon-plus"></h1></span>
		</div>
		    </div>
		</div>
<?php  } ?>