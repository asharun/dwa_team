<?php
    $this->load->view('common/allocation/allocation_header');
    $this->load->view('common/allocation/sidenavbar');
    $userId=$this->session->userdata('user_id');

    $tabName = $this->uri->segment(2);
    $levelCode = $this->uri->segment(3);
    $deptId = $this->uri->segment(4);
    $levelId = $this->uri->segment(5);
    $hierarchyId = $this->uri->segment(6);
 ?>
<div class="main">
    <?php $this->load->view('common/allocation/allocationtopbar'); ?>

    <hr class="str_hr" style="border-top:2px solid #ddd;">

    <div class="row">
        <div class="col-md-12 c_row">
            <div class='row hid1'>
                <div class='row'>
                    <div class='col-md-12'>
                        <div id="tree-list-demo">
                            <div id="tasks"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div style="padding: 10px; position:absolute; top:20px; left:0px">
      <div id="notif-toast"></div>
      <div id="warning-notif-toast"></div>

    </div>
    <!-- Modal -->
    <div id="allocationModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content" id="AllocationModal">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
          </div>
          <div class="modal-body">

            <!-- SPINNER -->
            <div class="text-center modal-spinner hide">
                <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Please wait..
            </div>

            <!-- FORM -->
            <form id="levelForm">
            <div class="row entity-form hide">
           		<div class="row">
           			<div class="col-md-3">
           				<label class='l_font_fix_3 form-check-label' id="mid_code_1_label">CODE: </label>
           				<input required class='form-control syllabus_code' value='' name="mid_code_1">
           			</div>
                    <div class="col-md-3">
           				<label class='l_font_fix_3 form-check-label' id="mid_code_2_label"> </label>
                        <!-- <label class='l_font_fix_3 form-check-label levelcodename' ><?php echo $level_mst[count($level_mst)-1]['level_name'].':'; ?>   <a class="popuppattern"  title="Field Patter" onmouseover="" style="cursor: pointer;"data-content="<?php echo 'Must have '.strlen($level_mst[count($level_mst)-1]['level_code_example']).' character-Example:'.$level_mst[count($level_mst)-1]['level_code_example'] ?>"><i class="glyphicon glyphicon-info-sign"></i></a></label> -->
           				<input required class='form-control syllabus_code' value='' name="mid_code_2">
           			</div>
           			<div class="col-md-3">
           				<label class='l_font_fix_3 form-check-label' >NAME: </label>
           				<input class='form-control syllabus_name' value='' name="mid_name"/>
           			</div>

                    <div class="col-md-3">
               				<label class='l_font_fix_3 form-check-label' >PROGRESS: </label>
               				<select id='team_name' class='selectpicker form-control' name="progress" title='Nothing Selected'>
        					       <option value="Active">Active</option>
                         <option value="Not Started">Not Started</option>
                         <option value="Need Assistance">Need Assistance</option>
                         <option value="In Progress">In Progress</option>
                         <option value="Deferred">Deferred</option>
                         <option value="Completed">Completed</option>
                       </select>
               		</div>
           		</div>
           		<div class="row">
                    <div class="col-md-3">
           				<label class='l_font_fix_3 form-check-label' >ALLOCATED TO: </label>
  						<select id='syllabus_owner' class='selectpicker form-control' name="allocated_to[]" multiple="multiple" title='Nothing Selected' data-live-search='true'>
                            <?php foreach ($users as $user): ?>
                              <option value="<?php echo $user['user_id'] ?>"><?php echo $user['full_name'] ?></option>
                            <?php endforeach; ?>
          				</select>
           			</div>

           			<div class="col-md-3">
           				<label class='l_font_fix_3 form-check-label' >START DATE: </label>
         					<input  type='text' data-date-format='dd-mm-yyyy' id='s_dt' name='start_dt' class='initalizedt form-control toary' value='' />
           			</div>
           			<div class="col-md-3">
           				<label class='l_font_fix_3 form-check-label' >END DATE: </label>
           				<input  type='text' data-date-format='dd-mm-yyyy' id='s_dt' name='end_dt' class='initalizedt form-control toary' value='' />
           			</div>

                    <div class="col-md-3">
           				<label class='l_font_fix_3 form-check-label' >STATUS: </label>
                        <select id='team_name' class='selectpicker form-control' name="status_nm" title='Nothing Selected'>
                            <option value="Active">Active</option>
                            <option value="InActive">InActive</option>
                        </select>
               		</div>

           		</div>
              <div class="row">
                  <div class="col-sm-12">
                      <label class='l_font_fix_3 form-check-label' >DESCRIPTION: </label>
                     <textarea id='hier_desc'name='hier_desc' class="form-control hier_desc"></textarea>
                  </div>
           	  </div>
          </div>
          </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default add_but" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-default add_but" id="save-level-btn">Save</button>
          </div>
        </div>

      </div>
    </div>



    <!-- Modal -->
    <div id="activityModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content" id="AllocationModal">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
          </div>
          <div class="modal-body">

            <!-- SPINNER -->
            <div class="text-center modal-spinner hide">
                <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Please wait..
            </div>

            <!-- FORM -->
            <form id="activityForm">
            <div class="row entity-form hide">
                <div id="load_ac">
                    <div class='row row_style_1'>
                        <div class='col-md-6'>
                            <label class='l_font_fix_3'>Projects</label>
                            <select id='sel_1' name="project_id_fk" class='selectpicker form-control' title="Nothing Selected" data-live-search="true">
                            <?php
                                if ($deptProjects) {
                                    foreach ($deptProjects as $row2) {
                                        echo "<option value='" . $row2['sel_1_id'] .  "' >" . $row2['sel_1_name'] . "</option>";
                                    }
                                }
                            ?>
                            </select>
                        </div>
                        <div class='col-md-3'>
                            <label class='l_font_fix_3'>Category</label>
                            <select id='sel_2' name="task_id_fk" class='selectpicker form-control'  title="Nothing Selected" data-live-search="true">
                                <?php
                                    if ($mediaCategories) {
                                        foreach ($mediaCategories as $category) {
                                            echo "<option value='" . $category['p_id'] .  "' >" . $category['p_nm'] . "</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </div>

                        <div class='col-md-3'>
                            <label class='l_font_fix_3'>Status</label>
                            <select id='statusNm' class='selectpicker form-control' name="status_nm" title='Nothing Selected'>
                                <option value="Active">Active</option>
                                <option value="InActive">InActive</option>
                            </select>
                        </div>
                        </div>
                        <div class='row row_style_1'>
                            <div class='col-md-6'>
                                <label class='l_font_fix_3'>Element</label>
                                <select id='sel_3' name="sub_task_id_fk" class='selectpicker form-control'  title="Nothing Selected" data-live-search="true">
                                </select>
                            </div>
                            <div class='col-md-6'>
                                <label class='l_font_fix_3'>Action</label>
                                <select id='sel_4' name="job_id_fk" class='selectpicker form-control'  title="Nothing Selected" data-live-search="true">
                                </select>
                            </div>
                        </div>

                        <div class='row row_style_1'>
                            <div class='col-md-3'>
                                <label class='l_font_fix_3'>Allocated To</label>
                                <select id='syllabus_owner' class='selectpicker form-control' name="allocated_to[]" multiple="multiple" title='Nothing Selected' data-live-search='true'>
                                    <?php foreach ($users as $user): ?>
                                      <option value="<?php echo $user['user_id'] ?>"><?php echo $user['full_name'] ?></option>
                                    <?php endforeach; ?>
          						</select>
                            </div>
                            <div class='col-md-3'>
                                <label class='l_font_fix_3'>Progress</label>
                                <select id='team_name' class='selectpicker form-control' name="progress" title='Nothing Selected'>
                                    <option value="Active">Active</option>
                                    <option value="Not Started">Not Started</option>
                                    <option value="Need Assistance">Need Assistance</option>
                                    <option value="In Progress">In Progress</option>
                                    <option value="Deferred">Deferred</option>
                                    <option value="Completed">Completed</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label class='l_font_fix_3'>Start Date</label>
                                <input  type='text' data-date-format='dd-mm-yyyy' id='s_dt' name='start_dt' class='initalizedt form-control toary' value='' />

                            </div>
                            <div class="col-md-3">
                                <label class='l_font_fix_3'>End Date</label>
                                <input  type='text' data-date-format='dd-mm-yyyy' id='s_dt' name='end_dt' class='initalizedt form-control toary' value='' />
                            </div>
                        </div>
                    </div>
           	</div>
          </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default add_but" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-default add_but" id="save-activity-btn">Save</button>
          </div>
        </div>

      </div>
    </div>

</div>
<?php
  $this->load->view("common/allocation/allocation_footer.php");
?>

<script>
console.log( getQueryStringValue('exportSelector'));
window.exportResource = '';
window.isExport = getQueryStringValue('exportSelector') == 'true' ? true : false;
window.deptID = '<?php echo $deptId; ?>';
window.currentDate = '<?php echo $currentDate; ?>';
window.allLevelData = <?php echo json_encode($allLevelData); ?>;
window.collapsedParentLevelId = '';

$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
    window.userId = <?php echo $userId; ?>;

    // setTimeout(function() {
    //     exportToExcel();
    // }, 5000);
});

$('.dx-link-add').on('click', function(e) {
    e.preventDefault();
    alert("Archito Testing");
});


$(function(){
    var treeView, dataGrid;
    window.rowKey = '';
    window.rowActivityKey = '';
    window.assignedToUpdated = false;
    window.cellRowEdit = false;
    window.hasLevelPermission = false;
    window.hasActivityPermission = false;
    window.editDone = false;
    var deptHasAccess = ["2"];

    var syncTreeViewSelection = function(treeView, value){

       if (!value) {
           treeView.unselectAll();
           return;

       }
       treeView.unselectAll();

       if (typeof value != 'object') {
         value = value.split(",");
       }

       //console.log("Archito Testing");
       //console.log(value);
       value.forEach(function(key){
         //console.log("Archito Testing");

           treeView.selectItem(key);
       });
    };

    var makeAsyncDataSource = function(jsonFile){
       return new DevExpress.data.CustomStore({
           loadMode: "raw",
           key: "ID",
           load: function() {
               return $.getJSON("data/" + jsonFile);
           }
       });
    };

    var getSelectedItemsKeys = function(items) {
       var result = [];
       items.forEach(function(item) {
           if(item.selected) {
               result.push(item.key);
           }
           if(item.items.length) {
               result = result.concat(getSelectedItemsKeys(item.items));
           }
       });
       return result;
    };

var selectionType = window.isExport === true ? "multiple" : "none";
  var treeList =   $("#tasks").dxTreeList({
        dataSource: new DevExpress.data.DataSource({
            store: allocationStore,
            paginate: true,
            pageSize: 10
        }),

        // autoExpandAll: true,

        // height: "800px",
        keyExpr: "mid_code",
        parentIdExpr: "parent_mid",
        columnAutoWidth: true,
        wordWrapEnabled: true,
        showBorders: true,
		scrolling: {
            mode: "standard"
        },
        paging: {
            enabled: true,
            pageSize: 20
        },
        selection: {
            mode: selectionType,
            allowSelectAll: false
        },
        onSelectionChanged: function(selectedItem) {
            console.log("Archito Testing Selection");

            if (selectedItem.selectedRowsData.length > 1) {
                var treeList = $("#tasks").dxTreeList("instance");
                treeList.deselectAll();
                alert("Please select only one row");
            }
            if (selectedItem.selectedRowsData[0] != undefined && selectedItem.selectedRowsData[0]['level_id_fk'] > 1) {
                window.exportResource = 'http://localhost/dwa/index.php/allocation/getAllocationDeepChildsData/' + selectedItem.selectedRowsData[0]['level_id_fk'] + '/' + selectedItem.selectedRowsData[0]['hierarchy_id'];
            } else if (selectedItem.selectedRowsData[0] != undefined) {
                alert("Please select year or below");
            }
            console.log(window.exportResource);
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 50, 100],
            showInfo: true
        },
       //  searchPanel: {
       //     visible: true
       // },
        remoteOperations: {
            filtering: true
        },
        showBorders: true,

        hasItemsExpr: "hasItems",
        rootValue: "",
        editing: {
            mode: "cell",
            allowAdding: false,
            allowUpdating: false,
            allowDeleting: false
        },
        columns: [{

                dataField: "mid_code",
                caption: "MID Code",
                minWidth: 250,
                validationRules: [{ type: "required" }]
            },{
                dataField: "mid_name",
                caption: "MID Name",
                minWidth: 250
            }, {
                dataField: "owner_mid",
                caption: "Allocated to",
                minWidth: 120,
                dataSource: new DevExpress.data.DataSource({
                    store: userStore
                }),
                customizeText: function (cellInfo) {
                        if (window.assignedToUpdated) {
                            showNotifyToast();
                            window.assignedToUpdated = false;
                        }

                        if (cellInfo.value == null) return
                        var listOfAllocatedUsers = cellInfo.value.split(',');
                        var allocatedUsersNames = '';
                        listOfAllocatedUsers.forEach(function(key) {

                            userStore.byKey(key).done(function(data) {
                                allocatedUsersNames += data.full_name + ', ';
                            });
                        });
                        allocatedUsersNames = allocatedUsersNames.replace(/,\s*$/, "");
                        return allocatedUsersNames;
                },
                editCellTemplate: function(cellElement, cellInfo){
                    var value = cellInfo.value;
                    $("<div />").dxDropDownBox({
                        value: cellInfo.value,
                        text: "Text",
                        valueExpr: "user_id",
                        displayExpr: "full_name",
                        placeholder: "Select a value...",
                        showClearButton: true,
                        dataSource: new DevExpress.data.DataSource({
                            store: userStore
                        }),

                        onContentReady: function(args){
                                if (value != null)
                                    args.component.option("value", value.split(","));
                        },
                        contentTemplate: function(e){
                          var value = e.component.option("value"),
                                $treeView = $("<div>").dxTreeView({
                                    dataSource: e.component.option("dataSource"),
                                    dataStructure: "plain",
                                    keyExpr: "user_id",
                                    selectionMode: "multiple",
                                    displayExpr: "full_name",
                                    selectByClick: true,
                                    onContentReady: function(args){
                                       syncTreeViewSelection(args.component, value);
                                    },

                                    selectNodesRecursive: false,
                                    showCheckBoxesMode: "normal",
                                    onItemSelectionChanged: function(args){

                                        var nodes = args.component.getNodes(),
                                            value = getSelectedItemsKeys(nodes);

                                        window.cellRowEdit = true;
                                        e.component.option("value", value);
                                    }
                                });

                            treeView = $treeView.dxTreeView("instance");

                            e.component.on("valueChanged", function(args){
                                var value = args.value.toString();
                                if (window.rowKey != '') {
                                    BYJU.allocationDetails.updateLevel(window.rowKey, {'owner_mid': value});
                                    window.rowKey = '';
                                }
                                if (window.rowActivityKey != '') {
                                    BYJU.allocationDetails.updateActivity(window.rowActivityKey, {'alloc_to': value});
                                    window.rowActivityKey = '';
                                }
                                $("#tasks").dxTreeList("refresh");
                                window.assignedToUpdated = true;
                                // syncTreeViewSelection(treeView, value);
                            });

                            return $treeView;
                        }
                    }).appendTo(cellElement);
                }
				//,validationRules: [{ type: "required" }]
            }, {
                dataField: "progress",
                caption: "Status",
                minWidth: 120,
                lookup: {
                    dataSource: [
                        "Active",
                        "Not Started",
                        "Need Assistance",
                        "In Progress",
                        "Deferred",
                        "Completed"
                    ]
                }
            }, {
                dataField: "start_dt",
                caption: "Start Date",
                dataType: "date"
            }, {
                dataField: "end_dt",
                caption: "Due Date",
                dataType: "date"
            },
            // {
            //     dataField: "",
            //     caption: "Options",
            //     cellTemplate: function(container, options) {
            //       $("<div />").dxButton({
            //         text: "Add",
            //         height: 34,
            //         width: 195,
            //         onClick: function(e) {
            //

            //           //console.log("Archito Testing");
            //           //console.log();
            //         }
            //       }).appendTo(container);
            //     }
            // }

        ],
        onInitNewRow: function(e) {
            e.data.Task_Status = "Not Started";
            e.data.Task_Start_Date = new Date();
            e.data.Task_Due_Date = new Date();
        },
        columnFixing: {
            enabled: true
        },
        // onContextMenuPreparing: function(e) {
        //
        //     //console.log(e);
        //     // Get the ID of edited column
        //    // var entityId = e.row.data.Task_ID;
        //     if (e.target == "content" && e.columnIndex == 0) {
        //       $("<div />").dxContextMenu({
        //          items: [
        //              { text: "Edit" },
        //              { text: "Add Next Level" },
        //              { text: "Add New Activity" },
        //              { text: "Delete" }
        //          ],
        //          target: "div.dx-treelist-text-content",
        //          onItemClick: function(info) {
        //            var actionName = info.itemData.text;
        //
        //             switch (actionName) {
        //                 case 'Edit':
        //                     window.hasLevelPermission = (e.row.data.show_p == '1') ? true : false;
        //                     if (window.hasLevelPermission) {
        //                         BYJU.allocationDetails.editAllocationEntity(e.row.data);
        //                     } else {
        //                         showNotifyWarningToast("Oops! Sorry, current department have no access to add resources");
        //                     }
        //                     break;
        //                 case 'Add Next Level':
        //                     if (e.row.data.level_id_fk == null) break;
        //                     window.hasLevelPermission = (e.row.data.show_p == '1') ? true : false;
        //                     if (deptHasAccess.indexOf(window.deptID) > -1 && e.row.data.show_p == "1") {
        //                         BYJU.allocationDetails.addAllocationEntity(e.row.data);
        //                     } else {
        //                         showNotifyWarningToast("Oops! Sorry, current department or person have no access to add resources");
        //                     }
        //                     break;
        //                 case 'Add New Activity':
        //                     if (e.row.data.level_id_fk == null) break;
        //                     window.hasLevelPermission = (e.row.data.show_p == '1') ? true : false;
        //                     if (deptHasAccess.indexOf(window.deptID) > -1 && window.hasLevelPermission) {
        //                         BYJU.allocationDetails.addAllocationActivityEntity(e.row.data);
        //                     } else {
        //                         showNotifyWarningToast("Oops! Sorry, current department have no access to add resources");
        //                     }
        //                     break;
        //                 case 'Delete':
        //                 break;
        //             break;
        //             default:
        //
        //            }
        //          }
        //      }).appendTo(e.targetElement);
        //     }
        // },
        onRowUpdating: function(e) {
            if (e.oldData.level_id_fk == null) {
                if (e.oldData.show_p == "1") {
                    window.rowActivityKey = e.oldData.hierarchy_id.replace('A-', '');
                    window.hasActivityPermission = true;
                } else {
                    window.hasActivityPermission = false;
                }
            } else {
                if (e.oldData.show_p == "1") {
                    window.hasLevelPermission = true;
                } else {
                    window.hasLevelPermission = false;
                }
            }

        },
        onRowUpdated: function(e) {
            if (window.hasLevelPermission) {
                // showNotifyToast();
            }
        },
        onEditingStart: function(event) {
            if (event.data.level_id_fk == null) {
                window.rowActivityKey = event.data.hierarchy_id.replace('A-', '');
            } else {
                window.rowKey = event.data.hierarchy_id;
            }
        },
        onRowExpanding: function(e) {
            var treeList = $("#tasks").dxTreeList("instance");
            if (!window.editDone) {
                window.collapsedParentLevelId = treeList.getNodeByKey(e.key) != undefined ? treeList.getNodeByKey(e.key).data.level_id_fk : '';
            } else {
                window.editDone = false;
            }
        },
        onRowPrepared: function(event) {
            var treeList = $("#tasks").dxTreeList("instance");

            if (window.rowKey != '') {
                event.isExpanded = false;
                if (window.editDone) {
                    treeList.collapseRow(event.rowIndex);
                    setTimeout(function() {
                        treeList.expandRow(event.rowIndex);
                    }, 400);
                }
            }
            if (event.node != undefined) {
                event.node.hasChildren = true;
            }
        },
        onCellPrepared: function(event) {
            if (event.rowType == 'data' && event.columnIndex == 0 && event.displayValue != '' && event.displayValue != undefined) {
              // //console.log(event.cellElement);
                var labelClass = 'label-default';
                switch (event.data.level_code) {
                  case 'SY':
                    labelClass = 'label-primary';
                    break;
                  case 'YE':
                    labelClass = 'label-info';
                    break;
                  case 'GR':
                    labelClass = 'label-success';
                    break;
                  case 'SU':
                    labelClass = 'label-danger';
                    break;
                  default:
                }
                var levelData = findInArrayByKeyValue(window.allLevelData, 'level_id', event.data.level_id_fk);

                var mainText = event.data.mid_code;
                var text = ' <label class="label level-legend" style="color:white;--background:'+levelData['color_code']+'">'+levelData['level_code']+'</label>  '+mainText;
                $(event.cellElement[0]).find('.dx-treelist-text-content').html(text);

            }

            if (event.rowType == 'data' && event.columnIndex == 0 && event.data.level_id_fk == null) {
                var mainText = event.data.mid_name ;
                var text = ' <label class="label label-warning">Activity</label>  '+mainText;
                $(event.cellElement[0]).find('.dx-treelist-text-content').html(text);
                // HERE WE ARE REMOVING COLLAPSED BUTTON
                $(event.cellElement[0]).find('.dx-treelist-collapsed').remove();
            }
        }
    });

    function upd(key) {
        // var key = getRandomKey(1, 10);
        // allocationStore.localUpdate(key, { FirstName: 'FirstName_' + (++index).toString() });

        // //console.log({ FirstName: 'FirstName_' + (++index).toString() });
        var rowIndex = $("#tasks").dxTreeList("getRowIndexByKey", key);
        //console.log("This is the row index.." + rowIndex);
        // $("#tasks").dxTreeList("repaintRows", [rowIndex]);
        // treeList.repaintRows([rowIndex]);
    }





});

function showNotifyToast() {
  $("#notif-toast").dxToast({
     "displayTime": 1000,
     "type": "success",
     "message": "Data updated successfully",
     "visible": true,
     "position": {
         "my": "right",
         "at": "right",
         "of": ".dx-treelist-table.dx-treelist-table-fixed"
     },
     "width": "300"
   });
}

function showNotifyWarningToast(warningMessage) {
  $("#warning-notif-toast").dxToast({
     "displayTime": 3000,
     "type": "error",
     "message": warningMessage,
     "visible": true,
     "position": {
         "my": "right",
         "at": "right",
         "of": ".dx-treelist-table.dx-treelist-table-fixed"
     },
     "width": "600"
   });
}

/**
 * USER STORE STARTS HERE
 */
var userStore = new DevExpress.data.ArrayStore({
    key: "user_id",
    data: <?php echo json_encode($users); ?>,
    // Other ArrayStore options go here
});



/**
 * ALLOCATION STORE STARTS HERE
 */
var allocationStore = new DevExpress.data.CustomStore({
    key: 'mid_code',
    load: function(options) {
        var d = $.Deferred();
        //simulate loading data from server
        // setTimeout(function(){
        var allocationDataUrl = '<?php echo $allocationAjaxUrl; ?>' + '/' +  options.parentIds[0] ;

        if (window.collapsedParentLevelId != '') {
            allocationDataUrl = base_url + 'index.php/allocation/getAllocationDataByMid/' +
             (Number(window.collapsedParentLevelId) + Number(1)) +
             '/' +
             options.parentIds[0] + '/true';
             window.collapsedParentLevelId = '';
        }
        console.log(allocationDataUrl);
        return $.ajax({
            url: allocationDataUrl,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                allocationStore.innerStoreData.length = 0;
                allocationStore.innerStoreData.push.apply(allocationStore.innerStoreData, response.data);
                d.resolve(allocationStore.innerStoreData);
                return d.promise();
            }
        });
        // }, 2000);
    },
    byKey: function(key, extra) {
        return this.innerStore.byKey(key, extra);
    },
    update: function(key, values) {
      window.cellRowEdit = true;
        if (window.rowActivityKey !== '') {
            if (Object.keys(values)[0] != 'mid_name') {
                BYJU.allocationDetails.updateActivity(window.rowActivityKey, values);
                showNotifyToast();
                window.rowActivityKey = '';
                window.location.reload();
            } else {
                alert("Please use modal to update activity MID")
            }
        } else {
            //console.log("Updating levels here...");
            if (window.hasLevelPermission) {
                if (Object.keys(values)[0] != 'mid_code') {
                    BYJU.allocationDetails.updateLevel(key, values);
                    showNotifyToast();
                    window.editDone = true;
                    // window.location.reload();
                } else {
                    showNotifyWarningToast("Please update MID from toolbar modal");
                }
            } else {
                showNotifyWarningToast("Oops! Sorry, you dont have permission to edit");
            }
        }
        // alert("update called - send update to server here");
        //but also update local store
        // allocationStore.innerStore.update(key, values);
    }
});
allocationStore.innerStoreData = [];
allocationStore.innerStore = new DevExpress.data.ArrayStore({
  data: allocationStore.innerStoreData,
  key: 'hierarchy_id'
});

allocationStore.localUpdate = function(key, values) {
		//we don't want to update the row being edited - it'll be refreshed by the server after update
		var rowIndexToUpdate = allocationStore.theGrid.getRowIndexByKey(key);
    if(allocationStore.editingRow != rowIndexToUpdate){
    	this.innerStore.update(key, values);
    }
}

function exportToExcel(e) {

            var tv = $("#tasks").dxTreeList("instance");
            var columns = tv.getVisibleColumns();
            var data = tv.getVisibleRows();
            console.log("Archito Testing visible rows..");
            console.log(data);
            var csvContent = "";

            for (i = 0; i < columns.length; i++) {
                csvContent += columns[i].caption + ",";
                }
            csvContent += "\r\n";

            for (i = 0; i < data.length; i++) {
                row = data[i].values;
                console.log(row);
                row.forEach(function (item, index) {
                    if (item === undefined || item === null) { csvContent += ","; }
                    else { csvContent += item + ","; };
                   }
                );
                csvContent += "\r\n";
            }

            var blob = new Blob([csvContent], { type: "text/csv;charset=utf-8;" });

            if (navigator.msSaveBlob) {
                navigator.msSaveBlob(blob, 'Job_Progress.csv')
            }
            else {
                var link = document.createElement("a");
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", "Job_Progress.csv");
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            };
        }



</script>