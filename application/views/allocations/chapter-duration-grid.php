<?php if (!empty($chapterDurations)): ?>
<div class="table-responsive">
    <h5>Video Clip Durations</h5>
    <table class="table table-striped">
        <thead>
            <td>Video Type</td>
            <td>Submitted</td>
            <td>Recorded</td>
            <td>Logged</td>
            <td></td>
        </thead>
        <tbody>
                <?php foreach ($chapterDurations as $key => $value): ?>


                    <tr id="duration-form-<?=$value['id']?>">
                            <td><?= $value['video_clip_type']?></td>
                            <td><?= $value['submitted'] ?></td>
                            <td><input type="number" class="form-control" name="recorded" value="<?= $value['recorded']?>"></td>
                            <td><input type="number" class="form-control" name="logged" value="<?= $value['logged']?>"></td>
                            <td> <button type="button" name="button" class="btn btn-sm btn-primary update-video-clip-duration-btn" data-id="<?=$value['id']?>">
                                <small><strong>Update</strong></small></button>
                            </td>
                    </tr>

                <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
        <h6>No Video Clip Duration data found</h6>
    <?php endif; ?>
</div>