<?php
    $this->load->view('common/allocation/allocation_header');
    $this->load->view('common/allocation/sidenavbar');
    $userId=$this->session->userdata('user_id');

    $tabName = $this->uri->segment(2);
    $levelCode = $this->uri->segment(3);
    $deptId = $this->uri->segment(4);
    $levelId = $this->uri->segment(5);
    $hierarchyId = $this->uri->segment(6);
 ?>
<div class="main">
    <?php $this->load->view('common/allocation/allocationtopbar'); ?>

    <hr class="str_hr" style="border-top:2px solid #ddd;">

    <div class="row">
        <div class="col-md-12 c_row">
            <div class='row hid1'>
                <div class='row'>
                    <div class='col-md-12'>
                        <div id="gantt-summary-demo">
                            <div id="gantt_here" style='height:450px;'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
  $this->load->view("common/allocation/allocation_footer.php");
?>

<script>var base_url = '<?php echo base_url() ?>';</script>

<script>
var start_date = moment().format("YYYY-MM-DD");

gantt.config.date_grid = "%d-%m-%Y";
/*gantt.config.scale_unit = "day";
gantt.config.date_scale = "%d %M %Y";
gantt.config.duration_unit = "day";*/
gantt.config.show_errors = false;
// gantt.config.start_date = start_date;
//Time filter config
var zoomConfig = {
    levels: [
       {
            name:"week",
            scale_height: 50,
            min_column_width:80,
            scales:[
                {unit: "week", step: 1, format: function (date) {
                    var dateToStr = gantt.date.date_to_str("%d %M");
                    var endDate = gantt.date.add(date, -6, "day");
                    var weekNum = gantt.date.date_to_str("%W")(date);
                    var year = gantt.date.date_to_str("%Y")(date);
                    return "#" + weekNum + " Week, " + dateToStr(date) + " - " + dateToStr(endDate) + " , " + year;
                }},
                {unit: "day", step: 1, format: "%j %D"}
            ]
        },
        {
            name:"month",
            scale_height: 50,
            min_column_width:120,
            scales:[
                {unit: "month", format: "%F, %Y"},
                {unit: "week", format: "Week #%W"}
            ]
        },
        {
            name:"quarter",
            height: 50,
            min_column_width:90,
            scales:[
                {unit: "month", step: 1, format: "%M"},
                {
                    unit: "quarter", step: 1, format: function (date) {
                        var dateToStr = gantt.date.date_to_str("%M");
                        var year = gantt.date.date_to_str("%Y")(date);
                        var endDate = gantt.date.add(gantt.date.add(date, 3, "month"), -1, "day");
                        return dateToStr(date) + " - " + dateToStr(endDate) + " , " + year;
                    }
                }
            ]
        }
    ]
};

gantt.ext.zoom.init(zoomConfig);
gantt.ext.zoom.setLevel("week");
 
/*gantt.config.subscales = [
    {unit: "month", step: 1, date: "%F, %Y"},
];*/
gantt.config.columns = [
    {name:"text",       label:"MID name",  width:"*", tree:true },
    {name:"start_date", label:"Start time", align:"center" },
    {name:"duration",   label:"Duration",   align:"center" },
];
gantt.config.show_errors = false;

gantt.templates.task_text=function(start,end,task){
    return "<b>Text:</b> "+task.text+",<b> Status:</b> "+task.progress;
};

gantt.templates.tooltip_text = function(start,end,task){
    return "<b>Task:</b> "+task.text+"<br/><b>Duration:</b> " + task.duration;
};

gantt.init("gantt_here");
gantt.addTaskLayer(function draw_deadline(task) {
    if (task.deadline) {
        var el = document.createElement('div');
        el.className = 'deadline';
        var sizes = gantt.getTaskPosition(task, new Date(task.deadline));
        el.style.left = sizes.left + 'px';
        el.style.top = sizes.top + 'px';
        //
        el.setAttribute('title', 'Due Date: ' + gantt.templates.task_date(new Date(task.deadline)));
        return el;
    }
    return false;
});

gantt.addTaskLayer(function draw_deadline(task) {
    if (task.actual_start_date) {
        var el1 = document.createElement('div');
        el1.className = 'actual-start-date';
        var sizes1 = gantt.getTaskPosition(task, new Date(task.actual_start_date));
        el1.style.left = sizes1.left + 'px';
        el1.style.top = sizes1.top + 'px';
        el1.setAttribute('title', 'Actual Start Date: ' + gantt.templates.task_date(new Date(task.actual_start_date)));
        return el1;
    }
    return false;
});

gantt.addTaskLayer(function draw_deadline(task) {
    if (task.actual_end_date) {
        var el1 = document.createElement('div');
        el1.className = 'actual-end-date';
        var sizes1 = gantt.getTaskPosition(task, new Date(task.actual_end_date));
        el1.style.left = sizes1.left + 'px';
        el1.style.top = sizes1.top + 'px';
        el1.setAttribute('title', 'Actual End Date: ' + gantt.templates.task_date(new Date(task.actual_end_date)));
        return el1;
    }
    return false;
});

// gantt.config.branch_loading = true;
//
// gantt.load('http://localhost/dwa/index.php/allocation/getSummaryPageData/syllabus/1/1?parent_id=1');
// var dp = new gantt.dataProcessor('http://localhost/dwa/index.php/allocation/getSummaryPageData/syllabus/1/1?parent_id=1');
// dp.init(gantt);
function getSummaryData(url) {
    url = url || base_url+"index.php/allocation/getSummaryPageData/syllabus/1";
    var table = $("#gantt_here");
    table.LoadingOverlay("show", {
        text        : "Loading...",
        image       : "",
        maxSize : 40
    });
    $.ajax({
         url: url,
         type: "get",
    }).done(function (result) {
        gantt.config.show_task_cells = false;
        gantt.config.static_background = true;
        gantt.config.branch_loading = true;
        var result =JSON.parse(JSON.stringify(result).replace(/null/g, '""'));
        gantt.config.start_date = moment().format("YYYY-MM-DD");
        gantt.config.end_date = moment().add(10, 'days').format("YYYY-MM-DD");

        gantt.parse(result);
    }).fail(function (jqXHR,status,err) {
        console.log(err);
    }).always(function () {
        table.LoadingOverlay("hide");
    });
}

$(document).ready(function() {
    var url = base_url+"index.php/allocation/getSummaryPageData/syllabus/<?php echo $levelId; ?>/<?php echo !empty($hierarchyId) ? $hierarchyId : 0; ?>/true";
    getSummaryData(url);
});

gantt.attachEvent("onTaskRowClick",function(id,row){
    // any custom logic here
    var rowData = gantt.getTask(id);
    var url = base_url+"index.php/allocation/getSummaryPageData/syllabus/" + (Number(rowData.level_id_fk) + Number(1)) + "/" + Number(rowData.id) ;
   // console.log(url);
    getSummaryData(url);

});


$(document).ready(function() {
    $(document).on("change","#time_filter",function(e){
        e.preventDefault();
        gantt.config.start_date = moment($(".s_dt ").val(),"DD-MM-YYYY").format("YYYY-MM-DD");
        gantt.config.end_date = moment($(".e_dt").val(),"DD-MM-YYYY").format("YYYY-MM-DD");
        var filter = $(this).val();
        gantt.ext.zoom.setLevel(filter);
    });
});


</script>