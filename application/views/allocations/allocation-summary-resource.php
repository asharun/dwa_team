<?php
    $this->load->view('common/allocation/allocation_header');
    $this->load->view('common/allocation/sidenavbar');
    $userId=$this->session->userdata('user_id');

    $tabName = $this->uri->segment(2);
    $levelCode = $this->uri->segment(3);
    $deptId = $this->uri->segment(4);
    $levelId = $this->uri->segment(5);
    $hierarchyId = $this->uri->segment(6);
 ?>
<div class="main">
    <?php
	//$data['s_dt']='';

	$this->load->view('common/allocation/allocationtopbar'); ?>

    <hr class="str_hr" style="border-top:2px solid #ddd;">

    <div class="row">
        <div class="col-md-12 c_row">
            <div class='row hid1'>
                <div class='row'>
                    <div class='col-md-12'>
                        <div id="gantt-summary-demo">
                            <div id="gantt_here" style='height:450px;'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
  $this->load->view("common/allocation/allocation_footer.php");
?>

<script>var base_url = '<?php echo base_url() ?>';</script>

<script>
var start_date = $(".s_dt") , end_date =  $(".e_dt");
function byId(list, id) {
    for (var i = 0; i < list.length; i++) {
        if (list[i].key == id)
            return list[i].label || "";
    }
    return "";
}

gantt.config.work_time = true;
gantt.config.show_errors = false;
gantt.config.scale_unit = "day";
gantt.config.date_scale = "%D, %d";
gantt.config.min_column_width = 60;
gantt.config.duration_unit = "day";
gantt.config.scale_height = 20 * 3;
gantt.config.row_height = 35;

/*gantt.config.start_date = new Date('2019, 02, 31');
gantt.config.end_date = new Date('2019, 03, 09');
*/
// var weekScaleTemplate = function (date) {

   // var weekNum = gantt.date.date_to_str("(week %W)");

	 // var day12 = date.getDay(),
      // diff_s = (day12 === 0 ? -6 : day12-1)*-1,
	  // diff_e =  7-day12;
    // var endDate = gantt.date.add(date,diff_e, "day");
	// var startDate = gantt.date.add(date,diff_s, "day");

	 // var dateToStr = gantt.date.date_to_str("%d %M");
    // return dateToStr(startDate) + " - " + dateToStr(endDate) + " " + weekNum;
// };

var weekScaleTemplate = function (date) {
					var dateToStr = gantt.date.date_to_str("%d %M");
					var startDate = gantt.date.week_start(new Date(date));
					var endDate = gantt.date.add(gantt.date.add(startDate, 1, "week"), -1, "day");
					return dateToStr(startDate) + " - " + dateToStr(endDate);
				};

gantt.config.subscales = [
    {unit: "month", step: 1, date: "%F, %Y"},
    {unit: "week", step: 1, template: weekScaleTemplate}
];

gantt.addCalendar({
    id: "custom",
    worktime: {
        hours: [8, 17],
        days: [1, 1, 1, 1, 1, 1, 1]
    }
});

gantt.templates.task_cell_class = function (task, date) {
    if (!gantt.isWorkTime({date: date, task: task}))
        return "week_end";
    return "";
};

gantt.locale.labels.section_calendar = "Calendar";


gantt.config.columns = [
    {name:"text",  label:"RESOURCE", tree: true,  width:"*" },
];

// gantt.config.layout = {
    // css: "gantt_here",
    // rows: [
       // {view: "scrollbar", id: "scrollHor"}
    // ]
// };

gantt.templates.task_text=function(start,end,task){
    return "<b>Text:</b> "+task.text+",<b> Status:</b> "+task.progress;
};

gantt.templates.tooltip_text = function(start,end,task){
    return "<b>Task:</b> "+task.text+"<br/><b>Duration:</b> " + task.duration + "<br/><b>Progress:</b> " + task.progress;
};

gantt.init("gantt_here",moment(start_date.val(),"DD-MM-YYYY").format("YYYY-MM-DD"),moment(end_date.val(),"DD-MM-YYYY").format("YYYY-MM-DD"));
gantt.addTaskLayer(function draw_deadline(task) {
    if (task.deadline) {
        var el = document.createElement('div');
        el.className = 'deadline';
        var sizes = gantt.getTaskPosition(task, new Date(task.deadline));
        // console.log("Sizes");
        // console.log(sizes);
        el.style.left = sizes.left + 'px';
        el.style.top = sizes.top + 'px';
        //
        el.setAttribute('title', 'Due Date: ' + gantt.templates.task_date(new Date(task.deadline)));
        return el;
    }
    return false;
});


gantt.addTaskLayer(function draw_deadline(task) {
    if (task.actual_start_date) {
        var el1 = document.createElement('div');
        el1.className = 'actual-start-date';
        var sizes1 = gantt.getTaskPosition(task, new Date(task.actual_start_date));
        el1.style.left = sizes1.left + 'px';
        el1.style.top = sizes1.top + 'px';
        el1.setAttribute('title', 'Actual Start Date: ' + gantt.templates.task_date(new Date(task.actual_start_date)));
        return el1;
    }
    return false;
});

gantt.addTaskLayer(function draw_deadline(task) {
    if (task.actual_end_date) {
        var el1 = document.createElement('div');
        el1.className = 'actual-end-date';
        var sizes1 = gantt.getTaskPosition(task, new Date(task.actual_end_date));
        el1.style.left = sizes1.left + 'px';
        el1.style.top = sizes1.top + 'px';
        el1.setAttribute('title', 'Actual End Date: ' + gantt.templates.task_date(new Date(task.actual_end_date)));
        return el1;
    }
    return false;
});

// gantt.config.branch_loading = true;
//
// gantt.load('http://localhost/dwa/index.php/allocation/getSummaryPageData/syllabus/1/1?parent_id=1');
// var dp = new gantt.dataProcessor('http://localhost/dwa/index.php/allocation/getSummaryPageData/syllabus/1/1?parent_id=1');
// dp.init(gantt);
function getSummaryData(data, url) {
	gantt.render();
    url = url || base_url+"index.php/allocation/getSummaryResourcePageData";
    var gantt_table = $("#gantt_here");
    gantt_table.LoadingOverlay("show", {
        text        : "Loading...",
        image       : "",
        maxSize : 40
    });
    $.ajax({
        url: url,
        data: {data: data},
        success: function (result) {
            gantt.config.show_task_cells = false;
            gantt.config.static_background = true;
            gantt.config.branch_loading = true;
            gantt.config.start_date = moment(start_date.val(),"DD-MM-YYYY").format("YYYY-MM-DD");
            gantt.config.end_date = moment(end_date.val(),"DD-MM-YYYY").format("YYYY-MM-DD");
            gantt.clearAll();
            gantt.parse(result);
        },
        complete: function(){
            gantt_table.LoadingOverlay("hide");
        }
    });
}



$(document).ready(function() {


    $('#sch-res-filter-btn').on('click', function() {
        data = {userId: $('#syllabus_owner').val(), startDate: $('.s_dt').val(), endDate: $('.e_dt').val()};
        getSummaryData(data);
    });
    // var url = base_url+"index.php/allocation/getSummaryPageData/syllabus/<?php echo $levelId; ?>/<?php echo !empty($hierarchyId) ? $hierarchyId : 0; ?>/true";
    // getSummaryData(url);
});

gantt.attachEvent("onTaskRowClick",function(id,row){
    // any custom logic here
    //console.log(id);
    var rowData = gantt.getTask(id);
    var url = base_url+"index.php/allocation/getSummaryPageData/syllabus/" + (Number(rowData.level_id_fk) + Number(1)) + "/" + Number(rowData.id) ;
   // console.log(url);
    getSummaryData(url);

});



</script>