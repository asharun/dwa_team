<?php
    $this->load->view('common/allocation/allocation_header');
    $this->load->view('common/allocation/sidenavbar');
    $userId=$this->session->userdata('user_id');

    $tabName = $this->uri->segment(2);
    $levelCode = $this->uri->segment(3);
    $deptId = $this->uri->segment(4);
    $levelId = $this->uri->segment(5);
    $hierarchyId = $this->uri->segment(6);
    $actualMid = $this->input->get('mid');
    $actualMessage = $actualMid ? $actualMid : "this level";
 ?>
<div class="main">
    <?php $this->load->view('common/allocation/allocationtopbar'); ?>

    <hr class="str_hr" style="border-top:2px solid #ddd;">
    <div class="row">
        <div class="col-md-12 c_row">
          <div class='row hid1'>
              <div class='row'>
                <div class="col-md-12" style="margin-bottom: 20px;">
                  <fieldset class="scheduler-border">
                        <legend class="legli">
                          Legend
                        </legend>
                        <div class="col-md-4 col_sp">
                          <i class="glyphicon glyphicon-stop t_stat_4" style=""></i>
                          <span class="fil_val" style="font-size: 12px;">Allocated and Logged</span>
                        </div>
                        <div class="col-md-4 col_sp">
                          <i class="glyphicon glyphicon-stop t_stat_1" style=""></i>
                          <span class="fil_val" style="font-size: 12px;">Allocated but no logs</span>
                        </div>

                        <div class="col-md-4 col_sp">
                          <i class="glyphicon glyphicon-stop t_stat_2" style=""></i>
                          <span class="fil_val" style="font-size: 12px;">Not Allocated</span>
                        </div>
                  </fieldset>
                </div>
                <div class='col-md-12'>
                  <div id="activity-logs-details">
                        <div id="activity-logs"></div>
                    </div>
                </div>
              </div>
          </div>
        </div>
    </div>


    <div style="padding: 10px; position:absolute; top:20px; left:0px">
      <div id="notif-toast"></div>
      <div id="warning-notif-toast"></div>
    </div>
</div>
<?php
  $this->load->view("common/allocation/allocation_footer.php");
?>
<script>
  window.exportResource = '';
  window.isExport = getQueryStringValue('exportSelector') == 'true' ? true : false;
  window.deptID = '<?php echo $deptId; ?>';
  window.currentDate = '<?php echo $currentDate; ?>';
  window.allLevelData = <?php echo json_encode($allLevelData); ?>;
  window.collapsedParentLevelId = '';
  var mid = activity_id = actualMessage = '';
  actualMessage = '<?php echo $actualMessage ?>';

  $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
    window.userId = <?php echo $userId; ?>;
    $("#activity-logs").dxDataGrid({
        dataSource: {
            store: midActivity
        },
        showBorders: true,
        noDataText:`No activities at ${actualMessage}`,
        paging: {
          pageSize: 10
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 20],
            showInfo: true
        },
        searchPanel: {
            visible: true,
            highlightCaseSensitive: true
        },
        columns: [
          {
            dataField: "activity_name",
            caption: "Activity"
			//,width:500
          },
          {
            dataField: "mid",
            caption: "MID Code"
			//,width : 110
          },
          {
            dataField: "planned_s_dt",
            caption: "Planned Start Date",
            dataType:"date",
            format : "dd-MM-yyyy"
          },
          {
            dataField: "planned_e_dt",
            caption: "Planned End Date",
            dataType:"date",
            format : "dd-MM-yyyy"
          },
          {
            dataField: "act_s_dt",
            caption: "Actual Start Date",
            dataType:"date",
            format : "dd-MM-yyyy"
          },
          {
            dataField: "act_e_dt",
            caption: "Actual End Date",
            dataType:"date",
            format : "dd-MM-yyyy"
          },
          {
            dataField: "progress",
            caption: "Progress",
          },
        ],masterDetail: {
          enabled: true,
          template: function(container, options) {
              mid = options.data.mid;
              activity_id = options.data.hierarchy_id;
            $("<div>")
              .dxDataGrid({
                  showBorders: true,
                  noDataText:"No logs at given activity",
                  searchPanel: {
                      visible: true,
                      highlightCaseSensitive: true
                  },
                  columns: [
                    {
                      dataField: "full_name",
                      caption: "User Name",
                    },
                    {
                      dataField: "projected_xp",
                      caption: "Projected XP",
                      dataType:"number"
                    },
                    {
                      dataField: "conf_xp",
                      caption: "Confirm XP",
                      dataType:"number"
                    },
                    {
                      dataField: "w_done",
                      caption: "Work Done",
                      dataType:"number"
                    },
                    {
                      dataField: "wu_name",
                      caption: "Work Unit"
                    },
                    {
                      dataField: "start_date",
                      caption: "Start Date",
                      dataType:"date",
                      format : "dd-MM-yyyy"
                    },
                    {
                      dataField: "end_date",
                      caption: "End Date",
                      dataType:"date",
                      format : "dd-MM-yyyy"
                    },
                    {
                      dataField: "ind",
                      caption: "Status",
                      allowExporting:false,
                      allowFiltering:false,
                      allowSorting:false,
                      cellTemplate: function (container, options) {
                        var displayValue = options.displayValue,status = '';

                        if(displayValue === "Allocated"){
                          status = "t_stat_4";
                        }
                        if(displayValue == "Not Logged"){
                          status = "t_stat_1";
                        }
                        if(displayValue === "Not Allocated"){
                          status = "t_stat_2";
                        }
               $("<div class='text-center'>").append(`<i class="glyphicon glyphicon-stop ${status}" style=""></i>`).appendTo(container);
                    }
                    }
                  ],
                  dataSource: {
                    store : activityLogs
                  }
              }).appendTo(container);
          }
      },
    }).dxDataGrid("instance");
  });

  //MID WISE ACTIVITY SHOW
  var midActivity = new DevExpress.data.CustomStore({
    load: function (loadOptions) {
        var deferred = $.Deferred(),
            args = {};

        if (loadOptions.sort) {
            args.orderby = loadOptions.sort[0].selector;
            if (loadOptions.sort[0].desc)
                args.orderby += " desc";
        }

        args.skip = loadOptions.skip;
        args.take = loadOptions.take;
        var midActivityURL = '<?php echo $allocationAjaxUrl; ?>' + '/';

        if (window.collapsedParentLevelId != '') {
              midActivityURL = base_url + 'index.php/allocation/getMidWiseActivity/' +
               (Number(window.collapsedParentLevelId) + Number(1)) +
               '/';
               window.collapsedParentLevelId = '';
        }
        $.ajax({
            url: midActivityURL,
            dataType: "json",
            data: args,
            success: function(result) {
              deferred.resolve(result.items, { totalCount: result.totalCount });
            },
            error: function() {
              deferred.reject("Data Loading Error");
            },
            timeout: 5000
        });
      return deferred.promise();
    }
  });

  //ACTIVITY LOG DATA
  var activityLogs = new DevExpress.data.CustomStore({
    load: function (loadOptions) {
        var deferred = $.Deferred(),
            args = {};

        if (loadOptions.sort) {
            args.orderby = loadOptions.sort[0].selector;
            if (loadOptions.sort[0].desc)
                args.orderby += " desc";
        }

        args.mid = mid;
        args.activity_id = activity_id;
        args.skip = loadOptions.skip;
        args.take = loadOptions.take;
        var activityLogURL =  base_url + 'index.php/allocation/getActivityLog/';
        $.ajax({
            url: activityLogURL,
            dataType: "json",
            data: args,
            success: function(result) {
              deferred.resolve(result.items, { totalCount: result.totalCount });
            },
            error: function() {
              deferred.reject("Data Loading Error");
            },
            timeout: 5000
        });
      return deferred.promise();
    }
  });
</script>