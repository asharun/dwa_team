<?php 
if ($_SERVER['HTTP_HOST'] == "byjusdwa.com")
{
   $url = "https://www." . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
   header("Location: $url");
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
	
  </head>
  
<body>
	<header id="header_2">
		<!--kode top bar start-->
		<div class="top_bar_2">
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<div class="edu2_ft_logo_wrap">
							<a href="<?= base_url() ?>index.php/User/home_page">
								<img class="half_w" src="<?= getAssestsUrl() ?>images/byjus_new.png" alt="">
								</a>
							</div>
						</div>
						<div class="col-md-8">
							<h3 class='ev-c1'>
								<center>
									<span>Work Tracker </span>
									<i class="glyphicon glyphicon-th-list" style="font-size: 21px;"></i>
								</center>
							</h3>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
			</div>
		</header>
		<div class="container form_wrap">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="login-panel panel">
						<div class="panel-heading pan_head">
							<h3 class="panel-title">Login</h3>
						</div>
						<div class="panel-body p_box">
							<?php            
             				
				echo '<a href='.$login_url.'><img class="g_sign" src="'.getAssestsUrl().'images/gplus_sign.png" alt=""></a>';?>
									</div>
								</div>
							</div>
						</div>
					</div>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
		<script>var base_url = '<?php echo base_url() ?>';</script>
		<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	</body>
</html>