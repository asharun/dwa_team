<?php
$user_id=$this->session->userdata('user_id');
  $file_nm="Reports";
$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	
$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);
if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
}
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/bootstrap-table.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>"  type="text/css">
	<style>
	.multiselect-container {
        width: 100% !important;
		max-height: 400px;
    overflow: auto;
    }
	
	.multiselect.dropdown-toggle,.bt_cont_pt{
		width:100%;
	}
	</style>
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">
	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Reports">Overview</button>
						<button class='stab_stages_2 stab_dis_selec' ch="PT_Report_Card">Report Card</button>
						<button class='stab_stages_2' ch="PT_Pies">Pies</button> 						
						<!--button class='stab_stages_2' ch="PT_Trends">Trends</button> 						
						<button class='stab_stages_2' ch="PT_Stats">Stats</button--> 						
						<button class='stab_stages_2' ch="PT_Feedback">Feedback</button>					
					</div>
				</div>													
				<div class='row hid'>	
						<div class='col-md-12'>						
						<?php 	
							$st_dt= date('d-M-y', strtotime($start_dt));
							$en_dt= date('d-M-y', strtotime($end_dt));	
							$titl3= "(".$st_dt." to ".$en_dt.")";
							$day_fm1=date('d-M-Y',strtotime($start_dt));
							$day_fm2=date('d-M-Y',strtotime($end_dt));
						?>
						<div class='row'>	
									<div class='col-md-12'>	
										<div class='row third-row head row_style_1 text-center'>
								<?php
										echo "<div class='col-md-2'>
															<label class='l_font_fix_3'>Choose Level:</label>
																<select id='sel_2345' class='selectpicker form-control' title='Nothing Selected' data-live-search='true'>																		
														
															<option data-hidden=true></option>";
															if(!isset($level_opt))
															{
																$level_opt=1;
															}
															$lel_val=array("Activity","Projects","Category","Element","Action","Trade","Date","Raw Data","MID");
															foreach ($lel_val as $key=>$value)
															{
																$sel='';
																	if($level_opt==$key)
																	{
																		$sel='selected';																		
																	}
																echo "<option value='".$key."' ".$sel.">".$value."</option>";
															}
															
																
														echo "</select>
															</div>";
												echo "<div class='col-md-4'>
												<label class='l_font_fix_3' style='width:100%'>Choose Employee</label>	";
												echo "<select id='sel_emp' class='form-control' title='Nothing Selected'  multiple='multiple' data-live-search=true'>";
												if($pro_sel_dta)
															{
															
															$rt_val=explode(",",$pro_sel_val);
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if(in_array($row2['sel_1_id'],$rt_val))
																	{
																		$sel='selected';																		
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
															
												echo "</select>";											
										echo "</div>";
								echo "<div class='col-md-2'>
											<span>
												<label class='l_font_fix_3'>Start Date</label>	
												<input id='t_dtpicker' class='s_dt form-control date-picker' dt='".$start_dt."' value='".$day_fm1."' />
											</span>
										</div>
										<div class='col-md-2'>
											<span>
												<label class='l_font_fix_3'>End Date</label>	
												<input id='t_dtpicker2' class='e_dt form-control date-picker' dt='".$end_dt."' value='".$day_fm2."' />
											</span>
										</div>";
										
												?>

									<div class='col-md-2'>
										<label class='l_font_fix_3 invisible' style='width:100%;'>Update</label>	
										<button class='btn add_but gre_but change_rep' type='button'>Submit</button>
										</div>
									</div>
							</div>
						</div>
								<hr class="st_hr2">
					<div id="toolbar"> 
							<select class="form-control">
									<option value="">Export Page</option>
									<option value="all">Export All</option>
									<option value="selected">Export Selected</option>
							</select>						
					</div>
							<table class="display table table-bordered table-responsive" data-show-footer="true" data-footer-style="footerStyle" data-filter-control="true"  data-show-export="true" data-checkbox-header="false" data-toolbar="#toolbar" id="table" data-search-time-out=500 data-pagination="true" data-search="true">
										<thead>
											<tr>
											
											<?php
											  echo '<th data-class="l_font_fix_3" data-formatter="runningFormatter">#</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="full_name">Member Name</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="val_name">'.$lel_val[$level_opt].'</th>';
											  if($level_opt==7)
											  {		 
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="activity_name">Activity</th>';
											  }	
											  if($level_opt==0 || $level_opt==7)
											  {											  
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="wu_name">Work Units</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="w_done">Qty Done</th>';
											  }											  										  
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="proj_xp" data-footer-formatter="totalProjectedXp">Projected XPs</th>';					  
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="act_xp" data-footer-formatter="totalConfirmedXp">Confirmed XPs</th>';
											  
											  if($level_opt==5)
											  {
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="quality_score" data-formatter="score_name">Quality Score</th>';
											  }
											
											if($level_opt==1)
											{
												 echo '<th data-sortable="true" data-class="l_font_fix_3" data-filter-control="input" data-field="totl_xp">Total Confirmed XPs</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="in_time">Median In Time</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="esp_wrk_hrs">Avg Office Work Hours</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="sapience_on">Avg Sapience On</th>';
											 // echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="sapience_off">Avg Sapience Off</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="attendance_cnt">Attendance</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="cu">% CU</th>';
											}
											
											if($level_opt==6)
											{
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="in_time">In Time</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="esp_wrk_hrs">Office Work Hours</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="sapience_on">Sapience On</th>';
											 // echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="sapience_off">Sapience Off</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="attendance_cnt">Attendance</th>';
											  echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="cu">% CU</th>';
											}
											// if($level_opt==1)
											// {
											 // echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="grade">Grade</th>';
											  // echo '<th data-sortable="true" data-class="l_font_fix_3" data-field="grade_remarks">Remarks</th>';
											// }
											  ?>
											</tr>
										</thead>
									</table>								
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-multiselect.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/feedback.js?v=<?= v_num() ?>"></script>
	
	<script>
	function runningFormatter(value, row, index) {
index++; {
return index;
}
}



	function score_name(value, row) {
		var grades=["","Needs Awareness","Needs Improvement","Meets Expectation","Exceeds Expectation","Strongly Exceeds Expectation"];
	if(value>0 && value<=5)
	{
return grades[value];
}else{
	return "";
}
}

function totalConfirmedXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "Total= "+total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
            }
			
			
			function totalProjectedXp(data) {
                var field = this.field;
                var total_sum = data.reduce(function(sum, row) {
                      //  console.log(sum);
                    return (sum) + (parseFloat(row[field]) || 0);
                }, 0);
                return "Total= "+total_sum.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
            }
			
			
			function footerStyle(value, row, index) {
          return {
            css: { "font-weight": "bold" }
          };
        }


	$('#toolbar').find('select').change(function () {
            $('#table').bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });

	$("#sel_emp").multiselect({
		enableCaseInsensitiveFiltering: true,
		includeSelectAllOption: false,
		selectAllJustVisible: true,
		numberDisplayed: 1
	});
	
	$("body").on("focus", ".s_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					autoclose:true,
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						$(this).attr('dt',date_v);
						}
					});
	});
	
	$("body").on("focus", ".e_dt",function(){
		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					autoclose:true,
					startDate:$(".s_dt").val(),
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{							
							var ele=Date.parse($(this).val()); 
							var date_v=moment(ele).format("YYYY-MM-DD");
							$(this).attr('dt',date_v);
						}
					});	
	});
	
	var lev=0;
	if($("#sel_2345").find("option:selected").val())
	{
		lev=$("#sel_2345").find("option:selected").val();
	}
	
	$("body").on("click", ".change_rep",function(){
		var sdt=$("body").find(".s_dt").attr('dt');
		var edt=$("body").find(".e_dt").attr('dt');
		var lvl=$("body").find("#sel_2345").val();
		if(sdt && edt && sdt<=edt)
		{
		window.location = base_url+"index.php/User/load_view_f?a=PT_Report_Card&s_dt="+sdt+"&e_dt="+edt+"&lvl="+lvl;
		}
	});
	
	
	$("body").on("change","#sel_emp",function(){
		if($(this).val())
		{
			$.ajax({				
				url: base_url+"index.php/User/load_emp_string",
				type: 'post',
				data : {param:"Personal_Trends",pt_emp:$(this).val().toString()}
			});
		}
	});
	
	$.ajax({
       url: base_url+"index.php/User/load_table_boots?a=PT_Report_Card&s_dt="+$("body").find(".s_dt").attr('dt')+"&e_dt="+$("body").find(".e_dt").attr('dt')+"&lvl="+lev,
       dataType: 'json',
       success: function(response) {
		  window.weeklyBootstrapTable = $('#table').bootstrapTable({
              data: response,
			  stickyHeader: true,
			   onPostBody:function() {
			 $("body").find('.fixed-table-body').scroll(function(){
				//$("body").find('#table-sticky-header-sticky-header-container').find('thead').parent().scrollLeft($(this).scrollLeft());
				$("body").find('.fix-sticky').find('thead').parent().scrollLeft($(this).scrollLeft());
				});
			  }
        });
       }
    });
	
	
	</script>
  </body>
</html>