<?php
$user_id=$this->session->userdata('user_id');

//echo $user_id;
$file_nm='Floor_View';

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
} 
 ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
	<meta name="google" content="notranslate">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
		<link href="<?= getAssestsUrl() ?>css/bootstrap-table.min.css?v=<?= v_num() ?>" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-sticky-header.css?v=<?= v_num() ?>"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-editable.css"  type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap-table-filter-control.css?v=<?= v_num() ?>"  type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				
				<div class="col-md-10 c_row">
				<div class='row row_style_1'>
			<div class="col-md-10">	
				<fieldset class="scheduler-border">
					<legend class='legli'>
						Legend
					</legend>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_1" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Audit Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_2" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Review Pending</span>
					</div>
					<div class="col-md-3 col_sp">										
					<i class="glyphicon glyphicon-stop t_stat_3" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmation Pending</span>					
					</div>
					<div class="col-md-3 col_sp">
					<i class="glyphicon glyphicon-stop t_stat_4" style=''></i>
					<span class="fil_val" style="font-size: 12px;">Confirmed</span>					
					</div>
			</fieldset>				
			</div>			
			</div>
				<div class='row hid1'>	
					<div class='col-md-12'>
					
					<button class='stab_stages_2' ch="Floor_View">Weekly Summary</button>
						<button class='stab_stages_2 ' ch="FV_Wow">Productivity WoW</button>	
						<button class='stab_stages_2 ' ch="FV_Spread">Trends</button>	
						<button class='stab_stages_2' ch="FV_Feed">Scatter</button>	
						<button class='stab_stages_2 ' ch="FV_Summary">Descriptives</button>
						<button class='stab_stages_2' ch="FV_Drilldown">Pies</button>					
						<button class='stab_stages_2' ch="FV_Member_Info">Weekly Report</button>
						<button class='stab_stages_2 stab_dis_selec' ch="FV_Contributor">Projects & Members</button>						
					</div>
				</div>
					<?php 	
						if(($date_view))
						{
							$day = date('Y-m-d H:i:s',strtotime($date_view));
						}else
						{
							$day = date('Y-m-d H:i:s');
						}
							$day_w = date('w',strtotime($day));				
							if($day_w==0)
							{
								$day_w=7;								
							}
							$day_fm=date('d-M-Y',strtotime($day));
							$week_start = date('Y-m-d H:i:s', strtotime($day.' -'.($day_w-1).' days'));
							$w_prev=date('Y-m-d', strtotime($day.' -'.($day_w).' days'));
							$w_nxt = date('Y-m-d', strtotime($day.' +'.(8-$day_w).' days'));							
							$week_st = date('d-M', strtotime($day.' -'.($day_w-1).' days'));							
							$week_end = date('d-M', strtotime($day.' +'.(7-$day_w).' days'));					
					?>
					<div class='row hid'>	
						<div class='col-md-12'>	
							<div class='row third-row head'>												
												<?php
												echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3'>Choose Date: </label>	
															<input id='t_dtpicker' class='ch_dt date_fm date-picker' value='".$day_fm."' />
														</span>
													</div>
													<div class='col-md-6 cur-month text-center'><span>Week (".$week_st." to ".$week_end.")</span>
													</div>";
												$titl1='';
												$titl2='';
												$titl3='';												
												?>
							</div>							
							<div class='row row_style_1' id='c_find'>	
									<div class='col-md-12'>					
												<a class='arr pull-left' ch='Floor_View' w_val="<?= $w_prev ?>">&laquo; Prev</a>						
												<a class='arr pull-right' ch='Floor_View' w_val="<?= $w_nxt ?>">Next&raquo;</a>					
									</div>														
							</div>
							<div class='row row_style_1'>	
							<div class='col-md-4'>
								<label class='l_font_fix_3'>Choose Dept:</label>
								<select id='sel_dept_1' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
								<?php
								foreach ($dept_val as $row)
								{
									$sel='';
										if($dept_opt==$row['dept_id'])
										{
											$sel='selected';																		
										}
									echo "<option value='".$row['dept_id']."' ".$sel.">".$row['dept_name']."</option>";
								}
								?>
								</select>
							</div>
							<div class='col-md-4'>
								<label class='l_font_fix_3'>Choose Level:</label>
									<select id='sel_2345' class='selectpicker form-control' title="Nothing Selected" data-live-search="true">																		
								<?php 
								echo '<option data-hidden="true"></option>';
								if(!$level_opt || $level_opt>=2)
								{
									$level_opt=0;
								}								
								$lel_val=array("Members","Contributors");
								
								foreach ($lel_val as $key=>$value)
								{
									$sel='';
										if($level_opt==$key)
										{
											$sel='selected';																		
										}
									echo "<option value='".$key."' ".$sel.">".$value."</option>";
								}
								?>
								</select>
								</div>	
						</div>
				
                       
                       
							  <table id="contribution-table" class="table table-bordered" data-sortable='true' data-align='center' data-valign='middle' data-show-columns='true' data-escape='false' data-show-export='true' data-toolbar='#toolbar' data-search-time-out=500 data-search='true'>
           <thead>
                <tr>
                    <?php foreach($projectList as $p_id=>$project): ?>
                        <th data-class="l_font_fix_3" data-sortable="true" data-field="<?php echo $p_id ?>">
                            <?php echo $project. " (<span class='badge badge-success' style='background-color:#03a9f4;'>".sizeof($contributorList[$p_id])."</span>) " ?>
                        </th>
                    <?php endforeach; ?>
                </tr>
           </thead>

           <tbody>
           <?php 
		  // print_r(($contributorList));
           for($i=0;$i<$max_cnt;$i++){
                    echo '<tr>';                        
                    foreach ($contributorList as $p_name => $row)
                    {
                            if(array_key_exists($i,$row))
                            {
                                echo  "<td>".$row[$i]."</td>"; 
                            }else
                            {
                           echo "<td></td>";
                            }
                         
                    }
                    echo "</tr>";
             } 
			 ?>
           </tbody>
        </table>
					</div>			        
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script>var dep_opt = '<?php echo $dept_opt ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-sticky-header.js?v=<?= v_num() ?>"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-editable.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-export.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/tableExport.js"></script>	
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-table-filter-control.js?v=<?= v_num() ?>"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
	<script>    
	$("body").on("focus", ".ch_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						var str='';
						var lvl=$("body").find("#sel_2345").val();
						var dept=$("body").find("#sel_dept_1").val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
						window.location = base_url+"index.php/User/load_view_f?a=FV_Contributor&date_view="+date_v+str;
						}
					});	
	});
	
	var ele=Date.parse($("body").find(".ch_dt").val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
	var lev=0;
	
	if($("#sel_2345").find("option:selected").val())
	{
		lev=$("#sel_2345").find("option:selected").val();
	}
	
	$("body").on("change","#sel_dept_1",function(){
		if($(this).val())
		{
						var ele=Date.parse($("body").find(".ch_dt").val()); 
						var date_v=moment(ele).format("YYYY-MM-DD");
						var str='';
						var lvl=$("body").find("#sel_2345").val();
						var dept=$(this).val();
						if(dept)
						{
							str=str+"&dept="+dept;
						}
						if(lvl)
						{
							str=str+"&lvl="+lvl;
						}
						window.location = base_url+"index.php/User/load_view_f?a=FV_Contributor&date_view="+date_v+str;			
		}	
	});
	
	
	$("body").on("change","#sel_2345",function(){
		if($(this).val())
		{
			var ele=Date.parse($(".ch_dt").val()); 
			var date_v=moment(ele).format("YYYY-MM-DD");
			//var pro_v=$("body").find("#sel_1234").val();
			var dept=$("body").find("#sel_dept_1").val();
						var str='';
						str=str+"&lvl="+$(this).val();
						
						if(dept)			
						{
							str=str+"&dept="+dept;
						}						
			window.location = base_url+"index.php/User/load_view_f?a=FV_Contributor&date_view="+date_v+str;
		}
	});
	
	 $('#contribution-table').bootstrapTable({
			pageSize:20,
			stickyHeader:true,
			exportDataType:'all',
		}); 
		
	// $.ajax({
       // url: base_url+"index.php/User/load_table_boots?a=FV_Contributor&date_view="+date_v+"&lvl="+lev+"&dept="+dep_opt,
       // dataType: 'json',
       // success: function(response) {
         
       // },
       // error: function(e) {
           // console.log(e.responseText);
       // }
    // });
	
	// Floor View Contributer Scrips
		
	
			// if (getQueryStringValue('type') == 'member') {
				// $('.mem-toggle').removeClass('btn-info');
				// $('.mem-toggle').addClass('btn-primary');
			// } else {
				// $('.contrib-toggle').removeClass('btn-info');
				// $('.contrib-toggle').addClass('btn-primary');
			// }
			
			// Remvove user from list while searching
			$('div.search > input').on('keyup', function() {
				var query = $(this).val();
				setTimeout(function(){ 
					$('#contribution-table').find('tbody>tr').each(function(value) {
						$(this).find('td').each(function() {
							if (!$(this).text().toLowerCase().includes(query.toLowerCase())) {
								$(this).html('');
							}
						});
					})
				 }, 1000);
			});

			$('.contributor-toggle').on('click', function() {
				var type = $(this).attr('data-type');
				window.location = window.location.href + '&type=' + type;
			});
	</script>
  </body>
</html>