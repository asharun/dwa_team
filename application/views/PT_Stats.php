<?php
$user_id=$this->session->userdata('user_id');
 //echo $user_id;
   $file_nm="Reports";

$a_right1=str_replace(' ','_',$this->session->userdata('access')); 			
$access_str1=explode("|",$a_right1);	

$a_right=$this->session->userdata('access'); 			
$access_str=explode("|",$a_right);

if(!$user_id || !in_array($file_nm,$access_str1)){
  redirect('user/login_view');
} 
 ?>
 
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" ">
    <meta name="robot" content="index,follow">
    <meta name="copyright" content="Copyright 2015 Think &amp; Learn Pvt Ltd. All Rights Reserved.">
    <meta name="revisit-after" content="30">
    <title>Work Tracker</title>
	<link rel="icon" type="http://byjusclasses.com/gmat1/images/png" href="<?= getAssestsUrl() ?>images/tnl132.png">
	
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/bootstrap.min.css">
	<link href="<?= getAssestsUrl() ?>css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> 
	<link href="<?= getAssestsUrl() ?>css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="<?= getAssestsUrl() ?>css/export.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= getAssestsUrl() ?>css/style.css?v=<?= v_num() ?>">
		
  </head>
  <body>
  <?php 
  $this->load->view("Header.php");  
  ?>
<div class="desc">

	<div class="ic_cont">
		<div class="row ma_row">
			<?php 
				$data['file_nm']=$file_nm;
				$this->load->view('common/sidebar',$data);
			?>	
				<div class="col-md-10 c_row">
				<div class='row hid1'>	
					<div class='col-md-12'>
						<button class='stab_stages_2' ch="Reports">Overview</button>
						<button class='stab_stages_2 ' ch="PT_Report_Card">Report Card</button>
						<button class='stab_stages_2' ch="PT_Pies">Pies</button> 						
						<button class='stab_stages_2' ch="PT_Trends">Trends</button> 						
						<button class='stab_stages_2 stab_dis_selec' ch="PT_Stats">Stats</button> 						
						<button class='stab_stages_2' ch="PT_Feedback">Feedback</button>	
					</div>
				</div>
					<?php 	
						if(($s_dt) && ($e_dt))
												{
													$start_dt1 = date('d-M-Y', strtotime($s_dt));
													$end_dt1 = date('d-M-Y', strtotime($e_dt));
												}else
												{
													$day = date('Y-m-d H:i:s');
													$day_w = date('w',strtotime($day));
													if($day_w==0)
													{
														$day_w=7;								
													}							
													$week_end = date('Y-m-d H:i:s', strtotime($day.' +'.(7-$day_w).' days'));
													
													$week_start = date('Y-m-d H:i:s', strtotime($week_end.' -1 month'));
													$week_st_w = date('w',strtotime($week_start));
													if($week_st_w==0)
													{
														$week_st_w=7;								
													}							
													$start_dt1 = date('d-M-Y', strtotime($week_start.' -'.($week_st_w-1).' days'));
													$end_dt1 = date('d-M-Y',strtotime($week_end));
													
													$s_dt = date('Y-m-d', strtotime($start_dt1));
													$e_dt = date('Y-m-d', strtotime($end_dt1));
												}		
							
							
					?>
					<div class='row hid'>	
						<div class='col-md-12'>	
			
				<!--div class='row row_style_1 text-center'>	
							<div class='col-md-12'>					
										<button class='stab_stages_2 stab_dis_selec' ch="PT_Stats">Contributors</button> || 
										<button class='stab_stages_2 in_stages' ch="Graph_Sum_Sc">Members</button>
							</div>														
					</div-->
					
					
			<div class='row third-row head' style='margin-top: 10px;'>
												
												<?php
												echo "
										<div class='col-md-12 cur-month text-center'><span>Summary (".$start_dt1." to ".$end_dt1.")</span>
												</div>	";
												$titl='';
												$titl1='';
												?>
							</div>
							
									
									<div class='row row_style_1  scroll-head'>
									<?php
														$pto='';
														echo "<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3' style='width:100%'>Start Date: </label>	
															<input id='t_dtpicker' class='s_dt date_fm date-picker' dt='".$s_dt."' value='".$start_dt1."' />
														</span>
													</div>
													<div class='col-md-3'>
														<span>
															<label class='l_font_fix_3' style='width:100%'>End Date: </label>	
															<input id='t_dtpicker' class='e_dt date_fm date-picker' dt='".$e_dt."' value='".$end_dt1."' />
														</span>
													</div>
													";
														echo "<div class='col-md-4'>
												<label class='l_font_fix_3' style='width:100%'>Choose Employee</label>	";
												echo "<select id='sel_emp' class='selectpicker form-control' title='Nothing Selected'  data-live-search=true'>";
												if($pro_sel_dta)
															{
																
															
															//$rt_val=explode(",",$pro_sel_val);
															foreach ($pro_sel_dta as $row2)
															{
																$sel='';
																	if($row2['sel_1_id']==$pro_sel_val)
																	{
																		$sel='selected';	
																		$pto=$row2['sel_1_name'];
																	}
																echo "<option value='" . $row2['sel_1_id'] .  "' ".$sel.">" . $row2['sel_1_name'] . "</option>";
															}
															}
															
												echo "</select>";											
										echo "</div>";							
										echo "</div>";
										
										
			// if($avg)
				// {
					
					
				// }
				echo '<hr class="st_hr2">';
			
			echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Stats #1:</label> ";
						echo "</div>";
					echo "</div>";
					echo "<div class='row row_style_1'>";
							if($sum)
							{
							echo "<div class='col-md-12'>
								<table class='table table-bordered' style='table-layout:fixed;'><th class='l_font_fix_3'>Total XP</th><th  class='l_font_fix_3' >Avg XP</th><th  class='l_font_fix_3' >Min XP per day</th><th  class='l_font_fix_3' >Max XP per day</th>";
								
							
							  	echo "<tr>";
										echo "<td>".$sum."</td>";
										echo "<td>".$avg." XP</td>";
										echo "<td>".$max." XP</td>";
										echo "<td>".$min." XP</td>";
								echo "</tr>";
							
							echo "</table>
								</div>";
							}
							
							echo "</div>";
							echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Stats #2:</label> ";
						echo "</div>";
					echo "</div>";
							
							echo "<div class='row row_style_1'>";
							if($act_n)
							{
							echo "<div class='col-md-12'>
								<table class='table table-bordered' style='table-layout:fixed;'><th class='l_font_fix_3'>Activity Name</th><th  class='l_font_fix_3' style='width:25%;' >Max XP</th>";
											foreach($act_n AS $pack)
											{											
							  	echo "<tr>";
										echo "<td>".$pack['a_name']."</td>";
										echo "<td>".$pack['xp_val']." XP</td>";										
								echo "</tr>";
											}
							
							echo "</table>
								</div>";
							}
							
							echo "</div>";
							
							echo "<div class='row row_style_1'>";
						echo "<div class='col-md-6'>";
							echo "<label class='l_font_fix_3'>Stats #3:</label> ";
						echo "</div>";
					echo "</div>";
							echo "<div class='row row_style_1'>";
							if($act_n2)
							{
							echo "<div class='col-md-12'>
								<table class='table table-bordered' style='table-layout:fixed;'><th class='l_font_fix_3'>Activity Name</th><th  class='l_font_fix_3' style='width:25%;' >Highest Score</th>";
											foreach($act_n2 AS $pack)
											{											
							  	echo "<tr>";
										echo "<td>".$pack['a_name']."</td>";
										echo "<td>".$pack['score']."</td>";										
								echo "</tr>";
											}
							
							echo "</table>
								</div>";
							}
							
							echo "</div>";
							// if($tm_data_min)
							// {
								// echo "<div class='col-md-6'>
								// <table class='table table-bordered' style='table-layout:fixed;'><th class='l_font_fix_3'>Lowest Contributor</th><th  class='l_font_fix_3' style='width:25%;'>Lowest XP</th>";
							// foreach($tm_data_min AS $key1=>$row1)
							// { 
							  	// echo "<tr>";
										// echo "<td>".$key1."</td>";
										// echo "<td>".$row1." XP</td>";
								// echo "</tr>";
							// }
							// echo "</table>
								// </div>";
							// }
						echo "</div>";	
						
			// if($Quart1 || $Quart3)
				// {
					// echo "<div class='row row_style_1'>
							// <div class='col-md-6'>
								// <table class='table table-bordered' style='table-layout:fixed;'><th class='l_font_fix_3'>Bottom Quartile Contributor</th><th  class='l_font_fix_3' style='width:25%;'>XP</th>";
							// foreach($Quart1 AS $key1=>$val1)
							// { 
									// echo "<tr>";
										// echo "<td>".$key1."</td>";
										// echo "<td>".$val1." XP</td>";
									// echo "</tr>";
							// }
							// echo "</table>
								// </div>
								// <div class='col-md-6'>
								// <table class='table table-bordered' style='table-layout:fixed;'><th class='l_font_fix_3'>Top Quartile Contributor</th><th  class='l_font_fix_3' style='width:25%;'>XP</th>";
							// foreach($Quart3 AS $key3=>$val3)
							// { 
							 // echo "<tr>";
										// echo "<td>".$key3."</td>";
										// echo "<td>".$val3." XP</td>";
							 // echo "</tr>";
							// }
							// echo "</table>
								// </div>
						// </div>";
						// }
						// echo "<hr class='st_hr3'>";
			// echo "<div class='row row_style_2'><div class='col-md-12'><div class='no_dt hide'>No Records!</div></div></div>";
						// echo "<div class='data'>";
						// //print_r($proj_xp);
						// if($Q1per || $Q3per)
							// {
							// $tm_data1=1;
									// echo "<h5 class='hide params1' titl='".$titl."' act_id='".$titl1."' user_id='Share of Bottom Quartile' >".$Q1per."</h5>";
									// echo "<h5 class='hide params2' user_id='Share of Top Quartile' >".$Q3per."</h5>";
									
							// }else
						// {
							// $tm_data1=0;
							// $tm_data=null;
						// }
								// echo "</div>";
							// echo "<div class='canvas_r text-center'>";
							// echo '<div style="width: 100%; height: 400px;" id="chartdiv"></div>';
		
			?>
			</div>
			</div>
			        
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/waypoints-min.js"></script>
	<script src="<?= getAssestsUrl() ?>js/bootstrap-select.min.js" type="text/javascript"></script>
	<script>var base_url = '<?php echo base_url() ?>';</script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/moment.min.js"></script>
<script type="text/javascript" src="<?= getAssestsUrl() ?>js/Date.js"></script>
	 <script src="<?= getAssestsUrl() ?>js/amcharts.js" type="text/javascript"></script>
     <script src="<?= getAssestsUrl() ?>js/serial.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/light.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/responsive.min.js" type="text/javascript"></script>
	 <script src="<?= getAssestsUrl() ?>js/export.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= getAssestsUrl() ?>js/main_script.js?v=<?= v_num() ?>"></script>
<script>


	$("body").on("focus", ".s_dt",function(){		
	$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
						if($(this).val())
						{
						var ele=Date.parse($(this).val()); 
						var sdt=moment(ele).format("YYYY-MM-DD");
						$(this).attr('dt',sdt);
						var ele1=Date.parse($('.e_dt').val()); 
						var edt=moment(ele1).format("YYYY-MM-DD");
						
						if(ele && ele1 && (ele<ele1))
						{													
						window.location = base_url+"index.php/User/load_view_f?a=PT_Stats&s_dt="+sdt+"&e_dt="+edt;
						}
						}
					});	
	});
	
		$("body").on("focus", ".e_dt",function(){		
		$(this).datepicker({
					format: 'dd-M-yyyy',							
					yearRange: "-1:+1",
					weekStart:1
			}).on('changeDate', function(e) {
				if($(this).val())
						{
						var ele=Date.parse($('.s_dt').val()); 
						var sdt=moment(ele).format("YYYY-MM-DD");
						
						var ele1=Date.parse($(this).val()); 
						var edt=moment(ele1).format("YYYY-MM-DD");	
						$(this).attr('dt',edt);						
						if(ele && ele1 && (ele<ele1))
						{								
						window.location = base_url+"index.php/User/load_view_f?a=PT_Stats&s_dt="+sdt+"&e_dt="+edt;
						}
						}
					});	
			});

			
			
	$("body").on("change","#sel_emp",function(){
		if($(this).val())
		{
			var sdt=$("body").find(".s_dt").attr('dt');
						var edt=$("body").find(".e_dt").attr('dt');
						if(sdt && edt && sdt<=edt)
						{
				$.ajax({				
				url: base_url+"index.php/User/load_emp_string",
				type: 'post',
				data : {param:"Personal_Trends",pt_emp:$(this).val().toString()},
				success: function(response) {						
						window.location = base_url+"index.php/User/load_view_f?a=PT_Stats&s_dt="+sdt+"&e_dt="+edt;
						
					}
			});		
						}
		}
	});
	
	// $("body").on("change","#sel_dept_1",function(){
		// if($(this).val())
		// {			
			// $.ajax({				
				// url: base_url+"index.php/User/load_proj_data",
				// type: 'post',
				// data : {param:"Project",dept:$(this).val()},
				// success: function(response){				
					// $('#sel_1234').html(response).selectpicker('refresh');
				// }
			// });
		// }	
	// });
	
     
  $(window.document).scroll(function() {
        var windowtop = $(window).scrollTop();

        $("body").find(".scroll-head").each(function(i, e) {
            $(this).removeClass("divposition");
            if (windowtop > $(e).offset().top) {
                $(e).addClass("divposition");
                return;
            }
        })
    });
	</script>
  </body>
</html>