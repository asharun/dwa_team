<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_task_type_for_activity extends CI_Migration
{

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        // Add all the possible column here
        $fields = array(
            'task_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'default' => NULL
            )
        );
        $this->dbforge->add_column('hier_act_log', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('hier_act_log', 'task_type');
    }
}

?>