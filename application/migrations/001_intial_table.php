<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_intial_table extends CI_Migration
{
	
	public function __construct()
  {
    parent::__construct();
    $this->load->dbforge();
  }
    public function up()
    {
				
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'access_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'access_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
              'icon' => array(
                'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			   'tab_name' => array(
                'type' => 'VARCHAR',
                 'constraint' => '100',
				 'null' => true
              ),
			   'tab_color' => array(
                'type' => 'VARCHAR',
                 'constraint' => '50',
				 'default' => '#813588'
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('access_id', TRUE);
        $this->dbforge->create_table('access_mst', FALSE, $attributes);
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'role_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'role_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			  'access_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('role_id', TRUE);
        $this->dbforge->create_table('role_mst', FALSE, $attributes);
		
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'dept_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'dept_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('dept_id', TRUE);
        $this->dbforge->create_table('dept_mst', FALSE, $attributes);
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'def_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'def_variable' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			  'def_val' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '20',
				 'null' => true
              ),
			   'start_dt' => array(
                'type' => 'date',
				 'null' => true
              ),
			  'end_dt' => array(
                'type' => 'date',
				 'null' => true
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('def_id', TRUE);
        $this->dbforge->create_table('def_values', FALSE, $attributes);
		
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'team_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'team_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('team_id', TRUE);
        $this->dbforge->create_table('team_mst', FALSE, $attributes);
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'project_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'project_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			  'team_id_fk' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'dept_id_fk' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('project_id', TRUE);
        $this->dbforge->create_table('project_mst', FALSE, $attributes);
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'user_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
			  'emp_id' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '50',
                 'null' => true
              ),
              'full_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ),
			  	'user_login_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '255',
				 'unique' => true
              ),
		 'role_id_fk' => array(
                 'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			'project_id_fk' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '100',
				 'null' => true
              ),
			   'dept_id_fk' => array(
                'type' => 'VARCHAR',
                 'constraint' => '20',
				 'null' => true
              ),
			  'week_off' => array(
                 'type' => 'INT',
                 'constraint' => 11,
				 'null' => true,
				 'default' => 0
              ),
			   'mail_flag' => array(
                 'type' => 'INT',
                 'constraint' => 2,
				 'null' => true,
				 'default' => 0
              ),			  
			  'streak' => array(
                 'type' => 'INT',
                 'constraint' => 11,
				 'null' => true,
				 'default' => 0
              ),
			'access_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ),
			  'manager_id' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('user_id', TRUE);
        $this->dbforge->create_table('user_mst', FALSE, $attributes);
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'task_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'task_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			   'dept_id_fk' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('task_id', TRUE);
        $this->dbforge->create_table('task_mst', FALSE, $attributes);
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'sub_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'sub_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			   'dept_id_fk' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('sub_id', TRUE);
        $this->dbforge->create_table('sub_mst', FALSE, $attributes);
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'job_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'job_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			   'dept_id_fk' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('job_id', TRUE);
        $this->dbforge->create_table('job_mst', FALSE, $attributes);
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'wu_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'wu_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			   'dept_id_fk' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'is_quantify' => array(
											'type' => 'VARCHAR',
											 'constraint' => '10',
											 'null' => true
											 ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('wu_id', TRUE);
        $this->dbforge->create_table('work_unit_mst', FALSE, $attributes);
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'activity_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'activity_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '1000',
				 'null' => true
              ),
			   'dept_id_fk' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'project_id_fk' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'null' => true
              ),
			 'task_id_fk' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'null' => true
              ),	
			  'sub_id_fk' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'null' => true
              ),
			  'job_id_fk' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'null' => true
              ),
			  'wu_id_fk' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'null' => true
              ),
			  'is_quantify' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'is_closed' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('activity_id', TRUE);
        $this->dbforge->create_table('activity_mst', FALSE, $attributes);
		$this->db->query('ALTER TABLE `activity_mst` ADD CONSTRAINT acti_comb UNIQUE (dept_id_fk,project_id_fk,task_id_fk,sub_id_fk,job_id_fk,wu_id_fk);');
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'std_tgt_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'activity_id_fk' => array(
                 'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'tgt_val' => array(
                 'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ),
			  'start_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'end_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('std_tgt_id', TRUE);
        $this->dbforge->create_table('std_tgt_mst', FALSE, $attributes);
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'gen_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'activity_id_fk' => array(
                 'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'dept_id_fk' => array(
                 'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'year' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			  'syllabus' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			  'grade' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			  'subject' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			  'chapter' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '5',
				 'null' => true
              ),			  
			  'sub_div' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '200',
				 'null' => true
              ),
			  'div_num' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '5',
				 'null' => true
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('gen_id', TRUE);
        $this->dbforge->create_table('mid_gen', FALSE, $attributes);
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'upload_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
			   'date_val' => array(
                'type' => 'date',
				 'null' => true
              ),
              'email_id' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 200,
				 'null' => true
              ),
			  'in_time' => array(
                 'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ),
			  'out_time' => array(
                 'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ),			  
			  'esp_status' => array(
                 'type' => 'VARCHAR',
                 'constraint' => '20',
				 'null' => true
              ),
			  'esp_wrk_hrs' => array(
                 'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ),
			  'sapience_on' => array(
                 'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ),
			  'sapience_off' => array(
                 'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ),
			   'sapience_off' => array(
                 'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ),
			  'total_days' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'attendance_cnt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'median_val' => array(
                'type' => 'timestamp',
				 'null' => true
              ),			  
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'user_id' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('upload_id', TRUE);
        $this->dbforge->create_table('in_out_upload', FALSE, $attributes);
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'n' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'null' => true
				)
			  )
        );
       // $this->dbforge->add_key('n', TRUE);
        $this->dbforge->create_table('numbers', FALSE, $attributes);
		
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'response_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'activity_id_fk' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'wu_id_fk' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'dept_id_fk' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'user_id_fk' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'day_val' => array(
                'type' => 'date',
				 'null' => true
              ),
			   'task_desc' => array(
                'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ), 
			  'std_tgt_val' => array(
                'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ),
				'work_comp' => array(
                'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ), 
				'equiv_xp' => array(
                'type' => 'DECIMAL',
                 'constraint' => '10,5',
				 'null' => true
              ), 
				'comp_xp' => array(
                'type' => 'DECIMAL',
                 'constraint' => '10,5',
				 'null' => true
              ), 	
				'comp_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'comp_upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'issue_flag' => array(
                'type' => 'INT',
                 'constraint' => 2,
				 'null' => true
              ),
				'issue_desc' => array(
                'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ),
			'issue_chk_flag' => array(
                  'type' => 'INT',
                 'constraint' => 2,
				 'null' => true
              ),
			  'rev_assignee' => array(
                'type' => 'INT',
                 'constraint' => '11',
				 'null' => true
              ),
			  'assignee_desc' => array(
                'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ),
			 'audit_work' => array(
                'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ), 
				'audit_xp' => array(
                'type' => 'DECIMAL',
                 'constraint' => '10,5',
				 'null' => true
              ), 
		'audit_user' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
		'audit_upd_dt' => array(
                  'type' => 'timestamp',
                 'null' => true
              ),
		'audit_desc' => array(
                  'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ),
		'ach_tgt' => array(
                  'type' => 'DECIMAL',
                 'constraint' => '10,5',
				 'null' => true
              ),
		'review_xp' => array(
                  'type' => 'DECIMAL',
                 'constraint' => '10,1',
				 'null' => true
              ),
		 'review_desc' => array(
                  'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ),
		'review_user' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
		'review_upd_dt' => array(
                  'type' => 'timestamp',
                 'null' => true
              ),
		'total_xp' => array(
                  'type' => 'DECIMAL',
                 'constraint' => '10,1',
				 'null' => true
              ),
		'conf_user' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
		'conf_upd_dt' => array(
                  'type' => 'timestamp',
                 'null' => true
              ),
		'final_xp' => array(
                  'type' => 'DECIMAL',
                 'constraint' => '10,1',
				 'null' => true
              ),
		'tast_stat' => array(
                  'type' => 'VARCHAR',
                 'constraint' => '20',
				 'null' => true
              ),			  
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('response_id', TRUE);
        $this->dbforge->create_table('response_mst', FALSE, $attributes);
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'hist_resp_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
			   'response_id_fk' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'dept_id_fk' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
              'activity_id_fk' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'wu_id_fk' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'user_id_fk' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'day_val' => array(
                'type' => 'date',
				 'null' => true
              ),
			   'task_desc' => array(
                'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ), 
			  'std_tgt_val' => array(
                'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ),
				'work_comp' => array(
                'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ), 
				'equiv_xp' => array(
                'type' => 'DECIMAL',
                 'constraint' => '10,5',
				 'null' => true
              ), 
				'comp_xp' => array(
                'type' => 'DECIMAL',
                 'constraint' => '10,5',
				 'null' => true
              ), 	
				'comp_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'comp_upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'issue_flag' => array(
                'type' => 'INT',
                 'constraint' => 2,
				 'null' => true
              ),
				'issue_desc' => array(
                'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ),
			'issue_chk_flag' => array(
                  'type' => 'INT',
                 'constraint' => 2,
				 'null' => true
              ),
			   'rev_assignee' => array(
                'type' => 'INT',
                 'constraint' => '11',
				 'null' => true
              ),
			  'assignee_desc' => array(
                'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ),
			 'audit_work' => array(
                'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ), 
				'audit_xp' => array(
                'type' => 'DECIMAL',
                 'constraint' => '10,5',
				 'null' => true
              ), 
		'audit_user' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
		'audit_upd_dt' => array(
                  'type' => 'timestamp',
                 'null' => true
              ),
		'audit_desc' => array(
                  'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ),
		'ach_tgt' => array(
                  'type' => 'DECIMAL',
                 'constraint' => '10,5',
				 'null' => true
              ),
		'review_xp' => array(
                  'type' => 'DECIMAL',
                 'constraint' => '10,1',
				 'null' => true
              ),
		 'review_desc' => array(
                  'type' => 'VARCHAR',
                 'constraint' => '500',
				 'null' => true
              ),
		'review_user' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
		'review_upd_dt' => array(
                  'type' => 'timestamp',
                 'null' => true
              ),
		'total_xp' => array(
                  'type' => 'DECIMAL',
                 'constraint' => '10,1',
				 'null' => true
              ),
		'conf_user' => array(
                  'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
		'conf_upd_dt' => array(
                  'type' => 'timestamp',
                 'null' => true
              ),
		'final_xp' => array(
                  'type' => 'DECIMAL',
                 'constraint' => '10,1',
				 'null' => true
              ),
		'tast_stat' => array(
                  'type' => 'VARCHAR',
                 'constraint' => '20',
				 'null' => true
              ),			  
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('hist_resp_id', TRUE);
        $this->dbforge->create_table('hist_response_mst', FALSE, $attributes);
		
		$attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->add_field(
           array(
              'audit_resp_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'auto_increment' => true
              ),
              'response_id_fk' => array(
                 'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'work_comp' => array(
                 'type' => 'DECIMAL',
                 'constraint' => '10,2',
				 'null' => true
              ),
			  'change_desc' => array(
                'type' => 'VARCHAR',
				'constraint' => '500',
				 'null' => true
              ),
			  'update_status' => array(
                'type' => 'VARCHAR',
				'constraint' => '10',
				 'null' => true
              ),
			  'status_nm' => array(
                'type' => 'VARCHAR',
                 'constraint' => '10',
				 'null' => true
              ), 
			  'ins_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			  'ins_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              ),
			  'upd_user' => array(
                'type' => 'INT',
                 'constraint' => 11,
				 'null' => true
              ),
			   'upd_dt' => array(
                'type' => 'timestamp',
				 'null' => true
              )
           )
        );
        $this->dbforge->add_key('audit_resp_id', TRUE);
        $this->dbforge->create_table('audit_pend_resp', FALSE, $attributes);
    }

    // public function down()
    // {
		// $this->dbforge->drop_table('audit_pend_resp');
	   // $this->dbforge->drop_table('hist_response_mst');
		// $this->dbforge->drop_table('response_mst');
		// $this->dbforge->drop_table('std_tgt_mst');
		// $this->db->query('ALTER TABLE `activity_mst` DROP CONSTRAINT acti_comb ;');
        // $this->dbforge->drop_table('activity_mst');
		// $this->dbforge->drop_table('work_unit_mst');
		// $this->dbforge->drop_table('job_mst');
		// $this->dbforge->drop_table('sub_mst');
		// $this->dbforge->drop_table('task_mst');
		// $this->dbforge->drop_table('user_mst');
		// $this->dbforge->drop_table('project_mst');
		// $this->dbforge->drop_table('team_mst');
		// $this->dbforge->drop_table('role_mst');
        // $this->dbforge->drop_table('access_mst');	
    // }
}

?>