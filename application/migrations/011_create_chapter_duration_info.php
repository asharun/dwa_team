<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_chapter_duration_info extends CI_Migration
{

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        // Add all the possible column here
        $fields = array(
            'id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'unsigned' => true,
                 'auto_increment' => true
            ),
            'chapter_id_fk' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => NULL
            ),
            'video_clip_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'default' => NULL
            ),
            'submitted' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'default' => NULL
            ),
            'recorded' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'default' => NULL
            ),
            'logged' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => NULL
            ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('chapter_duration_info');
    }

    public function down()
    {
        $this->dbforge->drop_table('chapter_duration_info');
    }
}

?>