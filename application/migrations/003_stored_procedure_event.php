<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_stored_procedure_event extends CI_Migration
{
	
	public function __construct()
	  {
		parent::__construct();
		$this->load->dbforge();
	  }
    public function up()
    {
		//$this->db->query('SET GLOBAL event_scheduler="ON";');		
		$this->db->query('CREATE PROCEDURE `streak_cal`()
								BEGIN    
								
									UPDATE user_mst U1
										JOIN(
											SELECT
												U.user_id,
												COUNT(DISTINCT R.user_id_fk) AS cnt
											FROM
												user_mst U
											LEFT JOIN response_mst R ON
												R.user_id_fk = U.user_id AND R.day_val = (SUBDATE(CURDATE(), 1))
											GROUP BY
												U.user_id
										) U2
										ON
											U1.user_id = U2.user_id
										SET
											U1.streak = U1.streak + U2.cnt;   
								END');
								
				$this->db->query("CREATE EVENT daily_streak ON SCHEDULE EVERY '1' DAY STARTS '2018-06-22 00:02:00' DO  CALL `streak_cal`();");
    }

    public function down()
    {
		$this->db->query('DROP EVENT daily_streak;');		
		$this->db->query('DROP PROCEDURE streak_cal();');		
		//$this->db->query('SET GLOBAL event_scheduler="OFF";');		
    }
}

?>