<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_allocation_extra_columns extends CI_Migration
{

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        // Add all the possible column here
        $fields = array(
            'presenter' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'default' => NULL
            ),
            'storyboard_incharge' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'default' => NULL
            ),
            'sign_off' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'default' => NULL
            ),
            'est_end_dt' => array(
                'type' => 'TIMESTAMP',
                'default' => NULL
            ),
			'duration' => array(
                'type' => 'INT',
				'constraint' => 11,
                'default' => NULL
            ),
            'mandays' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'default' => NULL
            ),
            'video_clip_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => NULL
            ),
            'storyboarded_by' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'default' => NULL
            ),

        );
        $this->dbforge->add_column('hierarchy_mst', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('hierarchy_mst', 'presenter');
        $this->dbforge->drop_column('hierarchy_mst', 'storyboard_incharge');
        $this->dbforge->drop_column('hierarchy_mst', 'sign_off');
		$this->dbforge->drop_column('hierarchy_mst', 'duration');
        $this->dbforge->drop_column('hierarchy_mst', 'est_end_dt');
        $this->dbforge->drop_column('hierarchy_mst', 'video_clip_type');
        $this->dbforge->drop_column('hierarchy_mst', 'storyboarded_by');
        $this->dbforge->drop_column('hierarchy_mst', 'mandays');
    }
}

?>